﻿using System;
using System.Collections.Generic;

namespace BusinessObject
{
    public class BOReceive
    {
        public static int Insert(DataObject.Receive item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAReceive.Insert(item, mDBConfig);
        }

        public static List<DataObject.Receive> GetReceive(int staffID, int supplierID, int cableID, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.Receive> lsArray = new List<DataObject.Receive>();
            lsArray = DataAccess.DAReceive.GetReceive(staffID, supplierID, cableID, false, DateTime.Now, DateTime.Now, mDBConfig);
            foreach (DataObject.Receive item in lsArray)
            {
                AddSuppelier(item, mDBConfig);
            }
            return lsArray;
        }

        public static List<DataObject.Receive> GetReceive(int staffID, int supplierID, int cableID, DateTime Start, DateTime End, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.Receive> lsArray = new List<DataObject.Receive>();
            lsArray = DataAccess.DAReceive.GetReceive(staffID, supplierID, cableID, true, Start, End, mDBConfig);
            foreach (DataObject.Receive item in lsArray)
            {
                AddSuppelier(item, mDBConfig);
            }
            return lsArray;
        }

        private static void AddSuppelier(DataObject.Receive item, SystemConfig.DBConfig mDBConfig)
        {
            item.Suppliers = BusinessObject.BOSuppliers.GetSuppliersByID(item.SupplierID, mDBConfig);
        }
    }
}