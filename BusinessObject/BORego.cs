﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObject
{
    public class BORego
    {
        public static List<DataObject.Rego> GetAll(int CustID)
        {
            return DataAccess.DARego.GetAll(CustID);
        }

        public static int Update(DataObject.Rego item)
        {
            return DataAccess.DARego.Update(item);
        }

        public static int Insert(DataObject.Rego item)
        {
            return DataAccess.DARego.Insert(item);
        }
        public static DataObject.Rego GetByID(int ID)
        {
            return DataAccess.DARego.GetByID(ID);
        }
    }
}
