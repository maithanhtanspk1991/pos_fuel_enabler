﻿namespace BusinessObject
{
    public class FunctionEnum
    {
        public static int GetVisual(DataObject.VisualType type)
        {
            int result = -1;
            switch (type)
            {
                case DataObject.VisualType.None:
                    result = -1;
                    break;

                case DataObject.VisualType.Display:
                    result = 1;
                    break;

                case DataObject.VisualType.Hiden:
                    result = 0;
                    break;
            }
            return result;
        }

        public static int GetSellSize(DataObject.SellType type)
        {
            int result = -1;
            switch (type)
            {
                case DataObject.SellType.Each:
                case DataObject.SellType.Mls:
                    result = 1;
                    break;

                case DataObject.SellType.Kg:
                case DataObject.SellType.Litre:
                    result = 1000;
                    break;
            }
            return result;
        }

        public static DataObject.SellType GetSellType(int i)
        {
            DataObject.SellType result = DataObject.SellType.Each;
            switch (i)
            {
                case 1:
                    result = DataObject.SellType.Each;
                    break;

                case 2:
                    result = DataObject.SellType.Mls;
                    break;

                case 3:
                    result = DataObject.SellType.Kg;
                    break;
            }
            return result;
        }

        public static int GetDelete(DataObject.DeleteType type)
        {
            int result = -1;
            switch (type)
            {
                case DataObject.DeleteType.None:
                    result = -1;
                    break;

                case DataObject.DeleteType.Deleted:
                    result = 1;
                    break;

                case DataObject.DeleteType.NoDelete:
                    result = 0;
                    break;
            }
            return result;
        }

        public static int GetIsFuel(DataObject.IsFuelType type)
        {
            int result = -1;
            switch (type)
            {
                case DataObject.IsFuelType.None:
                    result = -1;
                    break;

                case DataObject.IsFuelType.Yes:
                    result = 1;
                    break;

                case DataObject.IsFuelType.No:
                    result = 0;
                    break;
            }
            return result;
        }

        public static int GetCompleted(DataObject.CompletedType type)
        {
            int result = -1;
            switch (type)
            {
                case DataObject.CompletedType.None:
                    result = -1;
                    break;

                case DataObject.CompletedType.SubTotal:
                    result = 1;
                    break;

                case DataObject.CompletedType.NotSub:
                    result = 0;
                    break;
            }
            return result;
        }

        public static DataObject.TypeMenu GetTypeMenu(int item)
        {
            DataObject.TypeMenu result = DataObject.TypeMenu.NONE;
            switch (item)
            {
                case -1:
                    result = DataObject.TypeMenu.NONE;
                    break;

                case 0:
                    result = DataObject.TypeMenu.ALLMENU;
                    break;

                case 1:
                    result = DataObject.TypeMenu.TYREMENU;
                    break;
            }
            return result;
        }

        public static string GetTypeMenu(DataObject.TypeMenu type)
        {
            string result = "None";
            switch (type)
            {
                case DataObject.TypeMenu.NONE:
                    result = "None";
                    break;

                case DataObject.TypeMenu.ALLMENU:
                    result = "All Menu";
                    break;

                case DataObject.TypeMenu.TYREMENU:
                    result = "Tyre Menu";
                    break;
            }
            return result;
        }
    }
}