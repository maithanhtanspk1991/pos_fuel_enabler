﻿namespace BusinessObject
{
    public class BOCashInAndCashOut
    {
        public static int InsertSafeDropAuto(DataObject.CashInAndCashOut cashincashout)
        {
            return DataAccess.DACashInAndCashOut.InsertSafeDropAuto(cashincashout);
        }
    }
}