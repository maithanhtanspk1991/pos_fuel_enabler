﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOReceiveLine
    {
        public static List<DataObject.ReceiveLine> GetReceiveLine(int receiveID, int itemID, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.ReceiveLine> lsArray = new List<DataObject.ReceiveLine>();
            lsArray = DataAccess.DAReceiveLine.GetReceiveLine(receiveID, itemID, mDBConfig);
            AddItemList(lsArray, mDBConfig);
            return lsArray;
        }

        public static DataObject.ReceiveLine GetReceiveLineByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.ReceiveLine item = new DataObject.ReceiveLine();
            item = DataAccess.DAReceiveLine.GetReceiveLineByID(iD, mDBConfig);
            AddItem(item, mDBConfig);
            return item;
        }

        private static void AddItemList(List<DataObject.ReceiveLine> lsArray, SystemConfig.DBConfig mDBConfig)
        {
            foreach (DataObject.ReceiveLine item in lsArray)
            {
                item.ItemsMenu = BusinessObject.BOItemsMenu.GetItemMenuByID(item.ItemID, mDBConfig);
            }
        }

        private static void AddItem(DataObject.ReceiveLine item, SystemConfig.DBConfig mDBConfig)
        {
            item.ItemsMenu = BusinessObject.BOItemsMenu.GetItemMenuByID(item.ItemID, mDBConfig);
        }
    }
}