﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObject
{
    public class BOCard
    {
        public static List<DataObject.Card> GetAll(int IsActive)
        {
            return DataAccess.DACard.GetAll("", "", -1, 1, -1, -1, new DataObject.Limit() { Page = 0, PageSize = 0 });
        }
        public static int Insert(DataObject.Card item)
        {
            return DataAccess.DACard.Insert(item);
        }

        public static int Update(DataObject.Card item)
        {
            return DataAccess.DACard.Update(item);
        }
    }
}
