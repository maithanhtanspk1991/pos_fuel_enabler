﻿using System.Collections.Generic;
using DataAccess;
using DataObject;

namespace BusinessObject
{
    public class BOStaff
    {
        private SystemConfig.DBConfig dbconfig;
        private DAStaff das;

        public BOStaff(SystemConfig.DBConfig dbconfig)
        {
            this.dbconfig = dbconfig;
            das = new DAStaff(dbconfig);
        }

        public List<Staff> GetListStaff()
        {
            //DAStaff das = new DAStaff(dbconfig);
            List<Staff> lst = das.GetListStaff();
            return lst;
        }

        public bool CheckStaff(Staff s)
        {
            return das.CheckStaff(s);
        }

        public int InsertStaff(Staff s)
        {
            return das.InsertStaff(s);
        }

        public int UpdateStaff(Staff s)
        {
            return das.UpdateStaff(s);
        }

        public int DeleteStaff(Staff s)
        {
            return das.DeleteStaff(s);
        }

        public bool CheckLogin(ref Staff s)
        {
            return das.CheckLogin(ref s);
        }

        public static Staff GetStaffById(int ID, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAStaff.GetStaffById(ID, mDBConfig);
        }

        public static DataObject.Staff GetByStaffID(string ID)
        {
            return DataAccess.DAStaff.GetByStaffID(ID);
        }
    }
}