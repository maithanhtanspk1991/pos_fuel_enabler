﻿using System;

namespace BusinessObject
{
    public class BOConfig
    {
        private static SystemConfig.DBConfig mDBConfig = new SystemConfig.DBConfig();

        //Giá trị lấy dòng hiển thị của menu
        public static int GetMenuRowItemCount
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "RowItemCount", "7", mDBConfig)); }
        }

        //Giá trị lấy cột hiển thị của menu
        public static int GetMenuColumnsItemCount
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "ColunmsItemCount", "2", mDBConfig)); }
        }

        //Giá trị lấy dòng hiển thị của menu
        public static int GetMenuRowGroupCount
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "RowGroupCount", "7", mDBConfig)); }
        }

        //Giá trị lấy cột hiển thị của menu
        public static int GetMenuColumnsGroupCount
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "ColunmsGroupCount", "1", mDBConfig)); }
        }

        //Giá trị lấy cột hiển thị của menu
        public static int GetMenuGroupWidth
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "GroupWidth", "75", mDBConfig)); }
        }

        //Giá trị lấy cột hiển thị của menu
        public static int GetMenuItemWidth
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Menu", "ItemWidth", "300", mDBConfig)); }
        }

        //Giá trị lấy đường dẫn hình của group menu
        public static string GetMenuImagesGroup
        {
            get { return DataAccess.DAConfig.GetConfig("Menu", "ImagesGroup", "D:\\\\", mDBConfig).ToString(); }
        }

        //Giá trị lấy đường dẫn hình của items menu
        public static string GetMenuImagesItems
        {
            get { return DataAccess.DAConfig.GetConfig("Menu", "ImagesItems", "D:\\\\", mDBConfig).ToString(); }
        }

        //Giá trị lấy đường dẫn hình mặc định menu
        public static string GetMenuNoImages
        {
            get { return DataAccess.DAConfig.GetConfig("Menu", "NoImages", "D:\\\\Noimages.png", mDBConfig).ToString(); }
        }

        //Giá trị lấy dòng hiển thị của menu
        public static int GetCableID
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Cable", "ID", "0", mDBConfig)); }
        }

        //Giá trị lấy dòng hiển thị của menu
        public static string GetVersion
        {
            get { return DataAccess.DAConfig.GetConfig("Cable", "Version", "1.0.0.0", mDBConfig).ToString(); }
        }

        //Khi thoát hỏi câu này
        public static string GetExitSystem
        {
            get { return DataAccess.DAConfig.GetConfig("System", "ExitSystem", "Do you want exit POS System?", mDBConfig).ToString(); }
        }

        //Khi quẹt barcode có item mới hỏi câu này
        public static string GetNoticeNewItems
        {
            get { return DataAccess.DAConfig.GetConfig("Menu", "NoticeNewItems", "Do you want to create a new?", mDBConfig).ToString(); }
        }

        //Số ngày cho refunce
        public static int GetRefunceDate
        {
            get { return Convert.ToInt32(DataAccess.DAConfig.GetConfig("Refunce", "Date", "1", mDBConfig)); }
        }

        //Số ngày cho refunce
        public static double GetCustomerAccountLimit
        {
            get { return Convert.ToDouble(DataAccess.DAConfig.GetConfig("Customer", "AccountLimit", "1000", mDBConfig)); }
        }

        public static string GetCurency
        {
            get { return DataAccess.DAConfig.GetConfig("Cable", "Curency", "$", mDBConfig).ToString(); }
        }

        public static string GetPrinterOrder
        {
            get { return DataAccess.DAConfig.GetConfig("Printer", "Order", "\\\\\\\\192.168.0.100\\\\TM200 - copy 1", mDBConfig).ToString(); }
        }

        public static string SetPrinterOrder
        {
            set
            {
                value = value.Replace("\\", "\\\\");
                DataAccess.DAConfig.SetConfig("Printer", "Order", value, mDBConfig);
            }
        }

        public static string GetPrinterCustomer
        {
            get { return DataAccess.DAConfig.GetConfig("Printer", "Customer", "\\\\\\\\192.168.0.100\\\\TM200 - copy 1", mDBConfig).ToString(); }
        }

        public static string SetPrinterCustomer
        {
            set
            {
                value = value.Replace("\\", "\\\\");
                DataAccess.DAConfig.SetConfig("Printer", "Customer", value, mDBConfig);
            }
        }

        #region Thông tin của công ty

        public static string GetCompanyInfomationHeader
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "Header", "BECAS", mDBConfig).ToString(); }
        }

        public static string GetCompanyInfomationBankCode
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "BankCode", "(61) 401950967", mDBConfig).ToString(); }
        }

        public static string GetCompanyInfomationAddress
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "Address", "Unit F4, Level 1/101 Rookwood Road, Yagoona NSW 2199", mDBConfig).ToString(); }
        }

        public static string GetCompanyInfomationTell
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "Tell", "(612) 94327867", mDBConfig).ToString(); }
        }

        public static string GetCompanyInfomationWebsite
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "Website", "www.becas.net.au", mDBConfig).ToString(); }
        }

        public static string GetCompanyInfomationThankyou
        {
            get { return DataAccess.DAConfig.GetConfig("CompanyInfomation", "Thankyou", "Thank you, see you soon", mDBConfig).ToString(); }
        }

        public static string SetCompanyInfomationHeader
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "Header", value, mDBConfig); }
        }

        public static string SetCompanyInfomationBankCode
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "BankCode", value, mDBConfig); }
        }

        public static string SetCompanyInfomationAddress
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "Address", value, mDBConfig); }
        }

        public static string SetCompanyInfomationTell
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "Tell", value, mDBConfig); }
        }

        public static string SetCompanyInfomationWebsite
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "Website", value, mDBConfig); }
        }

        public static string SetCompanyInfomationThankyou
        {
            set { DataAccess.DAConfig.SetConfig("CompanyInfomation", "Thankyou", value, mDBConfig); }
        }

        #endregion Thông tin của công ty

        public static int GetDepartmentConfig(string value, SystemConfig.DBConfig dbconfig)
        {
            int resuilt = 0;
            object obj = DataAccess.DAConfig.GetConfig("Department", value, dbconfig);
            try
            {
                resuilt = Convert.ToInt32(obj.ToString());
            }
            catch (Exception)
            {
                DataAccess.DAConfig.SetConfig("Department", value, resuilt + "", dbconfig);
            }
            return resuilt;
        }

        public static int GetScaleConfig(string value, SystemConfig.DBConfig dbconfig)
        {
            int resuilt = 0;
            object obj = DataAccess.DAConfig.GetConfig("Scale", value, dbconfig);
            try
            {
                resuilt = Convert.ToInt32(obj.ToString());
            }
            catch (Exception)
            {
                DataAccess.DAConfig.SetConfig("Scale", value, resuilt + "", dbconfig);
            }
            return resuilt;
        }

        public static double GetOrderService
        {
            get { return Convert.ToDouble(DataAccess.DAConfig.GetConfig("Order", "Service", "0", mDBConfig).ToString()); }
        }

        #region ImageSourceForCustomer

        public static string GetCustomerImage
        {
            get { return DataAccess.DAConfig.GetConfig("CustomerDisplay", "Images", "C:\\\\Customer\\\\", mDBConfig).ToString(); }
        }

        #endregion ImageSourceForCustomer

        public static string GetWelcomeVFD(string value, SystemConfig.DBConfig dbconfig)
        {
            string resuilt = "Welcome Supermarket";
            object obj = DataAccess.DAConfig.GetConfig("VFD", value, dbconfig);
            try
            {
                resuilt = obj.ToString();
            }
            catch (Exception)
            {
                DataAccess.DAConfig.SetConfig("VFD", value, resuilt + "", dbconfig);
            }
            return resuilt;
        }

        public static void SetCurrentShift(int value)
        {
            DataAccess.DAConfig.SetConfig("Shift", "CurrentShift", value.ToString(), mDBConfig);
        }

        public static int GetCurrentShift(SystemConfig.DBConfig dbconfig)
        {
            object obj = DataAccess.DAConfig.GetConfig("Shift", "CurrentShift", dbconfig);
            try
            {
                return int.Parse(obj.ToString());
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("BoConfig::GetCurrentShift::" + ex.Message);
                return -1;
            }
        }

        public static void SetCurrentCashFloatIn(double value)
        {
            DataAccess.DAConfig.SetConfig("Shift", "CurrentCashFloatIn", value.ToString(), mDBConfig);
        }

        public static double GetCurrentCashFloatIn(SystemConfig.DBConfig dbconfig)
        {
            object obj = DataAccess.DAConfig.GetConfig("Shift", "CurrentCashFloatIn", dbconfig);
            try
            {
                return double.Parse(obj.ToString());
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("BoConfig::GetCurrentShift::" + ex.Message);
                return -1;
            }
        }

        public static bool CheckPassWord(SystemConfig.DBConfig dbconfig, string text)
        {
            object obj = DataAccess.DAConfig.GetConfig("Pass", "ManagerPassWord", dbconfig);            
            if (text == string.Empty)
            {
                return false;
            }
            if (obj.ToString() == DataObject.PassWord.MaHoaPassWord(text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void SetPassWord(string text, SystemConfig.DBConfig dbconfig)
        {
            DataAccess.DAConfig.SetConfig("Pass", "ManagerPassWord", DataObject.PassWord.MaHoaPassWord(text), dbconfig);
        }
    }
}