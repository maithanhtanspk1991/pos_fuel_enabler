﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObject
{
    public class BOCustomers
    {
        public static DataObject.Customers GetByID(int CustID, int isActive, int deleted)
        {
            return DataAccess.DACustomers.GetBy(CustID, "", isActive, deleted);
        }
        public static DataObject.Customers GetByID(int CustID)
        {
            return GetByID(CustID, -1, 0);
        }

        public static DataObject.Customers GetByMemberNo(string MemberNo, int isActive, int deleted)
        {
            return DataAccess.DACustomers.GetBy(-1, MemberNo, isActive, deleted);
        }
        public static DataObject.Customers GetByMemberNo(string MemberNo)
        {
            return GetByMemberNo(MemberNo, 1, 0);
        }

        public static int Deleted(DataObject.Customers item)
        {
            return DataAccess.DACustomers.Deleted(item);
        }
        public static int Insert(DataObject.Customers item)
        {
            return DataAccess.DACustomers.Insert(item);
        }

        public static int Update(DataObject.Customers item)
        {
            return DataAccess.DACustomers.Update(item);
        }

        public static List<DataObject.Customers> GetAll(string lastName, string firstName, string phone, string mobile, string email, string memberNo, string cardNo, string postCode, string streetNo, string streetName, int isActive, int deleted, int company, string orderBy, DataObject.Limit limit)
        {
            List<DataObject.Customers> lsArray = DataAccess.DACustomers.GetAll(lastName, firstName, phone, mobile, email, memberNo, cardNo, postCode, streetNo, streetName, isActive, deleted, company, orderBy, limit);
            List<DataObject.Company> lsCompany = BusinessObject.BOCompany.GetAll("", -1);
            foreach (DataObject.Customers item in lsArray)
            {
                if (item.Company != 0)
                    if (lsCompany.Exists(s => s.IdCompany == item.Company))
                        item.CompanyName = lsCompany.Find(s => s.IdCompany == item.Company).CompanyName;
            }
            return lsArray;
        }

        public static List<DataObject.Customers> GetAll(string lastName, string firstName, string mobile, string memberNo, string email, string streetNo, string streetName, int isActive, int deleted, int company, string orderBy, DataObject.Limit limit)
        {
            return GetAll(lastName, firstName, "", mobile, email, memberNo, "", "", streetNo, streetName, isActive, deleted, company, orderBy, limit);
        }
        public static List<DataObject.Customers> GetAll(string lastName, string firstName, string mobile, string memberNo, string email, string streetNo, string streetName, string orderBy, DataObject.Limit limit)
        {
            return GetAll(lastName, firstName, "", mobile, email, memberNo, "", "", streetNo, streetName, -1, 0, -1, orderBy, limit);
        }

        public static List<DataObject.Customers> GetByCompanyID(int company)
        {
            return GetAll("", "", "", "", "", "", "", "", "", "", -1, 0, company, "", new DataObject.Limit() { Page = 0, PageSize = 0 });
        }

        public static int DeletedByConpanyID(int ConpanyID)
        {
            return DataAccess.DACustomers.DeletedByConpanyID(ConpanyID);
        }

        public static List<DataObject.Customers> Search(string regon, string name, string phone, DataObject.Limit limit)
        {
            return DataAccess.DACustomers.Search(regon, name, phone, 1, 0, limit);
        }
    }
}
