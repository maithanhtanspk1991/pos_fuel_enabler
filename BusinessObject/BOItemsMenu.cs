﻿using System.Collections.Generic;
using DataObject;

namespace BusinessObject
{
    public class BOItemsMenu
    {
        public static List<DataObject.ItemsMenu> GetAll(int groupID, string barcode, string itemname, int visual, int deleted, int IsFuel, int TypeMenu, SystemConfig.DBConfig mDBConfig)
        {
            return GetAll(groupID, barcode, itemname, visual, deleted, IsFuel, -1, -1, mDBConfig, new Limit());
        }
        public static List<DataObject.ItemsMenu> GetAll(int groupID, string barcode, string itemname, int visual, int deleted, int IsFuel, int TypeMenu, int sizeItem, SystemConfig.DBConfig mDBConfig, DataObject.Limit limit)
        {
            return DataAccess.DAItemsMenu.GetAll(groupID, barcode, itemname, -1, -1, visual, deleted, IsFuel, TypeMenu, sizeItem, mDBConfig, limit);
        }
        public static List<DataObject.ItemsMenu> GetItemsMenu(int groupID, string barcode, string itemname, int visual, int deleted, int IsFuel, SystemConfig.DBConfig mDBConfig)
        {
            return GetAll(groupID, barcode, itemname, visual, deleted, IsFuel, -1, mDBConfig);
        }

        public static List<DataObject.ItemsMenu> GetByTypeMenu(int TypeMenu, int sizeItem, SystemConfig.DBConfig mDBConfig, Limit limit)
        {
            return GetAll(-1, "", "", -1, 0, -1, TypeMenu, sizeItem, mDBConfig, limit);
        }

        public static List<DataObject.ItemsMenu> GetAll(int TypeMenu, int sizeItem, string barcode, string itemname, SystemConfig.DBConfig mDBConfig, Limit limit)
        {
            return GetAll(-1, barcode, itemname, -1, 0, -1, TypeMenu, sizeItem, mDBConfig, limit);
        }

        public static int UpdateBarcode(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.UpdateBarcode(item, mDBConfig);
        }



        public static List<DataObject.ItemsMenu> GetFindItem(string barcode, string itemname, SystemConfig.DBConfig mDBConfig)
        {
            return GetItemsMenu(-1, barcode, itemname, 1, 0, 0, mDBConfig);
        }

        public static List<DataObject.ItemsMenu> GetFindItem(int groupID, string barcode, string itemname, int isFuel, SystemConfig.DBConfig mDBConfig)
        {
            return GetItemsMenu(groupID, barcode, itemname, -1, 0, isFuel, mDBConfig);
        }

        public static List<DataObject.ItemsMenuGroup> GetItemsMenuGroup(ref int ListCount, int groupID, int DisplayOrder, int visual, int deleted, int IsFuel, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.ItemsMenuGroup> listItemsMenuGroup = new List<DataObject.ItemsMenuGroup>();
            List<DataObject.ItemsMenu> listItemsMenu = new List<DataObject.ItemsMenu>();
            listItemsMenu = GetItemsMenu(groupID, "", "", visual, deleted, IsFuel, mDBConfig);
            ListCount = listItemsMenu.Count;
            int ShowDisplay = 1;
            if (listItemsMenu.Count > 0)
            {
                int Index = 0;
                DataObject.ItemsMenuGroup img = new DataObject.ItemsMenuGroup();
                foreach (DataObject.ItemsMenu gm in listItemsMenu)
                {
                    gm.ShowDisplay = ShowDisplay++;
                    if (Index == 0)
                    {
                        img = new DataObject.ItemsMenuGroup();
                    }
                    img.Items.Add(gm);
                    Index++;
                    if (Index > DisplayOrder - 1)
                    {
                        listItemsMenuGroup.Add(img);
                        Index = 0;
                    }
                }
                if (Index != 0)
                    listItemsMenuGroup.Add(img);
            }
            return listItemsMenuGroup;
        }

        public static int Delete(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.Delete(item, mDBConfig);
        }

        public static int DeleteGroup(int groupid, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.DeleteGroup(groupid, mDBConfig);
        }

        public static int Insert(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.Insert(item, mDBConfig);
        }

        public static void ProcessDynItem(ItemsMenu item, SystemConfig.DBConfig dbconfig)
        {
            int dynID = DataAccess.DAItemsMenu.GetDynID(item, dbconfig);
            if (dynID > 0)
            {
                item.ItemID = dynID;
            }
            else
            {
                DataAccess.DAItemsMenu.InsertDynItem(item, dbconfig);
            }
        }

        public static int Update(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.Update(item, mDBConfig);
        }

        public static ItemsMenu GetItemMenuByID(int itemID, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAItemsMenu.GetItemMenuByID(itemID, dbconfig);
        }

        public static ItemsMenu GetItemMenuByBarcode(string Barcode, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.GetItemMenuByBarcode(Barcode, 0, mDBConfig);
        }

        public static int RefreshDisplayOrder(int GroupID, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.RefreshDisplayOrder(GroupID, mDBConfig);
        }

        public static void ChangeDispalyOrder(ItemsMenu item, bool DispalyOrderChange, SystemConfig.DBConfig mDBConfig)
        {
            if (DispalyOrderChange)
            {
                double DispalyOrderTop = 0;
                double DispalyOrderBottom = 0;
                DispalyOrderTop = DataAccess.DAItemsMenu.GetDisplayOrder(item.ShowDisplay, item.GroupID, mDBConfig);
                DispalyOrderTop = DispalyOrderTop == 0 ? 0 : DispalyOrderTop;
                DispalyOrderBottom = DataAccess.DAItemsMenu.GetDisplayOrder(item.ShowDisplay + 1, item.GroupID, mDBConfig);
                if (DispalyOrderBottom <= 0)
                {
                    item.DisplayOrder = item.ShowDisplay;
                }
                else
                {
                    item.DisplayOrder = (DispalyOrderTop + DispalyOrderBottom) / 2;
                }
            }
        }

        public static string GetBarcode(SystemConfig.DBConfig mDBConfig)
        {
            string MemberNo = DataObject.CreateBarcode.CalculateChecksumDigit("893", 12);
            while (CheckBarcodeItemsMenu(MemberNo, mDBConfig))
            {
                MemberNo = DataObject.CreateBarcode.CalculateChecksumDigit("893", 12);
            }
            return MemberNo;
        }

        public static bool CheckBarcodeItemsMenu(string MemberNo, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.ItemsMenu c = new DataObject.ItemsMenu();
            c = DataAccess.DAItemsMenu.GetBarcodeItemsMenu(MemberNo, mDBConfig);
            if (c.ItemID > 0)
                return true;
            else
                return false;            
        }
        public static List<DataObject.SellTypeObject> GetSellType(SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAItemsMenu.GetListSellType(mDBConfig);
        }
    }
}