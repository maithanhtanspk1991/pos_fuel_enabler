﻿using System.Collections.Generic;
using DataObject;

namespace BusinessObject
{
    public class BOGroupMenu
    {
        //private static string _nameClass = "BOGroupMenu";

        public static List<DataObject.GroupMenu> GetGroupMenu(int ShortCut, int visual, int deleted, bool ExistsItem, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.GetGroupMenu(-1, ShortCut, -1, visual, deleted, ExistsItem, mDBConfig);
        }

        /// <summary>
        /// Lay danh sach group theo group count
        /// </summary>
        /// <param name="ShortCut"></param>
        /// <returns></returns>
        public static List<DataObject.GroupMenuGroup> GetGroupMenuGroup(ref int ListCount, int DisplayOrder, int ShortCut, int visual, int deleted, bool ExistsItem, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.GroupMenuGroup> listGroupMenuGroup = new List<DataObject.GroupMenuGroup>();
            List<DataObject.GroupMenu> listGroupMenu = new List<DataObject.GroupMenu>();
            listGroupMenu = GetGroupMenu(ShortCut, visual, deleted, ExistsItem, mDBConfig);
            ListCount = listGroupMenu.Count;
            int ShowDisplay = 1;
            if (listGroupMenu.Count > 0)
            {
                int Index = 0;
                DataObject.GroupMenuGroup gmg = new DataObject.GroupMenuGroup();
                foreach (DataObject.GroupMenu gm in listGroupMenu)
                {
                    gm.ShowDisplay = ShowDisplay++;
                    if (Index == 0)
                    {
                        gmg = new DataObject.GroupMenuGroup();
                    }
                    gm.Color = Index;
                    gmg.Group.Add(gm);
                    Index++;
                    if (Index > DisplayOrder - 1)
                    {
                        listGroupMenuGroup.Add(gmg);
                        Index = 0;
                    }
                }
                if (Index != 0)
                    listGroupMenuGroup.Add(gmg);
            }
            return listGroupMenuGroup;
        }

        public static int Delete(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = DataAccess.DAGroupMenu.Delete(item, mDBConfig);
            if (result > -1)
            {
                result = BOItemsMenu.DeleteGroup(item.GroupID, mDBConfig);
            }
            return result;
        }

        public static int Insert(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.Insert(item, mDBConfig);
        }

        public static int Update(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.Update(item, mDBConfig);
        }

        public static void ChangeDispalyOrder(GroupMenu item, bool DispalyOrderChange, SystemConfig.DBConfig mDBConfig)
        {
            if (DispalyOrderChange)
            {
                double DispalyOrderTop = 0;
                double DispalyOrderBottom = 0;
                DispalyOrderTop = DataAccess.DAGroupMenu.GetDisplayOrder(item.ShowDisplay, mDBConfig);
                DispalyOrderTop = DispalyOrderTop == 0 ? 0 : DispalyOrderTop;
                DispalyOrderBottom = DataAccess.DAGroupMenu.GetDisplayOrder(item.ShowDisplay + 1, mDBConfig);
                if (DispalyOrderBottom <= 0)
                {
                    item.DisplayOrder = item.ShowDisplay;
                }
                else
                {
                    item.DisplayOrder = (DispalyOrderTop + DispalyOrderBottom) / 2;
                }
            }
        }

        public static int RefreshDisplayOrder(SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.RefreshDisplayOrder(mDBConfig);
        }

        public static double GetDisplayOrder(int ShowDisplay, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.GetDisplayOrder(ShowDisplay, mDBConfig);
        }

        public static GroupMenu GetGroupMenuByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAGroupMenu.GetGroupMenuByID(iD, mDBConfig);
        }
    }
}