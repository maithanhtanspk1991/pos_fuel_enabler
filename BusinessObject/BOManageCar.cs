﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOManageCar
    {
        public static List<DataObject.ManageCar> GetListManageCarWithCustomerID(DataObject.ManageCar ManageCar, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAManageCar.GetListManageCarWithCustomerID(ManageCar, dbconfig);
        }

        public static int Insert(ref DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAManageCar.Insert(ref managecar, dbconfig);
        }

        public static int Update(DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAManageCar.Update(managecar, dbconfig);
        }

        public static int Delete(DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAManageCar.Delete(managecar, dbconfig);
        }
    }
}