﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOReason
    {
        public static List<DataObject.Reason> GetAllReason(SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAReason.GetAllReason(dbconfig);
        }
    }
}