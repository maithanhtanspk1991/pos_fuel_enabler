﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOSuppliers
    {
        public static int Insert(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DASuppliers.Insert(item, mDBConfig);
        }

        public static int Update(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DASuppliers.Update(item, mDBConfig);
        }

        public static int Delete(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DASuppliers.Delete(item, mDBConfig);
        }

        public static List<DataObject.Suppliers> GetSuppliers(string name, int active, int deleted, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DASuppliers.GetSuppliers(name, active, deleted, mDBConfig);
        }

        public static List<DataObject.Suppliers> GetSuppliers(string name, int active, SystemConfig.DBConfig mDBConfig)
        {
            return GetSuppliers(name, active, 0, mDBConfig);
        }

        public static DataObject.Suppliers GetSuppliersByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DASuppliers.GetSuppliersByID(iD, mDBConfig);
        }
    }
}