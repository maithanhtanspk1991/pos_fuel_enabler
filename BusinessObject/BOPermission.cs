﻿using System.Collections.Generic;
using DataAccess;
using DataObject;

namespace BusinessObject
{
    public class BOPermission
    {
        public BOPermission()
        { }

        public List<Permission> GetListPermission(SystemConfig.DBConfig dbconfig)
        {
            return DAPermission.GetListPermission(dbconfig);
        }
    }
}