﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOShiftReport
    {
        public static DataObject.ShiftReport GetReportShift(DataObject.ShiftReport shift)
        {
            return DataAccess.DAShiftReport.GetReportShift(shift);
        }

        public static DataObject.ShiftReport GetReportShiftAll(DataObject.ShiftReport shift, int CableID)
        {
            return DataAccess.DAShiftReport.GetReportShiftAll(shift, CableID);
        }

        public static List<DataObject.CardReportShift> GetListCardReportShift(DataObject.ShiftReport shift)
        {
            return DataAccess.DAShiftReport.GetListCardReportShift(shift);
        }

        public static List<DataObject.CardReportShift> GetListCardReportShiftAll(DataObject.ShiftReport shift)
        {
            return DataAccess.DAShiftReport.GetListCardReportShiftAll(shift);
        }
        public List<DataObject.CardReportShift> getCardName()
        {
            return DataAccess.DAShiftReport.getCardName();
        }
    }
}