﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObject
{
    public class BOFuelHistory
    {
        public static List<DataObject.FuelHistory> GetAll()
        {
            return DataAccess.DAFuelHistory.GetAll();
        }

        public static DataObject.FuelHistory GetFuelHistoryByID(int ID)
        {
            return DataAccess.DAFuelHistory.GetFuelHistoryByID(ID);
        }

        public static int UpdateCableIDByID(int ID, int CableID)
        {
            return DataAccess.DAFuelHistory.UpdateCableIDByID(ID, CableID);
        }

        public static int UpdateAllRefreshCableIDByID(int CableID)
        {
            return DataAccess.DAFuelHistory.UpdateAllRefreshCableIDByID(CableID);
        }

        public static int UpdateAllRefreshCableIDByID(int CableID, string NotID)
        {
            return DataAccess.DAFuelHistory.UpdateAllRefreshCableIDByID(CableID, NotID);
        }
        public static int InsertFuelHistory(DataObject.FuelHistory fuel)
        {
            return DataAccess.DAFuelHistory.InsertFuelHistory(fuel);
        }
        public static int getIDbyitemIDandPumpID(int itemID, int PumpID)
        {
            return DataAccess.DAFuelHistory.getIDbyitemIDandPumpID(itemID, PumpID);
        }
        public static int GetmaxID()
        {
            return DataAccess.DAFuelHistory.GetMaxID();
        }
        public static int UpdateCompleteOrder(int orderID, int pumpID, int fuelID)
        {
            return DataAccess.DAFuelHistory.UpdateCompleteOrder(orderID, pumpID, fuelID);
        }
    }
}
