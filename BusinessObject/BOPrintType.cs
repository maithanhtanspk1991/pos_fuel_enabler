﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessObject
{
    public class BOPrintType
    {
        public static List<DataObject.PrinterTypeObject> Get()
        {
            List<DataObject.PrinterTypeObject> lsArray = new List<DataObject.PrinterTypeObject>();
            int i = 0;
            foreach (DataObject.PrinterType item in Enum.GetValues(typeof(DataObject.PrinterType)).Cast<DataObject.PrinterType>().ToList())
            {
                DataObject.PrinterTypeObject _PrinterType = new DataObject.PrinterTypeObject((i++).ToString(), item.ToString(), item);
                lsArray.Add(_PrinterType);
            }
            return lsArray;
        }
    }
}