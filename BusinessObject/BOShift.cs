﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOShift
    {
        public static List<DataObject.ShiftObject> GetListShift()
        {
            return DataAccess.DAShift.GetListShift(-1);
        }

        public static List<DataObject.ShiftObject> GetListShift(int CableID)
        {
            return DataAccess.DAShift.GetListShift(CableID);
        }

        public static List<DataObject.ShiftObject> GetListShiftWithDate(DataObject.ShiftObject shift, int CableID)
        {
            return DataAccess.DAShift.GetListShiftWithDate(shift, CableID);
        }

        public static List<string> GetCableID()
        {
            return DataAccess.DAShift.GetCableID();
        }

        public static int InsertShift(ref DataObject.ShiftObject shift)
        {
            return DataAccess.DAShift.InsertShift(ref shift);
        }

        public static DataObject.ShiftObject GetShiftMaxWithCableID(DataObject.ShiftObject shift)
        {
            return DataAccess.DAShift.GetShiftMaxWithCableID(shift);
        }

        public static DataObject.ShiftObject GetShiftMaxWithShiftID(DataObject.ShiftObject shift)
        {
            return DataAccess.DAShift.GetShiftMaxWithShiftID(shift);
        }

        public static int UpdateCompleted(DataObject.ShiftObject shift)
        {
            return DataAccess.DAShift.UpdateCompleted(shift);
        }

        public static int GetCashFloatIn(ref DataObject.ShiftObject shift)
        {
            return DataAccess.DAShift.GetFloatCashIn(ref shift);
        }
    }
}