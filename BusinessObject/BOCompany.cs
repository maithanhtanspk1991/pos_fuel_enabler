﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObject
{
    public class BOCompany
    {
        public static List<DataObject.Company> GetAll(string companyName, int Active)
        {
            return DataAccess.DACompany.GetAll(companyName, Active);
        }

        public static List<DataObject.Company> GetAll(string companyName, int Active, DataObject.Limit limit)
        {
            return DataAccess.DACompany.GetAll(companyName, Active, limit);
        }

        public static DataObject.Company GetByID(int idCompany)
        {
            return DataAccess.DACompany.GetBy(idCompany, "", "", -1);
        }

        public static DataObject.Company GetByCode(string companyCode)
        {
            return DataAccess.DACompany.GetBy(-1, companyCode, "", -1);
        }

        public static DataObject.Company GetByPhone(string phone)
        {
            return DataAccess.DACompany.GetBy(-1, "", phone, -1);
        }

        /// <summary>
        /// Trường ID không phải để tìm kiếm, Trường ID để so sánh khác
        /// </summary>
        /// <param name="idCompany"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static int GetCountByPhone(int idCompany, string phone)
        {
            return DataAccess.DACompany.GetCountBy(idCompany, "", phone, -1);
        }

        public static int Insert(DataObject.Company item)
        {
            return DataAccess.DACompany.Insert(item);
        }
        public static int Update(DataObject.Company item)
        {
            return DataAccess.DACompany.Update(item);
        }
        public static int Deleted(DataObject.Company item)
        {
            if (DataAccess.DACompany.Deleted(item) > -1)
            {
                return BOCustomers.DeletedByConpanyID(item.IdCompany);
            }
            return -1;
        }
        public static int GetCode(string str)
        {
            return DataAccess.DACompany.GetCode(str);
        }

        public static int GetCountAccountByID(int ID)
        {
            return BusinessObject.BOCustomers.GetByCompanyID(ID).Count;
        }
    }
}
