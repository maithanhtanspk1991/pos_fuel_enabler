﻿using System.Collections.Generic;

namespace BusinessObject
{
    public class BOStockCheckItem
    {
        public static DataObject.StockItemCheck GetItemCheckFormItemMenu(DataObject.ItemsMenu itemmenu, SystemConfig.DBConfig dbconfig)
        {
            return DataAccess.DAStockCheckItem.GetItemCheckFormItemMenu(itemmenu, dbconfig);
        }

        public static int Insert(SystemConfig.DBConfig dbconfig, List<DataObject.StockItemCheck> lst, DataObject.Transit transit)
        {
            return DataAccess.DAStockCheckItem.Insert(dbconfig, lst, transit);
        }

        public static int GetQtyOnHand(int ItemID, SystemConfig.DBConfig mDBConfig)
        {
            return DataAccess.DAStockCheckItem.GetQtyOnHand(ItemID, mDBConfig);
        }
    }
}