﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessObject
{
    public class BOSellType
    {
        public static List<DataObject.SellTypeObject> GetType(SystemConfig.DBConfig mDBConfig)
        {
            return BOItemsMenu.GetSellType(mDBConfig);
        }
    }
}