using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Becas POS")]
[assembly: AssemblyDescription("BECAS Technology Australia, POS Vietnam, POS system Florida, POS system Miami, POS system USA, POS Sydney, POS Melbourne, POS Perth, POS Adelaide, Paging System, Restaurant Paging System, Club Paging System, Point Of Sale Australia, POS Solution")]
[assembly: AssemblyConfiguration("Tran Minh Tien, Tran Thu Khoa, Bui Thi Kieu Oanh, Phan Chi Thanh")]
[assembly: AssemblyCompany("BECAS Technology Australia")]
[assembly: AssemblyProduct("becasPOS")]
[assembly: AssemblyCopyright("Copyright © BECAS Technology Australia 2014")]
[assembly: AssemblyTrademark("Tran Minh Tien, Tran Thu Khoa, Bui Thi Kieu Oanh, Phan Chi Thanh, Nguyen Thanh Long")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("27362bca-f052-461f-991a-aa465b3886b1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.14.08.25")]
[assembly: AssemblyFileVersion("1.14.08.25")]
