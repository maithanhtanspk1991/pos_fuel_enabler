﻿using System;

namespace SystemLog
{
    public class LogPOS
    {
        public static void WriteLog(string data)
        {
            DateTime dt = DateTime.Now;
            System.IO.StreamWriter lStreamWriter = null;
            try
            {
                string directoryPath = "StoredFilesLog";
                if (!System.IO.Directory.Exists(directoryPath))
                    System.IO.Directory.CreateDirectory(directoryPath);
                string fileName = System.Windows.Forms.Application.StartupPath + "\\StoredFilesLog\\LogPOS_" + dt.Day.ToString("00") + "_" + dt.Month.ToString("00") + "_" + dt.Year + ".log";
                if (!System.IO.File.Exists(fileName))
                {
                    lStreamWriter = new System.IO.StreamWriter(fileName);
                }
                else
                {
                    lStreamWriter = System.IO.File.AppendText(fileName);
                }
                lStreamWriter.WriteLine(dt.ToString() + ":::" + data);
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    lStreamWriter.Close();
                }
                catch (Exception)
                {
                }
            }
        }

        static int n = 0;
        public static void ShowLogTime()
        {
            ShowLogTime("");
        }
        public static void ShowLogTime(string data)
        {
            DateTime dt = DateTime.Now;
            System.IO.StreamWriter lStreamWriter = null;
            try
            {
                string directoryPath = "StoredFilesLog";
                if (!System.IO.Directory.Exists(directoryPath))
                    System.IO.Directory.CreateDirectory(directoryPath);
                string fileName = System.Windows.Forms.Application.StartupPath + "\\StoredFilesLog\\LogPOS_" + dt.Day.ToString("00") + "_" + dt.Month.ToString("00") + "_" + dt.Year + ".log";
                if (!System.IO.File.Exists(fileName))
                {
                    lStreamWriter = new System.IO.StreamWriter(fileName);
                }
                else
                {
                    lStreamWriter = System.IO.File.AppendText(fileName);
                }
                lStreamWriter.WriteLine(data + ":::Index " + n + ": " + dt.ToString("HH:mm:ss:fff"));
                n++;
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    lStreamWriter.Close();
                }
                catch (Exception)
                {
                }
            }
        }
    }
}