using System;

namespace SystemConfig
{
    public class ReadDBConfig
    {
        private clsReadAndWriteINI ini;

        public string Server { get; set; }

        public string DatabaseName { get; set; }

        public ReadDBConfig()
        {
            try
            {
                //cai nay cho database
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                ini = new clsReadAndWriteINI(sPath);
                Server = ini.ReadValue("dbconfig", "Server");
                DatabaseName = ini.ReadValue("dbconfig", "Database");
                if (Server == "")
                {
                    ini.WriteValue("dbconfig", "Server", "localhost");
                }
                if (DatabaseName == "")
                {
                    ini.WriteValue("dbconfig", "Database", "paging");
                }
            }
            catch (Exception)
            {
            }
        }
    }
}