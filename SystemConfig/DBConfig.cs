using System;

namespace SystemConfig
{
    public class DBConfig
    {
        private clsReadAndWriteINI ini;

        public string Server { get; set; }

        public string Host { get; set; }

        public string DatabaseName { get; set; }

        public string User { get; set; }

        public string Pass { get; set; }

        public int CableID { get; set; }

        public string CurentHost { get; set; }

        public bool IsServer { get; set; }

        public DBConfig()
        {
            User = "root";
            Pass = "becastek";
            string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            sPath += "\\config.ini";
            ini = new clsReadAndWriteINI(sPath);
            ReadConfig();
            if (IsServer)
                CurentHost = Host;
            else
                CurentHost = Server;
        }

        private void ReadConfig()
        {
            try
            {
                Host = ini.ReadValue("dbconfig", "Host");
                if (Host == "")
                {
                    ini.WriteValue("dbconfig", "Host", "localhost");
                }
                Server = ini.ReadValue("dbconfig", "Server");
                if (Server == "")
                {
                    ini.WriteValue("dbconfig", "Server", "localhost");
                }
                DatabaseName = ini.ReadValue("dbconfig", "Database");
                if (DatabaseName == "")
                {
                    ini.WriteValue("dbconfig", "Database", "paging");
                }

                string strCableID = ini.ReadValue("dbconfig", "CableID");
                CableID = Convert.ToInt32(strCableID == "" ? "1" : strCableID);
                if (CableID == 1)
                    IsServer = true;
                else
                    IsServer = false;
            }
            catch (Exception)
            {
            }
        }

        public DBConfig Copy()
        {
            DBConfig db = new DBConfig();
            db.Server = this.Server;
            db.Host = this.Host;
            db.DatabaseName = this.DatabaseName;
            db.CurentHost = this.CurentHost;
            db.User = this.User;
            db.Pass = this.Pass;
            return db;
        }

        public void ChangeDatabase(DBConfig dbconfig)
        {
            try
            {
                ini.WriteValue("dbconfig", "Host", dbconfig.Host);
                ini.WriteValue("dbconfig", "Server", dbconfig.Server);
                ini.WriteValue("dbconfig", "Database", dbconfig.DatabaseName);
            }
            catch (Exception)
            {
            }
        }
    }
}