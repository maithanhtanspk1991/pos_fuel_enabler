﻿using System;
using System.Data;

namespace FuelAuto
{
    public class Fuel
    {
        public ClientMessage mClientMessage;
        private string _nameClass = "FuelAutoProtocol::Fuel::";
        private DataTable dtSourceKindOfFuel;
        private FuelAutoProtocol mFuelAutoProtocol;
        private MoneyFortmat money;

        private Connection.ReadDBConfig mReadDBConfig = new Connection.ReadDBConfig();

        private string strCashAmount = string.Empty;

        private string strFromPump = string.Empty;

        private string strGradeName = string.Empty;

        private string strGradeNo = string.Empty;

        private string strGST = string.Empty;

        private string strIDPumpHistory = string.Empty;

        private string strIDPumpTempHistory = string.Empty;

        private string strQuery = string.Empty;

        private string strUnitPrice = string.Empty;

        private string strVolumeAmount = string.Empty;

        public Fuel()
        {
            money = new MoneyFortmat(MoneyFortmat.AU_TYPE);
            LoadKindOfFuel();
            InitFuelAutoProtocol();
            InitClientMessage();
        }
        public char CheckSum(string strReceiveData)
        {
            Int32 LRC = 0;
            int intLth = strReceiveData.Length;
            for (int i = 0; i < intLth; i++)
            {
                Int32 dec = Convert.ToInt32(DecimalFromChar((Char)strReceiveData[i]));
                LRC = LRC ^ dec;
            }
            return DecimalToChar(Convert.ToDecimal(LRC));
        }

        public string ConvertToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public Decimal DecimalFromChar(char argument)
        {
            Decimal decValue;
            decValue = argument;
            return decValue;
        }

        private Char DecimalToChar(decimal argument)
        {
            Char CharValue;
            CharValue = (Char)argument;
            return CharValue;
        }

        private void ExcuteDateReceive(string strDataReceive)
        {
            string strPumpNo = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "ExcuteDateReceive::DataReceive=" + strDataReceive);
                conn.Open();
                conn.BeginTransaction();               
                strPumpNo = Convert.ToInt32(strDataReceive.Substring(1, 2)).ToString();
                strGradeNo = Convert.ToInt32(strDataReceive.Substring(3, 1)).ToString();
                strCashAmount = (Convert.ToDouble(strDataReceive.Substring(4, 5)) * 10).ToString();
                strVolumeAmount = (Convert.ToDouble(strDataReceive.Substring(9, 5)) * 10).ToString();
                strUnitPrice = (Convert.ToDouble(strDataReceive.Substring(14, 4))).ToString();
                DataRow[] foundRows = dtSourceKindOfFuel.Select("KindID = " + strGradeNo);
                strGradeNo = foundRows[0]["MenuItemID"].ToString();
                if (Convert.ToDouble(strDataReceive.Substring(14, 4)) != Convert.ToDouble(foundRows[0]["unitPrice"].ToString()))
                {
                    strQuery = "UPDATE itemsmenu SET unitPrice = '" + Convert.ToDouble(strDataReceive.Substring(14, 4)) + "' WHERE itemID = " + strGradeNo;
                    conn.ExecuteNonQuery(strQuery);
                }
                strQuery = "INSERT INTO fuelhistory(KindOfFuel,PumID,CashAmount,VolumeAmount,UnitPrice,FromPump) VALUES(" + strGradeNo + "," + strPumpNo + "," + strCashAmount + "," + Convert.ToInt32(money.getFortMat(Convert.ToDouble(strCashAmount) / Convert.ToDouble(strUnitPrice))) + "," + strUnitPrice + ",1)";
                conn.ExecuteNonQuery(strQuery);

                SystemLog.LogPOS.WriteLog(_nameClass + "ExcuteDateReceive::AddEventMessage");
                mClientMessage.AddEventMessage();
                conn.Commit();
            }
            catch (Exception ex)
            {
                conn.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "ExcuteDateReceive::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void InitClientMessage()
        {
            SystemLog.LogPOS.WriteLog(_nameClass + "InitClientMessage");
            mClientMessage = new ClientMessage(mReadDBConfig.Server, 12342, mReadDBConfig.CableID);
        }

        private void InitFuelAutoProtocol()
        {
            try
            {
                mFuelAutoProtocol = new FuelAutoProtocol();
                mFuelAutoProtocol.Received += new FuelAutoProtocol.FuelAutoEvenHandler(mFuelAutoProtocol_Received);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "InitFuelAutoProtocol::" + ex.Message);
            }
        }

        private void LoadKindOfFuel()
        {
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string strQuery = "SELECT f.KindID , f.MenuItemID , i.unitPrice,f.PumpHeadID FROM fuel f inner join itemsmenu i on f.MenuItemID = i.itemID";
                dtSourceKindOfFuel = new DataTable();
                dtSourceKindOfFuel = conn.Select(strQuery);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "LoadKindOfFuel::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void mFuelAutoProtocol_Received(FuelAutoProtocol.FuelData data)
        {
            char chekSum = CheckSum(data.Data + data.End);
            SystemLog.LogPOS.WriteLog(_nameClass + "Checksum::" + chekSum + "::" + CheckSum(data.Data) + "::" + data.CheckSum);
            if (chekSum == data.CheckSum)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "mFuelAutoProtocol_Received::SerialPort_T24_" + data);
                ExcuteDateReceive(data.Start + data.Data + data.End + data.CheckSum);                
            }
        }
    }
}