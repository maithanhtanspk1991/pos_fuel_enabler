﻿using System;

namespace FuelAuto
{
    public class ClientMessage
    {
        private int mCableID = 0;
        private int mEventMessage = 0;
        private SocketServerClient.SocketClient mSocketClient;

        private System.Threading.Thread mThread;

        public ClientMessage(string ip, int port, string cableID)
        {
            this.EnableClientServer = Config.GetEnableClientServer();
            if (this.EnableClientServer)
            {
                try
                {
                    mCableID = Convert.ToInt32(cableID);
                }
                catch (Exception)
                {
                }

                mSocketClient = new SocketServerClient.SocketClient(port, ip);
                mThread = new System.Threading.Thread(MesageChanging);
                mThread.Start();
            }
        }

        public delegate void ClientMessageEvenHandler(object obj, EventArgs e);

        public event ClientMessageEvenHandler MesageChange;

        public bool EnableClientServer { get; set; }

        public void AddEventMessage()
        {
            if (this.EnableClientServer)
            {
                lock ("AddEventMessage")
                {
                    mEventMessage = 0;
                    SendMessageEvent();
                }
            }
        }

        private void MesageChanging()
        {
            while (true)
            {
                if (SendMessageEvent())
                {
                    OnMesageChange(null);
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        private void OnMesageChange(EventArgs e)
        {
            if (MesageChange != null)
            {
                MesageChange(this, e);
            }
        }

        private bool SendMessageEvent()
        {
            try
            {
                string data = mCableID + "," + mEventMessage;
                string resuilt = "";
                try
                {
                    resuilt = mSocketClient.SendData(data, 1500);
                }
                catch (Exception ex)
                {
                    SystemLog.LogPOS.WriteLog("SendMessageEvent:::" + ex.Message);
                }
                if (resuilt != "")
                {
                    string[] s = resuilt.Split(',');
                    int cable = Convert.ToInt32(s[0]);
                    mEventMessage = Convert.ToInt32(s[1]);
                    if (cable > 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("SendMessageEvent:::" + ex.Message);
            }
            return false;
        }
    }
}