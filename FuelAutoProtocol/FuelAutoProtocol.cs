﻿using System;

namespace FuelAuto
{
    public class FuelAutoProtocol
    {
        public int ScaleType { get; set; }

        private System.IO.Ports.SerialPort mSerialPort;
        //private char mEnd = (char)13;
        private int ack = 06;
        private string mLastStringPum = "";
        private int mLastCheckSum = -1;

        //1:item 2 sub
        public int ItemType { get; set; }

        //private string mResuilt = "";
        public delegate void FuelAutoEvenHandler(FuelData data);

        public event FuelAutoEvenHandler Received;

        //private List<ScaleEvenHandler> mListEvent;

        #region Duc

        // count send
        private int mCount = 0;

        private int mMaxCount = 20;
        private System.Timers.Timer mTimer;

        #endregion Duc

        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);

        public FuelAutoProtocol()
        {
            PortCom PortCom = PortCom.Get(PortComType.Fuel);
            //mListEvent = new List<ScaleEvenHandler>();
            mSerialPort = new System.IO.Ports.SerialPort(PortCom.PortName, PortCom.BaudRate, PortCom.Parity, PortCom.DataBits, PortCom.StopBits);
            mSerialPort.RtsEnable = true;
            SystemLog.LogPOS.WriteLog(PortCom.PortName + "-" + PortCom.BaudRate + "-" + PortCom.Parity.ToString() + "-" + PortCom.DataBits + "-" + PortCom.StopBits);
            mSerialPort.Open();
            SystemLog.LogPOS.WriteLog("OPen::" + mSerialPort.IsOpen + "");
            System.Threading.Thread thread = new System.Threading.Thread(ReadPort);
            thread.Start();
            mTimer = new System.Timers.Timer(1000);
            mTimer.Elapsed += new System.Timers.ElapsedEventHandler(SetTextTimeOut);
            mTimer.Start();
        }

        public void Close()
        {
            mSerialPort.Close();
            mSerialPort.Dispose();
        }

        private int ReadChar()
        {
            char[] buff = new char[1];
            mSerialPort.Read(buff, 0, 1);
            SystemLog.LogPOS.WriteLog(buff[0] + ":::" + (int)buff[0]);
            if (buff.Length == 0)
            {
                return -1;
            }
            else
            {
                return buff[0];
            }
        }

        private void ReadPort()
        {
            SystemLog.LogPOS.WriteLog("READ::DATA");
            string resuilt = "";
            int start = 02;
            int end = 03;
            bool key = false;
            while (true)
            {
                int data = mSerialPort.ReadChar();
                SystemLog.LogPOS.WriteLog("Read Fuel:::" + data + "::" + (char)data);
                //int data = ReadChar();
                if (key)
                {
                    //LogPOS.WriteLog("ReadPort Fuel Auto:::data:" + resuilt + "--checkSum::" + (char)data);
                    if (mLastStringPum != resuilt)
                    {
                        //LogPOS.WriteLog("ReadPort Checked Fuel Auto:::data:" + resuilt + "--checkSum::" + (char)data);
                        OnReceived(new FuelData(resuilt, (char)data, (char)start, (char)end));
                        mLastStringPum = resuilt;
                        mLastCheckSum = data;
                    }
                    mSerialPort.Write((char)ack + "");
                    mCount = 0;
                    key = false;
                }
                else if (data == start)
                {
                    resuilt = "";
                    key = false;
                }
                else if (data == end)
                {
                    try
                    {
                        key = true;
                    }
                    catch (Exception ex)
                    {
                        SystemLog.LogPOS.WriteLog("ReadPort Scales:::" + ex.Message);
                    }
                }
                else if (data > 0)
                {
                    resuilt += (char)data;
                }
            }
        }

        public void SendAck()
        {
            try
            {
                mSerialPort.Write((char)ack + "");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetTextTimeOut(object source, System.Timers.ElapsedEventArgs e)
        {
            if (mLastStringPum != "" && mCount == mMaxCount)
            {
                mLastStringPum = "";
                mLastCheckSum = -1;
            }

            if (mCount <= mMaxCount)
            {
                mCount++;
                //Console.WriteLine(mCount);
            }
        }

        private void OnReceived(FuelData data)
        {
            if (Received != null)
            {
                Received(data);
            }
        }

        public void SetText(string text, System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        public Int32 CheckSum(string strReceiveData)
        {
            Int32 LRC = 0;
            int intLth = strReceiveData.Length;
            for (int i = 1; i < intLth - 1; i++)
            {
                Int32 dec = Convert.ToInt32(DecimalFromChar((Char)strReceiveData[i]));
                LRC = LRC ^ dec;
            }
            return DecimalToChar(Convert.ToDecimal(LRC));
        }

        public Decimal DecimalFromChar(char argument)
        {
            Decimal decValue;
            decValue = argument;
            return decValue;
        }

        private Char DecimalToChar(decimal argument)
        {
            Char CharValue;
            CharValue = (Char)argument;
            return CharValue;
        }

        public class FuelData
        {
            public string Data { get; set; }

            public char CheckSum { get; set; }

            public char Start { get; set; }

            public char End { get; set; }

            public FuelData(string data, char checkSum, char start, char end)
            {
                Data = data;
                CheckSum = checkSum;
                Start = start;
                End = end;
            }
        }
    }
}