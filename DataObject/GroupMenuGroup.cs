﻿using System.Collections.Generic;

namespace DataObject
{
    public class GroupMenuGroup
    {
        public List<DataObject.GroupMenu> Group;

        public GroupMenuGroup()
        {
            Group = new List<GroupMenu>();
        }
    }
}