﻿namespace DataObject
{
    public enum ListItemType
    {
        Items,
        ItemsLine,
        Group,
        Option,
        DynItems
    }

    public enum VisualType
    {
        None = -1,
        Display = 1,
        Hiden = 0
    }

    public enum IsFuelType
    {
        None = -1,
        Yes = 1,
        No = 0
    }

    public enum DeleteType
    {
        None = -1,
        Deleted = 1,
        NoDelete = 0
    }

    public enum Query
    {
        None,
        Delete,
        Selete,
        Insert,
        Update,
        Backup
    }

    public enum CompletedType
    {
        None = -1,
        SubTotal = 1,
        NotSub = 0
    }

    public enum SellType
    {
        Each = 1,
        Mls,
        Kg,
        Litre// Each ban theo cai,MLS la ban theo lit la ban theo kg,
    }

    public enum TypeDailyAll
    {
        Daily,
        All
    }

    public enum TypePayment
    {
        Debit = 1,
        Credit,
        Manager
    }

    public enum TypeSubtoal
    {
        Subtotal,
        Credit
    }

    public enum PrinterType
    {
        PrintAll = 0,
        Kitchen,
        Bar
    }

    public enum FindItemType
    {
        FindItemInOrder = 0,
        FindItemInStock
    }

    public enum TypeMenu
    {
        NONE = -1,
        ALLMENU = 2,
        TYREMENU = 3,
        MECHANICMENU = 4,
        MAGMENU = 5
    }

    public enum CompletedShift
    {
        NONE = 0,
        COMPLETED
    }

    public enum TypeFromShift
    {
        Staff, Manager
    }

    /// <summary>
    /// Cashin =2,SafeDrop=3,CashOut=5,PayOut=6,SafeDropAuto=10
    /// </summary>
    public enum TypeCashInAndCashOut
    {
        Cashin = 2, SafeDrop = 3, CashOut = 5, PayOut = 6, SafeDropAuto = 10
    }
}