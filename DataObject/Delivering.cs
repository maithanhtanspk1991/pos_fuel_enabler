﻿using System;
using System.Collections.Generic;

namespace DataObject
{
    public class Delivering
    {
        public static string _TableName = "delivering";

        public Delivering()
        {
            Date = DateTime.Now;
            DeliveringLine = new List<DeliveringLine>();
        }

        public int CoutItem { get; set; }

        public DateTime Date { get; set; }

        public int Id { get; set; }

        public int OrderID { get; set; }

        public double SubTotal { get; set; }

        public int Type { get; set; }

        public List<DeliveringLine> DeliveringLine { get; set; }

        public Delivering Copy()
        {
            Delivering item = new Delivering();
            item.Id = this.Id;
            item.Date = this.Date;
            item.Type = this.Type;
            item.CoutItem = this.CoutItem;
            item.SubTotal = this.SubTotal;
            item.OrderID = this.OrderID;
            foreach (DeliveringLine line in DeliveringLine)
            {
                item.DeliveringLine.Add(line.Copy());
            }
            return item;
        }
    }
}