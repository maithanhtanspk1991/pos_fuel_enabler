﻿namespace DataObject
{
    internal class StockLevel
    {
        public int ItemID { get; set; }

        public int QtyOnHand { get; set; }

        public int QtyReceived { get; set; }

        public int QtyAdjust { get; set; }

        public int QtyRefund { get; set; }

        public int QtyAdd { get; set; }
    }
}