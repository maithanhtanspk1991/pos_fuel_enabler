﻿using System;

namespace DataObject
{
    public class CashInAndCashOut : Object
    {
        public int ID { get; set; }

        public DateTime datetime { get; set; }

        public double TotalAmount { get; set; }

        public int Employee { get; set; }

        public TypeCashInAndCashOut CashType { get; set; }

        public string Description { get; set; }

        public int ShiftID { get; set; }
    }
}