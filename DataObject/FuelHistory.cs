﻿namespace DataObject
{
    public class FuelHistory
    {
        public FuelHistory()
        {
            CashAmount = 0;
            FromPump = 0;
            Gst = 0;
            ID = 0;
            IsComplete = 0;
            KindOfFuel = 0;
            PumID = 0;
            UnitPrice = 0;
            VolumeAmount = 0;
            CableID = 0;
        }
        public FuelHistory(int id, int amount, int frompump, int gst, int complete, int kindofFuel, int pumpID, int price, int volume, int cable)
        {
            ID = id;
            CashAmount = amount;
            FromPump = frompump;
            Gst = gst;
            IsComplete = complete;
            KindOfFuel = kindofFuel;
            PumID = pumpID;
            UnitPrice = price;
            VolumeAmount = volume;
            CableID = cable;
        }
        public FuelHistory(int amount, int frompump, int complete, int kindofFuel, int pumpID, int price, int volume, int cable)
        {
            ID = 0;
            CashAmount = amount;
            FromPump = frompump;
            Gst = 0;
            IsComplete = complete;
            KindOfFuel = kindofFuel;
            PumID = pumpID;
            UnitPrice = price;
            VolumeAmount = volume;
            CableID = cable;
        }
        public static string mTableName = "fuelhistory";

        public int CashAmount { get; set; }

        public int FromPump { get; set; }

        public int Gst { get; set; }

        public int ID { get; set; }

        public int IsComplete { get; set; }

        public int KindOfFuel { get; set; }

        public int PumID { get; set; }

        public int UnitPrice { get; set; }

        public int VolumeAmount { get; set; }

        public int CableID { get; set; }

    }
}