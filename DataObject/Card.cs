﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Card
    {
        public int CardID { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string Description { get; set; }
        public int TypeCard { get; set; }
        public bool IsActive { get; set; }
        public bool IsSurchart { get; set; }
        public double Surchart { get; set; }
        public bool IsCashOut { get; set; }

        public Card()
        {
            Description = "";
            CardName = "";
            CardCode = "";
            IsActive = true;
            IsCashOut = false;
        }

        public Card Copy()
        {
            return new Card()
            {
                CardID = this.CardID,
                CardCode = this.CardCode,
                CardName = this.CardName,
                Description = this.Description,
                TypeCard = this.TypeCard,
                IsActive = this.IsActive,
                IsSurchart = this.IsSurchart,
                Surchart = this.Surchart,
                IsCashOut = this.IsCashOut
            };
        }
    }
}
