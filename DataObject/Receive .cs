﻿using System;
using System.Collections.Generic;

namespace DataObject
{
    public class Receive
    {
        public static string _TableName = "stockreceive";
        public static string _TableNameShort = "sr";

        public int ID { get; set; }

        public int StaffID { get; set; }

        public string StaffName { get; set; }

        public int SupplierID { get; set; }

        public DateTime Date { get; set; }

        public double SubTotal { get; set; }

        public int CableID { get; set; }

        public string TicketID { get; set; }

        public DataObject.Suppliers Suppliers { get; set; }

        public List<DataObject.ReceiveLine> ReceiveLine { get; set; }

        public Receive()
        {
            ReceiveLine = new List<ReceiveLine>();
            Suppliers = new Suppliers();
            StaffName = "Manager";
        }

        public Receive Copy()
        {
            Receive item = new Receive();
            item.ID = this.ID;
            item.StaffID = this.StaffID;
            item.SupplierID = this.SupplierID;
            item.Date = this.Date;
            item.SubTotal = this.SubTotal;
            item.CableID = this.CableID;
            item.TicketID = this.TicketID;
            foreach (ReceiveLine r in ReceiveLine)
            {
                item.ReceiveLine.Add(r.Copy());
            }
            return item;
        }

        public override bool Equals(object obj)
        {
            Receive item = (Receive)obj;
            return item.ID == this.ID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}