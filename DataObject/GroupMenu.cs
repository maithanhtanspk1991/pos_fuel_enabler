﻿using System.Collections.Generic;

namespace DataObject
{
    public class GroupMenu
    {
        public static string _TableName = "groupsmenu";

        public int GroupID { get; set; }

        public string GroupShort { get; set; }

        public string GroupDesc { get; set; }

        public int GrTypeID { get; set; }

        public int GrShortCut { get; set; }

        public int GrOption { get; set; }

        public double DisplayOrder { get; set; }

        public int ShowDisplay { get; set; }

        public bool Visual { get; set; }

        public bool Deleted { get; set; }

        public string Bitmap { get; set; }

        private List<ItemsMenu> _ItemMenu { get; set; }

        public int Color { get; set; }

        public GroupMenu()
        {
            _ItemMenu = new List<ItemsMenu>();
            Visual = true;
        }

        public GroupMenu Copy()
        {
            GroupMenu gm = new GroupMenu();
            gm.GroupID = GroupID;
            gm.GroupShort = GroupShort;
            gm.GroupDesc = GroupDesc;
            gm.GrTypeID = GrTypeID;
            gm.GrShortCut = GrShortCut;
            gm.GrOption = GrOption;
            gm.DisplayOrder = DisplayOrder;
            gm.ShowDisplay = ShowDisplay;
            gm.Visual = Visual;
            gm.Deleted = Deleted;
            gm.Bitmap = Bitmap;
            return gm;
        }

        public override bool Equals(object obj)
        {
            GroupMenu item = new GroupMenu();
            try
            {
                item = (GroupMenu)obj;
            }
            catch { }

            return item.GroupID == this.GroupID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return this.GroupID.ToString();
        }
    }
}