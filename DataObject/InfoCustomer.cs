﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class InfoCustomer
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ABN { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Thank { get; set; }
        public string Suport { get; set; }

        public InfoCustomer()
        {
            Name = "Bizzell's Garage";
            Address = "93 Latro Terace, Paddington, QLD, 4064";
            ABN = "98812 905 384";
            Phone = "07 33695988";
            Fax = "0438 775324";
            Email = "mail@bizzellsgarage.com";
            Website = "www.bizzellsgarage.com";
            Thank = "THANK YOU FOR YOUR MOTOR VEHICLE BUSINESS";
            Suport = "IF YOU HAVE ANY ENQUIRIES PLEASE FEEL FREE TO CALL IN OR PHONE 07 3369 5988";
        }
    }
}
