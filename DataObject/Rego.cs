﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Rego
    {
        public int ID { get; set; }
        public int CustID { get; set; }
        public string CarNumber { get; set; }
        public string CarColor { get; set; }
        public string CarProduct { get; set; }
        public bool Deleted { get; set; }
        public Rego()
        {
            CarNumber = "";
            CarColor = "";
            CarProduct = "";
        }

        public Rego Copy()
        {
            Rego item = new Rego();
            item.ID = ID;
            item.CustID = CustID;
            item.CarNumber = CarNumber;
            item.CarColor = CarColor;
            item.CarProduct = CarProduct;
            item.Deleted = Deleted;
            return item;
        }
    }
}
