﻿using System;

namespace DataObject
{
    public class MonneyFormat
    {
        /// <summary>
        /// Chuyen doi tien te tu string sang double dang 0.00
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double GetDouble2(string value)
        {
            double resuilt = 0;
            try
            {
                resuilt = Convert.ToDouble(value);
                resuilt = Math.Round(resuilt, 2);
            }
            catch (Exception)
            {
            }
            return resuilt;
        }

        /// <summary>
        /// Chuyen doi tien te tu string sang double dang 0.000
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double GetDouble3(string value)
        {
            double resuilt = 0;
            try
            {
                resuilt = Convert.ToDouble(value);
                resuilt = Math.Round(resuilt, 3);
            }
            catch (Exception)
            {
            }
            return resuilt;
        }

        /// <summary>
        /// Chuyển sang kiểu tiền có 2 số 0 o sau dấu chấm
        /// </summary>
        /// <param name="value">Giá truyền vào là double</param>
        /// <returns></returns>
        public static string Format2(double value)
        {
            return String.Format("{0:#,##0.00}", value);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="per">1:10 - 2:100 - 3:100</param>
        /// <returns></returns>
        public static string Format2(double value, int per)
        {
            string result = String.Format("{0:#,##0.00}", 0);

            if (value != 0)
            {
                result = String.Format("{0:#,##0.00}", value / Math.Pow(10, per));
            }
            return result;
        }
        /// <summary>
        /// AU: 1000 -> 1.000
        /// VN: 1000 -> 1000.000
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Format(double value)
        {            
            return String.Format("{0:0.00}", value / 1000);
        }


        public static string Format0(double value)
        {
            return String.Format("{0}", value);
        }
        /// <summary>
        /// Chuyển sang kiểu tiền có 3 số 0 o sau dấu chấm
        /// </summary>
        /// <param name="value">Giá truyền vào</param>
        /// <returns></returns>
        public static string Format3(double value)
        {
            return String.Format("{0:#,##0.000}", value);
        }

        /// <summary>
        /// Chuyển kiểu số về kiểu có dấu ngăn chia ngàn, triệu, tỉ
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatNumber(double value)
        {
            return String.Format("{0:#,##0}", value);
        }

        /// <summary>
        /// Chuyển kiểu số về kiểu có dấu ngăn chia ngàn, triệu, tỉ
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatNumber(int value)
        {
            return String.Format("{0:#,##0}", value);
        }

        /// <summary>
        /// Làm tròn số
        /// </summary>
        /// <param name="value">so tien can lam tron</param>
        /// <param name="index">so ki tu thap phan</param>
        /// <returns></returns>
        public static RoundObject Round(double value, int index)
        {
            RoundObject round = new RoundObject();

            int money = (int)(value * Math.Pow(10, index));
            int a = money % 10;
            int b = 0;
            if (a < 3)
            {
                a = 0;
                b = 0;
            }
            else if (a >= 3 && a <= 7)
            {
                a = 5;
                b = 0;
            }
            else
            {
                a = 0;
                b = 10;
            }
            money = (money / 10) * 10 + a + b;
            round.Value = (double)money / Math.Pow(10, index);
            round.Round = round.Value - value;
            round.Value = Math.Round(round.Value, index);
            round.Round = Math.Round(round.Round, index);
            return round;
        }
        public String FormatCurenMoney(double value)
        {
            return String.Format("{0:0.00}", value);
        }

        /// <summary>
        /// 1000 -> 1000.00
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public String FormatCurenMoney2(double value)
        {
            value = Math.Round(value, 2);
            return String.Format("{0:0.00}", value);
        }

    }
}