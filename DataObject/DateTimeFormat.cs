﻿using System;

namespace DataObject
{
    public class DateTimeFormat
    {
        public static string DisplayDate(DateTime dt)
        {
            return dt.Day.ToString("00") + "/" + dt.Month.ToString("00") + "/" + dt.Year;
        }

        public static string DisplayDateTime(DateTime dt)
        {
            return DisplayDate(dt) + " " + DisplayTime(dt);
        }

        public static string DisplayTime(DateTime dt)
        {
            return dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") + ":" + dt.Second.ToString("00");
        }

        public static string GetMysqlDate(DateTime dt)
        {
            return dt.Year + "-" + dt.Month.ToString("00") + "-" + dt.Day.ToString("00");
        }

        public static string GetMysqlDateTime(DateTime dt)
        {
            return GetMysqlDate(dt) + " " + GetMysqlTime(dt);
        }

        public static string GetMysqlTime(DateTime dt)
        {
            return dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") + ":" + dt.Second.ToString("00");
        }
    }
}