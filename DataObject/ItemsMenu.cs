﻿namespace DataObject
{
    public class ItemsMenu
    {
        public static string _TableName = "itemsmenu";
        public static string _TableNameDyn = "dynitemsmenu";

        public int LineID { get; set; }

        public int ItemID { get; set; }

        public int ItemType { get; set; }

        public string Barcode { get; set; }

        public string ItemShort { get; set; }

        public string ItemDesc { get; set; }

        public double UnitPrice { get; set; }

        public int GroupID { get; set; }

        public int BulkID { get; set; }

        public int SellSize { get; set; }

        public double DisplayOrder { get; set; }

        public int ShowDisplay { get; set; }

        public int SellType { get; set; }

        public int PrinterType { get; set; }

        public bool Visual { get; set; }

        public string Bitmap { get; set; }

        public double Gst { get; set; }

        public bool Deleted { get; set; }

        public int Weight { get; set; }

        public int CableID { get; set; }

        public bool EnableFuelDiscount { get; set; }

        public bool IsFuel { get; set; }

        public int TypeMenu { get; set; }

        public int SizeItem { get; set; }

        public ItemsMenu()
        {
            this.SellSize = 1;
            DisplayOrder = 1;
            Visual = true;
            SellType = 1;
            IsFuel = false;
            TypeMenu = 1;
            SizeItem = 0;
        }

        public ItemsMenu Copy()
        {
            ItemsMenu item = new ItemsMenu();
            item.LineID = LineID;
            item.ItemID = ItemID;
            item.Barcode = Barcode;
            item.ItemShort = ItemShort;
            item.ItemDesc = ItemDesc;
            item.UnitPrice = UnitPrice;
            item.GroupID = GroupID;
            item.BulkID = BulkID;
            item.SellSize = SellSize;
            item.DisplayOrder = DisplayOrder;
            item.ShowDisplay = ShowDisplay;
            item.SellType = SellType;
            item.PrinterType = PrinterType;
            item.Visual = Visual;
            item.Bitmap = Bitmap;
            item.Gst = Gst;
            item.ItemType = this.ItemType;
            item.Deleted = Deleted;
            item.CableID = this.CableID;
            item.EnableFuelDiscount = EnableFuelDiscount;
            item.IsFuel = IsFuel;
            item.TypeMenu = TypeMenu;
            item.SizeItem = SizeItem;
            return item;
        }

        public override bool Equals(object obj)
        {
            ItemsMenu im = (ItemsMenu)obj;
            return im.ItemID == this.ItemID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}