﻿namespace DataObject
{
    public class Reason
    {
        public int ID { get; set; }

        public string strReason { get; set; }

        public int Negative { get; set; }
    }
}