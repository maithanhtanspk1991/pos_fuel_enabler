﻿using System;

namespace DataObject
{
    public class DynItemsMenu
    {
        public static string _TableName = "dynitemsmenu";

        public int DynID { get; set; }

        public string ItemShort { get; set; }

        public string ItemDesc { get; set; }

        public double UnitPrice { get; set; }

        public int PrinterType { get; set; }

        public DateTime Date { get; set; }

        public int GST { get; set; }

        public int CableID { get; set; }

        public DynItemsMenu()
        {
        }

        public override bool Equals(object obj)
        {
            DynItemsMenu d = (DynItemsMenu)obj;
            return d.DynID == this.DynID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}