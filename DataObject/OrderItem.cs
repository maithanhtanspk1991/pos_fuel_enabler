﻿namespace DataObject
{
    public class OrderItem
    {
        public static string _TableNameAll = "ordersallline";
        public static string _TableNameDailLy = "ordersdailyline";
        public static string _TableNameLoyalty = "loyaltyhistory";
        public static string _TableNameSave = "orderssaveline";
        public static int ITEM_TYPE_DISCOUNT = 3;
        public static int ITEM_TYPE_DYN = 1;
        public static int ITEM_TYPE_ITEM = 0;
        public static int ITEM_TYPE_LOYALTY = 4;
        public static int ITEM_TYPE_OPTION = 2;
        public static int ITEM_TYPE_REFUND = 5;

        public OrderItem()
        {
            DiscountMoney = 0;
            this.SellSize = 1;
            //ItemSellType = new List<ItemSellType>();
            SellTypeName = "";
        }

        public string Bracode { get; set; }

        public int CableID { get; set; }

        //public double UnitPrice { get; set; }
        public double DiscountMoney { get; set; }

        public double DiscountPercent { get; set; }

        public double GetDiscount
        {
            get
            {
                return DiscountMoney * 100 / SubTotal;
            }
        }

        /// <summary>
        /// Cái này có trừ đi refund rồi đó!
        /// </summary>
        public int GetQty { get { return Qty - RefundQty; } }

        public double GST { get; set; }

        public int IDLoyalty { get; set; }

        public bool IsLoyalty { get; set; }

        public int ItemID { get; set; }

        public int Status { get; set; }

        public string ItemName { get; set; }

        //public List<ItemSellType> ItemSellType { get; set; }

        public int ItemType { get; set; }

        public double Loyalty { get; set; }

        public double MoneyGST
        {
            get { return (SubTotal - DiscountMoney - Loyalty) / (100 + GST) * GST; }
        }

        public double MoneyRefund { get; set; }

        public int ObjectID { get; set; }

        public int ParentLine { get; set; }

        public double Price { get; set; }

        public int PrinterType { get; set; }

        public int Qty { get; set; }

        public double QtyAfterRefund { get; set; }

        public int RefundQty { get; set; }

        public int SellSize { get; set; }

        public int SellType { get; set; }

        public string SellTypeName { get; set; }

        public int SellTypeObjectID { get; set; }

        public double SubTotal { get; set; }

        public int Weight { get; set; }

        //public static OrderItem GetOrderItem(ItemsMenu item)
        //{
        //    OrderItem o = new OrderItem();
        //    o.ItemID = item.ItemID;
        //    o.ItemType = item.ItemType;
        //    o.ItemName = item.ItemDesc;
        //    o.Price = item.UnitPrice;
        //    //o.GST = item.GST;
        //    o.Qty = 1;
        //    o.SellType = item.SellType;
        //    o.SellTypeName = item.SellTypeName;
        //    o.SellTypeObjectID = item.SellTypeObjectID;
        //    o.SellSize = item.SellSize;
        //    o.Weight = item.Weight;
        //    o.PrinterType = item.PrinterType;
        //    o.Bracode = item.ScanCode;
        //    o.ItemType = item.ItemType;
        //    if (o.SellType == (int)DataObject.SellType.Kg)
        //    {
        //        o.SubTotal = o.Price / o.SellSize * o.Weight * o.Qty;
        //    }
        //    else
        //    {
        //        o.SubTotal = o.Price * o.Qty;
        //    }

        //    //o.UnitPrice = item.UnitPrice;
        //    return o;
        //}

        public OrderItem Copy()
        {
            OrderItem item = new OrderItem();
            item.ObjectID = this.ObjectID;
            item.ItemID = this.ItemID;
            item.CableID = this.CableID;
            item.Bracode = this.Bracode;
            item.ItemName = this.ItemName;
            item.ItemType = this.ItemType;
            item.Qty = this.Qty;
            item.Price = this.Price;
            item.Weight = this.Weight;
            item.SubTotal = this.SubTotal;
            item.SellType = this.SellType;
            item.SellTypeName = this.SellTypeName;
            item.SellTypeObjectID = this.SellTypeObjectID;
            item.ParentLine = this.ParentLine;
            item.GST = this.GST;
            item.PrinterType = this.PrinterType;
            //o.UnitPrice = this.UnitPrice;
            item.DiscountMoney = this.DiscountMoney;
            item.SellSize = this.SellSize;
            item.RefundQty = this.RefundQty;
            item.MoneyRefund = MoneyRefund;
            item.Loyalty = Loyalty;
            item.Status = Status;
            //foreach (ItemSellType ist in ItemSellType)
            //{
            //    item.ItemSellType.Add(ist.Copy());
            //}
            return item;
        }

        public override bool Equals(object obj)
        {
            OrderItem o = (OrderItem)obj;
            return (this.ObjectID == o.ObjectID);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}