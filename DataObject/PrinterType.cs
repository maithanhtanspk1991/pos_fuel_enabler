﻿namespace DataObject
{
    public class PrinterTypeObject
    {
        public string Value { get; set; }

        public string Display { get; set; }

        public DataObject.PrinterType PrinterType { get; set; }

        public PrinterTypeObject()
        {
        }

        public PrinterTypeObject(string value, string display, DataObject.PrinterType printerType)
        {
            Value = value;
            Display = display;
            PrinterType = printerType;
        }
    }
}