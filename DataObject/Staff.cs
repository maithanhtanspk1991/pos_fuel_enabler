﻿namespace DataObject
{
    public class Staff : Permission
    {
        public Staff()
            : base()
        {
            StaffName = "Manager";
        }

        public Staff(int StaffID, string StaffName, string UserName, string Password, string rtPassWord, int Permission)
            : base(Permission)
        {
            this.StaffID = StaffID;
            this.StaffName = StaffName;
            this.UserName = UserName;
            this.PassWord = PassWord;
            this.rtPassWord = rtPassWord;
        }

        public int StaffID { get; set; }

        public string StaffName { get; set; }

        public string UserName { get; set; }

        public string PassWord { get; set; }

        public string rtPassWord { get; set; }
    }
}