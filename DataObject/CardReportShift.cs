﻿namespace DataObject
{
    public class CardReportShift
    {
        public string Name { get; set; }

        public double Total { get; set; }
    }
}