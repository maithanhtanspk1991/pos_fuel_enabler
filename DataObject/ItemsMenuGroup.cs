﻿using System.Collections.Generic;

namespace DataObject
{
    public class ItemsMenuGroup
    {
        public List<ItemsMenu> Items;

        public ItemsMenuGroup()
        {
            Items = new List<ItemsMenu>();
        }
    }
}