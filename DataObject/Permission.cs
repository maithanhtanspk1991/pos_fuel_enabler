﻿namespace DataObject
{
    public class Permission
    {
        public Permission()
        { }

        public Permission(int IDPermission)
        {
            this.IDPermission = IDPermission;
        }

        public int IDPermission { get; set; }

        public string PermissionName { get; set; }
    }
}