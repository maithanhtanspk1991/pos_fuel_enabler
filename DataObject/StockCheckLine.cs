﻿namespace DataObject
{
    public class StockCheckLine
    {
        public int ID { get; set; }

        public int StockCheckID { get; set; }

        public int itemID { get; set; }

        public int Qty { get; set; }

        public int ReasonID { get; set; }

        public string Note { get; set; }
    }
}