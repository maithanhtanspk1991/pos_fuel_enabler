﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Customers
    {
        public int CustID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Company { get; set; }
        public string CompanyName { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string PostCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string CardNo { get; set; }
        public int Accountlimit { get; set; }
        public int Balance { get; set; }
        public string MemberNo { get; set; }
        public string CarNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsPickup { get; set; }
        public bool IsLock { get; set; }
        public bool Deleted { get; set; }
        public string RegoNumber { get; set; }
        public int RegoID { get; set; }

        public string MemberNoShort
        {
            get
            {
                if (MemberNo.Length > 3)
                    return MemberNo.Substring(0, 3) + "..." + MemberNo.Substring(MemberNo.Length - 3, 3);
                else
                    return MemberNo;
            }
        }

        public List<Rego> RegoItem { get; set; }

        public Customers()
        {
            RegoItem = new List<Rego>();
            LastName = "";
            FirstName = "";
            StreetNo = "";
            StreetName = "";
            PostCode = "";
            Email = "";
            Phone = "";
            Mobile = "";
            CardNo = "";
            MemberNo = "";
            CarNumber = "";
            RegoNumber = "";
            CompanyName = "None company";
            IsActive = true;
        }

        public Customers Copy()
        {
            Customers item = new Customers();
            item.LastName = LastName;
            item.FirstName = FirstName;
            item.StreetNo = StreetNo;
            item.StreetName = StreetName;
            item.PostCode = PostCode;
            item.Email = Email;
            item.Phone = Phone;
            item.Mobile = Mobile;
            item.CardNo = CardNo;
            item.MemberNo = MemberNo;
            item.CarNumber = CarNumber;
            item.IsActive = IsActive;
            item.RegoNumber = RegoNumber;
            item.Balance = Balance;
            item.Accountlimit = Accountlimit;
            item.RegoID = RegoID;
            foreach (Rego sub in RegoItem)
            {
                item.RegoItem.Add(sub.Copy());
            }
            return item;
        }

        public enum CustomersColumn
        {
            CustID = 0,
            Name = 1,
            Company = 2,
            FirstName = 3,
            CompanyName = 4
        }

        public class CustomersFunction
        {
            public static string GetColumn(CustomersColumn item)
            {
                string result = "";
                switch (item)
                {
                    case CustomersColumn.CustID:
                        result = "custID";
                        break;
                    case CustomersColumn.Name:
                        result = "Name";
                        break;
                    case CustomersColumn.Company:
                        result = "Company";
                        break;
                    case CustomersColumn.FirstName:
                        result = "FirstName";
                        break;
                }
                return result;
            }
        }
    }
}

