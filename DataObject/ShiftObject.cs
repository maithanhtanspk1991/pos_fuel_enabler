﻿using System;

namespace DataObject
{
    public class ShiftObject : Object
    {
        public int ShiftID { get; set; }

        public int ShiftName { get; set; }

        public string strShiftName
        {
            get;
            set;
        }

        public string Shift { get; set; }

        public DateTime datetime { get; set; }

        public DataObject.CompletedShift Completed { get; set; }

        public int EndDay { get; set; }

        public int CableID { get; set; }

        public int StaffID { get; set; }

        public bool bActiveShift { get; set; }

        public bool continueShift { get; set; }

        public double CashFloatIn { get; set; }

        public double CashFloatOut { get; set; }

        public ShiftObject copy()
        {
            ShiftObject shift = new ShiftObject();
            shift.ShiftID = this.ShiftID;
            shift.ShiftName = this.ShiftName;
            shift.Shift = this.Shift;
            shift.StaffID = this.StaffID;
            shift.CableID = this.CableID;
            shift.Completed = this.Completed;
            shift.datetime = this.datetime;
            shift.EndDay = this.EndDay;

            shift.bActiveShift = this.bActiveShift;
            shift.continueShift = this.continueShift;
            shift.CashFloatIn = this.CashFloatIn;
            shift.CashFloatOut = this.CashFloatOut;
            return shift;
        }

        public void Reset()
        {
            Completed = CompletedShift.NONE;
            bActiveShift = false;
            ShiftID = 0;
        }

        public override string ToString()
        {
            return ShiftName.ToString();
        }

        public void copyFromAnother(ShiftObject shift)
        {
            this.ShiftID = shift.ShiftID;
            this.ShiftName = shift.ShiftName;
            this.Shift = shift.Shift;
            this.StaffID = shift.StaffID;
            this.CableID = shift.CableID;
            this.Completed = shift.Completed;
            this.datetime = shift.datetime;
            this.EndDay = shift.EndDay;
            //this.bActiveShift = true;
        }
    }
}