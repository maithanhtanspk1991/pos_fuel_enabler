﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Utilities
    {
        public static string ReFormatStringToNumber(string str)
        {
            if (str == null || str == "" || str == "0." || str == ".0" || str == ".")
                str = "0";            
            return str;
        }
    }
}
