﻿namespace DataObject
{
    public class ManageCar
    {
        public int ObjectID { get; set; }

        public string CarNumber { get; set; }

        public string CarColor { get; set; }

        public string CarProduct { get; set; }

        public int CustomerID { get; set; }
    }
}