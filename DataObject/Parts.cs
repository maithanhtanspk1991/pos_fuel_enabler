﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Parts
    {
        public int No { get; set; }
        public string Quantity { get; set; }
        public string Description { get; set; }
        public string UnitPrice { get; set; }
        public string LineTotal { get; set; }
        public Parts()
        {
            Description = "";
            UnitPrice = "";
            LineTotal = "";
        }
    }
}
