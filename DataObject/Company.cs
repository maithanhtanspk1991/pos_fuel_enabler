﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class Company
    {
        
        public int IdCompany { get; set; }
        public string CompanyName { get; set; }
        public int AccountLimit { get; set; }
        public int Debt { get; set; }        
        public string Description { get; set; }
        public bool Active { get; set; }
        public string CompanyCode { get; set; }
        public string Email { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }

        public string CompanyCodeShort
        {
            get
            {
                if (CompanyCode.Length > 3)
                    return CompanyCode.Substring(0, 3) + "..." + CompanyCode.Substring(CompanyCode.Length - 3, 3);
                else
                    return CompanyCode;
            }
        }
        public Company()
        {
            CompanyName = "";
            Description = "";
            CompanyCode = "";
            Email = "";
            StreetName = "";
            StreetNo = "";
            Mobile = "";
            Phone = "";
        }
    }
}
