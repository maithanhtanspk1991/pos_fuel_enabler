﻿using System;

namespace DataObject
{
    public class CreateBarcode
    {
        public static string RandomNumber()
        {
            DateTime tmeNow = DateTime.Now;
            int mls = tmeNow.Millisecond;
            Random rndNumber = new Random(mls);
            int NewNumber1 = rndNumber.Next(100, 999);
            int NewNumber2 = rndNumber.Next(100, 999);
            int NewNumber3 = rndNumber.Next(100, 999);
            int NewNumber4 = rndNumber.Next(100, 999);
            String strItemNumber = NewNumber1.ToString() + NewNumber2.ToString() + NewNumber3.ToString() + NewNumber4.ToString();
            return strItemNumber;
        }

        public static string CreatRandom()
        {
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 12; i++)
            {
                resuilt += rd.Next(10);
            }
            return resuilt;
        }

        public static string CalculateChecksumDigit(string first, int MaxLenght)
        {
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < MaxLenght - first.Length; i++)
            {
                resuilt += rd.Next(10);
            }
            string sTemp = first + resuilt;
            int iSum = 0;
            int iDigit = 0;

            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));

                if (i % 2 == 0)
                {
                    iSum += iDigit * 3;
                }
                else
                {
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
        }
    }
}