﻿namespace DataObject
{
    public class SellTypeObject
    {
        public string Value { get; set; }

        public string Display { get; set; }

        public int SellSize { get; set; }

        //public DataObject.SellType SellType { get; set; }

        public SellTypeObject()
        {
        }

        public SellTypeObject(string value, string display, int sellSize)
        {
            Value = value;
            Display = display;
            SellSize = sellSize;
        }
    }
}