﻿namespace DataObject
{
    public class Suppliers
    {
        public static string _TableName = "suppliers";
        public static string _TableNameShort = "s";

        public int SupplierID { get; set; }

        public string ABN { get; set; }

        public int AccountID { get; set; }

        public string SupplierName { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string PostCode { get; set; }

        public string Mobile { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Suburb { get; set; }

        public string Email { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public Suppliers()
        {
            Active = true;
        }

        public Suppliers Copy()
        {
            Suppliers item = new Suppliers();
            item.SupplierID = this.SupplierID;
            item.ABN = this.ABN;
            item.AccountID = this.AccountID;
            item.SupplierName = this.SupplierName;
            item.StreetNo = this.StreetNo;
            item.StreetName = this.StreetName;
            item.PostCode = this.PostCode;
            item.Mobile = this.Mobile;
            item.Phone = this.Phone;
            item.Fax = this.Fax;
            item.Suburb = this.Suburb;
            item.Email = this.Email;
            item.Active = this.Active;
            item.Deleted = this.Deleted;

            return item;
        }

        public override bool Equals(object obj)
        {
            Suppliers item = new Suppliers();
            try
            {
                item = (Suppliers)obj;
            }
            catch { }

            return item.SupplierID == this.SupplierID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return this.SupplierID.ToString();
        }
    }
}