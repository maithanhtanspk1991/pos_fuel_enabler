﻿namespace DataObject
{
    public class ReceiveLine
    {
        public static string _TableName = "stockreceiveline";
        public static string _TableNameShort = "srl";

        public int ID { get; set; }

        public int ReceiveID { get; set; }

        public int ItemID { get; set; }

        public DataObject.ItemsMenu ItemsMenu { get; set; }

        public int Qty { get; set; }

        public int SellSize { get; set; }

        public bool IsFuel { get; set; }

        public int QtyOnHand { get; set; }

        public double Price { get; set; }

        public double PriceSell { get; set; }

        public double Total { get; set; }

        public ReceiveLine()
        {
            ItemsMenu = new ItemsMenu();
            QtyOnHand = 0;
            Price = 0;
            Total = 0;
            SellSize = 0;
            PriceSell = 0;
        }

        public ReceiveLine Copy()
        {
            ReceiveLine item = new ReceiveLine();
            item.ID = this.ID;
            item.ReceiveID = this.ReceiveID;
            item.ItemID = this.ItemID;
            item.ItemsMenu = this.ItemsMenu.Copy();
            item.Total = this.Total;
            item.Qty = this.Qty;
            item.Price = this.Price;
            item.Total = this.Total;
            item.QtyOnHand = this.QtyOnHand;
            item.SellSize = this.SellSize;
            item.IsFuel = this.IsFuel;
            item.PriceSell = this.PriceSell;
            return item;
        }
    }
}