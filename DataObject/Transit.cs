﻿namespace DataObject
{
    public class Transit
    {
        public int StaffID { get; set; }

        public string StaffName { get; set; }

        public int CableID { get; set; }

        public string Curency { get; set; }

        public bool Online { get; set; }

        public string FirstBracodeItemMenu { get; set; }

        public string FirstBracodeCustomer { get; set; }

        public int MaxLenghtBarcode { get; set; }

        public Transit()
        {
            StaffName = "";
            Curency = "$";
            FirstBracodeItemMenu = "893";
            FirstBracodeCustomer = "893";
            MaxLenghtBarcode = 12;
        }

        public Transit Copy()
        {
            Transit item = new Transit();
            item.StaffID = this.StaffID;
            item.StaffName = this.StaffName;
            item.CableID = this.CableID;
            item.Curency = this.Curency;
            item.FirstBracodeItemMenu = this.FirstBracodeItemMenu;
            item.FirstBracodeCustomer = this.FirstBracodeCustomer;
            item.MaxLenghtBarcode = this.MaxLenghtBarcode;
            return item;
        }
    }
}