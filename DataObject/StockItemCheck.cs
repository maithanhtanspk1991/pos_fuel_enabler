﻿namespace DataObject
{
    public class StockItemCheck
    {
        public static string _TableName = "stocklevel";

        public string barcode { get; set; }

        public int itemID { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public int Qty { get; set; }

        public int QtyChange { get; set; }

        public int QtyInHand { get; set; }

        public string strTypeReason { get; set; }

        public int TypeReason { get; set; }

        //public bool IsInStockLevel { get; set; }
        public void ChangeFromItemMenu(DataObject.ItemsMenu itemmenu)
        {
            this.itemID = itemmenu.ItemID;
            this.Name = itemmenu.ItemDesc;
            this.barcode = itemmenu.Barcode;
        }
    }
}