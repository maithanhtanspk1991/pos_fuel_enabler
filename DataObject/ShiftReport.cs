﻿namespace DataObject
{
    public class ShiftReport : ShiftObject
    {
        public double totalsales { get; set; }

        public double cash { get; set; }

        public double card { get; set; }

        public double gst { get; set; }

        public double refun { get; set; }

        public double account { get; set; }

        public double discount { get; set; }

        public int numoforder { get; set; }

        public double cheque { get; set; }

        public double ChangeSales { get; set; }

        public double CashIn { get; set; }

        public double CashOut { get; set; }

        public double SafeDrop { get; set; }

        public double PayOut { get; set; }

        public double cashAccount { get; set; }

        public double cardAccount { get; set; }

        public bool IsShiftAll { get; set; }

        public ShiftReport CopyDateAndShiftName(ShiftReport shift)
        {
            this.ShiftID = shift.ShiftID;
            this.ShiftName = shift.ShiftName;
            this.datetime = shift.datetime;
            this.strShiftName = shift.ShiftName.ToString();
            return this;
        }
    }
}