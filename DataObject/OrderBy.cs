﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class OrderBy
    {
        private int CurenColumn;
        public int Column { get; set; }
        public List<OrderByColumn> ListColumn { get; set; }
        /// <summary>
        /// Có cho thay đổi kiểu sắp xếp hay không!
        /// </summary>
        public bool IsChangeOrderBy { get; set; }
        public OrderBy()
        {
            CurenColumn = -1;
            Column = -1;
            ListColumn = new List<OrderByColumn>();
            IsChangeOrderBy = false;
        }

        private bool ASC = true;

        public string OrderBySql
        {
            get
            {
                if (IsChangeOrderBy)
                    if (Column != CurenColumn)
                        ASC = true;
                    else
                        ASC = !ASC;
                IsChangeOrderBy = false;
                CurenColumn = Column;
                string result = "";
                if (ListColumn.Exists(s => s.Column == Column))
                {
                    OrderByColumn col = ListColumn.Find(s => s.Column == Column);
                    result = " Order by " + col.ColumnName + " " + (ASC ? "ASC" : "DESC");
                    return result;
                }
                return result;
            }
        }

        /// <summary>
        /// Khi được cấu hình là cột này được sắp xếp, thì mới trả về true, ngược lại trả về false
        /// </summary>
        public bool IsColumnOrderBy
        {
            get
            {
                if (ListColumn.Exists(s => s.Column == Column))
                {
                    return true;
                }
                return false;
            }
        }

    }

    public class OrderByColumn
    {
        public int Column { get; set; }
        public string ColumnName { get; set; }
    }
}
