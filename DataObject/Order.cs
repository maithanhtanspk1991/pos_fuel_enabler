﻿using System;
using System.Collections.Generic;

namespace DataObject
{
    public class Order
    {
        public static string _TableNameAll = "ordersall";
        public static string _TableNameDailLy = "ordersdaily";
        public static string _TableNameSave = "orderssave";
        public static string _TableNameServer = "ordersall_ser";

        public Order()
        {
            ListItem = new List<OrderItem>();
            this.ObjectID = 0;
            this.OrderID = 0;
            StaffName = "";
            IsNewOrder = true;
            Description = "";
            DateOrder = DateTime.Now;
            //Customers = new Customers();
            DiscountPercent = 0;
            Transit = new Transit();
        }

        public double AccountMoney { get; set; }

        public double Balance { get; set; }

        public bool bSaveOrder { get; set; }

        public int CableID { get; set; }

        public int Status { get; set; }

        public double Card { get; set; }

        public double Cash { get; set; }

        public double ChangeAmount { get; set; }

        public int Completed { get; set; }

        public int CustID { get; set; }

        //public Customers Customers { get; set; }

        public Transit Transit { get; set; }

        public DateTime DateOrder { get; set; }

        public string Description { get; set; }

        public double DiscountMoney
        {
            get
            {
                return SubTotal * DiscountPercent / 100;
            }
        }

        public double DiscountMoneyCayXang { get; set; }

        public int DiscountPercent { get; set; }

        public double GST { get; set; }

        public bool IsNewOrder { get; set; }

        public bool IsSaveOrder { get; set; }

        public List<OrderItem> ListItem { get; set; }

        public double Loyalty { get; set; }

        public int ObjectID { get; set; }

        public int OrderID { get; set; }

        public int RefundQty { get; set; }

        public double Round { get; set; }

        public double Service { get; set; }

        public int ShiftID { get; set; }

        public int StaffID { get; set; }

        public string StaffName { get; set; }

        public double SubTotal { get; set; }

        // Use in UI
        public double SubTotalOrder
        {
            get
            {
                return SubTotal - SubTotal * DiscountPercent / 100;
            }
        }

        public double Tendered { get; set; }

        // Use with database
        public Order Copy()
        {
            Order item = new Order();
            item.ObjectID = ObjectID;
            item.OrderID = OrderID;
            item.DateOrder = DateOrder;
            item.CableID = CableID;
            item.SubTotal = SubTotal;
            item.Completed = Completed;
            item.CustID = CustID;
            item.StaffID = StaffID;
            item.Cash = Cash;
            item.Card = Card;
            item.DiscountPercent = DiscountPercent;
            item.GST = GST;
            item.ShiftID = ShiftID;
            item.ChangeAmount = ChangeAmount;
            item.Tendered = Tendered;
            item.Round = Round;
            item.Service = Service;
            item.RefundQty = RefundQty;
            item.Balance = Balance;
            item.AccountMoney = this.AccountMoney;
            item.Loyalty = this.Loyalty;
            item.StaffName = this.StaffName;
            item.Description = this.Description;
            //item.Customers = Customers.Copy();
            item.Transit = Transit.Copy();
            item.Status = Status;
            foreach (OrderItem OrderItem in this.ListItem)
            {
                item.ListItem.Add(OrderItem.Copy());
            }
            return item;
        }

        public void Default()
        {
            OrderID = 0;
            SubTotal = 0;
            DateOrder = DateTime.Now;
            Completed = 0;
            CustID = 0;
            StaffID = 0;
            Cash = 0;
            Card = 0;
            DiscountPercent = 0;
            GST = 0;
            ShiftID = 0;
            ChangeAmount = 0;
            Tendered = 0;
            Round = 0;
            Service = 0;
            RefundQty = 0;
            Balance = 0;
            AccountMoney = 0;
            IsNewOrder = true;
            //Customers = null;
            Transit = null;
            Description = "";
            Loyalty = 0;
            StaffName = "";
            Status = -1;
        }

        public override bool Equals(object obj)
        {
            Order o = (Order)obj;
            return o.ObjectID == this.ObjectID;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}