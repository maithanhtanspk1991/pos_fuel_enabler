﻿namespace DataObject
{
    public class RoundObject
    {
        public double Value { get; set; }

        public double Round { get; set; }

        public override string ToString()
        {
            return Value + "," + Round;
        }
    }
}