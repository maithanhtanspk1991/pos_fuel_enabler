﻿namespace DataObject
{
    public class StockCheck
    {
        public int ID { get; set; }

        public int StaffID { get; set; }

        public int CableID { get; set; }
    }
}