﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObject
{
    public class EnablerConfig
    {
        public string ServerName { get; set; }
        public int TerminalID { get; set; }
        public string TerminalPassword { get; set; }
        public string myApplicationName { get; set; }
        public bool Enabler { get; set; }
		public bool authorizeModePump { get; set; }

        public EnablerConfig()
        {
            ServerName = "localhost";
            TerminalID = 1;
            TerminalPassword = "becastek";
            myApplicationName = "Enabler";
            Enabler = false;
			authorizeModePump = false;
        }
    }
}
