﻿namespace DataObject
{
    public class DeliveringLine
    {
        public static string _TableName = "deliveringline";

        public DeliveringLine()
        {
        }

        public int DeliveringID { get; set; }

        public int Id { get; set; }

        public int ItemID { get; set; }

        public int Qty { get; set; }

        public double Total { get; set; }

        public DeliveringLine Copy()
        {
            DeliveringLine item = new DeliveringLine();
            item.Id = this.Id;
            item.ItemID = this.ItemID;
            item.Qty = this.Qty;
            item.Total = this.Total;
            item.DeliveringID = this.DeliveringID;
            return item;
        }
    }
}