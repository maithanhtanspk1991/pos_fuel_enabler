﻿namespace DataObject
{
    public class Limit
    {
        public Limit()
        {
            Page = 0;
            PageSize = 0;
            SQLTotal = "";
        }

        public Limit(int page, int pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }

        public bool IsLimit
        {
            get
            {
                if (Page == 0 && PageSize == 0)
                    return false;
                else
                    return true;
            }
        }

        public int Page { get; set; }

        public int PageNum
        {
            get
            {
                if (Total > 0 && PageSize > 0)
                {
                    return Total / PageSize + (Total % PageSize == 0 ? 0 : 1);
                }
                return 0;
            }
        }

        public int PageSize { get; set; }

        public int RowFirstIndex
        {
            get
            {
                return (Page - 1) * PageSize;
            }
        }

        public string SqlLimit
        {
            get
            {
                if (IsLimit)
                    return " Limit " + (Page - 1) * PageSize + ", " + PageSize;
                return "";
            }
        }

        public int Total { get; set; }
        public string SQLTotal { get; set; }
        public bool Back()
        {
            if (Page > 1)
            {
                Page--;
                return true;
            }
            return false;
        }

        public void Default()
        {
            Page = 1;
            Total = 0;
        }

        public bool FirstPage()
        {
            if (Page != 1)
            {
                Page = 1;
                return true;
            }
            return false;
        }

        public void GoPage(int index)
        {
            if (index < 1)
                Page = 1;
            else if (index > PageNum)
                Page = PageNum;
            else
                Page = index;
        }

        public bool LastPage()
        {
            if (Page != PageNum)
            {
                Page = PageNum;
                return true;
            }
            return false;
        }

        public bool Next()
        {
            if (Page < PageNum)
            {
                Page++;
                return true;
            }
            return false;
        }
    }
}