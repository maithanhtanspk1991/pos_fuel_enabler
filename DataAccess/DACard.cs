﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class DACard
    {
        private static string _nameClass = "POS::Class::DACard::";
        private static Connection.Connection conn = new Connection.Connection();
        private static string Orderby = " Order by cardName ASC";
        private static DataObject.Card SetValue(System.Data.DataRow row)
        {
            DataObject.Card item = new DataObject.Card();
            try
            {
                item.CardID = row["cardID"].ToString() != "" ? Convert.ToInt32(row["cardID"]) : 0;
                item.CardCode = row["cardCode"].ToString();
                item.CardName = row["cardName"].ToString();
                item.Description = row["description"].ToString();
                item.TypeCard = row["typeCard"].ToString() != "" ? Convert.ToInt32(row["typeCard"]) : 0;
                item.IsActive = row["IsActive"].ToString() != "" ? Convert.ToBoolean(row["IsActive"]) : false;
                item.IsSurchart = row["IsSurchart"].ToString() != "" ? Convert.ToBoolean(row["IsSurchart"]) : false;
                item.Surchart = row["surchart"].ToString() != "" ? Convert.ToDouble(row["surchart"]) : 0;
                item.IsCashOut = row["IsCashOut"].ToString() != "" ? Convert.ToBoolean(row["IsCashOut"]) : false;

            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }

        public static DataObject.Card GetByID(int cardID)
        {
            DataObject.Card item = new DataObject.Card();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM card c Where 1 = 1";
                if (cardID > -1)
                    sql += " And cardID = " + cardID;
                sql += " Limit 0,1";
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return item;
        }


        public static List<DataObject.Card> GetAll(string cardCode, string cardName, int typeCard, int IsActive, int IsSurchart, int IsCashOut, DataObject.Limit limit)
        {
            List<DataObject.Card> lsArray = new List<DataObject.Card>();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM card c Where 1 = 1";
                if (cardCode != "")
                    sql += " And cardCode like '%" + cardCode + "%'";
                if (cardName != "")
                    sql += " And cardName like '%" + cardName + "%'";
                if (IsActive > -1)
                    sql += " And IsActive = " + IsActive;
                if (IsSurchart > -1)
                    sql += " And IsSurchart = " + IsSurchart;
                if (IsCashOut > -1)
                    sql += " And IsCashOut = " + IsCashOut;
                sql += Orderby;
                if (limit.IsLimit)
                {
                    DataAccess.DALimit.GetLimit(sql, limit);
                    sql += limit.SqlLimit;
                }
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        lsArray.Add(SetValue(row));
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetAll::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }

        public static int Insert(DataObject.Card item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Insert Into card (cardCode, cardName, description, typeCard, IsActive, IsSurchart, surchart, IsCashOut) Values ("
                    + "" + "'" + item.CardCode + "'"
                    + "," + "'" + item.CardName + "'"
                    + "," + "'" + item.Description + "'"
                    + "," + "" + item.TypeCard + ""
                    + "," + "" + (item.IsActive ? "1" : "0") + ""
                    + "," + "" + (item.IsSurchart ? "1" : "0") + ""
                    + "," + "" + item.Surchart + ""
                    + "," + "" + (item.IsCashOut ? "1" : "0") + ""

                    + ");";
                result = conn.ExecuteNonQuery(sql);
                result = item.CardID = Convert.ToInt32(conn.ExecuteScalar("select LAST_INSERT_ID()"));
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Update(DataObject.Card item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Update card Set "
                    + " cardCode = " + "'" + item.CardCode + "'"
                    + ", cardName = " + "'" + item.CardName + "'"
                    + ", description = " + "'" + item.Description + "'"
                    + ", typeCard = " + "" + item.TypeCard + ""
                    + ", IsActive = " + "" + (item.IsActive ? 1 : 0) + ""
                    + ", IsSurchart = " + "" + (item.IsSurchart ? 1 : 0) + ""
                    + ", surchart = " + "" + item.Surchart + ""
                    + ", IsCashOut = " + "" + (item.IsCashOut ? 1 : 0) + ""
                    + " Where cardID = " + item.CardID
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.CardID;
                conn.Commit();                
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Deleted(DataObject.Card item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Delete From card Where cardID =  " + item.CardID + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.CardID;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Deleted::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }


    }
}
