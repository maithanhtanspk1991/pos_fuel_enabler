﻿using System;
using System.Collections.Generic;
using System.Data;
using DataObject;

namespace DataAccess
{
    public class DAPermission
    {
        public static List<Permission> GetListPermission(SystemConfig.DBConfig dbconfig)
        {
            List<Permission> lst = new List<Permission>();
            Connection.Connection con = new Connection.Connection(dbconfig);

            try
            {
                con.Open();
                con.BeginTransaction();
                string query = "select id,name from permission";
                DataTable dt = con.Select(query);
                foreach (DataRow dr in dt.Rows)
                {
                    Permission p = new Permission();
                    p.IDPermission = Convert.ToInt32(dr["id"].ToString());
                    p.PermissionName = dr["name"].ToString();
                    lst.Add(p);
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::DAPermission::GetListPermission::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lst;
        }
    }
}