﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class DACompany
    {
        private static string _nameClass = "POS::Class::DACompany::";
        private static Connection.Connection conn = new Connection.Connection();
        private static string Orderby = " Order by companyName ASC";
        private static DataObject.Company SetValue(System.Data.DataRow row)
        {
            //idCompany, companyName, accountLimit, debt, description, Active, companyCode
            //streetNo, streetName, email, phone, mobile
            DataObject.Company item = new DataObject.Company();
            try
            {
                item.IdCompany = row["idCompany"].ToString() != "" ? Convert.ToInt32(row["idCompany"]) : 0;
                item.CompanyName = row["companyName"].ToString();
                item.AccountLimit = row["accountLimit"].ToString() != "" ? Convert.ToInt32(row["accountLimit"]) : 0;
                item.Debt = row["debt"].ToString() != "" ? Convert.ToInt32(row["debt"]) : 0;
                item.Description = row["description"].ToString();
                item.Active = row["Active"].ToString() != "" ? Convert.ToBoolean(row["Active"]) : false;
                item.CompanyCode = row["companyCode"].ToString();
                item.StreetNo = row["streetNo"].ToString();
                item.StreetName = row["streetName"].ToString();
                item.Email = row["email"].ToString();
                item.Phone = row["phone"].ToString();
                item.Mobile = row["mobile"].ToString();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }

        /// <summary>
        /// Trường ID không phải để tìm kiếm, Trường ID để so sánh khác
        /// </summary>
        /// <param name="idCompany"></param>
        /// <param name="companyCode"></param>
        /// <param name="phone"></param>
        /// <param name="Active"></param>
        /// <returns></returns>
        public static int GetCountBy(int idCompany, string companyCode, string phone, int Active)
        {
            int result = 0;
            try
            {
                conn.Open();
                string sql = "SELECT * FROM company c Where 1 = 1 And deleted = 0 ";
                if (idCompany > -1)
                    sql += " And idCompany != " + idCompany;
                if (companyCode != "")
                    sql += " And companyCode = '" + companyCode + "'";
                if (phone != "")
                    sql += " And phone = '" + phone + "'";
                if (Active > -1)
                    sql += " And Active = " + Active;
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                result = tbl.Rows.Count;
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetBy::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public static DataObject.Company GetBy(int idCompany, string companyCode, string phone, int Active)
        {
            DataObject.Company item = new DataObject.Company();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM company c Where 1 = 1 And deleted = 0 ";
                if (idCompany > -1)
                    sql += " And idCompany = " + idCompany;
                if (companyCode != "")
                    sql += " And companyCode = '" + companyCode + "'";
                if (phone != "")
                    sql += " And phone = '" + phone + "'";
                if (Active > -1)
                    sql += " And Active = " + Active;
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetBy::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return item;
        }

        public static List<DataObject.Company> GetAll(string companyName, int Active)
        {
            return GetAll(companyName, Active, new DataObject.Limit() { Page = 0, PageSize = 0 });
        }
        public static List<DataObject.Company> GetAll(string companyName, int Active, DataObject.Limit limit)
        {
            List<DataObject.Company> lsArray = new List<DataObject.Company>();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM company c Where 1 = 1 and deleted <> 1";
                if (companyName != "")
                    sql += " And companyName like '%" + companyName + "%'";
                if (Active > -1)
                    sql += " And Active = " + Active;
                sql += Orderby;
                if (limit.IsLimit)
                {
                    DataAccess.DALimit.GetLimit(sql, limit);
                    sql += limit.SqlLimit;
                }
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        lsArray.Add(SetValue(row));
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetAll::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }

        public static int Insert(DataObject.Company item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Insert Into company (companyName, accountLimit, companyCode, streetNo, streetName, email, phone, mobile) Values ("
                    + "" + "'" + item.CompanyName + "'"
                    + "," + "" + item.AccountLimit + ""
                    + "," + "'" + item.CompanyCode + "'"
                    + "," + "'" + item.StreetNo + "'"
                    + "," + "'" + item.StreetName + "'"
                    + "," + "'" + item.Email + "'"
                    + "," + "'" + item.Phone + "'"
                    + "," + "'" + item.Mobile + "'"
                    + ");";
                result = conn.ExecuteNonQuery(sql);
                item.IdCompany = Convert.ToInt32(conn.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.IdCompany;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Update(DataObject.Company item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Update company Set "
                    + " companyName = " + "'" + item.CompanyName + "'"
                    + " ,accountLimit = " + "" + item.AccountLimit + ""
                    + " ,streetNo = " + "'" + item.StreetNo + "'"
                    + " ,streetName = " + "'" + item.StreetName + "'"
                    + " ,email = " + "'" + item.Email + "'"
                    + " ,phone = " + "'" + item.Phone + "'"
                    + " ,mobile = " + "'" + item.Mobile + "'"
                    + " Where idCompany = " + item.IdCompany
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.IdCompany;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Deleted(DataObject.Company item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Update company Set "
                    + " Active = " + "" + (item.Active ? "1" : "0") + ""
                    + " Where idCompany = " + item.IdCompany
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.IdCompany;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Deleted::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int GetCode(string str)
        {
            int result = -1;
            try
            {
                conn.Open();
                result = Convert.ToInt16(conn.ExecuteScalar("select count(*) from company where companyCode=" + str));
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Deleted::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
    }
}
