﻿using System;
using System.Collections.Generic;
using System.Data;
namespace DataAccess
{
    public class DAShiftReport
    {
        private static Connection.Connection mcon;

        private static DataObject.ShiftReport SetValue(System.Data.DataRow row)
        {
            DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
            try
            {
                shiftrp.gst = double.Parse(row["gst"].ToString());
                shiftrp.account = double.Parse(row["account"].ToString());
                shiftrp.totalsales = double.Parse(row["totalsales"].ToString());
                shiftrp.discount = double.Parse(row["discount"].ToString());
                shiftrp.numoforder = int.Parse(row["numoforder"].ToString());
                shiftrp.cash = double.Parse(row["cash"].ToString());
                shiftrp.card = double.Parse(row["card"].ToString());
                shiftrp.cheque = double.Parse(row["cheque"].ToString());
                shiftrp.ChangeSales = double.Parse(row["changeSales"].ToString());
                shiftrp.CashIn = double.Parse(row["cashin"].ToString());
                shiftrp.CashOut = double.Parse(row["cashout"].ToString());
                shiftrp.PayOut = double.Parse(row["payout"].ToString());
                shiftrp.SafeDrop = double.Parse(row["safedrop"].ToString());
                shiftrp.cashAccount = double.Parse(row["cashaccount"].ToString());
                shiftrp.cardAccount = double.Parse(row["cardAccount"].ToString());
                shiftrp.CashFloatIn = double.Parse(row["CashFloatIn"].ToString());
                shiftrp.CashFloatOut = double.Parse(row["CashFloatOut"].ToString());
            }
            catch
            { }
            return shiftrp;
        }


        public static DataObject.ShiftReport GetReportShift(DataObject.ShiftReport shift)
        {
            DataObject.ShiftReport shiftreport = new DataObject.ShiftReport();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::GetReportShift::CALL sp_reportShiftID" + shift.ShiftID);
                string sql = "CALL sp_reportShiftID(" + shift.ShiftID + ")";
                System.Data.DataTable datatable = mcon.Select(sql);
                if (datatable.Rows.Count > 0)
                {
                    shiftreport = SetValue(datatable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::GetReportShift::Error::" + ex.Message);
            }
            return shiftreport;
        }

        public static DataObject.ShiftReport GetReportShiftAll(DataObject.ShiftReport shift, int CableID)
        {
            DataObject.ShiftReport shiftreport = new DataObject.ShiftReport();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string sql = "CALL sp_reportShiftAll(date('" + DataObject.DateTimeFormat.GetMysqlDate(shift.datetime) + "'), " + CableID + ");";
                System.Data.DataTable datatable = mcon.Select(sql);
                if (datatable.Rows.Count > 0)
                {
                    shiftreport = SetValue(datatable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::GetReportShift::Error::" + ex.Message);
            }
            return shiftreport;
        }

        public static List<DataObject.CardReportShift> GetListCardReportShift(DataObject.ShiftReport shift)
        {
            List<DataObject.CardReportShift> lstshiftreport = new List<DataObject.CardReportShift>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string sql = "CALL sp_reportCardShift(" + shift.ShiftID + ",date('" + DataObject.DateTimeFormat.GetMysqlDate(shift.datetime) + "'))";
                System.Data.DataTable datatable = mcon.Select(sql);
                foreach (System.Data.DataRow dr in datatable.Rows)
                {
                    lstshiftreport.Add(SetValueCard(dr));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::GetReportShift::Error::" + ex.Message);
            }
            return lstshiftreport;
        }

        public static List<DataObject.CardReportShift> GetListCardReportShiftAll(DataObject.ShiftReport shift)
        {
            List<DataObject.CardReportShift> lstshiftreport = new List<DataObject.CardReportShift>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string sql = "CALL sp_reportCardShiftAll(date('" + DataObject.DateTimeFormat.GetMysqlDate(shift.datetime) + "'))";
                System.Data.DataTable datatable = mcon.Select(sql);
                foreach (System.Data.DataRow dr in datatable.Rows)
                {
                    lstshiftreport.Add(SetValueCard(dr));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::GetReportShift::Error::" + ex.Message);
            }
            return lstshiftreport;
        }

        private static DataObject.CardReportShift SetValueCard(System.Data.DataRow row)
        {
            DataObject.CardReportShift shiftrp = new DataObject.CardReportShift();
            shiftrp.Name = row["cardName"].ToString();
            shiftrp.Total = double.Parse(row["subtotal"].ToString());
            return shiftrp;
        }
        private static DataObject.CardReportShift SetValueCardname(System.Data.DataRow row)
        {
            DataObject.CardReportShift shiftrp = new DataObject.CardReportShift();
            shiftrp.Name = row["cardName"].ToString();

            return shiftrp;
        }
        public static List<DataObject.CardReportShift> getCardName()
        {
            List<DataObject.CardReportShift> lstcardname = new List<DataObject.CardReportShift>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string sql = "select cardName from card";
                DataTable dt = new DataTable();
                dt = mcon.Select(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    lstcardname.Add(SetValueCardname(dr));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAShiftReport::getCardName:::::::::::::::::::::::::::::::" + ex.Message);
            }
            return lstcardname;
        }
    }
}