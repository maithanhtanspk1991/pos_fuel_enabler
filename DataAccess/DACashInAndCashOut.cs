﻿using System;

namespace DataAccess
{
    public class DACashInAndCashOut
    {
        private static Connection.Connection con;

        public static int InsertSafeDropAuto(DataObject.CashInAndCashOut cashincashout)
        {
            int kq = 0;
            try
            {
                con = new Connection.Connection();
                con.Open();
                con.BeginTransaction();
                string query = "insert into cashinandcashout(`TotalAmount`, `EmployeeID`, `CashType`, `Description`, `ShiftID`) values(" +
                                                             cashincashout.TotalAmount + "," +
                                                                0 + "," +
                                                                (int)cashincashout.CashType + "," +
                                                                 "'" + cashincashout.Description + "'" + "," +
                                                                 cashincashout.ShiftID + ")";

                kq = con.ExecuteNonQuery(query);
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::DACashInAndCashOut::InsertSafeDrop::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return 1;
        }
    }
}