﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAManageCar
    {
        private static Connection.Connection mcon;

        private static DataObject.ManageCar setValue(System.Data.DataRow dr)
        {
            DataObject.ManageCar managecar = new DataObject.ManageCar();
            try
            {
                managecar.CarNumber = dr["CarNumber"].ToString();
                managecar.CarColor = dr["CarColor"].ToString();
                managecar.CarProduct = dr["CarProduct"].ToString();
                managecar.ObjectID = Convert.ToInt32(dr["id"].ToString());

                managecar.CustomerID = Convert.ToInt32(dr["custID"]);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::setValue::" + ex.Message);
            }
            return managecar;
        }

        public static List<DataObject.ManageCar> GetListManageCarWithCustomerID(DataObject.ManageCar ManageCar, SystemConfig.DBConfig dbconfig)
        {
            List<DataObject.ManageCar> lst = new List<DataObject.ManageCar>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::GetListManageCarWithCustomerID::Start");

                string sql = "Select * from managecar where `deleted`=0 and custID=" + ManageCar.CustomerID;

                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::GetListManageCarWithCustomerID::" + sql);
                System.Data.DataTable dt = mcon.Select(sql);
                foreach (System.Data.DataRow rowitem in dt.Rows)
                {
                    DataObject.ManageCar caritem = setValue(rowitem);
                    lst.Add(caritem);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::GetListManageCarWithCustomerID::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return lst;
        }

        public static int Insert(ref DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                mcon.BeginTransaction();
                string sql = "insert into managecar(`custID`, `CarNumber`, `CarColor`, `CarProduct`)values(" +
                                                        "" + managecar.CustomerID + "," +
                                                        "'" + managecar.CarNumber + "'," +
                                                        "'" + managecar.CarColor + "'," +
                                                        "'" + managecar.CarProduct + "'" +
                                                        ")";
                kq = mcon.ExecuteNonQuery(sql);
                managecar.ObjectID = Convert.ToInt32(mcon.ExecuteScalar("SELECT LAST_INSERT_ID()"));
                mcon.Commit();
            }
            catch (Exception ex)
            {
                mcon.Rollback();
                kq = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::Insert::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }

        public static int Update(DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                mcon.BeginTransaction();
                string sql = "update managecar set custID =" + managecar.CustomerID + "," +
                                                   "CarNumber ='" + managecar.CarNumber + "'," +
                                                   "CarColor = '" + managecar.CarColor + "'," +
                                                   "CarProduct ='" + managecar.CarProduct + "'" +
                                                        " where id=" + managecar.ObjectID;
                kq = mcon.ExecuteNonQuery(sql);
                mcon.Commit();
            }
            catch (Exception ex)
            {
                mcon.Rollback();
                kq = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::Update::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }

        public static int Delete(DataObject.ManageCar managecar, SystemConfig.DBConfig dbconfig)
        {
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                mcon.BeginTransaction();
                string sql = "update managecar set deleted = 1" +
                                                       " where id=" + managecar.ObjectID;
                kq = mcon.ExecuteNonQuery(sql);
                mcon.Commit();
            }
            catch (Exception ex)
            {
                mcon.Rollback();
                kq = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::DAManageCar::Delete::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }
    }
}