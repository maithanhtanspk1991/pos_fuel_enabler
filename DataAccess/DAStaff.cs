﻿using System;
using System.Collections.Generic;
using System.Data;
using DataObject;

namespace DataAccess
{
    public class DAStaff
    {
        private SystemConfig.DBConfig dbconfig;
        private static string _nameClass = "POS::Class::DAStaff::";
        private static Connection.Connection conn = new Connection.Connection();

        public DAStaff(SystemConfig.DBConfig dbconfig)
        {
            this.dbconfig = dbconfig;
        }

        public List<Staff> GetListStaff()
        {
            List<Staff> lst = new List<Staff>();
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query = "Select s.id as id,userPass,userName,staffName,p.id as permissionID,name from staffs s,permission p where s.permissionID = p.id";
                DataTable dt = con.Select(query);

                foreach (DataRow dr in dt.Rows)
                {
                    Staff s = new Staff();
                    s.StaffID = Convert.ToInt32(dr["id"].ToString());
                    s.PassWord = dr["userPass"].ToString();
                    s.UserName = dr["userName"].ToString();
                    s.StaffName = dr["staffName"].ToString();
                    s.IDPermission = Convert.ToInt32(dr["permissionID"].ToString());
                    s.PermissionName = dr["name"].ToString();
                    lst.Add(s);
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "GetListData::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lst;
        }

        public static Staff GetStaffById(int ID, SystemConfig.DBConfig mDBConfig)
        {
            Connection.Connection con = new Connection.Connection(mDBConfig);
            Staff item = new Staff();
            try
            {
                con.Open();
                con.BeginTransaction();
                string query = "Select * from staffs s id= " + ID;
                DataTable dt = con.Select(query);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    item.StaffID = Convert.ToInt32(dr["id"].ToString());
                    item.PassWord = dr["userPass"].ToString();
                    item.UserName = dr["userName"].ToString();
                    item.StaffName = dr["staffName"].ToString();
                    item.IDPermission = Convert.ToInt32(dr["permissionID"].ToString());
                    item.PermissionName = dr["name"].ToString();
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "GetListData::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public bool CheckStaff(Staff s)
        {
            bool kq = false;
            Connection.Connection con = new Connection.Connection(this.dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query;
                if (s.StaffID == 0)
                {
                    query = "Select count(ID) from staffs s where s.UserName = '" + s.UserName + "'";
                }
                else
                {
                    query = "Select count(ID) from staffs s where s.UserName = '" + s.UserName + "' and ID <> " + s.StaffID;
                }
                int count = Convert.ToInt32(con.ExecuteScalar(query));
                if (count > 0)
                {
                    kq = true;
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "GetListData::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return kq;
        }

        public int InsertStaff(Staff s)
        {
            int kq = 0;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query = "insert into staffs(userName,userPass,staffName,permissionID) values('" + s.UserName + "',password('" + s.PassWord + "'),'" + s.StaffName + "'," + s.IDPermission + ")";

                kq = con.ExecuteNonQuery(query);
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "InsertStaff::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return kq;
        }

        public int UpdateStaff(Staff s)
        {
            int kq = 0;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query = string.Empty;
                if (s.PassWord == string.Empty)
                {
                    query = "update staffs set userName='" + s.UserName + "',staffName='" + s.StaffName + "',permissionID=" + s.IDPermission + " where id=" + s.StaffID;
                }
                else
                {
                    query = "update staffs set userName='" + s.UserName + "',userPass =password('" + s.PassWord + "'),staffName='" + s.StaffName + "',permissionID=" + s.IDPermission + " where id=" + s.StaffID;
                }

                kq = con.ExecuteNonQuery(query);
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateStaff::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return kq;
        }

        public int DeleteStaff(Staff s)
        {
            int kq = 0;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query = "delete from staffs where ID=" + s.StaffID;
                kq = con.ExecuteNonQuery(query);
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "DeleteStaff::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return kq;
        }

        public bool CheckLogin(ref Staff s)
        {
            bool kq = false;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string query;
                if (s.IDPermission == 0)
                {
                    query = "select ID,StaffName from staffs where userName='" + s.UserName + "' and userPass=password('" + s.PassWord + "')";
                }
                else
                {
                    query = "select ID,StaffName from staffs where userName='" + s.UserName + "' and userPass=password('" + s.PassWord + "') and permissionID >=" + s.IDPermission;
                }
                DataTable dt = con.Select(query);
                if (dt.Rows.Count > 0)
                {
                    s.StaffID = Convert.ToInt32(dt.Rows[0]["ID"].ToString());
                    s.StaffName = dt.Rows[0]["StaffName"].ToString();
                    kq = true;
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "DeleteStaff::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return kq;
        }

        public static DataObject.Staff GetByStaffID(string ID)
        {
            Staff item = new Staff();
            try
            {
                conn.Open();
                string query = "Select * from staffs s where staffID= " + ID;
                System.Data.DataTable dt = conn.Select(query);
                if (dt.Rows.Count > 0)
                {
                    item.StaffID = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    item.PassWord = dt.Rows[0]["userPass"].ToString();
                    item.UserName = dt.Rows[0]["userName"].ToString();
                    item.StaffName = dt.Rows[0]["staffName"].ToString();
                    item.IDPermission = Convert.ToInt32(dt.Rows[0]["permissionID"].ToString());
                    item.PermissionName = dt.Rows[0]["name"].ToString();
                }

            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetByStaffID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return item;
        }
    }
}