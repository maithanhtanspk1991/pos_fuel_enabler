﻿using System;
using System.Collections.Generic;
using DataObject;

namespace DataAccess
{
    public class DADeliveringLine
    {
        private static string _nameClass = "DADeliveringLine";

        private static DeliveringLine SetValue(System.Data.DataRow row)
        {
            DeliveringLine item = new DeliveringLine();
            if (row != null)
            {
                item.Id = row["id"].ToString() != "" ? Convert.ToInt32(row["id"].ToString()) : 0;
                item.ItemID = row["itemID"].ToString() != "" ? Convert.ToInt32(row["itemID"].ToString()) : 0;
                item.Qty = row["qty"].ToString() != "" ? Convert.ToInt32(row["qty"].ToString()) : 0;
                item.Total = row["total"].ToString() != "" ? Convert.ToDouble(row["total"].ToString()) : 0;
                item.DeliveringID = row["deliveringID"].ToString() != "" ? Convert.ToInt32(row["deliveringID"].ToString()) : 0;
            }
            return item;
        }

        public static int InsertConnection(DeliveringLine item, Connection.Connection con)
        {
            int result = -1;
            try
            {
                string sql = "";
                sql += "Insert Into " + DataObject.DeliveringLine._TableName + " (id, itemID, qty, total, deliveringID) Values ("
                    + "" + "" + item.Id + ""
                    + "," + "" + item.ItemID + ""
                    + "," + "" + item.Qty + ""
                    + "," + "" + item.Total + ""
                    + "," + "" + item.DeliveringID + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.Id = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.Id;
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::InsertConnection::" + ex.Message);
            }
            return result;
        }

        public static int Insert(DeliveringLine item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                result = InsertConnection(item, con);
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static List<DeliveringLine> GetDeliveringLineConnection(int DeliveringID, Connection.Connection con)
        {
            List<DeliveringLine> lsArray = new List<DeliveringLine>();
            try
            {
                string sql = "";
                sql = "select * from " + DeliveringLine._TableName + " where 1=1";
                if (DeliveringID > 0)
                    sql += " And deliveringID = " + DeliveringID;
                sql += ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        DeliveringLine item = new DeliveringLine();
                        item = SetValue(row);
                        lsArray.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDeliveringLineConnection::" + ex.Message);
            }
            return lsArray;
        }

        public static List<DeliveringLine> GetDeliveringLine(int DeliveringID, SystemConfig.DBConfig mDBConfig)
        {
            List<DeliveringLine> lsArray = new List<DeliveringLine>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                lsArray = GetDeliveringLineConnection(DeliveringID, con);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDelivering::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }
    }
}