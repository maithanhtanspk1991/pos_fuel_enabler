﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DASuppliers
    {
        private static string _nameClass = "DASuppliers";
        private static string Orderby = " Order by supplierName ASC";

        private static DataObject.Suppliers SetValue(System.Data.DataRow row)
        {
            DataObject.Suppliers item = new DataObject.Suppliers();
            if (row != null)
            {
                item.SupplierID = row["supplierID"].ToString() != "" ? Convert.ToInt32(row["supplierID"].ToString()) : 0;
                item.ABN = row["ABN"].ToString() != "" ? row["ABN"].ToString() : "";
                item.AccountID = row["accountID"].ToString() != "" ? Convert.ToInt32(row["accountID"].ToString()) : 0;
                item.SupplierName = row["supplierName"].ToString() != "" ? row["supplierName"].ToString() : "";
                item.StreetNo = row["streetNo"].ToString() != "" ? row["streetNo"].ToString() : "";
                item.StreetName = row["streetName"].ToString() != "" ? row["streetName"].ToString() : "";
                item.PostCode = row["postCode"].ToString() != "" ? row["postCode"].ToString() : "";
                item.Phone = row["phone"].ToString() != "" ? row["phone"].ToString() : "";
                item.Mobile = row["mobile"].ToString() != "" ? row["mobile"].ToString() : "";
                item.Email = row["email"].ToString() != "" ? row["email"].ToString() : "";
                item.Fax = row["fax"].ToString() != "" ? row["fax"].ToString() : "";
                item.Suburb = row["suburb"].ToString() != "" ? row["suburb"].ToString() : "";
                item.Active = row["active"].ToString() != "" ? Convert.ToBoolean(row["active"]) : false;
                item.Deleted = row["deleted"].ToString() != "" ? Convert.ToBoolean(row["deleted"]) : false;
            }
            return item;
        }

        public static DataObject.Suppliers GetSuppliersByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.Suppliers item = new DataObject.Suppliers();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + DataObject.Suppliers._TableName + " where supplierID=" + iD);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetSuppliersByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public static List<DataObject.Suppliers> GetSuppliers(string name, int active, int deleted, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.Suppliers> lsArray = new List<DataObject.Suppliers>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + DataObject.Suppliers._TableName + " " + DataObject.Suppliers._TableNameShort + " where 1=1";
                if (active > -1)
                    sql += " And active = " + active;
                if (deleted > -1)
                    sql += " And deleted = " + deleted;
                if (name != "")
                    sql += " And supplierName like '%" + name + "%'";
                sql += Orderby;
                sql += ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        if (row != null)
                        {
                            DataObject.Suppliers item = new DataObject.Suppliers();
                            item = SetValue(row);
                            lsArray.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetSuppliers::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Insert(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Insert Into " + DataObject.Suppliers._TableName + " (ABN, accountID, supplierName, streetNo, streetName, postCode, mobile, phone, fax, suburb, email, active, deleted) Values ("
                    + "" + "'" + item.ABN + "'"
                    + "," + "" + item.AccountID + ""
                    + "," + "'" + item.SupplierName + "'"
                    + "," + "'" + item.StreetNo + "'"
                    + "," + "'" + item.StreetName + "'"
                    + "," + "'" + item.PostCode + "'"
                    + "," + "'" + item.Mobile + "'"
                    + "," + "'" + item.Phone + "'"
                    + "," + "'" + item.Fax + "'"
                    + "," + "'" + item.Suburb + "'"
                    + "," + "'" + item.Email + "'"
                    + "," + "" + (item.Active == true ? "1" : "0") + ""
                    + "," + "" + (item.Deleted == true ? "1" : "0") + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.SupplierID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.SupplierID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Update(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.Suppliers._TableName + " Set "
                    + "ABN = " + "'" + item.ABN + "'"
                    + ",accountID = " + "" + item.AccountID + ""
                    + ",supplierName = " + "'" + item.SupplierName + "'"
                    + ",streetNo = " + "'" + item.StreetNo + "'"
                    + ",streetName = " + "'" + item.StreetName + "'"
                    + ",postCode = " + "'" + item.PostCode + "'"
                    + ",phone = " + "'" + item.Phone + "'"
                    + ",mobile = " + "'" + item.Mobile + "'"
                    + ",email = " + "'" + item.Email + "'"
                    + ",fax = " + "'" + item.Fax + "'"
                    + ",suburb = " + "'" + item.Suburb + "'"
                    + ",active = " + "" + (item.Active == true ? "1" : "0") + ""
                    + " Where supplierID = " + item.SupplierID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.SupplierID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Delete(DataObject.Suppliers item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.Suppliers._TableName + " Set "
                    + "deleted = " + "1" + ""
                    + " Where supplierID = " + item.SupplierID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.SupplierID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }
    }
}