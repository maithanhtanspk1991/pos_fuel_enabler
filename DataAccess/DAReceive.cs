﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAReceive
    {
        private static string _nameClass = "DAReceive";
        private static string Orderby = " Order by ts DESC";

        private static DataObject.Receive SetValue(System.Data.DataRow row)
        {
            DataObject.Receive item = new DataObject.Receive();
            if (row != null)
            {
                item.ID = row["id"].ToString() != "" ? Convert.ToInt32(row["id"].ToString()) : 0;
                item.StaffID = row["staffID"].ToString() != "" ? Convert.ToInt32(row["staffID"].ToString()) : 0;
                item.SupplierID = row["supplierID"].ToString() != "" ? Convert.ToInt32(row["supplierID"].ToString()) : 0;
                item.Date = row["ts"].ToString() != "" ? Convert.ToDateTime(row["ts"]) : DateTime.Now;
                item.SubTotal = row["subTotal"].ToString() != "" ? Convert.ToDouble(row["subTotal"].ToString()) : 0;
                item.TicketID = row["ticketID"].ToString() != "" ? row["ticketID"].ToString() : "";
                item.CableID = row["cableID"].ToString() != "" ? Convert.ToInt32(row["cableID"].ToString()) : 0;
            }
            return item;
        }

        public static DataObject.Receive GetReceiveByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.Receive item = new DataObject.Receive();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + DataObject.Receive._TableName + " where id=" + iD);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetReceiveByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="staffID">Mã staffID</param>
        /// <param name="supplierID">Mã supplierID</param>
        /// <param name="cableID">Mã cableID</param>
        /// <param name="date">Có kiểm tra ngày tháng hay không</param>
        /// <param name="Start">Ngày bắt đầu</param>
        /// <param name="End">Ngày kết thúc</param>
        /// <param name="mDBConfig">Config chuỗi kết nối</param>
        /// <returns></returns>
        public static List<DataObject.Receive> GetReceive(int staffID, int supplierID, int cableID, bool date, DateTime Start, DateTime End, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.Receive> lsArray = new List<DataObject.Receive>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + DataObject.Receive._TableName + " " + DataObject.Receive._TableNameShort + " where 1=1";
                if (staffID > -1)
                    sql += " And staffID = " + staffID;
                if (supplierID > -1)
                    sql += " And supplierID = " + supplierID;
                if (cableID > -1)
                    sql += " And cableID = " + cableID;
                if (date)
                {
                    sql += String.Format(" And date(ts) >= date('{0}')", DataObject.DateTimeFormat.GetMysqlDate(Start));
                    sql += String.Format(" And date(ts) <= date('{0}')", DataObject.DateTimeFormat.GetMysqlDate(End));
                }
                sql += Orderby;
                sql += ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        if (row != null)
                        {
                            DataObject.Receive item = new DataObject.Receive();
                            item = SetValue(row);
                            lsArray.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetReceive::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Insert(DataObject.Receive item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql = "Insert Into " + DataObject.Receive._TableName + " (staffID, supplierID, ts, subTotal, ticketID, cableID) Values ("
                    + "" + "" + item.StaffID + ""
                    + "," + "" + item.SupplierID + ""
                    + "," + "'" + DataObject.DateTimeFormat.GetMysqlDateTime(item.Date) + "'"
                    + "," + "" + item.SubTotal + ""
                    + "," + "'" + item.TicketID + "'"
                    + "," + "" + item.CableID + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.ID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                foreach (DataObject.ReceiveLine line in item.ReceiveLine)
                {
                    DataAccess.DAStockCheckItem.ExistItemConnection(line.ItemID, con);
                    sql = String.Format("Update stocklevel Set QtyOnHand = QtyOnHand + {0}, QtyReceived = QtyReceived + {1} Where ItemID = {2}", line.Qty, line.Qty, line.ItemID);
                    result = con.ExecuteNonQuery(sql);

                    sql = "Insert Into " + DataObject.ReceiveLine._TableName + " (receiveID, itemID, SellSize, qty, price, total) Values ("
                    + "" + "" + item.ID + ""
                    + "," + "" + line.ItemID + ""
                    + "," + "" + line.SellSize + ""
                    + "," + "" + line.Qty + ""
                    + "," + "" + line.Price + ""
                    + "," + "" + line.Total + ""
                    + ");";
                    result = con.ExecuteNonQuery(sql);
                    line.ID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                }
                result = item.ID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Update(DataObject.Receive item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.Receive._TableName + " Set "
                    + "staffID = " + "" + item.StaffID + ""
                    + ",supplierID = " + "" + item.SupplierID + ""
                    + ",ts = " + "'" + DataObject.DateTimeFormat.GetMysqlDateTime(item.Date) + "'"
                    + ",subTotal = " + "" + item.SubTotal + ""
                    + ",ticketID = " + "'" + item.TicketID + "'"
                    + ",cableID = " + "" + item.CableID + ""
                    + " Where id = " + item.ID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.ID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Delete(DataObject.Receive item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Delete from " + DataObject.Receive._TableName
                    + " Where id = " + item.ID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.SupplierID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }
    }
}