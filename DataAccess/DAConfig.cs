﻿using System;

namespace DataAccess
{
    public class DAConfig
    {
        /// <summary>
        /// Hàm lấy giá trị config
        /// </summary>
        /// <param name="header">Header</param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="dbconfig"></param>
        /// <returns></returns>
        public static object GetConfig(string header, string name, string value, SystemConfig.DBConfig dbconfig)
        {
            object obj = GetConfig(header, name, dbconfig);
            if (value != "" && obj == null)
            {
                SetConfig(header, name, value, dbconfig);
                obj = value;
            }
            return obj;
        }

        public static object GetConfig(string header, string name, SystemConfig.DBConfig dbconfig)
        {
            object obj = null;
            string sql = "";
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                sql = "select `value` from config where cableID = " + dbconfig.CableID + " And `header`='" + header + "'" + " and `varName`='" + name + "'";
                obj = con.ExecuteScalar(sql);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAConfig::GetConfig::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return obj;
        }

        public static void SetConfig(string header, string name, string value, SystemConfig.DBConfig dbconfig)
        {
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql = "select `value` from config where cableID = " + dbconfig.CableID + " And `header`='" + header + "'" + " and `varName`='" + name + "'";
                object obj = con.ExecuteScalar(sql);
                if (obj != null)
                {
                    sql = "update config set `value`='" + value + "' where cableID = " + dbconfig.CableID + " And `header`='" + header + "'" + " and `varName`='" + name + "'";
                    con.ExecuteNonQuery(sql);
                }
                else
                {
                    sql = "insert into config(`value`,`header`,`varName`, cableID) values('" + value + "','" + header + "','" + name + "', " + dbconfig.CableID + ")";
                    con.ExecuteNonQuery(sql);
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAConfig::SetConfig::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
        }
    }
}