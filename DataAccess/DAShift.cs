﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAShift
    {
        private static Connection.Connection mcon;
        private static string _nameClass = "DataAccess::DAShift::";

        public static List<DataObject.ShiftObject> GetListShift(int CableID)
        {
            List<DataObject.ShiftObject> lstshift = new List<DataObject.ShiftObject>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string sql = "Select shiftID, ts, staffID, cableID, areaID, endday, completed, ShiftName, CashFloatIn, CashFloatOut from shifts Where 1 = 1";
                if (CableID > 0)
                    sql += " And cableID = " + CableID;
                sql += ";";
                System.Data.DataTable datatable = mcon.Select(sql);
                foreach (System.Data.DataRow row in datatable.Rows)
                {
                    lstshift.Add(Setvalue(row));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetListShift::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return lstshift;
        }

        private static DataObject.ShiftObject Setvalue(System.Data.DataRow datarow)
        {
            DataObject.ShiftObject item = new DataObject.ShiftObject();
            try
            {
                item.Completed = int.Parse(datarow["completed"].ToString()) == 0 ? DataObject.CompletedShift.NONE : DataObject.CompletedShift.COMPLETED;
                item.datetime = DateTime.Parse(datarow["ts"].ToString());
                item.EndDay = int.Parse(datarow["endday"].ToString());
                item.ShiftName = int.Parse(datarow["ShiftName"].ToString());
                item.ShiftID = int.Parse(datarow["shiftID"].ToString());
                item.CableID = int.Parse(datarow["cableID"].ToString());
                item.strShiftName = item.ShiftName.ToString();
                item.CashFloatIn = double.Parse(datarow["CashFloatIn"].ToString());
                item.CashFloatOut = double.Parse(datarow["CashFloatOut"].ToString());
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "Setvalue::" + ex.Message);
            }
            return item;
        }

        public static List<DataObject.ShiftObject> GetListShiftWithDate(DataObject.ShiftObject shift, int CableID)
        {
            List<DataObject.ShiftObject> lstshift = new List<DataObject.ShiftObject>();
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string query = "Select shiftID, ts, staffID, cableID, areaID, endday, completed, ShiftName,CashFloatIn, CashFloatOut from shifts where ts >= '" + DataObject.DateTimeFormat.GetMysqlDateTime(shift.datetime) + "' and ts <= '" + DataObject.DateTimeFormat.GetMysqlDateTime(shift.datetime.AddDays(1)) + "'";
                if (CableID > 0)
                    query += " And cableID = " + CableID;
                System.Data.DataTable datatable = mcon.Select(query);
                foreach (System.Data.DataRow row in datatable.Rows)
                {
                    lstshift.Add(Setvalue(row));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetListShiftWithDate::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return lstshift;
        }

        public static int InsertShift(ref DataObject.ShiftObject shift)
        {
            string query = "Insert into Shifts( staffID, cableID,  completed, ShiftName,CashFloatIn) values (" +
                            shift.StaffID + "," +
                            shift.CableID + "," +
                            "0 " + "," +
                            shift.ShiftName + "," +
                            shift.CashFloatIn + "" +
                            ");";
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                mcon.BeginTransaction();
                kq = mcon.ExecuteNonQuery(query);
                if (kq > 0)
                {
                    shift.ShiftID = int.Parse(mcon.ExecuteScalar("Select LAST_INSERT_ID()").ToString());
                }
                mcon.Commit();
            }
            catch (Exception ex)
            {
                mcon.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "InsertShift::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }

        public static DataObject.ShiftObject GetShiftMaxWithCableID(DataObject.ShiftObject shift)
        {
            mcon = new Connection.Connection();
            DataObject.ShiftObject shiftObject = new DataObject.ShiftObject();
            try
            {
                mcon.Open();
                string query = "select MAX(shiftID) from shifts where cableID=" + shift.CableID;
                shift.ShiftID = int.Parse(mcon.ExecuteScalar(query).ToString());
                shiftObject = GetShiftMaxWithShiftID(shift);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetShiftMaxWithCableID::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return shiftObject;
        }

        public static List<string> GetCableID()
        {
            List<string> lsArray = new List<string>();
            mcon = new Connection.Connection();
            DataObject.ShiftObject shiftObject = new DataObject.ShiftObject();
            try
            {
                mcon.Open();
                string query = "SELECT cableID FROM config c Where header = 'Shift' And varName = 'CurrentShift';";
                System.Data.DataTable dt = mcon.Select(query);
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    lsArray.Add(row["cableID"].ToString());
                }                
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetShiftMaxWithCableID::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return lsArray;
        }

        public static DataObject.ShiftObject GetShiftMaxWithShiftID(DataObject.ShiftObject shift)
        {
            mcon = new Connection.Connection();
            DataObject.ShiftObject shiftobject = new DataObject.ShiftObject();
            try
            {
                mcon.Open();
                string query = "select shiftID, ts, staffID, cableID, areaID, endday, completed, ShiftName,CashFloatIn,CashFloatOut from shifts where shiftID=" + shift.ShiftID;
                System.Data.DataTable datatable = mcon.Select(query);
                if (datatable.Rows.Count > 0)
                {
                    shiftobject = Setvalue(datatable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetShiftMaxWithShiftID::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }

            return shiftobject;
        }

        public static int UpdateCompleted(DataObject.ShiftObject shift)
        {
            string query = "Update Shifts set completed =1,ts =ts,CashFloatOut=" + shift.CashFloatOut + " where shiftID=" + shift.ShiftID;
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                mcon.BeginTransaction();
                kq = mcon.ExecuteNonQuery(query);
                mcon.Commit();
            }
            catch (Exception ex)
            {
                mcon.Rollback();
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateCompleted::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }

        public static int GetFloatCashIn(ref DataObject.ShiftObject shift)
        {
            int kq = 0;
            mcon = new Connection.Connection();
            try
            {
                mcon.Open();
                string query = "Select CashFloatIn,CashFloatOut, shiftID, ts, staffID, cableID, areaID, endday, completed, ShiftName from shifts where shiftID= " + shift.ShiftID + ";";

                System.Data.DataTable datatable = mcon.Select(query);
                kq = datatable.Rows.Count;
                if (kq > 0)
                {
                    shift.CashFloatIn = Setvalue(datatable.Rows[0]).CashFloatIn;
                    shift.CashFloatOut = Setvalue(datatable.Rows[0]).CashFloatOut;
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetFloatCashIn::" + ex.Message);
            }
            finally
            {
                mcon.Close();
            }
            return kq;
        }
    }
}