﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class DALimit
    {
        private static string _nameClass = "DataAccess::DALimit::";
        private static Connection.Connection conn = new Connection.Connection();
        public static void GetLimit(string sql, DataObject.Limit limit)
        {
            try
            {
                conn.Open();
                if (limit.SQLTotal != "")
                    limit.Total = Convert.ToInt32(conn.Select(limit.SQLTotal).Rows[0][0]);
                else
                    limit.Total = Convert.ToInt32(conn.Select(SQL(sql)).Rows[0][0]);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetLimit::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public static void GetLimit(string sql, DataObject.Limit limit, SystemConfig.DBConfig mDBConfig)
        {
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                if (limit.SQLTotal != "")
                    limit.Total = Convert.ToInt32(con.Select(limit.SQLTotal).Rows[0][0]);
                else
                    limit.Total = Convert.ToInt32(con.Select(SQL(sql)).Rows[0][0]);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetLimit::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private static string SQL(string sql)
        {
            sql = sql.ToLower();
            int first = sql.IndexOf("select") + 7;
            int last = sql.IndexOf("from") - 1;
            sql = sql.Substring(0, first) + " Count(*) As Total " + sql.Substring(last);
            return sql;
        }
    }
}
