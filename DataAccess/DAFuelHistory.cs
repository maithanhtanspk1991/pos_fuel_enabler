﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAFuelHistory
    {
        private static string _nameClass = "DataAccess::DAFuelHistory::";

        public static List<DataObject.FuelHistory> GetAll()
        {
            List<DataObject.FuelHistory> lsArray = new List<DataObject.FuelHistory>();
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM fuelhistory f;";
                System.Data.DataTable dt = conn.Select(sql);
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    lsArray.Add(SetValue(row));
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetAll::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }

        public static DataObject.FuelHistory GetFuelHistoryByID(int ID)
        {
            DataObject.FuelHistory item = new DataObject.FuelHistory();
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM fuelhistory f Where ID = " + ID + ";";
                System.Data.DataTable dt = conn.Select(sql);
                if (dt.Rows.Count > 0)
                {
                    item = SetValue(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetFuelHistoryByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return item;
        }

        private static DataObject.FuelHistory SetValue(System.Data.DataRow row)
        {
            DataObject.FuelHistory item = new DataObject.FuelHistory();
            try
            {
                if (row != null)
                {
                    item.ID = row["ID"].ToString() != "" ? Convert.ToInt32(row["ID"]) : 0;
                    item.KindOfFuel = row["KindOfFuel"].ToString() != "" ? Convert.ToInt32(row["KindOfFuel"]) : 0;
                    item.PumID = row["PumID"].ToString() != "" ? Convert.ToInt32(row["PumID"]) : 0;
                    item.CashAmount = row["CashAmount"].ToString() != "" ? Convert.ToInt32(row["CashAmount"]) : 0;
                    item.VolumeAmount = row["VolumeAmount"].ToString() != "" ? Convert.ToInt32(row["VolumeAmount"]) : 0;
                    item.UnitPrice = row["UnitPrice"].ToString() != "" ? Convert.ToInt32(row["UnitPrice"]) : 0;
                    item.IsComplete = row["IsComplete"].ToString() != "" ? Convert.ToInt32(row["IsComplete"]) : 0;
                    item.FromPump = row["FromPump"].ToString() != "" ? Convert.ToInt32(row["FromPump"]) : 0;
                    item.Gst = row["Gst"].ToString() != "" ? Convert.ToInt32(row["Gst"]) : 0;
                    item.CableID = row["CableID"].ToString() != "" ? Convert.ToInt32(row["CableID"]) : 0;
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }

        public static int UpdateCableIDByID(int ID, int CableID)
        {
            int result = -1;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "UPDATE fuelhistory Set CableID = " + CableID + " Where ID = " + ID + ";";
                result = conn.ExecuteNonQuery(sql);
                conn.Commit();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateCableIDByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int UpdateAllRefreshCableIDByID(int CableID)
        {
            int result = -1;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "UPDATE fuelhistory Set CableID = " + 0 + " Where CableID = " + CableID + " And IsComplete = 0;";
                result = conn.ExecuteNonQuery(sql);
                conn.Commit();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateAllRefreshCableIDByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int UpdateAllRefreshCableIDByID(int CableID, string NotID)
        {
            int result = -1;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                conn.BeginTransaction();
                if (NotID != "")
                    NotID = "And ID Not in (" + NotID + ")";
                string sql = "UPDATE fuelhistory Set CableID = " + 0 + " Where CableID = " + CableID + " And IsComplete = 0 " + NotID + ";";
                result = conn.ExecuteNonQuery(sql);
                conn.Commit();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateAllRefreshCableIDByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int InsertFuelHistory(DataObject.FuelHistory fuel)
        {
            int result = -1;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "insert into fuelhistory (KindOfFuel, PumID, CashAmount, VolumeAmount, UnitPrice, IsComplete, FromPump, Gst, CableID) values " +
                                " (" + fuel.KindOfFuel + "," + fuel.PumID + "," + fuel.CashAmount + "," + fuel.VolumeAmount + "," + fuel.UnitPrice + "," + fuel.IsComplete + "," + fuel.FromPump + "," + fuel.Gst + "," + fuel.CableID + ")";
                result = conn.ExecuteNonQuery(sql);
                conn.Commit();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateAllRefreshCableIDByID::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public static int getIDbyitemIDandPumpID(int itemID, int PumpID)
        {
            int result = 0;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "select ID from fuelhistory where PumID = " + PumpID + " and KindOfFuel = " + itemID + " and IsComplete = 0";
                result = Convert.ToInt32(conn.ExecuteScalar(sql));
            }
            catch (Exception ex)
            {
                result = 0;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public static int UpdateCompleteOrder(int orderID, int pumpID,int fuelID)
        {
            int result = -1;
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "update ordersdailyline set pumpID = " + pumpID + ", pumpHistoryID = " + fuelID + " where orderID = " + orderID;
                result = conn.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "UpdateAllRefreshCableIDByID::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
            //connection.ExecuteNonQuery("update fuelhistory as fh inner join ordersdailyline as odl on fh.ID = odl.pumpHistoryID set fh.IsComplete = 1 where odl.pumpID <> 0 AND odl.orderID = " + order.OrderID);
        }
        public static int GetMaxID()
        {
            Connection.Connection conn = new Connection.Connection();
            conn.Open();
            int id = Convert.ToInt32(conn.ExecuteScalar("select max(ID) as id from fuelhistory"));
            conn.Close();
            return id;
        }
    }
}