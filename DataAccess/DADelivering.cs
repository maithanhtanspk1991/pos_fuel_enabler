﻿using System;
using System.Collections.Generic;
using DataObject;

namespace DataAccess
{
    public class DADelivering
    {
        private static string _nameClass = "DADelivering";

        public static List<Delivering> GetDelivering(SystemConfig.DBConfig mDBConfig)
        {
            List<Delivering> lsArray = new List<Delivering>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + Delivering._TableName + " where 1=1";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        Delivering item = new Delivering();
                        item = SetValue(row);
                        lsArray.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDelivering::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Insert(Delivering item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                result = InsertConnection(item, con);
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int InsertConnection(Delivering item, Connection.Connection con)
        {
            int result = -1;
            try
            {
                string sql = "";
                sql += "Insert Into " + DataObject.Delivering._TableName + " (type, coutItem, subTotal, orderID) Values ("
                    + "" + "" + item.Type + ""
                    + "," + "" + item.CoutItem + ""
                    + "," + "" + item.SubTotal + ""
                    + "," + "" + item.OrderID + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.Id = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                foreach (DeliveringLine line in item.DeliveringLine)
                {
                    line.DeliveringID = item.Id;
                    DADeliveringLine.InsertConnection(line, con);
                    //DataAccess.DAStockCheckItem.ExistItemConnection(line.ItemID, con);
                    //DAStockCheckItem.UpdateQtyOnHandConnection(line.ItemID, line.Qty, con);
                }
                result = item.Id;
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::InsertConnection::" + ex.Message);
            }
            return result;
        }

        private static Delivering SetValue(System.Data.DataRow row)
        {
            Delivering item = new Delivering();
            if (row != null)
            {
                item.Id = row["id"].ToString() != "" ? Convert.ToInt32(row["id"].ToString()) : 0;
                item.Date = row["ts"].ToString() != "" ? Convert.ToDateTime(row["ts"]) : DateTime.Now;
                item.Type = row["type"].ToString() != "" ? Convert.ToInt32(row["type"].ToString()) : 0;
                item.CoutItem = row["coutItem"].ToString() != "" ? Convert.ToInt32(row["coutItem"].ToString()) : 0;
                item.SubTotal = row["subTotal"].ToString() != "" ? Convert.ToDouble(row["subTotal"].ToString()) : 0;
                item.OrderID = row["orderID"].ToString() != "" ? Convert.ToInt32(row["orderID"].ToString()) : 0;
            }
            return item;
        }
    }
}