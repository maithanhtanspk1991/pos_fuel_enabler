﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAReceiveLine
    {
        private static string _nameClass = "DAReceiveLine";
        private static string Orderby = " Order by id ASC";

        private static DataObject.ReceiveLine SetValue(System.Data.DataRow row)
        {
            DataObject.ReceiveLine item = new DataObject.ReceiveLine();
            if (row != null)
            {
                item.ID = row["id"].ToString() != "" ? Convert.ToInt32(row["id"].ToString()) : 0;
                item.ReceiveID = row["receiveID"].ToString() != "" ? Convert.ToInt32(row["receiveID"].ToString()) : 0;
                item.ItemID = row["itemID"].ToString() != "" ? Convert.ToInt32(row["itemID"].ToString()) : 0;
                item.SellSize = row["SellSize"].ToString() != "" ? Convert.ToInt32(row["SellSize"].ToString()) : 0;
                item.Qty = row["qty"].ToString() != "" ? Convert.ToInt32(row["qty"].ToString()) : 0;
                item.Price = row["price"].ToString() != "" ? Convert.ToDouble(row["price"].ToString()) : 0;
                item.Total = row["total"].ToString() != "" ? Convert.ToDouble(row["total"].ToString()) : 0;
            }
            return item;
        }

        public static DataObject.ReceiveLine GetReceiveLineByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.ReceiveLine item = new DataObject.ReceiveLine();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + DataObject.ReceiveLine._TableName + " where id=" + iD);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetReceiveByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public static List<DataObject.ReceiveLine> GetReceiveLine(int receiveID, int itemID, SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.ReceiveLine> lsArray = new List<DataObject.ReceiveLine>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + DataObject.ReceiveLine._TableName + " " + DataObject.ReceiveLine._TableNameShort + " where 1=1";
                if (receiveID > -1)
                    sql += " And receiveID = " + receiveID;
                if (itemID > -1)
                    sql += " And itemID = " + itemID;
                sql += Orderby;
                sql += ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        if (row != null)
                        {
                            DataObject.ReceiveLine item = new DataObject.ReceiveLine();
                            item = SetValue(row);
                            lsArray.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetReceiveLine::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }
    }
}