﻿using System;
using System.Collections.Generic;
using DataObject;
using System.Data;

namespace DataAccess
{
    public class DAItemsMenu
    {
        private static string Orderby = " Order by displayOrder ASC";
        private static string _nameClass = "DAItemsMenu";

        public static List<ItemsMenu> GetAll(int groupID, string barcode, string itemname, int grTypeID, int grOption, int visual, int deleted, int IsFuel, int typeMenu, int sizeItem, SystemConfig.DBConfig mDBConfig)
        {
            return GetAll(groupID, barcode, itemname, grTypeID, grOption, visual, deleted, IsFuel, typeMenu, sizeItem, mDBConfig, new DataObject.Limit());
        }
        /// <summary>
        /// Hàm lấy Item
        /// </summary>
        /// <param name="groupID">Theo group, -1 là không xét</param>
        /// <param name="barcode"></param>
        /// <param name="itemname"></param>
        /// <param name="grTypeID"></param>
        /// <param name="grOption"></param>
        /// <param name="visual"></param>
        /// <param name="deleted"></param>
        /// <param name="mDBConfig"></param>
        /// <returns></returns>
        public static List<ItemsMenu> GetAll(int groupID, string barcode, string itemname, int grTypeID, int grOption, int visual, int deleted, int IsFuel, int typeMenu, int sizeItem, SystemConfig.DBConfig mDBConfig, DataObject.Limit limit)
        {
            List<ItemsMenu> lsArray = new List<ItemsMenu>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + ItemsMenu._TableName + " where 1=1";
                if (groupID > -1)
                    sql += " And groupID = " + groupID;
                if (barcode != "")
                    sql += " And scanCode like '%" + barcode + "%'";
                if (itemname != "")
                    sql += " And itemDesc like '%" + itemname + "%'";
                if (grTypeID > -1)
                    sql += " And grTypeID = " + grTypeID;
                if (grOption > -1)
                    sql += " And grOption = " + grOption;
                if (visual > -1)
                    sql += " And visual = " + visual;
                if (deleted > -1)
                    sql += " And deleted = " + deleted;
                if (IsFuel > -1)
                    sql += " And isFuel = " + IsFuel;
                if (typeMenu > -1)
                    sql += " And TypeMenu = " + typeMenu;
                if (sizeItem > -1)
                    sql += " And SizeItem = " + sizeItem;
                sql += Orderby;

                if (limit.IsLimit)
                {
                    DataAccess.DALimit.GetLimit(sql, limit, mDBConfig);
                    sql += limit.SqlLimit;
                }
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        lsArray.Add(SetValue(row));
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetItemsMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Delete(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "Update " + DataObject.ItemsMenu._TableName + " Set deleted = 1 Where itemID = " + item.ItemID;
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Delete::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int DeleteGroup(int groupid, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "Update " + DataObject.ItemsMenu._TableName + " Set deleted = 1 Where groupID = " + groupid;
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::DeleteGroup::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Insert(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Insert Into " + DataObject.ItemsMenu._TableName + " (scanCode, itemShort, itemDesc, unitPrice, groupID, bulkID, sellSize, displayOrder, sellType, printerType, visual, isFuel, bitmap, gst, SizeItem, TypeMenu) Values ("
                    + "" + "'" + item.Barcode + "'"
                    + "," + "'" + item.ItemShort + "'"
                    + "," + "'" + item.ItemDesc + "'"
                    + "," + "" + item.UnitPrice + ""
                    + "," + "" + item.GroupID + ""
                    + "," + "" + item.BulkID + ""
                    + "," + "" + item.SellSize + ""
                    + "," + "" + item.DisplayOrder + ""
                    + "," + "" + item.SellType + ""
                    + "," + "" + item.PrinterType + ""
                    + "," + "" + (item.Visual == true ? "1" : "0") + ""
                    + "," + "" + (item.IsFuel == true ? "1" : "0") + ""
                    + "," + "'" + item.Bitmap + "'"
                    + "," + "" + item.Gst + ""
                    + "," + "" + item.SizeItem + ""
                    + "," + "" + item.TypeMenu + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.ItemID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.ItemID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        /// <summary>
        /// Lay dyn ID dua vao ten
        /// </summary>
        /// <param name="item">dyn item</param>
        /// <param name="dbconfig"></param>
        /// <returns>rea ve dynid neu co gia tri 0 neu null</returns>
        public static int GetDynID(ItemsMenu item, SystemConfig.DBConfig dbconfig)
        {
            int result = 0;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select dynID from " + DataObject.ItemsMenu._TableNameDyn + " where itemShort='" + item.ItemShort + "' limit 0,1";
                object obj = con.ExecuteScalar(sql);
                if (obj != null)
                {
                    result = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::CheckDynItemName::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int InsertDynItem(ItemsMenu item, SystemConfig.DBConfig dbconfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(dbconfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Insert Into " + DataObject.ItemsMenu._TableNameDyn + " (itemShort, itemDesc, unitPrice, printerType, gst, cableID) Values ("
                    + "" + "'" + item.ItemShort + "'"
                    + "," + "'" + item.ItemDesc + "'"
                    + "," + "" + item.UnitPrice + ""
                    + "," + "" + item.PrinterType + ""
                    + "," + "" + item.Gst + ""
                    + "," + "" + item.CableID + ""
                    + ");";
                con.ExecuteNonQuery(sql);
                item.ItemID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = 1;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::InsertDynItem::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int UpdateBarcode(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.ItemsMenu._TableName + " Set "
                    + "scanCode = " + "'" + item.Barcode + "'"                    
                    + " Where itemID = " + item.ItemID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.ItemID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::UpdateBarcode::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Update(ItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.ItemsMenu._TableName + " Set "
                    + "scanCode = " + "'" + item.Barcode + "'"
                    + ",itemShort = " + "'" + item.ItemShort + "'"
                    + ",itemDesc = " + "'" + item.ItemDesc + "'"
                    + ",unitPrice = " + "" + item.UnitPrice + ""
                    + ",groupID = " + "" + item.GroupID + ""
                    + ",bulkID = " + "" + item.BulkID + ""
                    + ",sellSize = " + "" + item.SellSize + ""
                    + ",displayOrder = " + "" + item.DisplayOrder + ""
                    + ",sellType = " + "" + item.SellType + ""
                    + ",printerType = " + "" + item.PrinterType + ""
                    + ",enableFuelDiscount = " + "" + (item.EnableFuelDiscount == true ? "1" : "0") + ""
                    + ",visual = " + "" + (item.Visual == true ? "1" : "0") + ""
                    + ",isFuel = " + "" + (item.IsFuel == true ? "1" : "0") + ""
                    //+ ",bitmap = " + "'" + item.Bitmap + "'"
                    + ",gst =" + "" + item.Gst + ""
                    + ",SizeItem =" + "" + item.SizeItem + ""
                    + ",TypeMenu =" + "" + item.TypeMenu + ""
                    + " Where itemID = " + item.ItemID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.ItemID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        private static ItemsMenu SetValue(System.Data.DataRow row)
        {
            ItemsMenu item = new ItemsMenu();
            if (row != null)
            {
                item.ItemID = row["itemID"].ToString() != "" ? Convert.ToInt32(row["itemID"].ToString()) : 0;
                item.Barcode = row["scanCode"].ToString() != "" ? row["scanCode"].ToString() : "";
                item.ItemShort = row["itemShort"].ToString() != "" ? row["itemShort"].ToString() : "";
                item.ItemDesc = row["itemDesc"].ToString() != "" ? row["itemDesc"].ToString() : "";
                item.UnitPrice = row["unitPrice"].ToString() != "" ? Convert.ToDouble(row["unitPrice"].ToString()) : 0;
                item.GroupID = row["groupID"].ToString() != "" ? Convert.ToInt32(row["groupID"].ToString()) : 0;
                item.BulkID = row["bulkID"].ToString() != "" ? Convert.ToInt32(row["bulkID"].ToString()) : 0;
                item.SellSize = row["sellSize"].ToString() != "" ? Convert.ToInt32(row["sellSize"].ToString()) : 0;
                item.DisplayOrder = row["displayOrder"].ToString() != "" ? Convert.ToDouble(row["displayOrder"].ToString()) : 0;
                item.SellType = row["sellType"].ToString() != "" ? Convert.ToInt32(row["sellType"].ToString()) : 0;
                item.PrinterType = row["printerType"].ToString() != "" ? Convert.ToInt32(row["printerType"].ToString()) : 0;
                item.EnableFuelDiscount = row["enableFuelDiscount"].ToString() != "" ? Convert.ToBoolean(row["enableFuelDiscount"]) : false;
                item.Visual = row["visual"].ToString() != "" ? Convert.ToBoolean(row["visual"]) : true;
                item.IsFuel = row["isFuel"].ToString() != "" ? Convert.ToBoolean(row["isFuel"]) : false;
                item.Deleted = row["deleted"].ToString() != "" ? Convert.ToBoolean(row["deleted"]) : true;
                item.Gst = row["gst"].ToString() != "" ? Convert.ToInt32(row["gst"].ToString()) : 0;
                item.Bitmap = row["bitmap"].ToString() != "" ? row["bitmap"].ToString() : "";
                item.SizeItem = row["SizeItem"].ToString() != "" ? Convert.ToInt32(row["SizeItem"].ToString()) : 0;
                item.TypeMenu = row["TypeMenu"].ToString() != "" ? Convert.ToInt32(row["TypeMenu"].ToString()) : 0;
            }
            return item;
        }

        public static ItemsMenu GetItemMenuByID(int itemID, SystemConfig.DBConfig mDBConfig)
        {
            ItemsMenu item = new ItemsMenu();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from itemsmenu where itemID=" + itemID);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                item = new ItemsMenu();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetItemMenuByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public static ItemsMenu GetItemMenuByBarcode(string Barcode, int deleted, SystemConfig.DBConfig mDBConfig)
        {
            ItemsMenu item = new ItemsMenu();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "select * from itemsmenu where scanCode='" + Barcode + "'";

                if (deleted > -1)
                    sql += " And deleted = " + deleted;
                sql += ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                item = new ItemsMenu();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetItemMenuByBarcode::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public static int RefreshDisplayOrder(int GroupID, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "CALL SP_ItemMenu_Refresh_DisplayOrder(" + GroupID.ToString() + ");";
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::RefreshDisplayOrder::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static double GetDisplayOrder(int ShowDisplay, int groupID, SystemConfig.DBConfig mDBConfig)
        {
            double result = 0;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = String.Format("SELECT displayOrder FROM " + DataObject.ItemsMenu._TableName + " Where deleted = 0 And groupID = {1} Order by displayOrder ASC Limit {0},1;", ShowDisplay - 2, groupID);
                result = Convert.ToDouble(con.ExecuteScalar(sql));
            }
            catch (Exception ex)
            {
                result = 0;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDisplayOrder::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static DataObject.ItemsMenu GetBarcodeItemsMenu(string ScanCode, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.ItemsMenu item = new DataObject.ItemsMenu();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + DataObject.ItemsMenu._TableName + " where deleted = 0 And scanCode = " + ScanCode);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetBarcodeItemsMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }

        public static List<DataObject.SellTypeObject> GetListSellType(SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.SellTypeObject> lsArray = new List<DataObject.SellTypeObject>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "SELECT * FROM selltype s";
                DataTable dt = con.Select(sql);
                //sellTypeID, name, defaultSellSize4
                foreach (DataRow row in dt.Rows)
                {
                    DataObject.SellTypeObject _SellType = new DataObject.SellTypeObject(row["sellTypeID"].ToString(), row["name"].ToString(), Convert.ToInt32(row["defaultSellSize"].ToString()));
                    lsArray.Add(_SellType);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetListSellType::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }
    }
}