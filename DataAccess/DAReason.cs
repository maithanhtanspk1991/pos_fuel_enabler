﻿using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccess
{
    public class DAReason
    {
        private static Connection.Connection mcon;

        public static List<DataObject.Reason> GetAllReason(SystemConfig.DBConfig dbconfig)
        {
            List<DataObject.Reason> lst = new List<DataObject.Reason>();
            try
            {
                mcon = new Connection.Connection();
                mcon.Open();
                string sql = "Select * from stockreason";

                DataTable datatable = mcon.Select(sql);
                foreach (DataRow row in datatable.Rows)
                {
                    DataObject.Reason reasonitem = new DataObject.Reason();
                    reasonitem.ID = Convert.ToInt32(row["reasonID"].ToString());
                    reasonitem.strReason = row["reason"].ToString();
                    reasonitem.Negative = Convert.ToInt32(row["Negative"].ToString());
                    lst.Add(reasonitem);
                }
            }
            catch
            {
            }
            finally
            {
                mcon.Close();
            }
            return lst;
        }
    }
}