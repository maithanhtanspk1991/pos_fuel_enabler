﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAStockCheckItem
    {
        private static string _nameClass = "DAStockCheckItem";
        private static Connection.Connection con;

        public static int ExistItem(int ItemID, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                result = ExistItemConnection(ItemID, con);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::ExistItem::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int ExistItemConnection(int ItemID, Connection.Connection con)
        {
            int result = -1;
            try
            {
                string sql = "";
                sql = String.Format("select ItemID FROM stocklevel Where ItemID = {0};", ItemID);
                if (!(Convert.ToInt32(con.ExecuteScalar(sql)) > 0))
                {
                    sql = String.Format("Insert Into stocklevel(ItemID, QtyOnHand, QtyReceived, QtyAdjust, QtyRefund, QtyAdd) Values({0},{1},{2},{3},{4},{5})", ItemID, 0, 0, 0, 0, 0);
                    result = con.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::ExistItemConnection::" + ex.Message);
            }
            return result;
        }

        public static DataObject.StockItemCheck GetItemCheckFormItemMenu(DataObject.ItemsMenu itemmenu, SystemConfig.DBConfig dbconfig)
        {
            DataObject.StockItemCheck stockitem = new DataObject.StockItemCheck();
            con = new Connection.Connection();
            try
            {
                con.Open();
                string sql = "select sl.* from stocklevel sl where sl.itemID =" + itemmenu.ItemID;
                System.Data.DataTable dt = con.Select(sql);
                //foreach (System.Data.DataRow dr in dt.Rows)
                //{
                //    DataObject.StockItemCheck stockitem = SetValue(dr);
                //    lst.Add(stockitem);
                //}
                if (dt.Rows.Count > 0)
                {
                    stockitem = SetValue(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::GetItemCheckFormItemMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return stockitem;
        }

        public static int GetQtyOnHand(int ItemID, SystemConfig.DBConfig mDBConfig)
        {
            int result = 0;
            DataObject.StockItemCheck stockitem = new DataObject.StockItemCheck();
            con = new Connection.Connection();
            try
            {
                con.Open();
                string sql = "SELECT QtyOnHand FROM stocklevel Where  ItemID = " + ItemID + " ;";
                result = Convert.ToInt32(con.ExecuteScalar(sql));
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::GetItemCheckFormItemMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Insert(SystemConfig.DBConfig dbconfig, List<DataObject.StockItemCheck> lst, DataObject.Transit transit)
        {
            con = new Connection.Connection(dbconfig);
            int kq = 0;
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "insert into stockcheck(`StaffID`, `CableID`)values(" + transit.StaffID + "," + transit.CableID + ")";
                kq = con.ExecuteNonQuery(sql);
                sql = "Select LAST_INSERT_ID()";
                int id = Convert.ToInt32(con.ExecuteScalar(sql));
                if (kq > 0)
                {
                    foreach (var item in lst)
                    {
                        sql = "insert into stockcheckline(`StockCheckID`, `ItemID`, `Qty`, `ReasonID`, `Note`) values(" +
                            "," + id.ToString() + "" +
                            "," + item.itemID + "" +
                            "," + item.QtyChange + "" +
                            "," + item.TypeReason + "" +
                            ",'" + item.Note + "'" +
                            ")";
                        kq += con.ExecuteNonQuery(sql);
                        sql = "select sl.* from stocklevel sl where sl.itemID =" + item.itemID.ToString();
                        System.Data.DataTable dt = con.Select(sql);
                        switch (item.TypeReason)
                        {
                            case 0:
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        sql = "Update stocklevel set QtyOnHand+=" + item.Qty + ", QtyLost +=" + item.Qty + " where itemID=" + item.itemID;
                                        //sql = "Update stocklevel set QtyOnHand+=" + item.Qty + ", QtyLost +=" + item.Qty;
                                    }
                                    else
                                    {
                                        //sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyReceived, QtyAdjust, QtyRefund, QtyAdd) values("+
                                        sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyLost) values(" +
                                            "" + item.itemID + "" +
                                            "," + item.QtyInHand + "" +
                                            "," + item.Qty + ")"
                                            ;
                                    }
                                    kq += con.ExecuteNonQuery(sql);
                                    break;
                                }
                            case 1:
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        sql = "Update stocklevel set QtyOnHand+=" + item.Qty + ", QtyAdd +=" + item.Qty + " where itemID=" + item.itemID;
                                    }
                                    else
                                    {
                                        //sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyReceived, QtyAdjust, QtyRefund, QtyAdd) values("+
                                        sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyAdd) values(" +
                                            "" + item.itemID + "" +
                                            "," + item.QtyInHand + "" +
                                            "," + item.Qty + ")"
                                            ;
                                    }
                                    kq += con.ExecuteNonQuery(sql);
                                    break;
                                }
                            case 2:
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        sql = "Update stocklevel set QtyOnHand+=" + item.Qty + ", QtyRefund +=" + item.Qty + " where itemID=" + item.itemID;
                                    }
                                    else
                                    {
                                        //sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyReceived, QtyAdjust, QtyRefund, QtyAdd) values("+
                                        sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyRefund) values(" +
                                            "" + item.itemID + "" +
                                            "," + item.QtyInHand + "" +
                                            "," + item.Qty + ")"
                                            ;
                                    }
                                    kq += con.ExecuteNonQuery(sql);
                                    break;
                                }
                            case 3:
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        sql = "Update stocklevel set QtyOnHand+=" + item.Qty + ", QtyAdjust +=" + item.Qty + " where itemID=" + item.itemID;
                                    }
                                    else
                                    {
                                        //sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyReceived, QtyAdjust, QtyRefund, QtyAdd) values("+
                                        sql = "Insert into stocklevel (ItemID, QtyOnHand, QtyAdjust) values(" +
                                            "" + item.itemID + "" +
                                            "," + item.QtyInHand + "" +
                                            "," + item.Qty + ")"
                                            ;
                                    }
                                    kq += con.ExecuteNonQuery(sql);
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::DAStockCheckItem::Insert" + ex.Message);
            }
            return kq;
        }

        public static int UpdateQtyOnHand(int ItemID, int QtyOnHand, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                result = UpdateQtyOnHandConnection(ItemID, QtyOnHand, con);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::UpdateQtyOnHand::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int UpdateQtyOnHandConnection(int ItemID, int QtyOnHand, Connection.Connection con)
        {
            int result = -1;
            try
            {
                string sql = "Update " + DataObject.StockItemCheck._TableName + " Set QtyOnHand = QtyOnHand - " + QtyOnHand + " Where itemID = " + ItemID;
                result = con.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::UpdateQtyOnHandConnection::" + ex.Message);
            }
            return result;
        }

        private static DataObject.StockItemCheck SetValue(System.Data.DataRow dr)
        {
            DataObject.StockItemCheck item = new DataObject.StockItemCheck();
            //item.itemID = Convert.ToInt32(dr["ID"].ToString());
            item.QtyInHand = Convert.ToInt32(dr["qtyonhand"].ToString());
            return item;
        }
    }
}