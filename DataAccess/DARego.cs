﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class DARego
    {
        private static string _nameClass = "POS::Class::DARego::";
        private static Connection.Connection conn = new Connection.Connection();
        private static string Orderby = " Order by CarNumber ASC";

        private static DataObject.Rego SetValue(System.Data.DataRow row)
        {
            DataObject.Rego item = new DataObject.Rego();
            try
            {
                //id, custID, CarNumber, CarColor, CarProduct, deleted
                item.ID = row["id"].ToString() != "" ? Convert.ToInt32(row["id"]) : 0;
                item.CustID = row["custID"].ToString() != "" ? Convert.ToInt32(row["custID"]) : 0;
                item.CarNumber = row["CarNumber"].ToString();
                item.CarColor = row["CarColor"].ToString();
                item.CarProduct = row["CarProduct"].ToString();
                item.Deleted = row["deleted"].ToString() != "" ? Convert.ToBoolean(row["deleted"]) : false;
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }

        public static int Update(DataObject.Rego item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                //id, custID, CarNumber, CarColor, CarProduct, deleted
                sql += "Update managecar set "
                    + " custID = " + "" + item.CustID + ""
                    + ", CarNumber = " + "'" + item.CarNumber + "'"
                    + ", CarColor = " + "'" + item.CarColor + "'"
                    + ", CarProduct = " + "'" + item.CarProduct + "'"
                    + ", deleted = " + "" + (item.Deleted == true ? "1" : "0") + ""
                    + " Where id = " + item.ID
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.ID;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Insert(DataObject.Rego item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Insert Into managecar (custID, CarNumber, CarColor, CarProduct, deleted) Values ("
                    + "" + "" + item.CustID + ""
                    + "," + "'" + item.CarNumber + "'"
                    + "," + "'" + item.CarColor + "'"
                    + "," + "'" + item.CarProduct + "'"
                    + "," + "" + (item.Deleted == true ? "1" : "0") + ""
                    + ");";
                result = conn.ExecuteNonQuery(sql);
                item.ID = Convert.ToInt32(conn.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.ID;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static List<DataObject.Rego> GetAll(int CustID)
        {
            List<DataObject.Rego> lsArray = new List<DataObject.Rego>();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM managecar m Where 1 = 1 And m.deleted = 0";
                if (CustID > -1)
                    sql += " And custID = " + CustID + "";
                sql += Orderby;
                sql += ";";
                System.Data.DataTable dt = conn.Select(sql);
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    lsArray.Add(SetValue(row));
                }

            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetAll::" + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }

        public static DataObject.Rego GetByID(int ID)
        {
            DataObject.Rego item = new DataObject.Rego();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM managecar m Where id = " + ID;

                sql += ";";
                System.Data.DataTable dt = conn.Select(sql);
                if (dt.Rows.Count > 0)
                    item = SetValue(dt.Rows[0]);

            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetByID::" + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return item;
        }
    }
}
