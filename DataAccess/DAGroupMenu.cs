﻿using System;
using System.Collections.Generic;
using DataObject;
using System.Data;

namespace DataAccess
{
    public class DAGroupMenu
    {
        private static string _nameClass = "DAGroupMenu";
        private static string Orderby = " Order by displayOrder ASC";
        private static string Des = "g";
        private static string ExistsItem = " And  Exists (Select itemID from itemsmenu i Where i.deleted = 0 And i.visual = 1 And i.isFuel = 0 And i.groupID = " + Des + ".groupID)";

        private static GroupMenu SetValue(System.Data.DataRow row)
        {
            GroupMenu gm = new GroupMenu();
            if (row != null)
            {
                gm.GroupID = row["groupID"].ToString() != "" ? Convert.ToInt32(row["groupID"].ToString()) : 0;
                gm.GroupShort = row["groupShort"].ToString() != "" ? row["groupShort"].ToString() : "";
                gm.GroupDesc = row["groupDesc"].ToString() != "" ? row["groupDesc"].ToString() : "";
                gm.GrTypeID = row["grTypeID"].ToString() != "" ? Convert.ToInt32(row["grTypeID"].ToString()) : 0;
                gm.GrShortCut = row["grShortCut"].ToString() != "" ? Convert.ToInt32(row["grShortCut"].ToString()) : 0;
                gm.GrOption = row["grOption"].ToString() != "" ? Convert.ToInt32(row["grOption"].ToString()) : 0;
                gm.DisplayOrder = row["displayOrder"].ToString() != "" ? Convert.ToDouble(row["displayOrder"].ToString()) : 0;
                gm.Visual = row["visual"].ToString() != "" ? Convert.ToBoolean(row["visual"]) : false;
                gm.Deleted = row["deleted"].ToString() != "" ? Convert.ToBoolean(row["deleted"]) : false;
                gm.Bitmap = row["bitmap"].ToString() != "" ? row["bitmap"].ToString() : "";
            }
            return gm;
        }

        /// <summary>
        /// Get Group Menu By ID
        /// </summary>
        /// <param name="iD">ID</param>
        /// <returns></returns>
        public static GroupMenu GetGroupMenuByID(int iD, SystemConfig.DBConfig mDBConfig)
        {
            GroupMenu GroupMenu = new GroupMenu();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + GroupMenu._TableName + " where groupID=" + iD);
                if (tbl.Rows.Count > 0)
                {
                    GroupMenu = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetGroupMenuByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return GroupMenu;
        }

        /// <summary>
        /// Get group menu, mac dinh -1 la khong xet
        /// </summary>
        /// <param name="grTypeID"></param>
        /// <param name="grShortCut">Giống như gom nhóm lại</param>
        /// <param name="grOption"></param>
        /// <param name="visual">Có thể hiện trên menu hay không</param>
        /// <param name="deleted">Đã bị xóa</param>
        /// <returns></returns>
        public static List<GroupMenu> GetGroupMenu(int grTypeID, int grShortCut, int grOption, int visual, int deleted, bool existsItem, SystemConfig.DBConfig mDBConfig)
        {
            DataTable tbl = new DataTable();
            List<GroupMenu> lsArray = new List<GroupMenu>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + GroupMenu._TableName + " " + Des + " where 1=1";
                if (grTypeID > -1)
                    sql += " And grTypeID = " + grTypeID;
                if (grShortCut > -1)
                    sql += " And grShortCut = " + grShortCut;
                if (grOption > -1)
                    sql += " And grOption = " + grOption;
                if (visual > -1)
                    sql += " And visual = " + visual;
                if (deleted > -1)
                    sql += " And deleted = " + deleted;
                if (existsItem)
                    sql += ExistsItem;
                sql += Orderby;
                sql += ";";
                tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        if (row != null)
                        {
                            GroupMenu gm = new GroupMenu();
                            gm = SetValue(row);
                            lsArray.Add(gm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetGroupMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Delete(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "Update " + DataObject.GroupMenu._TableName + " Set deleted = 1 Where groupID = " + item.GroupID;
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Delete::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static double GetDisplayOrder(int ShowDisplay, SystemConfig.DBConfig mDBConfig)
        {
            double result = 0;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = String.Format("SELECT displayOrder FROM " + DataObject.GroupMenu._TableName + " Where deleted = 0 Order by displayOrder ASC Limit {0},1;", ShowDisplay - 2);
                result = Convert.ToDouble(con.ExecuteScalar(sql));
            }
            catch (Exception ex)
            {
                result = 0;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDisplayOrder::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Insert(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Insert Into " + DataObject.GroupMenu._TableName + " (groupShort, groupDesc, grTypeID, grShortCut, grOption, displayOrder, visual, bitmap) Values ("
                    + "" + "'" + item.GroupShort + "'"
                    + "," + "'" + item.GroupDesc + "'"
                    + "," + "" + item.GrTypeID + ""
                    + "," + "" + item.GrShortCut + ""
                    + "," + "" + item.GrOption + ""
                    + "," + "" + item.DisplayOrder + ""
                    + "," + "" + (item.Visual == true ? "1" : "0") + ""
                    + "," + "'" + item.Bitmap + "'"
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.GroupID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.GroupID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Update(GroupMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.GroupMenu._TableName + " Set "
                    + "groupShort = " + "'" + item.GroupShort + "'"
                    + ",groupDesc = " + "'" + item.GroupDesc + "'"
                    + ",grTypeID = " + "" + item.GrTypeID + ""
                    + ",grShortCut = " + "" + item.GrShortCut + ""
                    + ",grOption = " + "" + item.GrOption + ""
                    + ",displayOrder = " + "" + item.DisplayOrder + ""
                    + ",visual = " + "" + (item.Visual == true ? "1" : "0") + ""
                    //+ ",bitmap = " + "'" + item.Bitmap + "'"
                    + " Where groupID = " + item.GroupID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.GroupID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int RefreshDisplayOrder(SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "CALL SP_GroupMenu_Refresh_DisplayOrder();";
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::RefreshDisplayOrder::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }
    }
}