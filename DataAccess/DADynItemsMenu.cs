﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class DADynItemsMenu
    {
        private static string _nameClass = "DAItemsMenu";

        /// <summary>
        /// Hàm lấy Item
        /// </summary>
        /// <param name="groupID">Theo group, -1 là không xét</param>
        /// <param name="barcode"></param>
        /// <param name="itemname"></param>
        /// <param name="grTypeID"></param>
        /// <param name="grOption"></param>
        /// <param name="visual"></param>
        /// <param name="deleted"></param>
        /// <param name="mDBConfig"></param>
        /// <returns></returns>
        public static List<DataObject.DynItemsMenu> GetDynItemsMenu(SystemConfig.DBConfig mDBConfig)
        {
            List<DataObject.DynItemsMenu> lsArray = new List<DataObject.DynItemsMenu>();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                string sql = "";
                sql = "select * from " + DataObject.DynItemsMenu._TableName + " where 1=1";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        DataObject.DynItemsMenu item = new DataObject.DynItemsMenu();
                        item = SetValue(row);
                        lsArray.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                con.Rollback();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDynItemsMenu::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }

        public static int Delete(DataObject.DynItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "Update " + DataObject.DynItemsMenu._TableName + " Set deleted = 1 Where itemID = " + item.DynID;
                result = con.ExecuteNonQuery(sql);
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Delete::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Insert(DataObject.DynItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Insert Into " + DataObject.DynItemsMenu._TableName + " (itemShort, itemDesc, unitPrice, printerType, gst, cableID) Values ("
                    + "," + "'" + item.ItemShort + "'"
                    + "," + "'" + item.ItemDesc + "'"
                    + "," + "" + item.UnitPrice + ""
                    + "," + "" + item.PrinterType + ""
                    + "," + "" + item.GST + ""
                    + "," + "" + item.CableID + ""
                    + ");";
                result = con.ExecuteNonQuery(sql);
                item.DynID = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.DynID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static int Update(DataObject.DynItemsMenu item, SystemConfig.DBConfig mDBConfig)
        {
            int result = -1;
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                con.BeginTransaction();
                string sql = "";
                sql += "Update " + DataObject.DynItemsMenu._TableName + " Set "
                    + "itemShort = " + "'" + item.ItemShort + "'"
                    + ",itemDesc = " + "'" + item.ItemDesc + "'"
                    + ",unitPrice = " + "" + item.UnitPrice + ""
                    + ",printerType = " + "" + item.PrinterType + ""
                    + ",gst = " + "" + item.GST + ""
                    + ",cableID = " + "" + item.CableID + ""
                    + " Where itemID = " + item.DynID
                    + ";";
                result = con.ExecuteNonQuery(sql);
                result = item.DynID;
                con.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        private static DataObject.DynItemsMenu SetValue(System.Data.DataRow row)
        {
            DataObject.DynItemsMenu item = new DataObject.DynItemsMenu();
            if (row != null)
            {
                item.DynID = row["dynID"].ToString() != "" ? Convert.ToInt32(row["dynID"].ToString()) : 0;
                item.ItemShort = row["itemShort"].ToString() != "" ? row["itemShort"].ToString() : "";
                item.ItemDesc = row["itemDesc"].ToString() != "" ? row["itemDesc"].ToString() : "";
                item.UnitPrice = row["unitPrice"].ToString() != "" ? Convert.ToDouble(row["unitPrice"].ToString()) : 0;
                item.PrinterType = row["printerType"].ToString() != "" ? Convert.ToInt32(row["printerType"].ToString()) : 0;
                item.Date = row["ts"].ToString() != "" ? Convert.ToDateTime(row["ts"]) : DateTime.Now;
                item.GST = row["gst"].ToString() != "" ? Convert.ToInt32(row["gst"].ToString()) : 0;
                item.CableID = row["cableID"].ToString() != "" ? Convert.ToInt32(row["cableID"].ToString()) : 0;
            }
            return item;
        }

        public static DataObject.DynItemsMenu GetDynItemsMenuByID(int dynID, SystemConfig.DBConfig mDBConfig)
        {
            DataObject.DynItemsMenu item = new DataObject.DynItemsMenu();
            Connection.Connection con = new Connection.Connection(mDBConfig);
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from " + DataObject.DynItemsMenu._TableName + " where dynID=" + dynID);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                item = new DataObject.DynItemsMenu();
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::GetDynItemsMenuByID::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return item;
        }
    }
}