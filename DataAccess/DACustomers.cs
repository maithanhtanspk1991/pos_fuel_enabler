﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class DACustomers
    {
        private static string _nameClass = "POS::Class::DACustomers::";
        private static Connection.Connection conn = new Connection.Connection();
        private static string mOrderBy = " Order by FirstName ASC";

        private static DataObject.Customers SetValue(System.Data.DataRow row)
        {
            DataObject.Customers item = new DataObject.Customers();
            try
            {
                item.CustID = row["custID"].ToString() != "" ? Convert.ToInt32(row["custID"]) : 0;
                item.Company = row["Company"].ToString() != "" ? Convert.ToInt32(row["Company"]) : 0;
                item.LastName = row["Name"].ToString();
                item.FirstName = row["FirstName"].ToString();
                item.StreetNo = row["streetNo"].ToString();
                item.StreetName = row["streetName"].ToString();
                item.PostCode = row["postCode"].ToString();
                item.Phone = row["phone"].ToString();
                item.Mobile = row["mobile"].ToString();
                item.CardNo = row["cardNo"].ToString();
                item.Email = row["email"].ToString();
                item.MemberNo = row["memberNo"].ToString();
                item.CarNumber = row["carNumber"].ToString();
                item.Accountlimit = row["accountlimit"].ToString() != "" ? Convert.ToInt32(row["accountlimit"]) : 0;
                item.Balance = row["debt"].ToString() != "" ? Convert.ToInt32(row["debt"]) : 0;
                item.Deleted = row["Deleted"].ToString() != "" ? Convert.ToBoolean(row["Deleted"]) : false;
                item.IsActive = row["IsActive"].ToString() != "" ? Convert.ToBoolean(row["IsActive"]) : false;
                item.IsDelivery = row["IsDelivery"].ToString() != "" ? Convert.ToBoolean(row["IsDelivery"]) : false;
                item.IsPickup = row["IsPickup"].ToString() != "" ? Convert.ToBoolean(row["IsPickup"]) : false;
                item.IsLock = row["IsLock"].ToString() != "" ? Convert.ToBoolean(row["IsLock"]) : false;
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }

        public static DataObject.Customers GetBy(int CustID, string MemberNo, int isActive, int deleted)
        {
            DataObject.Customers item = new DataObject.Customers();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM customers c Where 1 = 1";
                if (CustID > -1)
                    sql += " And CustID = " + CustID;
                if (MemberNo != "")
                    sql += " And MemberNo = '" + MemberNo + "'";
                if (isActive > -1)
                    sql += " And IsActive = " + isActive;
                if (deleted > -1)
                    sql += " And Deleted = " + deleted;
                sql += " Limit 0,1";
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    item = SetValue(tbl.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetBy::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return item;
        }

        public static List<DataObject.Customers> GetAll(string name, string firstName, string phone, string mobile, string email, string memberNo, string cardNo, string postCode, string streetNo, string streetName, int isActive, int deleted, int company, string orderBy, DataObject.Limit limit)
        {
            List<DataObject.Customers> lsArray = new List<DataObject.Customers>();
            try
            {
                conn.Open();
                string sql = "SELECT c.*, co.companyName FROM customers c Left Join company co on co.idCompany = c.Company Where 1 = 1";
                if (name != "")
                    sql += " And name like '%" + name + "%'";
                if (firstName != "")
                    sql += " And firstName like '%" + firstName + "%'";
                if (phone != "")
                    sql += " And phone like '%" + phone + "%'";
                if (mobile != "")
                    sql += " And mobile like '%" + mobile + "%'";
                if (email != "")
                    sql += " And email like '%" + email + "%'";
                if (memberNo != "")
                    sql += " And memberNo like '%" + memberNo + "%'";
                if (cardNo != "")
                    sql += " And cardNo like '%" + cardNo + "%'";
                if (postCode != "")
                    sql += " And postCode like '%" + postCode + "%'";
                if (streetNo != "")
                    sql += " And streetNo like '%" + streetNo + "%'";
                if (streetName != "")
                    sql += " And streetName like '%" + streetName + "%'";
                if (isActive > -1)
                    sql += " And IsActive = " + isActive;
                if (deleted > -1)
                    sql += " And c.Deleted = " + deleted;
                if (company > -1)
                    sql += " And c.Company = " + company;
                if (orderBy != "")
                    mOrderBy = orderBy;
                sql += mOrderBy;

                if (limit.IsLimit)
                {
                    DataAccess.DALimit.GetLimit(sql, limit);
                    sql += limit.SqlLimit;
                }
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        lsArray.Add(SetValue(row));
                    }
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "GetAll::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }

        public static int Insert(DataObject.Customers item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Insert Into customers (Name, Company, streetNo, streetName, postCode, phone, mobile, cardNo, email, accountlimit, debt, memberNo, carNumber, IsActive, IsDelivery, IsPickup, FirstName, IsLock, Deleted) Values ("
                    + "" + "'" + item.LastName + "'"
                    + "," + "" + item.Company + ""
                    + "," + "'" + item.StreetNo + "'"
                    + "," + "'" + item.StreetName + "'"
                    + "," + "'" + item.PostCode + "'"
                    + "," + "'" + item.Phone + "'"
                    + "," + "'" + item.Mobile + "'"
                    + "," + "'" + item.CardNo + "'"
                    + "," + "'" + item.Email.ToLower() + "'"
                    + "," + "" + item.Accountlimit + ""
                    + "," + "" + item.Balance + ""
                    + "," + "'" + item.MemberNo + "'"
                    + "," + "'" + item.CarNumber + "'"
                    + "," + "" + (item.IsActive == true ? "1" : "0") + ""
                    + "," + "" + (item.IsDelivery == true ? "1" : "0") + ""
                    + "," + "" + (item.IsPickup == true ? "1" : "0") + ""
                    + "," + "'" + item.FirstName + "'"
                    + "," + "" + (item.IsLock == true ? "1" : "0") + ""
                    + "," + "" + (item.Deleted == true ? "1" : "0") + ""
                    + ");";
                result = conn.ExecuteNonQuery(sql);
                item.CustID = Convert.ToInt32(conn.ExecuteScalar("select LAST_INSERT_ID()"));
                result = item.CustID;
                if (item.RegoItem != null)
                    foreach (DataObject.Rego r in item.RegoItem)
                    {
                        r.CustID = item.CustID;
                        DARego.Insert(r);
                    }
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Insert::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Update(DataObject.Customers item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Update customers Set "
                    + " Name = " + "'" + item.LastName + "'"
                    + " ,Company = " + "" + item.Company + ""
                    + " ,firstName = " + "'" + item.FirstName + "'"
                    + " ,streetNo = " + "'" + item.StreetNo + "'"
                    + " ,streetName = " + "'" + item.StreetName + "'"
                    + " ,postCode = " + "'" + item.PostCode + "'"
                    + " ,phone = " + "'" + item.Phone + "'"
                    + " ,mobile = " + "'" + item.Mobile + "'"
                    + " ,cardNo = " + "'" + item.CardNo + "'"
                    + " ,email = " + "'" + item.Email.ToLower() + "'"
                    + " ,accountlimit = " + "" + item.Accountlimit + ""
                    + " ,debt = " + "" + item.Balance + ""
                    + " ,IsActive = " + "" + (item.IsActive ? "1" : "0") + ""
                    + " ,IsDelivery = " + "" + (item.IsDelivery ? "1" : "0") + ""
                    + " ,IsPickup = " + "" + (item.IsPickup ? "1" : "0") + ""
                    + " ,IsLock = " + "" + (item.IsLock ? "1" : "0") + ""
                    + " Where CustID = " + item.CustID
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.CustID;
                conn.Commit();
                //custID, Name, Company, streetNo, streetName, postCode, phone, mobile, cardNo, suburb, email, accountlimit, debt, memberNo, carNumber, IsActive, IsDelivery, IsPickup, FirstName, IsLock, Deleted
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Update::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int Deleted(DataObject.Customers item)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();
                string sql = "";
                sql += "Update customers Set "
                    + " deleted = " + "" + (item.Deleted ? "1" : "0") + ""
                    + " Where CustID = " + item.CustID
                    + ";";
                result = conn.ExecuteNonQuery(sql);
                result = item.CustID;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Deleted::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int DeletedByConpanyID(int ConpanyID)
        {
            int result = -1;
            try
            {
                conn.Open();
                conn.BeginTransaction();

                string sql = "Update company Set "
                    + " deleted = 1" + ""
                    + " Where idCompany = " + ConpanyID
                    + ";";
                result = conn.ExecuteNonQuery(sql);

                result = ConpanyID;
                conn.Commit();
            }
            catch (Exception ex)
            {
                result = -1;
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Deleted::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static List<DataObject.Customers> Search(string regoNumber, string nameCus, string phoneNumber, int isActive, int deleted, DataObject.Limit limit)
        {
            List<DataObject.Customers> lsArray = new List<DataObject.Customers>();
            try
            {
                conn.Open();
                string sql = "select c.custID,m.id, c.Name,m.carNumber,c.Mobile,c.streetNo,c.streetName,c.FirstName from customers c,managecar m where 1=1 and c.custID=m.custID And m.deleted = 0";
                if (regoNumber != "")
                {
                    sql += " And m.carNumber like '%" + regoNumber + "%'";
                }
                if (nameCus != "")
                {
                    sql += " And c.FirstName like '%" + nameCus + "%'";
                }
                if (phoneNumber != "")
                {
                    sql += " And c.mobile like '%" + phoneNumber + "%'";
                }
                if (isActive > -1)
                    sql += " And IsActive = " + isActive;
                if (deleted > -1)
                    sql += " And c.Deleted = " + deleted;
                sql += mOrderBy;

                if (limit.IsLimit)
                {
                    DataAccess.DALimit.GetLimit(sql, limit);
                    sql += limit.SqlLimit;
                }
                sql += ";";
                System.Data.DataTable tbl = conn.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        lsArray.Add(SetValueSearch(row));
                    }
                }
            }

            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("DataAccess::" + _nameClass + "::Search::" + ex.Message);
                conn.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return lsArray;
        }

        private static DataObject.Customers SetValueSearch(System.Data.DataRow row)
        {
            DataObject.Customers item = new DataObject.Customers();
            try
            {
                item.CustID = Convert.ToInt32(row["custID"].ToString());
                item.RegoID = Convert.ToInt32(row["id"].ToString());
                item.CarNumber = row["carNumber"].ToString();
                item.FirstName = row["Name"].ToString();
                item.Mobile = row["Mobile"].ToString();
                item.StreetNo = row["StreetNo"].ToString();
                item.StreetName = row["StreetName"].ToString();
                item.LastName = row["FirstName"].ToString();
            }
            catch (Exception)
            {

            }
            return item;
        }

    }
}
