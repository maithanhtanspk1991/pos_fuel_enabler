﻿namespace Connection
{
    internal class Parameter
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public System.Data.DbType DBtype { get; set; }

        public Parameter(string name, string value, System.Data.DbType dbtype)
        {
            Name = name;
            Value = value;
            DBtype = dbtype;
        }
    }
}