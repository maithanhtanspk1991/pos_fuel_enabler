using System;

namespace Connection
{
    public class ReadDBConfig
    {
        private clsReadAndWriteINI ini;
        private string strServer;
        private string strHour;
        private string strCOM;
        private string strSecond;
        private string strMinute;
        private string strDatabase;
        private string strUserID;
        private string strUserPass;
        private string strLocalMode;
        private string strKitchenPrinter;
        private string strBarPrinter;
        private string strBillPrinter;
        private string strCableID;
        private string strHeader1;
        private string strHeader2;
        private string strHeader3;
        private string strHeader4;
        private string strHeader5;
        private string strFootNode1;
        private string strFootNode2;
        private int intAllowPrint;
        private int intAllowSalesFuel;
        private int intAllowSalesFastFood;
        private double dblDeliveryAmount;

        public double DeliveryAmount
        {
            get { return dblDeliveryAmount; }
            set { dblDeliveryAmount = value; }
        }

        public string LocalMode
        {
            get { return strLocalMode; }
            set { strLocalMode = value; }
        }

        public string UserPass
        {
            get { return strUserPass; }
            set { strUserPass = value; }
        }

        public string UserID
        {
            get { return strUserID; }
            set { strUserID = value; }
        }

        public string Database
        {
            get { return strDatabase; }
            set { strDatabase = value; }
        }

        public string Server
        {
            get { return strServer; }
            set { strServer = value; }
        }

        public string Hour
        {
            get { return strHour; }
            set { strHour = value; }
        }

        public string Second
        {
            get { return strSecond; }
            set { strSecond = value; }
        }

        public string Minute
        {
            get { return strMinute; }
            set { strMinute = value; }
        }

        public string KitchenPrinter
        {
            get { return strKitchenPrinter; }
            set { strKitchenPrinter = value; }
        }

        public string BarPrinter
        {
            get { return strBarPrinter; }
            set { strBarPrinter = value; }
        }

        public string BillPrinter
        {
            get { return strBillPrinter; }
            set { strBillPrinter = value; }
        }

        public string CableID
        {
            get { return strCableID; }
            set { strCableID = value; }
        }

        public string Header1
        {
            get { return strHeader1; }
            set { strHeader1 = value; }
        }

        public string Header2
        {
            get { return strHeader2; }
            set { strHeader2 = value; }
        }

        public string Header3
        {
            get { return strHeader3; }
            set { strHeader3 = value; }
        }

        public string Header4
        {
            get { return strHeader4; }
            set { strHeader4 = value; }
        }

        public string Header5
        {
            get { return strHeader5; }
            set { strHeader5 = value; }
        }

        public string FootNode1
        {
            get { return strFootNode1; }
            set { strFootNode1 = value; }
        }

        public string FootNode2
        {
            get { return strFootNode2; }
            set { strFootNode2 = value; }
        }

        public string Host { get; set; }

        public int AllowPrint
        {
            get { return intAllowPrint; }
            set { intAllowPrint = value; }
        }

        public int AllowSalesFuel
        {
            get { return intAllowSalesFuel; }
            set { intAllowSalesFuel = value; }
        }

        public int AllowSalesFastFood
        {
            get { return intAllowSalesFastFood; }
            set { intAllowSalesFastFood = value; }
        }

        public ReadDBConfig()
        {
            try
            {
                //cai nay cho database
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                ini = new clsReadAndWriteINI(sPath);
                strServer = ini.ReadValue("dbconfig", "Server");
                Host = ini.ReadValue("dbconfig", "Host");
                strDatabase = ini.ReadValue("dbconfig", "Database");
                strUserID = ini.ReadValue("dbconfig", "User");
                strUserPass = ini.ReadValue("dbconfig", "Pass");
                strLocalMode = ini.ReadValue("dbconfig", "LocalMode");
                strKitchenPrinter = ini.ReadValue("dbconfig", "KitchenPrinter");
                strBarPrinter = ini.ReadValue("dbconfig", "BarPrinter");
                strBillPrinter = ini.ReadValue("dbconfig", "BillPrinter");
                strCableID = ini.ReadValue("dbconfig", "CableID");

                strHeader1 = ini.ReadValue("dbconfig", "Header1");
                strHeader2 = ini.ReadValue("dbconfig", "Header2");
                strHeader3 = ini.ReadValue("dbconfig", "Header3");
                strHeader4 = ini.ReadValue("dbconfig", "Header4");
                strHeader5 = ini.ReadValue("dbconfig", "Header5");
                strFootNode1 = ini.ReadValue("dbconfig", "FootNode1");
                strFootNode2 = ini.ReadValue("dbconfig", "FootNode2");
                strHour = ini.ReadValue("TimeShift", "Hour");
                strMinute = ini.ReadValue("TimeShift", "Minute");
                strSecond = ini.ReadValue("TimeShift", "Second");
                strCOM = ini.ReadValue("COM_fuelAuto", "COM");
                DeliveryAmount = Convert.ToDouble(ini.ReadValue("DeliveryAmount", "Total"));
                intAllowPrint = Convert.ToInt32(ini.ReadValue("PrintTaxInvoice", "AllowPrint"));
                intAllowSalesFuel = Convert.ToInt32(ini.ReadValue("PrintUseSaleFuel", "AllowSaleFuel"));
                intAllowSalesFastFood = Convert.ToInt32(ini.ReadValue("UseSaleFastFood", "AllowSaleFastFood"));

                if (strServer == "" || strDatabase == "" || strUserID == "")
                {
                    ini.WriteValue("dbconfig", "Server", "localhost");
                    ini.WriteValue("dbconfig", "Database", "paging");
                    ini.WriteValue("dbconfig", "User", "root");
                    ini.WriteValue("dbconfig", "Pass", "becastek");
                    throw new Exception("Set file config.ini");
                }
                if (strHeader1 == "")
                {
                    ini.WriteValue("dbconfig", "Header1", strHeader1);
                }
                if (strCOM == "")
                {
                    ini.WriteValue("COM_fuelAuto", "COM", strCOM);
                }
                if (strHeader2 == "")
                {
                    ini.WriteValue("dbconfig", "Header2", strHeader2);
                }
                if (strHeader3 == "")
                {
                    ini.WriteValue("dbconfig", "Header3", strHeader3);
                }
                if (strHeader4 == "")
                {
                    ini.WriteValue("dbconfig", "Header4", strHeader4);
                }
                if (strHeader5 == "")
                {
                    ini.WriteValue("dbconfig", "Header5", strHeader5);
                }
            }
            catch (Exception)
            {
            }
        }

        public void WriteConfig(string databaseName, string server, string user, string pass, string kitchen, string bill)
        {
            this.strDatabase = databaseName;
            this.strServer = server;
            this.strUserID = user;
            this.strUserPass = pass;
            this.strKitchenPrinter = kitchen;
            this.strBillPrinter = bill;
            ini.WriteValue("dbconfig", "Database", strDatabase);
            ini.WriteValue("dbconfig", "Server", strServer);
            ini.WriteValue("dbconfig", "User", strUserID);
            ini.WriteValue("dbconfig", "Pass", strUserPass);
            ini.WriteValue("dbconfig", "LocalMode", strLocalMode);
            ini.WriteValue("dbconfig", "KitchenPrinter", strKitchenPrinter);
            ini.WriteValue("dbconfig", "BarPrinter", strBarPrinter);
            ini.WriteValue("dbconfig", "BillPrinter", strBillPrinter);
            ini.WriteValue("dbconfig", "CableID", strCableID);

            ini.WriteValue("dbconfig", "Header1", strHeader1);
            ini.WriteValue("dbconfig", "Header2", strHeader2);
            ini.WriteValue("dbconfig", "Header3", strHeader3);
            ini.WriteValue("dbconfig", "Header4", strHeader4);
            ini.WriteValue("dbconfig", "Header5", strHeader5);
            ini.WriteValue("dbconfig", "FootNode1", strFootNode1);
            ini.WriteValue("dbconfig", "FootNode2", strFootNode2);
            ini.WriteValue("TimeShift", "Minute", strMinute);
            ini.WriteValue("TimeShift", "Hour", strHour);
            ini.WriteValue("TimeShift", "Second", strSecond);
            ini.WriteValue("PrintTaxInvoice", "AllowPrint", intAllowPrint.ToString());
            ini.WriteValue("PrintUseSaleFuel", "AllowSaleFuel", intAllowSalesFuel.ToString());
            ini.WriteValue("UseSaleFastFood", "AllowSaleFastFood", intAllowSalesFastFood.ToString());
        }

        public void WriteConfig(string databaseName, string server, string user, string pass)
        {
            this.strDatabase = databaseName;
            this.strServer = server;
            this.strUserID = user;
            this.strUserPass = pass;            
            ini.WriteValue("dbconfig", "Database", strDatabase);
            ini.WriteValue("dbconfig", "Server", strServer);
            ini.WriteValue("dbconfig", "User", strUserID);
            ini.WriteValue("dbconfig", "Pass", strUserPass);
            ini.WriteValue("dbconfig", "LocalMode", strLocalMode);            
            ini.WriteValue("dbconfig", "CableID", strCableID);

            ini.WriteValue("dbconfig", "Header1", strHeader1);
            ini.WriteValue("dbconfig", "Header2", strHeader2);
            ini.WriteValue("dbconfig", "Header3", strHeader3);
            ini.WriteValue("dbconfig", "Header4", strHeader4);
            ini.WriteValue("dbconfig", "Header5", strHeader5);
            ini.WriteValue("dbconfig", "FootNode1", strFootNode1);
            ini.WriteValue("dbconfig", "FootNode2", strFootNode2);
            ini.WriteValue("TimeShift", "Minute", strMinute);
            ini.WriteValue("TimeShift", "Hour", strHour);
            ini.WriteValue("TimeShift", "Second", strSecond);
            ini.WriteValue("PrintTaxInvoice", "AllowPrint", intAllowPrint.ToString());
            ini.WriteValue("PrintUseSaleFuel", "AllowSaleFuel", intAllowSalesFuel.ToString());
            ini.WriteValue("UseSaleFastFood", "AllowSaleFastFood", intAllowSalesFastFood.ToString());
        }

        public void WriteConfig(string databaseName, string server, string user, string pass, string bill)
        {
            this.strDatabase = databaseName;
            this.strServer = server;
            this.strUserID = user;
            this.strUserPass = pass;
            this.strBillPrinter = bill;
            ini.WriteValue("dbconfig", "Database", strDatabase);
            ini.WriteValue("dbconfig", "Server", strServer);
            ini.WriteValue("dbconfig", "User", strUserID);
            ini.WriteValue("dbconfig", "Pass", strUserPass);
            ini.WriteValue("dbconfig", "LocalMode", strLocalMode);
            ini.WriteValue("dbconfig", "KitchenPrinter", strKitchenPrinter);
            ini.WriteValue("dbconfig", "BarPrinter", strBarPrinter);
            ini.WriteValue("dbconfig", "BillPrinter", strBillPrinter);
            ini.WriteValue("dbconfig", "CableID", strCableID);

            ini.WriteValue("dbconfig", "Header1", strHeader1);
            ini.WriteValue("dbconfig", "Header2", strHeader2);
            ini.WriteValue("dbconfig", "Header3", strHeader3);
            ini.WriteValue("dbconfig", "Header4", strHeader4);
            ini.WriteValue("dbconfig", "Header5", strHeader5);
            ini.WriteValue("dbconfig", "FootNode1", strFootNode1);
            ini.WriteValue("dbconfig", "FootNode2", strFootNode2);
            ini.WriteValue("TimeShift", "Minute", strMinute);
            ini.WriteValue("TimeShift", "Hour", strHour);
            ini.WriteValue("TimeShift", "Second", strSecond);
            ini.WriteValue("PrintTaxInvoice", "AllowPrint", intAllowPrint.ToString());
            ini.WriteValue("PrintUseSaleFuel", "AllowSaleFuel", intAllowSalesFuel.ToString());
            ini.WriteValue("UseSaleFastFood", "AllowSaleFastFood", intAllowSalesFastFood.ToString());
        }

        public void WriteLocalMode(int mode)
        {
            strLocalMode = mode + "";
            ini.WriteValue("dbconfig", "LocalMode", strLocalMode);
        }
    }
}