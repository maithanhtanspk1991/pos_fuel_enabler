﻿using System;
using MySql.Data.MySqlClient;

namespace Connection
{
    public class Connection
    {
        private ReadDBConfig dbconfig;
        private MySqlConnection con;
        private MySqlCommand cmd;
        private MySqlDataAdapter da;
        private MySqlTransaction tran;
        private MySqlScript script;

        public delegate void ChangeLocalModeEventHandler(string localmode);

        public event ChangeLocalModeEventHandler ChangeLocalMode;

        /// <summary>
        /// Connection
        /// </summary>
        /// <param name="config"></param>
        public Connection(SystemConfig.DBConfig config)
        {
            string MyConString = "SERVER=" + config.CurentHost + ";" +
                "DATABASE=" + config.DatabaseName + ";" +
                "UID=" + config.User + ";" +
                "PWD=" + config.Pass + "; charset=utf8;";
            con = new MySqlConnection(MyConString);
            script = new MySqlScript();
            script.Connection = con;
            cmd = con.CreateCommand();
        }

        public Connection()
        {
            con = new MySqlConnection();
            script = new MySqlScript();
            script.Connection = con;
            cmd = con.CreateCommand();
            cmd.CommandTimeout = 1000 *600;
            SetConnectionString();
        }

        public Connection(string server)
        {
            dbconfig = new ReadDBConfig();
            dbconfig.Server = server;
            con = new MySqlConnection();
            script = new MySqlScript();
            script.Connection = con;
            cmd = con.CreateCommand();

            string MyConString = "SERVER=" + server + ";" +
                "DATABASE=" + dbconfig.Database + ";" +
                "UID=" + dbconfig.UserID + ";" +
                "PWD=" + dbconfig.UserPass + ";";
            con.ConnectionString = MyConString;
        }

        public string GetServer()
        {
            return dbconfig.Server;
        }

        public string GetCableID()
        {
            return dbconfig.CableID;
        }

        public void SetConnectionString()
        {
            dbconfig = new ReadDBConfig();

            string MyConString;
            if (dbconfig.CableID == "1")
            {
                MyConString = "SERVER=" + dbconfig.Host + ";" +
                "DATABASE=" + dbconfig.Database + ";" +
                "UID=" + dbconfig.UserID + ";" +
                "PWD=" + dbconfig.UserPass + ";";
            }
            //else if (dbconfig.LocalMode == "1")
            //{
            //    MyConString = "SERVER=localhost;" +
            //    "DATABASE=" + dbconfig.Database + ";" +
            //    "UID=" + dbconfig.UserID + ";" +
            //    "PWD=" + dbconfig.UserPass + ";";
            //    //con = new MySqlConnection(MyConString);
            //    OnChangeLocalMode("OFFLINE");
            //}
            else
            {
                MyConString = "SERVER=" + dbconfig.Server + ";" +
                "DATABASE=" + dbconfig.Database + ";" +
                "UID=" + dbconfig.UserID + ";" +
                "PWD=" + dbconfig.UserPass + ";";
                OnChangeLocalMode("ONLINE");
                //con = new MySqlConnection(MyConString);
            }
            if (con.State != System.Data.ConnectionState.Closed)
            {
                con.Close();
            }
            con.ConnectionString = MyConString;
        }

        public void ChangeLocalConnection(int mode)
        {
            dbconfig.WriteLocalMode(mode);
        }

        public bool CheckLocalhost()
        {
            if (dbconfig.LocalMode == "1")
            {
                return true;
            }
            return false;
        }

        public System.Data.DataTable Select(string sql)
        {
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                cmd.CommandText = sql;
                da = new MySqlDataAdapter(cmd);
                da.Fill(tbl);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return tbl;
        }

        public void AddPara(string name, object values, System.Data.DbType type)
        {
            MySqlParameter para = new MySqlParameter(name, type);
            para.Value = values;
            cmd.Parameters.Add(para);
        }

        public int ExecuteScript(String sql)
        {
            script.Query = sql;
            return script.Execute();
        }

        public int ExecuteNonQuery(string sql)
        {
            try
            {
                cmd.CommandText = sql;
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public object ExecuteScalar(string sql)
        {
            try
            {
                cmd.CommandText = sql;
                return cmd.ExecuteScalar();
            }
            catch (Exception)
            {
            }
            return null;
        }

        public void ClearPara()
        {
            cmd.Parameters.Clear();
        }

        public void Open()
        {
            con.Open();
        }

        private bool TryReconnection()
        {
            bool key = false;
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    con.Open();
                    key = true;
                }
                catch (Exception)
                { }
                if (key)
                {
                    break;
                }
            }
            return key;
        }

        public void Close()
        {
            try
            {
                con.Close();
            }
            catch (Exception)
            {
            }
        }

        public void BeginTransaction()
        {
            tran = con.BeginTransaction();
            cmd.Transaction = tran;
        }

        public void Commit()
        {
            tran.Commit();
        }

        public void Rollback()
        {
            try
            {
                tran.Rollback();
            }
            catch (Exception)
            {
            }
        }

        public bool TestConnection(string server, string database, string user, string pass)
        {
            try
            {
                string MyConString = "SERVER=" + server + ";" +
                "DATABASE=" + database + ";" +
                "UID=" + user + ";" +
                "PWD=" + pass + ";";
                MySqlConnection con = new MySqlConnection(MyConString);
                MySqlCommand cmd = con.CreateCommand();
                con.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }

        private void OnChangeLocalMode(string localmode)
        {
            if (ChangeLocalMode != null)
            {
                ChangeLocalMode(localmode);
            }
        }
    }
}