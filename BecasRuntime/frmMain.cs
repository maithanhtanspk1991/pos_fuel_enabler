﻿using System;
using System.Windows.Forms;

namespace BecasRuntime
{
    public partial class frmMain : Form
    {
        private Serial mSerial;
        private SerialConfig mSerialConfig;

        public frmMain()
        {
            InitializeComponent();
            mSerial = new Serial();
            mSerialConfig = new SerialConfig();
            mSerialConfig.SetPath(Application.StartupPath + "\\config.ini");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "becastek")
            {
                int numOfDay = 0;
                try
                {
                    numOfDay = Convert.ToInt32(txtNumOfDate.Text);
                    mSerialConfig.SetupTrial(numOfDay);
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "becastek")
            {
                try
                {
                    mSerialConfig.WriteActiveCode(mSerial.CreateSerial(mSerial.CreateMachineKey()));
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mSerialConfig.SetPath(openFileDialog1.FileName);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "becastek")
            {
                try
                {
                    mSerialConfig.WriteMasterCode(mSerial.CreateMasterKey(mSerial.CreateMachineKey()));
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            txtMacAddress.Text = mSerial.GetMacAddress();
        }
    }
}