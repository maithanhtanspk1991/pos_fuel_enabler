﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BecasRuntime
{
    public partial class Form1 : Form
    {
        private Serial serial;
        private SerialConfig serialConfig;
        public Form1()
        {
            InitializeComponent();
            serial = new Serial();
            serialConfig = new SerialConfig();
            serialConfig.SetPath(Application.StartupPath+"\\config.ini");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text=="becastek")
            {
                int numOfDay = 0;
                try
                {
                    numOfDay = Convert.ToInt32(txtNumOfDate.Text);
                    serialConfig.SetupTrial(numOfDay);
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {                    
                }                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "becastek")
            {                
                try
                {
                    serialConfig.WriteActiveCode(serial.CreateSerial(serial.CreateMachineKey()));
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {
                }
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                serialConfig.SetPath(openFileDialog1.FileName);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "becastek")
            {
                try
                {
                    serialConfig.WriteMasterCode(serial.CreateMasterKey(serial.CreateMachineKey()));
                    MessageBox.Show("OK");
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
