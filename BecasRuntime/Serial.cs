﻿using System;
using System.Text;

namespace BecasRuntime
{
    internal class Serial
    {
        public string MacAddress { get; set; }

        public string GetMacAddress()
        {
            System.Net.NetworkInformation.NetworkInterface[] infoNics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            string mac = "";
            if (infoNics == null || infoNics.Length < 1)
                return "notmac";
            foreach (System.Net.NetworkInformation.NetworkInterface adapter in infoNics)
            {
                System.Net.NetworkInformation.PhysicalAddress address = adapter.GetPhysicalAddress();
                byte[] bytes = address.GetAddressBytes();
                for (int i = 0; i < bytes.Length; i++)
                {
                    mac += bytes[i].ToString();
                }
            }
            MacAddress = mac;
            return mac;
        }

        public string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            System.Security.Cryptography.MD5 md5Hasher = System.Security.Cryptography.MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length / 2; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
                if (i % 2 != 0 && i < 7)
                {
                    sBuilder.Append("-");
                }
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public bool verifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = getMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string CreateMachineKey()
        {
            string output = "";
            return output = getMd5Hash(getMd5Hash(MacAddress) + "becasposVN").ToUpper();
        }

        public string CreateMasterKey(string machineKey)
        {
            string output = "";
            return output = getMd5Hash("KEY" + machineKey + "BECAS@123").ToUpper();
        }

        public string CreateSerial(string MachineKey)
        {
            string output = "";
            return output = getMd5Hash("OK" + MachineKey + "BECASTECH").ToUpper();
        }

        public bool CheckMasterKey(string key)
        {
            if (CreateMasterKey(CreateMachineKey()) == key)
            {
                return true;
            }
            return false;
        }

        public bool CheckSerial(string serial)
        {
            if (CreateSerial(CreateMachineKey()) == serial)
                return true;
            else
                return false;
        }
    }
}