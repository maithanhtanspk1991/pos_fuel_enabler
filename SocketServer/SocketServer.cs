﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SocketServerClient
{
    public class SocketServer
    {
        private bool isRunning = true;

        private TcpListener m_socketServer;
        private Thread m_thread;
        private Connection.Connection mConnection;

        public SocketServer()
        { }

        public SocketServer(int port, int timeout)
        {
            mConnection = new Connection.Connection();
            this.TimeOut = timeout;
           
            m_socketServer = new TcpListener(port);
            m_socketServer.Start();

            m_thread = new Thread(getData);
            m_thread.Start();
        }

        public delegate void MyServerEvenHandler(SocketServer serverSocket, SocketEventArgs e);

        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);

        public event MyServerEvenHandler Received;

        public int TimeOut { get; set; }

        public void Close()
        {
            isRunning = false;
            try
            {
                //m_thread.Suspend();
                m_socketServer.Stop();
            }
            catch (Exception)
            {
            }
        }

        public void SendData(SocketEventArgs t_event)
        {
            //t_event.Data = t_event.Data.Substring(2,t_event.Data.Length);
            Console.WriteLine("Server Gui:" + t_event.Data);
            //t_event.Data = new String('a', 15000);
            t_event.CurenSocket.Send(SocketStream.ConvertData(t_event.Data));
        }

        //public byte[] getStream(Socket m_socket,byte[] buff)
        //{
        //    int count = 0;
        //    while (count < TimeOut)
        //    {
        //        if (m_socket.Available>0)
        //        {
        //            buff = new byte[m_socket.Available];
        //            m_socket.Receive(buff);
        //            return buff;
        //        }
        //        Thread.Sleep(10);
        //        count++;
        //    }
        //    throw new Exception("Timeout Error");
        //}
        public void SetText(string text, System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        private void getData()
        {
            while (isRunning)
            {
                SocketEventArgs e = new SocketEventArgs();
                try
                {
                    Socket m_socket = m_socketServer.AcceptSocket();
                    new Thread(delegate() { ProcessData(m_socket, e); }).Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void OnReceived(SocketEventArgs e)
        {
            if (Received != null)
            {
                Received(this, e);
            }
        }

        private void ProcessData(Socket m_socket, SocketEventArgs e)
        {
            try
            {
                string result;
                //result= SocketStream.GetDataServerSecurity(m_socket, TimeOut,mConnection);
                result = SocketStream.GetData(m_socket, TimeOut);
                //Console.WriteLine("Nhan:"+result+"("+result.Length+")");
                e.IsError = false;
                e.Data = result;
                e.CurenSocket = m_socket;
                OnReceived(e);
                SendData(e);
            }
            catch (Exception er)
            {
                e.IsError = true;
                e.Data = er.Message;
                OnReceived(e);
            }
        }

        public class ThreadSocket
        {
            private SocketEventArgs t_event;

            public ThreadSocket(SocketEventArgs e)
            {
                t_event = e;
            }

            public void run()
            {
                string length = t_event.Data.Length + "";
                if (length.Length < SocketStream.HEAD_DATA_LENGTH)
                {
                    length = new string('0', SocketStream.HEAD_DATA_LENGTH - length.Length) + length;
                }
                t_event.Data = Encoding.Default.GetString(SocketStream.START) + length + t_event.Data + Encoding.Default.GetString(SocketStream.END);
                t_event.CurenSocket.Send(Encoding.Default.GetBytes(t_event.Data));
            }
        }
    }
}