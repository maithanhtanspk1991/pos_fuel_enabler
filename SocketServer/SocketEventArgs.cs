﻿using System;

namespace SocketServerClient
{
    public class SocketEventArgs : EventArgs
    {
        public string Data { get; set; }

        public bool IsError { get; set; }

        public System.Net.Sockets.Socket CurenSocket { get; set; }

        public SocketEventArgs(bool isError, string data, System.Net.Sockets.Socket curensocket)
        {
            CurenSocket = curensocket;
            Data = data;
            IsError = isError;
        }

        public SocketEventArgs()
        { }
    }
}