﻿using System;

namespace SocketServerClient
{
    public class Security
    {
        public static string GetCodeFromKey(string key)
        {
            double x;
            string s = "";
            int a = 0;
            Console.WriteLine("Key:");
            for (int i = 0; i < key.Length; i++)
            {
                x = (double)key[i];
                x = Math.Pow(x, 3);
                x = Math.Sin(x);
                x = x * 111;
                if (s.Length > 0)
                {
                    x = x * s[s.Length - 1];
                }
                a = (int)x;
                a = Math.Abs(a);
                a = a % 60;
                if (a < 10)
                {
                    s += a + "";
                }
                else if (a < 35)
                {
                    s += (char)((a - 10) + (int)'a');
                }
                else
                {
                    s += (char)((a - 35) + (int)'A');
                }
                Console.Write(a + " ");
            }
            Console.WriteLine();
            return s;
        }

        public static string CreateKey()
        {
            Random random = new Random();
            string s = "";
            for (int i = 0; i < 10; i++)
            {
                int a = random.Next(60);
                if (a < 10)
                {
                    s += a + "";
                }
                else if (a < 35)
                {
                    s += (char)((a - 10) + (int)'a');
                }
                else
                {
                    s += (char)((a - 35) + (int)'A');
                }
            }
            return s;
        }

        public static bool CompareKey(string key, string values, Connection.Connection con)
        {
            bool isKey = false;
            try
            {
                con.Open();
                if (con.ExecuteNonQuery("update securitys set sStatus=1 where sKey='" + key + "' and sStatus=0") > 0)
                {
                    isKey = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
            if (!isKey)
            {
                return false;
            }
            string s = Security.GetCodeFromKey(key);
            if (s.Length != values.Length)
            {
                return false;
            }
            for (int i = 0; i < s.Length; i++)
            {
                Console.WriteLine((int)s[i] + ":::" + (int)values[i]);
                if ((int)s[i] != (int)values[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}