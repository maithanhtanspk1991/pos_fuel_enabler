﻿using System;

namespace SocketServerClient
{
    public class SocketClient
    {
        //private System.Net.Sockets.TcpClient mTcpClient;
        private System.Net.Sockets.Socket mSocket;
        private int mPort;
        private string mIPAdress;

        public SocketClient(int port, string ipAdress)
        {
            //mTcpClient = new System.Net.Sockets.TcpClient();
            mPort = port;
            mIPAdress = ipAdress;
        }

        public string SendData(string data, int timeOut)
        {
            string resuilt = "";

            try
            {
                //cu co phan security
                //mTcpClient = new System.Net.Sockets.TcpClient(mIPAdress, mPort);
                ////send header
                //mTcpClient.Client.Send(SocketStream.ConvertData(""));
                //string key = SocketStream.GetData(mTcpClient.Client, timeOut);
                //Console.WriteLine("Nhan tu server:"+key);
                //mTcpClient = new System.Net.Sockets.TcpClient(mIPAdress, mPort);
                //mTcpClient.Client.Send(SocketStream.ConvertData(data,key,Security.GetCodeFromKey(key)));
                //resuilt = SocketStream.GetData(mTcpClient.Client, 1000);
                //mTcpClient.Close();

                //new
                mSocket = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                IAsyncResult lIAsyncResult = mSocket.BeginConnect(mIPAdress, mPort, null, null);

                bool check = lIAsyncResult.AsyncWaitHandle.WaitOne(1500, true);
                if (check)
                {
                    mSocket.EndConnect(lIAsyncResult);
                }
                else
                {
                    throw new Exception("Failed to connect server.");
                }
                mSocket.Send(SocketStream.ConvertData(data));
                resuilt = SocketStream.GetData(mSocket, timeOut);
                //mTcpClient = new System.Net.Sockets.TcpClient(mIPAdress, mPort);
                //mTcpClient.SendTimeout = 100;
                //mTcpClient.ReceiveTimeout = 100;
                //mTcpClient.Client.Send(SocketStream.ConvertData(data));
                //resuilt = SocketStream.GetData(mTcpClient.Client, timeOut);
                //mTcpClient.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Socket.SendData:::" + ex.Message);
                //resuilt = ex.Message;
                throw ex;
            }
            finally
            {
                mSocket.Close();
            }
            return resuilt;
        }
    }
}