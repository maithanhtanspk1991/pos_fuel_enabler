﻿using System;
using System.Text;

namespace SocketServerClient
{
    internal class SocketStream
    {
        //Server
        //xxyyyyydatazz - data:zxxxxxyyyyykeyvaluedata
        // xxyyyyydatazz
        public static byte[] START = { 01, 01 };

        public static byte[] END = { 04, 04 };
        public static int HEAD_DATA_LENGTH = 5;

        public static byte[] getStream(System.Net.Sockets.Socket m_socket, int timeout)
        {
            byte[] buff = null;
            int count = 0;
            while (count < timeout)
            {
                if (m_socket.Available > 0)
                {
                    buff = new byte[m_socket.Available];
                    //===============================================
                    m_socket.Receive(buff);
                    return buff;
                }
                System.Threading.Thread.Sleep(10);
                count++;
            }
            throw new Exception("Timeout Error");
        }

        public static byte[] getStream(System.Net.Sockets.Socket m_socket, int size, int timeout)
        {
            byte[] buff = null;
            int count = 0;
            while (count < timeout)
            {
                if (m_socket.Available > 0 && m_socket.Available >= size)
                {
                    buff = new byte[size];
                    //===============================================
                    m_socket.Receive(buff);
                    return buff;
                }
                System.Threading.Thread.Sleep(1);
                count++;
            }
            throw new Exception("Timeout Error");
        }

        public static byte[] ConvertData(string data)
        {
            data = GetLengthString(data) + data;
            return Encoding.Default.GetBytes(Encoding.Default.GetString(START) + data + Encoding.Default.GetString(END));
        }

        //xxyyyyydatazz
        //data: x yyyyy zzzzz key value data
        public static byte[] ConvertData(string data, string key, string value)
        {
            data = GetLengthString(data) + GetLengthString(key) + GetLengthString(value) + key + value + data;
            return Encoding.Default.GetBytes(Encoding.Default.GetString(START) + data + Encoding.Default.GetString(END));
        }

        public static string GetLengthString(string data)
        {
            string length = data.Length + "";
            if (length.Length < HEAD_DATA_LENGTH)
            {
                length = new String('0', HEAD_DATA_LENGTH - length.Length) + length;
            }
            return length;
        }

        public static string GetData(System.Net.Sockets.Socket socket, int timeOut)
        {
            string resuilt = "";
            byte[] buff;
            //get header
            buff = getStream(socket, START.Length, timeOut);
            for (int i = 0; i < START.Length; i++)
            {
                if (buff[i] != START[i])
                {
                    throw new Exception("Head Error - Start");
                }
            }
            //get length
            buff = getStream(socket, HEAD_DATA_LENGTH, timeOut);
            int total_length = Convert.ToInt32(Encoding.Default.GetString(buff));
            if (total_length == 0)
            {
                //new
                buff = getStream(socket, END.Length, timeOut);
                for (int i = 0; i < END.Length; i++)
                {
                    if (buff[buff.Length - END.Length + i] != END[i])
                    {
                        throw new Exception("Head Error - End1");
                    }
                }
                return resuilt;
            }
            while (resuilt.Length < (total_length + END.Length))
            {
                resuilt += Encoding.Default.GetString(getStream(socket, timeOut));
            }
            //check end
            if (resuilt.Length < END.Length)
            {
                throw new Exception("End Error");
            }
            for (int i = 0; i < END.Length; i++)
            {
                if (resuilt[resuilt.Length - END.Length + i] != END[i])
                {
                    throw new Exception("Head Error - End1");
                }
            }
            return resuilt.Remove(resuilt.Length - END.Length);
        }

        public static string GetDataServerSecurity(System.Net.Sockets.Socket socket, int timeOut, Connection.Connection con)
        {
            string resuilt = "";
            byte[] buff;
            //get header
            buff = getStream(socket, START.Length, timeOut);
            for (int i = 0; i < START.Length; i++)
            {
                if (buff[i] != START[i])
                {
                    throw new Exception("Head Error - Start");
                }
            }
            //get length
            buff = getStream(socket, HEAD_DATA_LENGTH, timeOut);
            int total_length = Convert.ToInt32(Encoding.Default.GetString(buff));
            if (total_length == 0)
            {
                //new
                buff = getStream(socket, END.Length, timeOut);
                for (int i = 0; i < END.Length; i++)
                {
                    if (buff[buff.Length - END.Length + i] != END[i])
                    {
                        throw new Exception("Head Error - End1");
                    }
                }
                return resuilt;
            }
            //security

            buff = getStream(socket, HEAD_DATA_LENGTH, timeOut);
            int keyLength = Convert.ToInt16(Encoding.Default.GetString(buff));
            buff = getStream(socket, HEAD_DATA_LENGTH, timeOut);
            int valueLength = Convert.ToInt16(Encoding.Default.GetString(buff));
            buff = getStream(socket, keyLength, timeOut);
            string key = Encoding.Default.GetString(buff);
            string values = Encoding.Default.GetString(getStream(socket, valueLength, timeOut));
            Console.WriteLine("key:" + keyLength + ":::" + valueLength + ":::" + key + ":::" + values + "===" + values.Length);
            //check security
            if (!Security.CompareKey(key, values, con))
            {
                throw new Exception("Key Error");
            }
            while (resuilt.Length < (total_length + END.Length))
            {
                resuilt += Encoding.Default.GetString(getStream(socket, timeOut));
            }
            //check end
            if (resuilt.Length < END.Length)
            {
                throw new Exception("End Error");
            }
            for (int i = 0; i < END.Length; i++)
            {
                if (resuilt[resuilt.Length - END.Length + i] != END[i])
                {
                    throw new Exception("Head Error - End1");
                }
            }
            return resuilt.Remove(resuilt.Length - END.Length);
        }
    }
}