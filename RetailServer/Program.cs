﻿using System;

namespace RetailServer
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            SocketServerClient.SocketServer server = new SocketServerClient.SocketServer(12341, 500);
            server.Received += new SocketServerClient.SocketServer.MyServerEvenHandler(server_Received);
        }

        private static void server_Received(SocketServerClient.SocketServer serverSocket, SocketServerClient.SocketEventArgs e)
        {
            if (e.Data == "are you online")
            {
                e.Data = "ok";
            }
        }
    }
}