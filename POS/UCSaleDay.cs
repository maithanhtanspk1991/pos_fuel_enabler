﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCSaleDay : UserControl
    {
        public UCSaleDay()
        {
            InitializeComponent(); 
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                groupBox1.Text = "Bán hàng";
                label2.Text = "Tổng bán:";
                label6.Text = "Tổng nhiên liệu bán:";
                label12.Text = "Tổng cửa hàng bán:";
                label3.Text = "Tổng giảm giá:";
                label4.Text = "Tổng thuế:";
                label5.Text = "Thẻ:";
                label7.Text = "Tiền mặt:";
                label9.Text = "Hoàn trả:";
                label1.Text = "Tiền vào:";
                label8.Text = "Tiền ra:";
                label13.Text = "Tài khoản:";
                label11.Text = "Phụ thu:";
                label14.Text = "Thanh toán hết nợ:";
                groupBox3.Text = "Thanh toán tài khoản";
                label10.Text = "Thẻ";
                label16.Text = "Tiền mặt";
                label23.Text = "Tổng cộng";
                groupBox4.Text = "Báo cáo ca làm việc";
                columnHeader1.Text = "Ca làm việc";
                columnHeader2.Text = "Số nợ";
                groupBox2.Text = "Thẻ";
                columnHeader7.Text = "Tên thẻ";
                columnHeader8.Text = "Thanh toán";
                columnHeader3.Text = "Phụ thu";
                lblTotalCardSurcharge.Text = "Tổng cộng";
                lblTotalCard.Text = "Tổng cộng";
                return;
            }
        }

        private void UCSaleDay_Load(object sender, EventArgs e)
        {
            SetMultiLanguage();
        }

    }
}