﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Data;
using System.Drawing;


namespace POS
{
    public class SocketServer
    {                
        private TcpListener server;
        private System.Threading.Thread thread;        
        private bool isRunning=true;
        private byte[] buff;                                
        public delegate void MyServerEvenHandler(SocketEventArgs e);
        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);
        public event MyServerEvenHandler Received;

        public SocketServer()
        {             
            InitServer();            
        }
        private void OnReceived(SocketEventArgs e)
        {
            if (Received != null)
            {
                Received(e);
            }
        }        
        private void InitServer()
        {
            server = new TcpListener(12345);
            server.Start();
            thread = new System.Threading.Thread(run);
            thread.Start();                    
        }
       
        private void run()
        {
            while (isRunning)
            {
                Socket socket = server.AcceptSocket();
                System.Threading.Thread t = new System.Threading.Thread(delegate() { ReceivedSocket(socket); });
                t.Start();
            }
        }
        private void ReceivedSocket(Socket socket)
        {
            int count = 0;            
            /*
            do
            {
                buff = new byte[socket.Available];
                count++;
            } while (buff.Length==0);
                        
            socket.Receive(buff);
             */
            while (count<1000 && socket.Available==0)
            {
                count++;
                System.Threading.Thread.Sleep(10);
            }
            buff = new byte[socket.Available];
            socket.Receive(buff);
            string s1 = "";
            for (int i = 0; i < buff.Length; i++)
            {
                s1 += buff[i] + ":";
            }
            string s = "";
            for (int i = 0; i < buff.Length; i++)
            {
                s += (char)buff[i] + ":";
            }
            ProcessData(buff); 
        }
        private void ProcessData(byte[] data)
        {
            Console.WriteLine(data.Length);
            if (data.Length>13)
            {    
                int size=Int16.Parse(""+data[9]+data[10]);
                int funct = Int16.Parse("" + data[7] + data[8]);
                if ((size+13)==data.Length)
                {
                    byte[] mydata=new byte[size];
                    for (int i = 0; i < mydata.Length; i++)
                    {
                        mydata[i] = data[i + 11];
                    }                    
                    OnReceived(new SocketEventArgs(funct,Encoding.Default.GetString(mydata)));
                }
          }
        }      
        public void Stop()
        {
            isRunning = false;
            thread.Suspend();            
            System.Threading.Thread.Sleep(100);
            server.Stop();                        
        }
        public class SocketEventArgs
        {
            public int Function { get; set; }
            public string Table { get; set; }
            public SocketEventArgs(int function,string table)
            {
                Function = function;
                Table = table;
            }

        }
        public void SetText(string text,System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text,control });
            }
            else
            {
                control.Text = text;
            }
        }
        //delegate void SetTextCallback(string text);
    }
}
