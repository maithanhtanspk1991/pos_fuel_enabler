﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace POS
{
    public partial class UCSettime : UserControl
    {
        private DateTime mDateTime;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public UCSettime()
        {
            InitializeComponent();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Đặt lại Ngày - Giờ";
                label2.Text = "Ngày hiện tại";
                label3.Text = "Giờ hiện tại";
                button1.Text = "Thiết lập giờ";
                button2.Text = "Thoát";
                return;
            }
        }

        [DllImport("kernel32.dll")]
        private static extern bool SetLocalTime(ref SYSTEMTIME time);

        [StructLayoutAttribute(LayoutKind.Sequential)]
        private struct SYSTEMTIME
        {
            public short year;
            public short month;
            public short dayOfWeek;
            public short day;
            public short hour;
            public short minute;
            public short second;
            public short milliseconds;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private string SplitDate(string date)
        {
            string[] listdate = date.Split('/');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            return listdate[1].ToString() + "-" + listdate[0].ToString() + "-" + listdate[2].ToString();
        }

        private bool CheckDate(string date)
        {
            string rightdate = @"^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
            Match m = Regex.Match(date, rightdate);
            if (m.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckTime(string time)
        {
            string righttime = @"^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$";
            Match m = Regex.Match(time, righttime);
            if (m.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void UCSettime_Load(object sender, EventArgs e)
        {
            mDateTime = DateTime.Now;
            textBox1.Text = String.Format("{0:dd/MM/yyyy}", mDateTime);
            textBox2.Text = String.Format("{0:HH:mm:ss}", mDateTime);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckDate(textBox1.Text) == true && CheckTime(textBox2.Text) == true)
            {
                string[] s = textBox1.Text.Split('/');
                try
                {
                    string date = s[2] + "/" + s[1] + "/" + s[0];
                    mDateTime = Convert.ToDateTime(date + " " + textBox2.Text);
                    //SYSTEMTIME time = new SYSTEMTIME();
                    //time.year = (short)mDateTime.Year;
                    //time.month = (short)mDateTime.Month;
                    //time.day = (short)mDateTime.Day;
                    //time.dayOfWeek = (short)mDateTime.DayOfWeek;
                    //time.hour = (short)mDateTime.Hour;
                    //time.minute = (short)mDateTime.Minute;
                    //time.second = (short)mDateTime.Second;
                    //time.milliseconds = (short)mDateTime.Millisecond;
                    //SetLocalTime(ref time);
                    Class.SystemTime.SetTime(mDateTime);
                    label4.Visible = false;
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCSettime:::SetLocalTime::::" + ex.Message);
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    label4.Text = "Định dạng ngày giờ không đúng";
                    label4.Visible = true;
                }
                label4.Text = "Wrong Date Time Format";
                label4.Visible = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (uCkeypad1.txtResult == null)
            {
                return;
            }
            uCkeypad1.txtResult.Focus();
            SendKeys.Send("/");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (uCkeypad1.txtResult == null)
            {
                return;
            }
            uCkeypad1.txtResult.Focus();
            SendKeys.Send(":");
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }
    }
}