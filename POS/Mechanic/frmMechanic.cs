﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Mechanic
{
    public partial class frmMechanic : Form
    {
        private readonly string _nameClass = "POS::Mechanic::frmMechanic::";
        private Connection.Connection mConnection = new Connection.Connection();
        private Class.MoneyFortmat money = new Class.MoneyFortmat(1);
        private Class.ProcessOrderNew mProcessOrderNew;

        public frmMechanic(Class.ProcessOrderNew processOrderNew)
        {
            InitializeComponent();
            mProcessOrderNew = processOrderNew;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnBack.Text = "Quay lại";
                return;
            }
        }

        public Class.ProcessOrderNew.Order Order { get; set; }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmMechanic_Click(object sender, EventArgs e)
        {
            UCMechanic uc = (UCMechanic)sender;
            DataRow row = (DataRow)uc.Tag;
            int OrderID = Convert.ToInt32(row["OrderID"]);
            Order = mProcessOrderNew.getSaveOrderByOrderID(OrderID);
            Order.RegoID = Convert.ToInt32(row["regoid"]);
            Order.Customer = BusinessObject.BOCustomers.GetByID(Convert.ToInt32(row["cusid"]));
            Order.Rego = BusinessObject.BORego.GetByID(Order.RegoID);
            Order.SlotID = Convert.ToInt32(row["id"]);
            Order.CableID = Convert.ToInt32(row["CableID"]);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void frmMechanic_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                mConnection.Open();
                string sql = "SELECT o.OrderID, o.subTotal, c.FirstName, m.CarNumber, s.slotid, s.cusid, s.regoid, s.id, o.CableID FROM `slot` s Inner Join saveordersdaily o on s.orderID = o.orderid Inner Join customers c on c.custID = s.cusid Inner Join managecar m on s.regoID = m.ID Where s.complete = 2 Order by s.slotid;";
                DataTable dt = mConnection.Select(sql);
                if (dt.Rows.Count > 0)
                {
                    int i = 0;
                    UCMechanic[] lsUCMechanic = new UCMechanic[dt.Rows.Count];
                    foreach (DataRow row in dt.Rows)
                    {
                        lsUCMechanic[i] = new UCMechanic();
                        lsUCMechanic[i].lbName.Text = "Name: " + row["FirstName"].ToString();
                        lsUCMechanic[i].lbRego.Text = "Rego: " + row["CarNumber"].ToString();
                        lsUCMechanic[i].lbTotal.Text = "Total: " + money.Format2(Convert.ToInt32(row["subTotal"]));
                        lsUCMechanic[i].lbSlot.Text = "Slot: " + row["slotid"].ToString();
                        lsUCMechanic[i].Click += new EventHandler(frmMechanic_Click);
                        lsUCMechanic[i].Tag = row;
                        i++;
                    }
                    flpOrder.Controls.Clear();
                    flpOrder.Controls.AddRange(lsUCMechanic);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadData::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }
    }
}