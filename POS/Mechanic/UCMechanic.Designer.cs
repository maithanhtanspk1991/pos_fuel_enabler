﻿namespace POS.Mechanic
{
    partial class UCMechanic
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSlot = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lbRego = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbSlot
            // 
            this.lbSlot.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbSlot.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot.ForeColor = System.Drawing.Color.White;
            this.lbSlot.Location = new System.Drawing.Point(0, 0);
            this.lbSlot.Name = "lbSlot";
            this.lbSlot.Size = new System.Drawing.Size(250, 25);
            this.lbSlot.TabIndex = 0;
            this.lbSlot.Text = "Slot:";
            this.lbSlot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbSlot.Click += new System.EventHandler(this.UCMechanic_Click);
            // 
            // lbTotal
            // 
            this.lbTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.ForeColor = System.Drawing.Color.Red;
            this.lbTotal.Location = new System.Drawing.Point(0, 25);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(250, 25);
            this.lbTotal.TabIndex = 1;
            this.lbTotal.Text = "Total:";
            this.lbTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbTotal.Click += new System.EventHandler(this.UCMechanic_Click);
            // 
            // lbName
            // 
            this.lbName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.ForeColor = System.Drawing.Color.White;
            this.lbName.Location = new System.Drawing.Point(0, 50);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(250, 25);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Name:";
            this.lbName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbName.Click += new System.EventHandler(this.UCMechanic_Click);
            // 
            // lbRego
            // 
            this.lbRego.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbRego.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRego.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbRego.Location = new System.Drawing.Point(0, 75);
            this.lbRego.Name = "lbRego";
            this.lbRego.Size = new System.Drawing.Size(250, 25);
            this.lbRego.TabIndex = 3;
            this.lbRego.Text = "Rego: ";
            this.lbRego.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbRego.Click += new System.EventHandler(this.UCMechanic_Click);
            // 
            // UCMechanic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.Controls.Add(this.lbRego);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbSlot);
            this.Name = "UCMechanic";
            this.Size = new System.Drawing.Size(250, 100);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lbSlot;
        public System.Windows.Forms.Label lbTotal;
        public System.Windows.Forms.Label lbName;
        public System.Windows.Forms.Label lbRego;

    }
}
