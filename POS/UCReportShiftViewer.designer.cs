﻿namespace POS
{
    partial class UCReportShiftViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAccount = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.labelCreditNote = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelCash = new System.Windows.Forms.Label();
            this.labelCard = new System.Windows.Forms.Label();
            this.labelRefunc = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelTotalGST = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelTotalDiscount = new System.Windows.Forms.Label();
            this.labelTotalSale = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstCard = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbto = new System.Windows.Forms.Label();
            this.lbfrom = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbshiftid = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTotalAccount = new System.Windows.Forms.Label();
            this.labelCashAccount = new System.Windows.Forms.Label();
            this.labelCardAccount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTotalCard = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelAccount
            // 
            this.labelAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAccount.Location = new System.Drawing.Point(135, 253);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(183, 20);
            this.labelAccount.TabIndex = 52;
            this.labelAccount.Text = "0.00";
            this.labelAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.labelAccount);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.labelCreditNote);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.labelCash);
            this.groupBox1.Controls.Add(this.labelCard);
            this.groupBox1.Controls.Add(this.labelRefunc);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.labelTotalGST);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.labelTotalDiscount);
            this.groupBox1.Controls.Add(this.labelTotalSale);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(5, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 323);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sales";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(53, 253);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 20);
            this.label13.TabIndex = 51;
            this.label13.Text = "Account:";
            // 
            // labelCreditNote
            // 
            this.labelCreditNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCreditNote.Location = new System.Drawing.Point(138, 189);
            this.labelCreditNote.Name = "labelCreditNote";
            this.labelCreditNote.Size = new System.Drawing.Size(183, 20);
            this.labelCreditNote.TabIndex = 6;
            this.labelCreditNote.Text = "0.00";
            this.labelCreditNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(69, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Eftpos:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Total Sales:";
            // 
            // labelCash
            // 
            this.labelCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCash.Location = new System.Drawing.Point(138, 158);
            this.labelCash.Name = "labelCash";
            this.labelCash.Size = new System.Drawing.Size(183, 20);
            this.labelCash.TabIndex = 4;
            this.labelCash.Text = "0.00";
            this.labelCash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCard
            // 
            this.labelCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCard.Location = new System.Drawing.Point(138, 129);
            this.labelCard.Name = "labelCard";
            this.labelCard.Size = new System.Drawing.Size(183, 20);
            this.labelCard.TabIndex = 4;
            this.labelCard.Text = "0.00";
            this.labelCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRefunc
            // 
            this.labelRefunc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRefunc.Location = new System.Drawing.Point(139, 221);
            this.labelRefunc.Name = "labelRefunc";
            this.labelRefunc.Size = new System.Drawing.Size(179, 20);
            this.labelRefunc.TabIndex = 4;
            this.labelRefunc.Text = "0.00";
            this.labelRefunc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(64, 221);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(69, 20);
            this.label31.TabIndex = 2;
            this.label31.Text = "Return:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Total Discount:";
            // 
            // labelTotalGST
            // 
            this.labelTotalGST.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalGST.Location = new System.Drawing.Point(138, 99);
            this.labelTotalGST.Name = "labelTotalGST";
            this.labelTotalGST.Size = new System.Drawing.Size(183, 20);
            this.labelTotalGST.TabIndex = 4;
            this.labelTotalGST.Text = "0.00";
            this.labelTotalGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(41, 99);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(95, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Total GST:";
            // 
            // labelTotalDiscount
            // 
            this.labelTotalDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalDiscount.Location = new System.Drawing.Point(138, 70);
            this.labelTotalDiscount.Name = "labelTotalDiscount";
            this.labelTotalDiscount.Size = new System.Drawing.Size(183, 20);
            this.labelTotalDiscount.TabIndex = 4;
            this.labelTotalDiscount.Text = "0.00";
            this.labelTotalDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTotalSale
            // 
            this.labelTotalSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalSale.Location = new System.Drawing.Point(138, 39);
            this.labelTotalSale.Name = "labelTotalSale";
            this.labelTotalSale.Size = new System.Drawing.Size(183, 20);
            this.labelTotalSale.TabIndex = 4;
            this.labelTotalSale.Text = "0.00";
            this.labelTotalSale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(84, 129);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 20);
            this.label27.TabIndex = 2;
            this.label27.Text = "Card:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(81, 158);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 20);
            this.label29.TabIndex = 2;
            this.label29.Text = "Cash:";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Subtotal";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader8.Width = 160;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Card Name";
            this.columnHeader7.Width = 210;
            // 
            // lstCard
            // 
            this.lstCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8});
            this.lstCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCard.FullRowSelect = true;
            this.lstCard.GridLines = true;
            this.lstCard.HideSelection = false;
            this.lstCard.HoverSelection = true;
            this.lstCard.Location = new System.Drawing.Point(3, 24);
            this.lstCard.MultiSelect = false;
            this.lstCard.Name = "lstCard";
            this.lstCard.Size = new System.Drawing.Size(376, 160);
            this.lstCard.TabIndex = 53;
            this.lstCard.UseCompatibleStateImageBehavior = false;
            this.lstCard.View = System.Windows.Forms.View.Details;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbto);
            this.panel1.Controls.Add(this.lbfrom);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lbshiftid);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(722, 80);
            this.panel1.TabIndex = 5;
            // 
            // lbto
            // 
            this.lbto.AutoSize = true;
            this.lbto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbto.Location = new System.Drawing.Point(495, 56);
            this.lbto.Name = "lbto";
            this.lbto.Size = new System.Drawing.Size(0, 20);
            this.lbto.TabIndex = 52;
            // 
            // lbfrom
            // 
            this.lbfrom.AutoSize = true;
            this.lbfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfrom.Location = new System.Drawing.Point(265, 56);
            this.lbfrom.Name = "lbfrom";
            this.lbfrom.Size = new System.Drawing.Size(0, 20);
            this.lbfrom.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(445, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 20);
            this.label3.TabIndex = 50;
            this.label3.Text = "To : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(194, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 49;
            this.label2.Text = "From : ";
            // 
            // lbshiftid
            // 
            this.lbshiftid.AutoSize = true;
            this.lbshiftid.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshiftid.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbshiftid.Location = new System.Drawing.Point(109, 49);
            this.lbshiftid.Name = "lbshiftid";
            this.lbshiftid.Size = new System.Drawing.Size(38, 29);
            this.lbshiftid.TabIndex = 48;
            this.lbshiftid.Text = "ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(3, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 29);
            this.label11.TabIndex = 47;
            this.label11.Text = "Shift ID :";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(722, 48);
            this.label1.TabIndex = 46;
            this.label1.Text = "Shift Report";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(0, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(723, 327);
            this.panel2.TabIndex = 6;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.labelTotalAccount);
            this.groupBox3.Controls.Add(this.labelCashAccount);
            this.groupBox3.Controls.Add(this.labelCardAccount);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(336, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 112);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Account Payment";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 82);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 20);
            this.label23.TabIndex = 45;
            this.label23.Text = "Total : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 20);
            this.label16.TabIndex = 45;
            this.label16.Text = "Cash : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 45;
            this.label10.Text = "Card : ";
            // 
            // labelTotalAccount
            // 
            this.labelTotalAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAccount.Location = new System.Drawing.Point(97, 87);
            this.labelTotalAccount.Name = "labelTotalAccount";
            this.labelTotalAccount.Size = new System.Drawing.Size(259, 20);
            this.labelTotalAccount.TabIndex = 46;
            this.labelTotalAccount.Text = "0.00";
            this.labelTotalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCashAccount
            // 
            this.labelCashAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashAccount.Location = new System.Drawing.Point(97, 60);
            this.labelCashAccount.Name = "labelCashAccount";
            this.labelCashAccount.Size = new System.Drawing.Size(259, 20);
            this.labelCashAccount.TabIndex = 46;
            this.labelCashAccount.Text = "0.00";
            this.labelCashAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCardAccount
            // 
            this.labelCardAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardAccount.Location = new System.Drawing.Point(97, 30);
            this.labelCardAccount.Name = "labelCardAccount";
            this.labelCardAccount.Size = new System.Drawing.Size(259, 20);
            this.labelCardAccount.TabIndex = 46;
            this.labelCardAccount.Text = "0.00";
            this.labelCardAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox2.Controls.Add(this.lblTotalCard);
            this.groupBox2.Controls.Add(this.lstCard);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(334, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 208);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Card";
            // 
            // lblTotalCard
            // 
            this.lblTotalCard.AutoSize = true;
            this.lblTotalCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCard.Location = new System.Drawing.Point(10, 184);
            this.lblTotalCard.Name = "lblTotalCard";
            this.lblTotalCard.Size = new System.Drawing.Size(54, 20);
            this.lblTotalCard.TabIndex = 54;
            this.lblTotalCard.Text = "Total:";
            // 
            // UCReportShiftViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "UCReportShiftViewer";
            this.Size = new System.Drawing.Size(722, 407);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label labelAccount;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label labelCreditNote;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label labelCash;
        public System.Windows.Forms.Label labelCard;
        public System.Windows.Forms.Label labelRefunc;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label labelTotalGST;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label labelTotalDiscount;
        public System.Windows.Forms.Label labelTotalSale;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.ColumnHeader columnHeader8;
        public System.Windows.Forms.ColumnHeader columnHeader7;
        public System.Windows.Forms.ListView lstCard;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label labelTotalAccount;
        public System.Windows.Forms.Label labelCashAccount;
        public System.Windows.Forms.Label labelCardAccount;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Label lblTotalCard;
        public System.Windows.Forms.Label lbto;
        public System.Windows.Forms.Label lbfrom;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbshiftid;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label1;

    }
}
