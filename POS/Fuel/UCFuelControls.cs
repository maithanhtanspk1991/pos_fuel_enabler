﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Fuel
{
    public partial class UCFuelControls : UserControl
    {
        Class.ReadConfig mReadConfig;
        Class.MoneyFortmat mMoneyFortmat;
        Forms.frmOrdersAll frmOrdersAll;
        private string _nameClass = "POS::Fuel::UCFuelControls::";
        List<FuelUse> lsFuel = new List<FuelUse>();
        public UCFuelControls()
        {
            InitializeComponent();

        }

        public void Init(Forms.frmOrdersAll frm, Class.ReadConfig readConfig, Class.MoneyFortmat moneyFortmat)
        {
            mReadConfig = readConfig;
            mMoneyFortmat = moneyFortmat;
            frmOrdersAll = frm;
            ChangeFuel();
        }

        public delegate void DelegateChangeStatus(string text);
        public delegate void AddItem(Class.ProcessOrderNew.Item item);
        public event DelegateChangeStatus OnChangeStatus;
        public event AddItem OnAddItem;
        private delegate void ChangeTextCallback(string text, Control control);

        public void SetText(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        private void ucPump_Click(object sender, EventArgs e)
        {
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "";
                UCPump mUCPump = (UCPump)sender;
                if (mUCPump.BackColor == Color.Red)
                {
                    sql = "SELECT fh.ID,fh.VolumeAmount,im.itemShort AS Name,fh.CashAmount FROM fuelhistory fh LEFT JOIN itemsmenu im ON im.itemID = fh.KindOfFuel where fh.IsComplete = 0 AND fh.PumID = " + mUCPump.PumpID;
                    string IDs = GetSqlListID(mUCPump.PumpID);
                    if (IDs != "")
                        sql += " And fh.ID not in (" + IDs + ")";
                    DataTable dtSource = conn.Select(sql);
                    if (dtSource.Rows.Count > 0)
                    {
                        Forms.frmSelectOfPump frmSelectOfPump = new Forms.frmSelectOfPump(dtSource, mMoneyFortmat);
                        if (frmSelectOfPump.ShowDialog() == DialogResult.OK)
                        {
                            int strPumIDHistory = Convert.ToInt32(frmSelectOfPump.strPumIDHistory);
                            string strQuery = "SELECT ID,PumID,VolumeAmount,CashAmount,FromPump,CableID FROM fuelhistory fh where fh.ID =" + strPumIDHistory;
                            dtSource = conn.Select(strQuery);

                            //Kiểm tra cable khác có sử dụng nó không
                            int CableID = dtSource.Rows[0]["CableID"].ToString() != "" ? Convert.ToInt32(dtSource.Rows[0]["CableID"]) : 0;
                            if (CableID != 0 && CableID != mReadConfig.iCableID)
                            {
                                OnChangeStatus("Other machines are being used");
                                return;
                            }

                            if (dtSource.Rows.Count > 0)
                            {
                                SetText(mMoneyFortmat.Format2(Convert.ToDouble(dtSource.Rows[0]["VolumeAmount"])) + " L", mUCPump.lbLitre);
                                SetText("$ " + mMoneyFortmat.Format2(Convert.ToDouble(dtSource.Rows[0]["CashAmount"])), mUCPump.lbTotal);
                                LoadDetailInformationFuelPump(mUCPump.PumpID, mUCPump.BackColor, strPumIDHistory);
                            }
                        }
                    }
                }
                else if ((mUCPump.BackColor == Color.Blue || mUCPump.BackColor == Color.LimeGreen) && mUCPump.BackColor != Color.Red)
                {
                    LoadDetailInformationFuelPump(mUCPump.PumpID, mUCPump.BackColor, 0);
                }
                if (mUCPump.BackColor == Color.White || mUCPump.BackColor == Color.LimeGreen)
                {
                    Forms.frmPump frm = new Forms.frmPump(mUCPump.lbPumpID.Text, mMoneyFortmat, frmOrdersAll);
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        mUCPump.lbLitre.Text = frm.strVolumeAmount + " L";
                        mUCPump.lbTotal.Text = "$ " + frm.strCashAmount;
                        mUCPump.BackColor = Color.LimeGreen;
                        ChangeColorPump(mUCPump.PumpID);
                    }
                }

            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ucPump_Click::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private string GetSqlListID(int PumpID)
        {
            string ressult = "";
            foreach (FuelUse f in lsFuel)
            {
                ressult += f.ID + ", ";
            }
            if (ressult != "")
            {
                ressult = ressult.Trim();
                ressult = ressult.Remove(ressult.Length - 1);
            }
            return ressult;
        }

        public void ChangeColorPump(int PumpID)
        {
            ChangeColorPump(PumpID, 0);
        }
        public void ChangeColorPump(int PumpID, int count)
        {
            bool clear = false;
            if (count == -1)
            {
                clear = true;
                count = 0;
            }
            int intCountRecord = count;
            if (intCountRecord == 0)
            {
                Connection.Connection conn = new Connection.Connection();
                try
                {
                    conn.Open();
                    string sql = "SELECT COUNT(f.ID) FROM fuelhistory f where f.IsComplete = 0 AND f.PumID = " + PumpID;
                    string IDs = GetSqlListID(PumpID);
                    if (IDs != "")
                        sql += " And f.ID not in (" + IDs + ")";
                    intCountRecord = Convert.ToInt32(conn.ExecuteScalar(sql));
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "ChangeColorPump::" + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }

            if (intCountRecord > 1)
            {
                ChangeColort(Color.Red, GetPumpByPumID(PumpID));
            }
            else if (intCountRecord == 1)
            {
                ChangeColort(Color.Blue, GetPumpByPumID(PumpID));
            }
            else if (clear)
            {
                ChangeColort(Color.White, GetPumpByPumID(PumpID));
            }
            else
            {
                ChangeColort(GetPumpByPumID(PumpID).BackColor, GetPumpByPumID(PumpID));
            }

        }
        private delegate void ChangeColorCallbackt(Color color, Control control);
        public void ChangeColort(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeColorCallbackt d = new ChangeColorCallbackt(ChangeColort);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.BackColor = color;
            }
        }

        private void LoadDetailInformationFuelPump(int PumpID, Color color, int ID)
        {
            Connection.Connection conn = new Connection.Connection();
            string sql = "";
            try
            {
                conn.Open();
                if ((color == Color.Blue || color == Color.LimeGreen) && ID == 0)
                {
                    sql = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice, fh.CableID FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + PumpID;
                    string IDs = GetSqlListID(PumpID);
                    if (IDs != "")
                        sql += " And fh.ID not in (" + IDs + ")";
                }
                else if ((color == Color.Blue || color == Color.LimeGreen) && ID != 0)
                {
                    sql = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice, fh.CableID FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + PumpID + " AND fh.ID =" + ID;
                }
                else if (color == Color.Red)
                {
                    sql = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice, fh.CableID FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + PumpID + " AND fh.ID =" + ID;
                }

                DataTable dtSource = conn.Select(sql);
                if (ID == 0)
                    ID = dtSource.Rows[0]["ID"].ToString() != "" ? Convert.ToInt32(dtSource.Rows[0]["ID"]) : 0;
                int CableID = dtSource.Rows[0]["CableID"].ToString() != "" ? Convert.ToInt32(dtSource.Rows[0]["CableID"]) : 0;
                if (CableID != 0 && CableID != mReadConfig.iCableID)
                {
                    OnChangeStatus("Other machines are being used");
                    return;
                }
                if (ID > 0)
                    BusinessObject.BOFuelHistory.UpdateCableIDByID(ID, mReadConfig.iCableID);

                foreach (FuelUse f in lsFuel)
                {
                    if (ID == f.ID && PumpID == f.PumpID)
                        return;
                }

                string KindName = dtSource.Rows[0]["KindName"].ToString();
                string GradeNo = dtSource.Rows[0]["KindOfFuel"].ToString();
                string CashAmount = dtSource.Rows[0]["CashAmount"].ToString();
                string VolumeAmount = dtSource.Rows[0]["VolumeAmount"].ToString();
                string UnitPrice = dtSource.Rows[0]["UnitPrice"].ToString();
                string GST = dtSource.Rows[0]["gst"].ToString();
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
                    ID,
                    Convert.ToInt32(GradeNo),
                    KindName,
                    Convert.ToInt32(VolumeAmount),
                    Convert.ToDouble(CashAmount),
                    Convert.ToDouble(UnitPrice),
                    Convert.ToDouble(GST),
                    0, 0,
                    PumpID.ToString());
                item.Weight = 1000;
                item.IsFuel = true;
                lsFuel.Add(new FuelUse() { ID = ID, PumpID = PumpID });
                OnAddItem(item);
                ChangeColorPump(PumpID);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadDetailInformationFuelPump::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public void ClearListFule(int ID)
        {
            if (lsFuel.Exists(s => s.ID == ID))
            {
                FuelUse item = lsFuel.Find(s => s.ID == ID);
                lsFuel.Remove(item);
                int PumpID = 0;
                PumpID = item.PumpID;
                ChangeColorPump(PumpID);
            }
        }

        public void ClearListFule()
        {
            lsFuel.Clear();
        }

        public void Complete()
        {
            foreach (FuelUse f in lsFuel)
            {

            }
            ClearListFule();
        }

        public void ChangeFuel()
        {
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "SELECT PumID,count(ID) AS NumRecord,FromPump, (Select count(*) FROM fuelhistory fh1 where fh1.IsComplete = 0 And CableID = " + mReadConfig.iCableID + " And fh1.PumID = fh.PumID) as UsePum FROM fuelhistory fh where fh.IsComplete = 0 group by PumID";
                System.Data.DataTable dt = conn.Select(sql);

                foreach (Control item in flpFuel.Controls)
                {
                    ChangeColort(Color.White, item);
                }

                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        int PumID = Convert.ToInt32(row["PumID"]);
                        UCPump uc = GetPumpByPumID(PumID);

                        sql = "SELECT ID,PumID,CashAmount,VolumeAmount,UnitPrice,Gst,FromPump FROM fuelhistory fh where fh.IsComplete = 0 and fh.FromPump = 1 and fh.PumID = " + uc.PumpID;
                        string IDs = GetSqlListID(PumID);
                        if (IDs != "")
                            sql += " And fh.ID not in (" + IDs + ")";
                        System.Data.DataTable dt1 = conn.Select(sql);
                        SetText(mMoneyFortmat.Format2(dt1.Rows[0]["VolumeAmount"].ToString()) + " L", uc.lbLitre);
                        SetText("$ " + mMoneyFortmat.Format2(dt1.Rows[0]["CashAmount"].ToString()), uc.lbTotal);
                        ChangeColorPump(PumID, Convert.ToInt32(row["NumRecord"]));
                        #region
                        //if (Convert.ToInt32(row["FromPump"]) == 1 && Convert.ToInt32(row["NumRecord"]) == 1)
                        //{
                        //    ChangeColort(Color.Blue, uc);
                        //}
                        //else if (Convert.ToInt32(row["FromPump"]) == 0 && Convert.ToInt32(row["NumRecord"]) == 1)
                        //{
                        //    ChangeColort(Color.LimeGreen, uc);
                        //}
                        //else if (Convert.ToInt32(row["FromPump"]) == 1 && Convert.ToInt32(row["NumRecord"]) != 1)
                        //{
                        //    if (Convert.ToInt32(row["UsePum"]) != 0)
                        //        ChangeColort(Color.Blue, uc);
                        //    else
                        //        ChangeColort(Color.Red, uc);
                        //}
                        //else if (Convert.ToInt32(row["FromPump"]) == 0 && Convert.ToInt32(row["NumRecord"]) != 1)
                        //{
                        //    if (Convert.ToInt32(row["UsePum"]) != 0)
                        //        ChangeColort(Color.Blue, uc);
                        //    else
                        //        ChangeColort(Color.Red, uc);
                        //}
                        #endregion 
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "ChangeFuel::" + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ChangeFuel::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private UCPump GetPumpByPumID(int ID)
        {
            UCPump result = null;
            switch (ID)
            {
                case 1:
                    result = ucPump1;
                    break;
                case 2:
                    result = ucPump2;
                    break;
                case 3:
                    result = ucPump3;
                    break;
                case 4:
                    result = ucPump4;
                    break;
                case 5:
                    result = ucPump5;
                    break;
                case 6:
                    result = ucPump6;
                    break;
                case 7:
                    result = ucPump7;
                    break;
                case 8:
                    result = ucPump8;
                    break;
                case 9:
                    result = ucPump9;
                    break;
                case 10:
                    result = ucPump10;
                    break;
                case 11:
                    result = ucPump11;
                    break;
                case 12:
                    result = ucPump12;
                    break;
                case 13:
                    result = ucPump13;
                    break;
                case 14:
                    result = ucPump14;
                    break;
                case 15:
                    result = ucPump15;
                    break;
                case 16:
                    result = ucPump16;
                    break;

            }
            return result;
        }
    }

    class FuelUse
    {
        public int ID { get; set; }
        public int PumpID { get; set; }
    }
}
