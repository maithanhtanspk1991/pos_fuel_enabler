﻿namespace POS.Fuel
{
    partial class UCFuelControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpFuel = new System.Windows.Forms.TableLayoutPanel();
            this.ucPump16 = new POS.UCPump();
            this.ucPump15 = new POS.UCPump();
            this.ucPump14 = new POS.UCPump();
            this.ucPump13 = new POS.UCPump();
            this.ucPump12 = new POS.UCPump();
            this.ucPump11 = new POS.UCPump();
            this.ucPump10 = new POS.UCPump();
            this.ucPump9 = new POS.UCPump();
            this.ucPump8 = new POS.UCPump();
            this.ucPump7 = new POS.UCPump();
            this.ucPump6 = new POS.UCPump();
            this.ucPump5 = new POS.UCPump();
            this.ucPump4 = new POS.UCPump();
            this.ucPump3 = new POS.UCPump();
            this.ucPump2 = new POS.UCPump();
            this.ucPump1 = new POS.UCPump();
            this.flpFuel.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpFuel
            // 
            this.flpFuel.ColumnCount = 16;
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flpFuel.Controls.Add(this.ucPump16, 15, 0);
            this.flpFuel.Controls.Add(this.ucPump15, 14, 0);
            this.flpFuel.Controls.Add(this.ucPump14, 13, 0);
            this.flpFuel.Controls.Add(this.ucPump13, 12, 0);
            this.flpFuel.Controls.Add(this.ucPump12, 11, 0);
            this.flpFuel.Controls.Add(this.ucPump11, 10, 0);
            this.flpFuel.Controls.Add(this.ucPump10, 9, 0);
            this.flpFuel.Controls.Add(this.ucPump9, 8, 0);
            this.flpFuel.Controls.Add(this.ucPump8, 7, 0);
            this.flpFuel.Controls.Add(this.ucPump7, 6, 0);
            this.flpFuel.Controls.Add(this.ucPump6, 5, 0);
            this.flpFuel.Controls.Add(this.ucPump5, 4, 0);
            this.flpFuel.Controls.Add(this.ucPump4, 3, 0);
            this.flpFuel.Controls.Add(this.ucPump3, 2, 0);
            this.flpFuel.Controls.Add(this.ucPump2, 1, 0);
            this.flpFuel.Controls.Add(this.ucPump1, 0, 0);
            this.flpFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpFuel.Location = new System.Drawing.Point(0, 0);
            this.flpFuel.Name = "flpFuel";
            this.flpFuel.RowCount = 1;
            this.flpFuel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.flpFuel.Size = new System.Drawing.Size(994, 68);
            this.flpFuel.TabIndex = 0;
            // 
            // ucPump16
            // 
            this.ucPump16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump16.Location = new System.Drawing.Point(930, 0);
            this.ucPump16.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump16.Name = "ucPump16";
            this.ucPump16.PumpID = 16;
            this.ucPump16.Size = new System.Drawing.Size(64, 68);
            this.ucPump16.TabIndex = 15;
            this.ucPump16.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump15
            // 
            this.ucPump15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump15.Location = new System.Drawing.Point(868, 0);
            this.ucPump15.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump15.Name = "ucPump15";
            this.ucPump15.PumpID = 15;
            this.ucPump15.Size = new System.Drawing.Size(62, 68);
            this.ucPump15.TabIndex = 14;
            this.ucPump15.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump14
            // 
            this.ucPump14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump14.Location = new System.Drawing.Point(806, 0);
            this.ucPump14.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump14.Name = "ucPump14";
            this.ucPump14.PumpID = 14;
            this.ucPump14.Size = new System.Drawing.Size(62, 68);
            this.ucPump14.TabIndex = 13;
            this.ucPump14.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump13
            // 
            this.ucPump13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump13.Location = new System.Drawing.Point(744, 0);
            this.ucPump13.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump13.Name = "ucPump13";
            this.ucPump13.PumpID = 13;
            this.ucPump13.Size = new System.Drawing.Size(62, 68);
            this.ucPump13.TabIndex = 12;
            this.ucPump13.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump12
            // 
            this.ucPump12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump12.Location = new System.Drawing.Point(682, 0);
            this.ucPump12.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump12.Name = "ucPump12";
            this.ucPump12.PumpID = 12;
            this.ucPump12.Size = new System.Drawing.Size(62, 68);
            this.ucPump12.TabIndex = 11;
            this.ucPump12.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump11
            // 
            this.ucPump11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump11.Location = new System.Drawing.Point(620, 0);
            this.ucPump11.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump11.Name = "ucPump11";
            this.ucPump11.PumpID = 11;
            this.ucPump11.Size = new System.Drawing.Size(62, 68);
            this.ucPump11.TabIndex = 10;
            this.ucPump11.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump10
            // 
            this.ucPump10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump10.Location = new System.Drawing.Point(558, 0);
            this.ucPump10.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump10.Name = "ucPump10";
            this.ucPump10.PumpID = 10;
            this.ucPump10.Size = new System.Drawing.Size(62, 68);
            this.ucPump10.TabIndex = 9;
            this.ucPump10.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump9
            // 
            this.ucPump9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump9.Location = new System.Drawing.Point(496, 0);
            this.ucPump9.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump9.Name = "ucPump9";
            this.ucPump9.PumpID = 9;
            this.ucPump9.Size = new System.Drawing.Size(62, 68);
            this.ucPump9.TabIndex = 8;
            this.ucPump9.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump8
            // 
            this.ucPump8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump8.Location = new System.Drawing.Point(434, 0);
            this.ucPump8.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump8.Name = "ucPump8";
            this.ucPump8.PumpID = 8;
            this.ucPump8.Size = new System.Drawing.Size(62, 68);
            this.ucPump8.TabIndex = 7;
            this.ucPump8.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump7
            // 
            this.ucPump7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump7.Location = new System.Drawing.Point(372, 0);
            this.ucPump7.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump7.Name = "ucPump7";
            this.ucPump7.PumpID = 7;
            this.ucPump7.Size = new System.Drawing.Size(62, 68);
            this.ucPump7.TabIndex = 6;
            this.ucPump7.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump6
            // 
            this.ucPump6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump6.Location = new System.Drawing.Point(310, 0);
            this.ucPump6.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump6.Name = "ucPump6";
            this.ucPump6.PumpID = 6;
            this.ucPump6.Size = new System.Drawing.Size(62, 68);
            this.ucPump6.TabIndex = 5;
            this.ucPump6.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump5
            // 
            this.ucPump5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump5.Location = new System.Drawing.Point(248, 0);
            this.ucPump5.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump5.Name = "ucPump5";
            this.ucPump5.PumpID = 5;
            this.ucPump5.Size = new System.Drawing.Size(62, 68);
            this.ucPump5.TabIndex = 4;
            this.ucPump5.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump4
            // 
            this.ucPump4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump4.Location = new System.Drawing.Point(186, 0);
            this.ucPump4.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump4.Name = "ucPump4";
            this.ucPump4.PumpID = 4;
            this.ucPump4.Size = new System.Drawing.Size(62, 68);
            this.ucPump4.TabIndex = 3;
            this.ucPump4.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump3
            // 
            this.ucPump3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump3.Location = new System.Drawing.Point(124, 0);
            this.ucPump3.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump3.Name = "ucPump3";
            this.ucPump3.PumpID = 3;
            this.ucPump3.Size = new System.Drawing.Size(62, 68);
            this.ucPump3.TabIndex = 2;
            this.ucPump3.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump2
            // 
            this.ucPump2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump2.Location = new System.Drawing.Point(62, 0);
            this.ucPump2.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump2.Name = "ucPump2";
            this.ucPump2.PumpID = 2;
            this.ucPump2.Size = new System.Drawing.Size(62, 68);
            this.ucPump2.TabIndex = 1;
            this.ucPump2.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // ucPump1
            // 
            this.ucPump1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPump1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPump1.Location = new System.Drawing.Point(0, 0);
            this.ucPump1.Margin = new System.Windows.Forms.Padding(0);
            this.ucPump1.Name = "ucPump1";
            this.ucPump1.PumpID = 1;
            this.ucPump1.Size = new System.Drawing.Size(62, 68);
            this.ucPump1.TabIndex = 0;
            this.ucPump1.Click += new System.EventHandler(this.ucPump_Click);
            // 
            // UCFuelControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flpFuel);
            this.Name = "UCFuelControls";
            this.Size = new System.Drawing.Size(994, 68);
            this.flpFuel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UCPump ucPump1;
        private UCPump ucPump16;
        private UCPump ucPump15;
        private UCPump ucPump14;
        private UCPump ucPump13;
        private UCPump ucPump12;
        private UCPump ucPump11;
        private UCPump ucPump10;
        private UCPump ucPump9;
        private UCPump ucPump8;
        private UCPump ucPump7;
        private UCPump ucPump6;
        private UCPump ucPump5;
        private UCPump ucPump4;
        private UCPump ucPump3;
        private UCPump ucPump2;
        public System.Windows.Forms.TableLayoutPanel flpFuel;
    }
}
