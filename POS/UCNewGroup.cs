﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCNewGroup : UserControl
    {
        private Connection.Connection conn = new Connection.Connection();

        public UCNewGroup()
        {
            InitializeComponent();
        }

        public void GetValue(string txtre)
        {
            textBox1.Text = txtre;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            textBox1.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox6_Leave(object sender, EventArgs e)
        {
            textBox6.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox1);
            textBox1.Tag = textBox1.BackColor;
            textBox1.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox6_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox6);
            textBox6.Tag = textBox6.BackColor;
            textBox6.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void lbvisual_Click(object sender, EventArgs e)
        {
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBox2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = conn.Select("select groupID,groupDesc from groupsmenu where `visual`=0 and isFF=1 and deleted = 0");
            DataRow row = dt.NewRow();
            row["groupID"] = 0;
            row["groupDesc"] = "None";
            dt.Rows.InsertAt(row, 0);
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "groupDesc";
            comboBox2.ValueMember = "groupID";
        }
    }
}