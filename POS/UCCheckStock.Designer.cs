﻿namespace POS
{
    partial class UCCheckStock
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPOS_OSK_Quantity = new POS.TextBoxPOS_OSK();
            this.uCkeypad1 = new POS.UCkeypad();
            this.textBoxPOS_OSK_ItemID = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_ItemName = new POS.TextBoxPOS_OSK();
            this.ucItem1 = new POS.UCItem();
            this.textBoxPOS_OSK_Area = new POS.TextBoxPOS_OSK();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonStockTake = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(612, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Item Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(612, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "ItemID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(612, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Quantity";
            // 
            // textBoxPOS_OSK_Quantity
            // 
            this.textBoxPOS_OSK_Quantity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Quantity.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Quantity.KeyPad = this.uCkeypad1;
            this.textBoxPOS_OSK_Quantity.Location = new System.Drawing.Point(694, 143);
            this.textBoxPOS_OSK_Quantity.Multiline = true;
            this.textBoxPOS_OSK_Quantity.Name = "textBoxPOS_OSK_Quantity";
            this.textBoxPOS_OSK_Quantity.Size = new System.Drawing.Size(131, 40);
            this.textBoxPOS_OSK_Quantity.TabIndex = 1;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(615, 285);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 4;
            this.uCkeypad1.txtResult = null;
            // 
            // textBoxPOS_OSK_ItemID
            // 
            this.textBoxPOS_OSK_ItemID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_ItemID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_ItemID.KeyBoadOSK = null;
            this.textBoxPOS_OSK_ItemID.KeyPad = null;
            this.textBoxPOS_OSK_ItemID.Location = new System.Drawing.Point(694, 51);
            this.textBoxPOS_OSK_ItemID.Multiline = true;
            this.textBoxPOS_OSK_ItemID.Name = "textBoxPOS_OSK_ItemID";
            this.textBoxPOS_OSK_ItemID.ReadOnly = true;
            this.textBoxPOS_OSK_ItemID.Size = new System.Drawing.Size(131, 40);
            this.textBoxPOS_OSK_ItemID.TabIndex = 1;
            // 
            // textBoxPOS_OSK_ItemName
            // 
            this.textBoxPOS_OSK_ItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_ItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_ItemName.KeyBoadOSK = null;
            this.textBoxPOS_OSK_ItemName.KeyPad = null;
            this.textBoxPOS_OSK_ItemName.Location = new System.Drawing.Point(694, 97);
            this.textBoxPOS_OSK_ItemName.Multiline = true;
            this.textBoxPOS_OSK_ItemName.Name = "textBoxPOS_OSK_ItemName";
            this.textBoxPOS_OSK_ItemName.ReadOnly = true;
            this.textBoxPOS_OSK_ItemName.Size = new System.Drawing.Size(131, 40);
            this.textBoxPOS_OSK_ItemName.TabIndex = 1;
            // 
            // ucItem1
            // 
            this.ucItem1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ucItem1.Location = new System.Drawing.Point(0, 0);
            this.ucItem1.Name = "ucItem1";
            this.ucItem1.Size = new System.Drawing.Size(604, 561);
            this.ucItem1.TabIndex = 0;
            // 
            // textBoxPOS_OSK_Area
            // 
            this.textBoxPOS_OSK_Area.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Area.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Area.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Area.KeyPad = null;
            this.textBoxPOS_OSK_Area.Location = new System.Drawing.Point(694, 6);
            this.textBoxPOS_OSK_Area.Multiline = true;
            this.textBoxPOS_OSK_Area.Name = "textBoxPOS_OSK_Area";
            this.textBoxPOS_OSK_Area.ReadOnly = true;
            this.textBoxPOS_OSK_Area.Size = new System.Drawing.Size(131, 40);
            this.textBoxPOS_OSK_Area.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(612, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Area";
            // 
            // buttonStockTake
            // 
            this.buttonStockTake.Location = new System.Drawing.Point(615, 200);
            this.buttonStockTake.Name = "buttonStockTake";
            this.buttonStockTake.Size = new System.Drawing.Size(210, 80);
            this.buttonStockTake.TabIndex = 3;
            this.buttonStockTake.Text = "Stock Take";
            this.buttonStockTake.UseVisualStyleBackColor = true;
            this.buttonStockTake.Click += new System.EventHandler(this.buttonStockTake_Click);
            // 
            // UCCheckStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uCkeypad1);
            this.Controls.Add(this.buttonStockTake);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPOS_OSK_Quantity);
            this.Controls.Add(this.textBoxPOS_OSK_Area);
            this.Controls.Add(this.textBoxPOS_OSK_ItemID);
            this.Controls.Add(this.textBoxPOS_OSK_ItemName);
            this.Controls.Add(this.ucItem1);
            this.Name = "UCCheckStock";
            this.Size = new System.Drawing.Size(838, 561);
            this.Load += new System.EventHandler(this.UCCheckStock_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public UCItem ucItem1;
        private TextBoxPOS_OSK textBoxPOS_OSK_ItemName;
        private System.Windows.Forms.Label label1;
        private TextBoxPOS_OSK textBoxPOS_OSK_ItemID;
        private System.Windows.Forms.Label label2;
        private TextBoxPOS_OSK textBoxPOS_OSK_Quantity;
        private System.Windows.Forms.Label label3;
        private TextBoxPOS_OSK textBoxPOS_OSK_Area;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonStockTake;
        private UCkeypad uCkeypad1;

    }
}
