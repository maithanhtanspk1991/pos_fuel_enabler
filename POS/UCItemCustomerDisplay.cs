﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS
{
    public partial class UCItemCustomerDisplay : UserControl
    {
        public UCItemCustomerDisplay()
        {
            InitializeComponent();
            lbItemName.Text = "";
            lbQty.Text = "";
            lbSubtotal.Text = "";
        }

        public int Index { get; set; }
        public string ItemName { set { lbItemName.Text = value; } }
        public string Qty { set { lbQty.Text = value; } }
        public string Subtotal { set { lbSubtotal.Text = value; } }
    }
}
