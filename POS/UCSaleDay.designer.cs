﻿namespace POS
{
    partial class UCSaleDay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReadmore = new System.Windows.Forms.Button();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTotalShopSales = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelSurcharge = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelAccount = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblTotalFuelSales = new System.Windows.Forms.Label();
            this.labelCashIn = new System.Windows.Forms.Label();
            this.labelPayOut = new System.Windows.Forms.Label();
            this.labelCashOut = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotalSale = new System.Windows.Forms.Label();
            this.labelTotalDiscount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelRefunc = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTotalGST = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCash = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelCard = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTotalCardSurcharge = new System.Windows.Forms.Label();
            this.lblTotalCard = new System.Windows.Forms.Label();
            this.lstCard = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTotalAccount = new System.Windows.Forms.Label();
            this.labelCashAccount = new System.Windows.Forms.Label();
            this.labelCardAccount = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lstReportShift = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnReadmore
            // 
            this.btnReadmore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReadmore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReadmore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadmore.Location = new System.Drawing.Point(0, 0);
            this.btnReadmore.Name = "btnReadmore";
            this.btnReadmore.Size = new System.Drawing.Size(474, 777);
            this.btnReadmore.TabIndex = 6;
            this.btnReadmore.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReadmore.UseVisualStyleBackColor = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(270, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(187, 39);
            this.lblDate.TabIndex = 8;
            this.lblDate.Text = "26/09/2011";
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDay.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblDay.Location = new System.Drawing.Point(15, 18);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(442, 38);
            this.lblDay.TabIndex = 7;
            this.lblDay.Text = "Monday";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.lblTotalShopSales);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.labelSurcharge);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.labelAccount);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblTotalFuelSales);
            this.groupBox1.Controls.Add(this.labelCashIn);
            this.groupBox1.Controls.Add(this.labelPayOut);
            this.groupBox1.Controls.Add(this.labelCashOut);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.labelTotalSale);
            this.groupBox1.Controls.Add(this.labelTotalDiscount);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.labelRefunc);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.labelTotalGST);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.labelCash);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.labelCard);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 291);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sales";
            // 
            // lblTotalShopSales
            // 
            this.lblTotalShopSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalShopSales.Location = new System.Drawing.Point(268, 64);
            this.lblTotalShopSales.Name = "lblTotalShopSales";
            this.lblTotalShopSales.Size = new System.Drawing.Size(170, 20);
            this.lblTotalShopSales.TabIndex = 59;
            this.lblTotalShopSales.Text = "0.00";
            this.lblTotalShopSales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(19, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(246, 20);
            this.label12.TabIndex = 58;
            this.label12.Text = "Total Shop Sales:";
            // 
            // labelSurcharge
            // 
            this.labelSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSurcharge.Location = new System.Drawing.Point(268, 258);
            this.labelSurcharge.Name = "labelSurcharge";
            this.labelSurcharge.Size = new System.Drawing.Size(170, 20);
            this.labelSurcharge.TabIndex = 59;
            this.labelSurcharge.Text = "0.00";
            this.labelSurcharge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 258);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(246, 20);
            this.label11.TabIndex = 58;
            this.label11.Text = "Surcharge:";
            // 
            // labelAccount
            // 
            this.labelAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAccount.Location = new System.Drawing.Point(268, 239);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(170, 20);
            this.labelAccount.TabIndex = 59;
            this.labelAccount.Text = "0.00";
            this.labelAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(19, 239);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(246, 20);
            this.label13.TabIndex = 58;
            this.label13.Text = "Account:";
            // 
            // lblTotalFuelSales
            // 
            this.lblTotalFuelSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalFuelSales.Location = new System.Drawing.Point(264, 45);
            this.lblTotalFuelSales.Name = "lblTotalFuelSales";
            this.lblTotalFuelSales.Size = new System.Drawing.Size(174, 20);
            this.lblTotalFuelSales.TabIndex = 57;
            this.lblTotalFuelSales.Text = "0.00";
            this.lblTotalFuelSales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCashIn
            // 
            this.labelCashIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashIn.Location = new System.Drawing.Point(264, 181);
            this.labelCashIn.Name = "labelCashIn";
            this.labelCashIn.Size = new System.Drawing.Size(174, 20);
            this.labelCashIn.TabIndex = 56;
            this.labelCashIn.Text = "0.00";
            this.labelCashIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPayOut
            // 
            this.labelPayOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPayOut.Location = new System.Drawing.Point(264, 221);
            this.labelPayOut.Name = "labelPayOut";
            this.labelPayOut.Size = new System.Drawing.Size(174, 20);
            this.labelPayOut.TabIndex = 57;
            this.labelPayOut.Text = "0.00";
            this.labelPayOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCashOut
            // 
            this.labelCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashOut.Location = new System.Drawing.Point(264, 201);
            this.labelCashOut.Name = "labelCashOut";
            this.labelCashOut.Size = new System.Drawing.Size(174, 20);
            this.labelCashOut.TabIndex = 57;
            this.labelCashOut.Text = "0.00";
            this.labelCashOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(246, 20);
            this.label6.TabIndex = 55;
            this.label6.Text = "Total Fuel Sales:";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 221);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(246, 20);
            this.label14.TabIndex = 55;
            this.label14.Text = "Pay Out:";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 20);
            this.label1.TabIndex = 54;
            this.label1.Text = "Cash In:";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(246, 20);
            this.label8.TabIndex = 55;
            this.label8.Text = "Cash Out:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total Sales:";
            // 
            // labelTotalSale
            // 
            this.labelTotalSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalSale.Location = new System.Drawing.Point(147, 25);
            this.labelTotalSale.Name = "labelTotalSale";
            this.labelTotalSale.Size = new System.Drawing.Size(291, 20);
            this.labelTotalSale.TabIndex = 44;
            this.labelTotalSale.Text = "0.00";
            this.labelTotalSale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTotalDiscount
            // 
            this.labelTotalDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalDiscount.Location = new System.Drawing.Point(147, 81);
            this.labelTotalDiscount.Name = "labelTotalDiscount";
            this.labelTotalDiscount.Size = new System.Drawing.Size(291, 20);
            this.labelTotalDiscount.TabIndex = 45;
            this.labelTotalDiscount.Text = "0.00";
            this.labelTotalDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Discount:";
            // 
            // labelRefunc
            // 
            this.labelRefunc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRefunc.Location = new System.Drawing.Point(147, 161);
            this.labelRefunc.Name = "labelRefunc";
            this.labelRefunc.Size = new System.Drawing.Size(291, 20);
            this.labelRefunc.TabIndex = 49;
            this.labelRefunc.Text = "0.00";
            this.labelRefunc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Total GST:";
            // 
            // labelTotalGST
            // 
            this.labelTotalGST.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalGST.Location = new System.Drawing.Point(147, 101);
            this.labelTotalGST.Name = "labelTotalGST";
            this.labelTotalGST.Size = new System.Drawing.Size(291, 20);
            this.labelTotalGST.TabIndex = 46;
            this.labelTotalGST.Text = "0.00";
            this.labelTotalGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(246, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Card:";
            // 
            // labelCash
            // 
            this.labelCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCash.Location = new System.Drawing.Point(147, 141);
            this.labelCash.Name = "labelCash";
            this.labelCash.Size = new System.Drawing.Size(291, 20);
            this.labelCash.TabIndex = 48;
            this.labelCash.Text = "0.00";
            this.labelCash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(246, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Cash:";
            // 
            // labelCard
            // 
            this.labelCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCard.Location = new System.Drawing.Point(147, 121);
            this.labelCard.Name = "labelCard";
            this.labelCard.Size = new System.Drawing.Size(291, 20);
            this.labelCard.TabIndex = 47;
            this.labelCard.Text = "0.00";
            this.labelCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(246, 20);
            this.label9.TabIndex = 2;
            this.label9.Text = "Refund:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox2.Controls.Add(this.lblTotalCardSurcharge);
            this.groupBox2.Controls.Add(this.lblTotalCard);
            this.groupBox2.Controls.Add(this.lstCard);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(13, 604);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(444, 161);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Card";
            // 
            // lblTotalCardSurcharge
            // 
            this.lblTotalCardSurcharge.AutoSize = true;
            this.lblTotalCardSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCardSurcharge.Location = new System.Drawing.Point(211, 138);
            this.lblTotalCardSurcharge.Name = "lblTotalCardSurcharge";
            this.lblTotalCardSurcharge.Size = new System.Drawing.Size(54, 20);
            this.lblTotalCardSurcharge.TabIndex = 54;
            this.lblTotalCardSurcharge.Text = "Total:";
            // 
            // lblTotalCard
            // 
            this.lblTotalCard.AutoSize = true;
            this.lblTotalCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCard.Location = new System.Drawing.Point(4, 138);
            this.lblTotalCard.Name = "lblTotalCard";
            this.lblTotalCard.Size = new System.Drawing.Size(54, 20);
            this.lblTotalCard.TabIndex = 54;
            this.lblTotalCard.Text = "Total:";
            // 
            // lstCard
            // 
            this.lstCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader3});
            this.lstCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCard.FullRowSelect = true;
            this.lstCard.GridLines = true;
            this.lstCard.HideSelection = false;
            this.lstCard.HoverSelection = true;
            this.lstCard.Location = new System.Drawing.Point(6, 29);
            this.lstCard.MultiSelect = false;
            this.lstCard.Name = "lstCard";
            this.lstCard.Size = new System.Drawing.Size(432, 95);
            this.lstCard.TabIndex = 53;
            this.lstCard.UseCompatibleStateImageBehavior = false;
            this.lstCard.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Card Name";
            this.columnHeader7.Width = 154;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Subtotal";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader8.Width = 146;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Surcharge";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 120;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.labelTotalAccount);
            this.groupBox3.Controls.Add(this.labelCashAccount);
            this.groupBox3.Controls.Add(this.labelCardAccount);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(13, 351);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(444, 112);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Account Payment";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 81);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 20);
            this.label23.TabIndex = 45;
            this.label23.Text = "Total";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 20);
            this.label16.TabIndex = 45;
            this.label16.Text = "Cash";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 20);
            this.label10.TabIndex = 45;
            this.label10.Text = "Card";
            // 
            // labelTotalAccount
            // 
            this.labelTotalAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAccount.Location = new System.Drawing.Point(264, 81);
            this.labelTotalAccount.Name = "labelTotalAccount";
            this.labelTotalAccount.Size = new System.Drawing.Size(174, 20);
            this.labelTotalAccount.TabIndex = 46;
            this.labelTotalAccount.Text = "0.00";
            this.labelTotalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCashAccount
            // 
            this.labelCashAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashAccount.Location = new System.Drawing.Point(264, 53);
            this.labelCashAccount.Name = "labelCashAccount";
            this.labelCashAccount.Size = new System.Drawing.Size(174, 20);
            this.labelCashAccount.TabIndex = 46;
            this.labelCashAccount.Text = "0.00";
            this.labelCashAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCardAccount
            // 
            this.labelCardAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardAccount.Location = new System.Drawing.Point(264, 25);
            this.labelCardAccount.Name = "labelCardAccount";
            this.labelCardAccount.Size = new System.Drawing.Size(174, 20);
            this.labelCardAccount.TabIndex = 46;
            this.labelCardAccount.Text = "0.00";
            this.labelCardAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox4.Controls.Add(this.lstReportShift);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(13, 459);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(444, 149);
            this.groupBox4.TabIndex = 57;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Shift Report";
            // 
            // lstReportShift
            // 
            this.lstReportShift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstReportShift.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstReportShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReportShift.FullRowSelect = true;
            this.lstReportShift.GridLines = true;
            this.lstReportShift.HideSelection = false;
            this.lstReportShift.HoverSelection = true;
            this.lstReportShift.Location = new System.Drawing.Point(6, 29);
            this.lstReportShift.MultiSelect = false;
            this.lstReportShift.Name = "lstReportShift";
            this.lstReportShift.Size = new System.Drawing.Size(432, 111);
            this.lstReportShift.TabIndex = 53;
            this.lstReportShift.UseCompatibleStateImageBehavior = false;
            this.lstReportShift.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Shift";
            this.columnHeader1.Width = 257;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Balance Count";
            this.columnHeader2.Width = 148;
            // 
            // UCSaleDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblDay);
            this.Controls.Add(this.btnReadmore);
            this.Name = "UCSaleDay";
            this.Size = new System.Drawing.Size(474, 777);
            this.Load += new System.EventHandler(this.UCSaleDay_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblDate;
        public System.Windows.Forms.Label lblDay;
        public System.Windows.Forms.Button btnReadmore;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label labelTotalSale;
        public System.Windows.Forms.Label labelTotalDiscount;
        public System.Windows.Forms.Label labelRefunc;
        public System.Windows.Forms.Label labelTotalGST;
        public System.Windows.Forms.Label labelCash;
        public System.Windows.Forms.Label labelCard;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        public System.Windows.Forms.Label lblTotalCard;
        public System.Windows.Forms.ListView lstCard;
        public System.Windows.Forms.Label labelCashIn;
        public System.Windows.Forms.Label labelCashOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label labelTotalAccount;
        public System.Windows.Forms.Label labelCashAccount;
        public System.Windows.Forms.Label labelCardAccount;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.ListView lstReportShift;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        public System.Windows.Forms.Label lblTotalShopSales;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label lblTotalFuelSales;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label labelSurcharge;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        public System.Windows.Forms.Label lblTotalCardSurcharge;
        public System.Windows.Forms.Label labelPayOut;
        private System.Windows.Forms.Label label14;
    }
}
