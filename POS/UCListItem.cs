﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS
{
    public partial class UCListItem : UserControl
    {
        private Connection.Connection mConnection = new Connection.Connection();
        private int comboDiscRuleID,comboDiscID;
        Class.MoneyFortmat mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        Forms.frmComboDisc _frmComboDisc;

        public UCListItem(int ruleID,int discID,Forms.frmComboDisc frm)
        {
            InitializeComponent();
            comboDiscRuleID = ruleID;
            comboDiscID = discID;
            _frmComboDisc = frm;
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                mConnection.Open();     
                foreach (ListViewItem item in lvListItem.Items)
                {
                    if (item.Checked)
                    {
                        Class.ItemComboDisc itemComboDisc = (Class.ItemComboDisc)item.Tag;
                        string sqlDelete = "delete from combodiscountitem where id = " + itemComboDisc.Id;
                        mConnection.ExecuteNonQuery(sqlDelete);                                                
                    }
                }
                LoadListItem();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
            
        }

        private void btnUpdateQty_Click(object sender, EventArgs e)
        {
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                string newQty = txtQuantity.Text.Trim();
                if (newQty.Length > 0 && !newQty.Equals(txtQuantity.Tag))
                {
                    //Không trùng quantity
                    //string sqltest = "SELECT id FROM combodiscountrule c where qty = " + newQty + " and comboDiscountId=" + comboDiscID + " limit 0,1";
                    //DataTable tbltest = mConnection.Select(sqltest);
                    //if (tbltest.Rows.Count > 0)
                    //{
                    //    string sqlItemTest = "select * from combodiscountitem where comboDiscountRuleId = " + tbltest.Rows[0]["id"].ToString();
                    //    DataTable tblItem = mConnection.Select(sqlItemTest);
                    //    foreach (DataRow row in tblItem.Rows)
                    //    {
                    //        string sqlDeleteItem = "delete from combodiscountitem where comboDiscountRuleId = " + comboDiscRuleID + " and itemId = " + row["itemId"];
                    //        mConnection.ExecuteNonQuery(sqlDeleteItem);
                    //    }
                    //    mConnection.Commit();
                    //    string sql = "UPDATE combodiscountitem SET comboDiscountRuleId = " + tbltest.Rows[0]["id"].ToString() + " where comboDiscountRuleId = " + comboDiscRuleID;
                    //    mConnection.ExecuteNonQuery(sql);

                    //    sql = "delete from combodiscountrule where id = " + comboDiscRuleID;
                    //    mConnection.ExecuteNonQuery(sql);                        
                    //    _frmComboDisc.LoadQuantity(comboDiscID);
                    //}
                    //else
                    //{
                    //    string sqlUpdateQty = "UPDATE combodiscountrule SET qty = " + newQty + " where id=" + comboDiscRuleID;
                    //    mConnection.ExecuteNonQuery(sqlUpdateQty);
                    //    mConnection.Commit();
                    //    txtQuantity.Tag = newQty;
                    //}

                    //Quatity có thể trùng
                    string sqlUpdateQty = "UPDATE combodiscountrule SET qty = " + newQty + " where id=" + comboDiscRuleID;
                    mConnection.ExecuteNonQuery(sqlUpdateQty);
                    mConnection.Commit();
                    txtQuantity.Tag = newQty;
                   
                }
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            Forms.frmSearchBarCode frm = new Forms.frmSearchBarCode(_frmComboDisc.mSerialPort, DataObject.FindItemType.FindItemInOrder,comboDiscRuleID);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mConnection.Open();
                    for (int i = 0; i < frm.cboListItem.Length; i++)
                    {
                        if(frm.cboListItem[i] == null)
                            break;
                        Class.ItemComboDisc item = new Class.ItemComboDisc(frm.cboListItem[i].ItemID, frm.cboListItem[i].ItemName, frm.cboListItem[i].Price);
                        //Kiểm tra xem item đã tồn tại trong rule nào chưa
                        string sqlCheckItem = "select ci.id from combodiscountrule cr " +
                                              "inner join combodiscount c on cr.comboDiscountId = c.id " +
                                              "inner join combodiscountitem ci on ci.comboDiscountRuleId=cr.id " +
                                              "where c.id=" + comboDiscID + " and ci.itemId=" + item.ItemID;
                        object mObj = mConnection.ExecuteScalar(sqlCheckItem);
                        if (mObj != null)
                            continue;
                        
                        if (!CheckListItem(item))
                        {
                            mConnection.ExecuteNonQuery("insert into combodiscountitem(comboDiscountRuleId, itemId) values(" + comboDiscRuleID + "," + item.ItemID + ")");
                            item.Id = Convert.ToInt32(mConnection.ExecuteScalar("select max(id) from combodiscountitem"));
                            AddLisItem(item);
                        }
                    }
                    
                }
                catch (Exception ex)
                {                                        
                }
                finally
                {
                    mConnection.Close();
                }
            }
        }
        private bool CheckListItem(Class.ItemComboDisc itemCheck)
        {
            foreach (ListViewItem li in lvListItem.Items)
            {
                try
                {
                    Class.ItemComboDisc item = (Class.ItemComboDisc)li.Tag;
                    if (item.ItemID == itemCheck.ItemID)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                }
            }
            return false;
        }

        private void btnDeleteQty_Click(object sender, EventArgs e)
        {
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                string sqlDeleteRule = "delete from combodiscountrule where id = " + comboDiscRuleID;
                mConnection.ExecuteNonQuery(sqlDeleteRule);
                string sqlDeleteItem = "delete from combodiscountitem where comboDiscountRuleId = " + comboDiscRuleID;
                mConnection.ExecuteNonQuery(sqlDeleteItem);
                mConnection.Commit();
                _frmComboDisc.LoadQuantity(comboDiscID);
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }
        private void LoadListItem()
        {
            try
            {
                string sqlItem = "select c.*,(select i.itemDesc from itemsmenu i where i.itemID=c.itemId) as Name," +
                                     "(select i.unitPrice from itemsmenu i where i.itemID=c.itemID) as Price " +
                                     "from combodiscountitem c where c.comboDiscountRuleId = " + comboDiscRuleID;
                DataTable tblListItem = mConnection.Select(sqlItem);
                lvListItem.Items.Clear();
                foreach (DataRow rowItem in tblListItem.Rows)
                {
                    Class.ItemComboDisc item = new Class.ItemComboDisc(Convert.ToInt32(rowItem["id"]), Convert.ToInt32(rowItem["itemId"]), rowItem["Name"].ToString(), Convert.ToDouble(rowItem["Price"]));
                    AddLisItem(item);
                }
            }
            catch (Exception)
            {
            }
            
        }
        private void AddLisItem(Class.ItemComboDisc item)
        {
            ListViewItem li = new ListViewItem();
            li.SubItems.Add(item.ItemName);
            li.SubItems.Add("$" + mMoneyFortmat.Format2(item.Price));
            li.Tag = item;
            //li.Selected = true;
            lvListItem.Items.Add(li);            
        }

        private void lvListItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(ListViewItem li in lvListItem.Items)
            {
                if (li.Selected)
                {
                    li.Checked = !li.Checked;
                }
            }
        }
    }
}
