﻿namespace POS
{
    partial class UCNewItemLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbshort = new System.Windows.Forms.Label();
            this.lbunitprice = new System.Windows.Forms.Label();
            this.lbsellsize = new System.Windows.Forms.Label();
            this.lbdisplay = new System.Windows.Forms.Label();
            this.lbselltype = new System.Windows.Forms.Label();
            this.lbvisual = new System.Windows.Forms.Label();
            this.lbprinter = new System.Windows.Forms.Label();
            this.lbdesc = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Print All",
            "Kitchen",
            "Bar"});
            this.comboBox1.Location = new System.Drawing.Point(155, 343);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(217, 28);
            this.comboBox1.TabIndex = 113;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(155, 308);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 112;
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(155, 204);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(217, 26);
            this.textBox5.TabIndex = 109;
            this.textBox5.Click += new System.EventHandler(this.textBox5_Click);
            this.textBox5.Leave += new System.EventHandler(this.textBox5_Leave);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox4.Enabled = false;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(155, 251);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(217, 26);
            this.textBox4.TabIndex = 108;
            this.textBox4.Click += new System.EventHandler(this.textBox4_Click);
            this.textBox4.Leave += new System.EventHandler(this.textBox4_Leave);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(155, 107);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(217, 26);
            this.textBox3.TabIndex = 107;
            this.textBox3.Click += new System.EventHandler(this.textBox3_Click);
            this.textBox3.Leave += new System.EventHandler(this.textBox3_Leave);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(155, 60);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(217, 26);
            this.textBox2.TabIndex = 106;
            this.textBox2.Click += new System.EventHandler(this.textBox2_Click);
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(155, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(217, 26);
            this.textBox1.TabIndex = 105;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // lbshort
            // 
            this.lbshort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbshort.AutoSize = true;
            this.lbshort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshort.Location = new System.Drawing.Point(22, 20);
            this.lbshort.Margin = new System.Windows.Forms.Padding(0);
            this.lbshort.Name = "lbshort";
            this.lbshort.Size = new System.Drawing.Size(91, 20);
            this.lbshort.TabIndex = 97;
            this.lbshort.Text = "Item Desc";
            this.lbshort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbunitprice
            // 
            this.lbunitprice.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbunitprice.AutoSize = true;
            this.lbunitprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbunitprice.Location = new System.Drawing.Point(22, 114);
            this.lbunitprice.Margin = new System.Windows.Forms.Padding(0);
            this.lbunitprice.Name = "lbunitprice";
            this.lbunitprice.Size = new System.Drawing.Size(87, 20);
            this.lbunitprice.TabIndex = 98;
            this.lbunitprice.Text = "Unit Price";
            this.lbunitprice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbsellsize
            // 
            this.lbsellsize.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbsellsize.AutoSize = true;
            this.lbsellsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbsellsize.Location = new System.Drawing.Point(22, 251);
            this.lbsellsize.Margin = new System.Windows.Forms.Padding(0);
            this.lbsellsize.Name = "lbsellsize";
            this.lbsellsize.Size = new System.Drawing.Size(79, 20);
            this.lbsellsize.TabIndex = 99;
            this.lbsellsize.Text = "Sell Size";
            this.lbsellsize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdisplay
            // 
            this.lbdisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbdisplay.AutoSize = true;
            this.lbdisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdisplay.Location = new System.Drawing.Point(22, 208);
            this.lbdisplay.Margin = new System.Windows.Forms.Padding(0);
            this.lbdisplay.Name = "lbdisplay";
            this.lbdisplay.Size = new System.Drawing.Size(117, 20);
            this.lbdisplay.TabIndex = 100;
            this.lbdisplay.Text = "Display Order";
            this.lbdisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbselltype
            // 
            this.lbselltype.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbselltype.AutoSize = true;
            this.lbselltype.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbselltype.Location = new System.Drawing.Point(22, 157);
            this.lbselltype.Margin = new System.Windows.Forms.Padding(0);
            this.lbselltype.Name = "lbselltype";
            this.lbselltype.Size = new System.Drawing.Size(82, 20);
            this.lbselltype.TabIndex = 101;
            this.lbselltype.Text = "Sell Type";
            this.lbselltype.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbvisual
            // 
            this.lbvisual.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbvisual.AutoSize = true;
            this.lbvisual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbvisual.Location = new System.Drawing.Point(22, 302);
            this.lbvisual.Margin = new System.Windows.Forms.Padding(0);
            this.lbvisual.Name = "lbvisual";
            this.lbvisual.Size = new System.Drawing.Size(58, 20);
            this.lbvisual.TabIndex = 102;
            this.lbvisual.Text = "Visual";
            this.lbvisual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbprinter
            // 
            this.lbprinter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbprinter.AutoSize = true;
            this.lbprinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbprinter.Location = new System.Drawing.Point(22, 347);
            this.lbprinter.Margin = new System.Windows.Forms.Padding(0);
            this.lbprinter.Name = "lbprinter";
            this.lbprinter.Size = new System.Drawing.Size(69, 20);
            this.lbprinter.TabIndex = 104;
            this.lbprinter.Text = "Print At";
            this.lbprinter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdesc
            // 
            this.lbdesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbdesc.AutoSize = true;
            this.lbdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdesc.Location = new System.Drawing.Point(22, 67);
            this.lbdesc.Margin = new System.Windows.Forms.Padding(0);
            this.lbdesc.Name = "lbdesc";
            this.lbdesc.Size = new System.Drawing.Size(94, 20);
            this.lbdesc.TabIndex = 96;
            this.lbdesc.Text = "Item Short";
            this.lbdesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Each",
            "Mls",
            "Kg"});
            this.comboBox2.Location = new System.Drawing.Point(155, 153);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(217, 28);
            this.comboBox2.TabIndex = 117;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // UCNewItemLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbshort);
            this.Controls.Add(this.lbunitprice);
            this.Controls.Add(this.lbsellsize);
            this.Controls.Add(this.lbdisplay);
            this.Controls.Add(this.lbselltype);
            this.Controls.Add(this.lbvisual);
            this.Controls.Add(this.lbprinter);
            this.Controls.Add(this.lbdesc);
            this.Name = "UCNewItemLine";
            this.Size = new System.Drawing.Size(386, 516);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox comboBox1;
        public System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.TextBox textBox5;
        public System.Windows.Forms.TextBox textBox4;
        public System.Windows.Forms.TextBox textBox3;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbshort;
        private System.Windows.Forms.Label lbunitprice;
        private System.Windows.Forms.Label lbsellsize;
        private System.Windows.Forms.Label lbdisplay;
        private System.Windows.Forms.Label lbselltype;
        private System.Windows.Forms.Label lbvisual;
        private System.Windows.Forms.Label lbprinter;
        private System.Windows.Forms.Label lbdesc;
        public System.Windows.Forms.ComboBox comboBox2;
    }
}
