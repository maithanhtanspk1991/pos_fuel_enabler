﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Forms;

namespace POS
{
    public partial class UCReportDaily : UserControl
    {
        private Connection.ReadDBConfig config = new Connection.ReadDBConfig();
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private double dblBalanceCount = 0;
        private double dblBankEftposCashOut = 0;
        private double dblCash = 0;
        private double dblCashDrop = 0;
        private double dblCashFloat = 0;
        private double dblCashIn = 0;
        private double dblCashOut = 0;
        private double dblClosingBar = 0;
        private double dblFloatIn = 0;
        private double dblFloatOut = 0;
        private double dblTotalAccount = 0;
        private double dblTotalCard = 0;
        private double dblTotalCash = 0;
        private double dblTotalCashAccount = 0;
        private double dblTotalDiscount = 0;
        private double dblTotalFuel;
        private double dblTotalNonFuel;
        private double dblTotalReturn = 0;
        private double dblTotalSale = 0;
        private double dblTotalSales = 0;
        private DataTable dtSource;
        private int i = 0;
        private int intAccountID;
        private int intGroupID;
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private POS.Printer mPrinterAll;
        private Class.Setting mSetting;
        private Class.ReadConfig rconfig;
        private string sql;
        private string strMemberNo = "";
        private string strTotal = "";
        private string strTotalCreditAccount = "";
        private string strTotalEmployeeQuanity = "";
        private double totalSaleFuel, totalSaleNonFuel;
        private string weekfrom;
        private string weekto;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public UCReportDaily(Class.MoneyFortmat m)
        {
            InitializeComponent();
            SetMultiLanguage();
            mConnection = new Connection.Connection();
            mPrinter = new Printer();
            mPrinterAll = new Printer();
            mSetting = new Class.Setting();
            money = m;
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDailySalesReport);
            mPrinterAll.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportAll);
            rconfig = new Class.ReadConfig();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label11.Text = "Ngày xem:";
                button25.Text = "IN TẤT CẢ BÁO CÁO";
                button1.Text = "In";
                button11.Text = "Xem";
                tbpDailyReport.Text = "Bán hàng ngày";
                groupBox1.Text = "Bán hàng";
                tbpQuantity.Text = "Báo cáo số lượng";
                tbpGroupQuantity.Text = "Báo cáo nhóm số lượng";
                tbpDelivery.Text = "Danh sách giao hàng";
                tbpListPickup.Text = "Danh sách nhận hàng";
                tbpSalesPerStaff.Text = "Hiệu năng bán hàng của nhân viên";
                tbpCashInOrCashOut.Text = "Tiền vào / Tiền ra";
                label6.Text = "Bán hàng:";
                label17.Text = "Tổng giảm giá:";
                label25.Text = "Tổng thuế:";
                label27.Text = "Thẻ:";
                label29.Text = "Tiền mặt:";
                label9.Text = "Phụ thu:";
                label31.Text = "Hoàn trả:";
                label7.Text = "Tiền vào:";
                label24.Text = "Phí giao hàng:";
                label12.Text = "Thanh toán hết nợ:";
                label13.Text = "Tài khoản:";
                groupBox3.Text = "Thanh toán tài khoản";
                label10.Text = "Thẻ";
                label16.Text = "Tiền mặt";
                label23.Text = "Tổng cộng";
                groupBox2.Text = "Thẻ";
                columnHeader7.Text = "Tên thẻ";
                columnHeader8.Text = "Thanh toán";
                columnHeader46.Text = "Phụ thu";
                label28.Text = "Tổng cộng";
                columnHeader33.Text = "Tên thẻ";
                columnHeader34.Text = "Thanh toán";
                label15.Text = "Tổng tiền mặt";
                label4.Text = "Tổng thẻ";
                label22.Text = "Tổng cộng";
                label33.Text = "Ngày";
                columnHeader1.Text = "Tên sản phẩm - BÁN HÀNG NHIÊN LIỆU";
                columnHeader2.Text = "Số lượng";
                columnHeader5.Text = "Tổng cộng";
                columnHeader32.Text = "Tên sản phẩm - BÁN HÀNG CỬA HÀNG";
                columnHeader41.Text = "Số lượng";
                columnHeader42.Text = "Tổng cộng";
                button9.Text = "Xem";
                columnHeader3.Text = "Tên sản phẩm - BÁN HÀNG NHIÊN LIỆU";
                columnHeader4.Text = "Số lượng";
                columnHeader6.Text = "Tổng cộng";
                columnHeader43.Text = "Tên sản phẩm - BÁN HÀNG CỬA HÀNG";
                columnHeader44.Text = "Số lượng";
                columnHeader45.Text = "Tổng cộng";
                label2.Text = "Ngày";
                columnHeader10.Text = "Tên nhân viên";
                columnHeader13.Text = "Thanh toán";
                columnHeader15.Text = "Tổng cộng";
                columnHeader16.Text = "Mô tả";
                button16.Text = "Xem hóa đơn";
                button19.Text = "Xem thanh toán";
                columnHeader11.Text = "Số thành viên";
                columnHeader12.Text = "Tên tài khoản";
                columnHeader14.Text = "Giới hạn tài khoản";
                columnHeader18.Text = "Thanh toán";
                columnHeader19.Text = "Số thành viên";
                columnHeader20.Text = "Tên tài khoản";
                columnHeader21.Text = "Thanh toán";
                button22.Text = "Xem chi tiết";
                button24.Text = "Xem chi tiết";
                columnHeader28.Text = "Mã order";
                columnHeader29.Text = "Thành tiền";
                columnHeader30.Text = "Ca làm việc";
                columnHeader31.Text = "Nhân viên";
                tbpDebitAccount.Text = "Tài khoản ghi nợ";
                tbpCreditAccount.Text = "Tài khoản tín dụng";
                columnHeader35.Text = "Mã khách hàng";
                columnHeader36.Text = "Loại tài khoản";
                //columnHeader37.Text = "";
                columnHeader38.Text = "Loại thẻ";
                //columnHeader39.Text = "";
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Print Report");
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Active || License.BOLicense.CheckLicense() == License.LicenseDialog.Trial)
            {
                try
                {
                    mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                    mPrinter.printDocument.Print();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportDaily.button1_Click::" + ex.Message);
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    string message1 = "Giấy phép của bạn đã hết hạn. Liên hệ Becas để được cấp mới ngay hôm nay cho bạn!!!";
                    frmMessageBoxOK frm1 = new frmMessageBoxOK("CẢNH BÁO", message1);
                    frm1.ShowDialog();
                    return;
                }
                string message = "Your license is expired. Contact Becas to renew your today!!!";
                frmMessageBoxOK frm = new frmMessageBoxOK("WARNING", message);
                frm.ShowDialog();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstGroupItemFuelSales.SelectedIndices[0];
                    lstGroupItemFuelSales.SelectedIndices.Clear();
                    if (oldselection + 2 > lstGroupItemFuelSales.Items.Count)
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(0);
                        lstGroupItemFuelSales.Items[0].Selected = true;
                        lstGroupItemFuelSales.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(oldselection + 1);
                        lstGroupItemFuelSales.Items[oldselection + 1].Selected = true;
                        lstGroupItemFuelSales.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            MessageBox.Show(sql);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (lstDebitAccount.SelectedIndices.Count > 0)
            {
                frmViewDetailDeptAccount frmView = new frmViewDetailDeptAccount(mConnection, money, intAccountID, new Class.ReadConfig());
                frmView.Show();
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (listViewdriveroff.SelectedIndices.Count > 0)
            {
                listViewdriveroff.SelectedIndices.Clear();
                listViewdriveroff.SelectedIndices.Add(0);
                listViewdriveroff.Items[0].Selected = true;
                listViewdriveroff.Items[0].EnsureVisible();
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            String strSelectionTabpage = tcSalesPerStaff.SelectedTab.Name.ToString();
            LoadReportControl(strSelectionTabpage);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (lstDebitAccount.SelectedIndices.Count > 0)
            {
                frmViewDetailPay frmView = new frmViewDetailPay(mConnection, money, intAccountID);
                frmView.ViewDetailAccountPay(weekfrom, weekto);
                frmView.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                lstQuantityReportFuelSales.SelectedIndices.Clear();
                lstQuantityReportFuelSales.SelectedIndices.Add(0);
                lstQuantityReportFuelSales.Items[0].Selected = true;
                lstQuantityReportFuelSales.Items[0].EnsureVisible();
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (listViewdriveroff.SelectedIndices.Count > 0)
            {
                listViewdriveroff.SelectedIndices.Clear();
                listViewdriveroff.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                listViewdriveroff.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                listViewdriveroff.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (listViewdriveroff.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listViewdriveroff.SelectedIndices[0];
                    listViewdriveroff.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        listViewdriveroff.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                        listViewdriveroff.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                        listViewdriveroff.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        listViewdriveroff.SelectedIndices.Add(oldselection - 1);
                        listViewdriveroff.Items[oldselection - 1].Selected = true;
                        listViewdriveroff.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (lstCreditAccount.SelectedIndices.Count > 0)
            {
                frmViewDetailCreditAccount frmView = new frmViewDetailCreditAccount(mConnection, money, strMemberNo);
                frmView.ViewDetailAccountCredit(weekfrom, weekto);
                frmView.Show();
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            if (listViewdriveroff.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listViewdriveroff.SelectedIndices[0];
                    listViewdriveroff.SelectedIndices.Clear();
                    if (oldselection + 2 > listViewdriveroff.Items.Count)
                    {
                        listViewdriveroff.SelectedIndices.Add(0);
                        listViewdriveroff.Items[0].Selected = true;
                        listViewdriveroff.Items[0].EnsureVisible();
                    }
                    else
                    {
                        listViewdriveroff.SelectedIndices.Add(oldselection + 1);
                        listViewdriveroff.Items[oldselection + 1].Selected = true;
                        listViewdriveroff.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            if (listViewdriveroff.SelectedIndices.Count > 0)
            {
                DriverOff droff = (DriverOff)listViewdriveroff.SelectedItems[0].Tag;
                frmDriverOffDetail frm = new frmDriverOffDetail(droff.OrderID, droff.Ts, money, droff.ShiftID);
                frm.ShowDialog();
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Print All Report");
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Active || License.BOLicense.CheckLicense() == License.LicenseDialog.Trial)
            {
                try
                {
                    LoadDataPrintAll();
                    mPrinterAll.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                    mPrinterAll.printDocument.Print();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportDaily.button1_Click::" + ex.Message);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReportFuelSales.SelectedIndices[0];
                    lstQuantityReportFuelSales.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(lstQuantityReportFuelSales.Items.Count - 1);
                        lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].Selected = true;
                        lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(oldselection - 1);
                        lstQuantityReportFuelSales.Items[oldselection - 1].Selected = true;
                        lstQuantityReportFuelSales.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                lstQuantityReportFuelSales.SelectedIndices.Clear();
                lstQuantityReportFuelSales.SelectedIndices.Add(lstQuantityReportFuelSales.Items.Count - 1);
                lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].Selected = true;
                lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].EnsureVisible();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReportFuelSales.SelectedIndices[0];
                    lstQuantityReportFuelSales.SelectedIndices.Clear();
                    if (oldselection + 2 > lstQuantityReportFuelSales.Items.Count)
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(0);
                        lstQuantityReportFuelSales.Items[0].Selected = true;
                        lstQuantityReportFuelSales.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(oldselection + 1);
                        lstQuantityReportFuelSales.Items[oldselection + 1].Selected = true;
                        lstQuantityReportFuelSales.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                lstGroupItemFuelSales.SelectedIndices.Clear();
                lstGroupItemFuelSales.SelectedIndices.Add(0);
                lstGroupItemFuelSales.Items[0].Selected = true;
                lstGroupItemFuelSales.Items[0].EnsureVisible();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstGroupItemFuelSales.SelectedIndices[0];
                    lstGroupItemFuelSales.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                        lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                        lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(oldselection - 1);
                        lstGroupItemFuelSales.Items[oldselection - 1].Selected = true;
                        lstGroupItemFuelSales.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                lstGroupItemFuelSales.SelectedIndices.Clear();
                lstGroupItemFuelSales.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                frmViewDetailSaleDaily frmView = new frmViewDetailSaleDaily();
                frmView.ViewSaleByGroup(mConnection, money, intGroupID, weekfrom, weekto);
                frmView.Show();
            }
        }

        private void dateTimePickerfrom_ValueChanged(object sender, EventArgs e)
        {
        }

        private string GetDateString(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        private void listViewdriveroff_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void LoadDataPrintAll()
        {
            LoadTabDaily();
            LoadReportQuantity();
            LoadReportGroupQuanity();
            LoadReportEmployeeQuanity();
            //LoadReportCashInCashOut();
            LoadListAccount();
            LoadListCreditAccount();
            //LoadReportDriverOff();
            //LoadReportCardReject();
            //LoadListDeliveryAndPickup(1);
            //LoadListDeliveryAndPickup(2);
            //LoadListStockTake();
        }

        private void LoadListAccount()
        {
            try
            {
                mConnection.Open();
                lstDebitAccount.Items.Clear();
                string sql = "SELECT custID,FirstName,memberNo,accountlimit,debt FROM customers WHERE memberNo <> '' AND memberNo is not null AND debt > 0  Order by FirstName";

                System.Data.DataTable tblAccount = mConnection.Select(sql);

                //DateTime dt = DateTime.Now;
                //labelDateTime.Text = "Date: " + dt.Day + "-" + dt.Month + "-" + dt.Year + " " + dt.ToLongTimeString();
                double decTotal = 0, debt = 0;
                foreach (System.Data.DataRow row in tblAccount.Rows)
                {
                    ListViewItem li = new ListViewItem(row["memberNo"].ToString());
                    li.SubItems.Add(row["FirstName"].ToString());
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["accountlimit"]))));
                    debt = Convert.ToDouble(row["debt"]) * -1;
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format(debt)));
                    li.Tag = Convert.ToInt32(row["custID"]);
                    decTotal += debt;
                    lstDebitAccount.Items.Add(li);
                }
                strTotal = money.Format(decTotal);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblTotal3.Text = "Tổng cộng : " + money.Format(decTotal);
                    return;
                }
                else
                {
                    lblTotal3.Text = "Total : " + money.Format(decTotal);
                }       
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily:::LoadListAccount:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadListCreditAccount()
        {
            try
            {
                mConnection.Open();
                lstCreditAccount.Items.Clear();
                string sql = "SELECT c.memberNo,c.Name,SUM( a.cash + a.card ) AS SubTotal FROM accountpayment a inner join customers c on a.CustomerNo = c.memberNo where a.isCredit = 1 group by c.memberNo,c.Name";

                System.Data.DataTable tblAccount = mConnection.Select(sql);

                double decTotal = 0;
                foreach (System.Data.DataRow row in tblAccount.Rows)
                {
                    ListViewItem li = new ListViewItem(row["memberNo"].ToString());
                    li.SubItems.Add(row["Name"].ToString());
                    if (Convert.ToDouble(row["SubTotal"]) < 0)
                    {
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["SubTotal"]) * (-1))));
                        decTotal += Convert.ToDouble(row["SubTotal"]) * (-1);
                    }
                    else
                    {
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["SubTotal"]))));
                        decTotal += Convert.ToDouble(row["SubTotal"]);
                    }

                    li.Tag = row["memberNo"].ToString();
                    lstCreditAccount.Items.Add(li);
                }
                strTotalCreditAccount = money.Format(decTotal);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    label14.Text = "Tổng cộng : " + money.Format(decTotal);
                }
                else
                {
                    label14.Text = "Total : " + money.Format(decTotal);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily:::LoadListCreditAccount:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadListDeliveryAndPickup(int intType)
        {
            try
            {
                mConnection.Open();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                string strCondition = "AND date(ts) =" + "date('" + weekfrom + "')";
                if (intType == 1)
                {
                    ucListDelivery.lstList.Columns[0].Width = 180;
                    ucListDelivery.lstList.Columns[1].Width = 150;
                    ucListDelivery.lstList.Columns[2].Width = 170;
                    ucListDelivery.lstList.Columns[3].Width = 100;
                    ucListDelivery.lstList.Columns[4].Width = 100;
                    ucListDelivery.LoadReport(mConnection, money, 0, 1, strCondition);
                }
                else
                {
                    ucListPickup.lstList.Columns[0].Width = 180;
                    ucListPickup.lstList.Columns[1].Width = 150;
                    ucListPickup.lstList.Columns[2].Width = 170;
                    ucListPickup.lstList.Columns[3].Width = 100;
                    ucListPickup.lstList.Columns[4].Width = 100;
                    ucListPickup.LoadReport(mConnection, money, 1, 0, strCondition);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadListStockTake()
        {
            try
            {
                mConnection.Open();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + "00:00:00";
                weekto = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + "23:59:59";
                ucListStockTake2.lstList.Columns[0].Width = 170;
                ucListStockTake2.lstList.Columns[1].Width = 180;
                ucListStockTake2.lstList.Columns[2].Width = 100;
                ucListStockTake2.lstList.Columns[3].Width = 100;
                ucListStockTake2.lstList.Columns[3].Width = 80;
                ucListStockTake2.lstList.Columns[4].Width = 100;
                ucListStockTake2.LoadReport(mConnection, money, weekfrom, weekto);
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReport()
        {
            Class.LogPOS.WriteLog("UCReportDaily_Load");
            try
            {
                string dateString = GetDateString(dateTimePickerfrom.Value);
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = weekfrom;
                mConnection.Open();
                string sql = "select " +
                                "sum(subTotal) as totalSales," +
                                "sum(discount) as totalDisCount," +
                                "sum(cash) as cash," +
                                "sum(eftpos) as eftpos," +
                                "sum(returnPro) as returnPro," +
                                "sum(creditNote) as creditNote, " +
                                "sum(gst) as gst, " +
                                "sum(account) as account, " +
                                "sum(`change`) as `change` " +
                             "from (" +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersdaily " +
                                        "where (day(ts)='" + dateTimePickerfrom.Value.Day + "' and month(ts)='" + dateTimePickerfrom.Value.Month + "' and year(ts)='" + dateTimePickerfrom.Value.Year + "' ) and (completed=1 or completed=2) " +
                                    "union all " +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersall " +
                                        "where (day(ts)='" + dateTimePickerfrom.Value.Day + "' and month(ts)='" + dateTimePickerfrom.Value.Month + "' and year(ts)='" + dateTimePickerfrom.Value.Year + "' ) and (completed=1 or completed=2) " +
                                     ") as total";

                System.Data.DataTable tbl = mConnection.Select(sql);
                DateTime dt = DateTime.Now;

                double sales = Convert.ToDouble(tbl.Rows[0]["totalSales"].ToString() == "" ? 0 : tbl.Rows[0]["totalSales"]);
                double discount = Convert.ToDouble(tbl.Rows[0]["totalDisCount"].ToString() == "" ? 0 : tbl.Rows[0]["totalDisCount"]);
                double refunc = Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]);
                double creditNote = Convert.ToDouble(tbl.Rows[0]["creditNote"].ToString() == "" ? 0 : tbl.Rows[0]["creditNote"]);
                double gst = Convert.ToDouble(tbl.Rows[0]["gst"].ToString() == "" ? 0 : tbl.Rows[0]["gst"]);
                double change = Convert.ToDouble(tbl.Rows[0]["change"].ToString() == "" ? 0 : tbl.Rows[0]["change"]);
                double account = Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]);

                double dis = Convert.ToDouble(mConnection.ExecuteScalar(
                    "select if(sum(subTotal) is null,0,sum(subTotal)) as subTotal from " +
                    "(" +
                        "select sum(ol.subTotal) as subTotal from ordersdailyline ol inner join ordersdaily o on o.orderID=ol.orderID where (day(o.ts)='" + dateTimePickerfrom.Value.Day + "' and month(o.ts)='" + dateTimePickerfrom.Value.Month + "' and year(o.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                        "union all " +
                        "select sum(ol.subTotal) as subTotal from ordersallline ol inner join ordersall o on o.bkId=ol.bkId where (day(o.ts)='" + dateTimePickerfrom.Value.Day + "' and month(o.ts)='" + dateTimePickerfrom.Value.Month + "' and year(o.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                    ") as tmp "
                    ));
                discount -= dis;
                sales -= dis;
                double cash = 0;
                double eftpos = 0;
                double refun = 0;

                try
                {
                    cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    eftpos = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    refun = Convert.ToDouble(tbl.Rows[0]["returnPro"]);
                }
                catch (Exception)
                {
                }

                labelTotalDiscount.Text = money.Format2(discount);
                labelTotalSale.Text = money.Format2(sales);
                labelTotalGST.Text = money.Format2(gst);
                labelCash.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? 0 : tbl.Rows[0]["cash"]));
                labelCard.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["eftpos"].ToString() == "" ? 0 : tbl.Rows[0]["eftpos"]));
                labelRefunc.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]));
                labelCreditNote.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["creditNote"].ToString() == "" ? 0 : tbl.Rows[0]["creditNote"]));
                labelAccount.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]) * -1);
                double decTotal;
                // duc sua 4-4-2013

                #region sua

                sql = "SELECT CardName,SUM(Subtotal) AS Subtotal " +
                  "FROM " +
                  "( " +
                  "SELECT c.cardName AS CardName,SUM(obc.subtotal) AS Subtotal " +
                  "FROM orderbycard obc " +
                  "inner join ordersdaily od on od.orderbycardID=obc.ID " +
                  "inner join card c on obc.cardID = c.cardID " +
                  "where (day(od.ts)='" + dateTimePickerfrom.Value.Day + "' and month(od.ts)='" + dateTimePickerfrom.Value.Month + "' and year(od.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (od.completed=1 or od.completed=2) group by CardName " +
                  "Union all " +
                  "SELECT c.cardName,SUM(obc.subtotal) AS Subtotal " +
                  "FROM orderbycard obc " +
                  "inner join ordersall oa on oa.orderbycardID=obc.ID " +
                  "inner join card c on obc.cardID = c.cardID " +
                  "where (day(oa.ts)='" + dateTimePickerfrom.Value.Day + "' and month(oa.ts)='" + dateTimePickerfrom.Value.Month + "' and year(oa.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (oa.completed=1 or oa.completed=2) group by CardName " +
                  ")AS Source group by CardName";

                #endregion sua

                // duc comment 4-4-2013

                #region comment

                //sql = "SELECT CardName,SUM(Subtotal) AS Subtotal " +
                //    "FROM " +
                //    "( " +
                //    "SELECT c.cardName AS CardName,SUM(obc.subtotal) AS Subtotal " +
                //    "FROM orderbycard obc " +
                //    "inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
                //    "inner join card c on obc.cardID = c.cardID " +
                //    "where (day(od.ts)='" + dateTimePickerfrom.Value.Day + "' and month(od.ts)='" + dateTimePickerfrom.Value.Month + "' and year(od.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (od.completed=1 or od.completed=2) group by CardName " +
                //    "Union all " +
                //    "SELECT c.cardName,SUM(obc.subtotal) AS Subtotal " +
                //    "FROM orderbycard obc " +
                //    "inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
                //    "inner join card c on obc.cardID = c.cardID " +
                //    "where (day(oa.ts)='" + dateTimePickerfrom.Value.Day + "' and month(oa.ts)='" + dateTimePickerfrom.Value.Month + "' and year(oa.ts)='" + dateTimePickerfrom.Value.Year + "' ) and (oa.completed=1 or oa.completed=2) group by CardName " +
                //    ")AS Source group by CardName";

                #endregion comment

                lstCard.Items.Clear();

                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);
                decTotal = 0;
                //foreach (System.Data.DataRow row in dtSource.Rows)
                //{
                //    ListViewItem li = new ListViewItem(row["CardName"].ToString());
                //    li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
                //    decTotal += Convert.ToDouble(row["Subtotal"]);
                //    lstCard.Items.Add(li);
                //}
                //lblTotalCard.Text = "Total card amount :" + money.Format2(decTotal);

                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(row["CardName"].ToString());
                    li.SubItems.Add(money.Format2(Convert.ToDouble(row["Subtotal"])));
                    decTotal += Convert.ToDouble(row["Subtotal"]);
                    lstCard.Items.Add(li);
                }
                double dblBalance = 0;
                if ((Convert.ToDouble(labelCard.Text) - Convert.ToDouble(money.Format2(decTotal))) != 0)
                {
                    ListViewItem li = new ListViewItem("Unknow card");
                    dblBalance = (Convert.ToDouble(labelCard.Text) - Convert.ToDouble(money.Format2(decTotal)));
                    li.SubItems.Add(money.getFortMat2(dblBalance));
                    lstCard.Items.Add(li);
                }
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblTotalCard.Text = "Tổng số tiền thẻ : " + money.Format2(money.getFortMat(dblBalance + decTotal / 1000));
                    return;
                }
                else
                {
                    lblTotalCard.Text = "Total card amount : " + money.Format2(money.getFortMat(dblBalance + decTotal / 1000));
                }

                double cashAccount = 0;
                double cardAccount = 0;
                dtSource = mConnection.Select(
                    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where (day(PayDate)='" + dateTimePickerfrom.Value.Day + "' and month(PayDate)='" + dateTimePickerfrom.Value.Month + "' and year(PayDate)='" + dateTimePickerfrom.Value.Year + "' ) AND PayAmount<>0 ) as tmp "
                    );
                if (dtSource.Rows.Count > 0)
                {
                    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
                }
                labelCardAccount.Text = money.Format2(cardAccount);
                labelCashAccount.Text = money.Format2(cashAccount);
                labelTotalAccount.Text = money.Format2(cardAccount + cashAccount);

                sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                        "FROM " +
                        "( " +
                        "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND (day(dateCash)='" + dateTimePickerfrom.Value.Day + "' and month(dateCash)='" + dateTimePickerfrom.Value.Month + "' and year(dateCash)='" + dateTimePickerfrom.Value.Year + "' ) " +
                        "UNION ALL " +
                        "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where CashType = 2 AND (day(dateCash)='" + dateTimePickerfrom.Value.Day + "' and month(dateCash)='" + dateTimePickerfrom.Value.Month + "' and year(dateCash)='" + dateTimePickerfrom.Value.Year + "' ) " +
                        ")AS Source";
                tbl = new DataTable();
                tbl = mConnection.Select(sql);

                labelCashIn.Text = money.Format2(0).ToString();
                labelCashOut.Text = money.Format2(0).ToString();

                if (tbl.Rows.Count != 0)
                {
                    foreach (System.Data.DataRow row in tbl.Rows)
                    {
                        labelCashIn.Text = money.Format2(Convert.ToDouble(row["CashIn"].ToString() == "" ? "0" : row["CashIn"].ToString())).ToString();
                        labelCashOut.Text = money.Format2(Convert.ToDouble(row["CashOut"].ToString() == "" ? "0" : row["CashOut"].ToString())).ToString();
                    }
                }

                double sumCash = cash + cashAccount - refun;
                double sumCard = eftpos + cardAccount;
                lblCashTotal.Text = money.Format(sumCash);
                lblCardTotal.Text = money.Format(sumCard);
                lblSumTotal.Text = money.Format(sumCard + sumCash);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReportCardReject()
        {
            mConnection.Open();
            listViewCardReject.Items.Clear();
            string sql = "SELECT cr.AuthCode,cr.AccountType,cr.Pan,cr.CardType,cr.AmtPurchase,cr.AmtCash FROM cardreject cr left join card c on cr.CardType=c.cardCode where (day(cr.ts)='" + dateTimePickerfrom.Value.Day + "' and month(cr.ts)='" + dateTimePickerfrom.Value.Month + "' and year(cr.ts)='" + dateTimePickerfrom.Value.Year + "')";
            System.Data.DataTable tblAccount = mConnection.Select(sql);
            foreach (System.Data.DataRow row in tblAccount.Rows)
            {
                ListViewItem li = new ListViewItem(row["AuthCode"].ToString());
                li.SubItems.Add(row["AccountType"].ToString());
                li.SubItems.Add(row["Pan"].ToString());
                li.SubItems.Add(row["CardType"].ToString());
                li.SubItems.Add(row["AmtPurchase"].ToString());
                li.SubItems.Add(row["AmtCash"].ToString());
                li.Tag = row["AuthCode"].ToString();
                listViewCardReject.Items.Add(li);
            }
        }

        private void LoadReportCashInCashOut()
        {
            string dateString = GetDateString(dateTimePickerfrom.Value);
            string sql;
            if (POS.Properties.Settings.Default.AllowUseShift)
            {
                sql = "SELECT shiftID FROM shifts s where (day(ts)='" + dateTimePickerfrom.Value.Day + "' and month(ts)='" + dateTimePickerfrom.Value.Month + "' and year(ts)='" + dateTimePickerfrom.Value.Year + "' )";
            }
            else
            {
                sql = "SELECT max(shiftID) as shiftID FROM shifts";
            }
            lstHistoryShift.Items.Clear();
            DataTable tbl = new DataTable();
            tbl = mConnection.Select(sql);
            foreach (DataRow dr in tbl.Rows)
            {
                try
                {
                    LoadTabDaily(dr["shiftID"].ToString());
                }
                catch (Exception)
                {
                    LoadTabDaily("1");
                }
            }
        }

        private void LoadReportControl(String strSelectionTabpage)
        {
            if (strSelectionTabpage.Equals("tbpDailyReport"))
            {
                LoadTabDaily();
                LoadReportQuantity();
                LoadReportGroupQuanity();
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDailySalesReport);
            }
            else if (strSelectionTabpage.Equals("tbpQuantity"))
            {
                LoadReportQuantity();
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportQuantity);
            }
            else if (strSelectionTabpage.Equals("tbpGroupQuantity"))
            {
                LoadReportGroupQuanity();
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportGroupQuantity);
            }
            else if (strSelectionTabpage.Equals("tbpSalesPerStaff"))
            {
                LoadReportEmployeeQuanity();
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportEmployeeQuantity);
            }
            else if (strSelectionTabpage.Equals("tbpCashInOrCashOut"))
            {
                LoadReportCashInCashOut();
                mPrinter = new Printer();
                button1.Visible = false;
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportEmployeeQuantity);
            }
            else if (strSelectionTabpage.Equals("tbpDebitAccount"))
            {
                LoadListAccount();
                button1.Visible = true;
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportAccount);
            }
            else if (strSelectionTabpage.Equals("tbpCreditAccount"))
            {
                LoadListCreditAccount();
                button1.Visible = true;
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportCreditAccount);
            }
            else if (strSelectionTabpage.Equals("tbpDriverOff"))
            {
                LoadReportDriverOff();
                button1.Visible = false;
                mPrinter = new Printer();
            }
            else if (strSelectionTabpage.Equals("tbpCardReject"))
            {
                LoadReportCardReject();
                button1.Visible = false;
            }
            else if (strSelectionTabpage.Equals("tbpDelivery"))
            {
                LoadListDeliveryAndPickup(1);
                button1.Visible = true;
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportDelivery);
            }
            else if (strSelectionTabpage.Equals("tbpListPickup"))
            {
                LoadListDeliveryAndPickup(2);
                button1.Visible = true;
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportListPickup);
            }
            else if (strSelectionTabpage.Equals("tbpStockTake"))
            {
                LoadListStockTake();
                button1.Visible = false;

                //mPrinter = new Printer();
                //mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportListPickup);
            }
        }

        private DataTable LoadReportDriverOff()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select o.orderID," +
                                    "o.subTotal," +
                                    "o.ts," +
                                    "o.shiftID," +
                                    "if(o.staffID<>0,(select Name from staffs where o.staffID=staffID),'Manager') as StaffName " +
                                    "from ordersdaily o where (day(o.ts)=" + dateTimePickerfrom.Value.Day.ToString() + " and month(o.ts)=" + dateTimePickerfrom.Value.Month.ToString() + " and year(o.ts)=" + dateTimePickerfrom.Value.Year.ToString() + " and completed=0)" +
                              " union all " +
                             "select a.orderID," +
                                    "a.subTotal," +
                                    "a.ts," +
                                    "a.shiftID," +
                                    "if(a.staffID<>0,(select Name from staffs where a.staffID=staffID),'Manager') as StaffName " +
                                    "from ordersall a where (day(a.ts)=" + dateTimePickerfrom.Value.Day.ToString() + " and month(a.ts)=" + dateTimePickerfrom.Value.Month.ToString() + " and year(a.ts)=" + dateTimePickerfrom.Value.Year.ToString() + " and completed=0)";
                mConnection.Open();
                dt = mConnection.Select(sql);
                listViewdriveroff.Items.Clear();
                double total = 0;
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        total += Convert.ToDouble(row["subTotal"].ToString());
                        ListViewItem lvi = new ListViewItem(row["orderID"].ToString());
                        DriverOff droff = new DriverOff(row["orderID"].ToString(), Convert.ToDateTime(row["ts"].ToString()), Convert.ToInt32(row["shiftID"]));
                        lvi.SubItems.Add(money.Format(row["subTotal"].ToString()));
                        lvi.SubItems.Add(row["shiftID"].ToString());
                        lvi.SubItems.Add(row["StaffName"].ToString());
                        lvi.Tag = droff;
                        listViewdriveroff.Items.Add(lvi);
                    }
                }
                lbtotaldriveroff.Text = money.Format(total);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                Class.LogPOS.WriteLog("UCReportDaily:::LoadReportDriverOff:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReportEmployeeQuanity()
        {
            weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
            try
            {
                mConnection.Open();
                lstEmployee.Items.Clear();
                //weekfrom = SplitDateFrom(Convert.ToDateTime(dateTimePickerfrom.Value.ToString()));
                //weekto = SplitDateTo(Convert.ToDateTime(dateTimePickerfrom.Value.ToString()));
                //string sql =
                //    "SELECT o.staffID,s.Name,SUM( o.subTotal) AS Subtotal FROM ordersdaily o left join staffs s on o.staffID = s.stfId where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) group by o.staffID,s.Name";
                sql =
                    "select " +
                        "staffID," +
                        "if((select s.Name from staffs s where s.staffID=tmp.staffID) is null,'Manager',(select s.Name from staffs s where s.staffID=tmp.staffID)) as Name," +
                        "sum(subTotal) as subTotal " +
                    "from " +
                    "(" +
                    "select o.staffID,sum(ol.subTotal) as subTotal " +
                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                    "where date(o.ts)=date('" + weekfrom + "') AND (o.completed = 1 OR o.completed = 2 ) " +
                    "group by o.staffID " +
                    "union all " +
                    "select o.staffID,sum(ol.subTotal) as subTotal " +
                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                    "where date(o.ts)=date('" + weekfrom + "') AND (o.completed = 1 OR o.completed = 2 ) " +
                    "group by o.staffID " +
                    ") as tmp " +
                    "group by staffID " +
                    "";
                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);

                double decTotal = 0;
                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    ListViewItem li;
                    if (Convert.ToInt32(row["staffID"]) == 0)
                    {
                        li = new ListViewItem("Manager");
                    }
                    else
                    {
                        li = new ListViewItem(row["Name"].ToString());
                    }
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
                    decTotal += Convert.ToDouble(row["Subtotal"]);
                    li.Tag = Convert.ToInt32(row["staffID"]);

                    lstEmployee.Items.Add(li);
                }
                strTotalEmployeeQuanity = money.Format(decTotal);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    label3.Text = "Tổng cộng : " + money.Format(decTotal);
                    return;
                }
                else
                {
                    label3.Text = "Total : " + money.Format(decTotal);
                }
                
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily:::LoadReportEmployeeQuanity:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReportGroupQuanity()
        {
            try
            {
                if (dbconfig.AllowSalesFuel == 0)
                {
                    lstGroupItemFuelSales.Visible = false;
                    lstGroupItemShopSales.Location = new Point(0, 0);
                    lstGroupItemShopSales.Height = 250;
                }

                mConnection.Open();
                lstGroupItemFuelSales.Items.Clear();
                lstGroupItemShopSales.Items.Clear();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = weekfrom;
                /*
                string sql =
                            "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                            "from " +
                            "( " +
                            "select " +
                              "if(groupID is null,-1,groupID) as groupID, " +
                              "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                              "subTotal as Total,qty as Quantity " +
                            "from " +
                            "(select " +
                              "if( " +
                                "itemID<>0, " +
                                "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                                "if( " +
                                  "optionID<>0, " +
                                  "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                                ") " +
                               ") as groupID, " +
                              "sum(qty) as qty, " +
                              "sum(subTotal) as subTotal " +
                            "from " +
                            "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                            "from " +
                            "(select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "sum(ol.qty) as qty, " +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID " +
                            "union all " +
                            "select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "sum(ol.qty) as qty, " +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                            "group by itemID,optionID,dynID) as tmp1 " +
                            "group by groupID) as tmp2 " +

                            //"group by groupName " +

                            "union all " +
                               "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                                "group by groupDesc " +
                            "union all " +
                                "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekto + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                                "group by groupDesc " +
                            ") as tmp3 " +
                            "group by groupID,groupDesc " +
                            "";
                */

                string sql = "CALL SP_ReportDaily_LoadReportGroupQuanity('" + weekfrom + "');";
                if (Convert.ToInt16(dbconfig.AllowSalesFastFood) == 1)
                {
                    sql =
                           "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                           "from " +
                           "( " +
                           "select " +
                             "if(groupID is null,-1,groupID) as groupID, " +
                             "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                             "subTotal as Total,qty as Quantity " +
                           "from " +
                           "(select " +
                             "if( " +
                               "itemID<>0, " +
                               "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                               "if( " +
                                 "optionID<>0, " +
                                 "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                               ") " +
                              ") as groupID, " +
                             "sum(qty) as qty, " +
                             "sum(subTotal) as subTotal " +
                           "from " +
                           "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                           "from " +
                           "(select " +
                             "ol.itemID, " +
                             "ol.optionID, " +
                             "ol.dynID, " +
                             "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                             "sum(ol.subTotal) as subTotal " +
                           "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                           "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                           "group by ol.itemID,ol.optionID,ol.dynID " +
                           "union all " +
                           "select " +
                             "ol.itemID, " +
                             "ol.optionID, " +
                             "ol.dynID, " +
                             "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                             "sum(ol.subTotal) as subTotal " +
                           "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                           "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                           "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                           "group by itemID,optionID,dynID) as tmp1 " +
                           "group by groupID) as tmp2 " +

                           //"group by groupName " +

                           "union all " +
                              "select " +
                                 "-1 as groupID, " +
                                 "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                 "sum(ol.subTotal) as Total, " +
                                 "sum(ol.qty) as Quantity " +
                               "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                               "where date(o.ts)=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                               "group by groupDesc " +
                           "union all " +
                               "select " +
                                 "-1 as groupID, " +
                                 "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                 "sum(ol.subTotal) as Total, " +
                                 "sum(ol.qty) as Quantity " +
                               "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                               "where date(o.ts)>=date('" + weekto + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                               "group by groupDesc " +
                           ") as tmp3 " +
                           "group by groupID,groupDesc " +
                           "";
                }

                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);

                totalSaleFuel = 0;
                totalSaleNonFuel = 0;

                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    if (row["groupDesc"].ToString().Contains("Fuel"))
                    {
                        ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                        li.SubItems.Add(money.Format(Convert.ToDouble(row["Quantity"])));
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                        totalSaleFuel += Convert.ToDouble(row["Total"]);
                        li.Tag = Convert.ToInt32(row["groupID"]);
                        lstGroupItemFuelSales.Items.Add(li);
                        lstGroupItemFuelSales.Columns[2].Text = "Total = " + money.Format(totalSaleFuel);
                    }
                    else if (!row["groupDesc"].ToString().Contains("Fuel"))
                    {
                        ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                        li.SubItems.Add(string.Format("{0:0,0}", Convert.ToDouble(row["Quantity"])));
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                        totalSaleNonFuel += Convert.ToDouble(row["Total"]);
                        li.Tag = Convert.ToInt32(row["groupID"]);
                        lstGroupItemShopSales.Items.Add(li);
                        lstGroupItemShopSales.Columns[2].Text = "Total = " + money.Format(totalSaleNonFuel);
                    }
                }
                //strTotal = money.Format2(decTotal);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblTotal2.Text = "Tổng cộng : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
                    return;
                }
                else
                {
                    lblTotal2.Text = "Total : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily:::LoadReportGroupQuanity:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReportQuantity()
        {
            try
            {
                if (dbconfig.AllowSalesFuel == 0)
                {
                    lstQuantityReportFuelSales.Visible = false;
                    lstQuantityReportShopSales.Location = new System.Drawing.Point(4, 3);
                    lstQuantityReportShopSales.Height = 250;
                    tcSalesPerStaff.TabPages.RemoveAt(7);
                }

                mConnection.Open();
                lstQuantityReportFuelSales.Items.Clear();
                lstQuantityReportShopSales.Items.Clear();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = weekfrom;
                /*
                string sql = "select " +
                               "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                               "sum(qty) AS Quantity, " +
                               "isFuel, " +
                               "sum(subTotal) AS Total " +
                             "from " +
                             "(select " +
                               "if( " +
                                 "itemID<>0, " +
                                 "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                 "if( " +
                                   "optionID<>0, " +
                                   "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                    "'Dyn Item' " +
                                 ") " +
                                ") as groupName, " +
                               "sum(qty) as qty, " +
                               "isFuel, " +
                               "sum(subTotal) as subTotal " +
                             "from " +
                             "(select " +
                               "ol.itemID, " +
                               "ol.optionID, " +
                               "ol.dynID, " +
                               "sum(ol.qty) as qty, " +
                               "if(ol.itemID<>0,(select `isFuel` from itemsmenu where itemID=ol.itemID),0) as isFuel, " +
                               "sum(ol.subTotal) as subTotal " +
                             "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                             "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                             "group by ol.itemID,ol.optionID,ol.dynID " +
                             "union all " +
                             "select " +
                               "ol.itemID, " +
                               "ol.optionID, " +
                               "ol.dynID, " +
                               "sum(ol.qty) as qty, " +
                               "if(ol.itemID<>0,(select `isFuel` from itemsmenu where itemID=ol.itemID),0) as isFuel, " +
                               "sum(ol.subTotal) as subTotall " +
                             "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                             "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                             "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                             "group by groupName " +

                             "union all " +
                                "select " +
                                   "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                                   "sum(ol.qty) as qty, " +
                                   "0 as isFuel, " +
                                   "sum(ol.subTotal) as subTotal " +
                                 "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                 "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                                 "group by groupName " +
                             "union all " +
                                 "select " +
                                   "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                   "sum(ol.qty) as qty, " +
                                   "0 as isFuel, " +
                                   "sum(ol.subTotal) as subTotal " +
                                 "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                 "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                             "group by groupName) as tmp2 " +
                             "group by itemDesc " +
                             "";
                */
                //Tan sua sql thanh procedure
                //2015-07-15
                string sql = "call SP_ReportDaily_LoadReportQuantity('" + weekfrom + "')";

                if (Convert.ToInt16(config.AllowSalesFastFood) == 1)
                {
                    sql = "select " +
                              "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                              "sum(qty) AS Quantity, " +
                              "isFuel, " +
                              "sum(subTotal) AS Total " +
                            "from " +
                            "(select " +
                              "if( " +
                                "itemID<>0 and parentLine=0, " +
                                "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                "if( " +
                                "itemID<>0 and parentLine<>0, " +
                                "'item+ vao subitem', " +
                                "if( " +
                                  "optionID<>0, " +
                                  "(select concat(i.itemDesc,' (',il.itemDesc,')') from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                   "'Dyn Item' " +
                                ") " +
                               ")) as groupName, " +
                              "sum(qty) as qty, " +
                              "isFuel, " +
                              "if(itemID<>0 and parentLine<>0,0,if(optionID<>0,sum(subTotal + qty*(select i.unitPrice from itemsmenu i where i.itemID=(select il.itemID from itemslinemenu il where il.lineID=optionID) limit 0,1)),sum(subTotal))) as subTotal " +
                            "from " +
                            "(select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "ol.parentLine, " +
                              "sum(ol.qty) as qty, " +
                              "if(ol.itemID<>0,(select `isFuel` from itemsmenu where itemID=ol.itemID),0) as isFuel, " +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID " +
                            "union all " +
                            "select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "ol.parentLine, " +
                              "sum(ol.qty) as qty, " +
                              "if(ol.itemID<>0,(select `isFuel` from itemsmenu where itemID=ol.itemID),0) as isFuel, " +
                              "sum(ol.subTotal) as subTotall " +
                            "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 and refund=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                            "group by groupName " +

                            "union all " +
                               "select " +
                                  "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                                  "sum(ol.qty) as qty, " +
                                  "0 as isFuel, " +
                                  "sum(ol.subTotal) as subTotal " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                                "group by groupName " +
                            "union all " +
                                "select " +
                                  "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                  "sum(ol.qty) as qty, " +
                                  "0 as isFuel, " +
                                  "sum(ol.subTotal) as subTotal " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 and refund=0 " +
                            "group by groupName) as tmp2 " +
                            "group by itemDesc " +
                            "";
                }

                dtSource = new DataTable();

                dtSource = mConnection.Select(sql);
                totalSaleFuel = 0;
                totalSaleNonFuel = 0;

                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    if (Convert.ToDouble(row["Total"].ToString()) != 0)
                    {
                        if (Convert.ToInt32(row["isFuel"].ToString()) == 1)
                        {
                            ListViewItem li = new ListViewItem(row["itemDesc"].ToString());
                            li.SubItems.Add(money.Format(Convert.ToDouble(row["Quantity"])));
                            li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                            li.Tag = row["isFuel"].ToString();
                            totalSaleFuel += Convert.ToDouble(row["Total"]);
                            lstQuantityReportFuelSales.Items.Add(li);
                            lstQuantityReportFuelSales.Columns[2].Text = "Total = " + money.Format(totalSaleFuel);
                        }
                        else
                        {
                            ListViewItem li = new ListViewItem(row["itemDesc"].ToString());
                            li.SubItems.Add(string.Format("{0:0,0}", Convert.ToDouble(row["Quantity"])));
                            li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                            li.Tag = row["isFuel"].ToString();
                            totalSaleNonFuel += Convert.ToDouble(row["Total"]);
                            lstQuantityReportShopSales.Items.Add(li);
                            lstQuantityReportShopSales.Columns[2].Text = "Total = " + money.Format(totalSaleNonFuel);
                        }
                    }
                }
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblTotal1.Text = "Tổng cộng : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
                    return;
                }
                else
                {
                    lblTotal1.Text = "Total : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
                }
                
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadReportQuantity:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        /// <summary>
        /// load tab id
        /// </summary>
        private void LoadTabDaily()
        {
            try
            {
                mConnection.Open();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = weekfrom;
                // chua hieu
                //string sql = "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where date(dateCash)=" + "date('" + weekfrom + "')" + " AND CashType =5";
                string sql = "";
                System.Data.DataTable tbl;
                //tbl = mConnection.Select(sql);
                //double dblBankCashout = Convert.ToDouble(tbl.Rows[0]["Subtotal"].ToString() == "" ? 0 : tbl.Rows[0]["Subtotal"]);
                //labelBankEftposCashOut.Text = money.Format2(dblBankCashout);

                tbl = new DataTable();
                #region
                //sql = "select " +
                //                "sum(subTotal) as totalSales," +
                //                "sum(discount) as totalDisCount," +
                //                "sum(cash) as cash," +
                //    //"sum(eftpos-`change`) as eftpos," +
                //                "sum(eftpos) as eftpos," +
                //                "sum(returnPro) as returnPro," +
                //                "sum(creditNote) as creditNote, " +
                //                "sum(gst) as gst, " +
                //                "sum(account) as account, " +
                //                "sum(Surchart) as Surchart, " +
                //                "sum(FeeDelivery) as FeeDelivery, " +
                //                "sum(`change`) as `change` " +
                //             "from (" +
                //                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change`,`FeeDelivery` " +
                //                        "from ordersdaily " +
                //                        "where date(ts) =" + "date('" + weekfrom + "')" + " and (completed=1 or completed=2) " +
                //                    "union all " +
                //                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change`,`FeeDelivery` " +
                //                        "from ordersall " +
                //                        "where date(ts) =" + "date('" + weekfrom + "')" + " and (completed=1 or completed=2) " +
                //                     ") as total";
                #endregion
                /*sql = "select " +
                                "sum(subTotal) as totalSales," +
                                "sum(discount) as totalDisCount," +
                                "sum(cash) as cash," +
                    //"sum(eftpos-`change`) as eftpos," +
                                "sum(eftpos) as eftpos," +
                                "sum(returnPro) as returnPro," +
                                "sum(creditNote) as creditNote, " +
                                "sum(gst) as gst, " +
                                "sum(account) as account, " +
                                "sum(Surchart) as Surchart, " +
                                "sum(FeeDelivery) as FeeDelivery, " +
                                "sum(`change`) as `change` " +
                             "from (" +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change`,`FeeDelivery` " +
                                        "from ordersdaily " +
                                        "where shiftid in(select shiftid from shifts where date(ts) =" + "date('" + weekfrom + "'))" + " and (completed=1 or completed=2) " +
                                    "union all " +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change`,`FeeDelivery` " +
                                        "from ordersall " +
                                        "where shiftid in(select shiftid from shifts where date(ts) =" + "date('" + weekfrom + "')" +
                                     ")) as total";

                */
                sql = "Call SP_ReportDaily_LoadDefault('" + weekfrom + "')";
                tbl = mConnection.Select(sql);

                //DateTime dt = DateTime.Now;
                //labelDateTime.Text = "Date: " + dt.Day + "-" + dt.Month + "-" + dt.Year + " " + dt.ToLongTimeString();

                double sales = Convert.ToDouble(tbl.Rows[0]["totalSales"].ToString() == "" ? 0 : tbl.Rows[0]["totalSales"]);
                double discount = Convert.ToDouble(tbl.Rows[0]["totalDisCount"].ToString() == "" ? 0 : tbl.Rows[0]["totalDisCount"]);
                double refunc = Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]);
                double creditNote = Convert.ToDouble(tbl.Rows[0]["creditNote"].ToString() == "" ? 0 : tbl.Rows[0]["creditNote"]);
                double gst = Convert.ToDouble(tbl.Rows[0]["gst"].ToString() == "" ? 0 : tbl.Rows[0]["gst"]);
                double change = Convert.ToDouble(tbl.Rows[0]["change"].ToString() == "" ? 0 : tbl.Rows[0]["change"]);
                double account = Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]);
                double feeDelivery = Convert.ToDouble(tbl.Rows[0]["FeeDelivery"].ToString() == "" ? 0 : tbl.Rows[0]["FeeDelivery"]);

                double dis = Convert.ToDouble(mConnection.ExecuteScalar(
                        "select if(sum(subTotal) is null,0,sum(subTotal)) as subTotal from " +
                        "(" +
                            "select sum(ol.subTotal) as subTotal from ordersdailyline ol inner join ordersdaily o on o.orderID=ol.orderID where  date(ts) =" + "date('" + weekfrom + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                            "union all " +
                            "select sum(ol.subTotal) as subTotal from ordersallline ol inner join ordersall o on o.bkId=ol.bkId where  date(o.ts) =" + "date('" + weekfrom + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                        ") as tmp "
                        ));
                discount -= dis;
                sales -= dis;

                double cash = 0;
                double eftpos = 0;
                double refun = 0;

                try
                {
                    cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    eftpos = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    refun = Convert.ToDouble(tbl.Rows[0]["returnPro"]);
                }
                catch (Exception)
                { }

                labelTotalDiscount.Text = money.Format2(discount);
                labelTotalSale.Text = money.Format2(sales);
                labelTotalGST.Text = money.Format2(gst);
                labelCash.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? 0 : tbl.Rows[0]["cash"]));
                lblFeeDelivery.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["FeeDelivery"].ToString() == "" ? 0 : tbl.Rows[0]["FeeDelivery"]));
                labelCard.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["eftpos"].ToString() == "" ? 0 : tbl.Rows[0]["eftpos"]));
                labelRefunc.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]));
                lblSurcharge.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["Surchart"].ToString() == "" ? 0 : tbl.Rows[0]["Surchart"]));
                labelAccount.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]) * -1);

                //khoa cash in cash out =====================================
                double lCashIn = 0;
                double lSafeDrop = 0;
                double lCashOut = 0;
                double lPayout = 0;
                tbl = mConnection.Select(
                        "SELECT " +
                            "SUM(IF(cashtype=1,totalamount,0)) AS cashin, " +
                            "SUM(IF(cashtype=3,totalamount,0)) AS safedrop, " +
                            "SUM(IF(cashtype=5,totalamount,0)) AS cashout, " +
                            "SUM(IF(cashtype=6,totalamount,0)) AS payout " +
                        "from cashinandcashout where date(dateCash)=date('" + weekfrom + "')"
                    );
                try
                {
                    lCashIn = Convert.ToDouble(tbl.Rows[0]["cashin"]);
                    lSafeDrop = Convert.ToDouble(tbl.Rows[0]["safedrop"]);
                    lCashOut = Convert.ToDouble(tbl.Rows[0]["cashout"]);
                    lPayout = Convert.ToDouble(tbl.Rows[0]["payout"]);
                }
                catch (Exception)
                {
                }
                labelPayOut.Text = money.Format2(lPayout);
                labelSafeDrop.Text = money.Format2(lSafeDrop);
                labelCashIn.Text = money.Format2(lCashIn);
                labelBankEftposCashOut.Text = money.Format2(lCashOut);

                //end khoa cash in cash out ****************************************
                sql = "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where date(dateCash)=" + "date('" + weekfrom + "')" + " AND CashType =3";
                tbl = new DataTable();
                tbl = mConnection.Select(sql);

                sql = "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where date(dateCash)=" + "date('" + weekfrom + "')" + " AND CashType =4";
                tbl = new DataTable();
                tbl = mConnection.Select(sql);

                double decTotal, decTotalSurcharge;
                // duc sua 4-4-2013

                #region sua

                sql = "SELECT C.cardName,SUM(obc.Subtotal) AS Subtotal,SUM(obc.cashOut) AS Cashout,SUM(obc.surcharge) AS Surcharge " +
                       "FROM (SELECT DISTINCT orderbycardID " +
                       "FROM " +
                       "( " +
                           "SELECT DISTINCT od.orderbycardID " +
                           "FROM ordersdaily od " +
                           "where date(od.ts) = date('" + weekfrom + "') and (od.completed=1 or od.completed=2) " +
                           "Union all " +
                           "SELECT DISTINCT oa.orderbycardID " +
                           "FROM ordersall oa " +
                           "where date(oa.ts) =" + "date('" + weekfrom + "') and (oa.completed=1 or oa.completed=2) " +
                           "UNION ALL " +
                           "SELECT DISTINCT orderbycardID as orderbycardID " +
                           "FROM accountpayment " +
                           "where orderbycardID <>'' AND date(PayDate) =" + "date('" + weekfrom + "')" +
                       ")AS tmp " +
                       ") AS Source " +
                       "  inner join orderbycard obc on Source.orderbycardID=obc.ID " +
                       "  inner join card C on obc.cardID = C.cardID " +
                       "group by C.cardName ";

                #endregion sua

                lstCard.Items.Clear();
                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);
                decTotal = 0;
                decTotalSurcharge = 0;
                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    if (Convert.ToDouble(row["Subtotal"]) != 0)
                    {
                        ListViewItem li = new ListViewItem(row["CardName"].ToString());
                        //li.SubItems.Add(money.Format2(Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"] == "" ? "0" : row["cashOut"]) - Convert.ToDouble(row["Surcharge"] == "" ? "0" : row["Surcharge"])));
                        li.SubItems.Add(money.Format2(Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["Surcharge"])));
                        li.SubItems.Add(money.Format2(Convert.ToDouble(row["Surcharge"])));
                        //decTotal += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]) - Convert.ToDouble(row["Surcharge"] == "" ? "0" : row["Surcharge"]);
                        decTotal += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["Surcharge"]);
                        decTotalSurcharge += Convert.ToDouble(row["Surcharge"]);
                        lstCard.Items.Add(li);
                    }
                }

                double dblBalance = 0;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblTotalCard.Text = "Tổng số thẻ :" + money.Format2(decTotal + dblBalance * 1000);
                    lblTotalSurcharge.Text = "Tổng số phụ thu :" + money.Format2(decTotalSurcharge);
                    //return;
                }
                else
                {
                    lblTotalCard.Text = "Total card :" + money.Format2(decTotal + dblBalance * 1000);
                    lblTotalSurcharge.Text = "Total surcharge :" + money.Format2(decTotalSurcharge);
                }
                

                decTotal = 0;
                // duc sua 4-4-2013

                #region Duc Sua 4-4-2013

                sql = "SELECT CardName,SUM(Subtotal) AS Subtotal " +
                   "FROM " +
                   "( " +
                   "SELECT c.cardName AS CardName,SUM(obc.cashOut) AS Subtotal " +
                   "FROM orderbycard obc " +
                   "inner join ordersdaily od on od.orderbycardID=obc.ID " +
                   "inner join card c on obc.cardID = c.cardID " +
                   "where date(od.ts) =" + "date('" + weekfrom + "')" + " and (od.completed=1 or od.completed=2) and c.typeCard = 1 group by CardName " +
                   "Union all " +
                   "SELECT c.cardName,SUM(obc.cashOut) AS Subtotal " +
                   "FROM orderbycard obc " +
                   "inner join ordersall oa on oa.orderbycardID=obc.ID " +
                   "inner join card c on obc.cardID = c.cardID " +
                   "where date(oa.ts) =" + "date('" + weekfrom + "')" + " and (oa.completed=1 or oa.completed=2) and c.typeCard = 1 group by CardName " +
                   ")AS Source group by CardName";

                #endregion Duc Sua 4-4-2013

                lstCardCashOut.Items.Clear();
                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:1");
                decTotal = 0;

                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    if (Convert.ToDouble(row["Subtotal"]) != 0)
                    {
                        ListViewItem li = new ListViewItem(row["CardName"].ToString());
                        li.SubItems.Add(money.Format2(Convert.ToDouble(row["Subtotal"])));
                        decTotal += Convert.ToDouble(row["Subtotal"]);
                        lstCardCashOut.Items.Add(li);
                    }
                }

                dblBalance = 0;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    label28.Text = "Tổng số tiền thẻ :" + money.Format2(decTotal + dblBalance * 1000);
                    //return;
                }
                else
                {
                    label28.Text = "Total card amount :" + money.Format2(decTotal + dblBalance * 1000);
                }
                double cashAccount = 0;
                double cardAccount = 0;
                dtSource = mConnection.Select(
                    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where date(PayDate) =" + "date('" + weekfrom + "')" + " AND PayAmount<>0 ) as tmp "
                    );
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:1");
                if (dtSource.Rows.Count > 0)
                {
                    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
                }
                labelCardAccount.Text = money.Format2(cardAccount);
                labelCashAccount.Text = money.Format2(cashAccount);
                labelTotalAccount.Text = money.Format2(cardAccount + cashAccount);

                double sumCash = cash + cashAccount - refun - lCashOut + lCashIn - lSafeDrop - lPayout;
                double sumCard = eftpos + cardAccount;
                lblCashTotal.Text = money.Format2(sumCash);
                lblCardTotal.Text = money.Format2(sumCard);
                lblSumTotal.Text = money.Format2(sumCard + sumCash);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadTabDaily(string Shf)
        {
            try
            {
                mConnection.Open();
                //string dateWhere = POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(ts)=date(now()) ";
                string sql = POS.Properties.Settings.Default.AllowUseShift ?
                    "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where shiftID=" + Shf + " AND CashType =5" :
                    "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where shiftID=" + Shf + " and date(dateCash)=date(now()) AND CashType =5";
                System.Data.DataTable tbl = mConnection.Select(sql);
                tbl = new DataTable();
                tbl = mConnection.Select(sql);
                dblBankEftposCashOut = Convert.ToDouble(tbl.Rows[0]["Subtotal"].ToString() == "" ? 0 : tbl.Rows[0]["Subtotal"]);

                sql = "select " +
                                "sum(subTotal) as totalSales," +
                                "sum(discount) as totalDisCount," +
                                "sum(cash) as cash," +
                                "sum(eftpos-`change`) as eftpos," +
                                "sum(returnPro) as returnPro," +
                                "sum(creditNote) as creditNote, " +
                                "sum(gst) as gst, " +
                                "sum(account) as account, " +
                                "sum(`change`) as `change` " +
                             "from (" +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersdaily " +
                                        "where shiftID = " + Shf + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(ts)=date(now()) ") + " and (completed=1 or completed=2) " +
                                    "union all " +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersall " +
                                        "where shiftID = " + Shf + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(ts)=date(now()) ") + " and (completed=1 or completed=2) " +
                                     ") as total";

                tbl = mConnection.Select(sql);
                dblTotalSales = Convert.ToDouble(tbl.Rows[0]["totalSales"].ToString() == "" ? 0 : tbl.Rows[0]["totalSales"]);
                dblTotalDiscount = Convert.ToDouble(tbl.Rows[0]["totalDisCount"].ToString() == "" ? 0 : tbl.Rows[0]["totalDisCount"]);
                dblTotalReturn = Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]);
                dblTotalAccount = Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]);
                dblTotalCard = Convert.ToDouble(tbl.Rows[0]["eftpos"].ToString() == "" ? 0 : tbl.Rows[0]["eftpos"]);
                dblCash = Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? 0 : tbl.Rows[0]["cash"]);

                tbl = new DataTable();
                tbl = mConnection.Select("SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where shiftID=" + Shf + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(dateCash)=date(now()) ") + " AND CashType =3");
                dblCashDrop = Convert.ToDouble(tbl.Rows[0]["Subtotal"].ToString() == "" ? 0 : tbl.Rows[0]["Subtotal"]);

                tbl = new DataTable();
                tbl = mConnection.Select("select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where shiftID = " + Shf + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(PayDate)=date(now()) ") + " AND PayAmount<>0 ) as tmp ");
                if (tbl.Rows.Count > 0)
                {
                    dblTotalCashAccount = Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? "0" : tbl.Rows[0]["cash"]);
                }

                dtSource = new DataTable();
                dtSource = mConnection.Select("select RealTotalInSafeLastShift,RealTotalInSafe,CashOut from shifts where shiftID = " + Shf);
                if (dtSource.Rows.Count != 0)
                {
                    if (Convert.ToInt32(Shf) == 1)
                        dblFloatIn = 0;
                    else
                        dblFloatIn = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafeLastShift"].ToString() == "" ? "0" : dtSource.Rows[0]["RealTotalInSafeLastShift"].ToString());
                    dblFloatOut = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafe"].ToString() == "" ? "0" : dtSource.Rows[0]["RealTotalInSafe"].ToString());
                    dblCashFloat = dblFloatOut;
                }

                sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                                "FROM " +
                                "( " +
                                "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND shiftID = " + Shf + " " + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(dateCash)=date(now()) ") +
                                "UNION ALL " +
                                "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 3) AND shiftID = " + Shf + " " + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(dateCash)=date(now()) ") +
                                ")AS Source";
                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);
                dblCashDrop = Convert.ToDouble(dtSource.Rows[0]["CashOut"].ToString() == "" ? 0 : dtSource.Rows[0]["CashOut"]);

                sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                        "FROM " +
                        "( " +
                        "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND shiftID = '" + Shf + "' " + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(dateCash)=date(now()) ") +
                        "UNION ALL " +
                        "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 2 OR CashType = 3 OR CashType = 4 OR CashType = 5) AND shiftID = " + Shf + " " + (POS.Properties.Settings.Default.AllowUseShift ? "" : " and date(dateCash)=date(now()) ") +
                        ")AS Source";
                tbl = new DataTable();
                tbl = mConnection.Select(sql);

                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    try
                    {
                        dblCashIn = Convert.ToDouble(row["CashIn"]);
                    }
                    catch (Exception)
                    {
                        dblCashIn = 0;
                    }

                    try
                    {
                        dblCashOut = Convert.ToDouble(row["CashOut"]);
                    }
                    catch (Exception)
                    {
                        dblCashOut = 0;
                    }
                }

                dblTotalCash = dblCash + dblTotalCashAccount - dblTotalReturn;
                dblClosingBar = dblTotalCash + dblTotalCard + dblTotalAccount - dblTotalReturn;
                dblTotalSale = dblTotalSales - dblTotalReturn - dblTotalDiscount;
                dblBalanceCount = ((dblCashOut + dblCashFloat) - (dblTotalCash + dblCashIn + dblFloatIn));

                ListViewItem li = new ListViewItem(Shf);
                li.SubItems.Add(money.Format2(dblFloatIn));
                li.SubItems.Add(money.Format2(dblCashFloat));
                li.SubItems.Add(money.Format2(dblCashDrop));
                li.SubItems.Add(money.Format2(dblFloatOut));
                li.SubItems.Add(money.Format2(dblBalanceCount));
                li.Tag = Convert.ToInt32(Shf).ToString();
                lstHistoryShift.Items.Add(li);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadTabEnable()
        {
            if (!rconfig.IsEnableStockTab)
            {
                tcSalesPerStaff.TabPages.RemoveAt(11);
            }
            if (!rconfig.IsEnablepickupTab)
            {
                tcSalesPerStaff.TabPages.RemoveAt(4);
            }

            if (!rconfig.IsEnableDeliveryListTab)
            {
                tcSalesPerStaff.TabPages.RemoveAt(3);
            }
        }

        private void lstCreditAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstCreditAccount.SelectedIndices.Count > 0)
            {
                strMemberNo = Convert.ToString(lstCreditAccount.SelectedItems[0].Tag);
            }
        }

        private void lstDebitAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstDebitAccount.SelectedIndices.Count > 0)
            {
                intAccountID = Convert.ToInt32(lstDebitAccount.SelectedItems[0].Tag);
            }
        }

        private void lstEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void lstGroupItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                intGroupID = Convert.ToInt32(lstGroupItemFuelSales.SelectedItems[0].Tag);
            }
        }

        private void lstHistoryShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstHistoryShift.SelectedItems.Count > 0)
            {
                string dateString = GetDateString(dateTimePickerfrom.Value);
                string sql = "SELECT CashType,dateCash,sum( TotalAmount ) AS Cash,Description FROM cashinandcashout c where date(dateCash)=date('" + dateString + "') and ShiftID = " + lstHistoryShift.SelectedItems[0].Tag + " group by dateCash";
                lstCashOut.Items.Clear();
                DataTable tbl = new DataTable();
                tbl = mConnection.Select(sql);

                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    ListViewItem li = new ListViewItem(Convert.ToInt32(row["CashType"]) == 2 ? "Cash Out" : "Cash In");
                    li.SubItems.Add(row["dateCash"].ToString());
                    li.SubItems.Add(money.Format2(Convert.ToDouble(row["Cash"])).ToString());
                    li.SubItems.Add(row["Description"].ToString());
                    lstCashOut.Items.Add(li);
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void printDocument_PrintPageDailySalesReport(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportDailySales(e, l_y);
        }

        private void printDocument_PrintPageReportAll(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            l_y = PrintPageReportDailySales(e, l_y);
            l_y = PrintPageReportQuantity(e, l_y);
            l_y = PrintPageReportGroupQuantity(e, l_y);
            l_y = PrintPageReportEmployeeQuantity(e, l_y);
            l_y = PrintPageReportAccount(e, l_y);
            l_y = PrintPageReportCreditAccount(e, l_y);
        }

        private void printDocument_PrintPageReportAccount(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportAccount(e, l_y);
        }

        private void printDocument_PrintPageReportCreditAccount(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportCreditAccount(e, l_y);
        }

        private void printDocument_PrintPageReportDelivery(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportDelivery(e, l_y);
        }

        private void printDocument_PrintPageReportEmployeeQuantity(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportEmployeeQuantity(e, l_y);
        }

        private void printDocument_PrintPageReportGroupQuantity(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            l_y = PrintPageReportGroupQuantity(e, l_y);
        }

        private void printDocument_PrintPageReportListPickup(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportListPickup(e, l_y);
        }

        private void printDocument_PrintPageReportQuantity(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            PrintPageReportQuantity(e, l_y);
        }

        private float PrintPageReportAccount(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo Tài khoản", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Account Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                mPrinter.DrawString(" Khách hàng số ", e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString("Tổng thành tiền", e, new Font("Arial", 13, FontStyle.Bold), l_y, 0);
            }
            else
            {
                mPrinter.DrawString(" Customer No ", e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString("Subtotal", e, new Font("Arial", 13, FontStyle.Bold), l_y, 0);
            }
            
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine() / 5;
            foreach (ListViewItem lvi in lstDebitAccount.Items)
            {
                mPrinter.DrawString(lvi.SubItems[3].Text, e, new Font("Arial", 10), l_y, 0);
                l_y = mPrinter.DrawString(lvi.SubItems[0].Text.Substring(0, 3) + "..." + lvi.SubItems[0].Text.Substring(lvi.SubItems[0].Text.Length - 3, 3) + ":" + lvi.SubItems[1].Text, e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 0);
            }
            l_y += mPrinter.GetHeightPrinterLine() / 10;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Tổng cộng :" + strTotal, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }
            else
            {
                l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Total :" + strTotal, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }
           

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
            return l_y;
        }

        private float PrintPageReportCreditAccount(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo tài khoản", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Account Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                mPrinter.DrawString(" Khách hàng số ", e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString("Tổng thành tiền", e, new Font("Arial", 13, FontStyle.Bold), l_y, 0);
            }
            else
            {
                mPrinter.DrawString(" Customer No ", e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString("Subtotal", e, new Font("Arial", 13, FontStyle.Bold), l_y, 0);
            }
            
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine() / 5;
            foreach (ListViewItem lvi in lstCreditAccount.Items)
            {
                mPrinter.DrawString(lvi.SubItems[0].Text.Substring(0, 3) + "..." + lvi.SubItems[0].Text.Substring(lvi.SubItems[0].Text.Length - 3, 3) + ":" + lvi.SubItems[1].Text, e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lvi.SubItems[3].Text, e, new Font("Arial", 10), l_y, 0);
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 0);
            }

            l_y += mPrinter.GetHeightPrinterLine() / 10;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotalCreditAccount, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Tổng cộng :" + strTotalCreditAccount, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotalCreditAccount, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }
            else
            {
                l_y = mPrinter.DrawLine("Total :" + strTotalCreditAccount, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Total :" + strTotalCreditAccount, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Total :" + strTotalCreditAccount, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
            return l_y;
        }

        private float PrintPageReportDailySales(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo bán hàng hàng ngày", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine();
                l_y = mPrinter.DrawString("Ngày :" + dateTimePickerfrom.Text, e, new Font("Arial", 10, FontStyle.Italic), l_y, 1);
                l_y += mPrinter.GetHeightPrinterLine() / 5;

                mPrinter.DrawString("Bán hàng: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalSale.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tổng giảm giá: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalDiscount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tổng thuế: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalGST.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tính tiền: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCash.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Phí vận chuyển: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lblFeeDelivery.Text, e, new Font("Arial", 10), l_y, 3);
                //mPrinter.DrawString("Total Eftpos: ", e, new Font("Arial", 10), l_y, 1);
                //l_y = mPrinter.DrawString(labelCreditNote.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Thẻ: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCard.Text, e, new Font("Arial", 10), l_y, 3);

                if (lstCard.Items.Count != 0)
                {
                    foreach (ListViewItem item in lstCard.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                    }
                }

                mPrinter.DrawString("Trả lại hàng: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelRefunc.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tiền thêm vào: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCashIn.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Thối tiền: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelPayOut.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Safe Drop: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelSafeDrop.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Bank eftpos cash out: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelBankEftposCashOut.Text, e, new Font("Arial", 10), l_y, 3);

                if (lstCardCashOut.Items.Count != 0)
                {
                    foreach (ListViewItem item in lstCardCashOut.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                    }
                }

                mPrinter.DrawString("Tài khoản: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                //mPrinter.DrawString("Account: ", e, new Font("Arial", 10), l_y, 1);
                //l_y = mPrinter.DrawString(labelAccount.Text, e, new Font("Arial", 10), l_y, 3);

                l_y = mPrinter.DrawString("TRẢ NỢ TÀI KHOẢN", e, new Font("Arial", 12, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine() / 5;

                mPrinter.DrawString("Thẻ: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCardAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tính tiền: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCashAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                mPrinter.DrawString("Tổng tính tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblCashTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                mPrinter.DrawString("Tổng thẻ: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblCardTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblSumTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                l_y += mPrinter.GetHeightPrinterLine() / 5;
                l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
                l_y += mPrinter.GetHeightPrinterLine();

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                dblTotalFuel = 0;
                dblTotalNonFuel = 0;

                l_y = mPrinter.DrawString("Báo cáo mặt hàng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine();
                if (dbconfig.AllowSalesFuel == 1)
                {
                    if (lstQuantityReportFuelSales.Items.Count != 0)
                    {
                        mPrinter.DrawString("Nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Lít", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                        l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        foreach (ListViewItem item in lstQuantityReportFuelSales.Items)
                        {
                            ListViewItem lvi = new ListViewItem();
                            if (Convert.ToInt32(item.Tag) == 1)
                            {
                                mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                                //mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);

                                mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);
                                l_y = mPrinter.DrawString(item.SubItems[2].Text, e, new Font("Arial", 10), l_y, 3);
                                dblTotalFuel += Convert.ToDouble(item.SubItems[2].Text);
                            }
                        }
                        mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        l_y = mPrinter.DrawString(String.Format("{0:0.00}", dblTotalFuel), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        l_y += mPrinter.GetHeightPrinterLine(); // was 100 LTFeb12
                    }
                }

                if (lstGroupItemShopSales.Items.Count != 0)
                {
                    mPrinter.DrawString("Số lượng - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    //mPrinter.DrawString("Qty", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    foreach (ListViewItem item in lstGroupItemShopSales.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString(item.SubItems[1].Text + " - " + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        //mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);
                        l_y = mPrinter.DrawString(item.SubItems[2].Text, e, new Font("Arial", 10), l_y, 3);
                        dblTotalNonFuel += Convert.ToDouble(item.SubItems[2].Text);
                    }
                    mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    l_y = mPrinter.DrawString(String.Format("{0:0.00}", dblTotalNonFuel), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
            }
            else
            {
                l_y = mPrinter.DrawString("Daily Sales Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine();
                l_y = mPrinter.DrawString("Date :" + dateTimePickerfrom.Text, e, new Font("Arial", 10, FontStyle.Italic), l_y, 1);
                l_y += mPrinter.GetHeightPrinterLine() / 5;

                mPrinter.DrawString("Sales: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalSale.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Total Discount: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalDiscount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Total GST: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalGST.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Cash: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCash.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Fee Delivery: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lblFeeDelivery.Text, e, new Font("Arial", 10), l_y, 3);
                //mPrinter.DrawString("Total Eftpos: ", e, new Font("Arial", 10), l_y, 1);
                //l_y = mPrinter.DrawString(labelCreditNote.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Card: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCard.Text, e, new Font("Arial", 10), l_y, 3);

                if (lstCard.Items.Count != 0)
                {
                    foreach (ListViewItem item in lstCard.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                    }
                }

                mPrinter.DrawString("Refund: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelRefunc.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Cash In: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCashIn.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Pay Out: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelPayOut.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Safe Drop: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelSafeDrop.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Bank eftpos cash out: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelBankEftposCashOut.Text, e, new Font("Arial", 10), l_y, 3);

                if (lstCardCashOut.Items.Count != 0)
                {
                    foreach (ListViewItem item in lstCardCashOut.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                    }
                }

                mPrinter.DrawString("Account: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                //mPrinter.DrawString("Account: ", e, new Font("Arial", 10), l_y, 1);
                //l_y = mPrinter.DrawString(labelAccount.Text, e, new Font("Arial", 10), l_y, 3);

                l_y = mPrinter.DrawString("ACCOUNT PAYMENT", e, new Font("Arial", 12, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine() / 5;

                mPrinter.DrawString("Card: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCardAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Cash: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelCashAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Total: ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(labelTotalAccount.Text, e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                mPrinter.DrawString("Total Cash: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblCashTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                mPrinter.DrawString("Total Card: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblCardTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                mPrinter.DrawString("Sum: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawString(lblSumTotal.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

                l_y += mPrinter.GetHeightPrinterLine() / 5;
                l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
                l_y += mPrinter.GetHeightPrinterLine();

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                dblTotalFuel = 0;
                dblTotalNonFuel = 0;

                l_y = mPrinter.DrawString("Item Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                l_y += mPrinter.GetHeightPrinterLine();
                if (dbconfig.AllowSalesFuel == 1)
                {
                    if (lstQuantityReportFuelSales.Items.Count != 0)
                    {
                        mPrinter.DrawString("Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Litre", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                        l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        foreach (ListViewItem item in lstQuantityReportFuelSales.Items)
                        {
                            ListViewItem lvi = new ListViewItem();
                            if (Convert.ToInt32(item.Tag) == 1)
                            {
                                mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                                //mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);

                                mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);
                                l_y = mPrinter.DrawString(item.SubItems[2].Text, e, new Font("Arial", 10), l_y, 3);
                                dblTotalFuel += Convert.ToDouble(item.SubItems[2].Text);
                            }
                        }
                        mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        l_y = mPrinter.DrawString(String.Format("{0:0.00}", dblTotalFuel), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        l_y += mPrinter.GetHeightPrinterLine(); // was 100 LTFeb12
                    }
                }

                if (lstGroupItemShopSales.Items.Count != 0)
                {
                    mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    //mPrinter.DrawString("Qty", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    foreach (ListViewItem item in lstGroupItemShopSales.Items)
                    {
                        ListViewItem lvi = new ListViewItem();
                        mPrinter.DrawString(item.SubItems[1].Text + " - " + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                        //mPrinter.DrawString(item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 2);
                        l_y = mPrinter.DrawString(item.SubItems[2].Text, e, new Font("Arial", 10), l_y, 3);
                        dblTotalNonFuel += Convert.ToDouble(item.SubItems[2].Text);
                    }
                    mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    l_y = mPrinter.DrawString(String.Format("{0:0.00}", dblTotalNonFuel), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
            }
            

            return l_y;
        }

        private float PrintPageReportDelivery(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo giao hàng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Delivery Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();

            if (ucListDelivery.lstList.Items.Count > 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("TS - Order", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    mPrinter.DrawString("Tên", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("Thành tiền", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                else
                {
                    mPrinter.DrawString("TS - Order", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    mPrinter.DrawString("Name", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("SubTotal", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                try
                {
                    foreach (ListViewItem item in ucListDelivery.lstList.Items)
                    {
                        if (Convert.ToInt32(item.Tag) == 0)
                        {
                            DateTime ts = Convert.ToDateTime(item.SubItems[0].Text);
                            mPrinter.DrawString(ts.Hour + ":" + ts.Minute + ":" + ts.Second + " - " + item.SubItems[1].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 2);
                            l_y = mPrinter.DrawString(item.SubItems[3].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }           
            return l_y;
        }

        private float PrintPageReportEmployeeQuantity(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo số lượng nhân viên", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Employee Quantity Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;
            foreach (ListViewItem lvi in lstEmployee.Items)
            {
                mPrinter.DrawString(lvi.SubItems[0].Text, e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lvi.SubItems[1].Text, e, new Font("Arial", 10), l_y, 0);
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            }

            l_y += mPrinter.GetHeightPrinterLine() / 10;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotalEmployeeQuanity, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Tổng cộng :" + strTotalEmployeeQuanity, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Tổng cộng :" + strTotalEmployeeQuanity, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }
            else
            {
                l_y = mPrinter.DrawLine("Total :" + strTotalEmployeeQuanity, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                l_y = mPrinter.DrawString("Total :" + strTotalEmployeeQuanity, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
                l_y = mPrinter.DrawLine("Total :" + strTotalEmployeeQuanity, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            }

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            return l_y;
        }

        private float PrintPageReportGroupQuantity(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo số lượng nhóm", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Group Quantity Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);

            totalSaleFuel = 0;
            totalSaleNonFuel = 0;
            if (dbconfig.AllowSalesFuel == 1)
            {
                if (lstGroupItemFuelSales.Items.Count != 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        l_y = mPrinter.DrawString("Báo cáo mặt hàng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                    }
                    else { l_y = mPrinter.DrawString("Item Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
                    l_y += mPrinter.GetHeightPrinterLine();
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("Nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Lít", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    }
                    else
                    {
                        mPrinter.DrawString("Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Litre", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    }
                   
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    try
                    {
                        foreach (ListViewItem item in lstGroupItemFuelSales.Items)
                        {
                            mPrinter.DrawString("  *" + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            mPrinter.DrawString(item.SubItems[1].Text.ToString(), e, new Font("Arial", 10), l_y, 2);
                            l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                            totalSaleFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                        }
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        }
                        else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                        l_y = mPrinter.DrawString(totalSaleFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        l_y += mPrinter.GetHeightPrinterLine();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            if (lstGroupItemShopSales.Items.Count != 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Số lượng  - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                }
                else { mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                try
                {
                    foreach (ListViewItem item in lstGroupItemShopSales.Items)
                    {
                        mPrinter.DrawString(item.SubItems[1].Text.ToString() + " - " + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                        totalSaleNonFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                    }
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString(totalSaleNonFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                catch (Exception)
                {
                }
            }

            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
            }
            else { mPrinter.DrawString("Total: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
            
            l_y = mPrinter.DrawString((totalSaleFuel + totalSaleNonFuel).ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            return l_y;
        }

        private float PrintPageReportListPickup(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Pickup Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Pickup Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();

            if (ucListPickup.lstList.Items.Count > 0)
            {
                mPrinter.DrawString("TS - Order", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Tên", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("Thành tiền", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                else
                {
                    mPrinter.DrawString("Name", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    l_y = mPrinter.DrawString("SubTotal", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                try
                {
                    foreach (ListViewItem item in ucListDelivery.lstList.Items)
                    {
                        if (Convert.ToInt32(item.Tag) == 0)
                        {
                            DateTime ts = Convert.ToDateTime(item.SubItems[0].Text);
                            mPrinter.DrawString(ts.Hour + ":" + ts.Minute + ":" + ts.Second + " - " + item.SubItems[1].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 2);
                            l_y = mPrinter.DrawString(item.SubItems[3].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            return l_y;
        }

        private float PrintPageReportQuantity(System.Drawing.Printing.PrintPageEventArgs e, float l_y)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo số lượng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else
            {
                l_y = mPrinter.DrawString("Quantity Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;
            totalSaleFuel = 0;
            totalSaleNonFuel = 0;
            if (dbconfig.AllowSalesFuel == 1)
            {
                if (lstQuantityReportFuelSales.Items.Count != 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        l_y = mPrinter.DrawString("Báo cáo mặt hàng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                        l_y += mPrinter.GetHeightPrinterLine();
                        mPrinter.DrawString("Nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Lít", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    }
                    else
                    {
                        l_y = mPrinter.DrawString("Item Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                        l_y += mPrinter.GetHeightPrinterLine();
                        mPrinter.DrawString("Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Litre", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    }
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    try
                    {
                        foreach (ListViewItem item in lstQuantityReportFuelSales.Items)
                        {
                            if (Convert.ToInt32(item.Tag) == 1)
                            {
                                mPrinter.DrawString("  *" + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                                mPrinter.DrawString(item.SubItems[1].Text.ToString(), e, new Font("Arial", 10), l_y, 2);
                                l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                                totalSaleFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                            }
                        }
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        }
                        else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                        l_y = mPrinter.DrawString(totalSaleFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        l_y += mPrinter.GetHeightPrinterLine();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            if (lstQuantityReportShopSales.Items.Count != 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Số lượng - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                }
                else { mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                try
                {
                    foreach (ListViewItem item in lstQuantityReportShopSales.Items)
                    {
                        if (Convert.ToInt32(item.Tag) == 0)
                        {
                            mPrinter.DrawString(item.SubItems[1].Text.ToString() + " - " + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                            totalSaleNonFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                        }
                    }
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    l_y = mPrinter.DrawString(totalSaleNonFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                catch (Exception)
                {
                }
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
            }
            else { mPrinter.DrawString("Total: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
            l_y = mPrinter.DrawString((totalSaleFuel + totalSaleNonFuel).ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            return l_y;
        }

        private string SplitDateFrom(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[1].ToString() + "-" + listdate[0].ToString() + " " + "00:00:00";
        }

        private string SplitDateTo(DateTime date)
        {
            if (date.Day == DateTime.Now.Day)
            {
                date = DateTime.Now;
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[1].ToString() + "-" + listdate[0].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
            }
            else
            {
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "23:59:59";
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Visible = true;
            String strSelectionTabpage = tcSalesPerStaff.SelectedTab.Name.ToString();
            LoadReportControl(strSelectionTabpage);
        }

        private void tabPage9_Click(object sender, EventArgs e)
        {
        }

        private void UCReportDaily_Load(object sender, EventArgs e)
        {
            LoadTabDaily();
            LoadReportQuantity();
            LoadReportGroupQuanity();
            LoadTabEnable();
            //LoadReportQuantity();
            //LoadReportGroupQuanity();
            //LoadReport();
        }

        private class DriverOff
        {
            public string OrderID;
            public int ShiftID;
            public DateTime Ts;

            public DriverOff(string orderid, DateTime ts, int shiftID)
            {
                OrderID = orderid;
                Ts = ts;
                ShiftID = shiftID;
            }
        }
    }
}