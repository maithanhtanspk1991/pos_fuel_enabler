﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCNewItem : UserControl
    {
        private Connection.Connection conn = new Connection.Connection();
        private Class.ReadConfig readconfig = new Class.ReadConfig();

        public UCNewItem()
        {
            InitializeComponent();
            checkBox1.Checked = false;
            checkBox2.Checked = true;
        }

        //private void textBox6_Enter(object sender, EventArgs e)
        //{
        //    Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox6);
        //    textBox6.Tag = textBox6.BackColor;
        //    textBox6.BackColor = Color.White;
        //    frm.ShowDialog();
        //}

        private void textBox1_Leave(object sender, EventArgs e)
        {
            textBox1.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            textBox3.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            textBox4.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox5_Leave(object sender, EventArgs e)
        {
            textBox5.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox1);
            textBox1.Tag = textBox1.BackColor;
            textBox1.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox3);
            textBox3.Tag = textBox3.BackColor;
            textBox3.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox5_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox5);
            textBox5.Tag = textBox5.BackColor;
            textBox5.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox4_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox4);
            textBox4.Tag = textBox4.BackColor;
            textBox4.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxScancode_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBoxScancode);
            textBoxScancode.Tag = textBoxScancode.BackColor;
            textBoxScancode.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxScancode_Leave(object sender, EventArgs e)
        {
            textBoxScancode.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxScancode.Text = RandomBarcode();
            }
            catch (Exception)
            {
            }
        }

        public string CalculateChecksumDigit()
        {
            //DateTime tmeNow = DateTime.Now;
            //int mls = tmeNow.Millisecond;
            //Random rndNumber = new Random(mls);
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 9; i++)
            {
                resuilt += rd.Next(10);
            }
            //string sTemp = readconfig.NationCode + rndNumber.Next(1000,9999).ToString() + rndNumber.Next(10000,99999).ToString();
            string sTemp = readconfig.NationCode + resuilt;
            int iSum = 0;
            int iDigit = 0;

            // Calculate the checksum digit here.

            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                // This appears to be backwards but the

                // EAN-13 checksum must be calculated

                // this way to be compatible with UPC-A.

                if (i % 2 == 0)
                { // odd
                    iSum += iDigit * 3;
                }
                else
                { // even
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
            //this.ChecksumDigit = iCheckSum.ToString();
        }

        private string RandomBarcode()
        {
            string scancode = CalculateChecksumDigit();
            int ishave = 1;
            try
            {
                while (ishave > 0)
                {
                    conn.Open();
                    ishave = Convert.ToInt32(conn.ExecuteScalar("select count(scanCode) from itemsmenu where scanCode=" + scancode));
                    conn.Close();
                }
                return scancode;
            }
            catch (Exception)
            {
                return scancode;
            }
        }

        private void txtgst_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(txtgst);
            txtgst.Tag = txtgst.BackColor;
            txtgst.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void txtgst_Leave(object sender, EventArgs e)
        {
            txtgst.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}