﻿namespace POS
{
    partial class UCItems
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBelongToOrder = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblItemName = new System.Windows.Forms.Label();
            this.lblNameCard = new System.Windows.Forms.Label();
            this.btnItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBelongToOrder
            // 
            this.lblBelongToOrder.BackColor = System.Drawing.Color.White;
            this.lblBelongToOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBelongToOrder.ForeColor = System.Drawing.Color.Maroon;
            this.lblBelongToOrder.Location = new System.Drawing.Point(249, 9);
            this.lblBelongToOrder.Name = "lblBelongToOrder";
            this.lblBelongToOrder.Size = new System.Drawing.Size(69, 23);
            this.lblBelongToOrder.TabIndex = 10;
            this.lblBelongToOrder.Text = "1";
            this.lblBelongToOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(55, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 29);
            this.label2.TabIndex = 13;
            this.label2.Text = "Belong To Order :";
            // 
            // lblQuantity
            // 
            this.lblQuantity.BackColor = System.Drawing.Color.White;
            this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.ForeColor = System.Drawing.Color.Maroon;
            this.lblQuantity.Location = new System.Drawing.Point(164, 55);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(154, 23);
            this.lblQuantity.TabIndex = 12;
            this.lblQuantity.Text = "100";
            this.lblQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(55, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "Quantity :";
            // 
            // lblItemName
            // 
            this.lblItemName.BackColor = System.Drawing.Color.White;
            this.lblItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemName.ForeColor = System.Drawing.Color.Maroon;
            this.lblItemName.Location = new System.Drawing.Point(184, 32);
            this.lblItemName.Name = "lblItemName";
            this.lblItemName.Size = new System.Drawing.Size(134, 23);
            this.lblItemName.TabIndex = 6;
            this.lblItemName.Text = "Items A";
            this.lblItemName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNameCard
            // 
            this.lblNameCard.BackColor = System.Drawing.Color.White;
            this.lblNameCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameCard.ForeColor = System.Drawing.Color.Navy;
            this.lblNameCard.Location = new System.Drawing.Point(55, 32);
            this.lblNameCard.Name = "lblNameCard";
            this.lblNameCard.Size = new System.Drawing.Size(134, 23);
            this.lblNameCard.TabIndex = 9;
            this.lblNameCard.Text = "Item Name :";
            // 
            // btnItem
            // 
            this.btnItem.BackColor = System.Drawing.SystemColors.Window;
            this.btnItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItem.ForeColor = System.Drawing.Color.Navy;
            this.btnItem.Image = global::POS.Properties.Resources.UnCheck;
            this.btnItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnItem.Location = new System.Drawing.Point(0, 0);
            this.btnItem.Name = "btnItem";
            this.btnItem.Size = new System.Drawing.Size(326, 92);
            this.btnItem.TabIndex = 5;
            this.btnItem.UseVisualStyleBackColor = false;
            // 
            // UCItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblBelongToOrder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblItemName);
            this.Controls.Add(this.lblNameCard);
            this.Controls.Add(this.btnItem);
            this.Name = "UCItems";
            this.Size = new System.Drawing.Size(326, 92);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblBelongToOrder;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lblQuantity;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblItemName;
        public System.Windows.Forms.Label lblNameCard;
        public System.Windows.Forms.Button btnItem;
    }
}
