﻿using System;

namespace POS
{
    internal class TextBoxNumbericPOS : System.Windows.Forms.TextBox
    {
        public UCkeypad ucKeypad { get; set; }

        public UCkeypadDiv uckeypadDiv { get; set; }

        public TextBoxNumbericPOS()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
        }

        protected override void OnEnter(EventArgs e)
        {
            if (ucKeypad != null)
            {
                this.BackColor = System.Drawing.Color.White;
                ucKeypad.txtResult = this;
            }
            if (uckeypadDiv != null)
            {
                this.BackColor = System.Drawing.Color.White;
                uckeypadDiv.txtresult = this;
            }
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }

        public void SetForcus()
        {
            OnEnter(null);
        }
    }
}