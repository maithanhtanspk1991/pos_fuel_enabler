﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Properties;
using POS.Forms;

namespace POS
{
    public partial class UCReportWeekly : UserControl
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private DataTable dtSource;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private string strTotalSale;
        private int i = 0;
        private string weekfrom;
        private string weekto;
        private string strTotal = "";

        //============================
        private string strDate;

        private double dblTotalSales = 0;
        private double dblTotalDisCount = 0;
        private double dblTotalGTS = 0;
        private double dblTotalCard = 0;
        private double dblTotalCash = 0;
        private double dblTotalRefuncs = 0;
        private double dblTotalAccount = 0;
        private double dblTotalDeposits = 0;
        private double dblTotalCardAccounts = 0;
        private double dblTotalCashAccount = 0;
        private double dblTotalTotalAccount = 0;
        private double dblTotalTotalDeposit = 0;
        private double dblTotalCreditNote = 0;
        private double dblTotalCreditTotal = 0;
        private double dblSumTotalCashIn = 0;
        private double dblSumTotalCashOut = 0;
        private double dblTotalSalesFuel = 0;
        private double dblTotalSalesNonFuel = 0;
        private double dbTotalCashIn = 0;
        private double dbTotalCashOut = 0;
        private double dblSumTotalCash = 0;
        private double dblSumTotalCard = 0;
        private double dblTotalSumTotal = 0;
        private double dblTotalSurcharge = 0;
        private double dblTotalPayOut = 0;
        private double dblSubTotalSale = 0;

        private DataTable dtSourceDelivery;
        private DataTable dtSourcePickup;
        private DataTable dtSourceShift;
        private DataTable dtListFuelSales;
        private DataTable dtListGroupSales;
        private WeeklyReport dailyReport;
        private List<WeeklyReport> Report;
        private Class.ReadConfig mReadConfig = new ReadConfig();
        //============================

        public UCReportWeekly(Class.MoneyFortmat m)
        {
            InitializeComponent();
            SetMultiLanguage();
            //textBox1.Text = DateTime.UtcNow.ToShortDateString();
            mConnection = new Connection.Connection();
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mSetting = new Class.Setting();
            money = m;
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Báo cáo bán hàng trong tuần";
                label6.Text = "TỪ";
                label8.Text = "ĐẾN";
                //label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnPrint.Text = "IN";
                btnPrintAllWeek.Text = "IN TẤT CẢ";
                tabPage1.Text = "Báo cáo bán hàng trong tuần";
                tabPage2.Text = "Báo cáo số lượng trong tuần";
                tabPage3.Text = "Báo cáo số lượng nhóm trong tuần";
                tabPage4.Text = "Báo cáo giao và nhận hàng trong tuần";
                columnHeader1.Text = "Tên sản phẩm - BÁN HÀNG NHIÊN LIỆU";
                columnHeader2.Text = "Số lượng";
                columnHeader4.Text = "Tổng cộng";
                columnHeader7.Text = "Tên sản phẩm - BÁN HÀNG CỬA HÀNG";
                columnHeader8.Text = "Số lượng";
                colTotal.Text = "Tổng cộng";
                columnHeader3.Text = "Tên nhóm - BÁN HÀNG NHIÊN LIỆU";
                columnHeader4.Text = "Số lượng";
                columnHeader6.Text = "Tổng cộng";
                columnHeader43.Text = "Tên nhóm - BÁN HÀNG CỬA HÀNG";
                columnHeader44.Text = "Số lượng";
                columnHeader45.Text = "Tổng cộng";
                button11.Text = "In";
                button1.Text = "In";
                label20.Text = "Danh sách giao hàng------------------------------------";
                label2.Text = "Danh sách nhận hàng-----------------------------------";
                return;
            }
        }

        private double totalSaleFuel, totalSaleNonFuel;

        private void printDocument_PrintPageReportQuantity(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo số lượng hàng tuần", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Weekly Quantity Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2); }
            
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + " -> " + dateTimePickerto.Value.Day.ToString() + "/" + dateTimePickerto.Value.Month.ToString() + "/" + dateTimePickerto.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;

            //foreach (ListViewItem lvi in lstQuantityReportFuelSales.Items)
            //{
            //    mPrinter.DrawString(lvi.SubItems[1].Text + " " + lvi.SubItems[0].Text + " ", e, new Font("Arial", 10), l_y, 1);
            //    l_y = mPrinter.DrawString(lvi.SubItems[2].Text, e, new Font("Arial", 10), l_y, 0);
            //    l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            //}

            //l_y += 10;
            //l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            //l_y = mPrinter.DrawString("Total :" + strTotal, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
            //l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);

            totalSaleFuel = 0;
            totalSaleNonFuel = 0;
            if (dbconfig.AllowSalesFuel == 1)
            {
                if (lstQuantityReportFuelSales.Items.Count != 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        l_y = mPrinter.DrawString("Báo cáo mặt hàng", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                        l_y += 100;
                        mPrinter.DrawString("Nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Lít", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                        l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    }
                    else {
                        l_y = mPrinter.DrawString("Item Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                        l_y += 100;
                        mPrinter.DrawString("Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        mPrinter.DrawString("Litre", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                        l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    }
                    
                   
                    try
                    {
                        foreach (ListViewItem item in lstQuantityReportFuelSales.Items)
                        {
                            if (Convert.ToInt32(item.Tag) == 1)
                            {
                                mPrinter.DrawString("  *" + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                                mPrinter.DrawString(item.SubItems[1].Text.ToString(), e, new Font("Arial", 10), l_y, 2);
                                l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                                totalSaleFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                            }
                        }
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        }
                        else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                        l_y = mPrinter.DrawString(totalSaleFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        l_y += 120;
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            if (lstQuantityReportShopSales.Items.Count != 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Số lượng - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                }
                else { mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                try
                {
                    foreach (ListViewItem item in lstQuantityReportShopSales.Items)
                    {
                        if (Convert.ToInt32(item.Tag) == 0)
                        {
                            mPrinter.DrawString(item.SubItems[1].Text.ToString() + " - " + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                            totalSaleNonFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                        }
                    }
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString(totalSaleNonFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                catch (Exception)
                {
                }
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
            }
            else { mPrinter.DrawString("Total: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
            
            l_y = mPrinter.DrawString((totalSaleFuel + totalSaleNonFuel).ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

            l_y += 100;
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            l_y += 100;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += 100;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            
        }

        private void printDocument_PrintPageReportGroupQuantity(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Báo cáo số lượng nhóm hàng tuần", e, new Font("Arial", 12, FontStyle.Bold), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Weekly Group Quantity Report", e, new Font("Arial", 12, FontStyle.Bold), l_y, 2); }
           
            l_y = mPrinter.DrawString("(" + dateTimePickerfrom.Value.Day.ToString() + "/" + dateTimePickerfrom.Value.Month.ToString() + "/" + dateTimePickerfrom.Value.Year.ToString() + " -> " + dateTimePickerto.Value.Day.ToString() + "/" + dateTimePickerto.Value.Month.ToString() + "/" + dateTimePickerto.Value.Year.ToString() + ")", e, new System.Drawing.Font("Arial", 12), l_y, 2);
            //int i = 0;
            //foreach (ListViewItem lvi in lstGroupItemFuelSales.Items)
            //{
            //    mPrinter.DrawString(lvi.SubItems[1].Text + " " + lvi.SubItems[0].Text + " ", e, new Font("Arial", 10), l_y, 1);
            //    l_y = mPrinter.DrawString(lvi.SubItems[2].Text, e, new Font("Arial", 10), l_y, 0);
            //    l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            //}

            //l_y += 10;
            //l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            //l_y = mPrinter.DrawString("Total :" + strTotal, e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
            //l_y = mPrinter.DrawLine("Total :" + strTotal, new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);

            totalSaleFuel = 0;
            totalSaleNonFuel = 0;
            if (dbconfig.AllowSalesFuel == 1)
            {
                if (lstGroupItemFuelSales.Items.Count != 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("Số lượng - Nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("Qty  - Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    try
                    {
                        foreach (ListViewItem item in lstGroupItemFuelSales.Items)
                        {
                            mPrinter.DrawString(item.SubItems[1].Text.ToString() + " - " + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                            l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                            totalSaleFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                        }
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        }
                        else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                        l_y = mPrinter.DrawString(totalSaleFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            if (lstGroupItemShopSales.Items.Count != 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Số lượng - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                }
                else { mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                try
                {
                    foreach (ListViewItem item in lstGroupItemShopSales.Items)
                    {
                        mPrinter.DrawString(item.SubItems[1].Text.ToString() + " - " + item.SubItems[0].Text.ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                        l_y = mPrinter.DrawString(item.SubItems[2].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                        totalSaleNonFuel += Convert.ToDouble(item.SubItems[2].Text.ToString());
                    }
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString(totalSaleNonFuel.ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                catch (Exception)
                {
                }
            }

            if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
            else { mPrinter.DrawString("Total: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
            l_y = mPrinter.DrawString((totalSaleFuel + totalSaleNonFuel).ToString(), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);

            l_y += 100;
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            l_y += 100;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += 100;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2);
            }
            else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Print Report Weekly");
            try
            {
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportWeekly.button1_Click::" + ex.Message);
            }
        }

        private string SplitDate(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[1].ToString() + "-" + listdate[0].ToString() + "-" + listdate[2].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
        }

        private string SplitDateFrom(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "00:00:00";
        }

        private string SplitDateTo(DateTime date)
        {
            if (date.Day == DateTime.Now.Day)
            {
                date = DateTime.Now;
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
            }
            else
            {
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "23:59:59";
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                float l_y = 0;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    l_y = mPrinter.DrawString("BÁO CÁO TUẦN", e, new Font("Arial", 22), l_y, 2);
                }
                else { l_y = mPrinter.DrawString("WEEKLY REPORT", e, new Font("Arial", 22), l_y, 2); }
                
                l_y += 10;
                if (i == 7)
                {
                    l_y = mPrinter.DrawString("(" + Report[i].day + ")", e, new Font("Arial", 14), l_y, 2);
                    l_y += 10;
                }
                else
                {
                    l_y = mPrinter.DrawString("(" + Report[i].date + ")", e, new Font("Arial", 14), l_y, 2);
                    l_y += 10;
                }

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);

                l_y += 10;
                if (mReadConfig.LanguageCode.Equals("vi")) { l_y = mPrinter.DrawString("BÁN HÀNG", e, new Font("Arial", 16), l_y, 2); }
                else { l_y = mPrinter.DrawString("SALES", e, new Font("Arial", 16), l_y, 2); }
                
                l_y += 20;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tổng bán hàng: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Total Sales: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].totalSales, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tổng bán hàng nhiên liệu: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Total Fuel Sales: ", e, new Font("Arial", 12), l_y, 1); }
                l_y = mPrinter.DrawString("$" + Report[i].TotalSalesFuel, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Tổng cửa hàng bán: ", e, new Font("Arial", 12), l_y, 1);
                }
                else { mPrinter.DrawString("Total Shop Sales: ", e, new Font("Arial", 12), l_y, 1); }
                l_y = mPrinter.DrawString("$" + Report[i].TotalSalesNonFuel, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Tổng giảm giá: ", e, new Font("Arial", 12), l_y, 1);
                }
                else { mPrinter.DrawString("Total Discount: ", e, new Font("Arial", 12), l_y, 1); }
               
                l_y = mPrinter.DrawString("$" + Report[i].totalDisCount, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Tổng thuế: ", e, new Font("Arial", 12), l_y, 1);
                }
                else { mPrinter.DrawString("Total GST: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].totalGTS, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tính tiền: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Cash: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].totalCash, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Thẻ: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Card: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].totalCard, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Trả lại hàng: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Refund: ", e, new Font("Arial", 12), l_y, 1); }
                l_y = mPrinter.DrawString("$" + Report[i].totalReturnPro, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Ghi chú ghi nợ: ", e, new Font("Arial", 12), l_y, 1);
                }
                else { mPrinter.DrawString("Credit Note: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].CreditNote, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tiền mặt: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Cash In: ", e, new Font("Arial", 12), l_y, 1); }
               
                l_y = mPrinter.DrawString("$" + Report[i].sumTotalCashIn, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Rút tiền: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Cash Out: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].sumTotalCashOut, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Thanh toán hết nợ: ", e, new Font("Arial", 12), l_y, 1);
                }
                else { mPrinter.DrawString("Pay Out: ", e, new Font("Arial", 12), l_y, 1); }
               
                l_y = mPrinter.DrawString("$" + Report[i].PayOut, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Thanh toán hết nợ: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Pay Out: ", e, new Font("Arial", 12), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].PayOut, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Tài khoản: ", e, new Font("Arial", 12), l_y, 1); }
                else { mPrinter.DrawString("Account: ", e, new Font("Arial", 12), l_y, 1); }
               
                l_y = mPrinter.DrawString("$" + Report[i].Account, e, new Font("Arial", 12), l_y, 3);
                l_y += 5;
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Thành tiền bán hàng: ", e, new Font("Arial", 12, FontStyle.Bold), l_y, 1); }
                else { mPrinter.DrawString("SubTotal Sales: ", e, new Font("Arial", 12, FontStyle.Bold), l_y, 1); }
                
                l_y = mPrinter.DrawString("$" + Report[i].SubtotalSales, e, new Font("Arial", 12,FontStyle.Bold), l_y, 3);
                l_y += 5;
                if (mReadConfig.LanguageCode.Equals("vi")) { l_y = mPrinter.DrawString("THẺ", e, new Font("Arial", 16), l_y, 2); }
                else { l_y = mPrinter.DrawString("CARD", e, new Font("Arial", 16), l_y, 2); }
                
                l_y += 20;
                if (Report[i].ListCard.Rows.Count != 0)
                {
                    foreach (DataRow dr in Report[i].ListCard.Rows)
                    {
                        mPrinter.DrawString(" *" + dr[0].ToString(), e, new Font("Arial", 12), l_y, 1);
                        l_y = mPrinter.DrawString("$" + money.Format(dr[1].ToString()), e, new Font("Arial", 12), l_y, 3);
                        l_y += 5;
                    }
                    l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
                    l_y += 5;
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("Thành tiền Thẻ: ", e, new Font("Arial", 12, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("SubTotal Card: ", e, new Font("Arial", 12, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString("$" + Report[i].totalCard, e, new Font("Arial", 12, FontStyle.Bold), l_y, 3);
                    l_y += 5;
                }

                if (Settings.Default.AllowUseShift)
                {
                    if (Report[i].ListShift.Rows.Count != 0)
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            l_y = mPrinter.DrawString("BÁO CÁO CA", e, new Font("Arial", 16), l_y, 2);
                        }
                        else { l_y = mPrinter.DrawString("SHIFT REPORT", e, new Font("Arial", 16), l_y, 2); }
                        
                        l_y += 2;
                        foreach (DataRow dr in Report[i].ListShift.Rows)
                        {
                            if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("Ca: " + dr[0].ToString(), e, new Font("Arial", 12), l_y, 1); }
                            else { mPrinter.DrawString("Shift: " + dr[0].ToString(), e, new Font("Arial", 12), l_y, 1); }
                           
                            l_y = mPrinter.DrawString("$" + money.Format(dr[1].ToString()), e, new Font("Arial", 12), l_y, 3);
                            l_y += 5;
                        }
                    }
                }
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    l_y = mPrinter.DrawString("THANH TOÁN TÀI KHOẢN", e, new Font("Arial", 16), l_y, 2);
                }
                else { l_y = mPrinter.DrawString("ACCOUNT PAYMENT", e, new Font("Arial", 16), l_y, 2); }
               
                l_y += 20;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    mPrinter.DrawString("Thẻ: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].cardAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                    mPrinter.DrawString("Tiền mặt: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].cashAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                    mPrinter.DrawString("Tổng cộng: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].TotalAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                }
                else { 
                    mPrinter.DrawString("Card: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].cardAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                    mPrinter.DrawString("Cash: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].cashAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                    mPrinter.DrawString("Total: ", e, new Font("Arial", 12), l_y, 1);
                    l_y = mPrinter.DrawString("$" + Report[i].TotalAccount, e, new Font("Arial", 12), l_y, 3);
                    l_y += 5;
                }
                
                #region
                //l_y = mPrinter.DrawString("Account: " + Report[i].Account, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("Total Discount: " + Report[i].Deposit, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                #endregion
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
                totalSaleFuel = 0;
                totalSaleNonFuel = 0;
                if (dbconfig.AllowSalesFuel == 1)
                {
                    #region
                    //if (Report[i].ListFuelSales.Rows.Count != 0)
                    //{
                    //    l_y = mPrinter.DrawString("Item Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
                    //    l_y += 10;
                    //    mPrinter.DrawString("Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    //    mPrinter.DrawString("Litre", e, new Font("Arial", 10, FontStyle.Bold), l_y, 2);
                    //    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    //    try
                    //    {
                    //        foreach (DataRow row in Report[i].ListFuelSales.Rows)
                    //        {
                    //            if (Convert.ToInt32(row["visual"]) == 1)
                    //            {
                    //                mPrinter.DrawString("  *" + row["itemDesc"].ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                    //                mPrinter.DrawString(money.Format(Convert.ToDouble(row["Quantity"].ToString())), e, new Font("Arial", 10), l_y, 2);
                    //                l_y = mPrinter.DrawString(money.Format(Convert.ToDouble(row["Total"].ToString())), e, new Font("Arial", 10), l_y, 3);
                    //                totalSaleFuel += Convert.ToDouble(row["Total"].ToString());
                    //            }
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {
                    //    }
                    #endregion
                        //mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                        //l_y = mPrinter.DrawString(money.Format(Convert.ToDouble(totalSaleFuel)), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                        //l_y += 12;
                    //}
                }

                if (Report[i].ListGroupSales.Rows.Count == 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        mPrinter.DrawString("Số lượng - Phi nhiên liệu", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                    }
                    else { mPrinter.DrawString("Qty  - Non Fuel", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    l_y = mPrinter.DrawString("$", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                    try
                    {
                        foreach (DataRow row in Report[i].ListGroupSales.Rows)
                        {
                            if (!row["groupDesc"].Equals("Fuel"))
                            {
                                mPrinter.DrawString(row["Quantity"].ToString() + " - " + row["groupDesc"].ToString() + ": ", e, new Font("Arial", 10), l_y, 1);
                                l_y = mPrinter.DrawString(money.Format(Convert.ToDouble(row["Total"].ToString())), e, new Font("Arial", 10), l_y, 3);
                                totalSaleNonFuel += Convert.ToDouble(row["Total"].ToString());
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    if (mReadConfig.LanguageCode.Equals("vi")) { mPrinter.DrawString("   Thành tiền: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    else { mPrinter.DrawString("   Subtotal: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1); }
                    
                    l_y = mPrinter.DrawString(money.Format(Convert.ToDouble(totalSaleNonFuel)), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                }
                //mPrinter.DrawString("Total: ", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
                //l_y = mPrinter.DrawString(money.Format(totalSaleFuel + totalSaleNonFuel), e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
                #region
                //l_y += 100;
                //l_y = mPrinter.DrawString("ACCOUNT PAYMENT", e, new Font("Arial", 16), l_y, 2);
                //l_y += 200;
                //l_y = mPrinter.DrawString("Card: " + Report[i].cardAccount, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("Cash: " + Report[i].cashAccount, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("Total: " + Report[i].TotalAccount, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);

                //l_y += 100;
                //l_y = mPrinter.DrawString("DEPOSIT PAYMENT", e, new Font("Arial", 16), l_y, 2);
                //l_y += 200;
                //l_y = mPrinter.DrawString("Card: " + Report[i].cardDeposit, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("Cash: " + Report[i].cashDeposit, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("Total: " + Report[i].totalDeposit, e, new Font("Arial", 12), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);

                //l_y += 100;
                //l_y = mPrinter.DrawString("TOTAL CASH: " + Report[i].sumTotalCash, e, new Font("Arial", 16), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("TOTAL CARD: " + Report[i].sumTotalCard, e, new Font("Arial", 16), l_y, 1);
                //l_y += 100;
                //l_y = mPrinter.DrawString("TOTAL SUM: " + Report[i].SumTotal, e, new Font("Arial", 16), l_y, 1);
                //l_y += 200;
                //l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
                #endregion
                l_y += 20;
                if (mReadConfig.LanguageCode.Equals("vi")) { l_y = mPrinter.DrawString("Ngày in: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
                else { l_y = mPrinter.DrawString("Print Date: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString(), e, new Font("Arial", 10), l_y, 2); }
                
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Print report all week:::" + ex.Message);
            }
        }

        private string GetDateString(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        private void LoadReportQuantity()
        {
            try
            {
                {
                    if (dbconfig.AllowSalesFuel == 0)
                    {
                        lstQuantityReportFuelSales.Visible = false;
                        lstQuantityReportShopSales.Location = new Point(0, 0);
                        lstQuantityReportShopSales.Height = 250;
                    }

                    mConnection.Open();
                    lstQuantityReportFuelSales.Items.Clear();
                    lstQuantityReportShopSales.Items.Clear();
                    weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                    weekto = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day;

                    string sql = "select " +
                                  "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                                  "sum(qty) AS Quantity, " +
                                  "visual, " +
                                  "sum(subTotal) AS Total " +
                                "from " +
                                "(select " +
                                  "if( " +
                                    "itemID<>0, " +
                                    "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                    "if( " +
                                      "optionID<>0, " +
                                      "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                       "'Dyn Item' " +
                                    ") " +
                                   ") as groupName, " +
                                  "sum(qty) as qty, " +
                                  "visual, " +
                                  "sum(subTotal) as subTotal " +
                                "from " +
                                "(select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotal " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID " +
                                "union all " +
                                "select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotall " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                                "group by groupName " +

                                "union all " +
                                   "select " +
                                      "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                    "group by groupName " +
                                "union all " +
                                    "select " +
                                      "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupName) as tmp2 " +
                                "group by itemDesc " +
                                "";
                    if (Convert.ToInt16(dbconfig.AllowSalesFastFood) == 1)
                    {
                        sql = "select " +
                                  "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                                  "sum(qty) AS Quantity, " +
                                  "visual, " +
                                  "sum(subTotal) AS Total " +
                                "from " +
                                "(select " +
                                  "if( " +
                                    "itemID<>0 and parentLine=0, " +
                                    "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                    "if( " +
                                    "itemID<>0 and parentLine<>0, " +
                                    "'item+ vao subitem', " +
                                    "if( " +
                                  "optionID<>0, " +
                                  "(select concat(i.itemDesc,' (',il.itemDesc,')') from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                       "'Dyn Item' " +
                                    ") " +
                                   ")) as groupName, " +
                                  "sum(qty) as qty, " +
                                  "visual, " +
                                  "if(itemID<>0 and parentLine<>0,0,if(optionID<>0,sum(subTotal + qty*(select i.unitPrice from itemsmenu i where i.itemID=(select il.itemID from itemslinemenu il where il.lineID=optionID) limit 0,1)),sum(subTotal))) as subTotal " +
                                "from " +
                                "(select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "ol.parentLine, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotal " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID " +
                                "union all " +
                                "select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "ol.parentLine, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotall " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                                "group by groupName " +

                                "union all " +
                                   "select " +
                                      "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                    "group by groupName " +
                                "union all " +
                                    "select " +
                                      "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupName) as tmp2 " +
                                "group by itemDesc " +
                                "";
                    }

                    dtSource = new DataTable();
                    dtSource = mConnection.Select(sql);
                    totalSaleFuel = 0;
                    totalSaleNonFuel = 0;
                    foreach (System.Data.DataRow row in dtSource.Rows)
                    {
                        if (Convert.ToDouble(row["Total"].ToString()) != 0)
                        {
                            if (Convert.ToInt32(row["visual"].ToString()) == 1)
                            {
                                ListViewItem li = new ListViewItem(row["itemDesc"].ToString());
                                li.SubItems.Add(money.Format(Convert.ToDouble(row["Quantity"])));
                                li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                                li.Tag = row["visual"].ToString();
                                //decTotal += Convert.ToDouble(row["Total"]);
                                totalSaleFuel += Convert.ToDouble(row["Total"]);
                                lstQuantityReportFuelSales.Items.Add(li);
                                lstQuantityReportFuelSales.Columns[2].Text = "Total = " + money.Format(totalSaleFuel);
                            }
                            else
                            {
                                ListViewItem li = new ListViewItem(row["itemDesc"].ToString());
                                li.SubItems.Add(string.Format("{0:0,0}", Convert.ToDouble(row["Quantity"])));
                                li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                                li.Tag = row["visual"].ToString();
                                //decTotal += Convert.ToDouble(row["Total"]);
                                totalSaleNonFuel += Convert.ToDouble(row["Total"]);
                                lstQuantityReportShopSales.Items.Add(li);
                                lstQuantityReportShopSales.Columns[2].Text = "Total = " + money.Format(totalSaleNonFuel);
                            }
                        }
                    }
                    //strTotal = money.Format(decTotal);
                    lblTotal1.Text = "Total : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportWeekly:::LoadReportQuantity:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadReportGroupQuanity()
        {
            try
            {
                if (dbconfig.AllowSalesFuel == 0)
                {
                    lstGroupItemFuelSales.Visible = false;
                    lstGroupItemShopSales.Location = new Point(0, 0);
                    lstGroupItemShopSales.Height = 250;
                }

                mConnection.Open();
                lstGroupItemFuelSales.Items.Clear();
                lstGroupItemShopSales.Items.Clear();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day;

                string sql =
                            "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                            "from " +
                            "( " +
                            "select " +
                              "if(groupID is null,-1,groupID) as groupID, " +
                              "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                              "subTotal as Total,qty as Quantity " +
                            "from " +
                            "(select " +
                              "if( " +
                                "itemID<>0, " +
                                "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                                "if( " +
                                  "optionID<>0, " +
                                  "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                                ") " +
                               ") as groupID, " +
                              "sum(qty) as qty, " +
                              "sum(subTotal) as subTotal " +
                            "from " +
                            "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                            "from " +
                            "(select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "sum(ol.qty) as qty, " +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID " +
                            "union all " +
                            "select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "sum(ol.qty) as qty, " +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                            "group by itemID,optionID,dynID) as tmp1 " +
                            "group by groupID) as tmp2 " +

                            //"group by groupName " +

                            "union all " +
                               "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupDesc " +
                            "union all " +
                                "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupDesc " +
                            ") as tmp3 " +
                            "group by groupID,groupDesc " +
                            "";

                if (Convert.ToInt16(dbconfig.AllowSalesFastFood) == 1)
                {
                    sql =
                            "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                            "from " +
                            "( " +
                            "select " +
                              "if(groupID is null,-1,groupID) as groupID, " +
                              "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                              "subTotal as Total,qty as Quantity " +
                            "from " +
                            "(select " +
                              "if( " +
                                "itemID<>0, " +
                                "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                                "if( " +
                                  "optionID<>0, " +
                                  "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                                ") " +
                               ") as groupID, " +
                              "sum(qty) as qty, " +
                              "sum(subTotal) as subTotal " +
                            "from " +
                            "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                            "from " +
                            "(select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID " +
                            "union all " +
                            "select " +
                              "ol.itemID, " +
                              "ol.optionID, " +
                              "ol.dynID, " +
                              "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                              "sum(ol.subTotal) as subTotal " +
                            "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                            "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                            "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                            "group by itemID,optionID,dynID) as tmp1 " +
                            "group by groupID) as tmp2 " +

                            //"group by groupName " +

                            "union all " +
                               "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupDesc " +
                            "union all " +
                                "select " +
                                  "-1 as groupID, " +
                                  "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                  "sum(ol.subTotal) as Total, " +
                                  "sum(ol.qty) as Quantity " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + weekfrom + "') AND date(o.ts)<=date('" + weekto + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupDesc " +
                            ") as tmp3 " +
                            "group by groupID,groupDesc " +
                            "";
                }

                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);

                //double decTotal = 0;
                totalSaleFuel = 0;
                totalSaleNonFuel = 0;

                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    if (row["groupDesc"].ToString().Contains("Fuel"))
                    {
                        ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                        li.SubItems.Add(money.Format(Convert.ToDouble(row["Quantity"])));
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                        //decTotal += Convert.ToDouble(row["Total"]);
                        totalSaleFuel += Convert.ToDouble(row["Total"]);
                        li.Tag = Convert.ToInt32(row["groupID"]);
                        lstGroupItemFuelSales.Items.Add(li);
                        lstGroupItemFuelSales.Columns[2].Text = "Total = " + money.Format(totalSaleFuel);
                    }
                    else if (!row["groupDesc"].ToString().Contains("Fuel"))
                    {
                        ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                        li.SubItems.Add(string.Format("{0:0,0}", Convert.ToDouble(row["Quantity"])));
                        li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Total"]))));
                        //decTotal += Convert.ToDouble(row["Total"]);
                        totalSaleNonFuel += Convert.ToDouble(row["Total"]);
                        li.Tag = Convert.ToInt32(row["groupID"]);
                        lstGroupItemShopSales.Items.Add(li);
                        lstGroupItemShopSales.Columns[2].Text = "Total = " + money.Format(totalSaleNonFuel);
                    }
                }
                //strTotal = money.Format2(decTotal);
                lblTotal2.Text = "Total : " + money.Format2(totalSaleFuel + totalSaleNonFuel);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportWeekly:::LoadReportGroupQuanity:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                try
                {
                    flow_layout_Week.Controls.Clear();
                    LoadWeeklyButton();
                    btnPrint.Visible = true;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 0:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                try
                {
                    LoadReportQuantity();
                    btnPrint.Visible = false;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 1:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                try
                {
                    LoadReportGroupQuanity();
                    btnPrint.Visible = false;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportGroupQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 2:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 3)
            {
                try
                {
                    LoadListDeliveryAndPickup();
                    btnPrint.Visible = false;
                    //mPrinter = new Printer();
                    //mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportGroupQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 2:::" + ex.Message);
                }
            }
        }

        private void LoadListDeliveryAndPickup()
        {
            try
            {
                mConnection.Open();
                weekfrom = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                weekto = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day;
                string strCondition = "AND date(ts) >=" + "date('" + weekfrom + "') AND date(ts) <=" + "date('" + weekto + "') ";
                ucListDelivery.LoadReport(mConnection, money, 0, 1, strCondition);
                ucListPickup.LoadReport(mConnection, money, 1, 0, strCondition);
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }
        #region
        //private void LoadWeeklyButton()
        //{
        //    dailyReport = new WeeklyReport();
        //    Report = new List<WeeklyReport>();
        //    for (int i = 0; i < 7; i++)
        //    {
        //        string dateString = GetDateString(dateTimePickerto.Value.AddDays((-1) * i));
        //        mConnection.Open();
        //        string sql = "select " +
        //                        "sum(subTotal) as totalSales," +
        //                        "sum(discount) as totalDisCount," +
        //                        "sum(cash) as cash," +
        //                        "sum(eftpos) as eftpos," +
        //                        "sum(returnPro) as returnPro," +
        //                        "sum(creditNote) as creditNote, " +
        //                        "sum(gst) as gst, " +
        //                        "sum(account) as account, " +
        //                        "sum(`change`) as `change` " +
        //                     "from (" +
        //                            "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
        //                                "from ordersdaily " +
        //                                "where date(ts)=" + "date('" + dateString + "')" + " and (completed=1 or completed=2) " +
        //                            "union all " +
        //                            "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
        //                                "from ordersall " +
        //                                "where date(ts)=" + "date('" + dateString + "')" + " and (completed=1 or completed=2) " +
        //                           ") as total";
        //        dtSource = new DataTable();
        //        dtSource = mConnection.Select(sql);
        //        double totalSales = 0;
        //        double totalDisCount = 0;
        //        double cash = 0;
        //        double eftpos = 0;
        //        double returnPro = 0;
        //        double account = 0;
        //        double creditNote = 0;
        //        double cashIn = 0;
        //        double cashOut = 0;
        //        double cardAccount = 0;
        //        double cashAccount = 0;
        //        double totalSaleNonFuel = 0;
        //        double totalSaleFuel = 0;
        //        double gst = 0;
        //        double change = 0;
        //        try
        //        {
        //            totalSales = Convert.ToDouble(dtSource.Rows[0]["totalSales"]);
        //            totalDisCount = Convert.ToDouble(dtSource.Rows[0]["totalDisCount"]);
        //            cash = Convert.ToDouble(dtSource.Rows[0]["cash"]);
        //            eftpos = Convert.ToDouble(dtSource.Rows[0]["eftpos"]);
        //            returnPro = Convert.ToDouble(dtSource.Rows[0]["returnPro"]);
        //            creditNote = Convert.ToDouble(dtSource.Rows[0]["creditNote"]);
        //            account = Convert.ToDouble(dtSource.Rows[0]["account"]);
        //            gst = Convert.ToDouble(dtSource.Rows[0]["gst"]);
        //            change = Convert.ToDouble(dtSource.Rows[0]["change"]);
        //            double dis = Convert.ToDouble(mConnection.ExecuteScalar(
        //                "select if(sum(subTotal) is null,0,sum(subTotal)) as subTotal from " +
        //                "(" +
        //                    "select sum(ol.subTotal) as subTotal from ordersdailyline ol inner join ordersdaily o on o.orderID=ol.orderID where  date(ts) =" + "date('" + dateString + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
        //                    "union all " +
        //                    "select sum(ol.subTotal) as subTotal from ordersallline ol inner join ordersall o on o.bkId=ol.bkId where  date(o.ts) =" + "date('" + dateString + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
        //                ") as tmp "
        //                ));
        //            totalDisCount -= dis;
        //            totalSales -= dis;
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        try
        //        {
        //            dtSource = mConnection.Select(
        //                "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where date(PayDate)=date('" + dateString + "') AND PayAmount<>0 ) as tmp"
        //                );
        //            if (dtSource.Rows.Count > 0)
        //            {
        //                cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
        //                cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
        //            }
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        try
        //        {
        //            //sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
        //            //    "FROM " +
        //            //    "( " +
        //            //    "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND date(dateCash)=" + "date('" + dateString + "') " +
        //            //    "UNION ALL " +
        //            //    "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where CashType = 2 AND date(dateCash)=" + "date('" + dateString + "') " +
        //            //    ")AS Source";
        //            //dtSource = new DataTable();
        //            //dtSource = mConnection.Select(sql);

        //            sql = "select SUM(CashIn) AS CashIn , SUM(CashOut) AS CashOut " +
        //                "FROM " +
        //                "( " +
        //                "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND date(dateCash)=" + "date('" + dateString + "') " +
        //                "UNION ALL " +
        //                "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 2 OR CashType = 3 OR CashType = 4 OR CashType = 5) AND date(dateCash)=" + "date('" + dateString + "') " +
        //                ")AS Source";
        //            dtSource = new DataTable();
        //            dtSource = mConnection.Select(sql);

        //            foreach (System.Data.DataRow row in dtSource.Rows)
        //            {
        //                cashIn = Convert.ToDouble(row["CashIn"] == "" ? "0" : row["CashIn"]);
        //                cashOut = Convert.ToDouble(row["CashOut"] == "" ? "0" : row["CashOut"]);
        //            }
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        dtSourceShift = new DataTable();
        //        dtSourceShift.Columns.Add("shiftID", typeof(String));
        //        dtSourceShift.Columns.Add("balanceCount", typeof(Double));
        //        dtSource = mConnection.Select("SELECT shiftID FROM shifts s where date(ts)=date('" + dateString + "')");
        //        foreach (DataRow dr in dtSource.Rows)
        //        {
        //            try
        //            {
        //                LoadTabDaily(dr["shiftID"].ToString());
        //            }
        //            catch (Exception)
        //            {
        //                LoadTabDaily("1");
        //            }
        //        }

        //        sql = "select " +
        //                  "if(groupName is null,'Unknown',groupName) as itemDesc , " +
        //                  "sum(qty) AS Quantity, " +
        //                  "visual, " +
        //                  "sum(subTotal) AS Total " +
        //                "from " +
        //                "(select " +
        //                  "if( " +
        //                    "itemID<>0, " +
        //                    "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
        //                    "if( " +
        //                      "optionID<>0, " +
        //                      "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
        //                       "'Dyn Item' " +
        //                    ") " +
        //                   ") as groupName, " +
        //                  "sum(qty) as qty, " +
        //                  "visual, " +
        //                  "sum(subTotal) as subTotal " +
        //                "from " +
        //                "(select " +
        //                  "ol.itemID, " +
        //                  "ol.optionID, " +
        //                  "ol.dynID, " +
        //                  "sum(ol.qty) as qty, " +
        //                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
        //                  "sum(ol.subTotal) as subTotal " +
        //                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
        //                "where (day(o.ts)='" + dateTimePickerto.Value.AddDays((-1) * i).Day + "' and month(o.ts)='" + dateTimePickerto.Value.Month + "' and year(o.ts)='" + dateTimePickerto.Value.Year + "' ) AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
        //                "group by ol.itemID,ol.optionID,ol.dynID " +

        //                "union all " +
        //                "select " +
        //                  "ol.itemID, " +
        //                  "ol.optionID, " +
        //                  "ol.dynID, " +
        //                  "sum(ol.qty) as qty, " +
        //                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
        //                  "sum(ol.subTotal) as subTotall " +
        //                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
        //                "where (day(o.ts)='" + dateTimePickerto.Value.AddDays((-1) * i).Day + "' and month(o.ts)='" + dateTimePickerto.Value.Month + "' and year(o.ts)='" + dateTimePickerto.Value.Year + "' ) AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
        //                "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
        //                "group by groupName " +

        //                "union all " +
        //                   "select " +
        //                      "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) limit 0,1) as groupName, " +
        //                      "sum(ol.qty) as qty, " +
        //                      "0 as visual, " +
        //                      "sum(ol.subTotal) as subTotal " +
        //                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
        //                    "where (day(o.ts)='" + dateTimePickerto.Value.AddDays((-1) * i).Day + "' and month(o.ts)='" + dateTimePickerto.Value.Month + "' and year(o.ts)='" + dateTimePickerto.Value.Year + "' ) AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
        //                    "group by groupName " +
        //                "union all " +
        //                    "select " +
        //                      "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
        //                      "sum(ol.qty) as qty, " +
        //                      "0 as visual, " +
        //                      "sum(ol.subTotal) as subTotal " +
        //                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
        //                    "where (day(o.ts)='" + dateTimePickerto.Value.AddDays((-1) * i).Day + "' and month(o.ts)='" + dateTimePickerto.Value.Month + "' and year(o.ts)='" + dateTimePickerto.Value.Year + "' ) AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
        //                "group by groupName) as tmp2 " +
        //                "group by itemDesc " +
        //                "";
        //        dtSource = new DataTable();
        //        dtSource = mConnection.Select(sql);
        //        foreach (DataRow drItem in dtSource.Rows)
        //        {
        //            if (Convert.ToInt32(drItem["visual"]) == 1)
        //            {
        //                totalSaleFuel += Convert.ToDouble(drItem["Total"] == "" ? "0" : drItem["Total"]);
        //            }
        //            else if (Convert.ToInt32(drItem["visual"]) == 0)
        //            {
        //                totalSaleNonFuel += Convert.ToDouble(drItem["Total"] == "" ? "0" : drItem["Total"]);
        //            }
        //        }

        //        double decTotal;
        //        sql = "SELECT CardName,SUM(Subtotal) AS Subtotal,cashOut " +
        //            "FROM " +
        //            "( " +
        //            "SELECT c.cardName AS CardName,SUM(obc.subtotal) AS Subtotal,SUM(cashOut) AS cashOut  " +
        //            "FROM orderbycard obc " +
        //            "inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
        //            "inner join card c on obc.cardID = c.cardID " +
        //            "where date(od.ts)=" + "date('" + dateString + "')" + " and (od.completed=1 or od.completed=2) group by CardName " +
        //            "Union all " +
        //            "SELECT c.cardName,SUM(obc.subtotal) AS Subtotal,SUM(cashOut) AS cashOut " +
        //            "FROM orderbycard obc " +
        //            "inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
        //            "inner join card c on obc.cardID = c.cardID " +
        //            "where date(oa.ts)=" + "date('" + dateString + "')" + " and (oa.completed=1 or oa.completed=2) group by CardName " +
        //            ")AS Source group by CardName";

        //        dtSource = new DataTable();
        //        dtSource = mConnection.Select(sql);
        //        //lstCard.Items.Clear();
        //        decTotal = 0;
        //        foreach (System.Data.DataRow row in dtSource.Rows)
        //        {
        //            UCSaleDay ucSale = new UCSaleDay();
        //            ListViewItem li = new ListViewItem(row["CardName"].ToString());
        //            li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]))));
        //            decTotal += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]);
        //            ucSale.lstCard.Items.Add(li);
        //        }

        //        //if ((Convert.ToDouble(eftpos) - decTotal) != 0)
        //        //{
        //        //    dtSource.Rows.Add("*Bank eftpos cash out", money.Format2(money.getFortMat((Convert.ToDouble(eftpos) - decTotal))));
        //        //}

        //        //lblTotalCard.Text = money.Format(decTotal);

        //        //=======================
        //        //double cashAccount = 0;
        //        //double cardAccount = 0;
        //        //dtSource = mConnection.Select(
        //        //    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where date(PayDate)=date('" + dateString + "') ) as tmp"
        //        //    );
        //        //if (dtSource.Rows.Count > 0)
        //        //{
        //        //    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
        //        //    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
        //        //}
        //        //=====================
        //        //double cashDeposit = 0;
        //        //double cardDeposit = 0;
        //        //dtSource = mConnection.Select("select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(eftpos) as card from depositorders where date(ts)=date('" + dateString + "') ) as tmp");
        //        //if (dtSource.Rows.Count > 0)
        //        //{
        //        //    cashDeposit = Convert.ToDouble(dtSource.Rows[0]["cash"]);
        //        //    cardDeposit = Convert.ToDouble(dtSource.Rows[0]["card"]);
        //        //}
        //        ////===========================
        //        //double sumCash = cash + cashAccount + cashDeposit - refun;
        //        //double sumCard = eftpos + cardAccount + cardDeposit;

        //        Report.Add(
        //        new WeeklyReport()
        //        {
        //            date = dateTimePickerto.Value.AddDays((-1) * i).Day + "/" + dateTimePickerto.Value.AddDays((-1) * i).Month + "/" + dateTimePickerto.Value.AddDays((-1) * i).Year,
        //            day = dateTimePickerto.Value.AddDays((-1) * i).Date.DayOfWeek.ToString(),
        //            totalSales = money.Format(totalSales),
        //            totalDisCount = money.Format(totalDisCount),
        //            //totalGTS = money.Format((totalSales - totalDisCount) / 11),
        //            totalGTS=money.Format(gst),
        //            totalCard = money.Format(eftpos),
        //            totalCash = money.Format(cash),
        //            totalReturnPro = money.Format(returnPro),
        //            Account = money.Format(account),
        //            CreditNote = money.Format(creditNote),
        //            ListCard = dtSource,
        //            ListShift = dtSourceShift,
        //            sumTotalCashIn = money.Format(cashIn),
        //            sumTotalCashOut = money.Format(cashOut),
        //            //sumTotalListCard = money.Format(decTotal),
        //            cardAccount = money.Format(cardAccount),
        //            cashAccount = money.Format(cashAccount),
        //            TotalAccount = money.Format(cardAccount + cashAccount),
        //            TotalSalesFuel = money.Format2(totalSaleFuel),
        //            TotalSalesNonFuel = money.Format2(totalSaleNonFuel)
        //            //cardDeposit = money.Format(cardDeposit),
        //            //cashDeposit = money.Format(cashDeposit),
        //            //totalDeposit = money.Format(cardDeposit + cashDeposit),
        //            //sumTotalCash = money.Format(sumCash),
        //            //sumTotalCard = money.Format(sumCard),
        //            //SumTotal = money.Format(sumCard + sumCash)
        //        }
        //        );
        //    }

        //    string strDate;
        //    double dblTotalSales = 0;
        //    double dblTotalDisCount = 0;
        //    double dblTotalGTS = 0;
        //    double dblTotalCard = 0;
        //    double dblTotalCash = 0;
        //    double dblTotalReturnPro = 0;
        //    double dblTotalCreditNote = 0;
        //    double dblTotalCreditTotal = 0;
        //    double dblSumTotalCash = 0;
        //    double dblSumTotalCard = 0;
        //    double dblTotalSumTotal = 0;
        //    double dblSumTotalCashIn = 0;
        //    double dblSumTotalCashOut = 0;
        //    double dblTotalCardAccounts = 0;
        //    double dblTotalCashAccount = 0;
        //    double dblTotalTotalAccount = 0;
        //    double dblTotalSalesFuel = 0;
        //    double dblTotalSalesNonFuel = 0;

        //    foreach (WeeklyReport w in Report)
        //    {
        //        dblTotalSales += money.getFortMat(w.totalSales);
        //        dblTotalDisCount += money.getFortMat(w.totalDisCount);
        //        dblTotalGTS += money.getFortMat(w.totalGTS);
        //        dblTotalCard += money.getFortMat(w.totalCard);
        //        dblTotalCash += money.getFortMat(w.totalCash);
        //        dblTotalRefuncs += money.getFortMat(w.totalReturnPro);
        //        dblTotalCreditNote += money.getFortMat(w.CreditNote);
        //        dblTotalAccount += money.getFortMat(w.Account);
        //        dblSumTotalCash += money.getFortMat(w.sumTotalCash);
        //        dblSumTotalCard += money.getFortMat(w.sumTotalCard);
        //        dblTotalSumTotal += money.getFortMat(w.SumTotal);
        //        dblTotalCreditTotal += money.getFortMat(w.CreditNote);
        //        dblSumTotalCashIn += money.getFortMat(w.sumTotalCashIn);
        //        dblSumTotalCashOut += money.getFortMat(w.sumTotalCashOut);
        //        dblTotalCardAccounts += money.getFortMat(w.cardAccount);
        //        dblTotalCashAccount += money.getFortMat(w.cashAccount);
        //        dblTotalTotalAccount += money.getFortMat(w.TotalAccount);
        //        dblTotalSalesFuel += money.getFortMat(w.TotalSalesFuel);
        //        dblTotalSalesNonFuel += money.getFortMat(w.TotalSalesNonFuel);
        //    }

        //    string strSQL =
        //           "SELECT CardName,SUM(Subtotal) AS Subtotal " +
        //           "FROM " +
        //           "( " +
        //           "SELECT DISTINCT obc.orderbycardID,obc.orderID,c.cardName AS CardName,obc.subtotal AS Subtotal " +
        //           "FROM orderbycard obc " +
        //           "inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
        //           "inner join card c on obc.cardID = c.cardID " +
        //           "where date(od.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(od.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (od.completed=1 or od.completed=2) " +
        //           "Union all " +
        //           "SELECT DISTINCT obc.orderbycardID,obc.orderID,c.cardName AS CardName,obc.subtotal AS Subtotal " +
        //           "FROM orderbycard obc " +
        //           "inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
        //           "inner join card c on obc.cardID = c.cardID " +
        //           "where date(oa.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(oa.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (oa.completed=1 or oa.completed=2) " +
        //           ")AS Source group by CardName";

        //    dtSource = new DataTable();
        //    dtSource = mConnection.Select(strSQL);
        //    double dblTotal = 0;
        //    foreach (DataRow dr in dtSource.Rows)
        //    {
        //        dblTotal += Convert.ToDouble(dr["Subtotal"]);
        //    }
        //    if ((dblTotalCard - dblTotal) != 0)
        //    {
        //        dtSource.Rows.Add("Unknow Card", (dblTotalCard - dblTotal).ToString());
        //    }

        //    Report.Add(
        //        new WeeklyReport()
        //        {
        //            date = "",
        //            day = dateTimePickerfrom.Value.Day + "/" + dateTimePickerfrom.Value.Month + "/" + dateTimePickerfrom.Value.Year + "->" + dateTimePickerto.Value.Day + "/" + dateTimePickerto.Value.Month + "/" + dateTimePickerto.Value.Year,
        //            totalSales = money.Format(dblTotalSales),
        //            totalDisCount = money.Format(dblTotalDisCount),
        //            totalGTS = money.Format(dblTotalGTS),
        //            totalCard = money.Format(dblTotalCard),
        //            totalCash = money.Format(dblTotalCash),
        //            totalReturnPro = money.Format(dblTotalRefuncs),
        //            Account = money.Format(dblTotalAccount),
        //            CreditNote = money.Format(dblTotalCreditTotal),
        //            cardAccount = money.Format(dblTotalCardAccounts),
        //            cashAccount = money.Format(dblTotalCashAccount),
        //            TotalAccount = money.Format(dblTotalTotalAccount),
        //            totalDeposit = money.Format(dblTotalTotalDeposit),
        //            sumTotalCash = money.Format(dblSumTotalCash),
        //            sumTotalCard = money.Format(dblSumTotalCard),
        //            sumTotalCashIn = money.Format(dblSumTotalCash),
        //            sumTotalCashOut = money.Format(dblSumTotalCard),
        //            SumTotal = money.Format(dblTotalSumTotal),
        //            ListCard = dtSource,
        //            ListShift = dtSourceShift,
        //            TotalSalesFuel = money.Format2(dblTotalSalesFuel),
        //            TotalSalesNonFuel = money.Format2(dblTotalSalesNonFuel)
        //        }
        //        );
        //    if (Settings.Default.AllowUseShift)
        //    {
        //        foreach (WeeklyReport w in Report)
        //        {
        //            UCSaleDay ucSale = new UCSaleDay();
        //            ucSale.lblDate.Text = w.date;
        //            ucSale.lblDay.Text = w.day;
        //            ucSale.labelTotalSale.Text = w.totalSales;
        //            ucSale.labelTotalDiscount.Text = w.totalDisCount;
        //            ucSale.labelTotalGST.Text = w.totalGTS;
        //            ucSale.labelCard.Text = w.totalCard;
        //            ucSale.labelCash.Text = w.totalCash;
        //            ucSale.labelAccount.Text = w.Account;
        //            ucSale.labelRefunc.Text = w.totalReturnPro;
        //            //ucSale.labelCreditNote.Text = w.CreditNote;
        //            ucSale.labelCashIn.Text = w.sumTotalCashIn;
        //            ucSale.labelCashOut.Text = w.sumTotalCashOut;
        //            ucSale.labelCardAccount.Text = w.cardAccount;
        //            ucSale.labelCashAccount.Text = w.cashAccount;
        //            ucSale.labelTotalAccount.Text = w.TotalAccount;
        //            ucSale.lblTotalFuelSales.Text = w.TotalSalesFuel;
        //            ucSale.lblTotalShopSales.Text = w.TotalSalesNonFuel;
        //            foreach (System.Data.DataRow row in w.ListCard.Rows)
        //            {
        //                ListViewItem li = new ListViewItem(row["CardName"].ToString());
        //                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
        //                w.sumTotalListCard += Convert.ToDouble(row["Subtotal"]);
        //                ucSale.lstCard.Items.Add(li);
        //            }
        //            foreach (System.Data.DataRow row in w.ListShift.Rows)
        //            {
        //                ListViewItem li = new ListViewItem(row["shiftID"].ToString());
        //                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["balanceCount"]))));
        //                ucSale.lstReportShift.Items.Add(li);
        //            }
        //            ucSale.lblTotalCard.Text = "Total card amount :" + money.Format(w.sumTotalListCard);
        //            //lblTotalCard.Text = money.Format(decTotal);
        //            //ucSale.labelAccount.Text = w.Account;
        //            //ucSale.labelDeposit.Text = w.Deposit;
        //            //ucSale.labelCardAccount.Text = w.cardAccount;
        //            //ucSale.labelCashAccount.Text = w.cashAccount;
        //            //ucSale.labelTotalAccount.Text = w.TotalAccount;
        //            //ucSale.labelCardDeposit.Text = w.cardDeposit;
        //            //ucSale.labelCashDeposit.Text = w.cashDeposit;
        //            //ucSale.labelTotalDeposit.Text = w.totalDeposit;
        //            //ucSale.labelCashTotal.Text = w.sumTotalCash;
        //            //ucSale.labelCardTotal.Text = w.sumTotalCard;
        //            //ucSale.labelSumTotal.Text = w.SumTotal;
        //            flow_layout_Week.Controls.Add(ucSale);
        //        }
        //    }
        //    else
        //    {
        //        foreach (WeeklyReport w in Report)
        //        {
        //            UCSaleDayNoneShift ucSale = new UCSaleDayNoneShift();
        //            ucSale.lblDate.Text = w.date;
        //            ucSale.lblDay.Text = w.day;
        //            ucSale.labelTotalSale.Text = w.totalSales;
        //            ucSale.labelTotalDiscount.Text = w.totalDisCount;
        //            ucSale.labelTotalGST.Text = w.totalGTS;
        //            ucSale.labelCard.Text = w.totalCard;
        //            ucSale.labelCash.Text = w.totalCash;
        //            ucSale.labelAccount.Text = w.Account;
        //            ucSale.labelRefunc.Text = w.totalReturnPro;
        //            //ucSale.labelCreditNote.Text = w.CreditNote;
        //            ucSale.labelCashIn.Text = w.sumTotalCashIn;
        //            ucSale.labelCashOut.Text = w.sumTotalCashOut;
        //            ucSale.labelCardAccount.Text = w.cardAccount;
        //            ucSale.labelCashAccount.Text = w.cashAccount;
        //            ucSale.labelTotalAccount.Text = w.TotalAccount;
        //            ucSale.lblTotalFuelSales.Text = w.TotalSalesFuel;
        //            ucSale.lblTotalShopSales.Text = w.TotalSalesNonFuel;
        //            foreach (System.Data.DataRow row in w.ListCard.Rows)
        //            {
        //                ListViewItem li = new ListViewItem(row["CardName"].ToString());
        //                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
        //                w.sumTotalListCard += Convert.ToDouble(row["Subtotal"]);
        //                ucSale.lstCard.Items.Add(li);
        //            }
        //            ucSale.lblTotalCard.Text = "Total card amount :" + money.Format(w.sumTotalListCard);
        //            //lblTotalCard.Text = money.Format(decTotal);
        //            //ucSale.labelAccount.Text = w.Account;
        //            //ucSale.labelDeposit.Text = w.Deposit;
        //            //ucSale.labelCardAccount.Text = w.cardAccount;
        //            //ucSale.labelCashAccount.Text = w.cashAccount;
        //            //ucSale.labelTotalAccount.Text = w.TotalAccount;
        //            //ucSale.labelCardDeposit.Text = w.cardDeposit;
        //            //ucSale.labelCashDeposit.Text = w.cashDeposit;
        //            //ucSale.labelTotalDeposit.Text = w.totalDeposit;
        //            //ucSale.labelCashTotal.Text = w.sumTotalCash;
        //            //ucSale.labelCardTotal.Text = w.sumTotalCard;
        //            //ucSale.labelSumTotal.Text = w.SumTotal;
        //            flow_layout_Week.Controls.Add(ucSale);
        //        }
        //    }
        //}
        #endregion
        private void LoadWeeklyButton()
        {
            try
            {
                //mConnection.Open();
               
                {
                    dailyReport = new WeeklyReport();
                    Report = new List<WeeklyReport>();
                    for (int i = 0; i < 7; i++)
                    {
                        try
                        {
                            mConnection.Open();
                            string dateString = GetDateString(dateTimePickerto.Value.AddDays((-1) * i));
                            string sql = "select " +
                                            "sum(subTotal) as totalSales," +
                                            "sum(discount) as totalDisCount," +
                                            "sum(cash) as cash," +
                                            "sum(eftpos-`change`) as eftpos," +
                                            "sum(returnPro) as returnPro," +
                                            "sum(creditNote) as creditNote, " +
                                            "sum(gst) as gst, " +
                                            "sum(account) as account, " +
                                            "sum(Surchart) as Surchart, " +
                                            "sum(`change`) as `change` " +
                                         "from (" +
                                                "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change` " +
                                                    "from ordersdaily " +
                                                    "where date(ts)=" + "date('" + dateString + "')" + " and (completed=1 or completed=2) " +
                                                "union all " +
                                                "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change` " +
                                                    "from ordersall " +
                                                    "where date(ts)=" + "date('" + dateString + "')" + " and (completed=1 or completed=2) " +
                                               ") as total";
                            dtSource = new DataTable();
                            dtSource = mConnection.Select(sql);
                            double totalSales = 0;
                            double totalDisCount = 0;
                            double cash = 0;
                            double eftpos = 0;
                            double returnPro = 0;
                            double account = 0;
                            double creditNote = 0;
                            double cashIn = 0;
                            double cashOut = 0;
                            double PayOut = 0;
                            double cardAccount = 0;
                            double cashAccount = 0;
                            double totalSaleNonFuel = 0;
                            double totalSaleFuel = 0;
                            double gst = 0;
                            double change = 0;
                            double surcharge = 0;
                            try
                            {
                                totalSales = Convert.ToDouble(dtSource.Rows[0]["totalSales"]);
                                totalDisCount = Convert.ToDouble(dtSource.Rows[0]["totalDisCount"]);
                                cash = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                                eftpos = Convert.ToDouble(dtSource.Rows[0]["eftpos"]);
                                returnPro = Convert.ToDouble(dtSource.Rows[0]["returnPro"]);
                                creditNote = Convert.ToDouble(dtSource.Rows[0]["creditNote"]);
                                account = Convert.ToDouble(dtSource.Rows[0]["account"]);
                                gst = Convert.ToDouble(dtSource.Rows[0]["gst"]);
                                change = Convert.ToDouble(dtSource.Rows[0]["change"]);
                                surcharge = Convert.ToDouble(dtSource.Rows[0]["Surchart"]);
                                double dis = Convert.ToDouble(mConnection.ExecuteScalar(
                                    "select if(sum(subTotal) is null,0,sum(subTotal)) as subTotal from " +
                                    "(" +
                                        "select sum(ol.subTotal) as subTotal from ordersdailyline ol inner join ordersdaily o on o.orderID=ol.orderID where  date(ts) =" + "date('" + dateString + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                                        "union all " +
                                        "select sum(ol.subTotal) as subTotal from ordersallline ol inner join ordersall o on o.bkId=ol.bkId where  date(o.ts) =" + "date('" + dateString + "')" + " and (o.completed=1 or o.completed=2) and ol.subTotal<0 " +
                                    ") as tmp "
                                    ));
                                totalDisCount -= dis;
                                totalSales -= dis;
                            }
                            catch (Exception)
                            {
                            }

                            try
                            {
                                dtSource = mConnection.Select(
                                    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where date(PayDate)=date('" + dateString + "') AND PayAmount<>0 ) as tmp"
                                    );
                                if (dtSource.Rows.Count > 0)
                                {
                                    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                                    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
                                }
                            }
                            catch (Exception)
                            {
                            }

                            try
                            {
                                sql = "SELECT c.Name,od.subtotal,od.completed " +
                                    "FROM ordersdaily AS od INNER JOIN customers AS c on od.CustCode = c.memberNo " +
                                    "where od.IsDelivery = 1 AND od.IsPickup = 0 AND date(od.ts)=" + "date('" + dateString + "')";
                                dtSourceDelivery = new DataTable();
                                dtSourceDelivery = mConnection.Select(sql);
                            }
                            catch (Exception)
                            {
                            }

                            try
                            {
                                sql = "SELECT c.Name,od.subtotal,od.completed " +
                                    "FROM ordersdaily AS od INNER JOIN customers AS c on od.CustCode = c.memberNo " +
                                    "where od.IsDelivery = 0 AND od.IsPickup = 1 AND date(od.ts)=" + "date('" + dateString + "')";
                                dtSourcePickup = new DataTable();
                                dtSourcePickup = mConnection.Select(sql);
                            }
                            catch (Exception)
                            {
                            }

                            try
                            {
                                #region
                                //sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                                //    "FROM " +
                                //    "( " +
                                //    "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND date(dateCash)=" + "date('" + dateString + "') " +
                                //    "UNION ALL " +
                                //    "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where CashType = 2 AND date(dateCash)=" + "date('" + dateString + "') " +
                                //    ")AS Source";
                                //dtSource = new DataTable();
                                //dtSource = mConnection.Select(sql);
                                #endregion
                                sql = "select SUM(CashIn) AS CashIn , SUM(CashOut) AS CashOut,SUM(PayOut) AS PayOut " +
                                    "FROM " +
                                    "( " +
                                    "SELECT TotalAmount AS CashIn,0 AS CashOut,0 AS PayOut FROM cashinandcashout c where CashType = 1 AND date(dateCash)=" + "date('" + dateString + "') " +
                                    "UNION ALL " +
                                    "SELECT 0 AS CashIn,TotalAmount AS CashOut,0 AS PayOut FROM cashinandcashout c where (CashType = 2 OR CashType = 3 OR CashType = 4 OR CashType = 5 OR CashType = 6) AND date(dateCash)=" + "date('" + dateString + "') " +
                                    "UNION ALL " +
                                    "SELECT 0 AS CashIn,0 AS CashOut,TotalAmount AS PayOut FROM cashinandcashout c where CashType = 6 AND date(dateCash)=" + "date('" + dateString + "') " +
                                    ")AS Source";
                                dtSource = new DataTable();
                                dtSource = mConnection.Select(sql);

                                foreach (System.Data.DataRow row in dtSource.Rows)
                                {
                                    cashIn = Convert.ToDouble(row["CashIn"] == "" ? "0" : row["CashIn"]);
                                    cashOut = Convert.ToDouble(row["CashOut"] == "" ? "0" : row["CashOut"]);
                                    PayOut = Convert.ToDouble(row["PayOut"] == "" ? "0" : row["PayOut"]);
                                }
                            }
                            catch (Exception)
                            {
                            }

                            dtSourceShift = new DataTable();
                            dtSourceShift.Columns.Add("shiftID", typeof(String));
                            dtSourceShift.Columns.Add("balanceCount", typeof(Double));
                            dtSource = mConnection.Select("SELECT shiftID FROM shifts s where date(ts)=date('" + dateString + "')");
                            foreach (DataRow dr in dtSource.Rows)
                            {
                                try
                                {
                                    LoadTabDaily(dr["shiftID"].ToString());
                                }
                                catch (Exception)
                                {
                                    LoadTabDaily("1");
                                }
                            }

                            sql = "select " +
                                      "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                                      "sum(qty) AS Quantity, " +
                                      "visual, " +
                                      "sum(subTotal) AS Total " +
                                    "from " +
                                    "(select " +
                                      "if( " +
                                        "itemID<>0, " +
                                        "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                        "if( " +
                                          "optionID<>0, " +
                                          "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                           "'Dyn Item' " +
                                        ") " +
                                       ") as groupName, " +
                                      "sum(qty) as qty, " +
                                      "visual, " +
                                      "sum(subTotal) as subTotal " +
                                    "from " +
                                    "(select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "sum(ol.qty) as qty, " +
                                      "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)=" + "date('" + dateString + "')" + " AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID " +

                                    "union all " +
                                    "select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "sum(ol.qty) as qty, " +
                                      "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                      "sum(ol.subTotal) as subTotall " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)=" + "date('" + dateString + "')" + " AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                                    "group by groupName " +

                                    "union all " +
                                       "select " +
                                          "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) limit 0,1) as groupName, " +
                                          "sum(ol.qty) as qty, " +
                                          "0 as visual, " +
                                          "sum(ol.subTotal) as subTotal " +
                                        "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                        "where date(o.ts)=" + "date('" + dateString + "')" + " AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                        "group by groupName " +
                                    "union all " +
                                        "select " +
                                          "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                          "sum(ol.qty) as qty, " +
                                          "0 as visual, " +
                                          "sum(ol.subTotal) as subTotal " +
                                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                        "where date(o.ts)=" + "date('" + dateString + "')" + " AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                    "group by groupName) as tmp2 " +
                                    "group by itemDesc " +
                                    "";

                            dtSource = new DataTable();
                            dtSource = mConnection.Select(sql);
                            foreach (DataRow drItem in dtSource.Rows)
                            {
                                if (Convert.ToInt32(drItem["visual"]) == 1)
                                {
                                    totalSaleFuel += Convert.ToDouble(drItem["Total"] == "" ? "0" : drItem["Total"]);
                                }
                                else if (Convert.ToInt32(drItem["visual"]) == 0)
                                {
                                    totalSaleNonFuel += Convert.ToDouble(drItem["Total"] == "" ? "0" : drItem["Total"]);
                                }
                            }

                            double decTotal, decTotalSurcharge;
                            #region
                            //sql = "SELECT CardName,SUM(Subtotal) AS Subtotal,cashOut " +
                            //    "FROM " +
                            //    "( " +
                            //    "SELECT c.cardName AS CardName,SUM(obc.subtotal) AS Subtotal,SUM(cashOut) AS cashOut  " +
                            //    "FROM orderbycard obc " +
                            //    "inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
                            //    "inner join card c on obc.cardID = c.cardID " +
                            //    "where date(od.ts)=" + "date('" + dateString + "')" + " and (od.completed=1 or od.completed=2) group by CardName " +
                            //    "Union all " +
                            //    "SELECT c.cardName,SUM(obc.subtotal) AS Subtotal,SUM(cashOut) AS cashOut " +
                            //    "FROM orderbycard obc " +
                            //    "inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
                            //    "inner join card c on obc.cardID = c.cardID " +
                            //    "where date(oa.ts)=" + "date('" + dateString + "')" + " and (oa.completed=1 or oa.completed=2) group by CardName " +
                            //    ")AS Source group by CardName";
                            #endregion
                            sql = "SELECT C.cardName,SUM(obc.Subtotal) AS Subtotal,SUM(obc.cashOut) AS Cashout,SUM(obc.surcharge) AS Surcharge " +
                            "FROM " +
                            "( " +
                                "SELECT DISTINCT od.orderbycardID " +
                                "FROM ordersdaily od " +
                                "where date(od.ts)=" + "date('" + dateString + "') and (od.completed=1 or od.completed=2) " +
                                "Union all " +
                                "SELECT DISTINCT oa.orderbycardID " +
                                "FROM ordersall oa " +
                                "where date(oa.ts)=" + "date('" + dateString + "') and (oa.completed=1 or oa.completed=2) " +
                                "Union all " +
                                "SELECT DISTINCT orderbycardID as orderbycardID " +
                                "FROM accountpayment " +
                                "where orderbycardID <>'' AND date(PayDate) =" + "date('" + dateString + "')" +
                            ")AS Source " +
                            "  inner join orderbycard obc on Source.orderbycardID=obc.orderbycardID " +
                            "  inner join card C on obc.cardID = C.cardID " +
                            "group by C.cardName ";

                            dtSource = new DataTable();
                            dtSource = mConnection.Select(sql);
                            //lstCard.Items.Clear();
                            decTotal = 0;
                            decTotalSurcharge = 0;
                            foreach (System.Data.DataRow row in dtSource.Rows)
                            {
                                UCSaleDay ucSale = new UCSaleDay();
                                ListViewItem li = new ListViewItem(row["CardName"].ToString());
                                li.SubItems.Add(money.Format2(Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]) - Convert.ToDouble(row["Surcharge"])));
                                li.SubItems.Add(money.Format2(Convert.ToDouble(row["Surcharge"])));
                                //decTotal += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]);
                                decTotal += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]) - Convert.ToDouble(row["Surcharge"]);
                                decTotalSurcharge += Convert.ToDouble(row["Surcharge"]);
                                ucSale.lstCard.Items.Add(li);
                            }
                            #region
                            //if ((Convert.ToDouble(eftpos) - decTotal) != 0)
                            //{
                            //    dtSource.Rows.Add("*Bank eftpos cash out", money.Format2(money.getFortMat((Convert.ToDouble(eftpos) - decTotal))));
                            //}

                            //lblTotalCard.Text = money.Format(decTotal);

                            //=======================
                            //double cashAccount = 0;
                            //double cardAccount = 0;
                            //dtSource = mConnection.Select(
                            //    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where date(PayDate)=date('" + dateString + "') ) as tmp"
                            //    );
                            //if (dtSource.Rows.Count > 0)
                            //{
                            //    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                            //    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
                            //}
                            //=====================
                            //double cashDeposit = 0;
                            //double cardDeposit = 0;
                            //dtSource = mConnection.Select("select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(eftpos) as card from depositorders where date(ts)=date('" + dateString + "') ) as tmp");
                            //if (dtSource.Rows.Count > 0)
                            //{
                            //    cashDeposit = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                            //    cardDeposit = Convert.ToDouble(dtSource.Rows[0]["card"]);
                            //}
                            ////===========================
                            //double sumCash = cash + cashAccount + cashDeposit - refun;
                            //double sumCard = eftpos + cardAccount + cardDeposit;
                            #endregion
                            Report.Add(
                            new WeeklyReport()
                            {
                                date = dateTimePickerto.Value.AddDays((-1) * i).Day + "/" + dateTimePickerto.Value.AddDays((-1) * i).Month + "/" + dateTimePickerto.Value.AddDays((-1) * i).Year,
                                day = dateTimePickerto.Value.AddDays((-1) * i).Date.DayOfWeek.ToString(),
                                totalSales = money.Format(totalSales),
                                totalDisCount = money.Format(totalDisCount),
                                //totalGTS = money.Format((totalSales - totalDisCount) / 11),
                                totalGTS = money.Format(gst),
                                totalCard = money.Format(decTotal),
                                totalCash = money.Format(cash),
                                totalReturnPro = money.Format(returnPro),
                                Account = money.Format(account),
                                CreditNote = money.Format(creditNote),
                                ListCard = dtSource,
                                ListShift = dtSourceShift,
                                sumTotalCashIn = money.Format(cashIn),
                                sumTotalCashOut = money.Format(cashOut),
                                //sumTotalListCard = money.Format(decTotal),
                                cardAccount = money.Format(cardAccount),
                                cashAccount = money.Format(cashAccount),
                                TotalAccount = money.Format(cardAccount + cashAccount),
                                TotalSalesFuel = money.Format2(totalSaleFuel),
                                Surcharge = money.Format2(surcharge),
                                TotalSalesNonFuel = money.Format2(totalSaleNonFuel),
                                PayOut = money.Format(PayOut),
                                SubtotalSales = money.Format(totalSales - returnPro - totalDisCount)
                                #region
                                //cardDeposit = money.Format(cardDeposit),
                                //cashDeposit = money.Format(cashDeposit),
                                //totalDeposit = money.Format(cardDeposit + cashDeposit),
                                //sumTotalCash = money.Format(sumCash),
                                //sumTotalCard = money.Format(sumCard),
                                //SumTotal = money.Format(sumCard + sumCash)
                                #endregion
                            }
                            );
                        }
                        catch (Exception ex) { }
                        finally { mConnection.Close(); }
                    }
                    mConnection.Open();

                    dblTotalSales = 0;
                    dblTotalDisCount = 0;
                    dblTotalGTS = 0;
                    dblTotalCard = 0;
                    dblTotalCash = 0;
                    dblTotalRefuncs = 0;
                    dblTotalAccount = 0;
                    dblSumTotalCash = 0;
                    dblSumTotalCard = 0;
                    dblTotalSumTotal = 0;
                    dblTotalCardAccounts = 0;
                    dblTotalCashAccount = 0;
                    dblTotalTotalAccount = 0;
                    dblTotalCreditNote = 0;
                    dblTotalCreditTotal = 0;
                    dblSumTotalCashIn = 0;
                    dblSumTotalCashOut = 0;
                    dblTotalSalesFuel = 0;
                    dblTotalSalesNonFuel = 0;
                    dbTotalCashIn = 0;
                    dbTotalCashOut = 0;
                    dblTotalSurcharge = 0;
                    dblTotalPayOut = 0;
                    dblSubTotalSale=0;

                    foreach (WeeklyReport w in Report)
                    {
                        dbTotalCashIn += money.getFortMat(w.sumTotalCashIn);
                        dbTotalCashOut += money.getFortMat(w.sumTotalCashOut);
                        dblTotalSales += money.getFortMat(w.totalSales);
                        dblTotalDisCount += money.getFortMat(w.totalDisCount);
                        dblTotalGTS += money.getFortMat(w.totalGTS);
                        dblTotalCard += money.getFortMat(w.totalCard);
                        dblTotalCash += money.getFortMat(w.totalCash);
                        dblTotalRefuncs += money.getFortMat(w.totalReturnPro);
                        dblTotalAccount += money.getFortMat(w.Account);
                        dblSumTotalCash += money.getFortMat(w.sumTotalCash);
                        dblSumTotalCard += money.getFortMat(w.sumTotalCard);
                        dblTotalSumTotal += money.getFortMat(w.SumTotal);
                        dblTotalCardAccounts += money.getFortMat(w.cardAccount);
                        dblTotalCashAccount += money.getFortMat(w.cashAccount);
                        dblTotalTotalAccount += money.getFortMat(w.TotalAccount);
                        dblTotalCreditNote += money.getFortMat(w.CreditNote);
                        dblTotalCreditTotal += money.getFortMat(w.CreditNote);
                        dblSumTotalCashIn += money.getFortMat(w.sumTotalCashIn);
                        dblSumTotalCashOut += money.getFortMat(w.sumTotalCashOut);
                        dblTotalSalesFuel += money.getFortMat(w.TotalSalesFuel);
                        dblTotalSalesNonFuel += money.getFortMat(w.TotalSalesNonFuel);
                        dblTotalSurcharge += money.getFortMat(w.Surcharge);
                        dblTotalPayOut += money.getFortMat(w.PayOut);
                        dblSubTotalSale+= money.getFortMat(w.SubtotalSales);
                    }
                    #region
                    //string strSQL =
                    //"SELECT CardName,SUM(Subtotal) AS Subtotal " +
                    //"FROM " +
                    //"( " +
                    //"SELECT DISTINCT obc.orderbycardID,obc.orderID,c.cardName AS CardName,obc.subtotal AS Subtotal " +
                    //"FROM orderbycard obc " +
                    //"inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
                    //"inner join card c on obc.cardID = c.cardID " +
                    //"where date(od.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(od.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (od.completed=1 or od.completed=2) " +
                    //"Union all " +
                    //"SELECT DISTINCT obc.orderbycardID,obc.orderID,c.cardName AS CardName,obc.subtotal AS Subtotal " +
                    //"FROM orderbycard obc " +
                    //"inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
                    //"inner join card c on obc.cardID = c.cardID " +
                    //"where date(oa.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(oa.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (oa.completed=1 or oa.completed=2) " +
                    //")AS Source group by CardName";
                    #endregion
                    string strSQL = "SELECT C.cardName,SUM(obc.Subtotal) AS Subtotal,SUM(obc.cashOut) AS Cashout,SUM(obc.surcharge) AS Surcharge " +
                         "FROM " +
                         "( " +
                             "SELECT DISTINCT od.orderbycardID " +
                             "FROM ordersdaily od " +
                             "where date(od.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(od.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (od.completed=1 or od.completed=2) " +
                             "Union all " +
                             "SELECT DISTINCT oa.orderbycardID " +
                             "FROM ordersall oa " +
                             "where date(oa.ts)>=" + "date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(oa.ts)<=" + "date('" + GetDateString(dateTimePickerto.Value) + "') and (oa.completed=1 or oa.completed=2) " +
                         ")AS Source " +
                         "  inner join orderbycard obc on Source.orderbycardID=obc.orderbycardID " +
                         "  inner join card C on obc.cardID = C.cardID " +
                         "group by C.cardName ";

                    dtSource = new DataTable();
                    dtSource = mConnection.Select(strSQL);
                    double dblTotal = 0;
                    foreach (DataRow dr in dtSource.Rows)
                    {
                        dblTotal += Convert.ToDouble(dr["Subtotal"]);
                    }
                    //if ((dblTotalCard - dblTotal) != 0)
                    //{
                    //    dtSource.Rows.Add("Unknow Card", (dblTotalCard - dblTotal).ToString());
                    //}

                    strSQL = "select " +
                                  "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                                  "sum(qty) AS Quantity, " +
                                  "visual, " +
                                  "sum(subTotal) AS Total " +
                                "from " +
                                "(select " +
                                  "if( " +
                                    "itemID<>0, " +
                                    "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                                    "if( " +
                                      "optionID<>0, " +
                                      "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                                       "'Dyn Item' " +
                                    ") " +
                                   ") as groupName, " +
                                  "sum(qty) as qty, " +
                                  "visual, " +
                                  "sum(subTotal) as subTotal " +
                                "from " +
                                "(select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotal " +
                                "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID " +
                                "union all " +
                                "select " +
                                  "ol.itemID, " +
                                  "ol.optionID, " +
                                  "ol.dynID, " +
                                  "sum(ol.qty) as qty, " +
                                  "if(ol.itemID<>0,(select `visual` from itemsmenu where itemID=ol.itemID),0) as visual, " +
                                  "sum(ol.subTotal) as subTotall " +
                                "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                                "group by groupName " +

                                "union all " +
                                   "select " +
                                      "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                    "group by groupName " +
                                "union all " +
                                    "select " +
                                      "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                                      "sum(ol.qty) as qty, " +
                                      "0 as visual, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                "group by groupName) as tmp2 " +
                                "group by itemDesc " +
                                "";

                    dtListFuelSales = new DataTable();
                    dtListFuelSales = mConnection.Select(strSQL);
                    dtListFuelSales.Select("visual = 1");

                    strSQL =
                                    "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                                    "from " +
                                    "( " +
                                    "select " +
                                      "if(groupID is null,-1,groupID) as groupID, " +
                                      "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                                      "subTotal as Total,qty as Quantity " +
                                    "from " +
                                    "(select " +
                                      "if( " +
                                        "itemID<>0, " +
                                        "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                                        "if( " +
                                          "optionID<>0, " +
                                          "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                                        ") " +
                                       ") as groupID, " +
                                      "sum(qty) as qty, " +
                                      "sum(subTotal) as subTotal " +
                                    "from " +
                                    "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                                    "from " +
                                    "(select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "sum(ol.qty) as qty, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID " +
                                    "union all " +
                                    "select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "sum(ol.qty) as qty, " +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                                    "group by itemID,optionID,dynID) as tmp1 " +
                                    "group by groupID) as tmp2 " +
                        //"group by groupName " +
                                    "union all " +
                                       "select " +
                                          "-1 as groupID, " +
                                          "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                          "sum(ol.subTotal) as Total, " +
                                          "sum(ol.qty) as Quantity " +
                                        "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                        "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                        "group by groupDesc " +
                                    "union all " +
                                        "select " +
                                          "-1 as groupID, " +
                                          "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                          "sum(ol.subTotal) as Total, " +
                                          "sum(ol.qty) as Quantity " +
                                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                        "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                        "group by groupDesc " +
                                    ") as tmp3 " +
                                    "group by groupID,groupDesc " +
                                    "";
                    ;

                    if (Convert.ToInt16(dbconfig.AllowSalesFastFood) == 1)
                    {
                        strSQL =
                                    "select -1 as groupID,groupDesc,sum(Total) as Total,sum(Quantity) as Quantity " +
                                    "from " +
                                    "( " +
                                    "select " +
                                      "if(groupID is null,-1,groupID) as groupID, " +
                                      "if(groupID is null,'Unknown',if(groupID=0,'Dyn Item',(select groupDesc from groupsmenu g where g.groupID=tmp2.groupID))) as groupDesc, " +
                                      "subTotal as Total,qty as Quantity " +
                                    "from " +
                                    "(select " +
                                      "if( " +
                                        "itemID<>0, " +
                                        "(select groupID from itemsmenu i where i.itemID=tmp1.itemID), " +
                                        "if( " +
                                          "optionID<>0, " +
                                          "(select i.groupID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID),0 " +
                                        ") " +
                                       ") as groupID, " +
                                      "sum(qty) as qty, " +
                                      "sum(subTotal) as subTotal " +
                                    "from " +
                                    "(select itemID,optionID,dynID,sum(qty) as qty,sum(subTotal) as subTotal " +
                                    "from " +
                                    "(select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID " +
                                    "union all " +
                                    "select " +
                                      "ol.itemID, " +
                                      "ol.optionID, " +
                                      "ol.dynID, " +
                                      "if(ol.itemID<>0 and ol.parentLine<>0,0,if(ol.optionID<>0,sum(ol.qty),sum(ol.qty))) as qty," +
                                      "sum(ol.subTotal) as subTotal " +
                                    "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                    "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID=0 " +
                                    "group by ol.itemID,ol.optionID,ol.dynID) as tmp " +
                                    "group by itemID,optionID,dynID) as tmp1 " +
                                    "group by groupID) as tmp2 " +
                            //"group by groupName " +
                                    "union all " +
                                       "select " +
                                          "-1 as groupID, " +
                                          "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupDesc, " +
                                          "sum(ol.subTotal) as Total, " +
                                          "sum(ol.qty) as Quantity " +
                                        "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                                        "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                        "group by groupDesc " +
                                    "union all " +
                                        "select " +
                                          "-1 as groupID, " +
                                          "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupDesc, " +
                                          "sum(ol.subTotal) as Total, " +
                                          "sum(ol.qty) as Quantity " +
                                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                                        "where date(o.ts)>=date('" + GetDateString(dateTimePickerfrom.Value) + "') AND date(o.ts)<=date('" + GetDateString(dateTimePickerto.Value) + "') AND (o.completed = 1 OR o.completed = 2 ) and ol.subTotal>=0 and ol.dynID<>0 " +
                                        "group by groupDesc " +
                                    ") as tmp3 " +
                                    "group by groupID,groupDesc " +
                                    "";
                    }

                    dtListGroupSales = new DataTable();
                    dtListGroupSales = mConnection.Select(strSQL);
                    dtListGroupSales.Select("groupDesc <> 'Fuel'");

                    Report.Add(
                        new WeeklyReport()
                        {
                            date = "",
                            day = dateTimePickerfrom.Value.Day + "/" + dateTimePickerfrom.Value.Month + "/" + dateTimePickerfrom.Value.Year + "->" + dateTimePickerto.Value.Day + "/" + dateTimePickerto.Value.Month + "/" + dateTimePickerto.Value.Year,
                            totalSales = money.Format(dblTotalSales),
                            totalDisCount = money.Format(dblTotalDisCount),
                            totalGTS = money.Format(dblTotalGTS),
                            totalCard = money.Format(dblTotalCard),
                            totalCash = money.Format(dblTotalCash),
                            totalReturnPro = money.Format(dblTotalRefuncs),
                            Account = money.Format(dblTotalAccount),
                            CreditNote = money.Format(dblTotalCreditTotal),
                            cardAccount = money.Format(dblTotalCardAccounts),
                            cashAccount = money.Format(dblTotalCashAccount),
                            TotalAccount = money.Format(dblTotalTotalAccount),
                            totalDeposit = money.Format(dblTotalTotalDeposit),
                            sumTotalCash = money.Format(dblSumTotalCash),
                            sumTotalCard = money.Format(dblSumTotalCard),
                            sumTotalCashIn = money.Format(dbTotalCashIn),
                            sumTotalCashOut = money.Format(dbTotalCashOut),
                            SumTotal = money.Format(dblTotalSumTotal),
                            ListFuelSales = dtListFuelSales,
                            ListGroupSales = dtListGroupSales,
                            ListCard = dtSource,
                            ListShift = dtSourceShift,
                            TotalSalesFuel = money.Format2(dblTotalSalesFuel),
                            TotalSalesNonFuel = money.Format2(dblTotalSalesNonFuel),
                            PayOut = money.Format(dblTotalPayOut),
                            Surcharge = money.Format2(dblTotalSurcharge),
                            SubtotalSales=money.Format(dblSubTotalSale)
                        });
                    if (Settings.Default.AllowUseShift)
                    {
                        foreach (WeeklyReport w in Report)
                        {
                            UCSaleDay ucSale = new UCSaleDay();
                            ucSale.lblDate.Text = w.date;
                            ucSale.lblDay.Text = w.day;
                            ucSale.labelTotalSale.Text = w.totalSales;
                            ucSale.labelTotalDiscount.Text = w.totalDisCount;
                            ucSale.labelTotalGST.Text = w.totalGTS;
                            ucSale.labelCard.Text = w.totalCard;
                            ucSale.labelCash.Text = w.totalCash;
                            ucSale.labelAccount.Text = w.Account;
                            ucSale.labelRefunc.Text = w.totalReturnPro;
                            //ucSale.labelCreditNote.Text = w.CreditNote;
                            ucSale.labelCashIn.Text = w.sumTotalCashIn;
                            ucSale.labelCashOut.Text = w.sumTotalCashOut;
                            ucSale.labelCardAccount.Text = w.cardAccount;
                            ucSale.labelCashAccount.Text = w.cashAccount;
                            ucSale.labelTotalAccount.Text = w.TotalAccount;
                            ucSale.lblTotalFuelSales.Text = w.TotalSalesFuel;
                            ucSale.lblTotalShopSales.Text = w.TotalSalesNonFuel;
                            ucSale.labelSurcharge.Text = w.Surcharge;
                            ucSale.labelPayOut.Text = w.PayOut;
                            foreach (System.Data.DataRow row in w.ListCard.Rows)
                            {
                                ListViewItem li = new ListViewItem(row["CardName"].ToString());
                                li.SubItems.Add(money.Format(Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]) - Convert.ToDouble(row["Surcharge"])));
                                li.SubItems.Add(money.Format(Convert.ToDouble(row["Surcharge"])));
                                w.sumTotalListCard += Convert.ToDouble(row["Subtotal"]) - Convert.ToDouble(row["cashOut"]) - Convert.ToDouble(row["Surcharge"]);
                                w.sumTotalListCardSurcharge += Convert.ToDouble(row["Surcharge"]);
                                ucSale.lstCard.Items.Add(li);
                            }
                            foreach (System.Data.DataRow row in w.ListShift.Rows)
                            {
                                ListViewItem li = new ListViewItem(row["shiftID"].ToString());
                                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["balanceCount"]))));
                                ucSale.lstReportShift.Items.Add(li);
                            }
                            ucSale.lblTotalCard.Text = "Total card :" + money.Format(w.sumTotalListCard);
                            ucSale.lblTotalCardSurcharge.Text = "Total surcharge :" + money.Format(w.sumTotalListCardSurcharge);
                            #region
                            //lblTotalCard.Text = money.Format(decTotal);
                            //ucSale.labelAccount.Text = w.Account;
                            //ucSale.labelDeposit.Text = w.Deposit;
                            //ucSale.labelCardAccount.Text = w.cardAccount;
                            //ucSale.labelCashAccount.Text = w.cashAccount;
                            //ucSale.labelTotalAccount.Text = w.TotalAccount;
                            //ucSale.labelCardDeposit.Text = w.cardDeposit;
                            //ucSale.labelCashDeposit.Text = w.cashDeposit;
                            //ucSale.labelTotalDeposit.Text = w.totalDeposit;
                            //ucSale.labelCashTotal.Text = w.sumTotalCash;
                            //ucSale.labelCardTotal.Text = w.sumTotalCard;
                            //ucSale.labelSumTotal.Text = w.SumTotal;
                            #endregion
                            flow_layout_Week.Controls.Add(ucSale);
                        }
                    }
                    else
                    {
                        foreach (WeeklyReport w in Report)
                        {
                            UCSaleDayNoneShift ucSale = new UCSaleDayNoneShift();
                            ucSale.lblDate.Text = w.date;
                            ucSale.lblDay.Text = w.day;
                            ucSale.labelTotalSale.Text = w.totalSales;
                            ucSale.labelTotalDiscount.Text = w.totalDisCount;
                            ucSale.labelTotalGST.Text = w.totalGTS;
                            ucSale.labelCard.Text = w.totalCard;
                            ucSale.labelCash.Text = w.totalCash;
                            ucSale.labelAccount.Text = w.Account;
                            ucSale.labelRefunc.Text = w.totalReturnPro;
                            //ucSale.labelCreditNote.Text = w.CreditNote;
                            ucSale.labelCashIn.Text = w.sumTotalCashIn;
                            ucSale.labelCashOut.Text = w.sumTotalCashOut;
                            ucSale.labelCardAccount.Text = w.cardAccount;
                            ucSale.labelCashAccount.Text = w.cashAccount;
                            ucSale.labelTotalAccount.Text = w.TotalAccount;
                            ucSale.lblTotalFuelSales.Text = w.TotalSalesFuel;
                            ucSale.lblTotalShopSales.Text = w.TotalSalesNonFuel;
                            foreach (System.Data.DataRow row in w.ListCard.Rows)
                            {
                                ListViewItem li = new ListViewItem(row["CardName"].ToString());
                                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
                                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Surcharge"]))));
                                w.sumTotalListCard += Convert.ToDouble(row["Subtotal"]);
                                w.sumTotalListCardSurcharge += Convert.ToDouble(row["Subtotal"]);
                                ucSale.lstCard.Items.Add(li);
                            }
                            ucSale.lblTotalCard.Text = "Total card :" + money.Format(w.sumTotalListCard);
                            #region
                            //ucSale.lblTotalCardSurcharge.Text = "Total surcharge :" + money.Format(w.sumTotalListCardSurcharge);
                            //lblTotalCard.Text = money.Format(decTotal);
                            //ucSale.labelAccount.Text = w.Account;
                            //ucSale.labelDeposit.Text = w.Deposit;
                            //ucSale.labelCardAccount.Text = w.cardAccount;
                            //ucSale.labelCashAccount.Text = w.cashAccount;
                            //ucSale.labelTotalAccount.Text = w.TotalAccount;
                            //ucSale.labelCardDeposit.Text = w.cardDeposit;
                            //ucSale.labelCashDeposit.Text = w.cashDeposit;
                            //ucSale.labelTotalDeposit.Text = w.totalDeposit;
                            //ucSale.labelCashTotal.Text = w.sumTotalCash;
                            //ucSale.labelCardTotal.Text = w.sumTotalCard;
                            //ucSale.labelSumTotal.Text = w.SumTotal;
                            #endregion
                            flow_layout_Week.Controls.Add(ucSale);
                        }
                    }
                    mConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportWeekly:::LoadWeeklyButton:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Active || License.BOLicense.CheckLicense() == License.LicenseDialog.Trial)
            {
                try
                {
                    i = 7;
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                    //mPrinter.SetPrinterName(dbconfig.BarPrinter);
                    mPrinter.Print();
                }
                catch (Exception)
                {
                }
            }
            else
            {
                if(mReadConfig.LanguageCode.Equals("vi"))
                {
                    string message1 = "Giấy phép của bạn đã hết hạn. Liên hệ Becas để được cấp mới cho bạn ngay hôm nay!!!";
                    frmMessageBoxOK frm1 = new frmMessageBoxOK("CẢNH BÁO", message1);
                    frm1.ShowDialog();
                    return;
                }
                string message = "Your license is expired. Contact Becas to renew your today!!!";
                frmMessageBoxOK frm = new frmMessageBoxOK("WARNING", message);
                frm.ShowDialog();
               // i = 7;
               // mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
              //  mPrinter.Print();
            }
        }

        private void UCReportWeekly_Load(object sender, EventArgs e)
        {
            try
            {
                dateTimePickerfrom.Text = dateTimePickerto.Value.AddDays(-6).ToShortDateString();
                LoadWeeklyButton();
            }
            catch (Exception)
            {
            }
        }

        private void dateTimePickerto_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerfrom.Text = dateTimePickerto.Value.AddDays(-6).ToShortDateString();
        }

        private double dblTotalDiscount = 0;
        private double dblTotalReturn = 0;
        private double dblTotalAccount2 = 0;
        private double dblBankEftposCashOut = 0;
        private double dblCashIn = 0;
        private double dblCashOut = 0;
        private double dblTotalCard2 = 0;
        private double dblTotalCash2 = 0;
        private double dblCash2 = 0;
        private double dblSurcharge2 = 0;
        private double dblTotalCashAccount2 = 0;
        private double dblCashDrop = 0;
        private double dblCashFloat = 0;
        private double dblFloatOut = 0;
        private double dblFloatIn = 0;
        private double dblClosingBar = 0;
        private double dblTotalSale = 0;
        private double dblBalanceCount = 0;

        private void LoadTabDaily(string Shf)
        {
            try
            {
                mConnection.Open();

                string sql = "SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where shiftID=" + Shf + " AND CashType =5";
                System.Data.DataTable tbl = mConnection.Select(sql);
                tbl = new DataTable();
                tbl = mConnection.Select(sql);
                dblBankEftposCashOut = Convert.ToDouble(tbl.Rows[0]["Subtotal"].ToString() == "" ? 0 : tbl.Rows[0]["Subtotal"]);

                sql = "select " +
                                "sum(subTotal) as totalSales," +
                                "sum(discount) as totalDisCount," +
                                "sum(cash) as cash," +
                                "sum(eftpos-`change`) as eftpos," +
                                "sum(returnPro) as returnPro," +
                                "sum(creditNote) as creditNote, " +
                                "sum(gst) as gst, " +
                                "sum(account) as account, " +
                                "sum(Surchart) as Surchart, " +
                                "sum(`change`) as `change` " +
                             "from (" +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change` " +
                                        "from ordersdaily " +
                                        "where shiftID = '" + Shf + "' and (completed=1 or completed=2) " +
                                    "union all " +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,Surchart,`change` " +
                                        "from ordersall " +
                                        "where shiftID = '" + Shf + "' and (completed=1 or completed=2) " +
                                     ") as total";

                tbl = mConnection.Select(sql);
                dblTotalSale = Convert.ToDouble(tbl.Rows[0]["totalSales"].ToString() == "" ? 0 : tbl.Rows[0]["totalSales"]);
                dblTotalDiscount = Convert.ToDouble(tbl.Rows[0]["totalDisCount"].ToString() == "" ? 0 : tbl.Rows[0]["totalDisCount"]);
                dblTotalReturn = Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]);
                dblTotalAccount2 = Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]);
                dblTotalCard2 = Convert.ToDouble(tbl.Rows[0]["eftpos"].ToString() == "" ? 0 : tbl.Rows[0]["eftpos"]);
                dblCash2 = Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? 0 : tbl.Rows[0]["cash"]);
                dblSurcharge2 = Convert.ToDouble(tbl.Rows[0]["Surchart"].ToString() == "" ? 0 : tbl.Rows[0]["Surchart"]);

                tbl = new DataTable();
                tbl = mConnection.Select("SELECT sum(TotalAmount) as Subtotal FROM cashinandcashout where shiftID=" + Shf + " AND CashType =3");
                dblCashDrop = Convert.ToDouble(tbl.Rows[0]["Subtotal"].ToString() == "" ? 0 : tbl.Rows[0]["Subtotal"]);

                tbl = new DataTable();
                tbl = mConnection.Select("select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where shiftID = '" + Shf + "' AND PayAmount<>0 ) as tmp ");
                if (tbl.Rows.Count > 0)
                {
                    dblTotalCashAccount2 = Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? "0" : tbl.Rows[0]["cash"]);
                }

                dtSource = new DataTable();
                dtSource = mConnection.Select("select RealTotalInSafeLastShift,RealTotalInSafe,CashOut from shifts where shiftID = " + Shf);
                if (dtSource.Rows.Count != 0)
                {
                    if (Convert.ToInt32(Shf) == 1)
                        dblFloatIn = 0;
                    else
                        dblFloatIn = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafeLastShift"].ToString() == "" ? "0" : dtSource.Rows[0]["RealTotalInSafeLastShift"].ToString());
                    dblFloatOut = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafe"].ToString() == "" ? "0" : dtSource.Rows[0]["RealTotalInSafe"].ToString());
                    dblCashFloat = dblFloatOut;
                }

                sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                                "FROM " +
                                "( " +
                                "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND shiftID = " + Shf + " " +
                                "UNION ALL " +
                                "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 3) AND shiftID = " + Shf + " " +
                                ")AS Source";
                dtSource = new DataTable();
                dtSource = mConnection.Select(sql);
                dblCashDrop = Convert.ToDouble(dtSource.Rows[0]["CashOut"].ToString() == "" ? 0 : dtSource.Rows[0]["CashOut"]);

                sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                        "FROM " +
                        "( " +
                        "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND shiftID = '" + Shf + "' " +
                        "UNION ALL " +
                        "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 2 OR CashType = 3 OR CashType = 4 OR CashType = 5) AND shiftID = '" + Shf + "' " +
                        ")AS Source";
                tbl = new DataTable();
                tbl = mConnection.Select(sql);

                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    try
                    {
                        dblCashIn = Convert.ToDouble(row["CashIn"]);
                    }
                    catch (Exception)
                    {
                        dblCashIn = 0;
                    }

                    try
                    {
                        dblCashOut = Convert.ToDouble(row["CashOut"]);
                    }
                    catch (Exception)
                    {
                        dblCashOut = 0;
                    }
                }

                dblTotalCash = dblCash2 + dblTotalCashAccount2 - dblTotalReturn;
                //dblClosingBar = dblTotalCash + dblTotalCard + dblTotalAccount - dblTotalReturn;
                //dblTotalSale = dblTotalSale - dblTotalReturn - dblTotalDiscount;
                dblBalanceCount = ((dblCashOut + dblCashFloat) - (dblTotalCash + dblCashIn + dblFloatIn));

                //ListViewItem li = new ListViewItem(Shf);
                //li.SubItems.Add(money.Format2(dblFloatIn));
                //li.SubItems.Add(money.Format2(dblCashFloat));
                //li.SubItems.Add(money.Format2(dblCashDrop));
                //li.SubItems.Add(money.Format2(dblFloatOut));
                //li.SubItems.Add(money.Format2(dblBalanceCount));
                //li.Tag = Convert.ToInt32(Shf).ToString();
                dtSourceShift.Rows.Add(Shf, dblBalanceCount);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.UCReportDaily_Load:::" + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                lstQuantityReportFuelSales.SelectedIndices.Clear();
                lstQuantityReportFuelSales.SelectedIndices.Add(0);
                lstQuantityReportFuelSales.Items[0].Selected = true;
                lstQuantityReportFuelSales.Items[0].EnsureVisible();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReportFuelSales.SelectedIndices[0];
                    lstQuantityReportFuelSales.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(lstQuantityReportFuelSales.Items.Count - 1);
                        lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].Selected = true;
                        lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(oldselection - 1);
                        lstQuantityReportFuelSales.Items[oldselection - 1].Selected = true;
                        lstQuantityReportFuelSales.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReportFuelSales.SelectedIndices[0];
                    lstQuantityReportFuelSales.SelectedIndices.Clear();
                    if (oldselection + 2 > lstQuantityReportFuelSales.Items.Count)
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(0);
                        lstQuantityReportFuelSales.Items[0].Selected = true;
                        lstQuantityReportFuelSales.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReportFuelSales.SelectedIndices.Add(oldselection + 1);
                        lstQuantityReportFuelSales.Items[oldselection + 1].Selected = true;
                        lstQuantityReportFuelSales.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstQuantityReportFuelSales.SelectedIndices.Count > 0)
            {
                lstQuantityReportFuelSales.SelectedIndices.Clear();
                lstQuantityReportFuelSales.SelectedIndices.Add(lstQuantityReportFuelSales.Items.Count - 1);
                lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].Selected = true;
                lstQuantityReportFuelSales.Items[lstQuantityReportFuelSales.Items.Count - 1].EnsureVisible();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                lstGroupItemFuelSales.SelectedIndices.Clear();
                lstGroupItemFuelSales.SelectedIndices.Add(0);
                lstGroupItemFuelSales.Items[0].Selected = true;
                lstGroupItemFuelSales.Items[0].EnsureVisible();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstGroupItemFuelSales.SelectedIndices[0];
                    lstGroupItemFuelSales.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                        lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                        lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(oldselection - 1);
                        lstGroupItemFuelSales.Items[oldselection - 1].Selected = true;
                        lstGroupItemFuelSales.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstGroupItemFuelSales.SelectedIndices[0];
                    lstGroupItemFuelSales.SelectedIndices.Clear();
                    if (oldselection + 2 > lstGroupItemFuelSales.Items.Count)
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(0);
                        lstGroupItemFuelSales.Items[0].Selected = true;
                        lstGroupItemFuelSales.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstGroupItemFuelSales.SelectedIndices.Add(oldselection + 1);
                        lstGroupItemFuelSales.Items[oldselection + 1].Selected = true;
                        lstGroupItemFuelSales.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (lstGroupItemFuelSales.SelectedIndices.Count > 0)
            {
                lstGroupItemFuelSales.SelectedIndices.Clear();
                lstGroupItemFuelSales.SelectedIndices.Add(lstGroupItemFuelSales.Items.Count - 1);
                lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].Selected = true;
                lstGroupItemFuelSales.Items[lstGroupItemFuelSales.Items.Count - 1].EnsureVisible();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            PrintReportDocument();
        }

        private void PrintReportDocument()
        {
            Class.LogPOS.WriteLog("Print Report");
            try
            {
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCWeeklyReport:::PrintReportDocument::" + ex.Message);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            PrintReportDocument();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                try
                {
                    flow_layout_Week.Controls.Clear();
                    LoadWeeklyButton();
                    btnPrint.Visible = true;
                    btnPrintAllWeek.Visible = true;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 0:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                try
                {
                    LoadReportQuantity();
                    btnPrint.Visible = false;
                    btnPrintAllWeek.Visible = false;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 1:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                try
                {
                    LoadReportGroupQuanity();
                    btnPrint.Visible = false;
                    btnPrintAllWeek.Visible = false;
                    mPrinter = new Printer();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportGroupQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 2:::" + ex.Message);
                }
            }
            else if (tabControl1.SelectedIndex == 3)
            {
                try
                {
                    LoadListDeliveryAndPickup();
                    btnPrint.Visible = false;
                    btnPrintAllWeek.Visible = false;
                    //mPrinter = new Printer();
                    //mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportGroupQuantity);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCReportWeekly:::btnRefesh_Click:::tabControl1.SelectedIndex == 2:::" + ex.Message);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (License.BOLicense.CheckLicense() != License.LicenseDialog.Active || License.BOLicense.CheckLicense() != License.LicenseDialog.Trial)
            {
                try
                {
                    i = 0;
                    foreach (WeeklyReport w in Report)
                    {
                        mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                        //mPrinter.SetPrinterName(dbconfig.BarPrinter);
                        mPrinter.Print();
                        i++;
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    string message1 = "Giấy phép của bạn đã hết hạn. Liên hệ Becas để được cấp mới cho bạn ngay hôm nay!!!";
                    frmMessageBoxOK frm1 = new frmMessageBoxOK("CẢNH BÁO", message1);
                    frm1.ShowDialog();
                    return;
                }
                string message = "Your license is expired. Contact Becas to renew your today!!!";
                frmMessageBoxOK frm = new frmMessageBoxOK("WARNING", message);
                frm.ShowDialog();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}