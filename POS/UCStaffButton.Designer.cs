﻿namespace POS
{
    partial class UCStaffButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelStaffID = new System.Windows.Forms.Label();
            this.labelStaffName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelStaffID
            // 
            this.labelStaffID.AutoSize = true;
            this.labelStaffID.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStaffID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaffID.Location = new System.Drawing.Point(0, 0);
            this.labelStaffID.Name = "labelStaffID";
            this.labelStaffID.Size = new System.Drawing.Size(22, 15);
            this.labelStaffID.TabIndex = 0;
            this.labelStaffID.Text = "ID:";
            this.labelStaffID.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelStaffName
            // 
            this.labelStaffName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStaffName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaffName.ForeColor = System.Drawing.Color.Red;
            this.labelStaffName.Location = new System.Drawing.Point(0, 15);
            this.labelStaffName.Name = "labelStaffName";
            this.labelStaffName.Size = new System.Drawing.Size(100, 85);
            this.labelStaffName.TabIndex = 1;
            this.labelStaffName.Text = "Name";
            this.labelStaffName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelStaffName.Click += new System.EventHandler(this.label2_Click);
            // 
            // UCStaffButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelStaffName);
            this.Controls.Add(this.labelStaffID);
            this.Name = "UCStaffButton";
            this.Size = new System.Drawing.Size(100, 100);
            this.Load += new System.EventHandler(this.UserControl1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelStaffID;
        private System.Windows.Forms.Label labelStaffName;
    }
}
