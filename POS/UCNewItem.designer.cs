﻿namespace POS
{
    partial class UCNewItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbshort = new System.Windows.Forms.Label();
            this.lbunitprice = new System.Windows.Forms.Label();
            this.lbsellsize = new System.Windows.Forms.Label();
            this.lbdisplay = new System.Windows.Forms.Label();
            this.lbselltype = new System.Windows.Forms.Label();
            this.lbvisual = new System.Windows.Forms.Label();
            this.lbprinter = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxScancode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBox3 = new POS.TextBoxPOSKeyPad();
            this.txtgst = new POS.TextBoxPOSKeyPad();
            this.textBox4 = new POS.TextBoxPOSKeyPad();
            this.textBox5 = new POS.TextBoxPOSKeyPad();
            this.SuspendLayout();
            // 
            // lbshort
            // 
            this.lbshort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbshort.AutoSize = true;
            this.lbshort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshort.Location = new System.Drawing.Point(16, 54);
            this.lbshort.Margin = new System.Windows.Forms.Padding(0);
            this.lbshort.Name = "lbshort";
            this.lbshort.Size = new System.Drawing.Size(97, 20);
            this.lbshort.TabIndex = 41;
            this.lbshort.Text = "Scan Code";
            this.lbshort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbunitprice
            // 
            this.lbunitprice.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbunitprice.AutoSize = true;
            this.lbunitprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbunitprice.Location = new System.Drawing.Point(16, 139);
            this.lbunitprice.Margin = new System.Windows.Forms.Padding(0);
            this.lbunitprice.Name = "lbunitprice";
            this.lbunitprice.Size = new System.Drawing.Size(87, 20);
            this.lbunitprice.TabIndex = 42;
            this.lbunitprice.Text = "Unit Price";
            this.lbunitprice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbsellsize
            // 
            this.lbsellsize.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbsellsize.AutoSize = true;
            this.lbsellsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbsellsize.Location = new System.Drawing.Point(16, 297);
            this.lbsellsize.Margin = new System.Windows.Forms.Padding(0);
            this.lbsellsize.Name = "lbsellsize";
            this.lbsellsize.Size = new System.Drawing.Size(79, 20);
            this.lbsellsize.TabIndex = 43;
            this.lbsellsize.Text = "Sell Size";
            this.lbsellsize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdisplay
            // 
            this.lbdisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbdisplay.AutoSize = true;
            this.lbdisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdisplay.Location = new System.Drawing.Point(16, 256);
            this.lbdisplay.Margin = new System.Windows.Forms.Padding(0);
            this.lbdisplay.Name = "lbdisplay";
            this.lbdisplay.Size = new System.Drawing.Size(117, 20);
            this.lbdisplay.TabIndex = 44;
            this.lbdisplay.Text = "Display Order";
            this.lbdisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbselltype
            // 
            this.lbselltype.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbselltype.AutoSize = true;
            this.lbselltype.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbselltype.Location = new System.Drawing.Point(16, 215);
            this.lbselltype.Margin = new System.Windows.Forms.Padding(0);
            this.lbselltype.Name = "lbselltype";
            this.lbselltype.Size = new System.Drawing.Size(82, 20);
            this.lbselltype.TabIndex = 45;
            this.lbselltype.Text = "Sell Type";
            this.lbselltype.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbvisual
            // 
            this.lbvisual.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbvisual.AutoSize = true;
            this.lbvisual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbvisual.Location = new System.Drawing.Point(16, 338);
            this.lbvisual.Margin = new System.Windows.Forms.Padding(0);
            this.lbvisual.Name = "lbvisual";
            this.lbvisual.Size = new System.Drawing.Size(64, 20);
            this.lbvisual.TabIndex = 46;
            this.lbvisual.Text = "Is Fuel";
            this.lbvisual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbprinter
            // 
            this.lbprinter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbprinter.AutoSize = true;
            this.lbprinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbprinter.Location = new System.Drawing.Point(16, 376);
            this.lbprinter.Margin = new System.Windows.Forms.Padding(0);
            this.lbprinter.Name = "lbprinter";
            this.lbprinter.Size = new System.Drawing.Size(46, 20);
            this.lbprinter.TabIndex = 48;
            this.lbprinter.Text = "Print";
            this.lbprinter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(149, 93);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(217, 26);
            this.textBox1.TabIndex = 49;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(149, 341);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 56;
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Print All",
            "Kitchen",
            "Bar"});
            this.comboBox1.Location = new System.Drawing.Point(149, 373);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(217, 28);
            this.comboBox1.TabIndex = 57;
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Each",
            "Mls",
            "Kg"});
            this.comboBox2.Location = new System.Drawing.Point(149, 213);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(217, 28);
            this.comboBox2.TabIndex = 96;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 95);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Item Desc";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxScancode
            // 
            this.textBoxScancode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxScancode.Enabled = false;
            this.textBoxScancode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxScancode.Location = new System.Drawing.Point(149, 52);
            this.textBoxScancode.Name = "textBoxScancode";
            this.textBoxScancode.ReadOnly = true;
            this.textBoxScancode.Size = new System.Drawing.Size(217, 26);
            this.textBoxScancode.TabIndex = 49;
            this.textBoxScancode.Click += new System.EventHandler(this.textBoxScancode_Click);
            this.textBoxScancode.Leave += new System.EventHandler(this.textBoxScancode_Leave);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(149, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(217, 43);
            this.button1.TabIndex = 97;
            this.button1.Text = "Generate BarCode";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 177);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 99;
            this.label2.Text = "GST";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(337, 177);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 20);
            this.label3.TabIndex = 100;
            this.label3.Text = "%";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 421);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 20);
            this.label4.TabIndex = 101;
            this.label4.Text = "Enable Fuel Discount";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(206, 425);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 102;
            this.checkBox2.UseVisualStyleBackColor = false;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox3.IsLockDot = false;
            this.textBox3.Location = new System.Drawing.Point(149, 133);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(217, 26);
            this.textBox3.TabIndex = 103;
            // 
            // txtgst
            // 
            this.txtgst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtgst.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtgst.IsLockDot = false;
            this.txtgst.Location = new System.Drawing.Point(149, 177);
            this.txtgst.Name = "txtgst";
            this.txtgst.Size = new System.Drawing.Size(185, 26);
            this.txtgst.TabIndex = 104;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox4.IsLockDot = false;
            this.textBox4.Location = new System.Drawing.Point(149, 297);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(217, 26);
            this.textBox4.TabIndex = 105;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox5.IsLockDot = true;
            this.textBox5.Location = new System.Drawing.Point(149, 256);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(217, 26);
            this.textBox5.TabIndex = 106;
            // 
            // UCNewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.txtgst);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBoxScancode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbshort);
            this.Controls.Add(this.lbunitprice);
            this.Controls.Add(this.lbsellsize);
            this.Controls.Add(this.lbdisplay);
            this.Controls.Add(this.lbselltype);
            this.Controls.Add(this.lbvisual);
            this.Controls.Add(this.lbprinter);
            this.Name = "UCNewItem";
            this.Size = new System.Drawing.Size(369, 518);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbshort;
        private System.Windows.Forms.Label lbunitprice;
        private System.Windows.Forms.Label lbsellsize;
        private System.Windows.Forms.Label lbdisplay;
        private System.Windows.Forms.Label lbselltype;
        private System.Windows.Forms.Label lbvisual;
        private System.Windows.Forms.Label lbprinter;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.ComboBox comboBox1;
        public System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxScancode;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.CheckBox checkBox2;
        internal TextBoxPOSKeyPad textBox3;
        internal TextBoxPOSKeyPad textBox4;
        internal TextBoxPOSKeyPad txtgst;
        internal TextBoxPOSKeyPad textBox5;

    }
}
