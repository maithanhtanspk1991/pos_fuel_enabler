﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCSurChanger : UserControl
    {
        private Connection.Connection mConnection;

        public UCSurChanger()
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
        }

        private void UCSurChanger_Load(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("UCSurChanger.UCSurChanger_Load: Load UC SurChanger");
            labelSurchanger.Text = "Surcharge Disable";
            label2.Text = "Enable surcharge, Press YES";
            try
            {
                mConnection.Open();
                int count = Convert.ToInt16(mConnection.ExecuteScalar("select count(loyaltyID) from loyaltyscheme where `enable`=1"));
                if (count > 0)
                {
                    labelSurchanger.Text = "Surcharge Enable";
                    label2.Text = "Disable surcharge, Press YES";
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCSurChanger.UCSurChanger_Load:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (labelSurchanger.Text == "Surcharge Enable")
            {
                try
                {
                    mConnection.Open();
                    mConnection.ExecuteNonQuery("update loyaltyscheme set `enable`=0 where `enable`=1");
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("UCSurChanger.Button_SurChanger_Click_To Disable:::" + ex.Message);
                }
                finally
                {
                    mConnection.Close();
                }
                labelSurchanger.Text = "Surcharge Disable";
                label2.Text = "Enable surcharge, Press YES";
            }
            else
            {
                try
                {
                    DateTime dt = DateTime.Now;
                    mConnection.Open();
                    mConnection.ExecuteNonQuery("insert into loyaltyscheme(name,startDate,startTime,mode,`enable`) values('Surchanger','" + dt.Year + "-" + dt.Month + "-" + dt.Day + "','" + dt.Hour + ":" + dt.Minute + ":" + dt.Second + "',99,1)");
                }
                catch (Exception exs)
                {
                    Class.LogPOS.WriteLog("UCSurChanger.Button_SurChanger_Click_To Enable:::" + exs.Message);
                }
                finally
                {
                    mConnection.Close();
                }
                labelSurchanger.Text = "Surcharge Enable";
                label2.Text = "Disable surcharge, Press YES";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}