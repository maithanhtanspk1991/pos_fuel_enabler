﻿using System;
using System.Collections.Generic;

namespace Barcode
{
    public class SerialPort
    {
        public static int ORDER_ALL = 1;
        public static int CHANGE_MENU = 2;
        public static int MENU_BARCODE = 3;
        public static int STOCK = 4;
        public static int MENU_ITEM = 5;
        private System.IO.Ports.SerialPort mSerialPort;
        private POS.Class.ReadConfig readconfig = new POS.Class.ReadConfig();

        //private System.Threading.Thread mThread;
        public event MyPortEvenHandler Received;

        private List<MyPortEvenHandler> mListEvent;

        //public bool LockOrdersAll { get; set; }
        public int TypeOfBarcode { get; set; }

        public delegate void MyPortEvenHandler(string data);

        public delegate void PortReceiveFuelEvenHandler(string data);

        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);

        public SerialPort()
        {
            mListEvent = new List<MyPortEvenHandler>();
        }

        public void OpenAndStart()
        {
            //LockOrdersAll = false;
            try
            {
                mSerialPort = new System.IO.Ports.SerialPort(readconfig.BarCode, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
                mSerialPort.Open();
                System.Threading.Thread mThread = new System.Threading.Thread(ReadPort);
                mThread.Start();
            }
            catch (Exception ex)
            {
                POS.Class.LogPOS.WriteLog("SerialPort:::" + ex.Message);
            }
        }

        public void AddEvent(MyPortEvenHandler e)
        {
            foreach (MyPortEvenHandler events in mListEvent)
            {
                Received -= events;
            }
            mListEvent.Clear();
            mListEvent.Add(e);
            Received += e;
        }

        public void ClearAllEvent()
        {
            if (mListEvent != null)
            {
                foreach (MyPortEvenHandler events in mListEvent)
                {
                    Received -= events;
                }
                mListEvent.Clear();
            }
        }

        private void OnReceived(string data)
        {
            if (Received != null)
            {
                Received(data);
            }
        }

        private void ReadPort()
        {
            string resuilt = "";
            while (true)
            {
                try
                {
                    int chr = mSerialPort.ReadChar();                    
                    if (chr == 13)
                    {
                        POS.Class.LogPOS.WriteLog("Barcode.ReadPort:::" + resuilt);
                        OnReceived(resuilt);
                        resuilt = "";
                    }
                    else
                    {
                        if (chr >= (int)'0' && chr <= (char)'9')
                        {
                            resuilt += (char)chr;
                        }
                    }
                }

                catch (Exception ex)
                {
                    resuilt = "";
                    SystemLog.LogPOS.WriteLog("Barcode::SerialPort::ReadPort::" + ex.Message);
                }
            }
        }

        public void SetText(string text, System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }
    }
}