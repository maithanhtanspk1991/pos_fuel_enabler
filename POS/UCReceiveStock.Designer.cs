﻿namespace POS
{
    partial class UCReceiveStock
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonItem = new System.Windows.Forms.Button();
            this.buttonSupplier = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxPOS_OSK_DocketNo = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_Areas = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_Receive_Tax = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_Supplier = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSKCost_Price = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_Item = new POS.TextBoxPOS_OSK();
            this.textBoxPOS_OSK_Qty = new POS.TextBoxPOS_OSK();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.uCkeypad1 = new POS.UCkeypad();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(494, 562);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item";
            this.columnHeader1.Width = 180;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Bulk";
            this.columnHeader2.Width = 117;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "qty";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Cost Price";
            this.columnHeader4.Width = 100;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Item";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Qty";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 402);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "Cost Price";
            // 
            // buttonItem
            // 
            this.buttonItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonItem.Location = new System.Drawing.Point(289, 284);
            this.buttonItem.Name = "buttonItem";
            this.buttonItem.Size = new System.Drawing.Size(75, 63);
            this.buttonItem.TabIndex = 8;
            this.buttonItem.Text = "Edit...";
            this.buttonItem.UseVisualStyleBackColor = true;
            this.buttonItem.Click += new System.EventHandler(this.buttonItem_Click);
            // 
            // buttonSupplier
            // 
            this.buttonSupplier.Location = new System.Drawing.Point(153, 8);
            this.buttonSupplier.Name = "buttonSupplier";
            this.buttonSupplier.Size = new System.Drawing.Size(103, 40);
            this.buttonSupplier.TabIndex = 8;
            this.buttonSupplier.Text = "Edit...";
            this.buttonSupplier.UseVisualStyleBackColor = true;
            this.buttonSupplier.Click += new System.EventHandler(this.buttonSupplier_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxPOS_OSK_DocketNo);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 50);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Docket No.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxPOS_OSK_Areas);
            this.groupBox2.Location = new System.Drawing.Point(166, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 50);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Areas";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxPOS_OSK_Receive_Tax);
            this.groupBox3.Location = new System.Drawing.Point(322, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 50);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Receive Tax";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonSupplier);
            this.groupBox4.Controls.Add(this.textBoxPOS_OSK_Supplier);
            this.groupBox4.Location = new System.Drawing.Point(478, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(259, 50);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Supplier";
            // 
            // buttonNew
            // 
            this.buttonNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNew.Location = new System.Drawing.Point(2, 460);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(180, 100);
            this.buttonNew.TabIndex = 11;
            this.buttonNew.Text = "New";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.Location = new System.Drawing.Point(186, 460);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(180, 100);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(177, 570);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(175, 70);
            this.buttonSave.TabIndex = 11;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(2, 570);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(175, 70);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.buttonSave);
            this.splitContainer1.Panel2.Controls.Add(this.buttonCancel);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(880, 768);
            this.splitContainer1.SplitterDistance = 66;
            this.splitContainer1.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(496, 564);
            this.panel1.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 641);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(876, 53);
            this.panel2.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.groupBox5);
            this.panel3.Controls.Add(this.buttonItem);
            this.panel3.Controls.Add(this.textBoxPOS_OSKCost_Price);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.textBoxPOS_OSK_Item);
            this.panel3.Controls.Add(this.buttonDelete);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.buttonNew);
            this.panel3.Controls.Add(this.textBoxPOS_OSK_Qty);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(501, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(372, 564);
            this.panel3.TabIndex = 15;
            // 
            // textBoxPOS_OSK_DocketNo
            // 
            this.textBoxPOS_OSK_DocketNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_DocketNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_DocketNo.KeyBoadOSK = null;
            this.textBoxPOS_OSK_DocketNo.KeyPad = null;
            this.textBoxPOS_OSK_DocketNo.Location = new System.Drawing.Point(9, 19);
            this.textBoxPOS_OSK_DocketNo.Name = "textBoxPOS_OSK_DocketNo";
            this.textBoxPOS_OSK_DocketNo.Size = new System.Drawing.Size(135, 26);
            this.textBoxPOS_OSK_DocketNo.TabIndex = 0;
            // 
            // textBoxPOS_OSK_Areas
            // 
            this.textBoxPOS_OSK_Areas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Areas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Areas.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Areas.KeyPad = null;
            this.textBoxPOS_OSK_Areas.Location = new System.Drawing.Point(6, 19);
            this.textBoxPOS_OSK_Areas.Name = "textBoxPOS_OSK_Areas";
            this.textBoxPOS_OSK_Areas.ReadOnly = true;
            this.textBoxPOS_OSK_Areas.Size = new System.Drawing.Size(138, 26);
            this.textBoxPOS_OSK_Areas.TabIndex = 0;
            // 
            // textBoxPOS_OSK_Receive_Tax
            // 
            this.textBoxPOS_OSK_Receive_Tax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Receive_Tax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Receive_Tax.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Receive_Tax.KeyPad = null;
            this.textBoxPOS_OSK_Receive_Tax.Location = new System.Drawing.Point(6, 19);
            this.textBoxPOS_OSK_Receive_Tax.Name = "textBoxPOS_OSK_Receive_Tax";
            this.textBoxPOS_OSK_Receive_Tax.Size = new System.Drawing.Size(138, 26);
            this.textBoxPOS_OSK_Receive_Tax.TabIndex = 0;
            // 
            // textBoxPOS_OSK_Supplier
            // 
            this.textBoxPOS_OSK_Supplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Supplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Supplier.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Supplier.KeyPad = null;
            this.textBoxPOS_OSK_Supplier.Location = new System.Drawing.Point(9, 19);
            this.textBoxPOS_OSK_Supplier.Name = "textBoxPOS_OSK_Supplier";
            this.textBoxPOS_OSK_Supplier.ReadOnly = true;
            this.textBoxPOS_OSK_Supplier.Size = new System.Drawing.Size(138, 26);
            this.textBoxPOS_OSK_Supplier.TabIndex = 0;
            // 
            // textBoxPOS_OSKCost_Price
            // 
            this.textBoxPOS_OSKCost_Price.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSKCost_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSKCost_Price.KeyBoadOSK = null;
            this.textBoxPOS_OSKCost_Price.KeyPad = null;
            this.textBoxPOS_OSKCost_Price.Location = new System.Drawing.Point(109, 402);
            this.textBoxPOS_OSKCost_Price.Name = "textBoxPOS_OSKCost_Price";
            this.textBoxPOS_OSKCost_Price.Size = new System.Drawing.Size(255, 29);
            this.textBoxPOS_OSKCost_Price.TabIndex = 0;
            this.textBoxPOS_OSKCost_Price.TextChanged += new System.EventHandler(this.textBoxPOS_OSKCost_Price_TextChanged);
            // 
            // textBoxPOS_OSK_Item
            // 
            this.textBoxPOS_OSK_Item.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Item.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Item.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Item.KeyPad = null;
            this.textBoxPOS_OSK_Item.Location = new System.Drawing.Point(109, 284);
            this.textBoxPOS_OSK_Item.Multiline = true;
            this.textBoxPOS_OSK_Item.Name = "textBoxPOS_OSK_Item";
            this.textBoxPOS_OSK_Item.ReadOnly = true;
            this.textBoxPOS_OSK_Item.Size = new System.Drawing.Size(177, 63);
            this.textBoxPOS_OSK_Item.TabIndex = 0;
            // 
            // textBoxPOS_OSK_Qty
            // 
            this.textBoxPOS_OSK_Qty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOS_OSK_Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOS_OSK_Qty.KeyBoadOSK = null;
            this.textBoxPOS_OSK_Qty.KeyPad = null;
            this.textBoxPOS_OSK_Qty.Location = new System.Drawing.Point(109, 353);
            this.textBoxPOS_OSK_Qty.Name = "textBoxPOS_OSK_Qty";
            this.textBoxPOS_OSK_Qty.Size = new System.Drawing.Size(255, 29);
            this.textBoxPOS_OSK_Qty.TabIndex = 0;
            this.textBoxPOS_OSK_Qty.TextChanged += new System.EventHandler(this.textBoxPOS_OSK_Qty_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.uCkeypad1);
            this.groupBox5.Location = new System.Drawing.Point(5, -3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(359, 284);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(103, 10);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 0;
            this.uCkeypad1.txtResult = null;
            // 
            // UCReceiveStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "UCReceiveStock";
            this.Size = new System.Drawing.Size(880, 768);
            this.Load += new System.EventHandler(this.UCReceiveStock_Load);
            this.VisibleChanged += new System.EventHandler(this.UCReceiveStock_VisibleChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxPOS_OSK textBoxPOS_OSK_DocketNo;
        private TextBoxPOS_OSK textBoxPOS_OSK_Supplier;
        private TextBoxPOS_OSK textBoxPOS_OSK_Areas;
        private TextBoxPOS_OSK textBoxPOS_OSK_Receive_Tax;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private TextBoxPOS_OSK textBoxPOS_OSK_Item;
        private System.Windows.Forms.Label label5;
        private TextBoxPOS_OSK textBoxPOS_OSK_Qty;
        private System.Windows.Forms.Label label7;
        private TextBoxPOS_OSK textBoxPOS_OSKCost_Price;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonItem;
        private System.Windows.Forms.Button buttonSupplier;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox5;
        private UCkeypad uCkeypad1;
    }
}
