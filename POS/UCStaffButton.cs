﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCStaffButton : UserControl
    {
        private int mStaffID;
        private string mStaffName;

        public int StaffID
        {
            set
            {
                mStaffID = value;
                labelStaffID.Text = "ID:" + mStaffID;
            }
            get { return mStaffID; }
        }

        public string StaffName
        {
            get { return mStaffName; }
            set
            {
                mStaffName = value;
                labelStaffName.Text = mStaffName;
            }
        }

        public UCStaffButton()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
        }
    }
}