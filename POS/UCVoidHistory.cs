﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Forms;

namespace POS
{
    public partial class UCVoidHistory : UserControl
    {
        private Class.MoneyFortmat mMoneyFortmat;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private Class.MoneyFortmat money;
        private Connection.ReadDBConfig dbconfig;

        public UCVoidHistory()
        {
            InitializeComponent();
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            dbconfig = new Connection.ReadDBConfig();
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mSetting = new Class.Setting();
            LoadData();
            SetMultiLanguge();
        }

        private void SetMultiLanguge()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnPrint.Text = "IN";
                label11.Text = "Ngày xem:";
                lblTotal.Text = "Tổng cộng:";
                columnHeader1.Text = "Thời gian";
                columnHeader2.Text = "Tên hàng";
                columnHeader3.Text = "Tên nhân viên";
                columnHeader4.Text = "Order";
                columnHeader5.Text = "Ca làm việc";
                columnHeader6.Text = "SL";
                columnHeader7.Text = "Hành động";
                columnHeader8.Text = "Thành tiền";
                return;
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = dbconfig.Header1.ToString();
            string bankCode = dbconfig.Header2.ToString();
            string address = dbconfig.Header3.ToString();
            string tell = dbconfig.Header4.ToString();
            string address1 = dbconfig.Header5.ToString();
            string terminal = dbconfig.CableID.ToString();
            string website = dbconfig.FootNode1.ToString();
            string thankyou = dbconfig.FootNode2.ToString();
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("Void History ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            l_y += mPrinter.GetHeightPrinterLine() / 5;
            mPrinter.DrawString("Order - Item (Quantity)", e, new Font("Arial", 10, FontStyle.Bold), l_y, 1);
            l_y = mPrinter.DrawString("Total", e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine();
            try
            {
                foreach (ListViewItem lvi in lstVoidHistory.Items)
                {
                    mPrinter.DrawString(lvi.SubItems[2].Text.ToString() + " - " + lvi.SubItems[1].Text.ToString() + "(" + lvi.SubItems[4].Text.ToString() + ")", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString(lvi.SubItems[6].Text.ToString(), e, new Font("Arial", 10), l_y, 3);
                    l_y += mPrinter.GetHeightPrinterLine();
                }
            }
            catch (Exception)
            {
            }

            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
        }

        private void LoadData()
        {
            Connection.Connection con = new Connection.Connection();
            double total = 0;
            try
            {
                con.Open();
                lstVoidHistory.Items.Clear();
                string date = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day;
                //DataTable tbl = con.Select(
                //    "select "+
                //        "v.ts,"+
                //        "if("+
                //                "itemID<>0," +
                //                "(select i.itemDesc from itemsmenu i where i.itemID=v.itemID),"+
                //                "if(optionID<>0," +
                //                "(select il.itemDesc from itemslinemenu il where v.optionID=il.lineID)," +
                //                "if((select d.itemDesc from dynitemsmenu d where d.dynID=v.dynID and date(d.ts)=date(v.ts) and d.shiftID=v.shiftID limit 0,1) is null,(select d.itemDesc from dynitemsall d where d.dynID=v.dynID and date(d.ts)=date(v.ts) and d.shiftID=v.shiftID limit 0,1 ),(select d.itemDesc from dynitemsmenu d where d.dynID=v.dynID and date(d.ts)=date(v.ts))))" +
                //            ") as ItemName,"+
                //        "orderID,"+
                //        "shiftID,"+
                //        "qty,"+
                //        "`type`,"+
                //        "v.subTotal "+
                //    "from voiditemhistory v "+
                //    "where date(ts)=date('"+date+"')"
                //);
                string query = "select " +
                        "v.ts," +
                        "if(" +
                                "itemID<>0," +
                                "(select i.itemDesc from itemsmenu i where i.itemID=v.itemID)," +
                                "if(optionID<>0," +
                                "(select il.itemDesc from itemslinemenu il where v.optionID=il.lineID)," +
                                "if((select d.itemDesc from dynitemsmenu d where d.dynID=v.dynID and date(d.ts)=date(v.ts) and d.shiftID=v.shiftID limit 0,1) is null,(select d.itemDesc from dynitemsall d where d.dynID=v.dynID and date(d.ts)=date(v.ts) and d.shiftID=v.shiftID limit 0,1 ),(select d.itemDesc from dynitemsmenu d where d.dynID=v.dynID and date(d.ts)=date(v.ts))))" +
                            ") as ItemName," +
                        "orderID," +
                        "shiftID," +
                        "qty," +
                        "`type`," +
                        "v.subTotal ," + "s.Name " +

                    "from voiditemhistory v,(SELECT staffID,Name FROM staffs union (select '0' as staffID,'Manager' as Name)) s" +
                    " where date(ts)=date('" + date + "') and s.staffID = v.staffID";
                DataTable tbl = con.Select(query);
                foreach (DataRow row in tbl.Rows)
                {
                    DateTime dt = Convert.ToDateTime(row["ts"]);
                    //ListViewItem li = new ListViewItem(dt.Day+"/"+dt.Month+"/"+dt.Year+" "+dt.Hour+":"+dt.Minute+":"+dt.Second);
                    ListViewItem li = new ListViewItem(dt.Hour + ":" + dt.Minute + ":" + dt.Second);
                    li.SubItems.Add(row["itemName"].ToString());

                    li.SubItems.Add(row["orderID"].ToString());
                    //li.SubItems.Add(row["shiftID"].ToString());
                    DataObject.ShiftObject shiftobject = new DataObject.ShiftObject();
                    shiftobject.ShiftID = Convert.ToInt32(row["shiftID"]);
                    shiftobject = BusinessObject.BOShift.GetShiftMaxWithShiftID(shiftobject);
                    li.SubItems.Add(shiftobject.ShiftName.ToString());

                    li.SubItems.Add(String.Format("{0:0,0}", row["qty"]));
                    int type = Convert.ToInt16(row["type"]);
                    string action = "delete";
                    if (type == 1)
                    {
                        action = "edit";
                    }
                    li.SubItems.Add(action);
                    double sub = Convert.ToDouble(row["subTotal"]);
                    total += sub;
                    li.SubItems.Add(mMoneyFortmat.Format2(sub));
                    li.SubItems.Add(row["Name"].ToString());
                    lstVoidHistory.Items.Add(li);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                con.Close();
            }
            lblTotal.Text = "Total:" + mMoneyFortmat.Format2(total);
        }

        private void dateTimePickerfrom_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void UCVoidHistory_Load(object sender, EventArgs e)
        {
            columnHeader1.Width = 131;
            columnHeader2.Width = 234;
            columnHeader3.Width = 81;
            columnHeader4.Width = 76;
            columnHeader5.Width = 60;
            columnHeader6.Width = 107;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Active || License.BOLicense.CheckLicense() == License.LicenseDialog.Trial)
            {
                try
                {
                    mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                    mPrinter.printDocument.Print();
                }
                catch (Exception)
                {
                }
            }
            //else
            //{
            //    string message = "Your license is expired. Contact Becas to renew your today!!!";
            //    frmMessageBoxOK frm = new frmMessageBoxOK("WARNING", message);
            //    frm.ShowDialog();
            //}
        }
    }
}