﻿namespace POS
{
    partial class UCListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lvListItem = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colItemPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUpdateQty = new System.Windows.Forms.Button();
            this.btnDeleteQty = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtQuantity = new POS.TextBoxPOSKeyPad();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(90)))), ((int)(((byte)(0)))));
            this.btnDeleteItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.Color.White;
            this.btnDeleteItem.Location = new System.Drawing.Point(171, 200);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(72, 53);
            this.btnDeleteItem.TabIndex = 9;
            this.btnDeleteItem.Text = "Delete Item";
            this.btnDeleteItem.UseVisualStyleBackColor = false;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(255)))));
            this.btnAddItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.Color.White;
            this.btnAddItem.Location = new System.Drawing.Point(15, 200);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(72, 53);
            this.btnAddItem.TabIndex = 8;
            this.btnAddItem.Text = "Add Item";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Quantity:";
            // 
            // lvListItem
            // 
            this.lvListItem.CheckBoxes = true;
            this.lvListItem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.colItemName,
            this.colItemPrice});
            this.lvListItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvListItem.FullRowSelect = true;
            this.lvListItem.GridLines = true;
            this.lvListItem.Location = new System.Drawing.Point(9, 44);
            this.lvListItem.MultiSelect = false;
            this.lvListItem.Name = "lvListItem";
            this.lvListItem.Size = new System.Drawing.Size(320, 150);
            this.lvListItem.TabIndex = 5;
            this.lvListItem.UseCompatibleStateImageBehavior = false;
            this.lvListItem.View = System.Windows.Forms.View.Details;
            this.lvListItem.SelectedIndexChanged += new System.EventHandler(this.lvListItem_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 22;
            // 
            // colItemName
            // 
            this.colItemName.Text = "Name";
            this.colItemName.Width = 196;
            // 
            // colItemPrice
            // 
            this.colItemPrice.Text = "Price";
            this.colItemPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colItemPrice.Width = 70;
            // 
            // btnUpdateQty
            // 
            this.btnUpdateQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.btnUpdateQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateQty.ForeColor = System.Drawing.Color.White;
            this.btnUpdateQty.Location = new System.Drawing.Point(93, 200);
            this.btnUpdateQty.Name = "btnUpdateQty";
            this.btnUpdateQty.Size = new System.Drawing.Size(72, 53);
            this.btnUpdateQty.TabIndex = 10;
            this.btnUpdateQty.Text = "Update Quantity";
            this.btnUpdateQty.UseVisualStyleBackColor = false;
            this.btnUpdateQty.Click += new System.EventHandler(this.btnUpdateQty_Click);
            // 
            // btnDeleteQty
            // 
            this.btnDeleteQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(50)))), ((int)(((byte)(0)))));
            this.btnDeleteQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteQty.ForeColor = System.Drawing.Color.White;
            this.btnDeleteQty.Location = new System.Drawing.Point(249, 200);
            this.btnDeleteQty.Name = "btnDeleteQty";
            this.btnDeleteQty.Size = new System.Drawing.Size(72, 53);
            this.btnDeleteQty.TabIndex = 11;
            this.btnDeleteQty.Text = "Delete Quantity";
            this.btnDeleteQty.UseVisualStyleBackColor = false;
            this.btnDeleteQty.Click += new System.EventHandler(this.btnDeleteQty_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnDeleteQty);
            this.groupBox1.Controls.Add(this.lvListItem);
            this.groupBox1.Controls.Add(this.btnUpdateQty);
            this.groupBox1.Controls.Add(this.txtQuantity);
            this.groupBox1.Controls.Add(this.btnDeleteItem);
            this.groupBox1.Controls.Add(this.btnAddItem);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(1, -4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(339, 262);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtQuantity.IsLockDot = false;
            this.txtQuantity.Location = new System.Drawing.Point(83, 13);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(246, 26);
            this.txtQuantity.TabIndex = 7;
            // 
            // UCListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCListItem";
            this.Size = new System.Drawing.Size(344, 262);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnDeleteItem;
        public System.Windows.Forms.Button btnAddItem;
        public TextBoxPOSKeyPad txtQuantity;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.ListView lvListItem;
        public System.Windows.Forms.ColumnHeader columnHeader1;
        public System.Windows.Forms.ColumnHeader colItemName;
        public System.Windows.Forms.ColumnHeader colItemPrice;
        public System.Windows.Forms.Button btnUpdateQty;
        private System.Windows.Forms.Button btnDeleteQty;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
