﻿using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCColorstatus : UserControl
    {
        public UCColorstatus()
        {
            InitializeComponent();
            panel1.BackColor = Color.White;
            panel2.BackColor = Color.CornflowerBlue;
            panel3.BackColor = Color.Green;
            panel4.BackColor = Color.Orange;
        }
    }
}