﻿using System;

namespace POS
{
    partial class TextBoxPOSKeyBoard : System.Windows.Forms.TextBox
    {
        public TextBoxPOSKeyBoard()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
        }

        protected override void OnEnter(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.White;
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(this);
            frm.ShowDialog();
            base.OnClick(e);
        }
    }
}