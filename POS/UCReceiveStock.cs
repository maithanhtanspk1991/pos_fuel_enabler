﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCReceiveStock : UserControl
    {
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Connection.Connection mConnection;
        private bool mIsLock = false;

        public UCReceiveStock()
        {
            InitializeComponent();
        }

        private void UCReceiveStock_Load(object sender, EventArgs e)
        {
            RemoveText();
            try
            {
                mConnection = new Connection.Connection();
                mConnection.Open();
                string areaid = mConnection.ExecuteScalar("select areaID from cable where cableID=" + dbconfig.CableID.ToString()).ToString();
                string areaname = mConnection.ExecuteScalar("select name from areas where areaID=" + areaid.ToString()).ToString();
                textBoxPOS_OSK_Areas.Text = areaname;
                textBoxPOS_OSK_Areas.Tag = areaid;
                mConnection.Close();
            }
            catch (Exception)
            {
            }
        }

        private void buttonSupplier_Click(object sender, EventArgs e)
        {
            //Forms.frmSupplier frm = new Forms.frmSupplier();
            //if (frm.ShowDialog() == DialogResult.OK)
            //{
            //    Class.Supplier sup = frm.getSupplier();
            //    if (sup != null)
            //    {
            //        textBoxPOS_OSK_Supplier.Text = sup.Name;
            //        textBoxPOS_OSK_Supplier.Tag = sup;
            //    }
            //}
        }

        private void buttonItem_Click(object sender, EventArgs e)
        {
            Forms.frmItems frm = new Forms.frmItems();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Forms.frmItems.Item item = frm.GetItem();
                if (item != null && listView1.SelectedIndices.Count > 0)
                {
                    ReceivedStockLine re = (ReceivedStockLine)listView1.SelectedItems[0].Tag;
                    listView1.SelectedItems[0].SubItems[0].Text = item.ItemName;
                    textBoxPOS_OSK_Item.Text = item.ItemName;
                    re.Item = item;
                    //textBoxPOS_OSK_Item.Tag = item;

                    listView1.SelectedItems[0].SubItems[1].Text = "";
                    re.Bulk = null;
                    //textBoxPOS_OSK_Bulk.Tag = null;
                }
            }
        }

        private void buttonBulk_Click(object sender, EventArgs e)
        {
            Forms.frmBulk frm = new Forms.frmBulk();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Class.Bulk bulk = frm.GetBulk();
                if (bulk != null && listView1.SelectedIndices.Count > 0)
                {
                    ReceivedStockLine re = (ReceivedStockLine)listView1.SelectedItems[0].Tag;
                    listView1.SelectedItems[0].SubItems[1].Text = bulk.Name;
                    re.Bulk = bulk;
                    //textBoxPOS_OSK_Bulk.Tag = bulk;

                    textBoxPOS_OSK_Item.Text = "";
                    listView1.SelectedItems[0].SubItems[0].Text = "";
                    //textBoxPOS_OSK_Item.Tag = null;
                    re.Item = null;
                }
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            mIsLock = true;
            ListViewItem li = new ListViewItem("");
            li.SubItems.Add("");
            li.SubItems.Add("0");
            li.SubItems.Add("0");
            ReceivedStockLine re = new ReceivedStockLine(null, null, 0, 0);
            li.Tag = re;
            listView1.Items.Add(li);
            li.Selected = true;
            li.EnsureVisible();
            mIsLock = false;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            mIsLock = true;
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                RemoveText();
            }
            mIsLock = false;
        }

        private void RemoveText()
        {
            textBoxPOS_OSK_Item.Text = "";
            textBoxPOS_OSK_Item.Tag = null;
            textBoxPOS_OSK_Qty.Text = "";
            textBoxPOS_OSKCost_Price.Text = "";
        }

        public void ClearText()
        {
            RemoveText();
            listView1.Items.Clear();
            textBoxPOS_OSK_DocketNo.Text = "";
            //textBoxPOS_OSK_Areas.Text = "";
            textBoxPOS_OSK_Receive_Tax.Text = "";
            textBoxPOS_OSK_Supplier.Text = "";
            textBoxPOS_OSK_Supplier.Tag = null;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mIsLock = true;
            if (listView1.SelectedIndices.Count > 0)
            {
                ListViewItem li = listView1.SelectedItems[0];
                if (li.Tag != null)
                {
                    ReceivedStockLine re = (ReceivedStockLine)li.Tag;
                    if (re.Item != null)
                    {
                        textBoxPOS_OSK_Item.Text = re.Item.ItemName;
                    }
                    else
                    {
                        textBoxPOS_OSK_Item.Text = "";
                    }
                    textBoxPOS_OSK_Qty.Text = re.Qty + "";
                    textBoxPOS_OSKCost_Price.Text = re.CostPrice + "";
                }
            }
            mIsLock = false;
        }

        private class ReceivedStockLine
        {
            public Forms.frmItems.Item Item { get; set; }

            public Class.Bulk Bulk { get; set; }

            public int Qty { get; set; }

            public double CostPrice { get; set; }

            public ReceivedStockLine(Forms.frmItems.Item item, Class.Bulk bulk, int qty, double costPrice)
            {
                Item = item;
                Bulk = bulk;
                Qty = qty;
                CostPrice = costPrice;
            }
        }

        private void textBoxPOS_OSK_Qty_TextChanged(object sender, EventArgs e)
        {
            if (!mIsLock && listView1.SelectedIndices.Count > 0)
            {
                ReceivedStockLine re = (ReceivedStockLine)listView1.SelectedItems[0].Tag;
                listView1.SelectedItems[0].SubItems[2].Text = textBoxPOS_OSK_Qty.Text;
                try
                {
                    re.Qty = Convert.ToInt16(textBoxPOS_OSK_Qty.Text);
                }
                catch (Exception)
                { }
            }
        }

        private void textBoxPOS_OSKCost_Price_TextChanged(object sender, EventArgs e)
        {
            if (!mIsLock && listView1.SelectedIndices.Count > 0)
            {
                ReceivedStockLine re = (ReceivedStockLine)listView1.SelectedItems[0].Tag;
                listView1.SelectedItems[0].SubItems[3].Text = textBoxPOS_OSKCost_Price.Text;
                try
                {
                    re.CostPrice = Convert.ToInt16(textBoxPOS_OSKCost_Price.Text);
                }
                catch (Exception)
                { }
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                int supplierID = 0;
                if (textBoxPOS_OSK_Supplier.Tag != null)
                {
                    supplierID = ((Class.Supplier)textBoxPOS_OSK_Supplier.Tag).SupplierID;
                }
                int areaID = 0;
                if (textBoxPOS_OSK_Areas.Tag != null)
                {
                    areaID = Convert.ToInt16(textBoxPOS_OSK_Areas.Tag);
                }
                int receivedAmt = 0;
                try
                {
                    receivedAmt = Convert.ToInt16(textBoxPOS_OSK_Receive_Tax.Text);
                }
                catch (Exception) { }
                mConnection.ExecuteNonQuery("insert into receivedstock(docketNo,supplierID,areaID,receivedAmt) values('" + textBoxPOS_OSK_DocketNo.Text + "'," + supplierID + "," + areaID + "," + receivedAmt + ")");
                int receivedID = Convert.ToInt16(mConnection.ExecuteScalar("select max(receivedID) from receivedstock"));
                foreach (ListViewItem li in listView1.Items)
                {
                    ReceivedStockLine re = (ReceivedStockLine)li.Tag;
                    if (re.Qty != 0 && (re.Bulk != null || re.Item != null))
                    {
                        if (re.Item != null)
                        {
                            mConnection.ExecuteNonQuery("insert into recvstocklines(receivedID,itemID,qty,costPrice) values(" + receivedID + "," + re.Item.ItemID + "," + re.Qty + "," + re.CostPrice + ")");
                        }
                        else
                        {
                            mConnection.ExecuteNonQuery("insert into recvstocklines(receivedID,bulkID,qty,costPrice) values(" + receivedID + "," + re.Bulk.BulkID + "," + re.Qty + "," + re.CostPrice + ")");
                        }
                    }
                }
                ClearText();
                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void UCReceiveStock_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                RemoveText();
            }
        }
    }
}