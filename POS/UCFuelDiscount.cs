﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCFuelDiscount : UserControl
    {
        public UCFuelDiscount()
        {
            InitializeComponent();
        }

        private void UCFuelDiscount_Load(object sender, EventArgs e)
        {
            txtDiscount.Text = Class.FuelDiscountConfig.GetDiscount() + "";
            txtSpend.Text = Class.FuelDiscountConfig.GetSpend() + "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class.FuelDiscountConfig.SetDiscount(txtDiscount.Text);
            Class.FuelDiscountConfig.SetSpend(txtSpend.Text);
        }
    }
}