﻿using System;

namespace POS
{
    internal class TextBoxPOS_OSK : System.Windows.Forms.TextBox
    {
        public POS.Forms.frmKeyBoadOSK KeyBoadOSK { get; set; }

        public UCkeypad KeyPad { get; set; }

        public TextBoxPOS_OSK()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
        }

        //protected override void OnEnter(EventArgs e)
        //{
        //    if (KeyBoadOSK!=null)
        //    {
        //        KeyBoadOSK.showKeypad();
        //    }
        //    this.BackColor = System.Drawing.Color.White;
        //    base.OnEnter(e);
        //}
        protected override void OnClick(EventArgs e)
        {
            if (KeyBoadOSK != null)
            {
                KeyBoadOSK.showKeypad();
            }
            if (KeyPad != null)
            {
                KeyPad.txtResult = this;
            }
            this.BackColor = System.Drawing.Color.White;
            this.SelectAll();
            base.OnClick(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }
    }
}