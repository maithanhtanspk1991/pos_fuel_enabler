﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmViewDetailDeptAccount : Form
    {
        private string _nameClass = "POS::Forms::frmViewDetailDeptAccount::";
        private int CustomerID;
        private Connection.ReadDBConfig mReadDBConfig = new Connection.ReadDBConfig();
        private double dblSubtotal;
        private double dblTotalAmount;
        private string dtServe;
        private int intTypePrint = 0;
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private int orderID;
        private string[] strDate;
        private string strMemberNo = string.Empty;
        private string strName = string.Empty;
        private string[] strOrder;
        private string strTableID;
        Class.ReadConfig mReadConfig;
        public frmViewDetailDeptAccount(Connection.Connection mConnection, Class.MoneyFortmat money, int customerID, Class.ReadConfig readConfig)
        {
            InitializeComponent();
            SetMultiLanguage();
            mReadConfig = readConfig;
            this.mConnection = mConnection;
            mSetting = new Class.Setting();
            mPrinter = new Printer();
            this.money = money;
            this.CustomerID = customerID;
            dtpFrom.Value = DateTime.Now.AddMonths(-1);
            dtpTo.Value = DateTime.Now;
            ucLimit.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit1_EventPage);
            VisibleButton();
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnView.Text = "Xem";
                btnPrint.Text = "In";
                btnBack.Text = "Quay lại";
                label1.Text = "Tổng";
                btnRefresh.Text = "Xem";
                label6.Text = "TỪ";
                label8.Text="ĐẾN";
                columnHeader1.Text = "Mã order";
                colDate.Text = "Ngày";
                columnHeader3.Text = "Tên nhân viên";
                colSubtotal.Text = "Thanh toán";
                colAccount.Text = "Tài khoản";
                columnHeader4.Text = "Số xe";
                return;
            }
        }

        private void VisibleButton()
        {
            VisibleButton(btnPrint, mReadConfig.IsPrinterAccount);
        }

        private void VisibleButton(Button btn, bool value)
        {
            btn.Visible = value;
        }

        public double GetTotal()
        {
            double result = 0;
            try
            {
                string strDateTo = GetDateString(dtpTo.Value);
                string strDateFrom = GetDateString(dtpFrom.Value);
                mConnection.Open();
                string sql = "SELECT Sum(account) as Total " +
                            "from ( " +
                            "SELECT 0 as bkId,OD.orderID,OD.tableID,OD.ts,S.Name,OD.account,IF(OD.staffID = 0 , 'Manager' ,S.Name) AS EmployeeID,OD.Subtotal AS Subtotal " +
                            ", (select CarNumber from managecar mc Where mc.id = OD.managecar) as CarNumber " +
                            "FROM ordersdaily OD left join staffs S on S.staffID = OD.staffID " +
                            "where OD.custID = " + CustomerID + " AND date(OD.ts)>=date('" + strDateFrom + "') AND date(OD.ts)<=date('" + strDateTo + "') " +
                            "group by OD.ts ASC " +
                            "UNION ALL " +
                            "SELECT OA.bkId,OA.orderID,OA.tableID,OA.ts,S.Name,OA.account,IF(OA.staffID = 0 , 'Manager' ,S.Name) AS EmployeeID,OA.Subtotal AS Subtotal " +
                            ", (select CarNumber from managecar mc Where mc.id = OA.managecar) as CarNumber " +
                            "FROM ordersall OA left join staffs S on S.staffID = OA.staffID " +
                            "where OA.custID = " + CustomerID + " AND date(OA.ts)>=date('" + strDateFrom + "') AND date(OA.ts)<=date('" + strDateTo + "') " +
                            " group by OA.ts ASC " +
                            ")AS S ";
                result = Convert.ToDouble(mConnection.Select(sql).Rows[0][0]);
                lbSubtotal.Text = money.Format2(result);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "GetTotal::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            return result;
        }

        public void ViewDetailAccountPay()
        {
            try
            {
                lvAccountDetail.Items.Clear();
                string strDateTo = GetDateString(dtpTo.Value);
                string strDateFrom = GetDateString(dtpFrom.Value);
                mConnection.Open();
                string sql = "SELECT bkId,orderID,tableID,ts AS DateDept,Name,account,EmployeeID,subtotal,CarNumber " +
                            "from ( " +
                            "SELECT 0 as bkId,OD.orderID,OD.tableID,OD.ts,S.Name,OD.account,IF(OD.staffID = 0 , 'Manager' ,S.Name) AS EmployeeID,OD.Subtotal AS Subtotal " +
                            ", (select CarNumber from managecar mc Where mc.id = OD.managecar) as CarNumber " +
                            "FROM ordersdaily OD left join staffs S on S.staffID = OD.staffID " +
                            "where OD.custID = " + CustomerID + " AND date(OD.ts)>=date('" + strDateFrom + "') AND date(OD.ts)<=date('" + strDateTo + "') " +
                            "group by OD.ts ASC " +
                            "UNION ALL " +
                            "SELECT OA.bkId,OA.orderID,OA.tableID,OA.ts,S.Name,OA.account,IF(OA.staffID = 0 , 'Manager' ,S.Name) AS EmployeeID,OA.Subtotal AS Subtotal " +
                            ", (select CarNumber from managecar mc Where mc.id = OA.managecar) as CarNumber " +
                            "FROM ordersall OA left join staffs S on S.staffID = OA.staffID " +
                            "where OA.custID = " + CustomerID + " AND date(OA.ts)>=date('" + strDateFrom + "') AND date(OA.ts)<=date('" + strDateTo + "') " +
                            " group by OA.ts ASC " +
                            ")AS S ";
                if (ucLimit.mLimit.IsLimit)
                {
                    BusinessObject.BOLimit.GetLimit(sql, ucLimit.mLimit);
                    sql += "order by DateDept ASC";
                    sql += ucLimit.mLimit.SqlLimit;
                }
                sql += ";";
                ucLimit.PageNumReload();
                System.Data.DataTable dt = mConnection.Select(sql);
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    ListViewItem li = new ListViewItem(row["orderID"].ToString());
                    li.SubItems.Add(Convert.ToDateTime(row["DateDept"]).ToString("dd/MM/yyyy HH:mm:ss"));
                    li.SubItems.Add(row["EmployeeID"].ToString());
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["subtotal"]))));
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format2(Convert.ToDouble(row["account"]))));
                    li.SubItems.Add(row["CarNumber"].ToString());
                    li.Tag = row["bkId"];
                    lvAccountDetail.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ViewDetailAccountPay::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            GetTotal();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (lvAccountDetail.SelectedIndices.Count > 0)
            {
                ListViewItem li = lvAccountDetail.SelectedItems[0];
                mPrinter = new Printer();
                PrintData pData = new PrintData(Convert.ToInt32(li.Tag), Convert.ToInt32(li.SubItems[0].Text));
                intTypePrint = 0;//print order history
                mPrinter.Tag = pData;
                mPrinter.SetPrinterName(mReadDBConfig.BillPrinter.ToString());
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                mPrinter.Print();
            }
        }

        private void btnPrintAllList_Click(object sender, EventArgs e)
        {
            if (lvAccountDetail.Items.Count > 0)
            {
                intTypePrint = 1;//print all list
                mPrinter = new Printer();
                mPrinter.SetPrinterName(mReadDBConfig.BillPrinter.ToString());
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                mPrinter.Print();
            }
        }

        private void btnPrintDetailAllList_Click(object sender, EventArgs e)
        {
            if (lvAccountDetail.Items.Count > 0)
            {
                intTypePrint = 2;//print all list detail
                mPrinter = new Printer();
                mPrinter.SetPrinterName(mReadDBConfig.BillPrinter.ToString());
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                mPrinter.Print();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ViewDetailAccountPay();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (lvAccountDetail.SelectedIndices.Count > 0)
            {
                ListViewItem li = lvAccountDetail.SelectedItems[0];
                frmViewDetailTaxInvoice frm = new frmViewDetailTaxInvoice(mConnection, money);
                frm.LoadList(Convert.ToInt16(li.Tag), Convert.ToInt16(li.SubItems[0].Text));
                frm.ShowDialog();
            }
        }

        private void frmViewDetailDeptAccount_Load(object sender, EventArgs e)
        {
        }

        private string GetDateString(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        private void lstAccountDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lvAccountDetail.SelectedItems.Count > 0)
                {
                    strOrder = lvAccountDetail.SelectedItems[0].Tag.ToString().Split('-');
                    orderID = Convert.ToInt32(strOrder[0]);
                    strTableID = lvAccountDetail.SelectedItems[0].SubItems[1].Text;
                    dtServe = Convert.ToDateTime(lvAccountDetail.SelectedItems[0].SubItems[2].Text).ToString();
                    //mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDetailAccount);
                }
            }
            catch
            {
            }
        }

        private void PrintAllDetail(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            dblSubtotal = 0;
            try
            {
                //=============================================
                System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
                float l_y = 0;
                string header = "";
                string bankCode = "";
                string address = "";
                string tell = "";

                string website = "";
                string thankyou = "";
                try
                {
                    header = mReadDBConfig.Header1.ToString();
                    bankCode = mReadDBConfig.Header2.ToString();
                    address = mReadDBConfig.Header3.ToString();
                    tell = mReadDBConfig.Header4.ToString();

                    website = mReadDBConfig.FootNode1.ToString();
                    thankyou = mReadDBConfig.FootNode2.ToString();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("printBuilt(Config):::" + ex.Message);
                }

                l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 18), l_y, 2);
                l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
                l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
                l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

                l_y += 100;

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += 100;
                string sBuilt = "ORDER HISTORY";
                l_y = mPrinter.DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);

                l_y += 100;

                DateTime dtFrom = Convert.ToDateTime(dtpFrom.Value.ToString());
                DateTime dtTo = Convert.ToDateTime(dtpTo.Value.ToString());

                mPrinter.DrawString("Member No:", e, font11, l_y, 1);
                l_y = mPrinter.DrawString(strMemberNo, e, font11, l_y, 3);

                mPrinter.DrawString("Name:", e, font11, l_y, 1);
                l_y = mPrinter.DrawString(strName, e, font11, l_y, 3);

                mPrinter.DrawString("FROM :" + dtFrom.Day + "/" + dtFrom.Month + "/" + dtFrom.Year, e, font11, l_y, 1);
                l_y = mPrinter.DrawString("TO :" + dtTo.Day + "/" + dtTo.Month + "/" + dtTo.Year, e, font11, l_y, 3);

                foreach (ListViewItem items in lvAccountDetail.Items)
                {
                    try
                    {
                        PrintData pData = new PrintData(Convert.ToInt16(items.Tag.ToString()), Convert.ToInt16(items.SubItems[0].Text.ToString()));

                        mPrinter.Tag = pData;
                        PrintData data = (PrintData)mPrinter.Tag;
                        string sql = "";
                        string sqlDetail = "";
                        if (data.BkId > 0)
                        {
                            sql = "select * from ordersall where bkId=" + data.BkId;
                            sqlDetail =
                                "select " +
                                    "ol.subTotal," +
                                    "ol.dynID," +
                                    "ol.dynID," +
                                    "ol.itemID," +
                                    "ol.optionID," +
                                    "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                                    "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),'Dym Item')) as itemDesc," +
                                    "ol.qty " +
                                "from ordersallline ol " +
                                "where ol.bkId=" + data.BkId;
                        }
                        else
                        {
                            sql = "select * from ordersdaily where orderID=" + data.OrderID;
                            sqlDetail =
                                "select " +
                                    "ol.subTotal," +
                                    "ol.dynID," +
                                    "ol.dynID," +
                                    "ol.itemID," +
                                    "ol.optionID," +
                                    "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                                    "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                                    "ol.qty " +
                                "from ordersdailyline ol " +
                                "where ol.orderID=" + data.OrderID;
                        }
                        DataTable tbl = mConnection.Select(sql);
                        Class.ProcessOrderNew.Order order = new Class.ProcessOrderNew.Order();
                        if (tbl.Rows.Count > 0)
                        {
                            order.TableID = tbl.Rows[0]["tableID"].ToString();
                            order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                            order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
                            order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                            order.Deposit = Convert.ToDouble(tbl.Rows[0]["deposit"]);
                            order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                            order.Customer.CustID = Convert.ToInt16(tbl.Rows[0]["custID"]);
                            order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                        l_y += 100;
                        mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                        mPrinter.DrawString("Date", e, font11, l_y, 1);
                        l_y = mPrinter.DrawString("SUBTOTAL", e, font11, l_y, 3);
                        DateTime dateTime = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                        mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year, e, font11, l_y, 1);
                        l_y = mPrinter.DrawString(money.Format2(order.SubTotal), e, font11, l_y, 3);

                        dblSubtotal += Convert.ToDouble(order.SubTotal);

                        List<Class.ItemOrderK> list = new List<Class.ItemOrderK>();
                        DataTable tblListItem = mConnection.Select(sqlDetail);
                        foreach (DataRow row in tblListItem.Rows)
                        {
                            if (row["dynID"].ToString() != "0")
                            {
                                list.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 1));
                            }
                            else
                            {
                                if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                                {
                                    list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                                }
                                else
                                {
                                    if (list.Count == 0 || list[list.Count - 1].GRShortCut == 1)
                                    {
                                        list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                                    }
                                    else
                                    {
                                        list[list.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < list.Count; i++)
                        {
                            for (int j = i + 1; j < list.Count; j++)
                            {
                                if (list[i].Compare(list[j]))
                                {
                                    list[i].AddSameItem(list[j]);
                                    list.RemoveAt(j);
                                    j--;
                                }
                            }
                        }
                        if (list.Count <= 0)
                        {
                            e.Cancel = true;
                            return;
                        }

                        double subTotal = 0;
                        foreach (Class.ItemOrderK item in list)
                        {
                            float yStart = l_y;
                            if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                            {
                                //l_y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                                //yStart = l_y;
                                l_y = mPrinter.DrawString(item.Qty + new String(' ', 3) + item.Name, e, new System.Drawing.Font("Arial", 8), l_y, 1);
                            }
                            foreach (Class.ItemOptionID option in item.Option)
                            {
                                l_y = mPrinter.DrawString(new String(' ', 6) + option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 1);
                                item.Price += option.Price;
                            }
                            if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                            {
                                mPrinter.DrawString(money.Format2(item.Price), e, new System.Drawing.Font("Arial", 9), yStart, 3);
                            }
                            subTotal += item.Price;
                        }
                        l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                System.Drawing.Font fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
                mPrinter.DrawString("Total:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(dblSubtotal), e, fontTotal, l_y, 3);
                l_y += 50;
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                l_y += 100;
                l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
                l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            }
            catch (Exception)
            {
                e.Cancel = true;
            }
        }

        private void PrintAllList(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            PrintData data = (PrintData)mPrinter.Tag;
            dblSubtotal = 0;
            try
            {
                //=============================================
                System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
                float l_y = 0;
                string header = "";
                string bankCode = "";
                string address = "";
                string tell = "";

                string website = "";
                string thankyou = "";
                try
                {
                    header = mReadDBConfig.Header1.ToString();
                    bankCode = mReadDBConfig.Header2.ToString();
                    address = mReadDBConfig.Header3.ToString();
                    tell = mReadDBConfig.Header4.ToString();

                    website = mReadDBConfig.FootNode1.ToString();
                    thankyou = mReadDBConfig.FootNode2.ToString();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("printBuilt(Config):::" + ex.Message);
                }

                l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 18), l_y, 2);
                l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
                l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
                l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

                l_y += 100;

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                string sBuilt = "BALANCE STATEMENT";

                l_y = mPrinter.DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);

                l_y += 100;

                mPrinter.DrawString("Date:", e, font11, l_y, 1);
                l_y = mPrinter.DrawString(strMemberNo, e, font11, l_y, 3);

                mPrinter.DrawString("Subtotal", e, font11, l_y, 2);
                l_y = mPrinter.DrawString("Account", e, font11, l_y, 3);

                mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                DateTime dt;
                foreach (ListViewItem item in lvAccountDetail.Items)
                {
                    try
                    {
                        strDate = item.SubItems[2].Text.Split(' ');

                        //dt = Convert.ToDateTime(item.SubItems[2].Text.ToString());
                        dblSubtotal += Convert.ToDouble(item.SubItems[5].Text.ToString());
                        //printer.DrawString(dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dt.Year.ToString(), e, font11, l_y, 1);
                        mPrinter.DrawString(strDate[0], e, font11, l_y, 1);
                        mPrinter.DrawString(item.SubItems[4].Text.ToString(), e, font11, l_y, 2);
                        l_y = mPrinter.DrawString(item.SubItems[5].Text.ToString(), e, font11, l_y, 3);
                    }
                    catch (Exception)
                    {
                    }
                }

                System.Drawing.Font fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
                mPrinter.DrawString("Total:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(String.Format("{0:0.00}", dblSubtotal), e, fontTotal, l_y, 3);

                l_y += 200;

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += 100;

                l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
                l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            }
            catch (Exception)
            {
                e.Cancel = true;
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (intTypePrint == 0)
            {
                PrintOrderHistory(sender, e);
            }
            else if (intTypePrint == 1)
            {
                PrintAllList(sender, e);
            }
            else if (intTypePrint == 2)
            {
                PrintAllDetail(sender, e);
            }
        }

        private void printDocument_PrintPageReportAccountPayment(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            l_y = mPrinter.DrawString("Account payment", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            //int i = 0;

            foreach (ListViewItem lvi in lvAccountDetail.Items)
            {
                mPrinter.DrawString(lvi.SubItems[0].Text, e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lvi.SubItems[1].Text, e, new Font("Arial", 10), l_y, 0);
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
                dblTotalAmount += money.getFortMat(lvi.SubItems[1].Text);
            }

            l_y += mPrinter.GetHeightPrinterLine() / 10;
            l_y = mPrinter.DrawLine("Total Pay:" + money.Format2(dblTotalAmount * 100), new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            l_y = mPrinter.DrawString("Total Pay:" + money.Format2(dblTotalAmount * 100), e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
            l_y = mPrinter.DrawLine("Total Pay:" + money.Format2(dblTotalAmount * 100), new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
        }

        private void PrintOrderHistory(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            PrintData data = (PrintData)mPrinter.Tag;
            try
            {
                //=============================================
                System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
                float l_y = 0;
                string header = "";
                string bankCode = "";
                string address = "";
                string tell = "";

                string website = "";
                string thankyou = "";
                try
                {
                    header = mReadDBConfig.Header1.ToString();
                    bankCode = mReadDBConfig.Header2.ToString();
                    address = mReadDBConfig.Header3.ToString();
                    tell = mReadDBConfig.Header4.ToString();

                    website = mReadDBConfig.FootNode1.ToString();
                    thankyou = mReadDBConfig.FootNode2.ToString();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("printBuilt(Config):::" + ex.Message);
                }

                l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 18), l_y, 2);
                l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
                l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
                l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

                l_y += mPrinter.GetHeightPrinterLine();

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                string sql = "";
                string sqlDetail = "";
                if (data.BkId > 0)
                {
                    sql = "select * from ordersall where bkId=" + data.BkId;
                    sqlDetail =
                        "select " +
                        //"o.clients," +
                            "ol.subTotal," +
                        //"o.discount," +
                        //"o.orderID," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                        //"if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID))) as itemDesc," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),'Dym Item')) as itemDesc," +
                            "ol.qty " +
                        //"from ordersall o inner join ordersallline ol on o.orderID=ol.orderID " +
                        "from ordersallline ol " +
                        "where ol.bkId=" + data.BkId;
                }
                else
                {
                    sql = "select * from ordersdaily where orderID=" + data.OrderID;
                    sqlDetail =
                        "select " +
                        //"o.clients," +
                            "ol.subTotal," +
                        //"o.discount," +
                        //"o.orderID," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "ol.qty " +
                        //"from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + data.OrderID;
                }
                DataTable tbl = mConnection.Select(sql);
                Class.ProcessOrderNew.Order order = new Class.ProcessOrderNew.Order();

                if (tbl.Rows.Count > 0)
                {
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    //order.Deposit = Convert.ToDouble(tbl.Rows[0]["deposit"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Tendered = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.Customer.CustID = Convert.ToInt16(tbl.Rows[0]["custID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
                l_y += mPrinter.GetHeightPrinterLine();
                DateTime dateTime = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
                if (!order.TableID.Contains("TKA-") && order.TableID != "")
                {
                    mPrinter.DrawString("TABLE# " + order.TableID, e, font11, l_y, 3);
                }
                l_y = mPrinter.DrawString("ORDER# " + order.OrderID, e, font11, l_y, 1);

                List<Class.ItemOrderK> list = new List<Class.ItemOrderK>();
                DataTable tblListItem = mConnection.Select(sqlDetail);
                foreach (DataRow row in tblListItem.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        list.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 1));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                        }
                        else
                        {
                            if (list.Count == 0 || list[list.Count - 1].GRShortCut == 1)
                            {
                                list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                            }
                            else
                            {
                                list[list.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if (list[i].Compare(list[j]))
                        {
                            list[i].AddSameItem(list[j]);
                            list.RemoveAt(j);
                            j--;
                        }
                    }
                }
                if (list.Count <= 0)
                {
                    e.Cancel = true;
                    return;
                }

                l_y += mPrinter.GetHeightPrinterLine();
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine();
                string sBuilt = "ORDER HISTORY";

                l_y = mPrinter.DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);

                l_y += mPrinter.GetHeightPrinterLine();

                double subTotal = 0;
                foreach (Class.ItemOrderK item in list)
                {
                    float yStart = l_y;
                    if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                    {
                        l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                        yStart = l_y;
                        l_y = mPrinter.DrawString(item.Qty + new String(' ', 3) + item.Name, e, new System.Drawing.Font("Arial", 8), l_y, 1);
                    }
                    foreach (Class.ItemOptionID option in item.Option)
                    {
                        l_y = mPrinter.DrawString(new String(' ', 6) + option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 1);
                        item.Price += option.Price;
                    }
                    if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                    {
                        mPrinter.DrawString(money.Format2(item.Price), e, new System.Drawing.Font("Arial", 11), yStart, 3);
                    }
                    subTotal += item.Price;
                }
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine() / 2;

                System.Drawing.Font fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
                mPrinter.DrawString("Total:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(order.SubTotal), e, fontTotal, l_y, 3);
                l_y += mPrinter.GetHeightPrinterLine() / 2;

                if (order.Discount > 0)
                {
                    l_y += mPrinter.GetHeightPrinterLine() / 2;
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Discount:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.Discount), e, fontTotal, l_y, 3);

                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Balance:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.SubTotal - order.Discount), e, fontTotal, l_y, 3);
                }

                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("GST(included in total):", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format2((order.SubTotal - order.Discount) / 11), e, fontTotal, l_y, 3);

                if (order.Deposit > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Deposit:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.Deposit), e, fontTotal, l_y, 3);
                }
                double balanceAccount = 0;
                if (order.Card > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Card:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.Card), e, fontTotal, l_y, 3);
                }
                try
                {
                    if (order.Customer.CustID != 0)
                    {
                        Connection.Connection conn = new Connection.Connection();
                        DataTable dtSourceCustomer = new DataTable();
                        dtSourceCustomer = conn.Select("SELECT c.Name, c.FirstName,c.memberNo,c.debt FROM customers c WHERE c.custID = " + order.Customer.CustID);
                        l_y = mPrinter.DrawString("ACC      # " + dtSourceCustomer.Rows[0]["memberNo"].ToString().Substring(0, 3) + "..." +
                            dtSourceCustomer.Rows[0]["memberNo"].ToString().Substring(dtSourceCustomer.Rows[0]["memberNo"].ToString().Length - 3, 3), e, font11, l_y, 1);
                        l_y = mPrinter.DrawString("ACC NAME # " + dtSourceCustomer.Rows[0]["FirstName"].ToString().ToUpper(), e, font11, l_y, 1);
                        balanceAccount = Convert.ToDouble(dtSourceCustomer.Rows[0]["debt"]);
                    }
                }
                catch
                {
                }

                if (order.Account > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Account:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.Account), e, fontTotal, l_y, 3);

                    fontTotal = new System.Drawing.Font("Arial", 11);
                }

                if (order.Tendered > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Cash:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(order.Tendered) + "", e, fontTotal, l_y, 3);
                }

                double change = order.Tendered + order.Deposit + order.Card - (order.SubTotal - order.Discount);

                if (change > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Change:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(money.Format2(change) + "", e, fontTotal, l_y, 3);
                }

                l_y += mPrinter.GetHeightPrinterLine() * 2;

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine();

                l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
                l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            }
            catch (Exception)
            {
                e.Cancel = true;
            }
        }

        private string SplitDateFrom(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "00:00:00";
        }

        private void ucLimit1_EventPage(DataObject.Limit limit)
        {
            ViewDetailAccountPay();
        }
        private class PrintData
        {
            public PrintData(int bkId, int orderID)
            {
                BkId = bkId;
                OrderID = orderID;
            }

            public int BkId { get; set; }

            public int OrderID { get; set; }
        }
    }
}