﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Properties;

namespace POS.FindObject
{
    public partial class frmFindCustomerByPhone : Form
    {
        private Connection.Connection conn;

        public string CustomerName { get; set; }

        public string CustomerCode { get; set; }

        public int CustomerID { get; set; }

        public int CustomerType { get; set; }

        public frmFindCustomerByPhone()
        {
            InitializeComponent();
        }

        public frmFindCustomerByPhone(Connection.Connection conn)
        {
            InitializeComponent();
            this.conn = conn;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                btnAccept.Enabled = false;
                String strSQL = "SELECT custID,memberNo,Name,IsPickup,IsDelivery FROM customers where mobile = '" + txtPhoneNumber.Text + "'";
                DataTable dtSource = conn.Select(strSQL);
                if (dtSource.Rows.Count == 0)
                {
                    frmAddCustomer frm = new frmAddCustomer(conn, txtPhoneNumber.Text);
                    DialogResult dlg = frm.ShowDialog();
                    if (dlg == DialogResult.OK)
                    {
                        CustomerName = frm.CustomerName;
                        CustomerCode = frm.CustomerCode;
                        CustomerID = frm.CustomerID;
                        CustomerType = frm.CustomerType;
                        this.DialogResult = DialogResult.OK;
                    }
                }
                else
                {
                    CustomerName = dtSource.Rows[0]["Name"].ToString();
                    CustomerCode = dtSource.Rows[0]["memberNo"].ToString();
                    CustomerID = Convert.ToInt32(dtSource.Rows[0]["custID"]);
                    txtCustomerName.Text = CustomerName;
                    if (Convert.ToInt32(dtSource.Rows[0]["IsDelivery"]) == 1)
                    {
                        CustomerType = 1;
                        EnableButtonType(true, false);
                    }
                    else if (Convert.ToInt32(dtSource.Rows[0]["IsPickup"]) == 1)
                    {
                        CustomerType = 2;
                        EnableButtonType(false, true);
                    }
                    else
                    {
                        CustomerType = 3;
                        EnableButtonType(false, false);
                    }
                    btnAccept.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("form find customer:::btnSearch_Click:::" + ex.Message);
            }
        }

        private void EnableButtonType(bool blnDelivery, bool blnPickup)
        {
            try
            {
                if (blnDelivery)
                {
                    btnDelivery.Image = (Bitmap)Resources.checkbox;
                    btnDelivery.Tag = "true";
                }
                else
                {
                    btnDelivery.Image = (Bitmap)Resources.UnCheck;
                    btnDelivery.Tag = "false";
                }
                if (blnPickup)
                {
                    btnPickup.Image = (Bitmap)Resources.checkbox;
                    btnPickup.Tag = "true";
                }
                else
                {
                    btnPickup.Image = (Bitmap)Resources.UnCheck;
                    btnPickup.Tag = "false";
                }
                btnDelivery.Enabled = true;
                btnPickup.Enabled = true;
            }
            catch (Exception)
            {
            }
        }

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(btnDelivery.Tag))
                {
                    UpdateTypeCustomer(0, 1, CustomerID);
                    EnableButtonType(false, false);
                }
                else
                {
                    UpdateTypeCustomer(1, 0, CustomerID);
                    EnableButtonType(true, false);
                }
            }
            catch (Exception)
            {
            }
        }

        private void UpdateTypeCustomer(int Delivery, int Pickup, int CustID)
        {
            try
            {
                conn.Open();
                string strSQL = "UPDATE customers SET IsPickup =" + Pickup + ",IsDelivery = " + Delivery + " WHERE custID =" + CustID;
                conn.ExecuteNonQuery(strSQL);
                if (Delivery == 1)
                {
                    CustomerType = 1;
                }
                else if (Pickup == 1)
                {
                    CustomerType = 2;
                }
                else
                {
                    CustomerType = 3;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnPickup_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(btnPickup.Tag))
                {
                    UpdateTypeCustomer(0, 0, CustomerID);
                    EnableButtonType(false, false);
                }
                else
                {
                    UpdateTypeCustomer(0, 1, CustomerID);
                    EnableButtonType(false, true);
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}