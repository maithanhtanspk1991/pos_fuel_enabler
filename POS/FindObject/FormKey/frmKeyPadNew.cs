﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS.FormKey
{
    public partial class frmKeyPadNew : Form
    {
        private TextBox mTextBox;
        private bool mIsFirstLoad = true;

        public frmKeyPadNew(TextBox textBox, bool isLockDot)
        {
            InitializeComponent();
            mTextBox = textBox;
            btndot.Enabled = !isLockDot;
            Point p = GetPositionInForm(textBox);
            this.Location = p;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            if (mIsFirstLoad)
            {
                mIsFirstLoad = false;
                mTextBox.Text = "";
            }
            Button btn = (Button)sender;
            mTextBox.Text += btn.Text;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (mTextBox.Text.Length == 0)
            {
                mTextBox.Text += "0.";
            }
            else
            {
                if (mTextBox.Text.Contains('.'))
                {
                    return;
                }
                else
                {
                    mTextBox.Text += ".";
                }
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            mTextBox.Text = "";
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (mTextBox.Text.Length > 0)
            {
                string text = mTextBox.Text;
                mTextBox.Text = text.Remove(text.Length - 1, 1);
            }
        }

        public Point GetPositionInForm(Control ctrl)
        {
            Point p = new Point();
            p = ctrl.Parent.PointToScreen(ctrl.Location);
            p.X = p.X + (ctrl.Width - this.Width) / 2;
            p.Y = p.Y + ctrl.Height;
            return p;
        }

        private void frmKeyPadNew_Load(object sender, EventArgs e)
        {
        }
    }
}