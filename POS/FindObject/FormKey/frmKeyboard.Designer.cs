﻿namespace POS.FormKey
{
    partial class frmKeyboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btntrondong = new System.Windows.Forms.Button();
            this.btntronmo = new System.Windows.Forms.Button();
            this.btnsao = new System.Windows.Forms.Button();
            this.btnmu = new System.Windows.Forms.Button();
            this.btnbang = new System.Windows.Forms.Button();
            this.btnphantram = new System.Windows.Forms.Button();
            this.btndola = new System.Windows.Forms.Button();
            this.btnthang = new System.Windows.Forms.Button();
            this.btnacong = new System.Windows.Forms.Button();
            this.btnthan = new System.Windows.Forms.Button();
            this.btnphay = new System.Windows.Forms.Button();
            this.btnchia = new System.Windows.Forms.Button();
            this.btnchamphay = new System.Windows.Forms.Button();
            this.btnhaicham = new System.Windows.Forms.Button();
            this.btnvuongdong = new System.Windows.Forms.Button();
            this.btndauhoi = new System.Windows.Forms.Button();
            this.btncong = new System.Windows.Forms.Button();
            this.btntru = new System.Windows.Forms.Button();
            this.btnvuongmo = new System.Windows.Forms.Button();
            this.btnnhohon = new System.Windows.Forms.Button();
            this.btnhoac = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.btncapslock = new System.Windows.Forms.Button();
            this.btnshift = new System.Windows.Forms.Button();
            this.txtresult = new System.Windows.Forms.TextBox();
            this.btnspace = new System.Windows.Forms.Button();
            this.btnm = new System.Windows.Forms.Button();
            this.btnn = new System.Windows.Forms.Button();
            this.btnb = new System.Windows.Forms.Button();
            this.btnv = new System.Windows.Forms.Button();
            this.btnc = new System.Windows.Forms.Button();
            this.btnx = new System.Windows.Forms.Button();
            this.btnz = new System.Windows.Forms.Button();
            this.btnl = new System.Windows.Forms.Button();
            this.btnk = new System.Windows.Forms.Button();
            this.btnj = new System.Windows.Forms.Button();
            this.btnh = new System.Windows.Forms.Button();
            this.btng = new System.Windows.Forms.Button();
            this.btnf = new System.Windows.Forms.Button();
            this.btnd = new System.Windows.Forms.Button();
            this.btns = new System.Windows.Forms.Button();
            this.btna = new System.Windows.Forms.Button();
            this.btnenter = new System.Windows.Forms.Button();
            this.btnp = new System.Windows.Forms.Button();
            this.btno = new System.Windows.Forms.Button();
            this.btni = new System.Windows.Forms.Button();
            this.btnu = new System.Windows.Forms.Button();
            this.btny = new System.Windows.Forms.Button();
            this.btnt = new System.Windows.Forms.Button();
            this.btnr = new System.Windows.Forms.Button();
            this.btne = new System.Windows.Forms.Button();
            this.btnw = new System.Windows.Forms.Button();
            this.btnq = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btnnhondong = new System.Windows.Forms.Button();
            this.btnnhonmo = new System.Windows.Forms.Button();
            this.btndot = new System.Windows.Forms.Button();
            this.btndel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btntrondong
            // 
            this.btntrondong.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btntrondong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntrondong.Location = new System.Drawing.Point(565, 59);
            this.btntrondong.Margin = new System.Windows.Forms.Padding(1);
            this.btntrondong.Name = "btntrondong";
            this.btntrondong.Size = new System.Drawing.Size(60, 60);
            this.btntrondong.TabIndex = 142;
            this.btntrondong.Text = ")";
            this.btntrondong.UseVisualStyleBackColor = false;
            this.btntrondong.Click += new System.EventHandler(this.btntrondong_Click);
            // 
            // btntronmo
            // 
            this.btntronmo.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btntronmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntronmo.Location = new System.Drawing.Point(503, 59);
            this.btntronmo.Margin = new System.Windows.Forms.Padding(1);
            this.btntronmo.Name = "btntronmo";
            this.btntronmo.Size = new System.Drawing.Size(60, 60);
            this.btntronmo.TabIndex = 141;
            this.btntronmo.Text = "(";
            this.btntronmo.UseVisualStyleBackColor = false;
            this.btntronmo.Click += new System.EventHandler(this.btntronmo_Click);
            // 
            // btnsao
            // 
            this.btnsao.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnsao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsao.Location = new System.Drawing.Point(441, 59);
            this.btnsao.Margin = new System.Windows.Forms.Padding(1);
            this.btnsao.Name = "btnsao";
            this.btnsao.Size = new System.Drawing.Size(60, 60);
            this.btnsao.TabIndex = 140;
            this.btnsao.Text = "*";
            this.btnsao.UseVisualStyleBackColor = false;
            this.btnsao.Click += new System.EventHandler(this.btnsao_Click);
            // 
            // btnmu
            // 
            this.btnmu.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnmu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmu.Location = new System.Drawing.Point(379, 59);
            this.btnmu.Margin = new System.Windows.Forms.Padding(1);
            this.btnmu.Name = "btnmu";
            this.btnmu.Size = new System.Drawing.Size(60, 60);
            this.btnmu.TabIndex = 139;
            this.btnmu.Text = "^";
            this.btnmu.UseVisualStyleBackColor = false;
            this.btnmu.Click += new System.EventHandler(this.btnmu_Click);
            // 
            // btnbang
            // 
            this.btnbang.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnbang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbang.Location = new System.Drawing.Point(317, 59);
            this.btnbang.Margin = new System.Windows.Forms.Padding(1);
            this.btnbang.Name = "btnbang";
            this.btnbang.Size = new System.Drawing.Size(60, 60);
            this.btnbang.TabIndex = 138;
            this.btnbang.Text = "=";
            this.btnbang.UseVisualStyleBackColor = false;
            this.btnbang.Click += new System.EventHandler(this.btnbang_Click);
            // 
            // btnphantram
            // 
            this.btnphantram.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnphantram.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphantram.Location = new System.Drawing.Point(255, 59);
            this.btnphantram.Margin = new System.Windows.Forms.Padding(1);
            this.btnphantram.Name = "btnphantram";
            this.btnphantram.Size = new System.Drawing.Size(60, 60);
            this.btnphantram.TabIndex = 137;
            this.btnphantram.Text = "%";
            this.btnphantram.UseVisualStyleBackColor = false;
            this.btnphantram.Click += new System.EventHandler(this.btnphantram_Click);
            // 
            // btndola
            // 
            this.btndola.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btndola.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndola.Location = new System.Drawing.Point(193, 59);
            this.btndola.Margin = new System.Windows.Forms.Padding(1);
            this.btndola.Name = "btndola";
            this.btndola.Size = new System.Drawing.Size(60, 60);
            this.btndola.TabIndex = 136;
            this.btndola.Text = "$";
            this.btndola.UseVisualStyleBackColor = false;
            this.btndola.Click += new System.EventHandler(this.btndola_Click);
            // 
            // btnthang
            // 
            this.btnthang.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnthang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnthang.Location = new System.Drawing.Point(131, 59);
            this.btnthang.Margin = new System.Windows.Forms.Padding(1);
            this.btnthang.Name = "btnthang";
            this.btnthang.Size = new System.Drawing.Size(60, 60);
            this.btnthang.TabIndex = 135;
            this.btnthang.Text = "#";
            this.btnthang.UseVisualStyleBackColor = false;
            this.btnthang.Click += new System.EventHandler(this.btnthang_Click);
            // 
            // btnacong
            // 
            this.btnacong.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnacong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnacong.Location = new System.Drawing.Point(69, 59);
            this.btnacong.Margin = new System.Windows.Forms.Padding(1);
            this.btnacong.Name = "btnacong";
            this.btnacong.Size = new System.Drawing.Size(60, 60);
            this.btnacong.TabIndex = 134;
            this.btnacong.Text = "@";
            this.btnacong.UseVisualStyleBackColor = false;
            this.btnacong.Click += new System.EventHandler(this.btnacong_Click);
            // 
            // btnthan
            // 
            this.btnthan.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnthan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnthan.Location = new System.Drawing.Point(7, 59);
            this.btnthan.Margin = new System.Windows.Forms.Padding(1);
            this.btnthan.Name = "btnthan";
            this.btnthan.Size = new System.Drawing.Size(60, 60);
            this.btnthan.TabIndex = 133;
            this.btnthan.Text = "!";
            this.btnthan.UseVisualStyleBackColor = false;
            this.btnthan.Click += new System.EventHandler(this.btnthan_Click);
            // 
            // btnphay
            // 
            this.btnphay.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnphay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphay.Location = new System.Drawing.Point(441, 245);
            this.btnphay.Margin = new System.Windows.Forms.Padding(1);
            this.btnphay.Name = "btnphay";
            this.btnphay.Size = new System.Drawing.Size(60, 60);
            this.btnphay.TabIndex = 132;
            this.btnphay.Text = ",";
            this.btnphay.UseVisualStyleBackColor = false;
            this.btnphay.Click += new System.EventHandler(this.btnphay_Click);
            // 
            // btnchia
            // 
            this.btnchia.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnchia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchia.Location = new System.Drawing.Point(503, 245);
            this.btnchia.Margin = new System.Windows.Forms.Padding(1);
            this.btnchia.Name = "btnchia";
            this.btnchia.Size = new System.Drawing.Size(60, 60);
            this.btnchia.TabIndex = 131;
            this.btnchia.Text = "/";
            this.btnchia.UseVisualStyleBackColor = false;
            this.btnchia.Click += new System.EventHandler(this.btnchia_Click);
            // 
            // btnchamphay
            // 
            this.btnchamphay.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnchamphay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchamphay.Location = new System.Drawing.Point(565, 183);
            this.btnchamphay.Margin = new System.Windows.Forms.Padding(1);
            this.btnchamphay.Name = "btnchamphay";
            this.btnchamphay.Size = new System.Drawing.Size(60, 60);
            this.btnchamphay.TabIndex = 130;
            this.btnchamphay.Text = ";";
            this.btnchamphay.UseVisualStyleBackColor = false;
            this.btnchamphay.Click += new System.EventHandler(this.btnchamphay_Click);
            // 
            // btnhaicham
            // 
            this.btnhaicham.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnhaicham.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhaicham.Location = new System.Drawing.Point(565, 245);
            this.btnhaicham.Margin = new System.Windows.Forms.Padding(1);
            this.btnhaicham.Name = "btnhaicham";
            this.btnhaicham.Size = new System.Drawing.Size(60, 60);
            this.btnhaicham.TabIndex = 129;
            this.btnhaicham.Text = ":";
            this.btnhaicham.UseVisualStyleBackColor = false;
            this.btnhaicham.Click += new System.EventHandler(this.btnhaicham_Click);
            // 
            // btnvuongdong
            // 
            this.btnvuongdong.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnvuongdong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvuongdong.Location = new System.Drawing.Point(565, 307);
            this.btnvuongdong.Margin = new System.Windows.Forms.Padding(1);
            this.btnvuongdong.Name = "btnvuongdong";
            this.btnvuongdong.Size = new System.Drawing.Size(60, 60);
            this.btnvuongdong.TabIndex = 128;
            this.btnvuongdong.Text = "]";
            this.btnvuongdong.UseVisualStyleBackColor = false;
            this.btnvuongdong.Click += new System.EventHandler(this.btnvuongdong_Click);
            // 
            // btndauhoi
            // 
            this.btndauhoi.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btndauhoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndauhoi.Location = new System.Drawing.Point(503, 307);
            this.btndauhoi.Margin = new System.Windows.Forms.Padding(1);
            this.btndauhoi.Name = "btndauhoi";
            this.btndauhoi.Size = new System.Drawing.Size(60, 60);
            this.btndauhoi.TabIndex = 127;
            this.btndauhoi.Text = "?";
            this.btndauhoi.UseVisualStyleBackColor = false;
            this.btndauhoi.Click += new System.EventHandler(this.btndauhoi_Click);
            // 
            // btncong
            // 
            this.btncong.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btncong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncong.Location = new System.Drawing.Point(441, 307);
            this.btncong.Margin = new System.Windows.Forms.Padding(1);
            this.btncong.Name = "btncong";
            this.btncong.Size = new System.Drawing.Size(60, 60);
            this.btncong.TabIndex = 126;
            this.btncong.Text = "+";
            this.btncong.UseVisualStyleBackColor = false;
            this.btncong.Click += new System.EventHandler(this.btncong_Click);
            // 
            // btntru
            // 
            this.btntru.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btntru.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntru.Location = new System.Drawing.Point(193, 307);
            this.btntru.Margin = new System.Windows.Forms.Padding(1);
            this.btntru.Name = "btntru";
            this.btntru.Size = new System.Drawing.Size(60, 60);
            this.btntru.TabIndex = 125;
            this.btntru.Text = "-";
            this.btntru.UseVisualStyleBackColor = false;
            this.btntru.Click += new System.EventHandler(this.btntru_Click);
            // 
            // btnvuongmo
            // 
            this.btnvuongmo.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnvuongmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvuongmo.Location = new System.Drawing.Point(131, 307);
            this.btnvuongmo.Margin = new System.Windows.Forms.Padding(1);
            this.btnvuongmo.Name = "btnvuongmo";
            this.btnvuongmo.Size = new System.Drawing.Size(60, 60);
            this.btnvuongmo.TabIndex = 124;
            this.btnvuongmo.Text = "[";
            this.btnvuongmo.UseVisualStyleBackColor = false;
            this.btnvuongmo.Click += new System.EventHandler(this.btnvuongmo_Click);
            // 
            // btnnhohon
            // 
            this.btnnhohon.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnnhohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhohon.Location = new System.Drawing.Point(751, 59);
            this.btnnhohon.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhohon.Name = "btnnhohon";
            this.btnnhohon.Size = new System.Drawing.Size(60, 60);
            this.btnnhohon.TabIndex = 123;
            this.btnnhohon.Text = "HOME";
            this.btnnhohon.UseVisualStyleBackColor = false;
            this.btnnhohon.Click += new System.EventHandler(this.btnnhohon_Click);
            // 
            // btnhoac
            // 
            this.btnhoac.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnhoac.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhoac.Location = new System.Drawing.Point(813, 59);
            this.btnhoac.Margin = new System.Windows.Forms.Padding(1);
            this.btnhoac.Name = "btnhoac";
            this.btnhoac.Size = new System.Drawing.Size(60, 60);
            this.btnhoac.TabIndex = 122;
            this.btnhoac.Text = "END";
            this.btnhoac.UseVisualStyleBackColor = false;
            this.btnhoac.Click += new System.EventHandler(this.btnhoac_Click);
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.Location = new System.Drawing.Point(627, 121);
            this.btnclear.Margin = new System.Windows.Forms.Padding(1);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(60, 60);
            this.btnclear.TabIndex = 119;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btncapslock
            // 
            this.btncapslock.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btncapslock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncapslock.Location = new System.Drawing.Point(627, 183);
            this.btncapslock.Margin = new System.Windows.Forms.Padding(1);
            this.btncapslock.Name = "btncapslock";
            this.btncapslock.Size = new System.Drawing.Size(60, 60);
            this.btncapslock.TabIndex = 118;
            this.btncapslock.Text = "Caps Lock";
            this.btncapslock.UseVisualStyleBackColor = false;
            this.btncapslock.Click += new System.EventHandler(this.btncapslock_Click);
            // 
            // btnshift
            // 
            this.btnshift.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnshift.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshift.Location = new System.Drawing.Point(7, 307);
            this.btnshift.Margin = new System.Windows.Forms.Padding(1);
            this.btnshift.Name = "btnshift";
            this.btnshift.Size = new System.Drawing.Size(122, 60);
            this.btnshift.TabIndex = 117;
            this.btnshift.Text = "Shift";
            this.btnshift.UseVisualStyleBackColor = false;
            this.btnshift.Click += new System.EventHandler(this.btnshift_Click);
            // 
            // txtresult
            // 
            this.txtresult.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtresult.Location = new System.Drawing.Point(7, 6);
            this.txtresult.Name = "txtresult";
            this.txtresult.Size = new System.Drawing.Size(866, 47);
            this.txtresult.TabIndex = 116;
            this.txtresult.TextChanged += new System.EventHandler(this.txtresult_TextChanged);
            // 
            // btnspace
            // 
            this.btnspace.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnspace.Location = new System.Drawing.Point(255, 307);
            this.btnspace.Margin = new System.Windows.Forms.Padding(1);
            this.btnspace.Name = "btnspace";
            this.btnspace.Size = new System.Drawing.Size(184, 60);
            this.btnspace.TabIndex = 115;
            this.btnspace.UseVisualStyleBackColor = false;
            this.btnspace.Click += new System.EventHandler(this.btnspace_Click);
            // 
            // btnm
            // 
            this.btnm.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnm.Location = new System.Drawing.Point(379, 245);
            this.btnm.Margin = new System.Windows.Forms.Padding(1);
            this.btnm.Name = "btnm";
            this.btnm.Size = new System.Drawing.Size(60, 60);
            this.btnm.TabIndex = 114;
            this.btnm.Tag = "m";
            this.btnm.Text = "M";
            this.btnm.UseVisualStyleBackColor = false;
            this.btnm.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnn
            // 
            this.btnn.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn.Location = new System.Drawing.Point(317, 245);
            this.btnn.Margin = new System.Windows.Forms.Padding(1);
            this.btnn.Name = "btnn";
            this.btnn.Size = new System.Drawing.Size(60, 60);
            this.btnn.TabIndex = 113;
            this.btnn.Tag = "n";
            this.btnn.Text = "N";
            this.btnn.UseVisualStyleBackColor = false;
            this.btnn.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnb
            // 
            this.btnb.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnb.Location = new System.Drawing.Point(255, 245);
            this.btnb.Margin = new System.Windows.Forms.Padding(1);
            this.btnb.Name = "btnb";
            this.btnb.Size = new System.Drawing.Size(60, 60);
            this.btnb.TabIndex = 112;
            this.btnb.Tag = "b";
            this.btnb.Text = "B";
            this.btnb.UseVisualStyleBackColor = false;
            this.btnb.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnv
            // 
            this.btnv.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnv.Location = new System.Drawing.Point(193, 245);
            this.btnv.Margin = new System.Windows.Forms.Padding(1);
            this.btnv.Name = "btnv";
            this.btnv.Size = new System.Drawing.Size(60, 60);
            this.btnv.TabIndex = 111;
            this.btnv.Tag = "v";
            this.btnv.Text = "V";
            this.btnv.UseVisualStyleBackColor = false;
            this.btnv.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnc
            // 
            this.btnc.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnc.Location = new System.Drawing.Point(131, 245);
            this.btnc.Margin = new System.Windows.Forms.Padding(1);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(60, 60);
            this.btnc.TabIndex = 110;
            this.btnc.Tag = "c";
            this.btnc.Text = "C";
            this.btnc.UseVisualStyleBackColor = false;
            this.btnc.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnx
            // 
            this.btnx.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnx.Location = new System.Drawing.Point(69, 245);
            this.btnx.Margin = new System.Windows.Forms.Padding(1);
            this.btnx.Name = "btnx";
            this.btnx.Size = new System.Drawing.Size(60, 60);
            this.btnx.TabIndex = 109;
            this.btnx.Tag = "x";
            this.btnx.Text = "X";
            this.btnx.UseVisualStyleBackColor = false;
            this.btnx.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnz
            // 
            this.btnz.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnz.Location = new System.Drawing.Point(7, 245);
            this.btnz.Margin = new System.Windows.Forms.Padding(1);
            this.btnz.Name = "btnz";
            this.btnz.Size = new System.Drawing.Size(60, 60);
            this.btnz.TabIndex = 108;
            this.btnz.Tag = "z";
            this.btnz.Text = "Z";
            this.btnz.UseVisualStyleBackColor = false;
            this.btnz.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnl
            // 
            this.btnl.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnl.Location = new System.Drawing.Point(503, 183);
            this.btnl.Margin = new System.Windows.Forms.Padding(1);
            this.btnl.Name = "btnl";
            this.btnl.Size = new System.Drawing.Size(60, 60);
            this.btnl.TabIndex = 106;
            this.btnl.Tag = "l";
            this.btnl.Text = "L";
            this.btnl.UseVisualStyleBackColor = false;
            this.btnl.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnk
            // 
            this.btnk.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnk.Location = new System.Drawing.Point(441, 183);
            this.btnk.Margin = new System.Windows.Forms.Padding(1);
            this.btnk.Name = "btnk";
            this.btnk.Size = new System.Drawing.Size(60, 60);
            this.btnk.TabIndex = 105;
            this.btnk.Tag = "k";
            this.btnk.Text = "K";
            this.btnk.UseVisualStyleBackColor = false;
            this.btnk.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnj
            // 
            this.btnj.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnj.Location = new System.Drawing.Point(379, 183);
            this.btnj.Margin = new System.Windows.Forms.Padding(1);
            this.btnj.Name = "btnj";
            this.btnj.Size = new System.Drawing.Size(60, 60);
            this.btnj.TabIndex = 104;
            this.btnj.Tag = "j";
            this.btnj.Text = "J";
            this.btnj.UseVisualStyleBackColor = false;
            this.btnj.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnh
            // 
            this.btnh.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnh.Location = new System.Drawing.Point(317, 183);
            this.btnh.Margin = new System.Windows.Forms.Padding(1);
            this.btnh.Name = "btnh";
            this.btnh.Size = new System.Drawing.Size(60, 60);
            this.btnh.TabIndex = 103;
            this.btnh.Tag = "h";
            this.btnh.Text = "H";
            this.btnh.UseVisualStyleBackColor = false;
            this.btnh.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btng
            // 
            this.btng.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btng.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btng.Location = new System.Drawing.Point(255, 183);
            this.btng.Margin = new System.Windows.Forms.Padding(1);
            this.btng.Name = "btng";
            this.btng.Size = new System.Drawing.Size(60, 60);
            this.btng.TabIndex = 102;
            this.btng.Tag = "g";
            this.btng.Text = "G";
            this.btng.UseVisualStyleBackColor = false;
            this.btng.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnf
            // 
            this.btnf.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnf.Location = new System.Drawing.Point(193, 183);
            this.btnf.Margin = new System.Windows.Forms.Padding(1);
            this.btnf.Name = "btnf";
            this.btnf.Size = new System.Drawing.Size(60, 60);
            this.btnf.TabIndex = 101;
            this.btnf.Tag = "f";
            this.btnf.Text = "F";
            this.btnf.UseVisualStyleBackColor = false;
            this.btnf.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnd
            // 
            this.btnd.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnd.Location = new System.Drawing.Point(131, 183);
            this.btnd.Margin = new System.Windows.Forms.Padding(1);
            this.btnd.Name = "btnd";
            this.btnd.Size = new System.Drawing.Size(60, 60);
            this.btnd.TabIndex = 100;
            this.btnd.Tag = "d";
            this.btnd.Text = "D";
            this.btnd.UseVisualStyleBackColor = false;
            this.btnd.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btns
            // 
            this.btns.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btns.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btns.Location = new System.Drawing.Point(69, 183);
            this.btns.Margin = new System.Windows.Forms.Padding(1);
            this.btns.Name = "btns";
            this.btns.Size = new System.Drawing.Size(60, 60);
            this.btns.TabIndex = 99;
            this.btns.Tag = "s";
            this.btns.Text = "S";
            this.btns.UseVisualStyleBackColor = false;
            this.btns.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btna
            // 
            this.btna.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btna.Location = new System.Drawing.Point(7, 183);
            this.btna.Margin = new System.Windows.Forms.Padding(1);
            this.btna.Name = "btna";
            this.btna.Size = new System.Drawing.Size(60, 60);
            this.btna.TabIndex = 98;
            this.btna.Tag = "a";
            this.btna.Text = "A";
            this.btna.UseVisualStyleBackColor = false;
            this.btna.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnenter
            // 
            this.btnenter.BackColor = System.Drawing.Color.Green;
            this.btnenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenter.Location = new System.Drawing.Point(627, 245);
            this.btnenter.Margin = new System.Windows.Forms.Padding(1);
            this.btnenter.Name = "btnenter";
            this.btnenter.Size = new System.Drawing.Size(60, 60);
            this.btnenter.TabIndex = 97;
            this.btnenter.Text = "ENTER";
            this.btnenter.UseVisualStyleBackColor = false;
            this.btnenter.Click += new System.EventHandler(this.btnenter_Click);
            // 
            // btnp
            // 
            this.btnp.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnp.Location = new System.Drawing.Point(565, 121);
            this.btnp.Margin = new System.Windows.Forms.Padding(1);
            this.btnp.Name = "btnp";
            this.btnp.Size = new System.Drawing.Size(60, 60);
            this.btnp.TabIndex = 96;
            this.btnp.Tag = "p";
            this.btnp.Text = "P";
            this.btnp.UseVisualStyleBackColor = false;
            this.btnp.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btno
            // 
            this.btno.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btno.Location = new System.Drawing.Point(503, 121);
            this.btno.Margin = new System.Windows.Forms.Padding(1);
            this.btno.Name = "btno";
            this.btno.Size = new System.Drawing.Size(60, 60);
            this.btno.TabIndex = 95;
            this.btno.Tag = "o";
            this.btno.Text = "O";
            this.btno.UseVisualStyleBackColor = false;
            this.btno.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btni
            // 
            this.btni.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btni.Location = new System.Drawing.Point(441, 121);
            this.btni.Margin = new System.Windows.Forms.Padding(1);
            this.btni.Name = "btni";
            this.btni.Size = new System.Drawing.Size(60, 60);
            this.btni.TabIndex = 94;
            this.btni.Tag = "i";
            this.btni.Text = "I";
            this.btni.UseVisualStyleBackColor = false;
            this.btni.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnu
            // 
            this.btnu.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnu.Location = new System.Drawing.Point(379, 121);
            this.btnu.Margin = new System.Windows.Forms.Padding(1);
            this.btnu.Name = "btnu";
            this.btnu.Size = new System.Drawing.Size(60, 60);
            this.btnu.TabIndex = 93;
            this.btnu.Tag = "u";
            this.btnu.Text = "U";
            this.btnu.UseVisualStyleBackColor = false;
            this.btnu.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btny
            // 
            this.btny.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btny.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btny.Location = new System.Drawing.Point(317, 121);
            this.btny.Margin = new System.Windows.Forms.Padding(1);
            this.btny.Name = "btny";
            this.btny.Size = new System.Drawing.Size(60, 60);
            this.btny.TabIndex = 92;
            this.btny.Tag = "y";
            this.btny.Text = "Y";
            this.btny.UseVisualStyleBackColor = false;
            this.btny.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnt
            // 
            this.btnt.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnt.Location = new System.Drawing.Point(255, 121);
            this.btnt.Margin = new System.Windows.Forms.Padding(1);
            this.btnt.Name = "btnt";
            this.btnt.Size = new System.Drawing.Size(60, 60);
            this.btnt.TabIndex = 91;
            this.btnt.Tag = "t";
            this.btnt.Text = "T";
            this.btnt.UseVisualStyleBackColor = false;
            this.btnt.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnr
            // 
            this.btnr.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnr.Location = new System.Drawing.Point(193, 121);
            this.btnr.Margin = new System.Windows.Forms.Padding(1);
            this.btnr.Name = "btnr";
            this.btnr.Size = new System.Drawing.Size(60, 60);
            this.btnr.TabIndex = 90;
            this.btnr.Tag = "r";
            this.btnr.Text = "R";
            this.btnr.UseVisualStyleBackColor = false;
            this.btnr.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btne
            // 
            this.btne.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btne.Location = new System.Drawing.Point(131, 121);
            this.btne.Margin = new System.Windows.Forms.Padding(1);
            this.btne.Name = "btne";
            this.btne.Size = new System.Drawing.Size(60, 60);
            this.btne.TabIndex = 89;
            this.btne.Tag = "e";
            this.btne.Text = "E";
            this.btne.UseVisualStyleBackColor = false;
            this.btne.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnw
            // 
            this.btnw.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnw.Location = new System.Drawing.Point(69, 121);
            this.btnw.Margin = new System.Windows.Forms.Padding(1);
            this.btnw.Name = "btnw";
            this.btnw.Size = new System.Drawing.Size(60, 60);
            this.btnw.TabIndex = 88;
            this.btnw.Tag = "w";
            this.btnw.Text = "W";
            this.btnw.UseVisualStyleBackColor = false;
            this.btnw.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnq
            // 
            this.btnq.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnq.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnq.Location = new System.Drawing.Point(7, 121);
            this.btnq.Margin = new System.Windows.Forms.Padding(1);
            this.btnq.Name = "btnq";
            this.btnq.Size = new System.Drawing.Size(60, 60);
            this.btnq.TabIndex = 87;
            this.btnq.Tag = "q";
            this.btnq.Text = "Q";
            this.btnq.UseVisualStyleBackColor = false;
            this.btnq.Click += new System.EventHandler(this.btnKey_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.Color.Red;
            this.btnexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Location = new System.Drawing.Point(627, 307);
            this.btnexit.Margin = new System.Windows.Forms.Padding(1);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 60);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "QUIT";
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(751, 307);
            this.btn0.Margin = new System.Windows.Forms.Padding(1);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(60, 60);
            this.btn0.TabIndex = 84;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(813, 121);
            this.btn9.Margin = new System.Windows.Forms.Padding(1);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(60, 60);
            this.btn9.TabIndex = 83;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(751, 121);
            this.btn8.Margin = new System.Windows.Forms.Padding(1);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(60, 60);
            this.btn8.TabIndex = 82;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(689, 121);
            this.btn7.Margin = new System.Windows.Forms.Padding(1);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(60, 60);
            this.btn7.TabIndex = 81;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(813, 183);
            this.btn6.Margin = new System.Windows.Forms.Padding(1);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(60, 60);
            this.btn6.TabIndex = 80;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(751, 183);
            this.btn5.Margin = new System.Windows.Forms.Padding(1);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 60);
            this.btn5.TabIndex = 79;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(689, 183);
            this.btn4.Margin = new System.Windows.Forms.Padding(1);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(60, 60);
            this.btn4.TabIndex = 78;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(813, 245);
            this.btn3.Margin = new System.Windows.Forms.Padding(1);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(60, 60);
            this.btn3.TabIndex = 77;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(751, 245);
            this.btn2.Margin = new System.Windows.Forms.Padding(1);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 60);
            this.btn2.TabIndex = 76;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(689, 245);
            this.btn1.Margin = new System.Windows.Forms.Padding(1);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 60);
            this.btn1.TabIndex = 75;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btnnhondong
            // 
            this.btnnhondong.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnnhondong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhondong.Image = global::POS.Properties.Resources.Next;
            this.btnnhondong.Location = new System.Drawing.Point(689, 59);
            this.btnnhondong.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhondong.Name = "btnnhondong";
            this.btnnhondong.Size = new System.Drawing.Size(60, 60);
            this.btnnhondong.TabIndex = 121;
            this.btnnhondong.UseVisualStyleBackColor = false;
            this.btnnhondong.Click += new System.EventHandler(this.btnnhondong_Click);
            // 
            // btnnhonmo
            // 
            this.btnnhonmo.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnnhonmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhonmo.Image = global::POS.Properties.Resources.Pre;
            this.btnnhonmo.Location = new System.Drawing.Point(627, 59);
            this.btnnhonmo.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhonmo.Name = "btnnhonmo";
            this.btnnhonmo.Size = new System.Drawing.Size(60, 60);
            this.btnnhonmo.TabIndex = 120;
            this.btnnhonmo.UseVisualStyleBackColor = false;
            this.btnnhonmo.Click += new System.EventHandler(this.btnnhonmo_Click);
            // 
            // btndot
            // 
            this.btndot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btndot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndot.Image = global::POS.Properties.Resources.cham2;
            this.btndot.Location = new System.Drawing.Point(689, 307);
            this.btndot.Margin = new System.Windows.Forms.Padding(1);
            this.btndot.Name = "btndot";
            this.btndot.Size = new System.Drawing.Size(60, 60);
            this.btndot.TabIndex = 107;
            this.btndot.UseVisualStyleBackColor = false;
            this.btndot.Click += new System.EventHandler(this.btndot_Click);
            // 
            // btndel
            // 
            this.btndel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndel.Image = global::POS.Properties.Resources.mui_ten2;            
            this.btndel.Location = new System.Drawing.Point(813, 307);
            this.btndel.Margin = new System.Windows.Forms.Padding(1);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(60, 60);
            this.btndel.TabIndex = 85;
            this.btndel.UseVisualStyleBackColor = false;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // frmKeyboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(880, 372);
            this.Controls.Add(this.btntrondong);
            this.Controls.Add(this.btntronmo);
            this.Controls.Add(this.btnsao);
            this.Controls.Add(this.btnmu);
            this.Controls.Add(this.btnbang);
            this.Controls.Add(this.btnphantram);
            this.Controls.Add(this.btndola);
            this.Controls.Add(this.btnthang);
            this.Controls.Add(this.btnacong);
            this.Controls.Add(this.btnthan);
            this.Controls.Add(this.btnphay);
            this.Controls.Add(this.btnchia);
            this.Controls.Add(this.btnchamphay);
            this.Controls.Add(this.btnhaicham);
            this.Controls.Add(this.btnvuongdong);
            this.Controls.Add(this.btndauhoi);
            this.Controls.Add(this.btncong);
            this.Controls.Add(this.btntru);
            this.Controls.Add(this.btnvuongmo);
            this.Controls.Add(this.btnnhohon);
            this.Controls.Add(this.btnhoac);
            this.Controls.Add(this.btnnhondong);
            this.Controls.Add(this.btnnhonmo);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btncapslock);
            this.Controls.Add(this.btnshift);
            this.Controls.Add(this.txtresult);
            this.Controls.Add(this.btnspace);
            this.Controls.Add(this.btnm);
            this.Controls.Add(this.btnn);
            this.Controls.Add(this.btnb);
            this.Controls.Add(this.btnv);
            this.Controls.Add(this.btnc);
            this.Controls.Add(this.btnx);
            this.Controls.Add(this.btnz);
            this.Controls.Add(this.btndot);
            this.Controls.Add(this.btnl);
            this.Controls.Add(this.btnk);
            this.Controls.Add(this.btnj);
            this.Controls.Add(this.btnh);
            this.Controls.Add(this.btng);
            this.Controls.Add(this.btnf);
            this.Controls.Add(this.btnd);
            this.Controls.Add(this.btns);
            this.Controls.Add(this.btna);
            this.Controls.Add(this.btnenter);
            this.Controls.Add(this.btnp);
            this.Controls.Add(this.btno);
            this.Controls.Add(this.btni);
            this.Controls.Add(this.btnu);
            this.Controls.Add(this.btny);
            this.Controls.Add(this.btnt);
            this.Controls.Add(this.btnr);
            this.Controls.Add(this.btne);
            this.Controls.Add(this.btnw);
            this.Controls.Add(this.btnq);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btndel);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmKeyboard";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KEY BOARD";
            this.Load += new System.EventHandler(this.frmKeyboard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btntrondong;
        private System.Windows.Forms.Button btntronmo;
        private System.Windows.Forms.Button btnsao;
        private System.Windows.Forms.Button btnmu;
        private System.Windows.Forms.Button btnbang;
        private System.Windows.Forms.Button btnphantram;
        private System.Windows.Forms.Button btndola;
        private System.Windows.Forms.Button btnthang;
        private System.Windows.Forms.Button btnacong;
        private System.Windows.Forms.Button btnthan;
        private System.Windows.Forms.Button btnphay;
        private System.Windows.Forms.Button btnchia;
        private System.Windows.Forms.Button btnchamphay;
        private System.Windows.Forms.Button btnhaicham;
        private System.Windows.Forms.Button btnvuongdong;
        private System.Windows.Forms.Button btndauhoi;
        private System.Windows.Forms.Button btncong;
        private System.Windows.Forms.Button btntru;
        private System.Windows.Forms.Button btnvuongmo;
        private System.Windows.Forms.Button btnnhohon;
        private System.Windows.Forms.Button btnhoac;
        private System.Windows.Forms.Button btnnhondong;
        private System.Windows.Forms.Button btnnhonmo;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btncapslock;
        private System.Windows.Forms.Button btnshift;
        public System.Windows.Forms.TextBox txtresult;
        private System.Windows.Forms.Button btnspace;
        private System.Windows.Forms.Button btnm;
        private System.Windows.Forms.Button btnn;
        private System.Windows.Forms.Button btnb;
        private System.Windows.Forms.Button btnv;
        private System.Windows.Forms.Button btnc;
        private System.Windows.Forms.Button btnx;
        private System.Windows.Forms.Button btnz;
        private System.Windows.Forms.Button btndot;
        private System.Windows.Forms.Button btnl;
        private System.Windows.Forms.Button btnk;
        private System.Windows.Forms.Button btnj;
        private System.Windows.Forms.Button btnh;
        private System.Windows.Forms.Button btng;
        private System.Windows.Forms.Button btnf;
        private System.Windows.Forms.Button btnd;
        private System.Windows.Forms.Button btns;
        private System.Windows.Forms.Button btna;
        public System.Windows.Forms.Button btnenter;
        private System.Windows.Forms.Button btnp;
        private System.Windows.Forms.Button btno;
        private System.Windows.Forms.Button btni;
        private System.Windows.Forms.Button btnu;
        private System.Windows.Forms.Button btny;
        private System.Windows.Forms.Button btnt;
        private System.Windows.Forms.Button btnr;
        private System.Windows.Forms.Button btne;
        private System.Windows.Forms.Button btnw;
        private System.Windows.Forms.Button btnq;
        public System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;


    }
}