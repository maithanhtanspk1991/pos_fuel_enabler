﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using POS.FindObject;

namespace POS.Forms.FindObject
{
    public partial class frmFindCustomer : Form
    {
        public static bool blnFlat = false;

        public static string gTableProduct = "";

        public static int intIsBegin = 0;

        public static Class.MoneyFortmat money;

        public static string sParaCustomer = "";

        private Connection.Connection con;

        private Class.ReadConfig mReadConfig;

        public DataObject.Customers mCustomers { get; set; }

        public frmFindCustomer(Class.ReadConfig readConfig)
        {
            InitializeComponent();
            con = new Connection.Connection();
            ucLimit.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit_EventPage);
            mCustomers = null;
            cbbType.SelectedIndex = 0;
            money = new Class.MoneyFortmat(1);
            InitOrderBy();
            mReadConfig = readConfig;
            //Khóa company
            if (!mReadConfig.IsEnableCompany)
            {
                lvCustomer.Columns[1].Width += lvCustomer.Columns[2].Width;
                lvCustomer.Columns[1].Text = "Name";
                lvCustomer.Columns[3].Width = 0;
                lvCustomer.Columns[2].Width = 0;

            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (mCustomers != null)
            {
                mCustomers.CompanyName = BusinessObject.BOCompany.GetByID(mCustomers.Company).CompanyName;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ucLimit.Default();
            mCustomers = null;
            LoadCustomer();
        }

        private void btnHistoryTransaction_Click(object sender, EventArgs e)
        {
            if (mCustomers != null)
            {
                frmHistoryTransaction frm = new frmHistoryTransaction(con, money, mCustomers.CustID);
                frm.Text = "Account Detail - " + mCustomers.FirstName;
                frm.ViewDetailAccountPay();
                frm.ShowDialog();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                frmViewDetailDeptAccount frm = new frmViewDetailDeptAccount(con, money, mCustomers.CustID, mReadConfig);
                frm.Text = "Account Detail - " + mCustomers.FirstName;
                frm.ViewDetailAccountPay();
                frm.ShowDialog();
            }
            catch (Exception)
            {
            }
        }

        private void frmFindCustomer_Load(object sender, EventArgs e)
        {
            LoadCustomer();
        }

        private void LoadCustomer()
        {
            string lastName = "";
            string firstName = "";
            string mobile = "";
            string memberNo = "";
            string email = "";
            string streetNo = "";
            string streetName = "";
            switch (cbbType.SelectedIndex)
            {
                case 0:
                    firstName = txtSreach.Text;
                    break;

                case 1:
                    lastName = txtSreach.Text;
                    break;
                case 2:
                    mobile = txtSreach.Text;
                    break;

                case 3:
                    memberNo = txtSreach.Text;
                    break;

                case 4:
                    email = txtSreach.Text;
                    break;

                case 5:
                    streetNo = txtSreach.Text;
                    break;

                case 6:
                    streetName = txtSreach.Text;
                    break;
            }
            lvCustomer.Items.Clear();
            List<DataObject.Customers> lsArray = BusinessObject.BOCustomers.GetAll(lastName, firstName, mobile, memberNo, email, streetNo, streetName, 1, 0, -1, mOrderBy.OrderBySql, ucLimit.mLimit);
            ucLimit.PageNumReload();
            foreach (DataObject.Customers item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.MemberNo);
                if (mReadConfig.IsEnableCompany)
                    li.SubItems.Add(item.FirstName);
                else
                    li.SubItems.Add(item.FirstName + " " + item.LastName);
                li.SubItems.Add(item.LastName);
                li.SubItems.Add(item.CompanyName);
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Balance * -1, 3));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Accountlimit, 3));
                li.SubItems.Add(item.StreetNo);
                li.SubItems.Add(item.StreetName);
                li.Tag = item;
                lvCustomer.Items.Add(li);
            }
        }

        private void lvCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCustomer.SelectedItems.Count > 0)
            {
                mCustomers = (DataObject.Customers)lvCustomer.SelectedItems[0].Tag;
            }
        }

        private void ucLimit_EventPage(DataObject.Limit limit)
        {
            LoadCustomer();
        }

        private void txtSreach_TextChanged(object sender, EventArgs e)
        {
            ucLimit.Default();
            mCustomers = null;
            LoadCustomer();
        }
        private DataObject.OrderBy mOrderBy;
        private void InitOrderBy()
        {
            mOrderBy = new DataObject.OrderBy();
            mOrderBy.ListColumn.Add(new DataObject.OrderByColumn() { Column = 1, ColumnName = DataObject.Customers.CustomersColumn.FirstName.ToString() });
            mOrderBy.ListColumn.Add(new DataObject.OrderByColumn() { Column = 2, ColumnName = DataObject.Customers.CustomersColumn.Name.ToString() });
            mOrderBy.ListColumn.Add(new DataObject.OrderByColumn() { Column = 3, ColumnName = DataObject.Customers.CustomersColumn.CompanyName.ToString() });
        }
        private void lvCustomer_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            mOrderBy.Column = e.Column;
            mOrderBy.IsChangeOrderBy = true;
            if (mOrderBy.IsColumnOrderBy)
                LoadCustomer();
        }
    }
}