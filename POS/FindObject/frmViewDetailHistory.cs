﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.FindObject
{
    public partial class frmHistoryTransaction : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat mMoneyFortmat;

        private Class.Setting mSetting;
        private Connection.ReadDBConfig mReadDBConfig;
        private DataObject.Customers mCustomers;

        public frmHistoryTransaction(Connection.Connection mConnection, Class.MoneyFortmat money, int intCustomerID)
        {
            InitializeComponent();
            SetMultiLanguage();
            this.mConnection = mConnection;
            mSetting = new Class.Setting();
            this.mMoneyFortmat = money;
            dtpFrom.Value = DateTime.Now.AddMonths(-1);
            dtpTo.Value = DateTime.Now;
            mCustomers = BusinessObject.BOCustomers.GetByID(intCustomerID);
            ucLimit1.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit1_EventPage);
            mReadDBConfig = new Connection.ReadDBConfig();
            if (mReadDBConfig.CableID != "1")
            {
                btnPrinter.Visible = false;
            }
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if(mReadConfig.LanguageCode.Equals("vi"))
            {
                btnRefresh.Text = "Xem";
                label6.Text = "TỪ";
                label8.Text = "ĐẾN";
                btnPrinter.Text = "In Giao dịch";
                btnBack.Text = "Quay lại";
                columnHeader3.Text = "Số";
                columnHeader4.Text = "Ngày";
                columnHeader5.Text = "Mã giao dịch";
                columnHeader1.Text = "Mô tả";
                columnHeader2.Text = "Số xe";
                columnHeader6.Text = "Thành tiền";
                columnHeader7.Text = "Thanh toán nợ";
                columnHeader8.Text = "Ghi nợ";
                columnHeader9.Text = "Còn nợ";
                return;
            }
        }

        void ucLimit1_EventPage(DataObject.Limit limit)
        {
            ViewDetailAccountPay();
        }

        private string GetDateString(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        private string _nameClass = "POS::FindObject";
        public void ViewDetailAccountPay()
        {
            try
            {
                mConnection.Open();
                lvAccountDetail.Items.Clear();
                string strDateTo = GetDateString(dtpTo.Value);
                string strDateFrom = GetDateString(dtpFrom.Value);

                ucLimit1.mLimit.SQLTotal = "Select Count(*) As SL from accountpayment a inner join customers c on a.CustomerNo = c.memberNo wHERE DATE(a.PayDate) >= DATE('" + strDateFrom + "') and DATE(a.PayDate) <= DATE('" + strDateTo + "') and a.CustomerNo = '" + mCustomers.MemberNo + "';";
                if (ucLimit1.mLimit.IsLimit)
                {
                    BusinessObject.BOLimit.GetLimit("", ucLimit1.mLimit);
                }
                ucLimit1.PageNumReload();
                string sql = "CALL SP_ACCOUNT_STATEMENT_CAITIEN('" + strDateFrom + "', '" + strDateTo + "', '" + mCustomers.MemberNo + "', '" + ucLimit1.mLimit.SqlLimit + "');";
                System.Data.DataTable dt = mConnection.Select(sql);
                int n = 1;
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    ListViewItem li = new ListViewItem((ucLimit1.mLimit.PageSize * (ucLimit1.mLimit.Page - 1) + (n++)).ToString());
                    li.SubItems.Add(Convert.ToDateTime(row["PayDate"]).ToString("dd/MM/yyyy HH:mm:ss"));
                    li.SubItems.Add(row["OrderID"].ToString());
                    li.SubItems.Add(row["Description"].ToString());
                    li.SubItems.Add(row["CarNumber"].ToString());
                    li.SubItems.Add(string.Format("{0:0,0}", mMoneyFortmat.Format(Convert.ToDouble(row["Subtotal"]))));
                    li.SubItems.Add(string.Format("{0:0,0}", mMoneyFortmat.Format(Convert.ToDouble(row["PayAmount"]))));
                    if (row["PayAmount"].ToString() != "0")
                        li.SubItems.Add(string.Format("{0:0,0}", mMoneyFortmat.Format(0)));
                    else
                        li.SubItems.Add(string.Format("{0:0,0}", mMoneyFortmat.Format(Convert.ToDouble(row["Subtotal"]))));
                    li.Tag = row;
                    li.SubItems.Add(string.Format("{0:0,0}", mMoneyFortmat.Format(Convert.ToDouble(row["balance"]))));
                    lvAccountDetail.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ViewDetailAccountPay::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ViewDetailAccountPay();
        }

        private void btnPrinter_Click(object sender, EventArgs e)
        {
            if (lvAccountDetail.SelectedItems.Count > 0)
            {
                System.Data.DataRow row = (System.Data.DataRow)lvAccountDetail.SelectedItems[0].Tag;
                int OrderID = row["OrderID"].ToString() == "" ? 0 : Convert.ToInt32(row["OrderID"]);
                int Bkid = GetBKID(row);
                int balance = row["balance"].ToString() == "" ? 0 : Convert.ToInt32(row["balance"]);
                PrinterHistory PrinterHistory = new PrinterHistory(mReadDBConfig, mMoneyFortmat);
                PrinterHistory.Print(Bkid, OrderID, balance * -1);
            }
        }

        private int GetBKID(System.Data.DataRow row)
        {
            int result = 0;
            try
            {
                mConnection.Open();
                DateTime date = row["PayDate"].ToString() != "" ? Convert.ToDateTime(row["PayDate"]) : DateTime.Now;
                string sql = "Select bkid from ordersall Where OrderID = " + (row["OrderID"].ToString() == "" ? 0 : row["OrderID"]) + " And Date(ts) = Date('" + DataObject.DateTimeFormat.GetMysqlDate(date) + "');";
                System.Data.DataTable dt = mConnection.Select(sql);
                if (dt.Rows.Count > 0)
                {
                    result = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            catch {}
            finally
            {
                mConnection.Close();
            }
            return result;
        }
    }
}