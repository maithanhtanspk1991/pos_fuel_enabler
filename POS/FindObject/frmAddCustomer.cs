﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Properties;

namespace POS.FindObject
{
    public partial class frmAddCustomer : Form
    {
        private Connection.Connection conn;

        public string CustomerName { get; set; }

        public string CustomerCode { get; set; }

        public int CustomerID { get; set; }

        public int CustomerType { get; set; }

        public frmAddCustomer()
        {
            InitializeComponent();
        }

        public frmAddCustomer(Connection.Connection conn)
        {
            InitializeComponent();
            this.conn = conn;
        }

        public frmAddCustomer(Connection.Connection conn, string strPhoneNumber)
        {
            InitializeComponent();
            this.conn = conn;
            txtPhone.Text = strPhoneNumber;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                string CustCode = CalculateChecksumDigit();
                while (Convert.ToInt16(conn.ExecuteScalar("select count(memberNo) from customers where memberNo=" + CustCode)) > 0)
                {
                    //CustCode = CreatRandom();
                    CustCode = CalculateChecksumDigit();
                }
                if (intChoseCustomerType == 1)
                {
                    conn.ExecuteNonQuery("insert into customers(Name,mobile,memberNo,streetName,IsActive,IsDelivery) value(" +
                                               "'" + txtName.Text + "'," +
                                               "'" + txtPhone.Text + "'," +
                                               "'" + CustCode + "'," +
                                               "'" + txtAddress.Text + "',1,1)");
                }
                else if (intChoseCustomerType == 2)
                {
                    conn.ExecuteNonQuery("insert into customers(Name,mobile,memberNo,streetName,IsActive,IsPickup) value(" +
                                               "'" + txtName.Text + "'," +
                                               "'" + txtPhone.Text + "'," +
                                               "'" + CustCode + "'," +
                                               "'" + txtAddress.Text + "',1,1)");
                }

                String strSQL = "SELECT custID,memberNo,Name,IsDelivery,IsPickup FROM customers where memberNo = '" + CustCode + "'";
                DataTable dtSource = conn.Select(strSQL);
                CustomerName = dtSource.Rows[0]["Name"].ToString();
                CustomerCode = dtSource.Rows[0]["memberNo"].ToString();
                CustomerID = Convert.ToInt32(dtSource.Rows[0]["custID"]);
                if (Convert.ToInt32(dtSource.Rows[0]["IsDelivery"]) == 1)
                {
                    CustomerType = 1;
                }
                else if (Convert.ToInt32(dtSource.Rows[0]["IsPickup"]) == 1)
                {
                    CustomerType = 2;
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }

        public string CalculateChecksumDigit()
        {
            //DateTime tmeNow = DateTime.Now;
            //int mls = tmeNow.Millisecond;
            //Random rndNumber = new Random(mls);
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 9; i++)
            {
                resuilt += rd.Next(10);
            }
            //string sTemp = readconfig.NationCode + rndNumber.Next(1000,9999).ToString() + rndNumber.Next(10000,99999).ToString();
            string sTemp = "893" + resuilt;
            int iSum = 0;
            int iDigit = 0;

            // Calculate the checksum digit here.

            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                // This appears to be backwards but the

                // EAN-13 checksum must be calculated

                // this way to be compatible with UPC-A.

                if (i % 2 == 0)
                { // odd
                    iSum += iDigit * 3;
                }
                else
                { // even
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
            //this.ChecksumDigit = iCheckSum.ToString();
        }

        private int intChoseCustomerType;

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            btnDelivery.Image = (Bitmap)Resources.checkbox;
            btnPickup.Image = (Bitmap)Resources.UnCheck;
            intChoseCustomerType = 1;
        }

        private void btnPickup_Click(object sender, EventArgs e)
        {
            btnDelivery.Image = (Bitmap)Resources.UnCheck;
            btnPickup.Image = (Bitmap)Resources.checkbox;
            intChoseCustomerType = 2;
        }

        private void frmAddCustomer_Load(object sender, EventArgs e)
        {
            btnDelivery_Click(sender, e);
        }
    }
}