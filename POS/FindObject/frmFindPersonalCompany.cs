﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms.FindObject
{
    public partial class frmFindPersonalCustomer : Form
    {
        public frmFindPersonalCustomer()
        {
            InitializeComponent();
            con = new Connection.Connection();
            prvBuildingComboSearch();
            prvSetDataGridCustomer();
            txtFind.Focus();
        }       

        private Connection.Connection con;
        public static string gTableProduct = "";

        private void prvSetDataGridCustomer()
        {
            string vSQL = "";
            string vCondition = "";
            vCondition = prvGetConditionSearch();
            if (gTableProduct.ToUpper() == "NEWCUSTOMER")
            {
                vSQL = "SELECT * " +
                        "FROM " +
                        "( " +
                        "SELECT PaymetID,PayDate,if(LENGTH(CustomerNo) <= 6, co.companyCode,CustomerNo) as CustomerNo,if(Name is null,co.companyName,Name)AS Name,Subtotal,if(Company is null,CustomerNo,Company) as companyID,if(typePayment = 0 ,'No Debt',if(typePayment = 1, 'Purchase',if(typePayment = 2,'Pay Debt','Deposit Account'))) AS TypePayment " +
                        "FROM " +
                        "( " +
                        "SELECT ap.PaymetID ,ap.PayDate,c.FirstName as Name ,ap.CustomerNo,if(ap.typePayment = 1,ap.Subtotal,ap.cash+ap.card) as Subtotal,c.Company,ap.typePayment " +
                        "FROM accountpayment as ap left join customers as c on ap.CustomerNo = c.memberNo where ap.typePayment <> 0" +
                        ")AS S left join company as co on S.CustomerNo = co.idCompany " +
                        ")AS S2 WHERE S2.companyID = " + idCompany + " ";
                vSQL += vCondition;
                vSQL += " order by PaymetID DESC ";
            }

            BindingSource sBindingSourceProduct = new BindingSource();
            DataTable dtSource = new DataTable();
            dtSource = con.Select(vSQL);

            DataTable dtSource2 = new DataTable();
            dtSource2.Columns.Add("PaymetID", typeof(string));
            dtSource2.Columns.Add("PayDate", typeof(string));
            dtSource2.Columns.Add("CustomerNo", typeof(string));
            dtSource2.Columns.Add("Name", typeof(string));
            dtSource2.Columns.Add("Subtotal", typeof(string));
            dtSource2.Columns.Add("TypePayment", typeof(string));

            foreach (DataRow dr in dtSource.Rows)
            {
                DateTime dtTime = Convert.ToDateTime(dr["PayDate"]);
                dtSource2.Rows.Add(dr["PaymetID"], dtTime.Day + "/" + dtTime.Month + "/" + dtTime.Year + " " + dtTime.Hour + ":" + dtTime.Minute + ":" + dtTime.Second, dr["CustomerNo"], dr["Name"], money.Format(Convert.ToDouble(dr["Subtotal"].ToString())), dr["TypePayment"]);
            }
            sBindingSourceProduct.DataSource = dtSource2;
            gTableCustomer.DataSource = sBindingSourceProduct;

            sBindingSourceProduct = null;

            prvFormatDataGridCustomer();
        }

        private void prvFormatDataGridCustomer()
        {
            string vCaption = "";
            string vWidth = "";
            string vAlign = "";
            string vFormat = "";

            if (gTableProduct.ToUpper() == "NEWCUSTOMER")
            {
                vCaption = "PaymetID | PayDate | CustomerNo | Company Name | Subtotal | Type ";
                vWidth = "20|150|100|150|100|100";
                vAlign = "1|1|1|1|1|1";
                vFormat = "C|C|C|C|N2|C";
            }
            prvSetHeaderCaptionCustomer(vCaption);
            prvSetWidthCustomer(vWidth, vAlign);
            prvSetDataType(vFormat);
        }

        private void prvSetDataType(string pDataType)
        {
            Int32 sCount;
            string[] sArrDataType;
            sArrDataType = pDataType.Split('|');
            sCount = 0;
            while (sCount < sArrDataType.Length - 1)
            {
                if (sArrDataType[sCount].ToString() == "C")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Format = "c";
                }
                else if (sArrDataType[sCount].ToString() == "N3")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Format = "N3";
                }
                else if (sArrDataType[sCount].ToString() == "N")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Format = "N0";
                }
                else if (sArrDataType[sCount].ToString() == "N2")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Format = "N2";
                }
                sCount++;
            }
        }

        private void prvSetHeaderCaptionCustomer(string pCaption)
        {
            Int32 sCount = 0;
            string[] sArrCaption;

            sArrCaption = pCaption.Split('|');
            while (sCount < sArrCaption.Length)
            {
                gTableCustomer.Columns[sCount].HeaderText = sArrCaption[sCount].ToString();
                gTableCustomer.Columns[sCount].ReadOnly = true;
                sCount++;
            }
            gTableCustomer.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gTableCustomer.Columns[0].Visible = false;
        }

        private void prvSetWidthCustomer(string pWidth, string pAlign)
        {
            Int32 sCount = 0;
            string vValue = "";
            string[] sArrWidth;
            string[] sArrAlign;
            sArrWidth = pWidth.Split('|');
            while (sCount < sArrWidth.Length)
            {
                gTableCustomer.Columns[sCount].Width = Convert.ToInt16(sArrWidth[sCount].ToString());
                sCount++;
            }
            sArrAlign = pAlign.Split('|');
            sCount = 0;

            while (sCount < sArrAlign.Length)
            {
                vValue = sArrAlign[sCount].ToString();
                if ((vValue == "1") || (vValue == "0"))
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else if (vValue == "2")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (vValue == "3")
                {
                    gTableCustomer.Columns[sCount].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                sCount++;
            }
            gTableCustomer.DisplayedRowCount(true);
            return;
        }

        public static string sParaCustomer = "";

        private string prvGetConditionSearch()
        {
            string vSQL;
            if (txtFind.Text != "")
            {
                if (cboFieldName.Text.Trim() != "Name" || cboFieldName.Text.Trim() != "streetName")
                {
                    vSQL = " and " + cboFieldName.Text + " like '%" + txtFind.Text.Trim() + "%' ";
                }
                else
                {
                    vSQL = " " + cboFieldName.Text + " ='" + txtFind.Text.Trim() + "'";
                }
            }
            else
            {
                vSQL = "";
            }

            if (vSQL != "")
            {
                if (sParaCustomer != "")
                {
                    vSQL = " " + vSQL + " AND " + sParaCustomer;
                }
                else
                {
                    vSQL = " " + vSQL + "";
                }
            }
            else
            {
                if (sParaCustomer != "")
                {
                    vSQL = " WHERE " + sParaCustomer;
                }
            }
            return (vSQL);
        }

        public static Class.MoneyFortmat money;

        public static frmFindPersonalCustomer Instance(string pID, Class.MoneyFortmat m, string idCompany2)
        {
            gTableProduct = pID;
            idCompany = idCompany2;
            money = m;
            sParaCustomer = "";
            if (sForm == null) { sForm = new frmFindPersonalCustomer(); }
            return sForm;
        }

        private void prvBuildingComboSearch()
        {
            if (gTableProduct.ToUpper() == "NEWCUSTOMER")
            {
                cboCondition.Items.Add("Name");
                cboCondition.Items.Add("CustomerNo");

                cboFieldName.Items.Add("Name");
                cboFieldName.Items.Add("CustomerNo");
                cboCondition.SelectedIndex = 0;
            }
        }

        private void txtFind_TextChanged(object sender, EventArgs e)
        {
        }

        private void cboCondition_TextChanged(object sender, EventArgs e)
        {
            cboFieldName.SelectedIndex = cboCondition.SelectedIndex;
        }

        private void cboCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboFieldName.SelectedIndex = cboCondition.SelectedIndex;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            prvSetSelectRow();
            sForm = null;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public static frmFindPersonalCustomer sForm = null;
        public static Object GetValue0;
        public static Object GetValue1 = "";
        public static Object GetValue2 = "";
        public static Object GetValue3 = "";
        public static Object GetValue4 = "";
        public static Object GetValue5 = "";
        public static Object GetValue6 = "";
        public static string idCompany;

        private void prvSetSelectRow()
        {
            try
            {
                if (gTableCustomer.RowCount <= 0)
                {
                    GetValue0 = "";
                }
                else
                {
                    GetValue0 = gTableCustomer.CurrentRow.Cells["custID"].Value;
                    GetValue1 = gTableCustomer.CurrentRow.Cells["memberNo"].Value;
                    GetValue2 = gTableCustomer.CurrentRow.Cells["Name"].Value;
                    GetValue3 = gTableCustomer.CurrentRow.Cells["accountlimit"].Value;
                    GetValue4 = gTableCustomer.CurrentRow.Cells["debt"].Value;
                    GetValue5 = gTableCustomer.CurrentRow.Cells["phone"].Value;
                    GetValue6 = gTableCustomer.CurrentRow.Cells["mobile"].Value;
                }
            }
            catch (Exception)
            {
                GetValue1 = "";
                GetValue2 = "";
                GetValue3 = "0";
                GetValue4 = "0";
                GetValue5 = "";
                GetValue6 = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GetValue1 = "";
            GetValue2 = "";
            sForm = null;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void txtFind_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtFind);
            frm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            prvSetDataGridCustomer();
        }

        private void frmFindCustomer_Load(object sender, EventArgs e)
        {
        }

        private void btnViewHistory_Click(object sender, EventArgs e)
        {
        }
    }
}