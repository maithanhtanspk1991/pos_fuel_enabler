﻿namespace POS
{
    partial class UCPump
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPumpID = new System.Windows.Forms.Label();
            this.lbLitre = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbPumpID
            // 
            this.lbPumpID.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbPumpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPumpID.Location = new System.Drawing.Point(0, 0);
            this.lbPumpID.Name = "lbPumpID";
            this.lbPumpID.Size = new System.Drawing.Size(88, 24);
            this.lbPumpID.TabIndex = 4;
            this.lbPumpID.Text = "A";
            this.lbPumpID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbPumpID.Click += new System.EventHandler(this.UCPump_Click);
            // 
            // lbLitre
            // 
            this.lbLitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbLitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLitre.ForeColor = System.Drawing.Color.White;
            this.lbLitre.Location = new System.Drawing.Point(0, 24);
            this.lbLitre.Name = "lbLitre";
            this.lbLitre.Size = new System.Drawing.Size(88, 20);
            this.lbLitre.TabIndex = 5;
            this.lbLitre.Text = "Litre";
            this.lbLitre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbLitre.Click += new System.EventHandler(this.UCPump_Click);
            // 
            // lbTotal
            // 
            this.lbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.ForeColor = System.Drawing.Color.White;
            this.lbTotal.Location = new System.Drawing.Point(0, 44);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(88, 38);
            this.lbTotal.TabIndex = 6;
            this.lbTotal.Text = "Total";
            this.lbTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbTotal.Click += new System.EventHandler(this.UCPump_Click);
            // 
            // UCPump
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbLitre);
            this.Controls.Add(this.lbPumpID);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UCPump";
            this.Size = new System.Drawing.Size(88, 82);
            this.Load += new System.EventHandler(this.UCPump_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lbPumpID;
        internal System.Windows.Forms.Label lbLitre;
        internal System.Windows.Forms.Label lbTotal;
    }
}
