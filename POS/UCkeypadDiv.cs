﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCkeypadDiv : UserControl
    {
        public TextBox txtresult { get; set; }

        public UCkeypadDiv()
        {
            InitializeComponent();
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            if (txtresult.Text != "")
            {
                if (txtresult.Text[0] == '-')
                {
                    txtresult.Text = txtresult.Text.Remove(0, 1);
                }
                else
                {
                    txtresult.Text = '-' + txtresult.Text;
                }
            }
        }
    }
}