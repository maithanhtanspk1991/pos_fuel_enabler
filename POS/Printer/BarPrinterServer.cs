﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

//using POS.Printer;
using POS.Class;

namespace POS
{
    public class BarPrinterServer
    {
        private static string[] changeType = { "", "ADD", "CAN" };
        private Connection.ReadDBConfig dbconfig;
        private Printer m_printer;
        private Connection.Connection m_con;
        private SocketServer m_socketserver;
        private Class.MoneyFortmat money;
        private string m_table;
        private int m_function;
        private static System.Drawing.Font m_font = new System.Drawing.Font("Arial", 12);
        private bool misKitchen;
        private bool mIsPrintBuilt;
        private bool mIsBuiltName;
        private bool mIsDriverOff;
        private bool mEnablePrintKitchenAndBar = false;

        public string KitchenName { get; set; }

        public string BarName { get; set; }

        public string BillName { get; set; }

        //private int mOrderID;

        private Class.ProcessOrderNew.Order mOrder;

        public BarPrinterServer(Printer printer, Connection.Connection con, SocketServer socketserver, Class.MoneyFortmat m)
        {
            m_con = con;
            m_printer = printer;
            money = m;
            //BarName = m_printer.printDocument.PrinterSettings.PrinterName;
            m_printer.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            m_socketserver = socketserver;
            m_socketserver.Received += new SocketServer.MyServerEvenHandler(m_socketserver_Received);
            dbconfig = new Connection.ReadDBConfig();
            mEnablePrintKitchenAndBar = Class.Config.GetEnablePrintBarAndKitchen();
        }

        private float DrawString(string s, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font, float y, int textAlign)
        {
            return DrawString(s, e, font, 0, y, textAlign);
        }

        private float DrawString(string s, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font, float x, float y, int textAlign)
        {
            List<string> list = SplitStringLine(s, e, font);
            foreach (string item in list)
            {
                if (x == 0)
                {
                    if (textAlign == 1)
                    {
                        x = 0;
                    }
                    else if (textAlign == 2)
                    {
                        x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(item, font).Width) / 2);
                    }
                    else
                    {
                        x = e.PageBounds.Width - e.Graphics.MeasureString(item, font).Width;
                    }
                }
                e.Graphics.DrawString(item, font, System.Drawing.Brushes.Black, x, y);
                y += e.Graphics.MeasureString(item, font).Height;
            }

            return y;
        }

        private List<string> SplitStringLine(string str, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font)
        {
            List<string> list = new List<string>();
            string[] s = str.Split(' ');
            string resuilt = "";

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Length > 0)
                {
                    if (e.Graphics.MeasureString(resuilt + s[i], font).Width > e.PageBounds.Width && resuilt.Length > 0)
                    {
                        list.Add(resuilt);
                        i--;
                        resuilt = "";
                    }
                    else if (e.Graphics.MeasureString(s[i], font).Width > e.PageBounds.Width)
                    {
                        list.Add(s[i]);
                        resuilt = "";
                    }
                    else
                    {
                        resuilt += s[i] + " ";
                    }
                }
            }
            if (resuilt.Length > 0)
            {
                list.Add(resuilt);
            }
            return list;
        }

        private void DrawCancelLine(System.Drawing.Printing.PrintPageEventArgs e, float y_start, float y_end)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Black);
            e.Graphics.DrawLine(pen, 0, y_start, e.PageBounds.Width, y_end);
            e.Graphics.DrawLine(pen, 0, y_end, e.PageBounds.Width, y_start);
        }

        private float DrawLine(string s, System.Drawing.Font font, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Drawing2D.DashStyle dashStyle, float y, int textAlign)
        {
            float x;
            float width;
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Black);
            if (dashStyle != System.Drawing.Drawing2D.DashStyle.Custom)
            {
                pen.DashStyle = dashStyle;
            }
            if (s == "" || s == null)
            {
                width = e.PageBounds.Width;
            }
            else
            {
                width = e.Graphics.MeasureString(s, font).Width;
            }
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawLine(pen, x, y, x + width, y);
            y += 2;
            return y;
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (m_table != null)
            {
                if (mIsPrintBuilt)
                {
                    printBuilt(e);
                }
                else
                {
                    PrintChickenOrBar(e, m_function, misKitchen);
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        public void printDriverOff(Class.ProcessOrderNew.Order order)
        {
            try
            {
                mIsPrintBuilt = true;
                mIsDriverOff = true;
                mOrder = order;
                mOrder.SubTotal = mOrder.getTotal();
                m_table = order.TableID;
                m_printer.printDocument.PrinterSettings.PrinterName = BillName;
                m_printer.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.printBuilt:::" + ex.Message);
            }
            finally
            {
                mIsDriverOff = false;
            }
        }

        private void printBuilt(System.Drawing.Printing.PrintPageEventArgs e)
        {
            ReadConfig mReadConfig = new ReadConfig();
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            System.Drawing.Font font9 = new System.Drawing.Font("Arial", 9);
            float l_y = 0;

            string header = "";
            string bankCode = "";
            string address = "";
            string address1 = "";
            string tell = "";
            string website = "";
            string thankyou = "";
            string terminal = "";
            int customerType = 0;

            try
            {
                header = dbconfig.Header1.ToString();
                bankCode = dbconfig.Header2.ToString();
                address = dbconfig.Header3.ToString();
                tell = dbconfig.Header4.ToString();
                address1 = dbconfig.Header5.ToString();
                website = dbconfig.FootNode1.ToString();
                thankyou = dbconfig.FootNode2.ToString();
                terminal = dbconfig.CableID.ToString();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer:::Set Header or Footer config:::" + ex.Message);
            }

            l_y = DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2); // was 18
            l_y = DrawString(bankCode, e, font11, l_y, 2);
            l_y = DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = DrawString(address1, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += m_printer.GetHeightPrinterLine();

            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += m_printer.GetHeightPrinterLine();
            DateTime dateTime = DateTime.Now;
            if (mOrder.OrderID > 0)
            {
                DrawString("ORDER# " + mOrder.OrderID, e, font11, l_y, 1);
            }
            l_y = DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            if (mOrder.CableID > 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Mã máy: " + mOrder.CableID, e, font11, l_y, 1);
                }
                else
                {
                    DrawString("CableID# " + mOrder.CableID, e, font11, l_y, 1);
                }
                
            }
            if (!m_table.Contains("TKA-") && m_table != "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("BÀN SỐ: " + m_table, e, font11, l_y, 3);
                }
                DrawString("TABLE# " + m_table, e, font11, l_y, 3);
            }
            if (mOrder.ShiftID != 0)
            {
                DataObject.ShiftObject so = new DataObject.ShiftObject();
                so.ShiftID = mOrder.ShiftID;
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    l_y = DrawString("CA: " + BusinessObject.BOShift.GetShiftMaxWithShiftID(so).ShiftName, e, font11, l_y, 3);
                }
                else
                {
                    l_y = DrawString("SHIFT# " + BusinessObject.BOShift.GetShiftMaxWithShiftID(so).ShiftName, e, font11, l_y, 3);
                }
            }
            try
            {
                if (mOrder.Delivery != 0 || mOrder.Pickup != 0)
                {
                    if (mOrder.CustCode != "" && mOrder.CustCode != null)
                    {
                        Connection.Connection conn = new Connection.Connection();
                        DataTable dtSourceCustomer = new DataTable();
                        dtSourceCustomer = conn.Select("SELECT c.Name,c.memberNo,c.streetName,c.mobile,c.IsDelivery,c.IsPickup FROM customers c WHERE c.memberNo = " + mOrder.CustCode);
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            l_y = DrawString("TÊN TÀI KHOẢN: " + dtSourceCustomer.Rows[0]["Name"].ToString().ToUpper(), e, font11, l_y, 1);
                            l_y = m_printer.DrawMessenge("ĐỊA CHỈ: " + dtSourceCustomer.Rows[0]["streetName"].ToString().ToUpper(), e, font11, l_y);
                            l_y = DrawString("SỐ ĐIỆN THOẠI: " + dtSourceCustomer.Rows[0]["mobile"].ToString().ToUpper(), e, font11, l_y, 1);
                        }
                        else
                        {
                            l_y = DrawString("ACC NAME # " + dtSourceCustomer.Rows[0]["Name"].ToString().ToUpper(), e, font11, l_y, 1);
                            l_y = m_printer.DrawMessenge("ADDRESS # " + dtSourceCustomer.Rows[0]["streetName"].ToString().ToUpper(), e, font11, l_y);
                            l_y = DrawString("MOBILE # " + dtSourceCustomer.Rows[0]["mobile"].ToString().ToUpper(), e, font11, l_y, 1);
                        }
                        if (Convert.ToBoolean(Convert.ToInt32(dtSourceCustomer.Rows[0]["IsDelivery"].ToString())))
                        {
                            customerType = 1;
                        }
                        else if (Convert.ToBoolean(Convert.ToInt32(dtSourceCustomer.Rows[0]["IsPickup"].ToString())))
                        {
                            customerType = 2;
                        }
                    }
                    else
                    {
                        customerType = 0;
                    }
                }
            }
            catch (Exception)
            {
            }

            List<Class.ItemOrderK> list = getOrderBill(m_table, mOrder.OrderID);
            if (list.Count <= 0)
            {
                e.Cancel = true;
                return;
            }

            l_y += m_printer.GetHeightPrinterLine();
            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += m_printer.GetHeightPrinterLine();
            string sBuilt = string.Empty;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                sBuilt = "HÓA ĐƠN THUẾ";
            }
            else {  sBuilt = "TAX INVOICE"; }
            if (mIsBuiltName)
            {
                if (customerType == 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        sBuilt = "HÓA ĐƠN";
                    }
                    else { sBuilt = "BILL"; }
                }
                else if (customerType == 1)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        sBuilt = "HÓA ĐƠN THUẾ";
                    }
                    else
                    {
                        sBuilt = "TAX INVOICE";
                    }
                }
                else if (customerType == 2)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        sBuilt = "HÓA ĐƠN THUẾ";
                    }
                    else { sBuilt = "TAX INVOICE"; }
                    
                }
            }
            if (mIsDriverOff)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    sBuilt = "Bỏ Chạy";
                }
                else { sBuilt = "Drive Off"; }
                
                mOrder.SubTotal = mOrder.getSubTotal();
            }
            l_y = DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);
            if (mOrder.Paid == 1)
            {
                if (mReadConfig.LanguageCode.Equals("vi")) {
                    l_y = DrawString("Trả tiền", e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);
                }
                else { l_y = DrawString("Paid", e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2); }
            }

            l_y += m_printer.GetHeightPrinterLine();

            double subTotal = 0;
            foreach (Class.ItemOrderK item in list)
            {
                float yStart = l_y;
                string itemName = "";
                string pumpID = "";
                l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                itemName = item.Name;
                if (item.Gst == 0)
                {
                    if (item.Total >= 0)
                    {
                        itemName = "* " + itemName;
                    }
                }
                if (item.Weight > 1)
                {
                    Connection.Connection conn = new Connection.Connection();
                    conn.Open();
                    DataTable dtSourceItem = new DataTable();
                    dtSourceItem = conn.Select("SELECT count(itemID) AS Numrows FROM itemsmenu where visual = 1 and itemID = " + item.ItemID);
                    conn.Close();
                    if (Convert.ToInt32(dtSourceItem.Rows[0]["Numrows"]) != 0)
                    {
                        pumpID = Convert.ToString(Convert.ToInt32(item.PumpID.ToString()) == 0 ? "" : "PUMP ID# " + item.PumpID.ToString());
                    }
                    else
                    {
                        pumpID = "";
                    }
                    string qty = item.Qty + "";
                    string data = "    Quantity  " + qty + " @ $" + money.Format2(item.Price) + " each";
                    if (item.Weight > 0)
                    {
                        qty = money.FormatCurenMoney2((double)item.Qty / item.Weight);
                        data = "    " + qty + " Litres @ $" + money.Format(item.Price) + "/litre";
                    }
                    l_y = DrawString(itemName + "        " + pumpID, e, font9, l_y, 1);
                    l_y = DrawString(data, e, font9, l_y, 1);
                }
                else
                {
                    DrawString(item.Qty.ToString(), e, font9, l_y, 1);
                    l_y = DrawString(itemName, e, font9, 30, l_y, 1);
                }
                foreach (Class.ItemOptionID option in item.Option)
                {
                    l_y = DrawString(new String(' ', 6) + option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Italic), l_y, 1);
                    item.Total += option.Price;
                }
                DrawString(money.Format2(item.Total), e, new System.Drawing.Font("Arial", 9), yStart, 3);
                subTotal += item.Total;
            }
            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);

            l_y += m_printer.GetHeightPrinterLine() / 10;
            System.Drawing.Font fontTotal;
            double itemDiscount = 0;
            foreach (Class.ItemOrderK item in list)
            {
                if (item.Total < 0)
                {
                    itemDiscount += item.Total;
                }
            }
            try
            {
                if (customerType == 1)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Phí giao hàng:", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                    }
                    else { DrawString("Fee Delivery:", e, new System.Drawing.Font("Arial", 9), l_y, 1); }
                    
                    l_y = DrawString("$" + money.getFortMat2(dbconfig.DeliveryAmount), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                }
            }
            catch (Exception)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.printBuilt.TaxInvoice:::FEE DELIVERY");
            }

            itemDiscount = Math.Abs(itemDiscount);
            if (itemDiscount > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Tổng giảm giá:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Total Discount:", e, fontTotal, l_y, 1); }
                l_y = DrawString("$" + money.Format2(itemDiscount), e, fontTotal, l_y, 3);
            }

            l_y += m_printer.GetHeightPrinterLine() / 2;

            fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            if (mOrder.Surchart != 0)
            {
                if(mReadConfig.LanguageCode.Equals("vi"))
                { DrawString("Thẻ tín dụng phụ thu:", e, fontTotal, l_y, 1); }
                else { DrawString("Credit Card Surcharge:", e, fontTotal, l_y, 1); }
                l_y = DrawString("$" + money.Format2(mOrder.Surchart), e, fontTotal, l_y, 3);
                l_y += m_printer.GetHeightPrinterLine() / 2;
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                DrawString("Tổng cộng:", e, fontTotal, l_y, 1);
            }
            else { DrawString("Total:", e, fontTotal, l_y, 1); }
            l_y = DrawString("$" + money.Format2(mOrder.SubTotal), e, fontTotal, l_y, 3);
            l_y += m_printer.GetHeightPrinterLine() / 2;

            if (mOrder.Discount > 0)
            {
                l_y += m_printer.GetHeightPrinterLine() / 2;
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Giảm giá " + Convert.ToInt16((mOrder.Discount / mOrder.SubTotal) * 100) + "%:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Discount " + Convert.ToInt16((mOrder.Discount / mOrder.SubTotal) * 100) + "%:", e, fontTotal, l_y, 1); }
                l_y = DrawString("$" + money.Format2(mOrder.Discount), e, fontTotal, l_y, 3);

                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Số nợ:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Balance:", e, fontTotal, l_y, 1); }
                
                if (mOrder.Card > 0)
                {
                    l_y = DrawString("$" + money.Format2(mOrder.SubTotal - mOrder.Discount), e, fontTotal, l_y, 3);
                }
                else
                {
                    l_y = DrawString("$" + money.Format2(Class.Round.RoundValue(mOrder.SubTotal - mOrder.Discount)), e, fontTotal, l_y, 3);
                }
            }

            if (mOrder.Gst != 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Thuế(bao gồm trong tổng cộng):", e, fontTotal, l_y, 1);
                }
                else { DrawString("GST(included in total):", e, fontTotal, l_y, 1); }
                
                l_y = DrawString("$" + money.Format2(mOrder.Gst), e, fontTotal, l_y, 3);
            }
            else
            {
                if (Math.Round(mOrder.GetTotalGST(), 2) > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Thuế(bao gồm trong tổng cộng):", e, fontTotal, l_y, 1);
                    }
                    else { DrawString("GST(included in total):", e, fontTotal, l_y, 1); }
                    l_y = DrawString("$" + money.Format2(mOrder.GetTotalGST()), e, fontTotal, l_y, 3);
                }
            }

            if (mOrder.Deposit > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Tiền đặt cọc:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Deposit:", e, fontTotal, l_y, 1); }
                
                l_y = DrawString("$" + money.Format2(mOrder.Deposit), e, fontTotal, l_y, 3);
            }

            try
            {
                if (mOrder.Customer.CustID != 0 && mOrder.Delivery == 0 && mOrder.Pickup == 0)
                {
                    Connection.Connection conn = new Connection.Connection();
                    DataTable dtSourceCustomer = new DataTable();
                    mOrder.Customer = BusinessObject.BOCustomers.GetByID(mOrder.Customer.CustID);
                    if (mOrder.Customer.Company == 0)
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            l_y = DrawString("Mã: " + mOrder.Customer.MemberNoShort, e, font11, l_y, 1);
                        }
                        else{l_y = DrawString("Code: " + mOrder.Customer.MemberNoShort, e, font11, l_y, 1);}
                        if (mOrder.Customer.FirstName != "")
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                l_y = DrawString("Tên: " + mOrder.Customer.FirstName + " " + mOrder.Customer.LastName, e, font11, l_y, 1);
                            }
                            else { l_y = DrawString("Name: " + mOrder.Customer.FirstName + " " + mOrder.Customer.LastName, e, font11, l_y, 1); }
                        //if (mOrder.Customer.LastName != "")
                        //    l_y = DrawString("Last Name: " + mOrder.Customer.LastName, e, font11, l_y, 1);
                    }
                    else
                    {
                        DataObject.Company company = BusinessObject.BOCompany.GetByID(mOrder.Customer.Company);
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            l_y = DrawString("Mã công ty: " + company.CompanyCodeShort, e, font11, l_y, 1);
                            l_y = DrawString("Tên công ty: " + company.CompanyName, e, font11, l_y, 1);
                        }
                        else
                        {
                            l_y = DrawString("Comany code: " + company.CompanyCodeShort, e, font11, l_y, 1);
                            l_y = DrawString("Comany Name: " + company.CompanyName, e, font11, l_y, 1);
                        }
                    }

                    if (mOrder.RegoID > 0)
                    {
                        DataObject.Rego rego = BusinessObject.BORego.GetByID(mOrder.RegoID);
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            l_y = DrawString("Số xe: " + rego.CarNumber, e, font11, l_y, 1);
                            l_y = DrawString("Hãng xe: " + rego.CarProduct, e, font11, l_y, 1);
                        }
                        else
                        {
                            l_y = DrawString("Rego Number: " + rego.CarNumber, e, font11, l_y, 1);
                            l_y = DrawString("Car Product: " + rego.CarProduct, e, font11, l_y, 1);
                        }
                    }
                }
            }
            catch
            {
            }

            if (mOrder.Account > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mOrder.Customer.Company == 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Số nợ ban đầu:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2((mOrder.Customer.Balance - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        DrawString("Số tiền phải trả:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        DrawString("Số nợ còn lại:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Customer.Balance * -1), e, fontTotal, l_y, 3);
                    }
                    else
                    {
                        DrawString("Openning Balance:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2((mOrder.Customer.Balance - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        DrawString("Pay Amount:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        DrawString("Closing Balance:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Customer.Balance * -1), e, fontTotal, l_y, 3);
                    }
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DataObject.Company company = BusinessObject.BOCompany.GetByID(mOrder.Customer.Company);
                        DrawString("Số nợ ban đầu:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2((company.Debt - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        DrawString("Số tiền phải trả:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        DrawString("Số nợ còn lại:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(company.Debt * -1), e, fontTotal, l_y, 3);
                    }
                    else
                    {
                        DataObject.Company company = BusinessObject.BOCompany.GetByID(mOrder.Customer.Company);
                        DrawString("Openning Balance:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2((company.Debt - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        DrawString("Pay Amount:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        DrawString("Closing Balance:", e, fontTotal, l_y, 1);
                        l_y = DrawString(money.Format2(company.Debt * -1), e, fontTotal, l_y, 3);
                    }
                   
                }

                l_y += m_printer.GetHeightPrinterLine();
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    l_y = DrawString("CHỮ KÝ:...................................", e, fontTotal, l_y, 3);
                }
                else { l_y = DrawString("SIGNATURE:...................................", e, fontTotal, l_y, 3); }
                
            }

            if (mOrder.Tendered > 0)
            {
                double totalRound = Class.Round.Round3to2(mOrder.SubTotal);
                if (totalRound != mOrder.SubTotal)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Làm tròn:", e, fontTotal, l_y, 1);
                    }
                    else { DrawString("Rounding:", e, fontTotal, l_y, 1); }
                    l_y = DrawString("$" + money.Format2(totalRound - mOrder.SubTotal), e, fontTotal, l_y, 3);
                }
                fontTotal = new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Bold);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Đấu thầu:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Tendered:", e, fontTotal, l_y, 1); }
                
                l_y = DrawString("$" + money.Format2(mOrder.Tendered) + "", e, fontTotal, l_y, 3);
            }

            if (mOrder.ChangeByCash > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Bold);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    DrawString("Thối lại:", e, fontTotal, l_y, 1);
                }
                else { DrawString("Change:", e, fontTotal, l_y, 1); }
                
                l_y = DrawString("$" + money.Format2(mOrder.ChangeByCash) + "", e, fontTotal, l_y, 3);
            }

            double change = mOrder.Change;
            if (Class.Round.RoundSubTotal(Convert.ToDouble(money.Format(change))) > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                if (mOrder.CashOut != 0)
                {
                    if(mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Rút tiền:", e, fontTotal, l_y, 1);
                    }
                    else {DrawString("Cash out:", e, fontTotal, l_y, 1); }
                    
                    l_y = DrawString("$" + money.Format2(Math.Abs(change)), e, fontTotal, l_y, 3);
                }
            }

            if (mIsDriverOff == false)
            {
                if (mOrder.Card > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        DrawString("Thẻ:", e, fontTotal, l_y, 1);
                    }
                    else { DrawString("Card:", e, fontTotal, l_y, 1); }
                    l_y = DrawString("$" + money.Format2(mOrder.Card), e, fontTotal, l_y, 3);
                    foreach (OrderByCard c in mOrder.MoreOrderByCard)
                    {
                        if (c.subtotal > 0)
                        {
                            fontTotal = new System.Drawing.Font("Arial", 11);
                            DrawString("  * " + c.nameCard + ":", e, fontTotal, l_y, 1);
                            l_y = DrawString("$" + money.Convert3to2(c.subtotal), e, fontTotal, l_y, 3);
                        }
                    }
                }
            }

            l_y += m_printer.GetHeightPrinterLine() * 2;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                l_y = DrawString("Mục có dấu * là không có thuế", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 2);
            }
            else { l_y = DrawString("Items with * is no GST", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 2); }
            l_y += m_printer.GetHeightPrinterLine() / 2;

            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += m_printer.GetHeightPrinterLine() / 2;

            try
            {
                if (Convert.ToInt32(mOrder.AmtCash + mOrder.AmtPurchase) != 0)
                {
                    Class.LogPOS.WriteLog("BarPrinterServer.printBuilt.TaxInvoice:::Begin");
                    try
                    {
                        l_y += m_printer.GetHeightPrinterLine();
                        l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                        l_y += m_printer.GetHeightPrinterLine();
                        l_y = DrawString(" ... " + mOrder.Pan.Substring(mOrder.Pan.Length - 4, 4), e, new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Italic), l_y, 1);
                    }
                    catch (Exception err1)
                    {
                        Class.LogPOS.WriteLog("Loi kai Pan:::End:" + err1.Message + ":" + mOrder.Pan);
                    }

                    try
                    {
                        l_y += m_printer.GetHeightPrinterLine();
                        l_y = DrawString(mOrder.AccountType + " A/C", e, new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Italic), l_y, 1);
                    }
                    catch (Exception err2)
                    {
                        Class.LogPOS.WriteLog("Loi kai AccountType:::End:" + err2.Message + ":" + mOrder.AccountType);
                    }

                    try
                    {
                        DrawString(mOrder.CardType, e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        l_y = DrawString(mOrder.DateExpiry.Substring(0, 2) + " " + mOrder.DateExpiry.Substring(2, 2), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err3)
                    {
                        Class.LogPOS.WriteLog("Loi kai DateExpiry:::End:" + err3.Message + ":" + mOrder.DateExpiry);
                    }

                    try
                    {
                        DrawString(mOrder.Date.Substring(0, 2) + "-" + mOrder.Date.Substring(2, 2) + "-" + mOrder.Date.Substring(4, 2), e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        l_y = DrawString(mOrder.Time.Substring(0, 2) + " " + mOrder.Time.Substring(2, 2), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err4)
                    {
                        Class.LogPOS.WriteLog("Loi kai Time:::End:" + err4.Message + ":" + mOrder.Time);
                    }

                    try
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            DrawString("PHÊ DUYỆT MÃ", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        }
                        else { DrawString("APPROVAL CODE", e, new System.Drawing.Font("Arial", 9), l_y, 1); }
                        
                        l_y = DrawString(mOrder.AuthCode, e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err5)
                    {
                        Class.LogPOS.WriteLog("Loi kai Time:::End:" + err5.Message + ":" + mOrder.AuthCode);
                    }

                    try
                    {
                        if (mOrder.AmtCash != 0)
                        {
                            l_y += m_printer.GetHeightPrinterLine();
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                DrawString("RÚT TIỀN", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                                DrawString("VNĐ", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                            }
                            else
                            {
                                DrawString("CASH OUT", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                                DrawString("AUD", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                            }
                            l_y = DrawString(money.FormatNorman(mOrder.AmtCash.ToString()), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                        }
                    }
                    catch (Exception err6)
                    {
                        Class.LogPOS.WriteLog("Loi kai AmtCash:::End:" + err6.Message + ":" + mOrder.AmtCash);
                    }

                    try
                    {
                        if (mOrder.AmtPurchase != 0)
                        {
                            l_y += m_printer.GetHeightPrinterLine();
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                DrawString("MUA", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                                DrawString("VNĐ", e, new System.Drawing.Font("Arial", 8), l_y, 2);
                            }
                            else
                            {
                                DrawString("PURCHASE", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                                DrawString("AUD", e, new System.Drawing.Font("Arial", 8), l_y, 2);
                            }
                            l_y = DrawString(money.FormatNorman(mOrder.AmtPurchase.ToString()), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                        }
                    }
                    catch (Exception err7)
                    {
                        Class.LogPOS.WriteLog("Loi kai AmtPurchase:::End:" + err7.Message + ":" + mOrder.AmtPurchase);
                    }

                    l_y += m_printer.GetHeightPrinterLine();
                    l_y = DrawString("---------", e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    l_y += m_printer.GetHeightPrinterLine();

                    try
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            DrawString("TỔNG CỘNG", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                            DrawString("VND", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                        }
                        else
                        {
                            DrawString("TOTAL", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                            DrawString("AUD", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                        }
                        l_y = DrawString(money.FormatNorman(Convert.ToString(mOrder.AmtCash + mOrder.AmtPurchase)), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err8)
                    {
                        Class.LogPOS.WriteLog("Loi kai AmtCash+AmtPurchase:::End:" + err8.Message + ":" + mOrder.AmtCash + mOrder.AmtPurchase);
                    }
                    Class.LogPOS.WriteLog("BarPrinterServer.printBuilt.TaxInvoice:::End");
                }
            }
            catch (Exception err)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.printBuilt.TaxInvoice:::End:" + err.Message);
            }

            l_y = DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            l_y = DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
        }

        //private void PrintChickenOrBar(System.Drawing.Printing.PrintPageEventArgs e, int function,bool isKitchen)
        //{
        //    float l_y = 0;
        //    //print
        //    string head = "BAR";
        //    if (isKitchen)
        //    {
        //        head = "KITCHEN";
        //    }
        //    float table_poision;
        //    l_y = DrawString(head, e, new System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Bold), l_y, 2);

        //    l_y += 100;

        //    l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);

        //    l_y += 100;

        //    DateTime dt = DateTime.Now;
        //    string date = dt.Day+"/"+dt.Month+"/"+dt.Year;
        //    DrawString(date, e, new System.Drawing.Font("Arial", 13), l_y, 1);

        //    string time = dt.ToShortTimeString();
        //    l_y = DrawString(time, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 3);

        //    table_poision = l_y;
        //    if (!m_table.Contains("TKA-") && m_table!="")
        //    {
        //        l_y = DrawString("TABLE#" + m_table, e, new System.Drawing.Font("Arial", 13), l_y, 3);
        //    }

        //    List<Class.ItemOrderK> list;
        //    if (function == 15)
        //    {
        //        list = getOrderOfTable(m_table, isKitchen);
        //    }
        //    else
        //    {
        //        list = getChangeOrder(m_table, isKitchen);
        //    }
        //    if (list.Count <= 0)
        //    {
        //        e.Cancel = true;
        //        return;
        //    }

        //    if (list[0].NumPeople>0)
        //    {
        //        l_y = DrawString("PEOPLE# "+list[0].NumPeople,e,new System.Drawing.Font("Arial",13),l_y,1);
        //    }

        //    l_y += 100;

        //    l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
        //    l_y += 100;
        //    //----------------------------------------------------------------

        //    foreach (Class.ItemOrderK item in list)
        //    {
        //        float y_cancel_start = l_y;
        //        l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
        //        DrawString(changeType[item.ChangeStatus], e, new System.Drawing.Font("Arial", 12), l_y, 3);

        //        l_y = DrawString(item.Qty + new String(' ', 3) + item.Name, e, new System.Drawing.Font("Arial", 12), l_y, 1);
        //        foreach (Class.ItemOptionID option in item.Option)
        //        {
        //            //l_y = DrawString(new String(' ', 6) + option.Name+" ("+option.Qty+")", e, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Italic), l_y, 1);
        //            l_y = DrawString(option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 3);
        //        }
        //        if (item.ChangeStatus==2)
        //        {
        //            DrawCancelLine(e, y_cancel_start, l_y);
        //        }
        //    }
        //    l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
        //    DrawString("ORDER#" + list[0].OrderID, e, new System.Drawing.Font("Arial", 13), table_poision, 1);
        //    //---------------------------------
        //    int itemcount = 0;
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        itemcount += list[i].Qty;
        //    }
        //    l_y += 200;

        //    string total = "TOTAL ITEMS:" + itemcount;
        //    System.Drawing.Font l_font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
        //    l_y = DrawLine(total, l_font, e, System.Drawing.Drawing2D.DashStyle.Custom, l_y, 1);
        //    l_y = DrawString(total, e, l_font, l_y, 1);
        //    l_y = DrawLine(total, l_font, e, System.Drawing.Drawing2D.DashStyle.Custom, l_y, 1);
        //}

        private void PrintChickenOrBar(System.Drawing.Printing.PrintPageEventArgs e, int function, bool isKitchen)
        {
            float l_y = 0;

            List<Class.ItemOrderK> list;
            if (function == 15)
            {
                list = getOrderOfTable(m_table, isKitchen);
            }
            else
            {
                list = getChangeOrder(m_table, isKitchen);
            }
            if (list.Count <= 0)
            {
                e.Cancel = true;
                return;
            }

            //print
            string head = "BAR";
            if (isKitchen)
            {
                if (Convert.ToInt32(list[0].IsDelivery) == 1)
                {
                    head = "KITCHEN DELIVERY";
                }
                else if (Convert.ToInt32(list[0].IsPickup) == 1)
                {
                    head = "KITCHEN PICKUP";
                }
                else
                {
                    head = "KITCHEN";
                }
            }

            float table_poision;
            l_y = DrawString(head, e, new System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Bold), l_y, 2);

            l_y += m_printer.GetHeightPrinterLine();

            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);

            l_y += m_printer.GetHeightPrinterLine();

            DateTime dt = DateTime.Now;
            string date = dt.Day + "/" + dt.Month + "/" + dt.Year;
            DrawString(date, e, new System.Drawing.Font("Arial", 13), l_y, 1);

            string time = dt.ToShortTimeString();
            l_y = DrawString(time, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 3);

            table_poision = l_y;
            if (!m_table.Contains("TKA-") && m_table != "")
            {
                l_y = DrawString("TABLE#" + m_table, e, new System.Drawing.Font("Arial", 13), l_y, 3);
            }

            if (list[0].NumPeople > 0)
            {
                l_y = DrawString("PEOPLE# " + list[0].NumPeople, e, new System.Drawing.Font("Arial", 13), l_y, 1);
            }

            l_y += m_printer.GetHeightPrinterLine();

            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
            l_y += m_printer.GetHeightPrinterLine();
            //----------------------------------------------------------------

            foreach (Class.ItemOrderK item in list)
            {
                if (item.Visual == 0)
                {
                    float y_cancel_start = l_y;
                    l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                    DrawString(changeType[item.ChangeStatus], e, new System.Drawing.Font("Arial", 12), l_y, 3);

                    l_y = DrawString(item.Qty + new String(' ', 3) + item.Name, e, new System.Drawing.Font("Arial", 12), l_y, 1);
                    foreach (Class.ItemOptionID option in item.Option)
                    {
                        //l_y = DrawString(new String(' ', 6) + option.Name+" ("+option.Qty+")", e, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Italic), l_y, 1);
                        l_y = DrawString(option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 3);
                    }
                    if (item.ChangeStatus == 2)
                    {
                        DrawCancelLine(e, y_cancel_start, l_y);
                    }
                }
            }
            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);

            //if (list[0].Delivery != 0)
            //{
            //    if (mOrder.CustCode != "" && mOrder.CustCode != null)
            //    {
            //        Connection.Connection conn = new Connection.Connection();
            //        DataTable dtSourceCustomer = new DataTable();
            //        dtSourceCustomer = conn.Select("SELECT c.Name,c.memberNo,c.streetName,c.mobile FROM customers c WHERE c.memberNo = " + mOrder.CustCode);

            //        //l_y = DrawString("ACC      # " + dtSourceCustomer.Rows[0]["memberNo"].ToString().Substring(0, 3) + "..." +
            //        //    dtSourceCustomer.Rows[0]["memberNo"].ToString().Substring(dtSourceCustomer.Rows[0]["memberNo"].ToString().Length - 3, 3), e, font11, l_y, 1);
            //        l_y = DrawString("ACC NAME # " + dtSourceCustomer.Rows[0]["Name"].ToString().ToUpper(), e, new System.Drawing.Font("Arial", 12), l_y, 1);
            //        l_y = DrawString("ADDRESS # " + dtSourceCustomer.Rows[0]["streetName"].ToString().ToUpper(), e, new System.Drawing.Font("Arial", 12), l_y, 1);
            //        l_y = DrawString("MOBILE # " + dtSourceCustomer.Rows[0]["mobile"].ToString().ToUpper(), e, new System.Drawing.Font("Arial", 12), l_y, 1);
            //    }
            //}

            DrawString("ORDER#" + list[0].OrderID, e, new System.Drawing.Font("Arial", 13), table_poision, 1);
            //---------------------------------
            int itemcount = 0;
            for (int i = 0; i < list.Count; i++)
            {
                itemcount += list[i].Qty;
            }
            l_y += m_printer.GetHeightPrinterLine() * 2;

            string total = "TOTAL ITEMS:" + itemcount;
            System.Drawing.Font l_font = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            l_y = DrawLine(total, l_font, e, System.Drawing.Drawing2D.DashStyle.Custom, l_y, 1);
            l_y = DrawString(total, e, l_font, l_y, 1);
            l_y = DrawLine(total, l_font, e, System.Drawing.Drawing2D.DashStyle.Custom, l_y, 1);
        }

        private List<Class.ItemOrderK> getOrderBill(string table, int orderID)
        {
            List<Class.ItemOrderK> result = new List<Class.ItemOrderK>();
            try
            {
                string sql =
                        "select o.clients," +
                            "ol.subTotal," +
                            "ol.gst," +
                            "ol.weight," +
                            "o.discount," +
                            "o.orderID," +
                            "if(ol.itemID<>0,ol.itemID,ol.dynID) as itemID," +
                            "ol.optionID," +
                            "1 AS Visual," +
                            "(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "if(itemID<>0,(select gst from itemsmenu i where i.itemID=ol.itemID),0) as gst," +
                            "ol.qty," +
                            "ol.pumpID " +
                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "where o.completed<>2 and o.completed<>1 and o." + "'" + m_table + "'";
                //string sql = "select o.discount,o.orderID,ol.itemID,ol.subTotal,ol.optionID,(select i.optionEnable+g.grOption from groupsmenu g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as optionEnable,(select itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as itemOption,ol.qty from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID where o.completed<>1 and o.completed<>2 and o.tableID=" + table;
                if (orderID != 0)
                {
                    //sql = "select o.discount,o.orderID,ol.itemID,ol.subTotal,ol.optionID,(select i.optionEnable+g.grOption from groupsmenu g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as optionEnable,(select itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as itemOption,ol.qty from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID where o.orderID=" + orderID;
                    sql =
                        "select " +
                            "ol.weight," +
                            "o.clients," +
                            "ol.subTotal," +
                            "ol.gst," +
                            "o.discount," +
                            "o.orderID," +
                            "o.IsDelivery," +
                            "o.IsPickup," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "1 AS Visual," +
                            "ol.price," +
                        //"if(ol.itemID<>0,(select unitPrice from itemsmenu where itemID=ol.itemID),if(ol.dynID<>0,(select unitPrice from dynitemsmenu where dynID=ol.dynID),(select unitPrice from itemslinemenu where lineID=ol.optionID))) as price," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "if(itemID<>0,(select gst from itemsmenu i where i.itemID=ol.itemID),0) as gst," +
                            "ol.qty," +
                            "ol.pumpID " +
                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "where o.orderID=" + orderID;
                }
                System.Data.DataTable tbl = m_con.Select(sql);
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        result.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 1, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                        }
                        else
                        {
                            if (result.Count == 0 || result[result.Count - 1].GRShortCut == 1)
                            {
                                result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                            }
                            else
                            {
                                result[result.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.getOrderBill:::" + ex.Message);
            }
            //return ProcessList(result);
            return result;
        }

        private List<Class.ItemOrderK> getChangeOrder(string tableID, bool isKitchen)
        {
            List<Class.ItemOrderK> result = new List<Class.ItemOrderK>();
            try
            {
                //select o.orderID,
                //      ol.itemID,
                //      ol.optionID,
                //      (select optionEnable from itemsmenu i where i.itemID=ol.itemID) as optionEnable,
                //      (select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as grShortCut,
                //      if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID)) as itemDesc,
                //      ol.qty
                //from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID
                //where o.completed=0 and o.tableID=11;
                string sql = "select c.orderID," +
                             "c.dynID," +
                             "c.itemID," +
                             "c.optionID," +
                             "(select o.IsPickup from ordersdaily o where o.tableID=" + "'" + m_table + "'" + " and o.completed=0 limit 0,1) AS IsPickup," +
                             "(select o.IsDelivery from ordersdaily o where o.tableID=" + "'" + m_table + "'" + " and o.completed=0 limit 0,1) AS IsDelivery," +
                             "(select visual from itemsmenu i where i.itemID=c.itemID) AS Visual," +
                    //"(select optionEnable from itemsmenu i where i.itemID=c.itemID) as optionEnable,"+
                             "if(c.itemID<>0,(select itemDesc from itemsmenu i where i.itemID=c.itemID),if(c.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=c.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=c.dynID))) as itemDesc," +
                             "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=c.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=c.itemID)) as grShortCut," +
                             "c.qty," +
                             "c.`changed` " +
                             "from changedorders c where c.orderID=(select o.orderID from ordersdaily o where o.tableID=" + "'" + m_table + "'" + " and o.completed=0 limit 0,1) " +
                    //"order by lineID";
                             "";
                if (KitchenName != null && KitchenName != "")
                {
                    if (isKitchen)
                    {
                        sql += " and (" +
                                        "c.itemID=(select si.itemID from itemsmenu si where si.itemID=c.itemID and si.printerType<>2) or " +
                                        "c.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=c.optionID and i.printerType<>2) or " +
                                        "(select d.printerType from dynitemsmenu d where d.dynID=c.dynID)<>2 " +
                                    ")";
                    }
                    else
                    {
                        sql += " and (" +
                                        "c.itemID=(select si.itemID from itemsmenu si where si.itemID=c.itemID and si.printerType=2) or " +
                                        "c.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=c.optionID and i.printerType=2) or " +
                                        "(select d.printerType from dynitemsmenu d where d.dynID=c.dynID)=2 " +
                                     ")";
                    }
                }
                System.Data.DataTable tbl = m_con.Select(sql);
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        result.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), Convert.ToInt16(row["changed"]), 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 1, 0, 0, 0, Convert.ToInt32(row["Visual"].ToString()), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), Convert.ToInt16(row["changed"]), 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0, 0, 0, 0, Convert.ToInt32(row["Visual"].ToString()), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                        }
                        else
                        {
                            if (result.Count == 0 || result[result.Count - 1].GRShortCut == 1)
                            {
                                result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), Convert.ToInt16(row["changed"]), 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0, 0, 0, 0, Convert.ToInt32(row["Visual"].ToString()), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                            }
                            else
                            {
                                result[result.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), 0, Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.getChangeOrder:::" + ex.Message);
            }
            //result = ProcessList(result);
            return result;
        }

        private List<Class.ItemOrderK> getOrderOfTable(string tableID, bool isKitchen)
        {
            //Code Old 21-09-2012
            //List<Class.ItemOrderK> result = new List<Class.ItemOrderK>();
            //try
            //{
            //    string sql =
            //            "select " +
            //                "o.clients," +
            //                "o.orderID," +
            //                "o.weight," +
            //                "ol.dynID," +
            //                "ol.itemID," +
            //                "ol.optionID," +
            //                "1 AS Visual," +
            //                "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
            //                "if(optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),if(ol.dynID=0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
            //                "ol.qty," +
            //                "ol.pumpID " +
            //            "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
            //            "where o.completed=0 and o.tableID=" + "'" + m_table + "'";
            //    if (KitchenName != null && KitchenName != "")
            //    {
            //        if (isKitchen)
            //        {
            //            sql +=
            //                " and (" +
            //                        "ol.itemID=(select si.itemID from itemsmenu si where si.itemID=ol.itemID and si.printerType<>2) or " +
            //                        "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=ol.optionID and i.printerType<>2) or " +
            //                        "(select d.printerType from dynitemsmenu d where d.dynID=ol.dynID)<>2 " +
            //                      ")";
            //        }
            //        else
            //        {
            //            sql +=
            //                " and (" +
            //                        "ol.itemID=(select si.itemID from itemsmenu si where si.itemID=ol.itemID and si.printerType=2) or " +
            //                        "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=ol.optionID and i.printerType=2) or " +
            //                        "(select d.printerType from dynitemsmenu d where d.dynID=ol.dynID)=2 " +
            //                      ")";
            //        }
            //    }
            //    System.Data.DataTable tbl = m_con.Select(sql);
            //    foreach (System.Data.DataRow row in tbl.Rows)
            //    {
            //        if (row["dynID"].ToString() != "0")
            //        {
            //            result.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 1, Convert.ToInt32(row["weight"]), 0, Convert.ToInt32(row["pumpID"]), Convert.ToInt32(row["Visual"])));
            //        }
            //        else
            //        {
            //            if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
            //            {
            //                result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt32(row["weight"]), 0, Convert.ToInt32(row["pumpID"]), Convert.ToInt32(row["Visual"])));
            //            }
            //            else
            //            {
            //                if (result.Count == 0 || result[result.Count - 1].GRShortCut == 1)
            //                {
            //                    result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt32(row["weight"]), 0, Convert.ToInt32(row["pumpID"]), Convert.ToInt32(row["Visual"])));
            //                }
            //                else
            //                {
            //                    result[result.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), 0, Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Class.LogPOS.WriteLog("BarPrinterServer.getOrderOfTable:::" + ex.Message);
            //}
            //result = ProcessList(result);
            //return result;
            List<Class.ItemOrderK> result = new List<Class.ItemOrderK>();
            try
            {
                string sql =
                        "select " +
                            "o.clients," +
                            "o.orderID," +
                            "o.custID," +
                            "o.CustCode," +
                            "o.IsDelivery," +
                            "o.IsPickup," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),if(ol.dynID=0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "ol.qty " +
                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "where o.completed=0 and o.tableID=" + "'" + m_table + "'";
                if (KitchenName != null && KitchenName != "")
                {
                    if (isKitchen)
                    {
                        //sql +=
                        //    " and (" +
                        //            "ol.itemID=(select si.itemID from itemsmenu si where si.itemID=ol.itemID and si.printerType<>2 and si.visual <> 1) or " +
                        //            "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=ol.optionID and i.printerType<>2 and i.visual <> 1) or " +
                        //            "(select d.printerType from dynitemsmenu d where d.dynID=ol.dynID)<>2 " +
                        //          ")";

                        sql +=
                            " and (" +
                                    "ol.itemID=(select si.itemID from itemsmenu si inner join groupsmenu gm on si.groupID = gm.groupID where si.itemID=ol.itemID and si.printerType<>2 and si.visual <> 1 and gm.isFF =1) or " +
                                    "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID inner join groupsmenu gm on i.groupID = gm.groupID where il.lineID=ol.optionID and i.printerType<>2 and i.visual <> 1 and gm.isFF =1) " +
                                  ")";
                    }
                    else
                    {
                        //sql +=
                        //    " and (" +
                        //            "ol.itemID=(select si.itemID from itemsmenu si where si.itemID=ol.itemID and si.printerType<>1 and si.visual <> 1) or " +
                        //            "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=ol.optionID and i.printerType<>1 and i.visual <> 1) or " +
                        //            "(select d.printerType from dynitemsmenu d where d.dynID=ol.dynID)<>1 " +
                        //          ")";

                        sql +=
                            " and (" +
                                    "ol.itemID=(select si.itemID from itemsmenu si where si.itemID=ol.itemID and si.printerType<>1 and si.visual <> 1) or " +
                                    "ol.optionID in(select il.lineID from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=ol.optionID and i.printerType<>1 and i.visual <> 1) " +
                                  ")";
                    }
                }
                System.Data.DataTable tbl = m_con.Select(sql);
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        result.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 1, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                        }
                        else
                        {
                            if (result.Count == 0 || result[result.Count - 1].GRShortCut == 1)
                            {
                                result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), 0, Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, 0, Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                            }
                            else
                            {
                                result[result.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), 0, Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.getOrderOfTable:::" + ex.Message);
            }
            //result = ProcessList(result);
            return result;
        }

        private List<Class.ItemOrderK> ProcessList(List<Class.ItemOrderK> result)
        {
            int count = result.Count;
            bool key;
            for (int i = 0; i < count; i++)
            {
                for (int j = i + 1; j < count; j++)
                {
                    if (result[i].ItemID == result[j].ItemID && result[i].Option.Count == result[j].Option.Count && result[i].ItemType == result[j].ItemType && (result[i].Weight <= 1 || result[j].Weight <= 1))
                    {
                        key = true;
                        for (int k = 0; k < result[i].Option.Count; k++)
                        {
                            if (result[i].Option[k].OptionID != result[j].Option[k].OptionID || result[i].Option[k].Qty != result[j].Option[k].Qty || result[i].Option[k].ItemID != result[j].Option[k].ItemID)
                            {
                                key = false;
                                break;
                            }
                        }
                        if (key)
                        {
                            if (result[i].PumpID == result[j].PumpID)
                            {
                                result[i].Qty += result[j].Qty;
                                result[i].Total += result[j].Total;
                                for (int l = 0; l < result[i].Option.Count; l++)
                                {
                                    result[i].Option[l].Qty += result[j].Option[l].Qty;
                                    result[i].Option[l].Price += result[j].Option[l].Qty;
                                }
                                result.RemoveAt(j);
                            }
                            j--;
                            count--;
                        }
                    }
                }
            }
            return result;
        }

        private void m_socketserver_Received(SocketServer.SocketEventArgs e)
        {
            if (e.Table != "")
            {
                lock ("BarPrinterSerReceived")
                {
                    print(e.Table, e.Function);
                }
            }
        }

        /// <summary>
        /// in
        /// </summary>
        /// <param name="table">so ban</param>
        /// <param name="function">15 la new 16 la change</param>
        public void print(string table, int function)
        {
            //New khong cho in thi return
            if (!mEnablePrintKitchenAndBar)
            {
                return;
            }
            try
            {
                m_table = table;
                m_function = function;
                mIsPrintBuilt = false;
                if (KitchenName == null || KitchenName == "")
                {
                    misKitchen = true;
                    m_printer.printDocument.PrinterSettings.PrinterName = BarName;
                    m_printer.Print();
                }
                else
                {
                    m_printer.printDocument.PrinterSettings.PrinterName = KitchenName;
                    misKitchen = true;
                    m_printer.Print();
                    m_printer.printDocument.PrinterSettings.PrinterName = BarName;
                    misKitchen = false;
                    m_printer.Print();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.print:::" + ex.Message);
            }
        }

        public void printBuilt(bool isBuildName, Class.ProcessOrderNew.Order order)
        {
            try
            {
                mIsPrintBuilt = true;
                mIsBuiltName = isBuildName;
                mOrder = order;
                m_table = order.TableID;
                m_printer.printDocument.PrinterSettings.PrinterName = BillName;
                m_printer.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.printBuilt:::" + ex.Message);
            }
        }

        private string[] arrayDataReceive;

        public void printBuilt(bool isBuildName, Class.ProcessOrderNew.Order order, string[] arrayDataReceive)
        {
            try
            {
                mIsPrintBuilt = true;
                mIsBuiltName = isBuildName;
                mOrder = order;
                this.arrayDataReceive = arrayDataReceive;
                m_table = order.TableID;
                m_printer.printDocument.PrinterSettings.PrinterName = BillName;
                m_printer.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.printBuilt:::" + ex.Message);
            }
        }

        private double dblSubtotalCashOut;

        internal void PrintCashInOrCashOut(double dblCashOut)
        {
            dblSubtotalCashOut = dblCashOut;
            m_printer.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage2);
            m_printer.printDocument.Print();
        }

        private void printDocument_PrintPage2(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = dbconfig.Header1.ToString();
            string bankCode = dbconfig.Header2.ToString();
            string address = dbconfig.Header3.ToString();
            string tell = dbconfig.Header4.ToString();

            string website = dbconfig.FootNode1.ToString();
            string thankyou = dbconfig.FootNode2.ToString();

            l_y = m_printer.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = m_printer.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = m_printer.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = m_printer.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += 100;

            l_y = m_printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;
            DateTime dateTime = DateTime.Now;
            l_y = m_printer.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            l_y = m_printer.DrawString("Cash Out ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);

            //l_y += 100;
            //l_y = mPrinter.DrawString(DateTime.Now.ToShortDateString(), e, new Font("Arial", 10, FontStyle.Italic), l_y, 3);
            l_y += 20;

            m_printer.DrawString("Total Cash ", e, new Font("Arial", 10), l_y, 1);
            l_y = m_printer.DrawString("$" + money.Format2(dblSubtotalCashOut), e, new Font("Arial", 10), l_y, 3);

            l_y += 100;
            l_y = m_printer.DrawString("", e, new Font("Arial", 10), l_y, 1);
        }
    }
}