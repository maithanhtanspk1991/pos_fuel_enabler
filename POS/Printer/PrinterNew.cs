﻿using System;
using System.Drawing.Printing;

namespace POS
{
    internal class PrinterNew : PrintDocument
    {
        public object Tag { get; set; }

        private Class.ReadConfig readconfig = new Class.ReadConfig();

        public string PrinterName
        {
            get { return this.PrinterSettings.PrinterName; }
            set { this.PrinterSettings.PrinterName = value; }
        }

        public PrinterNew()
        {
            this.PrintController = new StandardPrintController();
        }

        public int GetHeightPrinterLine()
        {
            if (readconfig.PrinterModel == "PRP-085")
            {
                return 100;
            }
            else
            {
                return 10;
            }
        }

        public float DrawString(string s, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font, float y, int textAlign)
        {
            float x;
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawString(s, font, System.Drawing.Brushes.Black, x, y);
            y += e.Graphics.MeasureString(s, font).Height;

            return y;
        }

        public void DrawCancelLine(System.Drawing.Printing.PrintPageEventArgs e, float y_start, float y_end)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Black);
            e.Graphics.DrawLine(pen, 0, y_start, e.PageBounds.Width, y_end);
            e.Graphics.DrawLine(pen, 0, y_end, e.PageBounds.Width, y_start);
        }

        public float DrawLine(string s, System.Drawing.Font font, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Drawing2D.DashStyle dashStyle, float y, int textAlign)
        {
            float x;
            float width;
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Black);
            if (dashStyle != System.Drawing.Drawing2D.DashStyle.Custom)
            {
                pen.DashStyle = dashStyle;
            }
            if (s == "" || s == null)
            {
                width = e.PageBounds.Width;
            }
            else
            {
                width = e.Graphics.MeasureString(s, font).Width;
            }
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawLine(pen, x, y, x + width, y);
            y += 2;
            return y;
        }
    }
}