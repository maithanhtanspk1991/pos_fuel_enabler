﻿using System;
using System.Collections.Generic;
using System.Data;

namespace POS
{
    internal class PrinterHistory
    {
        private Connection.Connection mConnection;

        private Class.MoneyFortmat mMoneyFortmat;

        private POS.Printer mPrinter;

        private Connection.ReadDBConfig mReadDBConfig;


        public PrinterHistory(Connection.ReadDBConfig readDBConfig, Class.MoneyFortmat moneyFortmat)
        {
            mPrinter = new Printer();
            mConnection = new Connection.Connection();
            mReadDBConfig = readDBConfig;
            mMoneyFortmat = moneyFortmat;
        }

        public void Print(int BKID, int OrderID, int balance)
        {
            mPrinter.SetPrinterName(mReadDBConfig.BillPrinter.ToString());
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mPrinter.Tag = new Order(BKID, OrderID, balance);
            mPrinter.Print();
        }

        private Class.ProcessOrderNew.Order GetOrder(Order item, ref List<Class.ItemOrderK> list)
        {
            list = new List<Class.ItemOrderK>();
            Class.ProcessOrderNew.Order order = new Class.ProcessOrderNew.Order();
            try
            {
                mConnection.Open();
                string sql = "";
                string sqlDetail = "";
                if (item.BkId > 0)
                {
                    sql = "select * from ordersall where bkId=" + item.BkId;
                    sqlDetail =
                       "select " +
                           "ol.subTotal," +
                           "ol.dynID," +
                           "ol.dynID," +
                           "ol.itemID," +
                           "ol.optionID," +
                           "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                           "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),'Dym Item')) as itemDesc," +
                           "ol.qty " +
                       "from ordersallline ol " +
                       "where ol.bkId=" + item.BkId;
                }
                else
                {
                    sql = "select * from ordersdaily where orderID=" + item.OrderID;
                    sqlDetail =
                       "select " +
                           "ol.subTotal," +
                           "ol.dynID," +
                           "ol.dynID," +
                           "ol.itemID," +
                           "ol.optionID," +
                           "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                           "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                           "ol.qty " +
                       "from ordersdailyline ol " +
                       "where ol.orderID=" + item.OrderID;
                }
                System.Data.DataTable tbl = mConnection.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Tendered = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.Customer.CustID = Convert.ToInt16(tbl.Rows[0]["custID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);                    
                    order.RegoID = Convert.ToInt32(tbl.Rows[0]["managecar"]);
                }
                System.Data.DataTable tblListItem = mConnection.Select(sqlDetail);
                foreach (System.Data.DataRow row in tblListItem.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        list.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 1));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                        }
                        else
                        {
                            if (list.Count == 0 || list[list.Count - 1].GRShortCut == 1)
                            {
                                list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                            }
                            else
                            {
                                list[list.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if (list[i].Compare(list[j]))
                        {
                            list[i].AddSameItem(list[j]);
                            list.RemoveAt(j);
                            j--;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                mConnection.Close();
            }
            return order;
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            PrintOrderHistory(sender, e);
        }

        private void PrintOrderHistory(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Order Order = (Order)mPrinter.Tag;
            try
            {
                //=============================================
                System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
                float l_y = 0;
                string header = "";
                string bankCode = "";
                string address = "";
                string tell = "";

                string website = "";
                string thankyou = "";
                try
                {
                    header = mReadDBConfig.Header1.ToString();
                    bankCode = mReadDBConfig.Header2.ToString();
                    address = mReadDBConfig.Header3.ToString();
                    tell = mReadDBConfig.Header4.ToString();

                    website = mReadDBConfig.FootNode1.ToString();
                    thankyou = mReadDBConfig.FootNode2.ToString();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("printBuilt(Config):::" + ex.Message);
                }

                l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 18), l_y, 2);
                l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
                l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
                l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

                l_y += mPrinter.GetHeightPrinterLine();

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                List<Class.ItemOrderK> list = null;
                Class.ProcessOrderNew.Order mOrder = GetOrder(Order, ref list);
                if (mOrder.OrderID == 0)
                {
                    e.Cancel = true;
                    return;
                }

                l_y += mPrinter.GetHeightPrinterLine();
                DateTime dateTime = mOrder.DateOrder;
                l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
                if (!mOrder.TableID.Contains("TKA-") && mOrder.TableID != "")
                {
                    mPrinter.DrawString("TABLE# " + mOrder.TableID, e, font11, l_y, 3);
                }
                l_y = mPrinter.DrawString("ORDER# " + mOrder.OrderID, e, font11, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine();
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine();
                string sBuilt = "ORDER HISTORY";

                l_y = mPrinter.DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);

                l_y += mPrinter.GetHeightPrinterLine();

                double subTotal = 0;
                foreach (Class.ItemOrderK item in list)
                {
                    float yStart = l_y;
                    if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                    {
                        l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                        yStart = l_y;
                        l_y = mPrinter.DrawString(item.Qty + new String(' ', 3) + item.Name, e, new System.Drawing.Font("Arial", 8), l_y, 1);
                    }
                    foreach (Class.ItemOptionID option in item.Option)
                    {
                        l_y = mPrinter.DrawString(new String(' ', 6) + option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 1);
                        item.Price += option.Price;
                    }
                    if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                    {
                        mPrinter.DrawString(mMoneyFortmat.Format2(item.Price), e, new System.Drawing.Font("Arial", 11), yStart, 3);
                    }
                    subTotal += item.Price;
                }
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine() / 2;

                System.Drawing.Font fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
                mPrinter.DrawString("Total:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.SubTotal), e, fontTotal, l_y, 3);
                l_y += mPrinter.GetHeightPrinterLine() / 2;

                if (mOrder.Discount > 0)
                {
                    l_y += mPrinter.GetHeightPrinterLine() / 2;
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Discount:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Discount), e, fontTotal, l_y, 3);

                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Balance:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.SubTotal - mOrder.Discount), e, fontTotal, l_y, 3);
                }

                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("GST(included in total):", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(mMoneyFortmat.Format2((mOrder.SubTotal - mOrder.Discount) / 11), e, fontTotal, l_y, 3);

                if (mOrder.Deposit > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Deposit:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Deposit), e, fontTotal, l_y, 3);
                }
                double balanceAccount = 0;
                if (mOrder.Card > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Card:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Card), e, fontTotal, l_y, 3);
                }
                try
                {
                    if (mOrder.Customer.CustID != 0)
                    {
                        Connection.Connection conn = new Connection.Connection();
                        DataTable dtSourceCustomer = new DataTable();
                        mOrder.Customer = BusinessObject.BOCustomers.GetByID(mOrder.Customer.CustID);
                        mOrder.Customer.Balance = Order.Balance;
                        if (mOrder.Customer.Company == 0)
                        {
                            l_y = mPrinter.DrawString("Code: " + mOrder.Customer.MemberNoShort, e, font11, l_y, 1);
                            if (mOrder.Customer.FirstName != "")
                                l_y = mPrinter.DrawString("Name: " + mOrder.Customer.FirstName + " " + mOrder.Customer.LastName, e, font11, l_y, 1);
                            //if (mOrder.Customer.LastName != "")
                            //    l_y = DrawString("Last Name: " + mOrder.Customer.LastName, e, font11, l_y, 1);
                        }
                        else
                        {
                            DataObject.Company company = BusinessObject.BOCompany.GetByID(mOrder.Customer.Company);
                            l_y = mPrinter.DrawString("Comany code: " + company.CompanyCodeShort, e, font11, l_y, 1);
                            l_y = mPrinter.DrawString("Comany Name: " + company.CompanyName, e, font11, l_y, 1);
                        }

                        if (mOrder.RegoID > 0)
                        {
                            DataObject.Rego rego = BusinessObject.BORego.GetByID(mOrder.RegoID);
                            l_y = mPrinter.DrawString("Rego Number: " + rego.CarNumber, e, font11, l_y, 1);
                            l_y = mPrinter.DrawString("Car Product: " + rego.CarProduct, e, font11, l_y, 1);
                        }

                    }
                }
                catch
                {
                }

                if (mOrder.Account > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    if (mOrder.Customer.Company == 0)
                    {
                        mPrinter.DrawString("Openning Balance:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2((mOrder.Customer.Balance - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        mPrinter.DrawString("Pay Amount:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        mPrinter.DrawString("Closing Balance:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Customer.Balance * -1), e, fontTotal, l_y, 3);
                    }
                    else
                    {
                        DataObject.Company company = BusinessObject.BOCompany.GetByID(mOrder.Customer.Company);
                        mPrinter.DrawString("Openning Balance:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2((company.Debt - mOrder.Account) * -1), e, fontTotal, l_y, 3);

                        mPrinter.DrawString("Pay Amount:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Account), e, fontTotal, l_y, 3);
                        mPrinter.DrawString("Closing Balance:", e, fontTotal, l_y, 1);
                        l_y = mPrinter.DrawString(mMoneyFortmat.Format2(company.Debt * -1), e, fontTotal, l_y, 3);
                    }

                    l_y += mPrinter.GetHeightPrinterLine();
                    l_y = mPrinter.DrawString("SIGNATURE:...................................", e, fontTotal, l_y, 3);
                }

                if (mOrder.Tendered > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Cash:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(mOrder.Tendered) + "", e, fontTotal, l_y, 3);
                }

                double change = mOrder.Tendered + mOrder.Deposit + mOrder.Card - (mOrder.SubTotal - mOrder.Discount);

                if (change > 0)
                {
                    fontTotal = new System.Drawing.Font("Arial", 11);
                    mPrinter.DrawString("Change:", e, fontTotal, l_y, 1);
                    l_y = mPrinter.DrawString(mMoneyFortmat.Format2(change) + "", e, fontTotal, l_y, 3);
                }

                l_y += mPrinter.GetHeightPrinterLine() * 2;

                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

                l_y += mPrinter.GetHeightPrinterLine();

                l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
                l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            }
            catch (Exception ex)
            {
                e.Cancel = true;
            }
        }

        private class Order
        {
            public Order(int bkId, int orderID, int balance)
            {
                BkId = bkId;
                OrderID = orderID;
                Balance = balance;
            }

            public int BkId { get; set; }

            public int Balance { get; set; }

            public int OrderID { get; set; }
        }
    }
}