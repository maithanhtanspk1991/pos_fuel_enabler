﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCNewItemLine : UserControl
    {
        public UCNewItemLine()
        {
            InitializeComponent();
        }

        //private void textBox6_Enter(object sender, EventArgs e)
        //{
        //    Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox6);
        //    textBox6.Tag = textBox6.BackColor;
        //    textBox6.BackColor = Color.White;
        //    frm.ShowDialog();
        //}

        private void textBox1_Leave(object sender, EventArgs e)
        {
            textBox1.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            textBox2.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            textBox3.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            textBox4.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox5_Leave(object sender, EventArgs e)
        {
            textBox5.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox1);
            textBox1.Tag = textBox1.BackColor;
            textBox1.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBox2);
            textBox2.Tag = textBox2.BackColor;
            textBox2.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox3);
            textBox3.Tag = textBox3.BackColor;
            textBox3.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox5_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox5);
            textBox5.Tag = textBox5.BackColor;
            textBox5.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox4_Click(object sender, EventArgs e)
        {
            Forms.frmKeyPad frm = new Forms.frmKeyPad(textBox4);
            textBox4.Tag = textBox4.BackColor;
            textBox4.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                textBox2.Text = textBox1.Text;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}