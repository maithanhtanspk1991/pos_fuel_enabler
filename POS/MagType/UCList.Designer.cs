﻿namespace POS.MagType
{
    partial class UCList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.lbFoundRecords = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLastPage = new System.Windows.Forms.Button();
            this.btmNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnFirstPage = new System.Windows.Forms.Button();
            this.btnGoPage = new System.Windows.Forms.Button();
            this.lbPage = new System.Windows.Forms.Label();
            this.pnContent = new System.Windows.Forms.Panel();
            this.tlpItem = new System.Windows.Forms.TableLayoutPanel();
            this.ucItem10 = new POS.MagType.UCItem();
            this.ucItem9 = new POS.MagType.UCItem();
            this.ucItem8 = new POS.MagType.UCItem();
            this.ucItem7 = new POS.MagType.UCItem();
            this.ucItem6 = new POS.MagType.UCItem();
            this.ucItem5 = new POS.MagType.UCItem();
            this.ucItem4 = new POS.MagType.UCItem();
            this.ucItem3 = new POS.MagType.UCItem();
            this.ucItem2 = new POS.MagType.UCItem();
            this.ucItem1 = new POS.MagType.UCItem();
            this.ucTitle1 = new POS.MagType.UCTitle();
            this.pnTop.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnContent.SuspendLayout();
            this.tlpItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnTop.Controls.Add(this.lbFoundRecords);
            this.pnTop.Controls.Add(this.lbTitle);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(492, 52);
            this.pnTop.TabIndex = 0;
            // 
            // lbFoundRecords
            // 
            this.lbFoundRecords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbFoundRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFoundRecords.Location = new System.Drawing.Point(204, 0);
            this.lbFoundRecords.Margin = new System.Windows.Forms.Padding(0);
            this.lbFoundRecords.Name = "lbFoundRecords";
            this.lbFoundRecords.Size = new System.Drawing.Size(286, 50);
            this.lbFoundRecords.TabIndex = 1;
            this.lbFoundRecords.Text = "Found 0 Records";
            this.lbFoundRecords.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTitle
            // 
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(0, 0);
            this.lbTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(204, 50);
            this.lbTitle.TabIndex = 0;
            this.lbTitle.Text = "Title";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.tableLayoutPanel1);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 535);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(492, 52);
            this.pnBottom.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.btnLastPage, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btmNext, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFirstPage, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnGoPage, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbPage, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(492, 52);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnLastPage
            // 
            this.btnLastPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.btnLastPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLastPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLastPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLastPage.Location = new System.Drawing.Point(405, 0);
            this.btnLastPage.Margin = new System.Windows.Forms.Padding(0);
            this.btnLastPage.Name = "btnLastPage";
            this.btnLastPage.Size = new System.Drawing.Size(87, 52);
            this.btnLastPage.TabIndex = 0;
            this.btnLastPage.Text = "Last Page";
            this.btnLastPage.UseVisualStyleBackColor = false;
            this.btnLastPage.Click += new System.EventHandler(this.btnLastPage_Click);
            // 
            // btmNext
            // 
            this.btmNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.btmNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btmNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmNext.Location = new System.Drawing.Point(324, 0);
            this.btmNext.Margin = new System.Windows.Forms.Padding(0);
            this.btmNext.Name = "btmNext";
            this.btmNext.Size = new System.Drawing.Size(81, 52);
            this.btmNext.TabIndex = 1;
            this.btmNext.Text = "Next";
            this.btmNext.UseVisualStyleBackColor = false;
            this.btmNext.Click += new System.EventHandler(this.btmNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(243, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(81, 52);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnFirstPage
            // 
            this.btnFirstPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.btnFirstPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFirstPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFirstPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirstPage.Location = new System.Drawing.Point(162, 0);
            this.btnFirstPage.Margin = new System.Windows.Forms.Padding(0);
            this.btnFirstPage.Name = "btnFirstPage";
            this.btnFirstPage.Size = new System.Drawing.Size(81, 52);
            this.btnFirstPage.TabIndex = 3;
            this.btnFirstPage.Text = "First Page";
            this.btnFirstPage.UseVisualStyleBackColor = false;
            this.btnFirstPage.Click += new System.EventHandler(this.btnFirstPage_Click);
            // 
            // btnGoPage
            // 
            this.btnGoPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.btnGoPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGoPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoPage.ForeColor = System.Drawing.Color.White;
            this.btnGoPage.Location = new System.Drawing.Point(81, 0);
            this.btnGoPage.Margin = new System.Windows.Forms.Padding(0);
            this.btnGoPage.Name = "btnGoPage";
            this.btnGoPage.Size = new System.Drawing.Size(81, 52);
            this.btnGoPage.TabIndex = 4;
            this.btnGoPage.Text = "Go Page";
            this.btnGoPage.UseVisualStyleBackColor = false;
            this.btnGoPage.Click += new System.EventHandler(this.btnGoPage_Click);
            // 
            // lbPage
            // 
            this.lbPage.AutoSize = true;
            this.lbPage.BackColor = System.Drawing.Color.White;
            this.lbPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPage.Location = new System.Drawing.Point(0, 0);
            this.lbPage.Margin = new System.Windows.Forms.Padding(0);
            this.lbPage.Name = "lbPage";
            this.lbPage.Size = new System.Drawing.Size(81, 52);
            this.lbPage.TabIndex = 5;
            this.lbPage.Text = "1";
            this.lbPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnContent
            // 
            this.pnContent.Controls.Add(this.tlpItem);
            this.pnContent.Controls.Add(this.ucTitle1);
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(0, 52);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(492, 483);
            this.pnContent.TabIndex = 2;
            // 
            // tlpItem
            // 
            this.tlpItem.ColumnCount = 1;
            this.tlpItem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpItem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpItem.Controls.Add(this.ucItem10, 0, 9);
            this.tlpItem.Controls.Add(this.ucItem9, 0, 8);
            this.tlpItem.Controls.Add(this.ucItem8, 0, 7);
            this.tlpItem.Controls.Add(this.ucItem7, 0, 6);
            this.tlpItem.Controls.Add(this.ucItem6, 0, 5);
            this.tlpItem.Controls.Add(this.ucItem5, 0, 4);
            this.tlpItem.Controls.Add(this.ucItem4, 0, 3);
            this.tlpItem.Controls.Add(this.ucItem3, 0, 2);
            this.tlpItem.Controls.Add(this.ucItem2, 0, 1);
            this.tlpItem.Controls.Add(this.ucItem1, 0, 0);
            this.tlpItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpItem.Location = new System.Drawing.Point(0, 30);
            this.tlpItem.Name = "tlpItem";
            this.tlpItem.RowCount = 10;
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpItem.Size = new System.Drawing.Size(492, 453);
            this.tlpItem.TabIndex = 1;
            // 
            // ucItem10
            // 
            this.ucItem10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem10.Location = new System.Drawing.Point(0, 405);
            this.ucItem10.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem10.Name = "ucItem10";
            this.ucItem10.Size = new System.Drawing.Size(492, 48);
            this.ucItem10.TabIndex = 9;
            this.ucItem10.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem9
            // 
            this.ucItem9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem9.Location = new System.Drawing.Point(0, 360);
            this.ucItem9.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem9.Name = "ucItem9";
            this.ucItem9.Size = new System.Drawing.Size(492, 45);
            this.ucItem9.TabIndex = 8;
            this.ucItem9.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem8
            // 
            this.ucItem8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem8.Location = new System.Drawing.Point(0, 315);
            this.ucItem8.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem8.Name = "ucItem8";
            this.ucItem8.Size = new System.Drawing.Size(492, 45);
            this.ucItem8.TabIndex = 7;
            this.ucItem8.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem7
            // 
            this.ucItem7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem7.Location = new System.Drawing.Point(0, 270);
            this.ucItem7.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem7.Name = "ucItem7";
            this.ucItem7.Size = new System.Drawing.Size(492, 45);
            this.ucItem7.TabIndex = 6;
            this.ucItem7.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem6
            // 
            this.ucItem6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem6.Location = new System.Drawing.Point(0, 225);
            this.ucItem6.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem6.Name = "ucItem6";
            this.ucItem6.Size = new System.Drawing.Size(492, 45);
            this.ucItem6.TabIndex = 5;
            this.ucItem6.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem5
            // 
            this.ucItem5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem5.Location = new System.Drawing.Point(0, 180);
            this.ucItem5.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem5.Name = "ucItem5";
            this.ucItem5.Size = new System.Drawing.Size(492, 45);
            this.ucItem5.TabIndex = 4;
            this.ucItem5.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem4
            // 
            this.ucItem4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem4.Location = new System.Drawing.Point(0, 135);
            this.ucItem4.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem4.Name = "ucItem4";
            this.ucItem4.Size = new System.Drawing.Size(492, 45);
            this.ucItem4.TabIndex = 3;
            this.ucItem4.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem3
            // 
            this.ucItem3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem3.Location = new System.Drawing.Point(0, 90);
            this.ucItem3.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem3.Name = "ucItem3";
            this.ucItem3.Size = new System.Drawing.Size(492, 45);
            this.ucItem3.TabIndex = 2;
            this.ucItem3.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem2
            // 
            this.ucItem2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem2.Location = new System.Drawing.Point(0, 45);
            this.ucItem2.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem2.Name = "ucItem2";
            this.ucItem2.Size = new System.Drawing.Size(492, 45);
            this.ucItem2.TabIndex = 1;
            this.ucItem2.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucItem1
            // 
            this.ucItem1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucItem1.Location = new System.Drawing.Point(0, 0);
            this.ucItem1.Margin = new System.Windows.Forms.Padding(0);
            this.ucItem1.Name = "ucItem1";
            this.ucItem1.Size = new System.Drawing.Size(492, 45);
            this.ucItem1.TabIndex = 0;
            this.ucItem1.Click += new System.EventHandler(this.ucItem_Click);
            // 
            // ucTitle1
            // 
            this.ucTitle1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTitle1.Location = new System.Drawing.Point(0, 0);
            this.ucTitle1.Name = "ucTitle1";
            this.ucTitle1.Size = new System.Drawing.Size(492, 30);
            this.ucTitle1.TabIndex = 0;
            // 
            // UCList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.pnContent);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.Name = "UCList";
            this.Size = new System.Drawing.Size(492, 587);
            this.pnTop.ResumeLayout(false);
            this.pnBottom.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pnContent.ResumeLayout(false);
            this.tlpItem.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Label lbFoundRecords;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnLastPage;
        private System.Windows.Forms.Button btmNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFirstPage;
        private System.Windows.Forms.Button btnGoPage;
        private System.Windows.Forms.Label lbPage;
        private UCTitle ucTitle1;
        private System.Windows.Forms.TableLayoutPanel tlpItem;
        private UCItem ucItem10;
        private UCItem ucItem9;
        private UCItem ucItem8;
        private UCItem ucItem7;
        private UCItem ucItem6;
        private UCItem ucItem5;
        private UCItem ucItem4;
        private UCItem ucItem3;
        private UCItem ucItem2;
        private UCItem ucItem1;
        public System.Windows.Forms.Label lbTitle;
    }
}
