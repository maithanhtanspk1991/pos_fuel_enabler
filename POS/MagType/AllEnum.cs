﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.MagType
{
    public class FunctionEnum
    {
        public static System.Drawing.Color GetTyreColor(TyreColor TyreColor)
        {
            return GetTyreColor((int)TyreColor);
        }
        public static System.Drawing.Color GetTyreColor(int TyreColor)
        {
            System.Drawing.Color result = System.Drawing.Color.FromArgb(0, 250, 99);
            switch (TyreColor)
            {
                case 2:

                    result = System.Drawing.Color.FromArgb(184, 173, 4);
                    break;
                case 3:
                    result = System.Drawing.Color.FromArgb(0, 250, 99);
                    break;
            }
            return result;
        }
    }

    public enum TyreColor
    {
        Mag = 2,
        Tyre = 3
    }
}
