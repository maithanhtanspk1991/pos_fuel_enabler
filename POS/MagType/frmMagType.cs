﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.MagType
{
    public partial class frmMagType : Form
    {
        public Barcode.SerialPort mSerialPort;
        public frmMagType(Barcode.SerialPort barcode)
        {
            InitializeComponent();
            UcListMag.Chosen += new UCList.MyChosen(UcListMag_Chosen);
            UcListType.Chosen += new UCList.MyChosen(UcListType_Chosen);
            ItemMenuMag = null;
            ItemMenuType = null;
            mSerialPort = barcode;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
        }
        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {

                mSerialPort.SetText("", lbBarcode);
                mSerialPort.SetText(data, lbBarcode);
            }
        }


        void UcListType_Chosen(DataObject.ItemsMenu ItemMenu)
        {
            ucChooseType.SetValue(ItemMenu);
        }

        void UcListMag_Chosen(DataObject.ItemsMenu ItemMenu)
        {
            UcListType.LoadData("", "", ItemMenu.SizeItem);
            ucChooseMag.SetValue(ItemMenu);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (ucChooseMag.Tag is DataObject.ItemsMenu)
                ItemMenuMag = (DataObject.ItemsMenu)ucChooseMag.Tag;
            if (ucChooseType.Tag is DataObject.ItemsMenu)
                ItemMenuType = (DataObject.ItemsMenu)ucChooseType.Tag;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            UcListMag.LoadData(lbBarcode.Text, lbName.Text, -1);
        }

        public DataObject.ItemsMenu ItemMenuMag { get; set; }
        public DataObject.ItemsMenu ItemMenuType { get; set; }

        private void frmMagType_Load(object sender, EventArgs e)
        {
            UcListMag.LoadData(lbBarcode.Text, lbName.Text, -1);
        }

        private void lbName_TextChanged(object sender, EventArgs e)
        {
            UcListMag.LoadData(lbBarcode.Text, lbName.Text, -1);
        }

        public void SetText(string text, System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }
        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);

        private void lbBarcode_TextChanged(object sender, EventArgs e)
        {
            if (lbBarcode.Text.Length > 9)
                UcListMag.LoadData(lbBarcode.Text, lbName.Text, -1);
        }
    }
}
