﻿namespace POS.MagType
{
    partial class UCItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNo = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lbBarcode = new System.Windows.Forms.Label();
            this.lbPrice = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbNo
            // 
            this.lbNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNo.Location = new System.Drawing.Point(0, 0);
            this.lbNo.Margin = new System.Windows.Forms.Padding(0);
            this.lbNo.Name = "lbNo";
            this.lbNo.Size = new System.Drawing.Size(56, 30);
            this.lbNo.TabIndex = 0;
            this.lbNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbNo.Click += new System.EventHandler(this.UCItem_Click);
            // 
            // lbName
            // 
            this.lbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbName.Location = new System.Drawing.Point(56, 0);
            this.lbName.Margin = new System.Windows.Forms.Padding(0);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(225, 30);
            this.lbName.TabIndex = 0;
            this.lbName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbName.Click += new System.EventHandler(this.UCItem_Click);
            // 
            // lbBarcode
            // 
            this.lbBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbBarcode.Location = new System.Drawing.Point(281, 0);
            this.lbBarcode.Margin = new System.Windows.Forms.Padding(0);
            this.lbBarcode.Name = "lbBarcode";
            this.lbBarcode.Size = new System.Drawing.Size(168, 30);
            this.lbBarcode.TabIndex = 0;
            this.lbBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbBarcode.Click += new System.EventHandler(this.UCItem_Click);
            // 
            // lbPrice
            // 
            this.lbPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbPrice.Location = new System.Drawing.Point(449, 0);
            this.lbPrice.Margin = new System.Windows.Forms.Padding(0);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(114, 30);
            this.lbPrice.TabIndex = 0;
            this.lbPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbPrice.Click += new System.EventHandler(this.UCItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lbNo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbBarcode, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbPrice, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(563, 30);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // UCItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UCItem";
            this.Size = new System.Drawing.Size(563, 30);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbNo;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbBarcode;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
