﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.MagType
{
    public partial class UCList : UserControl
    {
        private SystemConfig.DBConfig mDBConfig = new SystemConfig.DBConfig();
        private DataObject.Limit mLimit = new DataObject.Limit(0, 10);
        private string titleList;

        private DataObject.ItemsMenu mItemsMenu = null;

        public UCList()
        {

            InitializeComponent();
            Barcode = "";
            ItemName = "";
        }

        public string Barcode { get; set; }

        public string ItemName { get; set; }

        public int SizeItem { get; set; }

        public string TitleList
        {
            get { return titleList; }
            set { titleList = value; lbTitle.Text = value; }
        }

        private TyreColor typeMenu;

        public TyreColor TypeMenu
        {
            get { return typeMenu; }
            set
            {
                typeMenu = value;
                lbFoundRecords.BackColor = lbTitle.BackColor = FunctionEnum.GetTyreColor(typeMenu);
            }
        }


        public void LoadData(string barcode, string itemName, int sizeItem)
        {
            Barcode = barcode;
            ItemName = itemName;
            mLimit.Default();
            SizeItem = sizeItem;
            LoadData();
        }

        private void btmNext_Click(object sender, EventArgs e)
        {
            mLimit.Next();
            LoadData();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            mLimit.Back();
            LoadData();
        }

        private void btnFirstPage_Click(object sender, EventArgs e)
        {
            mLimit.FirstPage();
            LoadData();
        }

        private void btnGoPage_Click(object sender, EventArgs e)
        {
            mLimit.GoPage(Convert.ToInt32(lbPage.Text == "" ? 1 : Convert.ToInt32(lbPage.Text)));
            LoadData();
        }

        private void btnLastPage_Click(object sender, EventArgs e)
        {
            mLimit.LastPage();
            LoadData();
        }

        private void LoadData()
        {
            List<DataObject.ItemsMenu> lsArray = BusinessObject.BOItemsMenu.GetAll((int)TypeMenu, SizeItem, Barcode, ItemName, mDBConfig, mLimit);
            int n = 9;
            for (int i = 0; i < lsArray.Count; i++)
            {
                UCItem uc = (UCItem)tlpItem.Controls[n];
                uc.SetValue(mLimit.RowFirstIndex + i + 1, lsArray[i]);
                uc.Enabled = true;
                n--;
            }

            for (int i = n; i >= 0; i--)
            {
                UCItem uc = (UCItem)tlpItem.Controls[i];
                uc.SetDefault();
                uc.Enabled = false;
            }
            if (lsArray.Count > 0 && mLimit.Page == 1 && TypeMenu == TyreColor.Mag)
            {
                ucItem_Click(ucItem1, null);
            }
            lbPage.Text = mLimit.Page.ToString();
            SetFoundRecords(mLimit.Total);
            ReActive();
        }

        private void SetFoundRecords(int Total)
        {
            lbFoundRecords.Text = "Found " + Total + " Records";
        }

        private void ucItem_Click(object sender, EventArgs e)
        {
            LoadDefaultList();
            UCItem uc = (UCItem)sender;
            uc.Active(TypeMenu);
            DataObject.ItemsMenu ItemMenu = (DataObject.ItemsMenu)uc.Tag;
            mItemsMenu = ItemMenu;
            OnChosen(ItemMenu);
        }

        private void LoadDefaultList()
        {
            foreach (Control ctr in tlpItem.Controls)
            {
                UCItem uc = (UCItem)ctr;
                uc.Unactive();
            }
        }

        private void ucItem1_Click(object sender, EventArgs e)
        {

        }
        public delegate void MyChosen(DataObject.ItemsMenu ItemMenu);
        public event MyChosen Chosen;
        private void OnChosen(DataObject.ItemsMenu ItemMenu)
        {
            if (ItemMenu != null)
                Chosen(ItemMenu);
        }

        public void ReActive()
        {
            if (mItemsMenu != null)
            {
                LoadDefaultList();
                foreach (Control ctr in tlpItem.Controls)
                {
                    UCItem uc = (UCItem)ctr;
                    DataObject.ItemsMenu ItemMenu = (DataObject.ItemsMenu)uc.Tag;
                    if (ItemMenu != null)
                    {
                        if (ItemMenu.ItemID == mItemsMenu.ItemID)
                        {
                            uc.Active(TypeMenu);
                        }
                    }
                }
            }
        }
    }
}