﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.MagType
{
    public partial class UCChoose : UserControl
    {
        public UCChoose()
        {
            InitializeComponent();
        }

        public TyreColor TypeMenu { get; set; }

        public void SetValue(DataObject.ItemsMenu item)
        {
            lbBarcode.Text = item.Barcode;
            lbName.Text = item.ItemDesc;
            lbPrice.Text = "$" + DataObject.MonneyFormat.Format2(item.UnitPrice == 0 ? 0 : item.UnitPrice / 1000);
            Tag = item;
            this.BackColor = FunctionEnum.GetTyreColor(TypeMenu);
        }

        public void SetDefault()
        {
            lbName.Text = "";
            lbPrice.Text = "";
            lbBarcode.Text = "";
            this.BackColor = System.Drawing.SystemColors.Control;
            Tag = null;
        }
    }
}
