﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.MagType
{
    public partial class UCItem : UserControl
    {
        public UCItem()
        {
            InitializeComponent();
        }

        public void SetValue(int no, DataObject.ItemsMenu item)
        {
            lbNo.Text = no.ToString();
            lbName.Text = item.ItemDesc;
            lbBarcode.Text = item.Barcode;
            lbPrice.Text = "$" + DataObject.MonneyFormat.Format2(item.UnitPrice == 0 ? 0 : item.UnitPrice / 1000);
            Tag = item;
        }

        public void SetDefault()
        {
            lbNo.Text = "";
            lbName.Text = "";
            lbPrice.Text = "";
            lbBarcode.Text = "";
            this.BackColor = System.Drawing.SystemColors.Control;
            Tag = null;
        }

        public void Active(TyreColor Type)
        {
            this.BackColor = FunctionEnum.GetTyreColor(Type);
        }

        public void Unactive()
        {
            this.BackColor = System.Drawing.SystemColors.Control;
        }

        private void UCItem_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }
    }
}
