﻿namespace POS.Stock
{
    partial class frmPopupFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpPopupFuel = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // tlpPopupFuel
            // 
            this.tlpPopupFuel.ColumnCount = 3;
            this.tlpPopupFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPopupFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPopupFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPopupFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPopupFuel.Location = new System.Drawing.Point(0, 0);
            this.tlpPopupFuel.Name = "tlpPopupFuel";
            this.tlpPopupFuel.RowCount = 1;
            this.tlpPopupFuel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPopupFuel.Size = new System.Drawing.Size(458, 112);
            this.tlpPopupFuel.TabIndex = 0;
            // 
            // frmPopupFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 112);
            this.Controls.Add(this.tlpPopupFuel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPopupFuel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Fuel";
            this.Load += new System.EventHandler(this.frmPopupFuel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpPopupFuel;
    }
}