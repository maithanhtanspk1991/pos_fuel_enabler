﻿namespace POS.Stock
{
    partial class frmReViewCheckStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvReViewCheckStock = new System.Windows.Forms.ListView();
            this.cBarCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cQtyOnHand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cQtyChange = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cQtyAfterCheck = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cReason = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Note = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.lvReViewCheckStock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvReViewCheckStock.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cBarCode,
            this.cItemName,
            this.cQtyOnHand,
            this.cQtyChange,
            this.cQtyAfterCheck,
            this.cReason,
            this.Note});
            this.lvReViewCheckStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvReViewCheckStock.GridLines = true;
            this.lvReViewCheckStock.Location = new System.Drawing.Point(4, 3);
            this.lvReViewCheckStock.Name = "listView1";
            this.lvReViewCheckStock.Size = new System.Drawing.Size(966, 513);
            this.lvReViewCheckStock.TabIndex = 0;
            this.lvReViewCheckStock.UseCompatibleStateImageBehavior = false;
            this.lvReViewCheckStock.View = System.Windows.Forms.View.Details;
            // 
            // cBarCode
            // 
            this.cBarCode.Text = "BarCode";
            this.cBarCode.Width = 111;
            // 
            // cItemName
            // 
            this.cItemName.Text = "ItemName";
            this.cItemName.Width = 147;
            // 
            // cQtyOnHand
            // 
            this.cQtyOnHand.Text = "Before Check";
            this.cQtyOnHand.Width = 115;
            // 
            // cQtyChange
            // 
            this.cQtyChange.Text = "Change";
            this.cQtyChange.Width = 98;
            // 
            // cQtyAfterCheck
            // 
            this.cQtyAfterCheck.Text = "After Check";
            this.cQtyAfterCheck.Width = 108;
            // 
            // cReason
            // 
            this.cReason.Text = "Reason";
            this.cReason.Width = 111;
            // 
            // Note
            // 
            this.Note.Text = "Note";
            this.Note.Width = 192;
            // 
            // frmReViewCheckStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 520);
            this.Controls.Add(this.lvReViewCheckStock);
            this.Name = "frmReViewCheckStock";
            this.Text = "frmReViewCheckStock";
            this.Load += new System.EventHandler(this.frmReViewCheckStock_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvReViewCheckStock;
        private System.Windows.Forms.ColumnHeader cBarCode;
        private System.Windows.Forms.ColumnHeader cItemName;
        private System.Windows.Forms.ColumnHeader cQtyOnHand;
        private System.Windows.Forms.ColumnHeader cQtyChange;
        private System.Windows.Forms.ColumnHeader cQtyAfterCheck;
        private System.Windows.Forms.ColumnHeader cReason;
        private System.Windows.Forms.ColumnHeader Note;

    }
}