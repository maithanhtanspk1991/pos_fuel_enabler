﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmReViewCheckStock : Form
    {
        private List<DataObject.StockItemCheck> mlistitem;

        public frmReViewCheckStock(List<DataObject.StockItemCheck> listitem)
        {
            InitializeComponent();
            mlistitem = listitem;
        }

        private void frmReViewCheckStock_Load(object sender, EventArgs e)
        {
            foreach (var item in mlistitem)
            {
                ListViewItem lvi = new ListViewItem(item.barcode);
                lvi.SubItems.Add(item.Name);
                lvi.SubItems.Add(item.QtyInHand.ToString());
                lvi.SubItems.Add(item.QtyChange.ToString());
                lvi.SubItems.Add(item.Qty.ToString());
                lvi.SubItems.Add(item.strTypeReason);
                lvi.SubItems.Add(item.Note);
                lvReViewCheckStock.Items.Add(lvi);
            }
        }
    }
}