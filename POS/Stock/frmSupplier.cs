﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmSupplier : Form
    {
        private SystemConfig.DBConfig mDBConfig;
        private string mNameFrom = "POS::Form::frmSupplier";
        private DataObject.Suppliers mSuppliers = new DataObject.Suppliers();
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmSupplier(SystemConfig.DBConfig dBConfig)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::frmSupplier");
            InitializeComponent();
            mDBConfig = dBConfig;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label6.Text = "Tên:";
                label3.Text = "Di động:";
                label4.Text = "Số điện thoại:";
                label12.Text = "Biển số:";
                label14.Text = "Tên đường:";
                btnNew.Text = "Tạo mới";
                btnSave.Text = "Lưu lại";
                btnActive.Text = "Kích hoạt";
                btnDelete.Text = "Xóa";
                btnSearch.Text = "Tìm kiếm";
                btnDisplayAll.Text = "Hiển thị tất cả";
                btnExit.Text = "Thoát";
                btnExit.Font = new Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                columnHeader2.Text = "Tên";
                columnHeader5.Text = "Di động";
                columnHeader6.Text = "Số điện thoại";
                columnHeader7.Text = "Biển số";
                columnHeader8.Text = "Đường";
                columnHeader9.Text = "Kích hoạt";
                return;
            }
        }

        public void LoadListView()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::LoadListView");
            lvSuppliers.Items.Clear();
            List<DataObject.Suppliers> lsArray = new List<DataObject.Suppliers>();
            lsArray = BusinessObject.BOSuppliers.GetSuppliers(txtSearch.Text, -1, mDBConfig);
            foreach (DataObject.Suppliers item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.ABN);
                li.SubItems.Add(item.SupplierName);
                li.SubItems.Add(item.Fax);
                li.SubItems.Add(item.Email);
                li.SubItems.Add(item.Mobile);
                li.SubItems.Add(item.Phone);
                li.SubItems.Add(item.StreetNo);
                li.SubItems.Add(item.StreetName);
                li.SubItems.Add(item.Active == true ? "Yes" : "No");
                li.Tag = item;
                lvSuppliers.Items.Add(li);
            }
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnActive_Click::" + mSuppliers.SupplierName);
            mSuppliers.Active = !mSuppliers.Active;
            UpDateSupplier();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnDelete_Click::" + mSuppliers.SupplierName);
            DeleteSupplier();
        }

        private void btnDisplayAll_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnDisplayAll_Click");
            txtSearch.Text = string.Empty;
            LoadListView();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnExit_Click");
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnNew_Click");
            lbStatus.Text = string.Empty;
            btnSave.Enabled = true;
            btnSave.Text = "Save";
            btnActive.Enabled = false;
            btnDelete.Enabled = false;
            Default();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnSave_Click");
            if (mSuppliers.SupplierID == 0)
            {
                if (!CheckAccountInLayOut())
                {
                    return;
                }
                SetValue();
                if (BusinessObject.BOSuppliers.Insert(mSuppliers, mDBConfig) > 0)
                {
                    //PrintServer.PrinterAccount.Printer(mSuppliers);
                    LoadListView();
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Thành công !!";
                    }
					else{
						 lbStatus.Text = "Successful !!";
					}
                }
            }
            else
            {
                UpDateSupplier();
            }
            ClearTextBox();
            Default();
            btnSave.Enabled = false;
            //btnUpdate.Enabled = false;
            btnActive.Enabled = false;
            btnDelete.Enabled = false;
            txtSearch.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::btnSearch_Click");
            LoadListView();
        }

        private bool CheckAccountInLayOut()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::CheckAccountInLayOut");
            if (txtABN.Text == string.Empty || txtName.Text == string.Empty)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Tất cả thông tin bị trống";
                    return false;
                }
                lbStatus.Text = "All Information empty";
                return false;
            }
            return true;
        }

        private void ClearTextBox()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::ClearTextBox");
            txtFax.Text = "";
            txtName.Text = "";
            txtMobile.Text = "";
            txtPhone.Text = "";
            txtPlateNumber.Text = "";
            txtStreetName.Text = "";
            txtEmail.Text = "";
            txtABN.Text = "";
        }

        private void Default()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::Default");
            mSuppliers = new DataObject.Suppliers();
            txtABN.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtPlateNumber.Text = string.Empty;
            txtStreetName.Text = string.Empty;
        }

        private void DeleteSupplier()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::DeleteSupplier");
            if (mSuppliers.SupplierID > 0)
            {
                if (BusinessObject.BOSuppliers.Delete(mSuppliers, mDBConfig) > 0) ;
                {
                    LoadListView();
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Đã xóa thành công !!";
                        return;
                    }
                    lbStatus.Text = "Deleted Successful !!";
                }
            }
        }

        private void frmSupplier_Load(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::frmSupplier_Load");
            Default();
            LoadListView();
            btnSave.Enabled = true;
            btnActive.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void LoadTextBox()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::LoadTextBox");
            txtName.Text = mSuppliers.SupplierName;
            txtABN.Text = mSuppliers.ABN;
            txtMobile.Text = mSuppliers.Mobile;
            txtPhone.Text = mSuppliers.Phone;
            txtPlateNumber.Text = mSuppliers.StreetNo;
            txtStreetName.Text = mSuppliers.StreetName;
            txtFax.Text = mSuppliers.Fax;
            txtEmail.Text = mSuppliers.Email;
            btnActive.Text = mSuppliers.Active == false ? "Active" : "Unactive";
        }

        private void lvSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::lvCustomer_SelectedIndexChanged");
            if (lvSuppliers.SelectedItems.Count > 0)
            {
                mSuppliers = (DataObject.Suppliers)lvSuppliers.SelectedItems[0].Tag;
                LoadTextBox();
                lbStatus.Text = string.Empty;
                btnSave.Enabled = true;
                btnSave.Text = "Update";
                btnActive.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void SetValue()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::SetValue");
            mSuppliers.Email = txtEmail.Text; ;
            mSuppliers.ABN = txtABN.Text; ;
            mSuppliers.Fax = txtFax.Text;
            mSuppliers.Mobile = txtMobile.Text;
            mSuppliers.SupplierName = txtName.Text;
            mSuppliers.Phone = txtPhone.Text;
            mSuppliers.StreetNo = txtPlateNumber.Text;
            mSuppliers.StreetName = txtStreetName.Text;
        }

        private void UpDateSupplier()
        {
            SystemLog.LogPOS.WriteLog(mNameFrom + "::UpDateSupplier");
            if (!CheckAccountInLayOut())
            {
                return;
            }
            SetValue();
            if (mSuppliers.SupplierID > 0)
            {
                if (BusinessObject.BOSuppliers.Update(mSuppliers, mDBConfig) > 0) ;
                {
                    LoadListView();
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Cập nhật thông tin thành công !!";
                        return;
                    }
                    lbStatus.Text = "Update Information Successful !!";
                }
            }
        }
    }
}