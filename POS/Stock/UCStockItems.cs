﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class UCStockItems : UserControl
    {
        private Class.MoneyFortmat money;

        public DataObject.StockItemCheck mitemCheck { get; set; }

        public delegate void AddItem(DataObject.StockItemCheck itemcheck);

        public AddItem UCStockItemsAddItem;
        private SystemConfig.DBConfig mdbconfig;

        public bool _DispalyOrderChange { get; set; }

        //private int DispalyOrderOld = 0;

        public UCStockItems(SystemConfig.DBConfig dbconfig)
        {
            mitemCheck = new DataObject.StockItemCheck();
            InitializeComponent();
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mdbconfig = dbconfig;
            flagtextchange = false;
        }

        private void UCItemsChange_Load(object sender, EventArgs e)
        {
        }

        public void SetValue()
        {
            LoadComboboxReason();
            //LoadPrinterType();
            txtItemDesc.Text = mitemCheck.Name.ToString();
            txtBarcode.Text = mitemCheck.barcode;
            txtQtyInHand.Text = mitemCheck.QtyInHand.ToString();

            txtQtyChange.Text = "0";
            txtQty.Text = "0";
            txtQtyChange_TextChanged(txtQtyChange, null);
        }

        private void LoadComboboxReason()
        {
            List<DataObject.Reason> lst = BusinessObject.BOReason.GetAllReason(mdbconfig);
            cbReason.DataSource = lst; // BusinessObject.BOReason.GetAllReason(mdbconfig);
            cbReason.ValueMember = "ID";
            cbReason.DisplayMember = "strReason";
            cbReason.Tag = lst;

            if (lst.Count > 0)
            {
                cbReason.SelectedIndex = 0;
            }
        }

        private bool Check()
        {
            try
            {
                int kq = Convert.ToInt32(txtQtyChange.Text);
                List<DataObject.Reason> lst = (List<DataObject.Reason>)cbReason.Tag;
                if (lst[cbReason.SelectedIndex].Negative < 0 && kq > 0)
                {
                    lbStatus.Text = "Do not enter positive numbers for reasons " + lst[cbReason.SelectedIndex].strReason;
                    return false;
                }
                else if (lst[cbReason.SelectedIndex].Negative > 0 && kq < 0)
                {
                    lbStatus.Text = "Do not enter negative numbers for reasons " + lst[cbReason.SelectedIndex].strReason;
                    return false;
                }
                // Lý do other ko được để trống
                if (cbReason.SelectedIndex == lst.Count - 1 && txtNote.Text == string.Empty)
                {
                    lbStatus.Text = "Note empty !!";
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                UCStockItemsAddItem(GetValue());
                lbStatus.Text = "Add Success";
            }
        }

        private DataObject.StockItemCheck GetValue()
        {
            DataObject.StockItemCheck item = new DataObject.StockItemCheck();
            item.itemID = mitemCheck.itemID;
            item.Name = mitemCheck.Name;
            item.barcode = mitemCheck.barcode;
            item.QtyInHand = mitemCheck.Qty;
            item.QtyChange = Convert.ToInt32(txtQtyChange.Text);
            item.Qty = Convert.ToInt32(txtQty.Text);
            item.TypeReason = cbReason.SelectedIndex;
            item.strTypeReason = cbReason.SelectedText;
            item.Note = txtNote.Text;
            return item;
        }

        private bool flagtextchange;

        private void txtQtyChange_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Name == txtQtyChange.Name)
            {
                if (txtQtyChange.Text != string.Empty)
                {
                    if (flagtextchange == false)
                    {
                        flagtextchange = true;
                        int kq = 0;
                        try
                        {
                            kq = Convert.ToInt32(txtQtyChange.Text);
                        }
                        catch
                        {
                            kq = 0;
                        }
                        txtQty.Text = (mitemCheck.QtyInHand + kq).ToString();
                        flagtextchange = false;
                    }
                }
            }
            else if (((TextBox)sender).Name == txtQty.Name)
            {
                if (txtQty.Text != string.Empty)
                {
                    if (flagtextchange == false)
                    {
                        flagtextchange = true;
                        int kq = 0;
                        try
                        {
                            kq = Convert.ToInt32(txtQty.Text);
                        }
                        catch
                        {
                            kq = mitemCheck.QtyInHand;
                        }

                        txtQtyChange.Text = (kq - mitemCheck.QtyInHand).ToString();
                        flagtextchange = false;
                    }
                }
            }
        }
    }
}