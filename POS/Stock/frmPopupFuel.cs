﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmPopupFuel : Form
    {
        private SystemConfig.DBConfig mDBConfig;

        public frmPopupFuel(SystemConfig.DBConfig dBConfig)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
        }

        private void frmPopupFuel_Load(object sender, EventArgs e)
        {
            LoadItemsMenu();
            SetSize();
            LoadData();
        }

        private int CountItem = 0;
        private List<DataObject.ItemsMenu> lsArray;

        private void LoadItemsMenu()
        {
            lsArray = new List<DataObject.ItemsMenu>();
            lsArray = BusinessObject.BOItemsMenu.GetItemsMenu(-1, "", "", 1, 0, 1, mDBConfig);
            CountItem = lsArray.Count;
            if (CountItem == 0)
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void LoadData()
        {
            Button[] lsButton = new Button[lsArray.Count];
            int i = 0;
            foreach (DataObject.ItemsMenu item in lsArray)
            {
                lsButton[i] = new Button();
                lsButton[i].Text = item.ItemShort;
                lsButton[i].Dock = DockStyle.Fill;
                lsButton[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lsButton[i].ForeColor = System.Drawing.Color.White;
                lsButton[i].Margin = new System.Windows.Forms.Padding(0);
                lsButton[i].UseVisualStyleBackColor = false;
                lsButton[i].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
                lsButton[i].Click += new EventHandler(frmPopupFuel_Click);
                lsButton[i].Tag = item;
                i++;
            }
            tlpPopupFuel.Controls.AddRange(lsButton);
        }

        public DataObject.ItemsMenu ItemsMenu { get; set; }

        private void frmPopupFuel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            ItemsMenu = (DataObject.ItemsMenu)btn.Tag;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void SetSize()
        {
            int count = 3;
            tlpPopupFuel.ColumnStyles.Clear();
            System.Single LenghtColumn = 100 / count;
            tlpPopupFuel.ColumnCount = count;
            this.Width = 120 * count;
            for (int i = 0; i < count; i++)
            {
                tlpPopupFuel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, LenghtColumn));
            }

            int _RowItem = (CountItem - 1) / count + 1;

            tlpPopupFuel.RowStyles.Clear();
            tlpPopupFuel.RowCount = _RowItem;
            System.Single LenghtRow = 100 / _RowItem;
            for (int i = 0; i < _RowItem; i++)
            {
                tlpPopupFuel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, LenghtRow));
            }

            this.Height = 120 * _RowItem;
        }
    }
}