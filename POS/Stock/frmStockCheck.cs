﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmStockCheck : Form
    {
        private string _nameFrom = "frmMenuChange";
        private DataObject.Query _Query = DataObject.Query.None;
        private DataObject.ListItemType _Type = DataObject.ListItemType.Group;
        private SystemConfig.DBConfig mDBConfig;
        private Class.MoneyFortmat mMoney;
        private Barcode.SerialPort mSerialPortBarcode;
        private DataObject.TypeMenu mShortcut = DataObject.TypeMenu.ALLMENU;
        private DataObject.Transit mtransit;
        private string strBarcode = "";
        private string strNoticeNewItem = BusinessObject.BOConfig.GetNoticeNewItems;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        //private POS.Controls.UCGroupChange uCGroupChange = new Controls.UCGroupChange();
        private POS.Stock.UCStockItems ucStockItems;// = new Stock.UCStockItems();

        public frmStockCheck(SystemConfig.DBConfig dBConfig, Barcode.SerialPort serialPortBarcode, DataObject.Transit transit)
        {
            InitializeComponent();
            SetMultiLanguage();
            mDBConfig = dBConfig;
            mSerialPortBarcode = serialPortBarcode;
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPortBarcode.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            ucStockItems = new Stock.UCStockItems(mDBConfig);
            lststockitem = new System.Collections.Generic.List<DataObject.StockItemCheck>();
            mtransit = transit;
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnReview.Text = "Xem lại";
                btnFindItem.Text = "Tìm kiếm";
                btnFuel.Text = "Nhiên liệu";
                btnSubmit.Text = "Lưu lại";
                buttonExit.Text = "Thoát";
                buttonExit.Font = new Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }

        private void btnAllMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.ALLMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }

        /// <summary>
        /// Thoát
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTyreMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.TYREMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }

        private void btnReview_Click(object sender, EventArgs e)
        {
            if (lststockitem.Count > 0)
            {
                POS.Stock.frmReViewCheckStock frm = new Stock.frmReViewCheckStock(lststockitem);
                frm.ShowDialog();
            }
        }

        private void btnFindItem_Click(object sender, EventArgs e)
        {
            POS.Forms.frmSearchBarCode frm = new POS.Forms.frmSearchBarCode(DataObject.FindItemType.FindItemInStock);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ucMenuChange._Barcode = frm.ItemMenu.BarCode;
                ucMenuChange.RefreshBarcode();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            // Submit
            if (lststockitem.Count > 0)
            {
                BusinessObject.BOStockCheckItem.Insert(mDBConfig, lststockitem, mtransit);
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Danh sách trống";
                    return;
                }
                lbStatus.Text = "List Empty";
            }
        }

        private void frmMenuChange_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void frmMenuChange_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Load change item
        /// </summary>
        private void LoadChangeItems()
        {
            pnContent.Controls.Clear();

            ucStockItems.mitemCheck = GetStockItemCheck(ucMenuChange._ItemsMenu);
            if (!ucMenuChange._ItemsMenu.IsFuel)
                ucStockItems.mitemCheck.QtyInHand = ucMenuChange._ItemsMenu.SellSize > 0 ? ucStockItems.mitemCheck.QtyInHand / ucMenuChange._ItemsMenu.SellSize : ucStockItems.mitemCheck.QtyInHand;
            ucStockItems.mitemCheck.QtyInHand = ucMenuChange._ItemsMenu.IsFuel ? ucStockItems.mitemCheck.QtyInHand / 1000 : ucStockItems.mitemCheck.QtyInHand;
            ucStockItems.UCStockItemsAddItem = new Stock.UCStockItems.AddItem(UCStockItemsAddItem);
            //ucMenuChange._ItemsMenu;
            ucStockItems.mitemCheck.ChangeFromItemMenu(ucMenuChange._ItemsMenu);
            ucStockItems.SetValue();

            ucStockItems.Dock = DockStyle.Fill;
            pnContent.Controls.Add(ucStockItems);
        }

        /// <summary>
        /// Load nội dung
        /// </summary>
        private void LoadContent()
        {
            //btnDelete.Enabled = true;
            LoadNavigation();
            pnContent.Controls.Clear();
            _Query = DataObject.Query.Update;
            if (ucMenuChange._ItemsMenu != null)
            {
                LoadChangeItems();
                LoadNavigation();
            }
            else if (ucMenuChange._GroupMenu != null)
            {
                //LoadChangeGroup();
            }
        }

        /// <summary>
        /// Load Navigation
        /// </summary>
        private void LoadNavigation()
        {
            lbNavigation.Text = "";
            if (ucMenuChange._GroupMenu != null)
            {
                _Type = DataObject.ListItemType.Group;
                lbNavigation.Text += ucMenuChange._GroupMenu.GroupDesc;
            }
            if (ucMenuChange._ItemsMenu != null)
            {
                _Type = DataObject.ListItemType.Items;
                lbNavigation.Text += " >> ";
                lbNavigation.Text += ucMenuChange._ItemsMenu.ItemDesc;
            }
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "LoadLabelText::" + lbNavigation.Text);
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPortBarcode.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPortBarcode.SetText("", txtBarcode);
                mSerialPortBarcode.SetText(data, txtBarcode);
            }
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text != "")
            {
                strBarcode = txt.Text;
                ucMenuChange._Barcode = txtBarcode.Text;
                if (ucMenuChange.RefreshBarcode())
                {
                    LoadChangeItems();
                    LoadNavigation();
                }
                else
                {
                    POS.Forms.frmMessageBox frmMessageBox = new POS.Forms.frmMessageBox("Notice", strNoticeNewItem);
                    if (frmMessageBox.ShowDialog() == DialogResult.OK)
                    {
                        //btnNewItem_Click(sender, e);
                    }
                }
                strBarcode = "";
                txt.Text = "";
            }
        }

        /// <summary>
        /// sự kiện khi click vào menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucMenuChange_Click(object sender, EventArgs e)
        {
            LoadContent();
        }

        #region Duc

        public System.Collections.Generic.List<DataObject.StockItemCheck> lststockitem;

        public void UCStockItemsAddItem(DataObject.StockItemCheck stockitem)
        {
            Console.WriteLine("add item");
            if (!lststockitem.Exists(s => s.itemID == stockitem.itemID))
            {
                Console.WriteLine("add moi");
                lststockitem.Add(stockitem);
            }
            else
            {
                Console.WriteLine("Add lai cai cu");
                DataObject.StockItemCheck item = lststockitem.Find(s => s.itemID == stockitem.itemID);
                item.QtyInHand = stockitem.QtyInHand;
                item.QtyChange = stockitem.QtyChange;
                item.Qty = stockitem.Qty;
            }
        }

        private DataObject.StockItemCheck GetStockItemCheck(DataObject.ItemsMenu itemmenu)
        {
            DataObject.StockItemCheck item = BusinessObject.BOStockCheckItem.GetItemCheckFormItemMenu(itemmenu, mDBConfig);

            return item;
        }

        #endregion Duc

        /// <summary>
        /// Sự kiện load from
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucMenuChange_Load(object sender, EventArgs e)
        {
            ucMenuChange.InitMenu(mDBConfig);
            LoadContent();
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "ucMenuChange_Load");
        }

        private void btnFuel_Click(object sender, EventArgs e)
        {
            lbStatus.Text = "";
            frmPopupFuel frmPopupFuel = new frmPopupFuel(mDBConfig);
            frmPopupFuel.Location = GetPositionInForm(ucMenuChange);
            if (frmPopupFuel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (frmPopupFuel.ItemsMenu != null)
                {
                    ucMenuChange._ItemsMenu = frmPopupFuel.ItemsMenu;
                    LoadContent();
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Không có nhiên liệu";
                    return;
                }
                lbStatus.Text = "No item fuel";
            }
        }

        public Point GetPositionInForm(Control ctrl)
        {
            Point p = new Point();
            p = ctrl.Parent.PointToScreen(ctrl.Location);
            p.X = p.X + ctrl.Width;
            return p;
        }
    }
}