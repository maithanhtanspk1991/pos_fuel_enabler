﻿namespace POS.Stock
{
    partial class frmReceiveStock
    {
        private System.Windows.Forms.Button btnDelete;

        private System.Windows.Forms.Button btnExit;

        private System.Windows.Forms.Button btnReceive;

        private System.Windows.Forms.Button btnDeleteAll;

        private System.Windows.Forms.ComboBox cbbSupplier;

        private System.Windows.Forms.ColumnHeader columnHeader1;

        private System.Windows.Forms.ColumnHeader columnHeader2;

        private System.Windows.Forms.ColumnHeader columnHeader3;

        private System.Windows.Forms.ColumnHeader columnHeader4;

        private System.Windows.Forms.ColumnHeader columnHeader5;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private System.Windows.Forms.DateTimePicker dtpDate;

        private System.Windows.Forms.GroupBox groupBox1;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.Label label10;

        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.Label label3;

        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.Label label5;

        private System.Windows.Forms.Label lbCableID;

        private System.Windows.Forms.Label lbStaffName;

        private System.Windows.Forms.Label lbStatus;

        private System.Windows.Forms.Label lbSubTotal;

        private System.Windows.Forms.ListView lvReceive;

        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.Panel panel10;

        private System.Windows.Forms.Panel panel2;

        private System.Windows.Forms.Panel panel4;

        private System.Windows.Forms.Panel panel6;

        private System.Windows.Forms.Panel panel7;

        private System.Windows.Forms.Panel panel9;

        private Controls.TextBoxPOSKeyBoard txtTicket;

        private Controls.UCMenu ucMenuReceiveStock;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiveStock));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.txtTicket = new POS.Controls.TextBoxPOSKeyBoard();
            this.lbStaffName = new System.Windows.Forms.Label();
            this.cbbSupplier = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbSubTotal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCableID = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lvReceive = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAllMenu = new System.Windows.Forms.Button();
            this.btnTyreMenu = new System.Windows.Forms.Button();
            this.btnFuel = new System.Windows.Forms.Button();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.ucMenuReceiveStock = new POS.Controls.UCMenu();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uCkeypad = new POS.Controls.UCkeypad();
            this.txtPriceSell = new POS.Controls.TextBoxPOSInto();
            this.txtQtyNew = new POS.Controls.TextBoxPOSInto();
            this.txtQtyOnHand = new POS.Controls.TextBoxPOSInto();
            this.txtQtyAdd = new POS.Controls.TextBoxPOSInto();
            this.lbStatus = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInterest = new POS.Controls.TextBoxPOSInto();
            this.txtTotalSell = new POS.Controls.TextBoxPOSInto();
            this.txtTotalAdd = new POS.Controls.TextBoxPOSInto();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtItemName = new POS.Controls.TextBoxPOSKeyBoard();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPrice = new POS.Controls.TextBoxPOSInto();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tlpButton = new System.Windows.Forms.TableLayoutPanel();
            this.btnReceive = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tlpButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 120);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBarcode);
            this.groupBox1.Controls.Add(this.txtTicket);
            this.groupBox1.Controls.Add(this.lbStaffName);
            this.groupBox1.Controls.Add(this.cbbSupplier);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbSubTotal);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbCableID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1024, 120);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Receive";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(838, 72);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(100, 29);
            this.txtBarcode.TabIndex = 5;
            this.txtBarcode.Visible = false;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // txtTicket
            // 
            this.txtTicket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTicket.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtTicket.Location = new System.Drawing.Point(704, 31);
            this.txtTicket.Name = "txtTicket";
            this.txtTicket.Size = new System.Drawing.Size(234, 35);
            this.txtTicket.TabIndex = 4;
            // 
            // lbStaffName
            // 
            this.lbStaffName.AutoSize = true;
            this.lbStaffName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStaffName.ForeColor = System.Drawing.Color.Blue;
            this.lbStaffName.Location = new System.Drawing.Point(115, 60);
            this.lbStaffName.Name = "lbStaffName";
            this.lbStaffName.Size = new System.Drawing.Size(92, 24);
            this.lbStaffName.TabIndex = 0;
            this.lbStaffName.Text = "Manager";
            // 
            // cbbSupplier
            // 
            this.cbbSupplier.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSupplier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbSupplier.FormattingEnabled = true;
            this.cbbSupplier.ItemHeight = 24;
            this.cbbSupplier.Location = new System.Drawing.Point(428, 72);
            this.cbbSupplier.Name = "cbbSupplier";
            this.cbbSupplier.Size = new System.Drawing.Size(404, 32);
            this.cbbSupplier.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Staff Name: ";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(428, 31);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(201, 35);
            this.dtpDate.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "CableID: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(628, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 24);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ticket: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(332, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Date: ";
            // 
            // lbSubTotal
            // 
            this.lbSubTotal.AutoSize = true;
            this.lbSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSubTotal.ForeColor = System.Drawing.Color.Red;
            this.lbSubTotal.Location = new System.Drawing.Point(115, 90);
            this.lbSubTotal.Name = "lbSubTotal";
            this.lbSubTotal.Size = new System.Drawing.Size(21, 24);
            this.lbSubTotal.TabIndex = 1;
            this.lbSubTotal.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(332, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Supplier: ";
            // 
            // lbCableID
            // 
            this.lbCableID.AutoSize = true;
            this.lbCableID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCableID.ForeColor = System.Drawing.Color.Green;
            this.lbCableID.Location = new System.Drawing.Point(115, 30);
            this.lbCableID.Name = "lbCableID";
            this.lbCableID.Size = new System.Drawing.Size(21, 24);
            this.lbCableID.TabIndex = 0;
            this.lbCableID.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 1;
            this.label3.Text = "SubTotal:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 120);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1024, 648);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1024, 648);
            this.panel4.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1024, 573);
            this.panel7.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.lvReceive);
            this.panel10.Controls.Add(this.tableLayoutPanel1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(373, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(435, 571);
            this.panel10.TabIndex = 3;
            // 
            // lvReceive
            // 
            this.lvReceive.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvReceive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvReceive.FullRowSelect = true;
            this.lvReceive.GridLines = true;
            this.lvReceive.Location = new System.Drawing.Point(0, 0);
            this.lvReceive.MultiSelect = false;
            this.lvReceive.Name = "lvReceive";
            this.lvReceive.Size = new System.Drawing.Size(435, 491);
            this.lvReceive.TabIndex = 0;
            this.lvReceive.UseCompatibleStateImageBehavior = false;
            this.lvReceive.View = System.Windows.Forms.View.Details;
            this.lvReceive.SelectedIndexChanged += new System.EventHandler(this.lvReceive_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No";
            this.columnHeader1.Width = 43;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Item Name";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Qty";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 57;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Price";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 73;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Total";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 111;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnAllMenu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnTyreMenu, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFuel, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDeleteAll, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDelete, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 491);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(435, 80);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnAllMenu
            // 
            this.btnAllMenu.BackColor = System.Drawing.Color.Aqua;
            this.btnAllMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllMenu.Enabled = false;
            this.btnAllMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllMenu.ForeColor = System.Drawing.Color.White;
            this.btnAllMenu.Location = new System.Drawing.Point(0, 0);
            this.btnAllMenu.Margin = new System.Windows.Forms.Padding(0);
            this.btnAllMenu.Name = "btnAllMenu";
            this.btnAllMenu.Size = new System.Drawing.Size(87, 80);
            this.btnAllMenu.TabIndex = 1;
            this.btnAllMenu.UseVisualStyleBackColor = false;
            this.btnAllMenu.Click += new System.EventHandler(this.btnAllMenu_Click);
            // 
            // btnTyreMenu
            // 
            this.btnTyreMenu.BackColor = System.Drawing.Color.Fuchsia;
            this.btnTyreMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTyreMenu.Enabled = false;
            this.btnTyreMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTyreMenu.ForeColor = System.Drawing.Color.White;
            this.btnTyreMenu.Location = new System.Drawing.Point(87, 0);
            this.btnTyreMenu.Margin = new System.Windows.Forms.Padding(0);
            this.btnTyreMenu.Name = "btnTyreMenu";
            this.btnTyreMenu.Size = new System.Drawing.Size(87, 80);
            this.btnTyreMenu.TabIndex = 2;
            this.btnTyreMenu.UseVisualStyleBackColor = false;
            this.btnTyreMenu.Click += new System.EventHandler(this.btnTyreMenu_Click);
            // 
            // btnFuel
            // 
            this.btnFuel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuel.ForeColor = System.Drawing.Color.White;
            this.btnFuel.Location = new System.Drawing.Point(174, 0);
            this.btnFuel.Margin = new System.Windows.Forms.Padding(0);
            this.btnFuel.Name = "btnFuel";
            this.btnFuel.Size = new System.Drawing.Size(87, 80);
            this.btnFuel.TabIndex = 3;
            this.btnFuel.Text = "Fuel";
            this.btnFuel.UseVisualStyleBackColor = false;
            this.btnFuel.Click += new System.EventHandler(this.btnFuel_Click);
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.BackColor = System.Drawing.Color.Red;
            this.btnDeleteAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDeleteAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.ForeColor = System.Drawing.Color.White;
            this.btnDeleteAll.Location = new System.Drawing.Point(348, 0);
            this.btnDeleteAll.Margin = new System.Windows.Forms.Padding(0);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(87, 80);
            this.btnDeleteAll.TabIndex = 0;
            this.btnDeleteAll.Text = "Delete All";
            this.btnDeleteAll.UseVisualStyleBackColor = false;
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(261, 0);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(87, 80);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.ucMenuReceiveStock);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(373, 571);
            this.panel9.TabIndex = 2;
            // 
            // ucMenuReceiveStock
            // 
            this.ucMenuReceiveStock._Barcode = null;
            this.ucMenuReceiveStock._CheckConnectionServer = false;
            this.ucMenuReceiveStock._ClickGroup = false;
            this.ucMenuReceiveStock._ClickItems = true;
            this.ucMenuReceiveStock._ConnectionServer = false;
            this.ucMenuReceiveStock._Delete = DataObject.DeleteType.None;
            this.ucMenuReceiveStock._ExistsItem = false;
            this.ucMenuReceiveStock._GroupID = 0;
            this.ucMenuReceiveStock._GroupMenu = null;
            this.ucMenuReceiveStock._IsFuel = DataObject.IsFuelType.None;
            this.ucMenuReceiveStock._ItemID = 0;
            this.ucMenuReceiveStock._ItemsMenu = null;
            this.ucMenuReceiveStock._LoadMenu = true;
            this.ucMenuReceiveStock._Type = DataObject.ListItemType.Items;
            this.ucMenuReceiveStock._Visual = DataObject.VisualType.None;
            this.ucMenuReceiveStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMenuReceiveStock.GroupDisplayOrderMax = 0;
            this.ucMenuReceiveStock.ItemDisplayOrderMax = 0;
            this.ucMenuReceiveStock.Location = new System.Drawing.Point(0, 0);
            this.ucMenuReceiveStock.Margin = new System.Windows.Forms.Padding(6);
            this.ucMenuReceiveStock.mDBConfig = null;
            this.ucMenuReceiveStock.mShortcut = DataObject.TypeMenu.ALLMENU;
            this.ucMenuReceiveStock.Name = "ucMenuReceiveStock";
            this.ucMenuReceiveStock.Size = new System.Drawing.Size(373, 571);
            this.ucMenuReceiveStock.TabIndex = 0;
            this.ucMenuReceiveStock.Click += new System.EventHandler(this.ucMenuReceiveStock_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel3);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(808, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(214, 571);
            this.panel8.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uCkeypad);
            this.panel3.Controls.Add(this.txtPriceSell);
            this.panel3.Controls.Add(this.txtQtyNew);
            this.panel3.Controls.Add(this.txtQtyOnHand);
            this.panel3.Controls.Add(this.txtQtyAdd);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.txtInterest);
            this.panel3.Controls.Add(this.txtTotalSell);
            this.panel3.Controls.Add(this.txtTotalAdd);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.txtItemName);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.txtPrice);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(214, 571);
            this.panel3.TabIndex = 3;
            // 
            // uCkeypad
            // 
            this.uCkeypad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uCkeypad.Location = new System.Drawing.Point(3, 300);
            this.uCkeypad.Margin = new System.Windows.Forms.Padding(6);
            this.uCkeypad.Name = "uCkeypad";
            this.uCkeypad.Size = new System.Drawing.Size(212, 271);
            this.uCkeypad.TabIndex = 0;
            this.uCkeypad.txtResult = null;
            // 
            // txtPriceSell
            // 
            this.txtPriceSell._Keypad = null;
            this.txtPriceSell._LabelStatusError = null;
            this.txtPriceSell._LocketText = false;
            this.txtPriceSell._MaxNumber = 10000D;
            this.txtPriceSell._Money = null;
            this.txtPriceSell._Precision = 2;
            this.txtPriceSell._StatusError = "Invalid";
            this.txtPriceSell._Type = POS.Controls.TextBoxType.NumberInterger;
            this.txtPriceSell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtPriceSell.Enabled = false;
            this.txtPriceSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtPriceSell.Location = new System.Drawing.Point(111, 121);
            this.txtPriceSell.Name = "txtPriceSell";
            this.txtPriceSell.Size = new System.Drawing.Size(100, 26);
            this.txtPriceSell.TabIndex = 1;
            this.txtPriceSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtQtyNew
            // 
            this.txtQtyNew._Keypad = null;
            this.txtQtyNew._LabelStatusError = null;
            this.txtQtyNew._LocketText = false;
            this.txtQtyNew._MaxNumber = 10000D;
            this.txtQtyNew._Money = null;
            this.txtQtyNew._Precision = 2;
            this.txtQtyNew._StatusError = "Invalid";
            this.txtQtyNew._Type = POS.Controls.TextBoxType.NumberInterger;
            this.txtQtyNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtQtyNew.Enabled = false;
            this.txtQtyNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtQtyNew.Location = new System.Drawing.Point(112, 91);
            this.txtQtyNew.Name = "txtQtyNew";
            this.txtQtyNew.Size = new System.Drawing.Size(100, 26);
            this.txtQtyNew.TabIndex = 1;
            this.txtQtyNew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtQtyOnHand
            // 
            this.txtQtyOnHand._Keypad = null;
            this.txtQtyOnHand._LabelStatusError = null;
            this.txtQtyOnHand._LocketText = false;
            this.txtQtyOnHand._MaxNumber = 10000D;
            this.txtQtyOnHand._Money = null;
            this.txtQtyOnHand._Precision = 2;
            this.txtQtyOnHand._StatusError = "Invalid";
            this.txtQtyOnHand._Type = POS.Controls.TextBoxType.NumberInterger;
            this.txtQtyOnHand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtQtyOnHand.Enabled = false;
            this.txtQtyOnHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtQtyOnHand.Location = new System.Drawing.Point(111, 61);
            this.txtQtyOnHand.Name = "txtQtyOnHand";
            this.txtQtyOnHand.Size = new System.Drawing.Size(100, 26);
            this.txtQtyOnHand.TabIndex = 1;
            this.txtQtyOnHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtQtyAdd
            // 
            this.txtQtyAdd._Keypad = this.uCkeypad;
            this.txtQtyAdd._LabelStatusError = this.lbStatus;
            this.txtQtyAdd._LocketText = false;
            this.txtQtyAdd._MaxNumber = 1000000D;
            this.txtQtyAdd._Money = null;
            this.txtQtyAdd._Precision = 2;
            this.txtQtyAdd._StatusError = "Invalid";
            this.txtQtyAdd._Type = POS.Controls.TextBoxType.NumberInterger;
            this.txtQtyAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtQtyAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtQtyAdd.Location = new System.Drawing.Point(111, 151);
            this.txtQtyAdd.Name = "txtQtyAdd";
            this.txtQtyAdd.Size = new System.Drawing.Size(100, 26);
            this.txtQtyAdd.TabIndex = 1;
            this.txtQtyAdd.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(75, 0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(800, 73);
            this.lbStatus.TabIndex = 3;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 92);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 24);
            this.label15.TabIndex = 0;
            this.label15.Text = "Qty New: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 62);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 24);
            this.label11.TabIndex = 0;
            this.label11.Text = "On Hand: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 152);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Qty Add: ";
            // 
            // txtInterest
            // 
            this.txtInterest._Keypad = null;
            this.txtInterest._LabelStatusError = null;
            this.txtInterest._LocketText = false;
            this.txtInterest._MaxNumber = -1D;
            this.txtInterest._Money = null;
            this.txtInterest._Precision = 2;
            this.txtInterest._StatusError = "Invalid";
            this.txtInterest._Type = POS.Controls.TextBoxType.All;
            this.txtInterest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInterest.Enabled = false;
            this.txtInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtInterest.Location = new System.Drawing.Point(111, 271);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(100, 26);
            this.txtInterest.TabIndex = 1;
            this.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalSell
            // 
            this.txtTotalSell._Keypad = null;
            this.txtTotalSell._LabelStatusError = null;
            this.txtTotalSell._LocketText = false;
            this.txtTotalSell._MaxNumber = -1D;
            this.txtTotalSell._Money = null;
            this.txtTotalSell._Precision = 2;
            this.txtTotalSell._StatusError = "Invalid";
            this.txtTotalSell._Type = POS.Controls.TextBoxType.All;
            this.txtTotalSell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTotalSell.Enabled = false;
            this.txtTotalSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtTotalSell.Location = new System.Drawing.Point(111, 241);
            this.txtTotalSell.Name = "txtTotalSell";
            this.txtTotalSell.Size = new System.Drawing.Size(100, 26);
            this.txtTotalSell.TabIndex = 1;
            this.txtTotalSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalAdd
            // 
            this.txtTotalAdd._Keypad = null;
            this.txtTotalAdd._LabelStatusError = null;
            this.txtTotalAdd._LocketText = false;
            this.txtTotalAdd._MaxNumber = -1D;
            this.txtTotalAdd._Money = null;
            this.txtTotalAdd._Precision = 2;
            this.txtTotalAdd._StatusError = "Invalid";
            this.txtTotalAdd._Type = POS.Controls.TextBoxType.All;
            this.txtTotalAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTotalAdd.Enabled = false;
            this.txtTotalAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtTotalAdd.Location = new System.Drawing.Point(111, 211);
            this.txtTotalAdd.Name = "txtTotalAdd";
            this.txtTotalAdd.Size = new System.Drawing.Size(100, 26);
            this.txtTotalAdd.TabIndex = 1;
            this.txtTotalAdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 122);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Sell Price: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 182);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Price Add: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 272);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 24);
            this.label14.TabIndex = 0;
            this.label14.Text = "Interest:";
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtItemName.Enabled = false;
            this.txtItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtItemName.Location = new System.Drawing.Point(9, 31);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(202, 26);
            this.txtItemName.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 242);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 24);
            this.label13.TabIndex = 0;
            this.label13.Text = "Total Sell:";
            // 
            // txtPrice
            // 
            this.txtPrice._Keypad = this.uCkeypad;
            this.txtPrice._LabelStatusError = this.lbStatus;
            this.txtPrice._LocketText = false;
            this.txtPrice._MaxNumber = 1000000D;
            this.txtPrice._Money = null;
            this.txtPrice._Precision = 3;
            this.txtPrice._StatusError = "Invalid";
            this.txtPrice._Type = POS.Controls.TextBoxType.NumberDouble;
            this.txtPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtPrice.Location = new System.Drawing.Point(111, 181);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 26);
            this.txtPrice.TabIndex = 1;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 212);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Total Add:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 4);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "Item Name:";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lbStatus);
            this.panel6.Controls.Add(this.tlpButton);
            this.panel6.Controls.Add(this.btnExit);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(0, 573);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1024, 75);
            this.panel6.TabIndex = 1;
            // 
            // tlpButton
            // 
            this.tlpButton.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tlpButton.ColumnCount = 1;
            this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpButton.Controls.Add(this.btnReceive, 0, 0);
            this.tlpButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.tlpButton.Location = new System.Drawing.Point(875, 0);
            this.tlpButton.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButton.Name = "tlpButton";
            this.tlpButton.RowCount = 1;
            this.tlpButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButton.Size = new System.Drawing.Size(147, 73);
            this.tlpButton.TabIndex = 1;
            // 
            // btnReceive
            // 
            this.btnReceive.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnReceive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReceive.ForeColor = System.Drawing.Color.White;
            this.btnReceive.Location = new System.Drawing.Point(3, 3);
            this.btnReceive.Margin = new System.Windows.Forms.Padding(0);
            this.btnReceive.Name = "btnReceive";
            this.btnReceive.Size = new System.Drawing.Size(141, 67);
            this.btnReceive.TabIndex = 2;
            this.btnReceive.Text = "Receive";
            this.btnReceive.UseVisualStyleBackColor = false;
            this.btnReceive.Click += new System.EventHandler(this.btnReceive_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 73);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmReceiveStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReceiveStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RECEIVE STOCK";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmReceiveStock_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tlpButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpButton;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel3;
        private Controls.UCkeypad uCkeypad;
        private Controls.TextBoxPOSInto txtQtyAdd;
        private System.Windows.Forms.Label label6;
        private Controls.TextBoxPOSInto txtTotalAdd;
        private System.Windows.Forms.Label label7;
        private Controls.TextBoxPOSKeyBoard txtItemName;
        private Controls.TextBoxPOSInto txtPrice;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Controls.TextBoxPOSInto txtQtyOnHand;
        private System.Windows.Forms.Label label11;
        private Controls.TextBoxPOSInto txtPriceSell;
        private System.Windows.Forms.Label label12;
        private Controls.TextBoxPOSInto txtInterest;
        private Controls.TextBoxPOSInto txtTotalSell;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Controls.TextBoxPOSInto txtQtyNew;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnAllMenu;
        private System.Windows.Forms.Button btnTyreMenu;
        private System.Windows.Forms.Button btnFuel;
    }
}