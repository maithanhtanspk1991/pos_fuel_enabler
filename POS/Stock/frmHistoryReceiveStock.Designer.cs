﻿namespace POS.Stock
{
    partial class frmHistoryReceiveStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnView = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lvReceiveLine = new System.Windows.Forms.ListView();
            this.chItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chQty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTotal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonExit1 = new POS.Controls.ButtonExit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lvReceive = new System.Windows.Forms.ListView();
            this.chDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSubTotal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chStaffName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSupplierName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTicketID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnView);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dtpEnd);
            this.panel1.Controls.Add(this.dtpStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(920, 87);
            this.panel1.TabIndex = 0;
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnView.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.White;
            this.btnView.Location = new System.Drawing.Point(761, 23);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(156, 44);
            this.btnView.TabIndex = 50;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(405, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 37);
            this.label2.TabIndex = 49;
            this.label2.Text = "To:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 37);
            this.label1.TabIndex = 49;
            this.label1.Text = "From:";
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "dd/MM/yyyy";
            this.dtpEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(475, 23);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(279, 44);
            this.dtpEnd.TabIndex = 48;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "dd/MM/yyyy";
            this.dtpStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(120, 23);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(279, 44);
            this.dtpStart.TabIndex = 47;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lvReceiveLine);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 378);
            this.panel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(920, 372);
            this.panel2.TabIndex = 1;
            // 
            // lvReceiveLine
            // 
            this.lvReceiveLine.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chItemName,
            this.chQty,
            this.chPrice,
            this.chTotal});
            this.lvReceiveLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvReceiveLine.FullRowSelect = true;
            this.lvReceiveLine.GridLines = true;
            this.lvReceiveLine.Location = new System.Drawing.Point(0, 0);
            this.lvReceiveLine.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lvReceiveLine.MultiSelect = false;
            this.lvReceiveLine.Name = "lvReceiveLine";
            this.lvReceiveLine.Size = new System.Drawing.Size(920, 299);
            this.lvReceiveLine.TabIndex = 2;
            this.lvReceiveLine.UseCompatibleStateImageBehavior = false;
            this.lvReceiveLine.View = System.Windows.Forms.View.Details;
            // 
            // chItemName
            // 
            this.chItemName.Text = "Item Name";
            this.chItemName.Width = 216;
            // 
            // chQty
            // 
            this.chQty.Text = "Qty";
            this.chQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chQty.Width = 232;
            // 
            // chPrice
            // 
            this.chPrice.Text = "Price";
            this.chPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chPrice.Width = 191;
            // 
            // chTotal
            // 
            this.chTotal.Text = "Total";
            this.chTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chTotal.Width = 250;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonExit1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 299);
            this.panel4.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(920, 73);
            this.panel4.TabIndex = 1;
            // 
            // buttonExit1
            // 
            this.buttonExit1.BackColor = System.Drawing.Color.Red;
            this.buttonExit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonExit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.buttonExit1.ForeColor = System.Drawing.Color.White;
            this.buttonExit1.Location = new System.Drawing.Point(0, 0);
            this.buttonExit1.Margin = new System.Windows.Forms.Padding(0);
            this.buttonExit1.Name = "buttonExit1";
            this.buttonExit1.Size = new System.Drawing.Size(75, 73);
            this.buttonExit1.TabIndex = 0;
            this.buttonExit1.Text = "Exit";
            this.buttonExit1.UseVisualStyleBackColor = false;
            this.buttonExit1.Click += new System.EventHandler(this.buttonExit1_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lvReceive);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 87);
            this.panel3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(920, 291);
            this.panel3.TabIndex = 2;
            // 
            // lvReceive
            // 
            this.lvReceive.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chDate,
            this.chSubTotal,
            this.chStaffName,
            this.chSupplierName,
            this.chTicketID});
            this.lvReceive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvReceive.FullRowSelect = true;
            this.lvReceive.GridLines = true;
            this.lvReceive.Location = new System.Drawing.Point(0, 0);
            this.lvReceive.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lvReceive.MultiSelect = false;
            this.lvReceive.Name = "lvReceive";
            this.lvReceive.Size = new System.Drawing.Size(920, 291);
            this.lvReceive.TabIndex = 0;
            this.lvReceive.UseCompatibleStateImageBehavior = false;
            this.lvReceive.View = System.Windows.Forms.View.Details;
            this.lvReceive.SelectedIndexChanged += new System.EventHandler(this.lvReceive_SelectedIndexChanged);
            // 
            // chDate
            // 
            this.chDate.Text = "Date";
            this.chDate.Width = 164;
            // 
            // chSubTotal
            // 
            this.chSubTotal.Text = "SubTotal";
            this.chSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chSubTotal.Width = 208;
            // 
            // chStaffName
            // 
            this.chStaffName.Text = "Staff Name";
            this.chStaffName.Width = 264;
            // 
            // chSupplierName
            // 
            this.chSupplierName.Text = "Supplier Name";
            this.chSupplierName.Width = 187;
            // 
            // chTicketID
            // 
            this.chTicketID.Text = "TicketID";
            this.chTicketID.Width = 161;
            // 
            // frmHistoryReceiveStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 750);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "frmHistoryReceiveStock";
            this.Text = "History Receive Stock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmHistoryReceiveStock_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView lvReceive;
        private System.Windows.Forms.ListView lvReceiveLine;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ColumnHeader chDate;
        private System.Windows.Forms.ColumnHeader chSubTotal;
        private System.Windows.Forms.ColumnHeader chStaffName;
        private System.Windows.Forms.ColumnHeader chSupplierName;
        private System.Windows.Forms.ColumnHeader chTicketID;
        private Controls.ButtonExit buttonExit1;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.ColumnHeader chItemName;
        private System.Windows.Forms.ColumnHeader chQty;
        private System.Windows.Forms.ColumnHeader chPrice;
        private System.Windows.Forms.ColumnHeader chTotal;
    }
}