namespace POS.Stock
{
    partial class frmStockCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNavigation = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnFuel = new System.Windows.Forms.Button();
            this.btnReview = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnFindItem = new System.Windows.Forms.Button();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.lbStatus = new System.Windows.Forms.Label();
            this.buttonExit = new POS.Controls.ButtonExit();
            this.pnLeft = new System.Windows.Forms.Panel();
            this.ucMenuChange = new POS.Controls.UCMenu();
            this.pnRight = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.pnContent = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tlpButtonTypeMenu = new System.Windows.Forms.TableLayoutPanel();
            this.btnAllMenu = new System.Windows.Forms.Button();
            this.btnTyreMenu = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.pnLeft.SuspendLayout();
            this.pnRight.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tlpButtonTypeMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbNavigation
            // 
            this.lbNavigation.BackColor = System.Drawing.Color.Purple;
            this.lbNavigation.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNavigation.ForeColor = System.Drawing.Color.White;
            this.lbNavigation.Location = new System.Drawing.Point(0, 0);
            this.lbNavigation.Name = "lbNavigation";
            this.lbNavigation.Size = new System.Drawing.Size(422, 54);
            this.lbNavigation.TabIndex = 0;
            this.lbNavigation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(0, 639);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(422, 70);
            this.panel2.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.btnFuel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnReview, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSubmit, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFindItem, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(422, 70);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnFuel
            // 
            this.btnFuel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuel.ForeColor = System.Drawing.Color.White;
            this.btnFuel.Location = new System.Drawing.Point(107, 3);
            this.btnFuel.Margin = new System.Windows.Forms.Padding(0);
            this.btnFuel.Name = "btnFuel";
            this.btnFuel.Size = new System.Drawing.Size(101, 64);
            this.btnFuel.TabIndex = 4;
            this.btnFuel.Text = "Fuel";
            this.btnFuel.UseVisualStyleBackColor = false;
            this.btnFuel.Click += new System.EventHandler(this.btnFuel_Click);
            // 
            // btnReview
            // 
            this.btnReview.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnReview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReview.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReview.ForeColor = System.Drawing.Color.White;
            this.btnReview.Location = new System.Drawing.Point(3, 3);
            this.btnReview.Margin = new System.Windows.Forms.Padding(0);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(101, 64);
            this.btnReview.TabIndex = 0;
            this.btnReview.Text = "REVIEW";
            this.btnReview.UseVisualStyleBackColor = false;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.Red;
            this.btnSubmit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSubmit.Location = new System.Drawing.Point(315, 3);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(0);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(104, 64);
            this.btnSubmit.TabIndex = 0;
            this.btnSubmit.Text = "SUBMIT";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnFindItem
            // 
            this.btnFindItem.BackColor = System.Drawing.Color.Magenta;
            this.btnFindItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFindItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindItem.ForeColor = System.Drawing.Color.White;
            this.btnFindItem.Location = new System.Drawing.Point(211, 3);
            this.btnFindItem.Margin = new System.Windows.Forms.Padding(0);
            this.btnFindItem.Name = "btnFindItem";
            this.btnFindItem.Size = new System.Drawing.Size(101, 64);
            this.btnFindItem.TabIndex = 0;
            this.btnFindItem.Text = "FIND ITEM";
            this.btnFindItem.UseVisualStyleBackColor = false;
            this.btnFindItem.Click += new System.EventHandler(this.btnFindItem_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.lbStatus);
            this.pnBottom.Controls.Add(this.buttonExit);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 711);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1024, 57);
            this.pnBottom.TabIndex = 4;
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(73, 0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(951, 57);
            this.lbStatus.TabIndex = 2;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.Red;
            this.buttonExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonExit.ForeColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(0, 0);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(0);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(73, 57);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnLeft
            // 
            this.pnLeft.Controls.Add(this.ucMenuChange);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(600, 711);
            this.pnLeft.TabIndex = 5;
            // 
            // ucMenuChange
            // 
            this.ucMenuChange._Barcode = null;
            this.ucMenuChange._CheckConnectionServer = false;
            this.ucMenuChange._ClickGroup = false;
            this.ucMenuChange._ClickItems = true;
            this.ucMenuChange._ConnectionServer = false;
            this.ucMenuChange._Delete = DataObject.DeleteType.None;
            this.ucMenuChange._ExistsItem = false;
            this.ucMenuChange._GroupID = 0;
            this.ucMenuChange._GroupMenu = null;
            this.ucMenuChange._IsFuel = DataObject.IsFuelType.None;
            this.ucMenuChange._ItemID = 0;
            this.ucMenuChange._ItemsMenu = null;
            this.ucMenuChange._LoadMenu = true;
            this.ucMenuChange._Type = DataObject.ListItemType.Items;
            this.ucMenuChange._Visual = DataObject.VisualType.None;
            this.ucMenuChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucMenuChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMenuChange.GroupDisplayOrderMax = 0;
            this.ucMenuChange.ItemDisplayOrderMax = 0;
            this.ucMenuChange.Location = new System.Drawing.Point(0, 0);
            this.ucMenuChange.mDBConfig = null;
            this.ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
            this.ucMenuChange.Name = "ucMenuChange";
            this.ucMenuChange.Size = new System.Drawing.Size(600, 711);
            this.ucMenuChange.TabIndex = 0;
            this.ucMenuChange.Load += new System.EventHandler(this.ucMenuChange_Load);
            this.ucMenuChange.Click += new System.EventHandler(this.ucMenuChange_Click);
            // 
            // pnRight
            // 
            this.pnRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnRight.Controls.Add(this.panel1);
            this.pnRight.Controls.Add(this.panel3);
            this.pnRight.Controls.Add(this.panel2);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRight.Location = new System.Drawing.Point(600, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(424, 711);
            this.pnRight.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtBarcode);
            this.panel1.Controls.Add(this.pnContent);
            this.panel1.Controls.Add(this.lbNavigation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 72);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 567);
            this.panel1.TabIndex = 6;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(312, 12);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(100, 20);
            this.txtBarcode.TabIndex = 2;
            this.txtBarcode.Visible = false;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // pnContent
            // 
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(0, 54);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(422, 513);
            this.pnContent.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tlpButtonTypeMenu);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(422, 72);
            this.panel3.TabIndex = 8;
            this.panel3.Visible = false;
            // 
            // tlpButtonTypeMenu
            // 
            this.tlpButtonTypeMenu.ColumnCount = 2;
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtonTypeMenu.Controls.Add(this.btnAllMenu, 0, 0);
            this.tlpButtonTypeMenu.Controls.Add(this.btnTyreMenu, 1, 0);
            this.tlpButtonTypeMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtonTypeMenu.Location = new System.Drawing.Point(0, 0);
            this.tlpButtonTypeMenu.Name = "tlpButtonTypeMenu";
            this.tlpButtonTypeMenu.RowCount = 1;
            this.tlpButtonTypeMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtonTypeMenu.Size = new System.Drawing.Size(422, 72);
            this.tlpButtonTypeMenu.TabIndex = 0;
            // 
            // btnAllMenu
            // 
            this.btnAllMenu.BackColor = System.Drawing.Color.Aqua;
            this.btnAllMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllMenu.ForeColor = System.Drawing.Color.White;
            this.btnAllMenu.Location = new System.Drawing.Point(3, 3);
            this.btnAllMenu.Name = "btnAllMenu";
            this.btnAllMenu.Size = new System.Drawing.Size(205, 66);
            this.btnAllMenu.TabIndex = 0;
            this.btnAllMenu.Text = "ALL MENU";
            this.btnAllMenu.UseVisualStyleBackColor = false;
            this.btnAllMenu.Click += new System.EventHandler(this.btnAllMenu_Click);
            // 
            // btnTyreMenu
            // 
            this.btnTyreMenu.BackColor = System.Drawing.Color.Fuchsia;
            this.btnTyreMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTyreMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTyreMenu.ForeColor = System.Drawing.Color.White;
            this.btnTyreMenu.Location = new System.Drawing.Point(214, 3);
            this.btnTyreMenu.Name = "btnTyreMenu";
            this.btnTyreMenu.Size = new System.Drawing.Size(205, 66);
            this.btnTyreMenu.TabIndex = 1;
            this.btnTyreMenu.Text = "TYRE MENU";
            this.btnTyreMenu.UseVisualStyleBackColor = false;
            this.btnTyreMenu.Click += new System.EventHandler(this.btnTyreMenu_Click);
            // 
            // frmStockCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnRight);
            this.Controls.Add(this.pnLeft);
            this.Controls.Add(this.pnBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmStockCheck";
            this.Text = "frmMenuChange";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMenuChange_FormClosing);
            this.Load += new System.EventHandler(this.frmMenuChange_Load);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pnBottom.ResumeLayout(false);
            this.pnLeft.ResumeLayout(false);
            this.pnRight.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tlpButtonTypeMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.UCMenu ucMenuChange;
        private Controls.ButtonExit buttonExit;
        private System.Windows.Forms.Label lbNavigation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel pnLeft;
        private System.Windows.Forms.Panel pnRight;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnFindItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tlpButtonTypeMenu;
        private System.Windows.Forms.Button btnAllMenu;
        private System.Windows.Forms.Button btnTyreMenu;
        private System.Windows.Forms.Button btnFuel;
    }
}
