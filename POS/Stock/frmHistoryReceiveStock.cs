﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmHistoryReceiveStock : Form
    {
        private SystemConfig.DBConfig mDBConfig;
        private Class.MoneyFortmat mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

        public frmHistoryReceiveStock(SystemConfig.DBConfig dBConfig)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
            SetMultiLanguage();
        }
        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Từ:";
                label2.Text = "Đến:";
                label2.Font = new Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnView.Text = "Xem";
                chDate.Text = "Ngày";
                chSubTotal.Text = "Thành tiền";
                chStaffName.Text = "Tên nhân viên";
                chSupplierName.Text = "Người giám sát";
                chTicketID.Text = "Mã vé";
                chItemName.Text = "Mã sản phẩm";
                chQty.Text = "Số lượng";
                chPrice.Text = "Giá";
                chTotal.Text = "Tổng cộng";
                buttonExit1.Text = "Thoát";
                buttonExit1.Font = new Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }
        private void btnView_Click(object sender, EventArgs e)
        {
            LoadReceive();
        }

        private void buttonExit1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void frmHistoryReceiveStock_Load(object sender, EventArgs e)
        {
            SetSizeListView();
            SetTime();
            LoadReceive();
        }

        #region LoadData

        private void LoadReceive()
        {
            lvReceive.Items.Clear();
            List<DataObject.Receive> lsArray = new List<DataObject.Receive>();
            lsArray = BusinessObject.BOReceive.GetReceive(-1, -1, -1, dtpStart.Value, dtpEnd.Value, mDBConfig);
            int ReceiveID = 0;
            foreach (DataObject.Receive item in lsArray)
            {
                ListViewItem li = new ListViewItem(DataObject.DateTimeFormat.DisplayDateTime(item.Date));
                li.SubItems.Add(mMoney.FormatNew3(item.SubTotal));
                li.SubItems.Add(item.StaffName);
                li.SubItems.Add(item.Suppliers.SupplierName);
                li.SubItems.Add(item.TicketID);
                li.Tag = item;
                if (ReceiveID == 0)
                    ReceiveID = item.ID;
                lvReceive.Items.Add(li);
            }
            if (ReceiveID != 0)
                LoadReceiveLine(ReceiveID);
        }

        private void LoadReceiveLine(int ReceiveID)
        {
            lvReceiveLine.Items.Clear();
            List<DataObject.ReceiveLine> lsArray = new List<DataObject.ReceiveLine>();
            lsArray = BusinessObject.BOReceiveLine.GetReceiveLine(ReceiveID, -1, mDBConfig);
            foreach (DataObject.ReceiveLine item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.ItemsMenu.ItemShort);
                li.SubItems.Add(DataObject.MonneyFormat.FormatNumber(item.Qty / item.SellSize));
                li.SubItems.Add(mMoney.FormatNew3(item.Price));
                li.SubItems.Add(mMoney.FormatNew3(item.Total));
                li.Tag = item;
                lvReceiveLine.Items.Add(li);
            }
        }

        private void SetSizeListView()
        {
            int Width = lvReceive.Width;
            lvReceive.Columns[0].Width = 20 * Width / 100;
            lvReceive.Columns[1].Width = 13 * Width / 100;
            lvReceive.Columns[2].Width = 20 * Width / 100;
            lvReceive.Columns[3].Width = 20 * Width / 100;
            lvReceive.Columns[4].Width = 15 * Width / 100;

            Width = lvReceiveLine.Width;
            lvReceiveLine.Columns[0].Width = 35 * Width / 100;
            lvReceiveLine.Columns[1].Width = 20 * Width / 100;
            lvReceiveLine.Columns[2].Width = 20 * Width / 100;
            lvReceiveLine.Columns[3].Width = 20 * Width / 100;
        }

        private void SetTime()
        {
            dtpStart.Value = DateTime.Now.AddDays(-6);
            dtpEnd.Value = DateTime.Now;
        }

        #endregion LoadData

        private void lvReceive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                DataObject.Receive item = (DataObject.Receive)lvReceive.SelectedItems[0].Tag;
                LoadReceiveLine(item.ID);
            }
        }
    }
}