﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Stock
{
    public partial class frmReceiveStock : Form
    {
        private bool Cal = false;
        private SystemConfig.DBConfig mDBConfig;
        private Class.MoneyFortmat mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        private Barcode.SerialPort mSerialPortBarcode;
        private DataObject.TypeMenu mShortcut = DataObject.TypeMenu.ALLMENU;
        private DataObject.Transit mTransit;
        private string strBarcode = "";
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmReceiveStock(SystemConfig.DBConfig dBConfig, Barcode.SerialPort serialPortBarcode, DataObject.Transit transit)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
            mSerialPortBarcode = serialPortBarcode;
            mTransit = transit;
            ucMenuReceiveStock.mDBConfig = mDBConfig;
            SetBarcode();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                groupBox1.Text = "Nhận";
                label4.Text = "Mã:";
                label1.Text = "Tên nhân viên:";
                label3.Text = "Thành tiền:";
                label5.Text = "Ngày:";
                label2.Text = "Nhà cung cấp:";
                label2.Font = new Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label10.Text = "Vé:";
                btnFuel.Text = "Nhiên liệu";
                btnReceive.Text = "Nhận";
                btnDelete.Text = "Xóa";
                btnDeleteAll.Text = "Xóa tất cả";
                btnExit.Text = "THOÁT";
                btnExit.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label9.Text = "Tên sản phẩm:";
                label15.Text = "SL mới:";
                label12.Text = "Giá bán:";
                label6.Text = "SL thêm:";
                label7.Text = "Giá thêm:";
                label8.Text = "Tổng thêm:";
                label13.Text = "Tổng bán:";
                label14.Text = "Lãi suất:";
                columnHeader1.Text = "Mã";
                columnHeader2.Text = "Tên sản phẩm";
                columnHeader3.Text = "Số lượng";
                columnHeader4.Text = "Giá";
                columnHeader5.Text = "Tổng cộng";
                return;
            }
        }

        public DataObject.ReceiveLine mReceiveLine { get; set; }

        public Point GetPositionInForm(Control ctrl)
        {
            Point p = new Point();
            p = ctrl.Parent.PointToScreen(ctrl.Location);
            p.X = p.X + ctrl.Width;
            return p;
        }

        private void btnAllMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.ALLMENU)
            {
                mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuReceiveStock.mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuReceiveStock.Refresh();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Xóa thành công";
                    lbStatus.Text = "Deleted Successfull";
                    lvReceive.SelectedItems[0].Remove();
                    SubTotal();
                    ResetNo();
                    SelectedBottomListview();
                    return;
                }
                lbStatus.Text = "Deleted Successfull";
                lvReceive.SelectedItems[0].Remove();
                SubTotal();
                ResetNo();
                SelectedBottomListview();
            }
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            if (lvReceive.Items.Count < 2)
                lvReceive.Items.Clear();
            else
            {
                POS.Forms.frmMessageBox frmMessageBox = new POS.Forms.frmMessageBox("Delete", "Do you want delete all?");
                if (frmMessageBox.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lvReceive.Items.Clear();
                        lbStatus.Text = "Xóa tất cả thành công";
                        SelectedBottomListview();
                        return;
                    }
                    lvReceive.Items.Clear();
                    lbStatus.Text = "Deleted all Successfull";
                    SelectedBottomListview();
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnFuel_Click(object sender, EventArgs e)
        {
            lbStatus.Text = "";
            frmPopupFuel frmPopupFuel = new frmPopupFuel(mDBConfig);
            frmPopupFuel.Location = GetPositionInForm(ucMenuReceiveStock);
            if (frmPopupFuel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (frmPopupFuel.ItemsMenu != null)
                {
                    AddListView(ConvertItemsMenuToReceiveLine(frmPopupFuel.ItemsMenu), lvReceive.Items.Count + 1);
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Không có nhiên liệu";
                    return;
                }
                lbStatus.Text = "No item fuel";
            }
        }

        private void btnReceive_Click(object sender, EventArgs e)
        {
            if (lvReceive.Items.Count > 0)
            {
                DataObject.Receive item = new DataObject.Receive();
                item.TicketID = txtTicket.Text.Trim();
                item.SubTotal = Convert.ToDouble(lbSubTotal.Text);
                item.SupplierID = Convert.ToInt32(cbbSupplier.SelectedValue);
                item.StaffID = mTransit.StaffID;
                item.Date = dtpDate.Value;
                item.CableID = mTransit.CableID;
                foreach (ListViewItem li in lvReceive.Items)
                {
                    DataObject.ReceiveLine rl = (DataObject.ReceiveLine)li.Tag;
                    rl.Qty = rl.SellSize > 0 ? rl.Qty * rl.SellSize : rl.Qty;
                    //rl.Qty = rl.IsFuel ? rl.Qty * 1000 : rl.Qty;
                    item.ReceiveLine.Add(rl.Copy());
                }

                if (BusinessObject.BOReceive.Insert(item, mDBConfig) > 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Nhận thành công";
                        Default();
                        return;
                    }
                    lbStatus.Text = "Receive Successfull";
                    Default();
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Nhận thất bại!";
                        return;
                    }
                    lbStatus.Text = "Receive failed!";
                }
            }
        }

        private void btnTyreMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.TYREMENU)
            {
                mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuReceiveStock.mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuReceiveStock.Refresh();
            }
        }

        private DataObject.ReceiveLine ConvertItemsMenuToReceiveLine(DataObject.ItemsMenu item)
        {
            DataObject.ReceiveLine result = new DataObject.ReceiveLine();
            result.ItemID = item.ItemID;
            result.ItemsMenu = item;
            result.Price = item.UnitPrice / 1000;
            result.PriceSell = item.UnitPrice / 1000;
            result.Qty = 1;
            result.IsFuel = item.IsFuel;
            result.Total = result.Price * result.Qty;
            if (!item.IsFuel)
                result.SellSize = item.SellSize > 0 ? item.SellSize : 1;
            result.SellSize = item.IsFuel ? 1000 : result.SellSize;
            result.QtyOnHand = BusinessObject.BOStockCheckItem.GetQtyOnHand(item.ItemID, mDBConfig) / result.SellSize;
            return result;
        }

        //private void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    if (lvReceive.SelectedItems.Count > 0)
        //    {
        //        if (txtQty.Text == "" || txtPrice.Text == "")
        //        {
        //            lbStatus.Text = "Imformation Empty";
        //            return;
        //        }
        //        ListViewItem li = lvReceive.SelectedItems[0];
        //        DataObject.ReceiveLine item = ((DataObject.ReceiveLine)lvReceive.SelectedItems[0].Tag);
        //        item.Qty = Convert.ToInt32(txtQty.Text.Trim());
        //        item.Price = Convert.ToDouble(txtPrice.Text.Trim());
        //        item.Total = item.Qty * item.Price;
        //        li.SubItems[2].Text = txtQty.Text;
        //        li.SubItems[3].Text = DataObject.MonneyFormat.Format2(txtPrice.Text);
        //        li.SubItems[4].Text = DataObject.MonneyFormat.Format2(item.Total);
        //        lbStatus.Text = "Update Successfull";
        //        SubTotal();
        //    }
        //}
        private void frmReceiveStock_Load(object sender, EventArgs e)
        {
            SetSizeLsitView();
            LoadSupplier();
            ucMenuReceiveStock.InitMenu(mDBConfig);
            LoadConfig();
        }

        private void lvReceive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                lbStatus.Text = "";
                mReceiveLine = ((DataObject.ReceiveLine)lvReceive.SelectedItems[0].Tag);
                SetTextBoxReceiveLine();
                txtQtyAdd.Focus();
                txtQtyAdd.SelectAll();
            }
        }

        private void ResetNo()
        {
            int i = 1;
            foreach (ListViewItem li in lvReceive.Items)
            {
                li.SubItems[0].Text = (i++) + "";
            }
        }

        private void SelectedBottomListview()
        {
            if (lvReceive.Items.Count > 0)
            {
                lvReceive.Items[lvReceive.Items.Count - 1].Selected = true;
            }
            else
            {
                TextBoxDefault();
            }
        }

        private void SetBarcode()
        {
            //mSerialPortBarcode.ComPortTextBox = txtBarcode;
        }

        #region LoadData

        public void CalTotal()
        {
            //Giá nhập
            if (txtPrice.Text != "" && txtQtyAdd.Text != "")
            {
                double Price = Convert.ToDouble(txtPrice.Text.Trim());
                int Qty = Convert.ToInt32(txtQtyAdd.Text.Trim());
                txtTotalAdd.Text = mMoney.FormatNew3(Price * Qty);
            }
            else
            {
                txtTotalAdd.Text = "0";
            }
            //Giá bán
            if (txtPriceSell.Text != "" && txtQtyAdd.Text != "")
            {
                double Price = Convert.ToDouble(txtPriceSell.Text.Trim());
                int Qty = Convert.ToInt32(txtQtyAdd.Text.Trim());
                txtTotalSell.Text = mMoney.FormatNew3(Price * Qty);
            }
            else
            {
                txtTotalSell.Text = "0";
            }
            //Lãi
            if (txtTotalSell.Text != "" && txtTotalAdd.Text != "")
            {
                double TotalSell = Convert.ToDouble(txtTotalSell.Text.Trim());
                double TotalAdd = Convert.ToDouble(txtTotalAdd.Text.Trim());
                txtInterest.Text = mMoney.FormatNew3(TotalSell - TotalAdd);
            }
        }

        public void SubTotal()
        {
            double total = 0;
            foreach (ListViewItem li in lvReceive.Items)
            {
                DataObject.ReceiveLine item = (DataObject.ReceiveLine)li.Tag;
                total += item.Total;
            }
            lbSubTotal.Text = mMoney.FormatNew3(total);
        }

        private void AddListView(DataObject.ReceiveLine item, int no)
        {
            ListViewItem li = new ListViewItem(no.ToString());
            li.SubItems.Add(item.ItemsMenu.ItemShort);
            li.SubItems.Add(DataObject.MonneyFormat.FormatNumber(item.Qty));
            li.SubItems.Add(mMoney.FormatNew3(item.Price));
            li.SubItems.Add(mMoney.FormatNew3(item.Total));
            li.Tag = item;
            lvReceive.Items.Add(li);
            lvReceive.Items[lvReceive.Items.Count - 1].Selected = true;
            SubTotal();
        }

        private void CalQty()
        {
            if (txtQtyAdd.Text != "" && txtQtyOnHand.Text != "")
            {
                int QtyAdd = mMoney.NumberToInt(txtQtyAdd.Text);
                int QtyOnHand = mMoney.NumberToInt(txtQtyOnHand.Text);
                txtQtyNew.Text = mMoney.FormatNumber((QtyOnHand + QtyAdd));
            }
            else if (txtQtyAdd.Text != "")
            {
                txtQtyNew.Text = txtQtyAdd.Text;
            }
            else
            {
                txtQtyNew.Text = txtQtyOnHand.Text;
            }
        }

        private void Default()
        {
            lvReceive.Items.Clear();
            txtItemName.Text = "";
            txtPrice.Text = "";
            txtQtyAdd.Text = "";
            txtTotalAdd.Text = "";
            txtTicket.Text = "";
            dtpDate.Value = DateTime.Now;
            lbSubTotal.Text = "0";
        }

        private void LoadConfig()
        {
            lbCableID.Text = mTransit.CableID.ToString();
            lbStaffName.Text = mTransit.StaffName;
        }

        private void LoadSupplier()
        {
            List<DataObject.Suppliers> lsArray = new List<DataObject.Suppliers>();
            lsArray = BusinessObject.BOSuppliers.GetSuppliers("", 1, mDBConfig);
            cbbSupplier.DataSource = lsArray;
            cbbSupplier.DisplayMember = "SupplierName";
            cbbSupplier.ValueMember = "SupplierID";
        }

        private void SetSizeLsitView()
        {
            int Width = lvReceive.Width;
            lvReceive.Columns[0].Width = Width * 10 / 100;
            lvReceive.Columns[1].Width = Width * 45 / 100;
            lvReceive.Columns[2].Width = Width * 14 / 100;
            lvReceive.Columns[3].Width = Width * 14 / 100;
            lvReceive.Columns[4].Width = Width * 14 / 100;
        }

        private void SetTextBoxReceiveLine()
        {
            Cal = false;
            txtItemName.Text = mReceiveLine.ItemsMenu.ItemShort;
            txtPrice.Text = mMoney.FormatNew3(mReceiveLine.Price);
            txtPriceSell.Text = mMoney.FormatNew3(mReceiveLine.PriceSell);
            txtQtyAdd.Text = mReceiveLine.Qty.ToString();
            txtQtyOnHand.Text = mReceiveLine.QtyOnHand.ToString();
            Cal = true;
            CalQty();
            CalTotal();
        }

        #endregion LoadData

        private void TextBoxDefault()
        {
            txtItemName.Text = "";
            txtQtyAdd.Text = "0";
            txtPrice.Text = "0";
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text != "")
            {
                strBarcode = txt.Text;
                ucMenuReceiveStock._Barcode = txtBarcode.Text;
                if (ucMenuReceiveStock.RefreshBarcode())
                {
                    if (ucMenuReceiveStock._ItemsMenu != null)
                    {
                        AddListView(ConvertItemsMenuToReceiveLine(ucMenuReceiveStock._ItemsMenu), lvReceive.Items.Count + 1);
                    }
                    lbStatus.Text = "";
                }
                else
                {
                    lbStatus.Text = "";
                }
                strBarcode = "";
                txt.Text = "";
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            if (Cal)
            {
                if (txtPrice.Text == ".")
                {
                    txtPrice.Text = "0.";
                    txtPrice.Select(2, 1);
                }
                UpdateListViewItem();
                CalTotal();
                SubTotal();
            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (Cal)
            {
                UpdateListViewItem();
                CalTotal();
                CalQty();
                SubTotal();
            }
        }

        private void ucMenuReceiveStock_Click(object sender, EventArgs e)
        {
            if (ucMenuReceiveStock._ItemsMenu != null)
            {
                AddListView(ConvertItemsMenuToReceiveLine(ucMenuReceiveStock._ItemsMenu), lvReceive.Items.Count + 1);
            }
            lbStatus.Text = "";
        }

        private void UpdateListViewItem()
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                if (txtQtyAdd.Text == "" || txtPrice.Text == "")
                {
                    if(mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text="Thông tin bị trống";
                        return;
                    }
                    lbStatus.Text = "Imformation Empty";
                    return;
                }
                ListViewItem li = lvReceive.SelectedItems[0];
                DataObject.ReceiveLine item = ((DataObject.ReceiveLine)lvReceive.SelectedItems[0].Tag);
                item.Qty = Convert.ToInt32(txtQtyAdd.Text.Trim());
                item.Price = Convert.ToDouble(txtPrice.Text.Trim());
                item.Total = item.Qty * item.Price;
                li.SubItems[2].Text = txtQtyAdd.Text;
                li.SubItems[3].Text = txtPrice.Text;
                li.SubItems[4].Text = mMoney.FormatNew3(item.Total);
                SubTotal();
            }
        }
    }
}