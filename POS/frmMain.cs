﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Forms;

namespace POS
{
    public partial class frmMain : Form
    {
        private List<Class.Item> list = new List<Class.Item>();
        private Connection.Connection conn;
        private Barcode.SerialPort mSerialPort;

        //private List<Class.ProcessOrder.Order> listorder = new List<Class.ProcessOrder.Order>();
        private frmOrdersAll frmOder;

        private BarPrinterServer printServer;
        private Class.Functions fun = new Functions();
        private Class.MoneyFortmat money;
        private System.Threading.Thread threadChangeColor;
        private System.Threading.Thread threadRefreshListView;

        private delegate void ChangeColorCallback(Color color, Control control);

        private delegate void ChangeTextCallback(string text, Control control);

        private delegate void ProcessListViewItemCallback(int type, System.Windows.Forms.ListView list);

        public string staffid;

        public frmMain()
        {
            InitializeComponent();
            //conn = new Connection.Connection();
            //money = new MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            //mSerialPort = new Barcode.SerialPort();
            ////updatabase to sertver
            //printServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            //threadChangeColor = new System.Threading.Thread(runChangeColor);
            //threadChangeColor.Start();
            ////threadRefreshListView = new System.Threading.Thread(RefreshListView);
            ////threadRefreshListView.Start();
            //UCInfoTop uc = new UCInfoTop();
            //panel4.Controls.Add(uc);
            //InitFormMain();
        }

        public frmMain(string sid)
        {
            InitializeComponent();
            //conn = new Connection.Connection();
            //money = new MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            //mSerialPort = new Barcode.SerialPort();
            //printServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            //threadChangeColor = new System.Threading.Thread(runChangeColor);
            //threadChangeColor.Start();
            ////threadRefreshListView = new System.Threading.Thread(RefreshListView);
            ////threadRefreshListView.Start();
            //UCInfoTop uc = new UCInfoTop();
            //panel4.Controls.Add(uc);
            //InitFormMain();
            staffid = sid;
        }

        private void InitFormMain()
        {
            conn = new Connection.Connection();
            money = new MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPort = new Barcode.SerialPort();
            //mSerialPort.OpenAndStart();
            //updatabase to sertver
            printServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            threadChangeColor = new System.Threading.Thread(runChangeColor);
            threadChangeColor.Start();
            //threadRefreshListView = new System.Threading.Thread(RefreshListView);
            //threadRefreshListView.Start();
            UCInfoTop uc = new UCInfoTop();
            panel4.Controls.Add(uc);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //this.Hide();
            //Forms.frmAbout frmAbout = new Forms.frmAbout(this);
            //frmAbout.Show();
            InitFormMain();
            Class.LogPOS.WriteLog("Load from frmMain!");
            //Class.UpdateToServer.UpDatabaseToServer(new Connection.Connection());
            frmOder = new frmOrdersAll(printServer, conn, money, mSerialPort);
            showFromOrder("0");
            frmOder.Visible = false;
            try
            {
                for (int i = 1; i <= 60; i++)
                {
                    UCTable uct = new UCTable();
                    uct.Name = "" + i;
                    uct.label3.Text = "" + i;
                    uct.label3.Name = "" + i;
                    //uct.Tag = "" + i;
                    flp_eatin.Controls.Add(uct);
                    uct.Click += new EventHandler(uct_Click);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmMain.frmMain_Load:::" + ex.Message);
                //MessageBox.Show("Loi " + ex.Message);
            }
            threadRefreshListView = new System.Threading.Thread(RefreshListView);
            threadRefreshListView.Start();
            //frmAbout.IsStoped = true;
        }

        private void uct_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmMain.uct_Click.");
            UCTable btn = (UCTable)sender;
            showFromOrder(btn.label3.Name);
        }

        private void btn_main_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmMain.btn_main_Click.");
            showFromOrder("");
        }

        private void showFromOrder(string tableID)
        {
            frmOder.LoadOrder(tableID, 1);
            frmOder.Show();
        }

        private void btn_setting_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Load form frmSetting.");
            frmSetting frm = new frmSetting(null);
            frm.Show();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Class.LogPOS.WriteLog("frmMain_FormClosed.");
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                //Console.WriteLine(pr.ProcessName);
                if (pr.ProcessName == "POS")
                    pr.Kill();
            }
        }

        public void ChangeColor(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeColorCallback d = new ChangeColorCallback(ChangeColor);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.BackColor = color;
            }
        }

        public void SetText(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        public void ProcessListViewItem(int type, System.Windows.Forms.ListView list)
        {
            //private delegate void AddListViewItemCallback(System.Windows.Forms.ListViewItem li, System.Windows.Forms.ListView list);
            if (list.InvokeRequired)
            {
                ProcessListViewItemCallback d = new ProcessListViewItemCallback(ProcessListViewItem);
                list.Invoke(d, new object[] { type, list });
            }
            else
            {
                //change
                if (type == 1)
                {
                    list.Items.Clear();
                    List<Class.Functions.LastOrder> listlastorder = fun.GetLastOrders();
                    foreach (Class.Functions.LastOrder lastorder in listlastorder)
                    {
                        ListViewItem lvi = new ListViewItem(lastorder.TableID.ToString());
                        ListViewItem.ListViewSubItem subtotal = new ListViewItem.ListViewSubItem(lvi, money.Format(Convert.ToDouble(lastorder.SubTotal.ToString())));
                        ListViewItem.ListViewSubItem cableid = new ListViewItem.ListViewSubItem(lvi, lastorder.CableID.ToString());
                        lvi.SubItems.Add(subtotal);
                        lvi.SubItems.Add(cableid);
                        //lvi.Tag = lastorder.OrderID;
                        //listView1.Items.Add(lvi);
                        list.Items.Add(lvi);
                    }
                    list.Tag = listlastorder;
                }
                else
                {
                    list.Items.Clear();
                    List<Class.Functions.LastPaidOrder> listlastorder = fun.GetLastPaidOrders();
                    foreach (Class.Functions.LastPaidOrder lastorder in listlastorder)
                    {
                        ListViewItem lvi = new ListViewItem(lastorder.TableID.ToString());
                        ListViewItem.ListViewSubItem subtotal = new ListViewItem.ListViewSubItem(lvi, money.Format(Convert.ToDouble(lastorder.SubTotal.ToString())));
                        ListViewItem.ListViewSubItem cableid = new ListViewItem.ListViewSubItem(lvi, lastorder.CableID.ToString());
                        lvi.SubItems.Add(subtotal);
                        lvi.SubItems.Add(cableid);
                        //lvi.Tag = lastorder.OrderID;
                        list.Items.Add(lvi);
                    }
                    list.Tag = listlastorder;
                }
            }
        }

        private void runChangeColor()
        {
            while (true)
            {
                for (int i = 0; i < flp_eatin.Controls.Count; i++)
                {
                    try
                    {
                        UCTable uc = (UCTable)flp_eatin.Controls[i];
                        Class.Functions.table tbl = fun.GetStatusTable(flp_eatin.Controls[i].Name);
                        if (tbl.complete == 1)
                        {
                            uc.Tag = null;
                            SetText(" ", uc.lbtime);
                        }
                        else
                        {
                            if (uc.Tag == null)
                            {
                                uc.Tag = tbl.time;
                            }
                            SetText(GetLongTime(uc.Tag.ToString()), uc.lbtime);
                        }
                        if (tbl.complete == 0)
                        {
                            ChangeColor(Color.CornflowerBlue, flp_eatin.Controls[i]);
                            if (Convert.ToInt32(tbl.people) == 0)
                            {
                                SetText(" ", uc.lbpeople);
                            }
                            else
                            {
                                SetText(tbl.people + "", uc.lbpeople);
                            }
                            if (Convert.ToInt32(tbl.staff) == 0)
                            {
                                SetText(" ", uc.lbstaff);
                            }
                            else
                            {
                                SetText(tbl.staff + "", uc.lbstaff);
                            }
                            SetText(money.Format(tbl.subtotal) + "", uc.lbsubtotal);
                            //SetText(GetLongTime(tbl.time) + "", uc.lbtime);
                        }
                        else if (tbl.complete == 1)
                        {
                            ChangeColor(Color.White, flp_eatin.Controls[i]);
                            //SetText(tbl.people + "", uc.lbpeople);
                            //SetText(tbl.staff + "", uc.lbstaff);
                            //SetText(money.Format(tbl.subtotal) + "", uc.lbsubtotal);
                            SetText(" ", uc.lbpeople);
                            SetText(" ", uc.lbstaff);
                            SetText(" ", uc.lbsubtotal);
                            //SetText(GetLongTime(tbl.time) + "", uc.lbtime);
                        }
                        else if (tbl.complete == 2)
                        {
                            ChangeColor(Color.Orange, flp_eatin.Controls[i]);
                            if (Convert.ToInt32(tbl.people) == 0)
                            {
                                SetText(" ", uc.lbpeople);
                            }
                            else
                            {
                                SetText(tbl.people + "", uc.lbpeople);
                            }
                            //SetText(tbl.people + "", uc.lbpeople);
                            if (Convert.ToInt32(tbl.staff) == 0)
                            {
                                SetText(" ", uc.lbstaff);
                            }
                            else
                            {
                                SetText(tbl.staff + "", uc.lbstaff);
                            }
                            //SetText(tbl.staff + "", uc.lbstaff);
                            SetText(money.Format(tbl.subtotal) + "", uc.lbsubtotal);
                            //SetText(GetLongTime(tbl.time) + "", uc.lbtime);
                        }
                        else if (tbl.complete == 3)
                        {
                            ChangeColor(Color.Green, flp_eatin.Controls[i]);
                            if (Convert.ToInt32(tbl.people) == 0)
                            {
                                SetText(" ", uc.lbpeople);
                            }
                            else
                            {
                                SetText(tbl.people + "", uc.lbpeople);
                            }
                            //SetText(tbl.people + "", uc.lbpeople);
                            if (Convert.ToInt32(tbl.staff) == 0)
                            {
                                SetText(" ", uc.lbstaff);
                            }
                            else
                            {
                                SetText(tbl.staff + "", uc.lbstaff);
                            }
                            //SetText(tbl.staff + "", uc.lbstaff);
                            SetText(money.Format(tbl.subtotal) + "", uc.lbsubtotal);
                            //SetText(GetLongTime(tbl.time) + "", uc.lbtime);
                        }
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("frmMain.runChangeColor:::" + ex.Message);
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void RefreshListView()
        {
            while (true)
            {
                GetListViewLastOrder();
                GetListViewLastPaidOrder();
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Do you want exit POS System? ", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    Class.LogPOS.WriteLog("frmMain.btnexit_Click.");
            //    Application.Exit();
            //}
            frmMessageBox frm = new frmMessageBox("Information", "Do you want exit POS System?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                Class.LogPOS.WriteLog("frmMain.btnexit_Click.");
                Application.Exit();
            }
        }

        private string GetLongTime(string time)
        {
            try
            {
                DateTime curent = DateTime.Now;
                DateTime dt = Convert.ToDateTime(curent.Year + "-" + curent.Month + "-" + curent.Day + " " + curent.ToLongTimeString());

                DateTime old = Convert.ToDateTime(time);

                TimeSpan ts = dt - old;
                int totaSecond = (int)ts.TotalSeconds;
                string sTime = (totaSecond / 3600) + "";
                if (sTime.Length == 1)
                {
                    sTime = "0" + sTime;
                }
                string sMinute = ((totaSecond / 60) % 60) + "";
                if (sMinute.Length == 1)
                {
                    sMinute = "0" + sMinute;
                }
                string sSecond = (totaSecond % 60) + "";
                if (sSecond.Length == 1)
                {
                    sSecond = "0" + sSecond;
                }
                //Console.WriteLine(sTime + ":" + sMinute + ":" + sSecond);
                return sTime + ":" + sMinute + ":" + sSecond;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmMain.GetLongTime:::" + ex.Message);
            }
            return null;
        }

        public void GetListViewLastOrder()
        {
            try
            {
                if (CheckChangeLastOrder())
                {
                    return;
                }
                //code add vo last order
                ProcessListViewItem(1, listView1);
            }
            catch (Exception)
            {
            }
        }

        public void GetListViewLastPaidOrder()
        {
            try
            {
                if (CheckPaidOrders())
                {
                    return;
                }
                //code add vo last paid order
                ProcessListViewItem(2, listView2);
            }
            catch (Exception)
            {
            }
        }

        public bool CheckPaidOrders()
        {
            bool resuilt = false;

            try
            {
                //foreach (System.Windows.Forms.ListViewItem item in listView2.Items)
                //{
                //    s += item.Tag.ToString() + ",";
                //}
                if (listView2.Tag != null)
                {
                    List<Class.Functions.LastPaidOrder> list = (List<Class.Functions.LastPaidOrder>)listView2.Tag;
                    int sumID = 0;
                    double sumTotal = 0;
                    foreach (Class.Functions.LastPaidOrder item in list)
                    {
                        sumID += item.OrderID;
                        sumTotal += item.SubTotal;
                    }
                    string sql = "select sum(orderID) as sumId,sum(subTotal) as sumTotal from (select orderID,subTotal from ordersdaily where completed=1 order by ts desc limit 0,10) as tmp";
                    System.Data.DataTable tbl = conn.Select(sql);
                    if (tbl.Rows.Count > 0)
                    {
                        if (sumID == Convert.ToInt16(tbl.Rows[0]["sumId"]) && sumTotal == Convert.ToDouble(tbl.Rows[0]["sumTotal"]))
                        {
                            resuilt = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Class.LogPOS.WriteLog("Function.CheckChangeLastOrder:::" + ex.Message);
            }

            return resuilt;
        }

        public bool CheckChangeLastOrder()
        {
            bool resuilt = false;

            try
            {
                if (listView1.Tag != null)
                {
                    List<Class.Functions.LastOrder> list = (List<Class.Functions.LastOrder>)listView1.Tag;
                    int sumID = 0;
                    double sumTotal = 0;
                    foreach (Class.Functions.LastOrder item in list)
                    {
                        sumID += item.OrderID;
                        sumTotal += item.SubTotal;
                    }
                    string sql = "select sum(orderID) as sumId,sum(subTotal) as sumTotal from (select orderID,subTotal from ordersdaily where completed<>1 order by ts desc limit 0,10) as tmp";
                    System.Data.DataTable tbl = conn.Select(sql);
                    if (tbl.Rows.Count > 0)
                    {
                        if (sumID == Convert.ToInt16(tbl.Rows[0]["sumId"]) && sumTotal == Convert.ToDouble(tbl.Rows[0]["sumTotal"]))
                        {
                            resuilt = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Class.LogPOS.WriteLog("Function.CheckChangeLastOrder:::" + ex.Message);
            }

            return resuilt;
        }
    }
}