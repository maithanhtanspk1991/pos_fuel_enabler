﻿namespace POS
{
    partial class UCCardButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCard = new System.Windows.Forms.Button();
            this.lblNameCard = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnCard
            // 
            this.btnCard.BackColor = System.Drawing.Color.White;
            this.btnCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCard.ForeColor = System.Drawing.Color.Navy;
            this.btnCard.Image = global::POS.Properties.Resources.UnCheck;
            this.btnCard.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnCard.Location = new System.Drawing.Point(0, 0);
            this.btnCard.Name = "btnCard";
            this.btnCard.Size = new System.Drawing.Size(221, 80);
            this.btnCard.TabIndex = 0;
            this.btnCard.UseVisualStyleBackColor = false;
            this.btnCard.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // lblNameCard
            // 
            this.lblNameCard.BackColor = System.Drawing.Color.White;
            this.lblNameCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameCard.ForeColor = System.Drawing.Color.Navy;
            this.lblNameCard.Location = new System.Drawing.Point(73, 12);
            this.lblNameCard.Name = "lblNameCard";
            this.lblNameCard.Size = new System.Drawing.Size(138, 23);
            this.lblNameCard.TabIndex = 1;
            this.lblNameCard.Text = "lbl";
            this.lblNameCard.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(73, 38);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(138, 26);
            this.txtTotalAmount.TabIndex = 2;
            this.txtTotalAmount.Click += new System.EventHandler(this.txtQuantity_Click);
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            this.txtTotalAmount.Enter += new System.EventHandler(this.txtQuantity_Enter);
            this.txtTotalAmount.Leave += new System.EventHandler(this.txtQuantity_Leave);
            // 
            // UCCardButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.lblNameCard);
            this.Controls.Add(this.btnCard);
            this.Name = "UCCardButton";
            this.Size = new System.Drawing.Size(221, 80);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnCard;
        public System.Windows.Forms.Label lblNameCard;
        public System.Windows.Forms.TextBox txtTotalAmount;

    }
}
