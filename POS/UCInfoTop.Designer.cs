﻿namespace POS
{
    partial class UCInfoTop
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbCableID = new System.Windows.Forms.Label();
            this.lbVersion = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbServerMode = new System.Windows.Forms.Label();
            this.lblShift = new System.Windows.Forms.Label();
            this.lbCash = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lbCableID
            // 
            this.lbCableID.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbCableID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCableID.ForeColor = System.Drawing.Color.White;
            this.lbCableID.Location = new System.Drawing.Point(90, 0);
            this.lbCableID.Name = "lbCableID";
            this.lbCableID.Size = new System.Drawing.Size(120, 29);
            this.lbCableID.TabIndex = 0;
            this.lbCableID.Text = "Cable ID: 1";
            this.lbCableID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbVersion
            // 
            this.lbVersion.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVersion.ForeColor = System.Drawing.Color.White;
            this.lbVersion.Location = new System.Drawing.Point(330, 0);
            this.lbVersion.Name = "lbVersion";
            this.lbVersion.Size = new System.Drawing.Size(200, 29);
            this.lbVersion.TabIndex = 2;
            this.lbVersion.Text = "becasPOS v1.11.4.14";
            this.lbVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTime
            // 
            this.lbTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(680, 0);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(200, 29);
            this.lbTime.TabIndex = 3;
            this.lbTime.Text = "14/04/2014 12:00:00";
            this.lbTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbServerMode
            // 
            this.lbServerMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbServerMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbServerMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lbServerMode.Location = new System.Drawing.Point(880, 0);
            this.lbServerMode.Name = "lbServerMode";
            this.lbServerMode.Size = new System.Drawing.Size(102, 29);
            this.lbServerMode.TabIndex = 4;
            this.lbServerMode.Text = "ONLINE";
            this.lbServerMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShift
            // 
            this.lblShift.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShift.ForeColor = System.Drawing.Color.White;
            this.lblShift.Location = new System.Drawing.Point(210, 0);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(120, 29);
            this.lblShift.TabIndex = 0;
            this.lblShift.Text = "Shift: 1";
            this.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblShift.Visible = false;
            // 
            // lbCash
            // 
            this.lbCash.BackColor = System.Drawing.Color.Transparent;
            this.lbCash.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCash.ForeColor = System.Drawing.Color.White;
            this.lbCash.Location = new System.Drawing.Point(530, 0);
            this.lbCash.Name = "lbCash";
            this.lbCash.Size = new System.Drawing.Size(150, 29);
            this.lbCash.TabIndex = 2;
            this.lbCash.Text = "Cash: 1000.00";
            this.lbCash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCash.Visible = false;
            // 
            // picLogo
            // 
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.picLogo.Image = global::POS.Properties.Resources.becas_technology11;
            this.picLogo.Location = new System.Drawing.Point(0, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(90, 29);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 5;
            this.picLogo.TabStop = false;
            // 
            // UCInfoTop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(85)))), ((int)(((byte)(160)))));
            this.Controls.Add(this.lbServerMode);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.lbCash);
            this.Controls.Add(this.lbVersion);
            this.Controls.Add(this.lblShift);
            this.Controls.Add(this.lbCableID);
            this.Controls.Add(this.picLogo);
            this.Name = "UCInfoTop";
            this.Size = new System.Drawing.Size(1024, 29);
            this.Load += new System.EventHandler(this.UCInfoTop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbVersion;
        private System.Windows.Forms.Label lbTime;
        public System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Label lbCableID;
        public System.Windows.Forms.Label lbServerMode;
        public System.Windows.Forms.Label lblShift;
        public System.Windows.Forms.Label lbCash;
        private System.Windows.Forms.PictureBox picLogo;

    }
}
