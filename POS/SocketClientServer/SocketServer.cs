﻿using System.Net.Sockets;

namespace POS.SocketClientServer
{
    internal class SocketServer
    {
        private TcpListener mTcpListener;
        private System.Threading.Thread mThread;
        private byte[] buff;

        public delegate void MyServerEvenHandler(SocketEventArgs e);

        private bool mIsRunning;

        public SocketServer()
        {
            mTcpListener = new TcpListener(12347);
            mTcpListener.Start();
        }

        private void RunThread()
        {
            while (mIsRunning)
            {
                Socket socket = mTcpListener.AcceptSocket();
                System.Threading.Thread t = new System.Threading.Thread(delegate() { ReceivedSocket(socket); });
                t.Start();
            }
        }

        private void ReceivedSocket(Socket socket)
        {
            int count = 0;
            while (count < 1000 && socket.Available == 0)
            {
                count++;
                System.Threading.Thread.Sleep(10);
            }
            buff = new byte[socket.Available];
            socket.Receive(buff);
            string s1 = "";
            for (int i = 0; i < buff.Length; i++)
            {
                s1 += buff[i] + ":";
            }
            string s = "";
            for (int i = 0; i < buff.Length; i++)
            {
                s += (char)buff[i] + ":";
            }
            ProcessData(buff);
        }

        private void ProcessData(byte[] buff)
        {
        }

        public class SocketEventArgs
        {
        }
    }
}