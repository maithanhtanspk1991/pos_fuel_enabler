﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BusinessObject;
using POS.Forms;
namespace POS
{
    public partial class UCReportShift : UserControl
    {
        //private Connection.Connection mconnect = new Connection.Connection();
        //private Class.Setting mSetting;
        private POS.Printer mPrinter;

        //private string ShiftID = "";
        //DataTable dtSource;
        //private Class.MoneyFortmat money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        //private string shiftID;
        private Class.MoneyFortmat mmoneyFortmat;
        BOShiftReport reshift;        
        public UCReportShift(Class.MoneyFortmat moneyFortmat)
        {
            InitializeComponent();
            SetMutiLanguage();
            mmoneyFortmat = moneyFortmat;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        public UCReportShift(string shiftid, Class.MoneyFortmat moneyFortmat)
        {
            InitializeComponent();
            //ShiftID = shiftid;
            //mSetting = new Class.Setting();
            mmoneyFortmat = moneyFortmat;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        private void SetMutiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                button1.Text = "In";
                label1.Text = "Báo cáo Ca làm việc";
                label11.Text = "Mã ca:";
                label2.Text = "Từ:";
                groupBox1.Text = "Bán hàng";
                label6.Text = "Tổng bán:";
                label17.Text = "Tổng giảm giá:";
                label25.Text = "Tổng GST:";
                label27.Text = "Thẻ:";
                label29.Text = "Tiền mặt:";
                label31.Text = "Trả lại hàng:";
                label13.Text = "Tài khoản:";
                label5.Text = "Đưa tiền vào:";
                label3.Text = "Xuất tiền ra:";
                label7.Text = "Thối tiền:";
                label20.Text = "Tổng tiền mặt:";
                groupBox3.Text = "Thanh toán tài khoản";
                label10.Text = "Thẻ:";
                label16.Text = "Tiền mặt";
                label23.Text = "Tổng cộng:";
                groupBox2.Text = "Thẻ";
                columnHeader7.Text = "Tên thẻ";
                columnHeader8.Text = "Thành tiền";
                label4.Text = "Tổng cộng:";
                label14.Text = "Ghi nợ:";
                return;
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            l_y = mPrinter.DrawString("Shift Report", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("( Shift : " + lbshiftid.Text + " )", e, new Font("Arial", 12, FontStyle.Bold), l_y, 2);
            l_y += 20;

            //l_y = mPrinter.DrawString(lbfrom.Text + " To " + lbto.Text, e, new Font("Arial", 8, FontStyle.Regular), l_y, 1);
            l_y = mPrinter.DrawString("Date: " + lbfrom.Text, e, new Font("Arial", 8, FontStyle.Regular), l_y, 1);
            if (CableID > 0)
                l_y = mPrinter.DrawString("Cable ID: " + CableID, e, new Font("Arial", 8, FontStyle.Regular), l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();

            mPrinter.DrawString("Cash Float In: ", e, new Font("Arial", 8, FontStyle.Regular), l_y, 1);
            l_y = mPrinter.DrawString(lbCashFloatIn.Text, e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            mPrinter.DrawString("Card: ", e, new Font("Arial", 10), l_y, 1);  // Review this
            l_y = mPrinter.DrawString("$" + lbcardsales.Text, e, new Font("Arial", 10), l_y, 3);
            if (ListViewCard.Items.Count != 0)
            {
                List<DataObject.CardReportShift> lst = new List<DataObject.CardReportShift>();
                List<DataObject.CardReportShift> _lst = new List<DataObject.CardReportShift>();
                reshift = new BOShiftReport();
                lst = reshift.getCardName();
                for (int j = 0; j < ListViewCard.Items.Count; j++)
                {
                    //foreach (ListViewItem item in ListViewCard.Items)
                    //{
                    for (int i = 0; i < lst.Count; i++)
                    {
                        if (ListViewCard.Items[j].SubItems[0].Text == lst[i].Name)
                        {
                            lst.Remove(lst[i]);

                            //mPrinter.DrawString("  *" + ListViewCard.Items[j].SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                            //l_y = mPrinter.DrawString("$" + ListViewCard.Items[j].SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                        }
                        //else
                        //{
                        //    mPrinter.DrawString("  *" + lst[i].Name + ": ", e, new Font("Arial", 10), l_y, 1);
                        //    l_y = mPrinter.DrawString("$" + "0.00", e, new Font("Arial", 10), l_y, 3);
                        //}
                    }

                    // }
                }

                foreach (ListViewItem item in ListViewCard.Items)
                {
                    mPrinter.DrawString("  *" + item.SubItems[0].Text + ": ", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString("$" + item.SubItems[1].Text, e, new Font("Arial", 10), l_y, 3);
                }
                for (int k = 0; k < lst.Count; k++)
                {
                    mPrinter.DrawString("  *" + lst[k].Name + ": ", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString("$" + "0.00", e, new Font("Arial", 10), l_y, 3);
                }
            }
            else
            {
                List<DataObject.CardReportShift> lst = new List<DataObject.CardReportShift>();
                reshift = new BOShiftReport();
                lst = reshift.getCardName();
                for (int i = 0; i < lst.Count; i++)
                {
                    mPrinter.DrawString("  *" + lst[i].Name + ": ", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString("$" + "0.00", e, new Font("Arial", 10), l_y, 3);
                }
            }
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            mPrinter.DrawString("Total Card: ", e, new Font("Arial", 10), l_y, 1);

            l_y = mPrinter.DrawString("$" + lblTotalCard.Text, e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawString("Cash Sales: ", e, new Font("Arial", 10), l_y, 1);

            l_y = mPrinter.DrawString(lbcashsales.Text, e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();
            mPrinter.DrawString("Total Sales: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + lbTotalsales.Text, e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            mPrinter.DrawString("Discount: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(lbtotaldiscount.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("GST: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(lbtotalgst.Text, e, new Font("Arial", 10), l_y, 3);

            //mPrinter.DrawString("Cash Float In: ", e, new Font("Arial", 10), l_y, 1);  // Lien01Feb12
            //l_y = mPrinter.DrawString(lbtotalgst.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Account: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + lbAccount.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Cash Out: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("-$" + lbCashOut.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Cash In: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("+$" + lbCashIn.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Pay Out: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("-$" + lbPayOut.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Safe Drop: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("-$" + lbSafeDrop.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Refund: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("-$" + lbRefunc.Text, e, new Font("Arial", 10), l_y, 3);
            double totalcash = (Convert.ToDouble(lbTotalsales.Text) - Convert.ToDouble(lbCashOut.Text) + Convert.ToDouble(lbCashIn.Text) - Convert.ToDouble(lbPayOut.Text) - Convert.ToDouble(lbSafeDrop.Text) - Convert.ToDouble(lbRefunc.Text));
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();

            mPrinter.DrawString("Total Cash: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + (lbtotalcash.Text), e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawLine("", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            mPrinter.DrawString("Account Cash: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(labelCashAccount.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Account Card: ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(labelCardAccount.Text, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Total Account:", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(labelTotalAccount.Text, e, new Font("Arial", 10), l_y, 3);

            //mPrinter.DrawString("Drive Off: ", e, new Font("Arial", 10), l_y, 1);  // Lien01Feb12
            //l_y = mPrinter.DrawString(lbRefunc.Text, e, new Font("Arial", 10), l_y, 3);

            //mPrinter.DrawString("Safe Drop: ", e, new Font("Arial", 10), l_y, 1); // Lien01Feb12
            //l_y = mPrinter.DrawString(lbSafeDrop.Text, e, new Font("Arial", 10), l_y, 3);

            //mPrinter.DrawString("Cash Float Out: ", e, new Font("Arial", 10), l_y, 1); // Lien01Feb12
            //l_y = mPrinter.DrawString(lbAccount.Text, e, new Font("Arial", 10), l_y, 3);
            mPrinter.DrawString("Till Balance: ", e, new Font("Arial", 10, FontStyle.Regular), l_y, 1);
            l_y = mPrinter.DrawString(lbCashFloatOut.Text, e, new Font("Arial", 10), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
        }

        public double cashTotal;

        private int CableID = 0;

        public void SetValue(DataObject.ShiftReport shiftrp, List<DataObject.CardReportShift> lstcard)
        {
            //lbshiftid.Text = shiftrp.ShiftName.ToString();
            CableID = shiftrp.CableID;
            lbshiftid.Text = shiftrp.strShiftName.ToString();
            lbfrom.Text = shiftrp.datetime.Day + "/" + shiftrp.datetime.Month + "/" + shiftrp.datetime.Year;
            lbTotalsales.Text = mmoneyFortmat.Format2(shiftrp.totalsales);
            lbtotaldiscount.Text = mmoneyFortmat.Format2(shiftrp.discount);
            lbtotalgst.Text = mmoneyFortmat.Format2(shiftrp.gst);
            lbcardsales.Text = mmoneyFortmat.Format2(shiftrp.card);
            lbcashsales.Text = mmoneyFortmat.Format2(shiftrp.cash);
            lbRefunc.Text = mmoneyFortmat.Format2(shiftrp.refun);
            lbCashIn.Text = mmoneyFortmat.Format2(shiftrp.CashIn);
            lbCashOut.Text = mmoneyFortmat.Format2(shiftrp.CashOut);
            lbPayOut.Text = mmoneyFortmat.Format2(shiftrp.PayOut);
            lbSafeDrop.Text = mmoneyFortmat.Format2(shiftrp.SafeDrop);
            lbAccount.Text = mmoneyFortmat.Format2(shiftrp.account);
            labelCashAccount.Text = mmoneyFortmat.Format2(shiftrp.cashAccount);
            labelCardAccount.Text = mmoneyFortmat.Format2(shiftrp.cardAccount);
            labelTotalAccount.Text = mmoneyFortmat.Format2(shiftrp.cashAccount + shiftrp.cardAccount);
            lblTotalCard.Text = mmoneyFortmat.Format2(shiftrp.cardAccount + shiftrp.card);
            lbAccount.Text = mmoneyFortmat.Format2(shiftrp.account);
            lbtotalcash.Text = mmoneyFortmat.Format2(shiftrp.totalsales - shiftrp.CashOut + shiftrp.CashIn - shiftrp.PayOut - shiftrp.SafeDrop - shiftrp.refun);
            foreach (var card in lstcard)
            {
                ListViewItem lvi = new ListViewItem(card.Name);
                lvi.SubItems.Add(mmoneyFortmat.Format2(card.Total));
                ListViewCard.Items.Add(lvi);
            }
            cashTotal = shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.refun;
            lbCashFloatIn.Text = mmoneyFortmat.Format2(shiftrp.CashFloatIn);
            lbCashFloatOut.Text = mmoneyFortmat.Format2(shiftrp.CashFloatIn + cashTotal);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Active || License.BOLicense.CheckLicense() == License.LicenseDialog.Trial)
            {
                Print();
            }
            //else
            //{
            //    string message = "Your license is expired. Contact Becas to renew your today!!!";
            //    frmMessageBoxOK frm = new frmMessageBoxOK("WARNING", message);
            //    frm.ShowDialog();
            //}
        }

        public void Print()
        {
            mPrinter.SetPrinterName(new Class.Setting().GetBillPrinter());
            mPrinter.Print();
        }
    }
}