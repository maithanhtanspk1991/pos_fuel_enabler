﻿namespace POS
{
    partial class UCNewGroup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbshort = new System.Windows.Forms.Label();
            this.lbdisplay = new System.Windows.Forms.Label();
            this.lbvisual = new System.Windows.Forms.Label();
            this.checkBoxpastfood = new System.Windows.Forms.CheckBox();
            this.lbfastfood = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.lboption = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lbshortcut = new System.Windows.Forms.Label();
            this.textBox6 = new POS.TextBoxPOSKeyPad();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(150, 184);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 88;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(150, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(240, 26);
            this.textBox1.TabIndex = 82;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // lbshort
            // 
            this.lbshort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbshort.AutoSize = true;
            this.lbshort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshort.Location = new System.Drawing.Point(23, 23);
            this.lbshort.Margin = new System.Windows.Forms.Padding(0);
            this.lbshort.Name = "lbshort";
            this.lbshort.Size = new System.Drawing.Size(110, 20);
            this.lbshort.TabIndex = 76;
            this.lbshort.Text = "Group Name";
            this.lbshort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdisplay
            // 
            this.lbdisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbdisplay.AutoSize = true;
            this.lbdisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdisplay.Location = new System.Drawing.Point(23, 65);
            this.lbdisplay.Margin = new System.Windows.Forms.Padding(0);
            this.lbdisplay.Name = "lbdisplay";
            this.lbdisplay.Size = new System.Drawing.Size(117, 20);
            this.lbdisplay.TabIndex = 79;
            this.lbdisplay.Text = "Display Order";
            this.lbdisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbvisual
            // 
            this.lbvisual.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbvisual.AutoSize = true;
            this.lbvisual.Enabled = false;
            this.lbvisual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbvisual.Location = new System.Drawing.Point(23, 180);
            this.lbvisual.Margin = new System.Windows.Forms.Padding(0);
            this.lbvisual.Name = "lbvisual";
            this.lbvisual.Size = new System.Drawing.Size(58, 20);
            this.lbvisual.TabIndex = 81;
            this.lbvisual.Text = "Visual";
            this.lbvisual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbvisual.Visible = false;
            this.lbvisual.Click += new System.EventHandler(this.lbvisual_Click);
            // 
            // checkBoxpastfood
            // 
            this.checkBoxpastfood.AutoSize = true;
            this.checkBoxpastfood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxpastfood.Location = new System.Drawing.Point(150, 149);
            this.checkBoxpastfood.Name = "checkBoxpastfood";
            this.checkBoxpastfood.Size = new System.Drawing.Size(15, 14);
            this.checkBoxpastfood.TabIndex = 91;
            this.checkBoxpastfood.UseVisualStyleBackColor = true;
            this.checkBoxpastfood.Visible = false;
            // 
            // lbfastfood
            // 
            this.lbfastfood.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbfastfood.AutoSize = true;
            this.lbfastfood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfastfood.Location = new System.Drawing.Point(23, 145);
            this.lbfastfood.Margin = new System.Windows.Forms.Padding(0);
            this.lbfastfood.Name = "lbfastfood";
            this.lbfastfood.Size = new System.Drawing.Size(100, 20);
            this.lbfastfood.TabIndex = 90;
            this.lbfastfood.Text = "Fast Foods";
            this.lbfastfood.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbfastfood.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.White;
            this.comboBox2.DropDownHeight = 250;
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.IntegralHeight = false;
            this.comboBox2.Location = new System.Drawing.Point(150, 102);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(240, 28);
            this.comboBox2.TabIndex = 93;
            this.comboBox2.Visible = false;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.comboBox2.Click += new System.EventHandler(this.comboBox2_Click);
            // 
            // lboption
            // 
            this.lboption.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lboption.AutoSize = true;
            this.lboption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lboption.Location = new System.Drawing.Point(24, 105);
            this.lboption.Margin = new System.Windows.Forms.Padding(0);
            this.lboption.Name = "lboption";
            this.lboption.Size = new System.Drawing.Size(62, 20);
            this.lboption.TabIndex = 92;
            this.lboption.Text = "Option";
            this.lboption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lboption.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.White;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "Option",
            "Food",
            "Take Away",
            "Drink"});
            this.comboBox1.Location = new System.Drawing.Point(150, 214);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(240, 28);
            this.comboBox1.TabIndex = 95;
            this.comboBox1.Visible = false;
            // 
            // lbshortcut
            // 
            this.lbshortcut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbshortcut.AutoSize = true;
            this.lbshortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshortcut.Location = new System.Drawing.Point(23, 221);
            this.lbshortcut.Margin = new System.Windows.Forms.Padding(0);
            this.lbshortcut.Name = "lbshortcut";
            this.lbshortcut.Size = new System.Drawing.Size(86, 20);
            this.lbshortcut.TabIndex = 94;
            this.lbshortcut.Text = "Short Cut";
            this.lbshortcut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbshortcut.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox6.IsLockDot = true;
            this.textBox6.Location = new System.Drawing.Point(150, 59);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(240, 26);
            this.textBox6.TabIndex = 89;
            // 
            // UCNewGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lbshortcut);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lboption);
            this.Controls.Add(this.checkBoxpastfood);
            this.Controls.Add(this.lbfastfood);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbshort);
            this.Controls.Add(this.lbdisplay);
            this.Controls.Add(this.lbvisual);
            this.Name = "UCNewGroup";
            this.Size = new System.Drawing.Size(412, 404);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbshort;
        private System.Windows.Forms.Label lbdisplay;
        private System.Windows.Forms.Label lbvisual;
        public System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.TextBox textBox1;
        internal TextBoxPOSKeyPad textBox6;
        public System.Windows.Forms.CheckBox checkBoxpastfood;
        public System.Windows.Forms.Label lbfastfood;
        public System.Windows.Forms.ComboBox comboBox2;
        public System.Windows.Forms.Label lboption;
        public System.Windows.Forms.ComboBox comboBox1;
        public System.Windows.Forms.Label lbshortcut;

    }
}
