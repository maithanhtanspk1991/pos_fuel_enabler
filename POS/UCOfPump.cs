﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCOfPump : UserControl
    {
        public string Name { get; set; }

        public string Weight { get; set; }

        public string Price { get; set; }

        public UCOfPump()
        {
            InitializeComponent();
        }

        private void lbName_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }

        private void lbMoney_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }

        private void lbWeight_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }

        private void UCOfPump_Load(object sender, EventArgs e)
        {
            lbName.Text = Name;
            lbMoney.Text = Price;
            lbWeight.Text = Weight;
        }

        public void SetValues()
        {
            lbName.Text = Name;
            lbMoney.Text = "$ " + Price;
            lbWeight.Text = Weight + " L";
        }
    }
}