﻿using System;

namespace POS.FormKey
{
    public partial class frmKeyBoardPOS : System.Windows.Forms.Form
    {
        private POS.Controls.UCKeyboard uck;

        public frmKeyBoardPOS(string text)
        {
            uck = new POS.Controls.UCKeyboard();
            uck.txtresult.Text = text;
        }

        protected override void OnLoad(EventArgs e)
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Width = uck.Width;
            this.Height = uck.Height;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            uck.btnexit.Click += new EventHandler(btnexit_Click);
            uck.btnenter.Click += new EventHandler(btnenter_Click);
            this.Controls.Add(uck);
            base.OnLoad(e);
        }

        public string GetText()
        {
            return uck.txtresult.Text;
        }

        private void btnenter_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}