﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS.FormKey
{
    public partial class frmKeyPadLimit : Form
    {
        private TextBox mTextBox;
        private bool mIsFirstLoad = true;

        public bool IsNegative { get; set; }

        private bool misLockDot;

        public frmKeyPadLimit(TextBox textBox, bool isLockDot)
        {
            IsNegative = false;
            InitializeComponent();
            mTextBox = textBox;
            btndot.Enabled = !isLockDot;
            misLockDot = isLockDot;
            this.Location = GetPositionInForm(textBox);
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            if (mIsFirstLoad)
            {
                mIsFirstLoad = false;
                mTextBox.Text = "";
            }
            Button btn = (Button)sender;
            mTextBox.Text += btn.Text;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (!IsNegative && !misLockDot)
            {
                if (mTextBox.Text.Length == 0)
                {
                    mTextBox.Text += "0.";
                }
                else
                {
                    if (mTextBox.Text.Contains('.'))
                    {
                        return;
                    }
                    else
                    {
                        mTextBox.Text += ".";
                    }
                }
            }
            else if (IsNegative && misLockDot)
            {
                if (mTextBox.Text.Length == 0)
                {
                    mTextBox.Text += "-";
                }
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            mTextBox.Text = "";
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (mTextBox.Text.Length > 0)
            {
                string text = mTextBox.Text;
                mTextBox.Text = text.Remove(text.Length - 1, 1);
            }
        }

        public Point GetPositionInForm(Control ctrl)
        {
            Point p = new Point();
            p = ctrl.Parent.PointToScreen(ctrl.Location);
            p.X = (p.X + this.Width) < Screen.PrimaryScreen.Bounds.Width ? (p.X) : (p.X - this.Width + ctrl.Width);
            p.Y = (p.Y + ctrl.Height + this.Height) < Screen.PrimaryScreen.Bounds.Height ? (p.Y + ctrl.Height) : (p.Y - this.Height);
            return p;
        }

        private void frmKeyPadNew_Load(object sender, EventArgs e)
        {
            if (IsNegative && misLockDot)
            {
                btndot.Image = null;
                btndot.Text = "-";
                btndot.Enabled = true;
                btndot.Refresh();
            }
        }
    }
}