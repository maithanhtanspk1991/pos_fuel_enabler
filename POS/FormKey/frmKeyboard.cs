﻿using System;
using System.Windows.Forms;

namespace POS.FormKey
{
    public partial class frmKeyboard : Form
    {
        //private bool ktdot;
        private bool capslock;

        private bool IsShift;
        private TextBox mTextBox;

        public frmKeyboard()
        {
            InitializeComponent();
        }

        public frmKeyboard(TextBox txt)
        {
            InitializeComponent();
            mTextBox = txt;
            txtresult.PasswordChar = txt.PasswordChar;
            capslock = false;
            IsShift = true;
            ChangerCaplock(!capslock);
            //IsShift = false;
        }

        private void btnthan_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnthan.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnthan.Text);
        }

        private void btnacong_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnacong.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnacong.Text);
        }

        private void btnthang_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnthang.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnthang.Text);
        }

        private void btndola_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btndola.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btndola.Text);
        }

        private void btnphantram_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnphantram.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{" + btnphantram.Text + "}");
        }

        private void btnbang_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnbang.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnbang.Text);
        }

        private void btnmu_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnmu.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{" + btnmu.Text + "}");
        }

        private void btnsao_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnsao.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnsao.Text);
        }

        private void btntronmo_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btntronmo.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{" + btntronmo.Text + "}");
        }

        private void btntrondong_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btntrondong.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{" + btntrondong.Text + "}");
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            int l = txtresult.Text.Length;
            if (l > 0)
            {
                //txtresult.Text = txtresult.Text.Remove(l - 1);
                if (txtresult == null)
                {
                    return;
                }
                txtresult.Focus();
                SendKeys.Send("{BS}");
                //if (txtresult.Text.Contains("."))
                //{
                //    ktdot = true;
                //}
                //else
                //{
                //    ktdot = false;
                //}
            }
            if (txtresult.Text.Length > 0)
            {
                if (txtresult.Text[txtresult.Text.Length - 1] == ' ')
                {
                    if (!capslock)
                    {
                        IsShift = true;
                        ChangerCaplock(!capslock);
                    }
                }
                else
                {
                    ChangerCaplock(capslock);
                }
            }
            else
            {
                if (!capslock)
                {
                    IsShift = true;
                    ChangerCaplock(!capslock);
                }
            }
        }

        private void btnnhonmo_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnnhonmo.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{LEFT}");
        }

        private void btnnhondong_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnnhondong.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{RIGHT}");
        }

        private void btnhoac_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnhoac.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{END}");
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txtresult.Text = "";
            //ktdot = false;
            if (!capslock)
            {
                IsShift = true;
                ChangerCaplock(!capslock);
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "7";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "8";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "9";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("9");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "6";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("6");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "5";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("5");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "4";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("4");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "3";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("3");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "2";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("2");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "1";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("1");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            //txtresult.Text += "0";
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("0");
        }

        private void btnchia_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnchia.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnchia.Text);
        }

        private void btnchamphay_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnchamphay.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnchamphay.Text);
        }

        private void btnphay_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnphay.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnphay.Text);
        }

        private void btncapslock_Click(object sender, EventArgs e)
        {
            if (capslock == true)
            {
                capslock = false;
                ChangerCaplock(capslock);
            }
            else
            {
                capslock = true;
                ChangerCaplock(capslock);
            }
        }

        private void ChangerCaplock(bool key)
        {
            foreach (Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    char ch = '.';
                    try
                    {
                        ch = Convert.ToChar(control.Tag);
                    }
                    catch (Exception)
                    { }
                    if (ch <= 'z' && ch >= 'a')
                    {
                        if (key)
                        {
                            control.Text = (char)(ch - 'a' + 'A') + "";
                        }
                        else
                        {
                            control.Text = ch + "";
                        }
                    }
                }
            }
        }

        private void btnhaicham_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnhaicham.Text;\
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnhaicham.Text);
        }

        private void btnenter_Click(object sender, EventArgs e)
        {
            try
            {
                mTextBox.Text = txtresult.Text;
                this.DialogResult = DialogResult.OK;
                //btnexit_Click(sender, e);
                this.Close();
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog("frmKeyboard.btnenter_Click:::" + ex.Message);
            }
        }

        private void btnnhohon_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnnhohon.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{HOME}");
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(".");
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnvuongdong_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnvuongdong.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnvuongdong.Text);
        }

        private void btndauhoi_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btndauhoi.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btndauhoi.Text);
        }

        private void btncong_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btncong.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send("{add}");
        }

        private void btnspace_Click(object sender, EventArgs e)
        {
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(" ");
            if (!capslock)
            {
                IsShift = true;
                ChangerCaplock(!capslock);
            }
        }

        private void btntru_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btntru.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btntru.Text);
        }

        private void btnvuongmo_Click(object sender, EventArgs e)
        {
            //txtresult.Text += btnvuongmo.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btnvuongmo.Text);
        }

        private void btnshift_Click(object sender, EventArgs e)
        {
            IsShift = true;
            ChangerCaplock(!capslock);
        }

        private void frmKeyboard_Load(object sender, EventArgs e)
        {
            if (mTextBox != null)
            {
                txtresult.Text = mTextBox.Text;
            }
        }

        private void txtresult_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnKey_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //txtresult.Text += btn.Text;
            if (txtresult == null)
            {
                return;
            }
            txtresult.Focus();
            SendKeys.Send(btn.Text);
            if (IsShift)
            {
                ChangerCaplock(capslock);
                IsShift = false;
            }
        }
    }
}