﻿namespace POS
{
    partial class UCChoseKindOfFuel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPumID = new System.Windows.Forms.Label();
            this.cboKindOfFuel = new System.Windows.Forms.ComboBox();
            this.cboPumpHead = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPumID
            // 
            this.lblPumID.AutoSize = true;
            this.lblPumID.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPumID.ForeColor = System.Drawing.Color.Blue;
            this.lblPumID.Location = new System.Drawing.Point(15, 11);
            this.lblPumID.Name = "lblPumID";
            this.lblPumID.Size = new System.Drawing.Size(29, 31);
            this.lblPumID.TabIndex = 0;
            this.lblPumID.Text = "1";
            // 
            // cboKindOfFuel
            // 
            this.cboKindOfFuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKindOfFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKindOfFuel.ForeColor = System.Drawing.Color.Blue;
            this.cboKindOfFuel.FormattingEnabled = true;
            this.cboKindOfFuel.Location = new System.Drawing.Point(112, 8);
            this.cboKindOfFuel.Name = "cboKindOfFuel";
            this.cboKindOfFuel.Size = new System.Drawing.Size(301, 39);
            this.cboKindOfFuel.TabIndex = 1;
            // 
            // cboPumpHead
            // 
            this.cboPumpHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPumpHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPumpHead.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cboPumpHead.FormattingEnabled = true;
            this.cboPumpHead.Location = new System.Drawing.Point(419, 8);
            this.cboPumpHead.Name = "cboPumpHead";
            this.cboPumpHead.Size = new System.Drawing.Size(159, 39);
            this.cboPumpHead.TabIndex = 2;
            // 
            // UCChoseKindOfFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboPumpHead);
            this.Controls.Add(this.cboKindOfFuel);
            this.Controls.Add(this.lblPumID);
            this.Name = "UCChoseKindOfFuel";
            this.Size = new System.Drawing.Size(588, 54);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblPumID;
        public System.Windows.Forms.ComboBox cboKindOfFuel;
        public System.Windows.Forms.ComboBox cboPumpHead;

    }
}
