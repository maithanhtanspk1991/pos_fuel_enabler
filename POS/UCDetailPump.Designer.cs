﻿namespace POS
{
    partial class UCDetailPump
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.btnFuelInformation = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblVolume = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.BackColor = System.Drawing.Color.White;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblSubtotal.Location = new System.Drawing.Point(60, 26);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(184, 36);
            this.lblSubtotal.TabIndex = 6;
            this.lblSubtotal.Text = "Subtotal";
            this.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSubtotal.Click += new System.EventHandler(this.lblName_Click);
            // 
            // btnFuelInformation
            // 
            this.btnFuelInformation.BackColor = System.Drawing.Color.White;
            this.btnFuelInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuelInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuelInformation.ForeColor = System.Drawing.Color.Navy;
            this.btnFuelInformation.Image = global::POS.Properties.Resources.UnCheck;
            this.btnFuelInformation.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFuelInformation.Location = new System.Drawing.Point(0, 0);
            this.btnFuelInformation.Name = "btnFuelInformation";
            this.btnFuelInformation.Size = new System.Drawing.Size(247, 94);
            this.btnFuelInformation.TabIndex = 5;
            this.btnFuelInformation.UseVisualStyleBackColor = false;
            this.btnFuelInformation.Click += new System.EventHandler(this.lblName_Click);
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Blue;
            this.lblName.Location = new System.Drawing.Point(78, 2);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(166, 33);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // lblVolume
            // 
            this.lblVolume.BackColor = System.Drawing.Color.White;
            this.lblVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolume.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblVolume.Location = new System.Drawing.Point(65, 55);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(179, 34);
            this.lblVolume.TabIndex = 6;
            this.lblVolume.Text = "Volume";
            this.lblVolume.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblVolume.Click += new System.EventHandler(this.lblName_Click);
            // 
            // UCDetailPump
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblVolume);
            this.Controls.Add(this.lblSubtotal);
            this.Controls.Add(this.btnFuelInformation);
            this.Name = "UCDetailPump";
            this.Size = new System.Drawing.Size(247, 94);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblSubtotal;
        public System.Windows.Forms.Button btnFuelInformation;
        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.Label lblVolume;

    }
}
