﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCKeyboard : UserControl
    {
        private bool ktdot;
        private bool capslock;

        public UCKeyboard()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtresult.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtresult.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtresult.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtresult.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtresult.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtresult.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtresult.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtresult.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtresult.Text += "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtresult.Text += "0";
        }

        private void btnq_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "q";
            else
                txtresult.Text += "Q";
        }

        private void btnw_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "w";
            else
                txtresult.Text += "W";
        }

        private void btne_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "e";
            else
                txtresult.Text += "E";
        }

        private void btnr_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "r";
            else
                txtresult.Text += "R";
        }

        private void btnt_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "t";
            else
                txtresult.Text += "T";
        }

        private void btny_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "y";
            else
                txtresult.Text += "Y";
        }

        private void btnu_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "u";
            else
                txtresult.Text += "U";
        }

        private void btni_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "i";
            else
                txtresult.Text += "I";
        }

        private void btno_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "o";
            else
                txtresult.Text += "O";
        }

        private void btnp_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "p";
            else
                txtresult.Text += "P";
        }

        private void btna_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "a";
            else
                txtresult.Text += "A";
        }

        private void btns_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "s";
            else
                txtresult.Text += "S";
        }

        private void btnd_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "d";
            else
                txtresult.Text += "D";
        }

        private void btnf_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "f";
            else
                txtresult.Text += "F";
        }

        private void btng_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "g";
            else
                txtresult.Text += "G";
        }

        private void btnh_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "h";
            else
                txtresult.Text += "H";
        }

        private void btnj_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "j";
            else
                txtresult.Text += "J";
        }

        private void btnk_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "k";
            else
                txtresult.Text += "K";
        }

        private void btnl_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "l";
            else
                txtresult.Text += "L";
        }

        private void btnz_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "z";
            else
                txtresult.Text += "Z";
        }

        private void btnx_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "x";
            else
                txtresult.Text += "X";
        }

        private void btnc_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "c";
            else
                txtresult.Text += "C";
        }

        private void btnv_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "v";
            else
                txtresult.Text += "V";
        }

        private void btnb_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "b";
            else
                txtresult.Text += "B";
        }

        private void btnn_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "n";
            else
                txtresult.Text += "N";
        }

        private void btnm_Click(object sender, EventArgs e)
        {
            if (!capslock)
                txtresult.Text += "m";
            else
                txtresult.Text += "M";
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            int l = txtresult.Text.Length;
            if (l > 0)
            {
                txtresult.Text = txtresult.Text.Remove(l - 1);
                if (txtresult.Text.Contains("."))
                {
                    ktdot = true;
                }
                else
                {
                    ktdot = false;
                }
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
        }

        private void btnenter_Click(object sender, EventArgs e)
        {
        }

        private void btnspace_Click(object sender, EventArgs e)
        {
            txtresult.Text += " ";
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (txtresult.Text.Length == 0)
                txtresult.Text += "0.";
            else if (ktdot == false)
                txtresult.Text += ".";
            ktdot = true;
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txtresult.Text = "";
            ktdot = false;
        }

        private void btnshift_Click(object sender, EventArgs e)
        {
        }

        private void btncapslock_Click(object sender, EventArgs e)
        {
            if (capslock == true)
                capslock = false;
            else
                capslock = true;
        }

        private void btnthan_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnthan.Text;
        }

        private void btnacong_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnacong.Text;
        }

        private void btnthang_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnthang.Text;
        }

        private void btndola_Click(object sender, EventArgs e)
        {
            txtresult.Text += btndola.Text;
        }

        private void btnphantram_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnphantram.Text;
        }

        private void btnbang_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnbang.Text;
        }

        private void btnmu_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnmu.Text;
        }

        private void btnsao_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnsao.Text;
        }

        private void btntronmo_Click(object sender, EventArgs e)
        {
            txtresult.Text += btntronmo.Text;
        }

        private void btntrondong_Click(object sender, EventArgs e)
        {
            txtresult.Text += btntrondong.Text;
        }

        private void btnnhonmo_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnnhonmo.Text;
        }

        private void btnnhondong_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnnhondong.Text;
        }

        private void btnhoac_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnhoac.Text;
        }

        private void btnchia_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnchia.Text;
        }

        private void btnchamphay_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnchamphay.Text;
        }

        private void btnhaicham_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnhaicham.Text;
        }

        private void btnphay_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnphay.Text;
        }

        private void btnvuongmo_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnvuongmo.Text;
        }

        private void btntru_Click(object sender, EventArgs e)
        {
            txtresult.Text += btntru.Text;
        }

        private void btncong_Click(object sender, EventArgs e)
        {
            txtresult.Text += btncong.Text;
        }

        private void btndauhoi_Click(object sender, EventArgs e)
        {
            txtresult.Text += btndauhoi.Text;
        }

        private void btnvuongdong_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnvuongdong.Text;
        }

        private void btnnhohon_Click(object sender, EventArgs e)
        {
            txtresult.Text += btnnhohon.Text;
        }
    }
}