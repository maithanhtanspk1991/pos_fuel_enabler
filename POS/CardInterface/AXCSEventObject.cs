﻿using System;

namespace POS.CardInterface
{
    public class AXCSEventObject
    {
        public AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEventHandler PrintReceiptEvent { get; set; }

        public EventHandler TransactionEvent { get; set; }

        public AXCSEventObject(AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEventHandler lPrintReceiptEvent, EventHandler lTransactionEvent)
        {
            PrintReceiptEvent = lPrintReceiptEvent;
            TransactionEvent = lTransactionEvent;
        }
    }
}