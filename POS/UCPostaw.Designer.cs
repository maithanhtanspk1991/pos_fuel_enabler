﻿namespace POS
{
    partial class UCPostaw
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.lblOrderID = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSubTotal.Location = new System.Drawing.Point(0, 71);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(135, 13);
            this.lblSubTotal.TabIndex = 1;
            this.lblSubTotal.Text = "label1";
            this.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSubTotal.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblOrderID
            // 
            this.lblOrderID.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblOrderID.Location = new System.Drawing.Point(0, 58);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(135, 13);
            this.lblOrderID.TabIndex = 2;
            this.lblOrderID.Text = "label2";
            this.lblOrderID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderID.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "A";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // UCPostaw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblOrderID);
            this.Controls.Add(this.lblSubTotal);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "UCPostaw";
            this.Size = new System.Drawing.Size(135, 84);
            this.BackColorChanged += new System.EventHandler(this.UCPostaw_BackColorChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblSubTotal;
        public System.Windows.Forms.Label lblOrderID;
        public System.Windows.Forms.Label label3;


    }
}
