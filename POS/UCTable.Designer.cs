﻿namespace POS
{
    partial class UCTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbpeople = new System.Windows.Forms.Label();
            this.lbtime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbsubtotal = new System.Windows.Forms.Label();
            this.lbstaff = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbpeople
            // 
            this.lbpeople.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lbpeople.AutoSize = true;
            this.lbpeople.BackColor = System.Drawing.Color.Transparent;
            this.lbpeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbpeople.Location = new System.Drawing.Point(-2, 36);
            this.lbpeople.Name = "lbpeople";
            this.lbpeople.Size = new System.Drawing.Size(36, 16);
            this.lbpeople.TabIndex = 1;
            this.lbpeople.Text = "0000";
            this.lbpeople.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbpeople.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbtime
            // 
            this.lbtime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbtime.AutoSize = true;
            this.lbtime.BackColor = System.Drawing.Color.Transparent;
            this.lbtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtime.Location = new System.Drawing.Point(86, 36);
            this.lbtime.Name = "lbtime";
            this.lbtime.Size = new System.Drawing.Size(63, 16);
            this.lbtime.TabIndex = 2;
            this.lbtime.Text = "000:00:00";
            this.lbtime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbtime.Click += new System.EventHandler(this.label3_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 37);
            this.label3.TabIndex = 3;
            this.label3.Text = "table";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbsubtotal
            // 
            this.lbsubtotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lbsubtotal.AutoSize = true;
            this.lbsubtotal.BackColor = System.Drawing.Color.Transparent;
            this.lbsubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbsubtotal.ForeColor = System.Drawing.Color.Red;
            this.lbsubtotal.Location = new System.Drawing.Point(-2, 62);
            this.lbsubtotal.Name = "lbsubtotal";
            this.lbsubtotal.Size = new System.Drawing.Size(76, 16);
            this.lbsubtotal.TabIndex = 4;
            this.lbsubtotal.Text = "0000.0000";
            this.lbsubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbsubtotal.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbstaff
            // 
            this.lbstaff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbstaff.AutoSize = true;
            this.lbstaff.BackColor = System.Drawing.Color.Transparent;
            this.lbstaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbstaff.Location = new System.Drawing.Point(120, 62);
            this.lbstaff.Name = "lbstaff";
            this.lbstaff.Size = new System.Drawing.Size(29, 16);
            this.lbstaff.TabIndex = 5;
            this.lbstaff.Text = "100";
            this.lbstaff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbstaff.Click += new System.EventHandler(this.label3_Click);
            // 
            // UCTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbstaff);
            this.Controls.Add(this.lbsubtotal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbtime);
            this.Controls.Add(this.lbpeople);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UCTable";
            this.Size = new System.Drawing.Size(145, 124);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbpeople;
        public System.Windows.Forms.Label lbtime;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lbsubtotal;
        public System.Windows.Forms.Label lbstaff;

    }
}
