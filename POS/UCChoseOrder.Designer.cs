﻿namespace POS
{
    partial class UCChoseOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrder = new System.Windows.Forms.Button();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblOrderID = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOrder
            // 
            this.btnOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOrder.Location = new System.Drawing.Point(0, 0);
            this.btnOrder.Name = "btnOrder";
            this.btnOrder.Size = new System.Drawing.Size(86, 73);
            this.btnOrder.TabIndex = 2;
            this.btnOrder.UseVisualStyleBackColor = true;
            this.btnOrder.Click += new System.EventHandler(this.btnOrder_Click_1);
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(0, 49);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(87, 24);
            this.lblSubtotal.TabIndex = 3;
            this.lblSubtotal.Text = "label1";
            this.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSubtotal.Click += new System.EventHandler(this.btnOrder_Click_1);
            // 
            // lblCustomer
            // 
            this.lblCustomer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(0, 16);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(86, 33);
            this.lblCustomer.TabIndex = 4;
            this.lblCustomer.Text = "label1";
            this.lblCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCustomer.Click += new System.EventHandler(this.btnOrder_Click_1);
            // 
            // lblOrderID
            // 
            this.lblOrderID.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblOrderID.Location = new System.Drawing.Point(0, 0);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(86, 24);
            this.lblOrderID.TabIndex = 5;
            this.lblOrderID.Text = "label1";
            this.lblOrderID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderID.Click += new System.EventHandler(this.btnOrder_Click_1);
            // 
            // UCChoseOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblOrderID);
            this.Controls.Add(this.lblCustomer);
            this.Controls.Add(this.lblSubtotal);
            this.Controls.Add(this.btnOrder);
            this.Name = "UCChoseOrder";
            this.Size = new System.Drawing.Size(86, 73);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnOrder;
        public System.Windows.Forms.Label lblSubtotal;
        public System.Windows.Forms.Label lblCustomer;
        public System.Windows.Forms.Label lblOrderID;


    }
}
