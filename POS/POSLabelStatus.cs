﻿namespace POS
{
    internal class POSLabelStatus : System.Windows.Forms.Label
    {
        public int POSTimeMessenge { get; set; }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (value != "")
                {
                    POSTimeMessenge = 1;
                }
                base.Text = value;
            }
        }
    }
}