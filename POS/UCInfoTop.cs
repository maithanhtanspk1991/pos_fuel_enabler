﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCInfoTop : UserControl
    {
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();

        public UCInfoTop()
        {
            InitializeComponent();
            lbServerMode.Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }

        private void UCInfoTop_Load(object sender, EventArgs e)
        {
            lbCableID.Text = "Cable ID: " + dbconfig.CableID.ToString();
            lbVersion.Text = "becasPOS v" + Application.ProductVersion;
            this.Tag = dbconfig.CableID.ToString();
        }
    }
}