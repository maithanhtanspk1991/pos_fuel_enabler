﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS
{
    public partial class UCReportShiftViewer : UserControl
    {
        private Connection.Connection mconnect = new Connection.Connection();        
        private string ShiftID = "";
        DataTable dtSource;
        private Class.MoneyFortmat money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

        public UCReportShiftViewer()
        {
            InitializeComponent();
        }

        public UCReportShiftViewer(string shiftid)
        {
            InitializeComponent();
            ShiftID = shiftid;
            LoadShiftReport(ShiftID);
        }       

        private void LoadShiftReport(string Shf)
        {            
            try
            {
                lbshiftid.Text = Shf;
                mconnect.Open();
                string sql = "select " +
                                "sum(subTotal) as totalSales," +
                                "sum(discount) as totalDisCount," +
                                "sum(cash) as cash," +
                                "sum(eftpos) as eftpos," +
                                "sum(returnPro) as returnPro," +
                                "sum(creditNote) as creditNote, " +
                                "sum(gst) as gst, " +
                                "sum(account) as account, " +
                                "sum(`change`) as `change` " +
                             "from (" +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersdaily " +
                                        "where shiftID='" + Shf + "' and (completed=1 or completed=2) " +
                                    "union all " +
                                    "select subTotal,discount,cash,eftpos,refun AS returnPro,creditNote,gst,account,`change` " +
                                        "from ordersall " +
                                        "where shiftID='" + Shf + "' and (completed=1 or completed=2) " +
                                     ") as total";
                DataTable tbl = new DataTable();
                tbl = mconnect.Select(sql);
                double sales = Convert.ToDouble(tbl.Rows[0]["totalSales"].ToString() == "" ? 0 : tbl.Rows[0]["totalSales"]);
                double discount = Convert.ToDouble(tbl.Rows[0]["totalDisCount"].ToString() == "" ? 0 : tbl.Rows[0]["totalDisCount"]);
                double refunc = Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]);
                double creditNote = Convert.ToDouble(tbl.Rows[0]["creditNote"].ToString() == "" ? 0 : tbl.Rows[0]["creditNote"]);
                double gst = Convert.ToDouble(tbl.Rows[0]["gst"].ToString() == "" ? 0 : tbl.Rows[0]["gst"]);
                double change = Convert.ToDouble(tbl.Rows[0]["change"].ToString() == "" ? 0 : tbl.Rows[0]["change"]);
                double account = Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]);
                labelTotalDiscount.Text = money.Format2(discount);
                labelTotalSale.Text = money.Format2(sales);
                labelTotalGST.Text = money.Format2(gst);
                labelCash.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["cash"].ToString() == "" ? 0 : tbl.Rows[0]["cash"]));
                labelCard.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["eftpos"].ToString() == "" ? 0 : tbl.Rows[0]["eftpos"]));
                labelRefunc.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["returnPro"].ToString() == "" ? 0 : tbl.Rows[0]["returnPro"]));
                labelCreditNote.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["creditNote"].ToString() == "" ? 0 : tbl.Rows[0]["creditNote"]));
                labelAccount.Text = money.Format2(Convert.ToDouble(tbl.Rows[0]["account"].ToString() == "" ? 0 : tbl.Rows[0]["account"]));
                double decTotal;
                sql = "SELECT CardName,SUM(Subtotal) AS Subtotal " +
                    "FROM " +
                    "( " +
                    "SELECT c.cardName AS CardName,SUM(obc.subtotal) AS Subtotal " +
                    "FROM orderbycard obc " +
                    "inner join ordersdaily od on od.orderbycardID=obc.orderbycardID " +
                    "inner join card c on obc.cardID = c.cardID " +
                    "where od.shiftID='" + Shf + "' and (od.completed=1 or od.completed=2) group by CardName " +
                    "Union all " +
                    "SELECT c.cardName,SUM(obc.subtotal) AS Subtotal " +
                    "FROM orderbycard obc " +
                    "inner join ordersall oa on oa.orderbycardID=obc.orderbycardID " +
                    "inner join card c on obc.cardID = c.cardID " +
                    "where oa.shiftID='" + Shf + "' and (oa.completed=1 or oa.completed=2) group by CardName " +
                    ")AS Source group by CardName";
                lstCard.Items.Clear();
                dtSource = new DataTable();
                dtSource = mconnect.Select(sql);
                decTotal = 0;
                foreach (System.Data.DataRow row in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(row["CardName"].ToString());
                    li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
                    decTotal += Convert.ToDouble(row["Subtotal"]);
                    lstCard.Items.Add(li);
                }
                lblTotalCard.Text = "Total card amount :" + money.Format2(decTotal);

                double cashAccount = 0;
                double cardAccount = 0;
                dtSource = mconnect.Select(
                    "select if(cash is null,0,cash) as cash,if(card is null,0,card) as card from (select sum(cash) as cash,sum(card) as card from accountpayment where shiftID='" + Shf + "' ) as tmp"
                    );
                if (dtSource.Rows.Count > 0)
                {
                    cashAccount = Convert.ToDouble(dtSource.Rows[0]["cash"]);
                    cardAccount = Convert.ToDouble(dtSource.Rows[0]["card"]);
                }
                labelCardAccount.Text = money.Format(cardAccount);
                labelCashAccount.Text = money.Format(cashAccount);
                labelTotalAccount.Text = money.Format(cardAccount + cashAccount);
                DataTable dti = new DataTable();
                string strsql = "select ts,tsEnd from shifts where shiftID=" + Shf;
                dti = mconnect.Select(strsql);
                if (dti.Rows.Count > 0)
                {
                    lbfrom.Text = SplitDate(Convert.ToDateTime(dti.Rows[0]["ts"].ToString()));
                    lbto.Text = SplitDate(Convert.ToDateTime(dti.Rows[0]["tsEnd"].ToString()));
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadShiftReport:::" + ex.Message);
            }
            finally
            {
                mconnect.Close();
            }
        }

        private string SplitDate(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[1].ToString() + "-" + listdate[0].ToString() + "-" + listdate[2].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
        }        
    }
}
