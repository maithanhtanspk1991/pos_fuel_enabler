﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class TypeMenu
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public bool IsSize { get; set; }

        public TypeMenu()
        {
            Name = "";
            ID = 0;
            Deleted = false;
            IsSize = false;
        }
    }

    class DATypeMenu
    {
        private static string _nameClass = "POS::Class::DATypeMenu::";
        private static Connection.Connection conn = new Connection.Connection();

        private static TypeMenu SetValue(System.Data.DataRow row)
        {
            TypeMenu item = new TypeMenu();
            try
            {
                item.ID = row["ID"].ToString() != "" ? Convert.ToInt32(row["ID"]) : 0;
                item.Name = row["Name"].ToString();
                item.Deleted = row["Deleted"].ToString() != "" ? Convert.ToBoolean(row["Deleted"]) : false;
                item.IsSize = row["IsSize"].ToString() != "" ? Convert.ToBoolean(row["IsSize"]) : false;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "SetValue::" + ex.Message);
            }
            return item;
        }
        public static List<TypeMenu> GetAll()
        {
            List<TypeMenu> lsArray = new List<TypeMenu>();
            try
            {
                conn.Open();
                string sql = "SELECT * FROM TypeMenu i Where Deleted = 0;";
                System.Data.DataTable dt = conn.Select(sql);
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    lsArray.Add(SetValue(row));
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "GetAll::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return lsArray;
        }
    }

    public class BOTypeMenu
    {
        public static List<TypeMenu> GetAll()
        {
            return DATypeMenu.GetAll();
        }
    }
}
