﻿using System;

namespace POS.Class
{
    internal class FuelDiscountConfig
    {
        public static double GetSpend()
        {
            string resuilt = "";
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                resuilt = ini.ReadValue("fuel", "Spend");
                try
                {
                    return Convert.ToInt32(resuilt);
                }
                catch (Exception)
                {
                    ini.WriteValue("fuel", "Spend", "0");
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public static void SetSpend(string data)
        {
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                ini.WriteValue("fuel", "Spend", data);
            }
            catch (Exception)
            {
            }
        }

        public static void SetDiscount(string data)
        {
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);
                ini.WriteValue("fuel", "Discount", data);
            }
            catch (Exception)
            {
            }
        }

        public static double GetDiscount()
        {
            string resuilt = "";
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                resuilt = ini.ReadValue("fuel", "Discount");
                try
                {
                    return Convert.ToInt32(resuilt);
                }
                catch (Exception)
                {
                    ini.WriteValue("fuel", "Discount", "0");
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public static double GetMaxQtyDiscount()
        {
            string resuilt = "";
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                resuilt = ini.ReadValue("fuel", "MaxQtyDiscount");
                try
                {
                    return Convert.ToDouble(resuilt);
                }
                catch (Exception)
                {
                    ini.WriteValue("fuel", "MaxQtyDiscount", "0");
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public static int[] GetListFuelDiscount()
        {
            int[] resuilt = null;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);
                try
                {
                    string[] data = ini.ReadValue("fuel", "DiscountPerList").Split(',');
                    resuilt = new int[data.Length];
                    for (int i = 0; i < data.Length; i++)
                    {
                        resuilt[i] = Convert.ToInt32(data[i]);
                    }
                }
                catch (Exception)
                {
                    resuilt = null;
                    ini.WriteValue("fuel", "DiscountPerList", "");
                }
            }
            catch (Exception)
            {
            }
            return resuilt;
        }
    }
}