﻿namespace POS.Class
{
    internal class LoyaltyItem
    {
        public string itemname { get; set; }

        public int qty { get; set; }

        public int unitprice { get; set; }

        public LoyaltyItem(string name, int Qty, int price)
        {
            itemname = name;
            qty = Qty;
            unitprice = price;
        }
    }
}