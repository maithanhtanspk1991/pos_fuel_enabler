﻿namespace POS.Class
{
    public class AutoStartup
    {
        public static bool GetProcess(string processName)
        {
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                if (pr.ProcessName == processName)
                {
                    return true;
                }
            }
            return false;
        }

        public static void KillProcess(string processName)
        {
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                if (pr.ProcessName == processName)
                    pr.Kill();
            }
        }

        public static bool Restart(string processName, string fileName)
        {
            KillProcess(processName);
            StartProcess(fileName);
            return GetProcess(processName);
        }

        public static bool Resume(string processName, string fileName)
        {
            if (!GetProcess(processName))
            {
                StartProcess(fileName);
            }
            return GetProcess(processName);
        }

        public static void StartProcess(string fileName)
        {
            if (System.IO.File.Exists(fileName))
                System.Diagnostics.Process.Start(fileName);
        }
    }
}