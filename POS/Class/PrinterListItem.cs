﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class PrinterListItem
    {
        public static List<Class.ItemOrderK> GetListItemPrinter(int orderID)
        {
            Connection.Connection con = new Connection.Connection();
            List<Class.ItemOrderK> lsArray = new List<ItemOrderK>(); ;
            try
            {
                con.Open();
                string sql = "";
                if (orderID != 0)
                {
                    sql =
                        "select " +
                            "ol.weight," +
                            "o.clients," +
                            "ol.subTotal," +
                            "ol.gst," +
                            "o.discount," +
                            "o.orderID," +
                            "o.IsDelivery," +
                            "o.IsPickup," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "1 AS Visual," +
                            "ol.price," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "if(itemID<>0,(select gst from itemsmenu i where i.itemID=ol.itemID),0) as gst," +
                            "ol.qty," +
                            "ol.pumpID " +
                        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "where o.orderID=" + orderID;
                }
                System.Data.DataTable tbl = con.Select(sql);
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        lsArray.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 1, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            lsArray.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                        }
                        else
                        {
                            if (lsArray.Count == 0 || lsArray[lsArray.Count - 1].GRShortCut == 1)
                            {
                                lsArray.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["price"].ToString() == "" ? "0" : row["price"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, Convert.ToInt16(row["weight"]), Convert.ToInt16(row["gst"]), Convert.ToInt16(row["pumpID"]), Convert.ToInt16(row["Visual"]), row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                            }
                            else
                            {
                                lsArray[lsArray.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();
            }
            return lsArray;
        }
    }
}
