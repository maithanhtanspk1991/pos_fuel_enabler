﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Data;
using System.Windows.Forms;

namespace POS.Class
{
    class SendMail
    {
        public void CreateMessageThread()
        {
            System.Threading.Thread thread = new System.Threading.Thread(CreateMessage);
            thread.Start();
            //CreateMessage();
        }
        private void CreateMessage()
        {
            try
            {
                DateTime now = DateTime.Now;
                string dateSend = now.Year + "-" + now.Month + "-" + now.Day;
                string dateTo = now.Day + @"/" + now.Month + @"/" + now.Year;
                DataTable dt = getListCustomers();
                string mailfrom = getInfoMail("Mail");
                string mailfrompass = getInfoMail("Pass");
                string display = getInfoMail("Display");
                string subject = getInfoMail("Subject");
                string smtp = getInfoMail("SmtpClient");
                string port = getInfoMail("Port");
                string timeout = getInfoMail("Timeout");

                SmtpClient client = new SmtpClient(smtp);
                client.Port = Convert.ToInt32(port);
                client.EnableSsl = true;
                client.Timeout = Convert.ToInt32(timeout) * 1000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(mailfrom, mailfrompass);

                //DateTime now = DateTime.Now;
                //string dateSend = now.Year + "-" + now.Month + "-" + now.Day;
                //DataTable dt = getListCustomers();
                foreach (DataRow row in dt.Rows)
                {
                    if (row["checksendmail"].ToString() == "1")
                    {
                        try
                        {
                            string paymentdue = "";
                            string datefrom = row["tsSendMail"].ToString() + " 00:00:00";
                            string dateto = dateSend + " 23:59:59";
                            string accountstatement = getaccountstatement(datefrom, dateto, row["memberNo"].ToString(), paymentdue);
                            if (accountstatement == "")
                                continue;
                            MailMessage mMsg = new MailMessage();
                            MailAddress from = new MailAddress(mailfrom, display);
                            mMsg.From = from;
                            mMsg.IsBodyHtml = true;
                            mMsg.Subject = subject;
                            string content = "<h2>Your Account Statement from Bizzell’s Garage</h2>";
                            mMsg.To.Add(row["email"].ToString());
                            content += "<p>Dear " + row["FirstName"].ToString() + ",</p>";
                            content += "<p>Just letting you know your account statement of this month " +
                                        "(from " + row["tsSendMail"].ToString() + " to " + dateSend + ") " +
                                        "is generated successfully. </br>Please see the following information for your detail purchase history at Bizzell’s Garage." + ",</p>";
                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\">" +
                                        "<table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">Account details</td>" +
                                            "<td></td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Name:</td>" +
                                            "<td>" + row["FirstName"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Street No:</td>" +
                                            "<td>" + row["streetNo"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Street Name:</td>" +
                                            "<td>" + row["streetName"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Phone:</td>" +
                                            "<td>" + row["phone"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Mobile:</td>" +
                                            "<td>" + row["mobile"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Email:</td>" +
                                            "<td>" + row["email"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Customer ID:</td>" +
                                            "<td>" + row["custID"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Member No:</td>" +
                                            "<td>" + row["memberNo"].ToString() + "</td>" +
                                        "</tr>";
                            content += "</table></div>";
                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">For the period</td>" +
                                            "<td></td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td>From: " + row["tsSendMail"].ToString() + "</td>" +
                                            "<td>To: " + dateSend + "</td>" +
                                        "</tr>";
                            content += "</table></div>";
                            ////////////////////////////
                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">Payment due ($):</td>" +
                                            "<td>" + paymentdue + "</td>" +
                                        "</tr>";
                            content += "</table></div>";
                            /////////////////////////////
                            content += accountstatement;
                            content += "<p>To make payment for your account, you can use fund transfer via internet banking or in person at our office.</p>" +
                                       "<p>Thank you for using Bizzell’s Garage services.</p>";
                            mMsg.Body = content;
                            //Attach file
                            //string pathFile = @"C:\bgMain.jpg";
                            //Attachment data = new Attachment(pathFile);
                            //mMsg.Attachments.Add(data);
                            client.Send(mMsg);
                            updateCust(dateSend, row["custID"].ToString());
                        }
                        catch (Exception ex)
                        {
                            Class.LogPOS.WriteLog("POS.Class.SendMail::CreateMessage::Error Send Mail::custID::" + row["custID"] + "::Error" + ex.Message);
                        }
                    }
                }
                ////////////////////////////////
                DataTable dtCompany = getListCompany();
                foreach (DataRow row in dtCompany.Rows)
                {
                    if (row["checksendmail"].ToString() == "1")
                    {
                        try
                        {
                            string paymentdue = "";
                            string datefrom = row["tsSendMail"].ToString() + " 00:00:00";
                            string dateto = dateSend + " 23:59:59";
                            string companystatement = getcompanystatement(datefrom, dateto, row["idCompany"].ToString(), paymentdue);
                            if (companystatement == "")
                                continue;
                            MailMessage mMsg = new MailMessage();
                            MailAddress from = new MailAddress(mailfrom, display);
                            mMsg.From = from;
                            mMsg.IsBodyHtml = true;
                            mMsg.Subject = subject;

                            string content = "<h2>Your Account Statement from Bizzell’s Garage</h2>";

                            mMsg.To.Add(row["email"].ToString());
                            content += "<p>Dear " + row["companyName"].ToString() + ",</p>";
                            content += "<p>Just letting you know your account statement of this month " +
                                        "(from " + row["tsSendMail"].ToString() + " to " + dateSend + ") " +
                                        "is generated successfully. </br>Please see the following information for your detail purchase history at Bizzell’s Garage." + ",</p>";
                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\">" +
                                        "<table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">Company details</td>" +
                                            "<td></td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Name:</td>" +
                                            "<td>" + row["companyName"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Street No:</td>" +
                                            "<td>" + row["streetNo"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Street Name:</td>" +
                                            "<td>" + row["streetName"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Email:</td>" +
                                            "<td>" + row["email"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td>Phone:</td>" +
                                            "<td>" + row["phone"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Mobile:</td>" +
                                            "<td>" + row["mobile"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Company Code:</td>" +
                                            "<td>" + row["companyCode"].ToString() + "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td style=\"font-weight: bold;\">Company ID:</td>" +
                                            "<td>" + row["idCompany"].ToString() + "</td>" +
                                        "</tr>";
                            content += "</table></div>";

                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">For the period</td>" +
                                            "<td></td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td>From: " + row["tsSendMail"].ToString() + "</td>" +
                                            "<td>To: " + dateSend + "</td>" +
                                        "</tr>";
                            content += "</table></div>";

                            content += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                            content += "<tr>" +
                                            "<td style=\"color:red;font-weight: bold;\">Payment due:</td>" +
                                            "<td>$70</td>" +
                                        "</tr>";
                            content += "</table></div>";
                            /////////////////////////////
                            content += companystatement;
                            content += "<p>To make payment for your account, you can use fund transfer via internet banking or in person at our office.</p>" +
                                       "<p>Thank you for using Bizzell’s Garage services.</p>";
                            mMsg.Body = content;
                            //Attach file
                            //string pathFile = @"C:\bgMain.jpg";
                            //Attachment data = new Attachment(pathFile);
                            //mMsg.Attachments.Add(data);
                            client.Send(mMsg);
                            updateCompany(dateSend, row["idCompany"].ToString());
                        }
                        catch (Exception ex)
                        {
                            Class.LogPOS.WriteLog("POS.Class.SendMail::CreateMessage::Error::" + ex.Message);
                        }
                    }
                }
                //MessageBox.Show("OK");
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::CreateMessage::Error::" + ex.Message);
                //MessageBox.Show("Error::" + ex.Message);
            }
        }
        public string getcompanystatement(string from, string to, string memberNo, string paymentdue)
        {
            string result = "";
            DataTable dt = new DataTable();
            string balance = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "CALL SP_ACCOUNT_STATEMENT_COMPANY('" + from + "','" + to + "','" + memberNo + "')";
                dt = conn.Select(sql);
                if (dt.Rows.Count == 0)
                    return "";
                //result += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                result += "<table border=\"1px solid black\" style=\"width: 100%;border-spacing: 0;text-align: center;\">";
                result += "<tr>" +
                                "<th>No</th>" +
                                "<th>Date</th>" +
                                "<th>Time</th>" +
                                "<th>Transaction ID</th>" +
                                "<th>Account</th>" +
                                "<th>List of Items ordered</th>" +
                                "<th>Sub Total ($)</th>" +
                                "<th>Credit ($)</th>" +
                                "<th>Debit ($)</th>" +
                                "<th>Balance ($)</th>" +
                            "</tr>";

                int i = 1;
                //OrderID, Date, Time, PayAmount, SubTotal, Debt, balance, TypePayment, Description
                Class.MoneyFortmat money = new MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
                foreach (DataRow row in dt.Rows)
                {
                    if (row["SubTotal"].ToString() == "0" && row["PayAmount"].ToString() == "0" && row["balance"].ToString() == "0")
                    {
                        continue;
                    }
                    string openBalance = (Convert.ToInt32(row["balance"].ToString()) + Convert.ToInt32(row["PayAmount"].ToString())).ToString();
                    if (i == 1)
                    {
                        if (row["OrderID"].ToString() == "0")
                        {
                            result += "<tr><td colspan='10' align=\"center\" style=\"background-color: #6F9ECE;\">Opening Balance ($) <span style=\"float:right;margin-right: 8px;\">" + money.Format2(Convert.ToInt32(row["balance"].ToString()) + Convert.ToInt32(row["PayAmount"].ToString())) + "</span></td></tr>";
                        }
                        else
                        {
                            result += "<tr><td colspan='10' align=\"center\" style=\"background-color: #6F9ECE;\">Opening Balance ($) <span style=\"float:right;margin-right: 8px;\">" + money.Format2(Convert.ToInt32(row["balance"].ToString()) - Convert.ToInt32(row["SubTotal"].ToString())) + "</span></td></tr>";
                        }
                    }

                    string orderID = row["OrderID"].ToString();
                    string desc = row["Description"].ToString();
                    string subtotal = money.Format2(row["SubTotal"].ToString());
                    string payamount = money.Format2(row["PayAmount"].ToString());
                    balance = money.Format2(row["balance"].ToString());
                    string debit = subtotal;
                    if (row["OrderID"].ToString() == "0")
                    {
                        if (row["TypePayment"].ToString() == "5" && row["SubTotal"].ToString() != "0")
                            debit = subtotal;
                        else
                            debit = "";
                        orderID = "";
                        subtotal = "";
                    }
                    else
                    {
                        desc = getlistitem(row["OrderID"].ToString(), row["Date"].ToString());
                        if (row["SubTotal"].ToString() == "0")
                        {
                            subtotal = "";
                        }
                    }
                    if (row["PayAmount"].ToString() == "0")
                    {
                        payamount = "";
                    }
                    result += "<tr>" +
                                    "<td>" + i.ToString() + "</td>" +
                                    "<td>" + row["Date"].ToString() + "</td>" +
                                    "<td>" + row["Time"].ToString() + "</td>" +
                                    "<td>" + orderID + "</td>" +
                                    "<td>" + row["FirstName"].ToString() + "</td>" +
                                    "<td>" + desc + "</td>" +
                                    "<td>" + subtotal + "</td>" +
                                    "<td>" + payamount + "</td>" +
                                    "<td>" + debit + "</td>" +
                                    "<td>" + balance + "</td>" +
                              "</tr>";
                    paymentdue = balance;
                    i++;
                }
                result += "<tr><td colspan='10' align=\"center\" style=\"background-color: #6F9ECE;\">Closing Balance ($) <span style=\"float:right;margin-right: 8px;\">" + paymentdue + "</span></td></tr>";
                result += "</table>";
                //result += "</table></div>";

            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getaccountstatement::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public string getaccountstatement(string from, string to, string memberNo, string paymentdue)
        {
            string result = "";
            DataTable dt = new DataTable();
            string balance = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "CALL SP_ACCOUNT_STATEMENT_NO_COMPANY('" + from + "','" + to + "','" + memberNo + "')";
                dt = conn.Select(sql);
                if (dt.Rows.Count == 0)
                    return "";
                //result += "<div style=\"border: 1px solid #272A33;border-radius: 10px;padding: 10px;margin-bottom: 20px;\"><table style=\"width: 100%;\">";
                result += "<table border=\"1px solid black\" style=\"width: 100%;border-spacing: 0;text-align: center;\">";
                result += "<tr>" +
                                "<th>No</th>" +
                                "<th>Date</th>" +
                                "<th>Time</th>" +
                                "<th>Transaction ID</th>" +
                                "<th>List of Items ordered</th>" +
                                "<th>Sub Total ($)</th>" +
                                "<th>Credit ($)</th>" +
                                "<th>Debit ($)</th>" +
                                "<th>Balance ($)</th>" +
                            "</tr>" +
                            "<tr><td colspan='9' align=\"center\" style=\"background-color: #6F9ECE;\">Opening Balance</td></tr>";
                int i = 1;
                //OrderID, Date, Time, PayAmount, SubTotal, Debt, balance, TypePayment, Description
                Class.MoneyFortmat money = new MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
                foreach (DataRow row in dt.Rows)
                {
                    string orderID = row["OrderID"].ToString();
                    string desc = row["Description"].ToString();
                    string subtotal = money.Format2(row["SubTotal"].ToString());
                    string payamount = money.Format2(row["PayAmount"].ToString());
                    balance = money.Format2(row["balance"].ToString());
                    string debit = subtotal;
                    if (row["OrderID"].ToString() == "0")
                    {
                        if (row["TypePayment"].ToString() == "5" && row["SubTotal"].ToString() != "0")
                            debit = subtotal;
                        else
                            debit = "";
                        orderID = "";
                        subtotal = "";
                    }
                    else
                    {
                        desc = getlistitem(row["OrderID"].ToString(), row["Date"].ToString());
                        if (row["SubTotal"].ToString() == "0")
                        {
                            subtotal = "";
                        }
                    }
                    if (row["PayAmount"].ToString() == "0")
                    {
                        payamount = "";
                    }
                    result += "<tr>" +
                                    "<td>" + i.ToString() + "</td>" +
                                    "<td>" + row["Date"].ToString() + "</td>" +
                                    "<td>" + row["Time"].ToString() + "</td>" +
                                    "<td>" + orderID + "</td>" +
                                    "<td>" + desc + "</td>" +
                                    "<td>" + subtotal + "</td>" +
                                    "<td>" + payamount + "</td>" +
                                    "<td>" + debit + "</td>" +
                                    "<td>" + balance + "</td>" +
                                "</tr>";
                    i++;
                }
                result += "<tr><td colspan='9' align=\"center\" style=\"background-color: #6F9ECE;\">Closing Balance <span style=\"float:right;margin-right: 8px;\">" + balance + "</span></td></tr>";
                result += "</table>";
                //result += "</table></div>";
                paymentdue = balance;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getaccountstatement::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public string getlistitem(string orderID, string date)
        {
            string result = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string query = "SELECT o.*,c.Name,c.memberNo, (select CarNumber from managecar mc Where mc.id = o.managecar) as CarNumber  FROM ordersdaily o left join customers c on c.custID=o.custID WHERE orderID=" + orderID + " and date(ts) = date('" + date + "');";
                string queryline = "SELECT ol.*,i.itemDesc,o.ts,o.gst, (select CarNumber from managecar mc Where mc.id = o.managecar) as CarNumber FROM (ordersdailyline ol LEFT JOIN ordersdaily o on o.orderID=ol.orderID ) LEFT JOIN itemsmenu i ON ol.itemID=i.itemID WHERE ol.orderID=" + orderID + ";";

                DataTable dt = dt = conn.Select(query);
                if (dt.Rows.Count == 0)
                {
                    query = "SELECT o.*,c.Name,c.memberNo, (select CarNumber from managecar mc Where mc.id = o.managecar) as CarNumber FROM ordersall o left join customers c on c.custID=o.custID WHERE bkId=" + orderID + " and date(ts) = date('" + date + "');";
                    queryline = "SELECT ol.*,i.itemDesc,oa.ts as oats,oa.gst, (select CarNumber from managecar mc Where mc.id = oa.managecar) as CarNumber FROM (ordersallline ol LEFT JOIN ordersall oa on oa.bkId=ol.bkId) LEFT JOIN itemsmenu i ON ol.itemID=i.itemID WHERE oa.OrderID = " + orderID + " And Date(oa.ts) = date('" + date + "');";
                }
                string cardNumber = "";
                DataTable resultList = conn.Select(queryline);
                bool flat = true;
                foreach (DataRow row in resultList.Rows)
                {
                    //lineID, orderID, ts, qty, itemID, dynID, optionID, price, weight, subTotal, bkId, 
                    //refund, gst, pumpID, pumpHistoryID, parentLine, itemDesc, oats, gst, CarNumber
                    cardNumber = row["CarNumber"].ToString();
                    string itemName = (row["itemDesc"].ToString() != "") ? row["itemDesc"].ToString() : "Unknown";
                    string itemQty = row["qty"].ToString();
                    if (Convert.ToInt32(row["weight"].ToString()) > 0)
                    {
                        itemQty = (Convert.ToInt32(row["qty"].ToString()) / Convert.ToInt32(row["weight"].ToString())).ToString();
                    }
                    if (Convert.ToInt32(row["optionID"].ToString()) > 0)
                    {
                        string optionquery = "SELECT itemDesc FROM itemslinemenu WHERE lineID= " + row["optionID"].ToString();
                        object objDesc = conn.ExecuteScalar(optionquery);
                        if (objDesc != null)
                            itemName = objDesc.ToString();
                        else
                            itemName = "Unknown";

                    }
                    if (Convert.ToInt32(row["dynID"].ToString()) > 0)
                    {
                        string dynquery = "SELECT itemDesc, unitPrice FROM dynitemsmenu WHERE dynID=" + row["dynID"].ToString() + " and date(ts)=date('" + row["ts"].ToString() + "') limit 0,1;";
                        DataTable dtdyn = conn.Select(dynquery);
                        foreach (DataRow dynRow in dtdyn.Rows)
                        {
                            if (dynRow["itemDesc"].ToString() != "")
                            {
                                itemName = dynRow["itemDesc"].ToString();
                                if (Convert.ToInt32(dynRow["unitPrice"].ToString()) <= 0)
                                    flat = false;
                            }
                            else
                            {
                                dynquery = "SELECT itemDesc,unitPrice FROM dynitemsall WHERE dynID=" + row["dynID"].ToString() + " and date(ts)=date('" + row["oats"].ToString() + "') limit 0,1;";
                                DataTable dtdynsall = conn.Select(dynquery);
                                foreach (DataRow dynsallRow in dtdyn.Rows)
                                {
                                    if (dynsallRow["itemDesc"].ToString() != "")
                                        itemName = dynsallRow["itemDesc"].ToString();
                                    else
                                        itemName = "DynItem";
                                    if (Convert.ToInt32(dynsallRow["unitPrice"].ToString()) <= 0)
                                        flat = false;
                                }
                            }
                        }
                    }
                    if (flat)
                    {
                        result += "<p>" + itemQty + " " + itemName + "</p>";
                    }
                }
                if (cardNumber != "")
                    result += "<p>CarNumber: " + cardNumber + "</p>";
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getlistitem::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public DataTable getListCustomers()
        {
            DataTable dt = new DataTable();
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "select if((subdate(curdate(),interval 30 day)>=date(tsSendMail)),1,0) as checksendmail," +
                                "custID, Name,FirstName,phone,mobile, email,Company, accountlimit, debt, memberNo, streetNo, streetName,DATE_FORMAT(tsSendMail,'%Y-%m-%d') as tsSendMail " +
                                "from customers c " +
                             "where email is not null and email <>'' and deleted <>1 and Company = 0;";
                dt = conn.Select(sql);
                SystemLog.LogPOS.WriteLog(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getListCustomers::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public DataTable getListCompany()
        {
            DataTable dt = new DataTable();
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "select if((subdate(curdate(),interval 30 day)>=date(tsSendMail)),1,0) as checksendmail, idCompany, companyName, accountLimit, debt, companyCode, streetNo, streetName, email, phone, mobile, DATE_FORMAT(tsSendMail,'%Y-%m-%d') as tsSendMail from company " +
                            "where idCompany in (select Company from customers) and idCompany <>0 and email<>'' and email is not null;";
                dt = conn.Select(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getListCompany::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public void updateCust(string dateSend, string custID)
        {
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "update customers set tsSendMail=date('" + dateSend + "') where custID = " + custID + ";";
                conn.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::updateCust::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public void updateCompany(string dateSend, string companyID)
        {
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "update company set tsSendMail = date('" + dateSend + "') where idCompany = " + companyID + ";";
                conn.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::updateCompany::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public string getInfoMail(string varname)
        {
            string result = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "select `value` from config where header='SendMail' and varName='" + varname + "';";
                object obj = conn.ExecuteScalar(sql);
                if (obj != null)
                    result = obj.ToString();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::getInfoMail::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        public string updateInfoMail(string varname, string value)
        {
            string result = "";
            Connection.Connection conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "update config set `value`='" + value + "' where header='SendMail' and varName='" + varname + "';";
                conn.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Class.SendMail::updateInfoMail::Error::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
    }
}
