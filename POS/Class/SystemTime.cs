﻿using System;
using System.Runtime.InteropServices;

namespace POS.Class
{
    internal class SystemTime
    {
        [DllImport("kernel32.dll")]
        private static extern bool SetLocalTime(ref SYSTEMTIME time);

        [StructLayoutAttribute(LayoutKind.Sequential)]
        private struct SYSTEMTIME
        {
            public short year;
            public short month;
            public short dayOfWeek;
            public short day;
            public short hour;
            public short minute;
            public short second;
            public short milliseconds;
        }

        public static void SetTime(DateTime dateTime)
        {
            SYSTEMTIME time = new SYSTEMTIME();
            time.year = (short)dateTime.Year;
            time.month = (short)dateTime.Month;
            time.day = (short)dateTime.Day;
            time.dayOfWeek = (short)dateTime.DayOfWeek;
            time.hour = (short)dateTime.Hour;
            time.minute = (short)dateTime.Minute;
            time.second = (short)dateTime.Second;
            time.milliseconds = (short)dateTime.Millisecond;
            SetLocalTime(ref time);
        }
    }
}