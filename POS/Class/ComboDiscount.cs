﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class ComboDiscount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PriceDiscount { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool Enable { get; set; }
        public bool IsDateEnd { get; set; }
        public int Priority { get; set; }
        public int qtyRule { get; set; }
        public int qtyRuleOld { get; set; }

        public List<ComboDiscRule> listComboDiscRule { get; set; }

        public ComboDiscount(int id, string name, double priceDiscount, DateTime dateStart, DateTime dateEnd, bool enable, bool isDateEnd)
        {
            Id = id;
            Name = name;
            PriceDiscount = priceDiscount;
            DateStart = dateStart;
            DateEnd = dateEnd;
            Enable = enable;
            IsDateEnd = isDateEnd;
            listComboDiscRule = new List<ComboDiscRule>();
            qtyRuleOld = 0;
            qtyRule = 0;
        }
        public ComboDiscount Copy()
        {
            return new ComboDiscount(
                this.Id,
                this.Name,
                this.PriceDiscount,
                this.DateStart,
                this.DateEnd,
                this.Enable,
                this.IsDateEnd);                
        }
        public ComboDiscount(System.Data.DataRow row,Connection.Connection con)
        {
            listComboDiscRule = new List<ComboDiscRule>();
            if (row["enable"].ToString() == "1")
            {
                Enable = true;
            }
            else
            {
                Enable = false;
            }
            Id=Convert.ToInt32(row["id"]);
            Name=row["name"].ToString();
            PriceDiscount= Convert.ToDouble(row["priceDiscount"]);
            Priority = Convert.ToInt32(row["priority"]);
            DateStart= Convert.ToDateTime(row["dateStart"]);
            if (row["dateEnd"]!=DBNull.Value)
            {
                DateEnd = Convert.ToDateTime(Encoding.Default.GetString((byte[])row["dateEnd"]));
                IsDateEnd = true;
            }
            else
            {
                DateEnd = DateTime.Now;
                IsDateEnd = false;
            }
            LoadComboDiscountRule(con);
        }
        public ComboDiscount()
        {
            listComboDiscRule = new List<ComboDiscRule>();
            qtyRuleOld = 0;
            qtyRule = 0;
        }
        private void LoadComboDiscountRule(Connection.Connection con)
        {
            System.Data.DataTable tbl = con.Select("select * from combodiscountrule where comboDiscountId = " + this.Id);
            foreach (System.Data.DataRow item in tbl.Rows)
            {
                ComboDiscRule rule = new ComboDiscRule(item,con);
                this.listComboDiscRule.Add(rule);
            }
        }
        public void AddItemToComboDiscount(List<ProcessOrderNew.Item> listItem)
        {
            foreach (ComboDiscRule rule in this.listComboDiscRule)
            {
                rule.ListItemCheck.Clear();
            }
            foreach (ProcessOrderNew.Item item in listItem)
            {
                foreach (ComboDiscRule rule in this.listComboDiscRule)
                {
                    if (rule.ContainItem(item))
                    {
                        rule.ListItemCheck.Add(item);
                    }
                }
            }
        }        
        public bool CheckComboDiscount()
        {
            bool check = true;
            foreach (ComboDiscRule rule in this.listComboDiscRule)
            {
                check = rule.CheckLoyalty() && check;
            }
            return check;
        }
        public void RolbackQty()
        {
            foreach (ComboDiscRule rule in this.listComboDiscRule)
            {
                foreach (ProcessOrderNew.Item item in rule.ListItemCheck)
                {
                    item.Qty = item.Qty_Tmp;
                }
            }
        }        
    }
}
