﻿using System;

namespace POS.Class
{

    internal class UpdateToServer
    {
        public static string _nameClass = "POS::Class::UpdateToServer";
        public static void UpDatabaseToServer(Connection.Connection con, Class.ReadConfig mReadConfig)
        {
            try
            {
                con = new Connection.Connection();
                con.Open();
                con.BeginTransaction();

                System.Data.DataTable tbl = con.Select("select orderID from ordersdaily where date(ts)<date(now())");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    con.ExecuteNonQuery("INSERT INTO `ordersall` (`orderID`,`tableID`,`ts`,`cableID`,`clients`,`subTotal`,`payment`,`completed`,`custID`,`staffID`,`loyaltyEnable`,`cash`,`cheque`,`eftpos`,`account`,`points`,`discount`,`refun`,`gst`,`orderbycardID`,`shiftID`,`change`,`changebycash`,`Surchart`,`IsDelivery`,`IsPickup`,`FeeDelivery`, `managecar`) select `orderID`,`tableID`,`ts`,`cableID`,`clients`,`subTotal`,`payment`,`completed`,`custID`,`staffID`,`loyaltyEnable`,`cash`,`cheque`,`eftpos`,`account`,`points`,`discount`,`refun`,`gst`,`orderbycardID`,`shiftID`,`change`,`changebycash`,`Surchart`,`IsDelivery`,`IsPickup`,`FeeDelivery`, `managecar` from ordersdaily where orderID=" + row["orderID"].ToString());
                    int bkId = Convert.ToInt32(con.ExecuteScalar("select max(bkId) from ordersall"));
                    con.ExecuteNonQuery("INSERT INTO `ordersallline` (`bkId`,`orderID`,`qty`,`itemID`,`dynID`,`optionID`,`price`,`weight`,`subTotal`,`pumpID`,`gst`,`pumpHistoryID`,`parentLine`) select " + bkId + " as bkId,`orderID`,`qty`,`itemID`,`dynID`,`optionID`,`price`,`weight`,`subTotal`,`pumpID`,`gst`,`pumpHistoryID`,`parentLine` from ordersdailyline ol where orderID=" + row["orderID"].ToString());
                    con.ExecuteNonQuery("delete from ordersdaily where orderID=" + row["orderID"].ToString());
                    con.ExecuteNonQuery("delete from ordersdailyline where orderID=" + row["orderID"].ToString());
                }
                if (Convert.ToInt32(con.ExecuteScalar("select count(orderID) from ordersdaily")) == 0)
                {
                    con.ExecuteNonQuery("truncate table ordersdaily");
                    con.ExecuteNonQuery("truncate table ordersdailyline");
                }

                con.ExecuteNonQuery("INSERT INTO fuelhistoryall(IDFuelHistory,KindOfFuel,PumID,CashAmount,VolumeAmount,UnitPrice,IsComplete,FromPump,Gst) " +
                                    "SELECT ID,KindOfFuel,PumID,CashAmount,VolumeAmount,UnitPrice,IsComplete,FromPump,Gst " +
                                    "FROM fuelhistory WHERE IsComplete = 1");
                con.ExecuteNonQuery("DELETE FROM fuelhistory WHERE IsComplete = 1");

                con.ExecuteNonQuery("insert into dynitemsall(dynID,itemShort,itemDesc,unitPrice,groupID,ts,cableID,shiftID) select dynID,itemShort,itemDesc,unitPrice,groupID,ts,cableID,shiftID from dynitemsmenu where dynID not in (select dynID from (select dynID from ordersdailyline where dynID<>0 Union All select dynID from saveordersdailyline where dynID<>0) as Dyn);");
                con.ExecuteNonQuery("delete from dynitemsmenu where dynID not in (select dynID from ordersdailyline where dynID<>0)");
                int count = Convert.ToInt32(con.ExecuteScalar("select count(dynID) from dynitemsmenu"));
                if (count == 0)
                {
                    con.ExecuteNonQuery("truncate table dynitemsmenu");
                }
                con.Commit();
            }
            catch (Exception ex)
            {
                con.Rollback();
                Class.LogPOS.WriteLog(_nameClass + "UpDatabaseToServer::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}