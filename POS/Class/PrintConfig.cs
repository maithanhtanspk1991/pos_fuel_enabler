﻿using System;

namespace POS.Class
{
    public class PrintConfig
    {
        public static string GetLabelPrinter()
        {
            string resuilt = "";
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                resuilt = ini.ReadValue("dbconfig", "LabelPrinter");
                if (resuilt == "")
                {
                    ini.WriteValue("dbconfig", "LabelPrinter", "");
                }
            }
            catch (Exception)
            {
            }
            return resuilt;
        }
    }
}