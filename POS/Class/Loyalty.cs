﻿using System.Collections.Generic;

namespace POS.Class
{
    internal class Loyalty
    {
        private Connection.Connection conn = new Connection.Connection();

        public string loyaltyid { get; set; }

        public string staffid { get; set; }

        public string orderid { get; set; }

        public double subloyalty { get; set; }

        public double totalafter { get; set; }

        public List<LoyaltyItem> list { get; set; }

        public Loyalty(string lyid, string sid, string oid, double sly, double after)
        {
            loyaltyid = lyid;
            staffid = sid;
            orderid = oid;
            subloyalty = sly;
            totalafter = after;
            list = new List<LoyaltyItem>();
        }

        public Loyalty()
        {
            list = new List<LoyaltyItem>();
        }

        public bool addloyaltyitem(LoyaltyItem loyaitem)
        {
            if (!list.Contains(loyaitem))
            {
                list.Add(loyaitem);
                return true;
            }
            return false;
        }

        public class LoyaltyItem
        {
            public string itemname { get; set; }

            public int Qty { get; set; }

            public double unitprice { get; set; }

            public double subtotal { get; set; }

            public LoyaltyItem(string name, int qty, double price, double stt)
            {
                itemname = name;
                Qty = qty;
                unitprice = price;
                subtotal = stt;
            }

            public LoyaltyItem()
            {
            }
        }

        //public bool submitLoyalty(Loyalty lo)
        //{
        //    bool resuilt = true;
        //    try
        //    {
        //        conn.Open();
        //        conn.BeginTransaction();
        //        //LoyaltyItem loi=new LoyaltyItem();
        //        conn.ExecuteNonQuery("update ordersdaily set subTotal=" + lo.subloyalty + ",staffID=" + lo.staffid + ",discount=" + lo.totalafter + " where orderID=" + lo.orderid);
        //        foreach (LoyaltyItem item in lo.list)
        //        {
        //            if (conn.ExecuteNonQuery("update ordersdailyline set qty=" + item.Qty +",price=" + item.unitprice +",subTotal=" + item.subtotal +" where orderID=" + lo.orderid) != 1)
        //            {
        //                throw new Exception();
        //            }
        //        }
        //        foreach (LoyaltyItem item in lo.list)
        //        {
        //            if (conn.ExecuteNonQuery("update changedorders set qty=" + item.Qty +",subTotal=" + item.subtotal +" where orderID=" + lo.orderid) != 1)
        //            {
        //                throw new Exception();
        //            }
        //        }
        //        conn.Commit();
        //    }
        //    catch(Exception)
        //    {
        //        conn.Rollback();
        //        resuilt = false;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return resuilt;
        //}
    }
}