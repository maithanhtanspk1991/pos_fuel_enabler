﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class ItemComboDisc
    {
        public int Id { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
        public int qtyItem { get; set; }
        
        public ItemComboDisc(int id, int itemID, string itemName, double price)
        {
            Id = id;
            ItemID = itemID;
            ItemName = itemName;
            Price = price;
            qtyItem = 0;
        }
        public ItemComboDisc(int itemID, string itemName, double price)
        {
            ItemID = itemID;
            ItemName = itemName;
            Price = price;
            qtyItem = 0;
        }
        public ItemComboDisc(System.Data.DataRow rowItem)
        { 
            ItemID= Convert.ToInt32(rowItem["itemId"]);
            ItemName= rowItem["Name"].ToString();
            Price = Convert.ToDouble(rowItem["Price"]);
        }
    }
}
