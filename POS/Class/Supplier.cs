﻿namespace POS.Class
{
    public class Supplier
    {
        public int SupplierID { get; set; }

        public string Name { get; set; }

        public string Account { get; set; }

        public string ABN { get; set; }

        public string Phone { get; set; }

        public Supplier(int supplierID, string name, string account, string abn, string phone)
        {
            SupplierID = supplierID;
            Name = name;
            Account = account;
            ABN = abn;
            Phone = phone;
        }
    }
}