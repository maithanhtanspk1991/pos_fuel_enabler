﻿using System.Data;

namespace POS.Class
{
    public class WeeklyReport
    {
        public string date { get; set; }

        public string day { get; set; }
        public string SubtotalSales { get; set; }

        public string totalSales { get; set; }

        public string totalDisCount { get; set; }

        public string totalGTS { get; set; }

        public string totalCard { get; set; }

        public string totalCash { get; set; }

        public string totalReturnPro { get; set; }

        public string Account { get; set; }

        public string cashDeposit { get; set; }

        public string cardDeposit { get; set; }

        public string CreditNote { get; set; }

        public string totalDeposit { get; set; }

        public string cardAccount { get; set; }

        public string cashAccount { get; set; }

        public string TotalAccount { get; set; }

        public string sumTotalCash { get; set; }

        public string sumTotalCard { get; set; }

        public string SumTotal { get; set; }

        public string sumTotalCashIn { get; set; }

        public string sumTotalCashOut { get; set; }

        public DataTable ListCard { get; set; }

        public DataTable ListShift { get; set; }

        public DataTable ListFuelSales { get; set; }

        public DataTable ListGroupSales { get; set; }

        public double sumTotalListCard { get; set; }

        public double sumTotalListCardSurcharge { get; set; }

        public string TotalSalesFuel { get; set; }

        public string TotalSalesNonFuel { get; set; }

        public string Surcharge { get; set; }

        public string PayOut { get; set; }
    }
}