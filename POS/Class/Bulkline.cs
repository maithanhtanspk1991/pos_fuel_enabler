﻿namespace POS.Class
{
    internal class Bulkline
    {
        public int LineID { get; set; }

        public int BulkID { get; set; }

        public int ItemID { get; set; }

        public int Size { get; set; }

        public string ItemName { get; set; }

        public Bulkline(int lineID, int bulkID, int itemID, int size, string itemname)
        {
            LineID = lineID;
            BulkID = bulkID;
            ItemID = itemID;
            Size = size;
            ItemName = itemname;
        }
    }
}