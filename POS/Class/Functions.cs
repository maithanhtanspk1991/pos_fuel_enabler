﻿using System;
using System.Collections.Generic;
using System.Data;

namespace POS.Class
{
    internal class Functions
    {
        private Connection.Connection conn = new Connection.Connection();

        public DataTable getgroupsmenubysql()
        {
            string query = "SELECT groupID,groupDesc FROM groupsmenu limit 0,7";
            conn.Open();
            DataTable dt = new DataTable();
            dt = conn.Select(query);
            conn.Close();
            return dt;
        }

        public DataTable getitemsmenubygroupsid(string query)
        {
            conn.Open();
            DataTable dt = new DataTable();
            dt = conn.Select(query);
            conn.Close();
            return dt;
        }

        public class table
        {
            public string tableid { get; set; }

            public int complete { get; set; }

            public string orderid { get; set; }

            public int people { get; set; }

            public double subtotal { get; set; }

            public int staff { get; set; }

            public string time { get; set; }

            public table()
            {
            }

            public table(string tbid, int comp)
            {
                tableid = tbid;
                complete = comp;
            }
        }

        public class TakeAway
        {
            public string orderid { get; set; }

            public int complete { get; set; }

            public string tableID { get; set; }

            public double SubTotal { get; set; }

            public int type { get; set; }

            public string Name { get; set; }

            public TakeAway()
            {
            }

            public TakeAway(string oid, int comp)
            {
                orderid = oid;
                complete = comp;
            }
        }

        public class LastOrder
        {
            public int OrderID { get; set; }

            public int Complete { get; set; }

            public string TableID { get; set; }

            public double SubTotal { get; set; }

            public int CableID { get; set; }

            public LastOrder()
            {
            }

            public LastOrder(int orderid, int complete, string tableid, double subtotal, int cableid)
            {
                OrderID = orderid;
                Complete = complete;
                TableID = tableid;
                SubTotal = subtotal;
                CableID = cableid;
            }
        }

        public class LastPaidOrder
        {
            public int OrderID { get; set; }

            public int Complete { get; set; }

            public string TableID { get; set; }

            public double SubTotal { get; set; }

            public int CableID { get; set; }

            public LastPaidOrder()
            {
            }

            public LastPaidOrder(int orderid, int complete, string tableid, double subtotal, int cableid)
            {
                OrderID = orderid;
                Complete = complete;
                TableID = tableid;
                SubTotal = subtotal;
                CableID = cableid;
            }
        }

        public List<TakeAway> GetStatusTakeAway()
        {
            List<TakeAway> list = new List<TakeAway>();
            try
            {
                conn.Open();
                //string strSQL = "SELECT subTotal,orderID,completed,tableID,SOURCE.IsDelivery,SOURCE.IsPickup,c.name "+
                //                "FROM " +
                //                "( "+
                //                "select subTotal,orderID,completed,tableID,od.IsDelivery,od.IsPickup,od.custID "+
                //                "from ordersdaily AS od "+
                //                "where completed<>1 and tableID like'TKA-%' "+
                //                "UNION ALL "+
                //                "select subTotal,orderID,completed,tableID,od.IsDelivery,od.IsPickup,od.custID "+
                //                "from ordersall AS od "+
                //                "where completed<>1 and tableID like'TKA-%' "+
                //                ")SOURCE left join customers AS c ON SOURCE.custID = c.custID";
                string strSQL = "select subTotal,orderID,completed,tableID,od.IsDelivery,od.IsPickup,c.name " +
                                "from ordersdaily AS od left join customers AS c ON od.custID = c.custID " +
                                "where completed<>1 and tableID like'TKA-%' ";
                System.Data.DataTable tb = conn.Select(strSQL);
                foreach (System.Data.DataRow row in tb.Rows)
                {
                    TakeAway tk = new TakeAway();
                    tk.complete = Convert.ToInt32(row["completed"]);
                    tk.orderid = row["orderID"].ToString();
                    tk.tableID = row["tableID"].ToString();
                    tk.SubTotal = Convert.ToDouble(row["subTotal"].ToString());
                    tk.Name = row["Name"].ToString();
                    if (Convert.ToInt32(row["IsDelivery"]) == 1)
                    {
                        tk.type = 1;
                    }
                    else if (Convert.ToInt32(row["IsPickup"]) == 1)
                    {
                        tk.type = 2;
                    }
                    else
                    {
                        tk.type = 0;
                    }

                    list.Add(tk);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Functions.GetStatusTakeAway:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return list;
        }

        //public bool Checkorder(string tableID)
        //{
        //    Class.ProcessOrder.Order order = new ProcessOrder.Order();
        //    if (prorder.getOrderByTable(order.TableID) == null)
        //    {
        //        return false;
        //    }
        //    else
        //        return true;
        //}

        public int GetCompletedTable(string tableID)
        {
            table tbl = new table();
            tbl.complete = 1;
            try
            {
                System.Data.DataTable tb = conn.Select("select ts,subTotal,orderID,completed,tableID,clients,staffID from ordersdaily where completed<>1 and tableID=" + "'" + tableID + "'");
                foreach (System.Data.DataRow row in tb.Rows)
                {
                    tbl.complete = Convert.ToInt32(tb.Rows[0]["completed"]);
                    tbl.tableid = tb.Rows[0]["tableID"].ToString();
                    tbl.people = Convert.ToInt32(tb.Rows[0]["clients"]);
                    tbl.subtotal = Convert.ToDouble(tb.Rows[0]["subTotal"]);
                    tbl.staff = Convert.ToInt32(tb.Rows[0]["staffID"]);
                    tbl.time = tb.Rows[0]["ts"].ToString();
                }
                return tbl.complete;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Functions.GetCompletedTable:::" + ex.Message);
            }
            return 1;
        }

        public table GetStatusTable(string tableID)
        {
            table tbl = new table();
            tbl.complete = 1;
            try
            {
                System.Data.DataTable tb = conn.Select("select ts,subTotal,orderID,completed,tableID,clients,staffID from ordersdaily where completed<>1 and tableID=" + "'" + tableID + "'");
                foreach (System.Data.DataRow row in tb.Rows)
                {
                    tbl.complete = Convert.ToInt32(tb.Rows[0]["completed"]);
                    tbl.tableid = tb.Rows[0]["tableID"].ToString();
                    tbl.people = Convert.ToInt32(tb.Rows[0]["clients"]);
                    tbl.subtotal = Convert.ToDouble(tb.Rows[0]["subTotal"]);
                    tbl.staff = Convert.ToInt32(tb.Rows[0]["staffID"]);
                    tbl.time = tb.Rows[0]["ts"].ToString();
                }
                return tbl;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Functions.GetStatusTable:::" + ex.Message);
            }
            return null;
        }

        public bool CheckChangeLastOrder(System.Windows.Forms.ListView lst)
        {
            bool resuilt = false;

            try
            {
                string s = "";
                foreach (System.Windows.Forms.ListViewItem item in lst.Items)
                {
                    s += item.Tag.ToString() + ",";
                }
                if (s.Length > 0)
                {
                    s = s.Remove(s.Length - 1, 1);
                }
                string sql = "select count(orderID) from (select orderID from ordersdailywhere completed<>1 order by orderID desc limit 0,10) as tmp where orderID in(" + s + ")";
                int count = Convert.ToInt16(conn.ExecuteScalar(sql));
                if (count < 10)
                {
                    resuilt = true;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Function.CheckChangeLastOrder:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return resuilt;
        }

        public bool CheckPaidOrders(System.Windows.Forms.ListView lst)
        {
            bool resuilt = false;

            try
            {
                string s = "";
                foreach (System.Windows.Forms.ListViewItem item in lst.Items)
                {
                    s += item.Tag.ToString() + ",";
                }
                if (s.Length > 0)
                {
                    s = s.Remove(s.Length - 1, 1);
                }
                string sql = "select count(orderID) from (select orderID from ordersdailywhere completed=1 order by orderID desc limit 0,10) as tmp where orderID in(" + s + ")";
                int count = Convert.ToInt16(conn.ExecuteScalar(sql));
                if (count < 10)
                {
                    resuilt = true;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Function.CheckChangeLastOrder:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return resuilt;
        }

        public List<LastOrder> GetLastOrders()
        {
            List<LastOrder> list = new List<LastOrder>();
            try
            {
                System.Data.DataTable dt = conn.Select("select tableID,subTotal,orderID,cableID,completed from ordersdaily where completed<>1 order by orderID desc limit 0,10;");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    LastOrder lp = new LastOrder();
                    lp.TableID = row["tableID"].ToString();
                    lp.SubTotal = Convert.ToDouble(row["subTotal"].ToString());
                    lp.CableID = Convert.ToInt32(row["cableID"].ToString());
                    lp.Complete = Convert.ToInt32(row["completed"].ToString());
                    lp.OrderID = Convert.ToInt32(row["orderID"].ToString());
                    list.Add(lp);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Functions.GetLastOrders:::" + ex.Message);
            }
            return list;
        }

        public List<LastPaidOrder> GetLastPaidOrders()
        {
            List<LastPaidOrder> list = new List<LastPaidOrder>();
            try
            {
                System.Data.DataTable dt = conn.Select("select tableID,subTotal,orderID,cableID,completed from ordersdaily where completed=1 order by ts desc limit 0,10;");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    LastPaidOrder lp = new LastPaidOrder();
                    lp.TableID = row["tableID"].ToString();
                    lp.SubTotal = Convert.ToDouble(row["subTotal"].ToString());
                    lp.CableID = Convert.ToInt32(row["cableID"].ToString());
                    lp.Complete = Convert.ToInt32(row["completed"].ToString());
                    lp.OrderID = Convert.ToInt32(row["orderID"].ToString());
                    list.Add(lp);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Functions.GetLastPaidOrders:::" + ex.Message);
            }
            return list;
        }
    }
}