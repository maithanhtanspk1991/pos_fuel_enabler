﻿namespace POS.Class
{
    public class OrderByCard
    {
        public string orderbycardID { get; set; }

        public int orderID { get; set; }

        public int cardID { get; set; }

        public double subtotal { get; set; }

        public double cashOut { get; set; }

        public string nameCard { get; set; }

        public int shiftID { get; set; }

        public double surcharge { get; set; }

        public string AuthCode { get; set; }

        public string AccountType { get; set; }

        public string Pan { get; set; }

        public string DateExpiry { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string CardType { get; set; }

        public decimal AmtPurchase { get; set; }

        public decimal AmtCash { get; set; }
    }
}