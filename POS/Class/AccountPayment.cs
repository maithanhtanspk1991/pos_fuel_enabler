﻿using System.Collections.Generic;

namespace POS.Class
{
    public class AccountPayment
    {
        public List<Item> ListItem { get; set; }

        public int PaymetID { get; set; }

        public int EmployeeID { get; set; }

        public double PayAmount { get; set; }

        public double Cash { get; set; }

        public double Card { get; set; }

        public double SubTotal { get; set; }

        public double Limit { get; set; }

        public string CustomerName { get; set; }

        public double Debt { get; set; }

        public double Change { get; set; }

        public string CustomerNo { get; set; }

        public double Surcharge { get; set; }

        public string note { get; set; }
        public AccountPayment()
        {
            ListItem = new List<Item>();
        }

        public AccountPayment(
            int PaymetID,
            int EmployeeID,
            double PayAmount,
            double Cash,
            double Card,
            double SubTotal,
            string CustomerNo,
            string Note
            )
        {
            this.PaymetID = PaymetID;
            this.EmployeeID = EmployeeID;
            this.PayAmount = PayAmount;
            this.Cash = Cash;
            this.Card = Card;
            this.SubTotal = SubTotal;
            this.note = Note;
            ListItem = new List<Item>();
        }

        public double getSubTotal()
        {
            double resuilt = 0;
            resuilt = Cash + Card;
            return resuilt;
        }

        public string OrderByCardID { get; set; }

        public string AuthCode { get; set; }

        public string AccountType { get; set; }

        public string DateExpiry { get; set; }

        public string Pan { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string CardType { get; set; }

        public decimal AmtPurchase { get; set; }

        public decimal AmtCash { get; set; }
    }
}