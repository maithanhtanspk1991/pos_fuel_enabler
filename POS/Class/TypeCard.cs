﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    class TypeCard
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public TypeCard(int id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
