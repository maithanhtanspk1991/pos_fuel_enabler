﻿namespace POS.Class
{
    internal class Customers
    {
        public int CustID { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Company { get; set; }

        public string Email { get; set; }

        public Customers(int custid, string name, string phone, string mobile)
        {
            CustID = custid;
            Name = name;
            Phone = phone;
            Mobile = mobile;
        }
    }
}