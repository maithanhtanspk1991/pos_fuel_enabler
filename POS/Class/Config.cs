﻿using System;

namespace POS.Class
{
    public class Config
    {
        public static bool GetEnableClientServer()
        {
            bool resuilt = false;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                string data = ini.ReadValue("ClientServer", "EnableClientServer");
                if (data == "")
                {
                    data = "false";
                    ini.WriteValue("ClientServer", "EnableClientServer", data);
                }
                try
                {
                    resuilt = Convert.ToBoolean(data);
                }
                catch (Exception)
                {
                    ini.WriteValue("ClientServer", "EnableClientServer", "false");
                }
            }
            catch (Exception)
            {
            }
            return resuilt;
        }

        public static int GetCable()
        {
            int resuilt = 0;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                string data = ini.ReadValue("dbconfig", "CableID");
                if (data == "")
                {
                    data = "1";
                    ini.WriteValue("dbconfig", "CableID", data);
                }
                resuilt = Convert.ToInt32(data);
            }
            catch (Exception)
            {
            }
            return resuilt;
        }

        public static Class.PortCom GetPortCom(Class.PortCom p)
        {
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                //string AllowUseShift = ini.ReadValue(p.PortComType.ToString(), "AllowUseShift");
                string BaudRate = ini.ReadValue(p.PortComType.ToString(), "BaudRate");
                string DataBits = ini.ReadValue(p.PortComType.ToString(), "DataBits");
                string Parity = ini.ReadValue(p.PortComType.ToString(), "Parity");
                string StopBits = ini.ReadValue(p.PortComType.ToString(), "StopBits");
                string PortName = ini.ReadValue(p.PortComType.ToString(), "PortName");

                try
                {
                    //p.AllowUseShift = Convert.ToBoolean(AllowUseShift);
                    p.BaudRate = Convert.ToInt32(BaudRate);
                    p.DataBits = Convert.ToInt32(DataBits);
                    p.Parity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), Parity);
                    p.StopBits = (System.IO.Ports.StopBits)Enum.Parse(typeof(System.IO.Ports.StopBits), StopBits);
                    p.PortName = PortName;
                }
                catch (Exception)
                {
                    //ini.WriteValue(p.PortComType.ToString(), "AllowUseShift", p.AllowUseShift + "");
                    ini.WriteValue(p.PortComType.ToString(), "BaudRate", p.BaudRate + "");
                    ini.WriteValue(p.PortComType.ToString(), "DataBits", p.DataBits + "");
                    ini.WriteValue(p.PortComType.ToString(), "Parity", p.Parity + "");
                    ini.WriteValue(p.PortComType.ToString(), "StopBits", p.StopBits + "");
                    ini.WriteValue(p.PortComType.ToString(), "PortName", p.PortName + "");
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS::Class::Config::GetFuelAuto::" + ex.Message);
            }
            return p;
        }

        public static int SetPortCom(Class.PortCom p)
        {
            int result = 0;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);
                ini.WriteValue(p.PortComType.ToString(), "AllowUseShift", p.AllowUseShift + "");
                ini.WriteValue(p.PortComType.ToString(), "BaudRate", p.BaudRate + "");
                ini.WriteValue(p.PortComType.ToString(), "DataBits", p.DataBits + "");
                ini.WriteValue(p.PortComType.ToString(), "Parity", p.Parity + "");
                ini.WriteValue(p.PortComType.ToString(), "StopBits", p.StopBits + "");
                ini.WriteValue(p.PortComType.ToString(), "PortName", p.PortName + "");
                result = 1;
            }
            catch (Exception ex)
            {
                result = -1;
                Class.LogPOS.WriteLog("POS::Class::Config::SetPortCom::" + ex.Message);
            }
            return result;
        }

        public static bool GetEnablePrintBarAndKitchen()
        {
            bool resuilt = false;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                string data = ini.ReadValue("PrintConfig", "EnablePrintKitchenAndBar");
                try
                {
                    resuilt = Convert.ToBoolean(data);
                }
                catch (Exception)
                {
                    ini.WriteValue("PrintConfig", "EnablePrintKitchenAndBar", resuilt + "");
                }
            }
            catch (Exception)
            {
            }
            return resuilt;
        }
        public static bool GetEnableResetOrderID()
        {
            bool resuilt = false;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                string data = ini.ReadValue("POS", "EnableResetOrderID");
                try
                {
                    resuilt = Convert.ToBoolean(data);
                }
                catch (Exception)
                {
                    ini.WriteValue("POS", "EnableResetOrderID", resuilt + "");
                }
            }
            catch (Exception)
            {
            }
            return resuilt;
        }
    }
}