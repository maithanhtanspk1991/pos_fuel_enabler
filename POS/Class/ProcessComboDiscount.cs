﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    class ProcessComboDiscount
    {
        //private List<ComboDiscount> mListComboDiscount;

        public static List<ComboDiscount> GetListComboDiscount(List<ProcessOrderNew.Item> listItem)
        {
            List<ProcessOrderNew.Item> listItemCopy = new List<ProcessOrderNew.Item>();
            foreach (ProcessOrderNew.Item item in listItem)
            {                
                listItemCopy.Add(item.Coppy());
            }
            List<ComboDiscount> resuilt = new List<ComboDiscount>();

            List<ComboDiscount> listComboDiscount = GetLisComboDiscountByListItem(listItemCopy);

            foreach (ComboDiscount combo in listComboDiscount)
            {
                combo.AddItemToComboDiscount(listItemCopy);                
                while (combo.CheckComboDiscount())
                {
                    bool check = false;
                    ComboDiscount comRe=combo.Copy();
                    comRe.qtyRule=1;
                    foreach (ComboDiscount item in resuilt)
                    {
                        if (item.Id==comRe.Id)
                        {
                            item.qtyRule+=comRe.qtyRule;
                            check = true;
                            break;
                        }
                    }
                    if (!check)
                    {
                        resuilt.Add(comRe);   
                    }                    
                }
                combo.RolbackQty();
            }


            return resuilt;
            //return listComboDiscount;
        }        
        private static List<ComboDiscount> GetLisComboDiscountByListItem(List<ProcessOrderNew.Item> listItem)
        {
            List<ComboDiscount> resuilt = new List<ComboDiscount>();
            string strListItem = "";
            foreach (ProcessOrderNew.Item item in listItem)
            {
                strListItem += item.ItemID + ",";
            }
            if (strListItem.Length > 0)
            {
                strListItem = strListItem.Remove(strListItem.Length - 1, 1);
            }

            Connection.Connection con = new Connection.Connection();
            try
            {
                con.Open();
                string rule = "";
                if (strListItem.Length>0)
                {
                    rule="where itemID in (" + strListItem + ")";
                }                
                string sql =
                    "select id,name,priceDiscount,priority,dateStart,if(dateEnd='0000-00-00 00:00:00',null,date(dateEnd)) as dateEnd,enable " +
                    "from combodiscount "+
                    "where "+                        
                        "enable=1 and "+
                        "date(dateStart)<=date(now()) and "+
                        "(dateEnd is not null or date(dateEnd)>=date(now())) and "+
                        "`id` in ("+
                            "select comboDiscountId "+
                            "from combodiscountrule "+
                            "where `id` in("+
                                "select comboDiscountRuleId "+
                                "from combodiscountitem "+
                                rule+
                            ")"+
                        ")"+
                      "order by priority";
                System.Data.DataTable tblCombo = con.Select(sql);
                foreach (System.Data.DataRow row in tblCombo.Rows)
                {
                    ComboDiscount combo = new ComboDiscount(row,con);                    
                    resuilt.Add(combo);
                }
            }
            catch (Exception ex)
            {
                LogPOS.WriteLog("GetLisComboDiscxountByListItem:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return resuilt;
        }       
    }
}
