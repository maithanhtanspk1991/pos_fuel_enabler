﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    class ListCardType
    {
        public System.Collections.ArrayList mListCardType;

        public ListCardType()
        {
            mListCardType = new System.Collections.ArrayList();
            mListCardType.Add(new Class.TypeCard(1, "Debit"));
            mListCardType.Add(new Class.TypeCard(2, "Credit"));
        }
        public TypeCard getTypeCardById(int id)
        {
            foreach (TypeCard type in mListCardType)
            {
                if (type.ID==id)
                {
                    return type;
                }
            }
            return null;
        }
    }
}
