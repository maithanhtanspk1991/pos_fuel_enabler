﻿using System;

namespace POS.Class
{
    internal class StaffConfig
    {
        public static bool PasswordEnable()
        {
            string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            sPath += "\\config.ini";
            clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);
            try
            {
                return Convert.ToBoolean(ini.ReadValue("staffconfig", "PasswordEnable"));
            }
            catch (Exception)
            {
                ini.WriteValue("staffconfig", "PasswordEnable", "true");
            }
            return true;
        }
    }
}