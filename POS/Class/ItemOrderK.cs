﻿using System;
using System.Collections.Generic;

namespace POS.Class
{
    public class ItemOrderK
    {
        public string Name { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public double Total { get; set; }

        public int ItemID { get; set; }

        public List<ItemOptionID> Option { get; set; }

        public string OrderID { get; set; }

        public string IsDelivery { get; set; }

        public string IsPickup { get; set; }

        public int ChangeStatus { get; set; }

        public double DisCount { get; set; }

        public int NumPeople { get; set; }

        public int GRShortCut { get; set; }

        public int ItemType { get; set; }

        public int Weight { get; set; }

        public int Gst { get; set; }

        public int PumpID { get; set; }

        public int Visual { get; set; }

        public string carid { get; set; }

        public string PrinterName
        {
            get
            {
                return Name;
                //if (Weight > 0)
                //    return Name + " (" + String.Format("{0:0.000}", Price / 1000) + "/litre)";
                //else

                //    return Name;
            }
        }

        public string PrinterQty
        {
            get
            {
                Weight = Weight > 1 ? Weight : 1;
                if (Weight > 1)
                {
                    double value = (double)Qty / Weight;
                    value = Math.Round(value, 2);
                    return String.Format("{0:0.00}", value) + " L";

                }
                else
                {
                    return Qty.ToString();
                }
            }
        }

        public ItemOrderK(int itemID, double price, double total, int qty, string name, string orderID, int changeStatus, double discount, int numPeople, int grShortCut, int itemType, int weight, int gst, int pumpID, int visual, string delivery, string pickup)
        {
            Weight = weight;
            ItemID = itemID;
            Price = price;
            Total = total;
            Qty = qty;
            Name = name;
            OrderID = orderID;
            ChangeStatus = changeStatus;
            DisCount = discount;
            NumPeople = numPeople;
            GRShortCut = grShortCut;
            ItemType = itemType;
            Gst = gst;
            PumpID = pumpID;
            Visual = visual;
            IsDelivery = delivery;
            IsPickup = pickup;
            Option = new List<ItemOptionID>();
        }

        public ItemOrderK(int itemID, double price, double total, int qty, string name, string orderID, int changeStatus, double discount, int numPeople, int grShortCut, int itemType, int weight, int gst, int pumpID, int visual)
        {
            Weight = weight;
            ItemID = itemID;
            Price = price;
            Total = total;
            Qty = qty;
            Name = name;
            OrderID = orderID;
            ChangeStatus = changeStatus;
            DisCount = discount;
            NumPeople = numPeople;
            GRShortCut = grShortCut;
            ItemType = itemType;
            Gst = gst;
            PumpID = pumpID;
            Visual = visual;
            Option = new List<ItemOptionID>();
        }

        public ItemOrderK(int itemID, double price, int qty, string name, string orderID, int changeStatus, double discount, int numPeople, int grShortCut, int itemType, string delivery, string pickup)
        {
            ItemID = itemID;
            Price = price;
            Qty = qty;
            Name = name;
            OrderID = orderID;
            ChangeStatus = changeStatus;
            DisCount = discount;
            NumPeople = numPeople;
            GRShortCut = grShortCut;
            ItemType = itemType;
            IsDelivery = delivery;
            IsPickup = pickup;
            Option = new List<ItemOptionID>();
        }

        public ItemOrderK(int itemID, double price, int qty, string name, string orderID, int changeStatus, double discount, int numPeople, int grShortCut, int itemType)
        {
            ItemID = itemID;
            Price = price;
            Qty = qty;
            Name = name;
            OrderID = orderID;
            ChangeStatus = changeStatus;
            DisCount = discount;
            NumPeople = numPeople;
            GRShortCut = grShortCut;
            ItemType = itemType;
            Option = new List<ItemOptionID>();
        }

        public bool Compare(ItemOrderK item)
        {
            if (Weight != item.Weight)
            {
                return false;
            }
            if (ItemID != item.ItemID || ItemType != item.ItemType)
            {
                return false;
            }
            if (ChangeStatus != item.ChangeStatus)
            {
                return false;
            }
            if (ItemID == 0 || item.ItemID == 0)
            {
                return false;
            }
            if ((Total / Qty) != (item.Total / item.Qty))
            {
                return false;
            }
            if (Option.Count != item.Option.Count)
            {
                return false;
            }
            for (int i = 0; i < Option.Count; i++)
            {
                if (!Option[i].Conpare(item.Option[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public void AddSameItem(ItemOrderK item)
        {
            this.Price += item.Price;
            this.Qty += item.Qty;
            try
            {
                for (int i = 0; i < Option.Count; i++)
                {
                    Option[i].Price += item.Option[i].Price;
                    Option[i].Qty += item.Option[i].Qty;
                }
            }
            catch (Exception)
            {
            }
        }
    }

    public class ItemOptionID
    {
        public string Name { get; set; }

        public double Price { get; set; }

        public int OptionID { get; set; }

        public int Qty { get; set; }

        public int ItemID { get; set; }

        public ItemOptionID(string name, double price, int optionID, int itemID, int qty)
        {
            Name = name;
            Price = price;
            OptionID = optionID;
            Qty = qty;
            ItemID = itemID;
        }

        public bool Conpare(ItemOptionID option)
        {
            if (OptionID != option.OptionID)
            {
                return false;
            }
            if (Qty == 0 || option.Qty == 0)
            {
                return false;
            }
            if ((Price / Qty) != (option.Price / option.Qty))
            {
                return false;
            }
            return true;
        }
    }
}