﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class FuelGrade
    {
        public int gradeId { get; set; }

        public string gradeName { get; set; }

        public decimal gradePrice { get; set; }

        public FuelGrade(int id, string name, decimal price)
        {
            gradeId = id;
            gradeName = name;
            gradePrice = price;
        }
    }
}
