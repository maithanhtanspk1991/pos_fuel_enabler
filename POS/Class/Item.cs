﻿using System.Collections.Generic;

namespace POS.Class
{
    public class Item
    {
        public int itemId { get; set; }

        public string itemName { get; set; }

        public List<SubItem> list;

        public Item(int id, string name)
        {
            itemId = id;
            itemName = name;
            list = new List<SubItem>();
        }

        public bool addSubitem(SubItem item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
                return true;
            }
            return false;
        }
    }
}