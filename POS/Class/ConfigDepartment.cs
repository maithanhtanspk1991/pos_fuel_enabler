﻿namespace POS.Class
{
    public class ConfigDepartment
    {
        public Departnemt Department1 { get; set; }

        public Departnemt Department2 { get; set; }

        public Departnemt Department3 { get; set; }

        public Departnemt Department4 { get; set; }

        public Departnemt Department5 { get; set; }

        public Departnemt Department6 { get; set; }

        public Departnemt Department7 { get; set; }

        public Departnemt Department8 { get; set; }

        public ConfigDepartment()
        {
            Department1 = Class.Departnemt.getDepartment(readData("department", "Dep1"));
            Department2 = Class.Departnemt.getDepartment(readData("department", "Dep2"));
            Department3 = Class.Departnemt.getDepartment(readData("department", "Dep3"));
            Department4 = Class.Departnemt.getDepartment(readData("department", "Dep4"));
            Department5 = Class.Departnemt.getDepartment(readData("department", "Dep5"));
            Department6 = Class.Departnemt.getDepartment(readData("department", "Dep6"));
            Department7 = Class.Departnemt.getDepartment(readData("department", "Dep7"));
            Department8 = Class.Departnemt.getDepartment(readData("department", "Dep8"));
        }

        private string readData(string head, string depa)
        {
            string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            sPath += "\\config.ini";
            clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);
            string result = ini.ReadValue(head, depa);
            if (result == "")
            {
                result = "Department;DepartmentDetail;0";
                ini.WriteValue(head, depa, result);
            }
            return result;
        }
    }
}