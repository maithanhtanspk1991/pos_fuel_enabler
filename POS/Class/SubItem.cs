﻿using System.Collections.Generic;

namespace POS.Class
{
    public class SubItem
    {
        public int subItemID { get; set; }

        public string SubItemName { get; set; }

        public List<SubItemline> list;

        public SubItem(int id, string name)
        {
            subItemID = id;
            SubItemName = name;
            list = new List<SubItemline>();
        }

        public bool addSubitemline(SubItemline subitem)
        {
            if (!list.Contains(subitem))
            {
                list.Add(subitem);
                return true;
            }
            return false;
        }
    }
}