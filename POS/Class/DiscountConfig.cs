﻿using System;

namespace POS.Class
{
    internal class DiscountConfig
    {
        public static int GetDiscountConfig()
        {
            int discount = 0;
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                clsReadAndWriteINI ini = new clsReadAndWriteINI(sPath);

                string resuilt = ini.ReadValue("discount", "MaxDiscount");
                if (resuilt == "")
                {
                    ini.WriteValue("discount", "MaxDiscount", "0");
                    resuilt = "0";
                }
                discount = Convert.ToInt32(resuilt);
            }
            catch (Exception)
            {
            }
            return discount;
        }
    }
}