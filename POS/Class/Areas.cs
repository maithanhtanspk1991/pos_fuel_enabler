﻿namespace POS.Class
{
    internal class Areas
    {
        public int AreaID { get; set; }

        public string Name { get; set; }

        public int FranchiseID { get; set; }

        public string Location { get; set; }

        public string Country { get; set; }

        public Areas(int areaID, string name, int franchiseID, string location, string country)
        {
            AreaID = areaID;
            Name = name;
            FranchiseID = franchiseID;
            Location = location;
            Country = country;
        }
    }
}