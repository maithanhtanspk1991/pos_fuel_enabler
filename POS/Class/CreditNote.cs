﻿using System;

namespace POS.Class
{
    internal class CreditNote
    {
        public int creditNoteID { get; set; }

        public string creditNoteCode { get; set; }

        public string creditNoteTotal { get; set; }

        public DateTime dateCreditNote { get; set; }
    }
}