﻿using System;

namespace POS.Class
{
    public class PortCom
    {
        private PortComType portComType = PortComType.None;

        public PortComType PortComType
        {
            get { return portComType; }
            set { portComType = value; }
        }

        private bool allowUseShift = false;

        public bool AllowUseShift
        {
            get { return allowUseShift; }
            set { allowUseShift = value; }
        }

        private int baudRate = 1;

        public int BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; }
        }

        private int dataBits = 7;

        public int DataBits
        {
            get { return dataBits; }
            set { dataBits = value; }
        }

        private System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even;

        public System.IO.Ports.Parity Parity
        {
            get { return parity; }
            set { parity = value; }
        }

        private System.IO.Ports.StopBits stopBits = System.IO.Ports.StopBits.One;

        public System.IO.Ports.StopBits StopBits
        {
            get { return stopBits; }
            set { stopBits = value; }
        }

        private string portName = "";

        public string PortName
        {
            get { return portName; }
            set { portName = value; }
        }

        public static PortCom Set(string AllowUseShift, string BaudRate, string DataBits, string Parity, string StopBits, string PortName)
        {
            PortCom item = new PortCom();
            item.AllowUseShift = Convert.ToBoolean(AllowUseShift);
            item.BaudRate = int.Parse(BaudRate);
            item.DataBits = int.Parse(DataBits);
            item.Parity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), Parity);
            item.StopBits = (System.IO.Ports.StopBits)Enum.Parse(typeof(System.IO.Ports.StopBits), StopBits);
            item.PortName = PortName;
            return item;
        }

        public static PortCom Get(PortComType PortComType)
        {
            PortCom item = new PortCom();
            item.PortComType = PortComType;
            item = Class.Config.GetPortCom(item);
            return item;
        }

        public static int Set(PortCom PortCom)
        {
            return Class.Config.SetPortCom(PortCom);
        }
    }

    public enum PortComType
    {
        None = 0,
        Fuel,
        Barcode
    }
}