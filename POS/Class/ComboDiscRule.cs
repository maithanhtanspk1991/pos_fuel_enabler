﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class ComboDiscRule
    {
        public int ruleId { get; set; }
        public int comboDiscountId { get; set; }
        public int qty { get; set; }
        /// <summary>
        /// số lần rule thỏa điều kiện: tổng số item trong rule = qty
        /// </summary>
        public int qtyDisc { get; set; }

        public List<ItemComboDisc> listItemComboDisc { get; set; }

        public List<ProcessOrderNew.Item> ListItemCheck { get; set; }

        public ComboDiscRule()
        {
            listItemComboDisc = new List<ItemComboDisc>();
            ListItemCheck = new List<ProcessOrderNew.Item>();
            qtyDisc = 0;
        }
        public ComboDiscRule(int ruleId,int comboDiscountId,int qty)
        {
            this.ruleId = ruleId;
            this.comboDiscountId = comboDiscountId;
            this.qty = qty;
            listItemComboDisc = new List<ItemComboDisc>();
            ListItemCheck = new List<ProcessOrderNew.Item>();
            qtyDisc = 0;
        }
        public ComboDiscRule(System.Data.DataRow rowRule,Connection.Connection con)
        {
            listItemComboDisc = new List<ItemComboDisc>();
            ListItemCheck = new List<ProcessOrderNew.Item>();
            ruleId= Convert.ToInt32(rowRule["id"]);
            comboDiscountId= Convert.ToInt32(rowRule["comboDiscountId"]);
            qty = Convert.ToInt32(rowRule["qty"]);
            LoadComboDiscountItem(con);
        }
        private void LoadComboDiscountItem(Connection.Connection con)
        {
            System.Data.DataTable tbl = con.Select("select c.*,(select i.itemDesc from itemsmenu i where i.itemID=c.itemId) as Name," +
                                              "(select i.unitPrice from itemsmenu i where i.itemID=c.itemID) as Price " +
                                              "from combodiscountitem c where c.comboDiscountRuleId = " + this.ruleId);
            foreach (System.Data.DataRow item in tbl.Rows)
            {
                this.listItemComboDisc.Add(new ItemComboDisc(item));
            }
        }
        public bool ContainItem(ProcessOrderNew.Item item)
        {
            bool resuilt = false;
            foreach (ItemComboDisc itemCombo in this.listItemComboDisc)
            {
                if (itemCombo.ItemID==item.ItemID)
                {
                    resuilt = true;
                }
            }
            return resuilt;
        }
        public bool CheckLoyalty()
        {
            int qtyCount = 0;
            bool check = false;
            foreach (ProcessOrderNew.Item item in ListItemCheck)
            {
                item.Qty_Tmp = item.Qty;
                if (!check)
                {
                    if (item.Qty > 0)
                    {
                        if ((item.Qty + qtyCount) == this.qty)
                        {
                            item.Qty = 0;
                            check= true;
                        }
                        else if ((item.Qty + qtyCount) > this.qty)
                        {
                            item.Qty = qtyCount + item.Qty - this.qty;
                            check= true;
                        }
                        else
                        {
                            qtyCount += item.Qty;
                            item.Qty = 0;
                        }
                    }    
                }
                
            }
            return check;
        }
    }
}
