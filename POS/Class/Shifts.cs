﻿using System;
using System.Data;
using POS.Forms;

namespace POS.Class
{
    public class Shifts
    {
        public int ShiftID { get; set; }

        public int CableID { get; set; }

        public double CashFloat { get; set; }

        public double CashDrop { get; set; }

        public double FinalDrop { get; set; }

        private Connection.Connection mConnection;

        public Shifts(int inNoneOfShift)
        {
            mConnection = new Connection.Connection();
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                CableID = Convert.ToInt32(mConnection.GetCableID());
                InitShift(inNoneOfShift);

                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        public Shifts()
        {
            mConnection = new Connection.Connection();
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                CableID = Convert.ToInt32(mConnection.GetCableID());
                InitShift();

                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();

        private void InitShift()
        {
            object obj = mConnection.ExecuteScalar("select shiftID from (select shiftID from shifts where completed=0 and cableID=" + CableID + " union all select shiftID from shiftsall where completed=0 and cableID=" + CableID + ") as tmp order by shiftID desc limit 0,1");
            if (obj != null)
            {
                ShiftID = Convert.ToInt32(obj);
            }
            else
            {
                //mConnection.ExecuteNonQuery("insert into shifts(cableID) values(" + CableID + ")");
                //ShiftID = Convert.ToInt32(mConnection.ExecuteScalar("select max(shiftID) from shifts"));
                if (Convert.ToInt32(dbconfig.CableID) == 1)
                {
                    mConnection.ExecuteNonQuery("insert into shifts(cableID,RealTotalInSafeLastShift,staffID) values(" + CableID + ",'" + strRealSubtotal + "','" + frmLogin.intEmployeeID + "')");
                }
                ShiftID = Convert.ToInt32(mConnection.ExecuteScalar("select max(shiftID) from shifts"));
            }
        }

        private void InitShift(int intNoneEndOfShift)
        {
            object obj = mConnection.ExecuteScalar("select shiftID from (select shiftID from shifts where completed=0 and cableID=" + CableID + " union all select shiftID from shiftsall where completed=0 and cableID=" + CableID + ") as tmp order by shiftID desc limit 0,1");
            if (obj != null)
            {
                ShiftID = Convert.ToInt32(obj);
            }
            else
            {
                //mConnection.ExecuteNonQuery("insert into shifts(cableID) values(" + CableID + ")");
                //ShiftID = Convert.ToInt32(mConnection.ExecuteScalar("select max(shiftID) from shifts"));

                mConnection.ExecuteNonQuery("insert into shifts(cableID,RealTotalInSafeLastShift) values(" + CableID + ",'" + strRealSubtotal + "')");
                ShiftID = Convert.ToInt32(mConnection.ExecuteScalar("select max(shiftID) from shifts"));
            }
        }

        public void EndOfShift2(MoneyFortmat moneyFortmat, string ShiftID)
        {
            try
            {
                LoadCashInAndCashOut();
                LoadSaleDaily();
                LoadLastShift();
                mConnection = new Connection.Connection();
                mConnection.Open();
                mConnection.BeginTransaction();
                mConnection.ExecuteNonQuery("update shifts set completed=1,tsEnd=now(),ts=ts,subTotal="
                    + frmLogin.dblRealSubtotal.ToString() + ",CashOut = "
                    + frmLogin.dblRealCashOut.ToString() + ",RealTotalInSafeLastShift = "
                    + frmLogin.dblRealTotalInSafe.ToString() + ",CashIn = "
                    + frmLogin.dblRealCashIn.ToString() + ",RealTotalInSafe = "
                    //+((frmLogin.dblRealSubtotal - frmLogin.dblSubtotalLastShift)+frmLogin.dblRealTotalInSafe-frmLogin.dblRealCashOut+frmLogin.dblRealCashIn).ToString()
                    + (frmLogin.dblRealSubtotal + frmLogin.dblRealTotalInSafe - frmLogin.dblRealCashOut + frmLogin.dblRealCashIn).ToString() + ",CashFloat = "
                    + moneyFortmat.getFortMat(CashFloat) + ",CashDrop = "
                    + moneyFortmat.getFortMat(CashDrop)
                    + " where shiftID=" + ShiftID);
                strRealSubtotal = (frmLogin.dblRealSubtotal + frmLogin.dblRealTotalInSafe - frmLogin.dblRealCashOut + frmLogin.dblRealCashIn).ToString();
                InitShift();

                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private string strRealSubtotal;

        public void EndOfShift(MoneyFortmat moneyFortmat, Connection.ReadDBConfig dbconfig)
        {
            try
            {
                mConnection = new Connection.Connection();
                mConnection.Open();
                //if (Convert.ToBoolean(Convert.ToInt32(dbconfig.AllowSalesFastFood)))
                //{
                //    mConnection.ExecuteNonQuery("insert into cashinandcashout(EmployeeID,TotalAmount,CashType,Description,ShiftID) values('" +
                //        frmOrdersAll.mStaffID + "'," +
                //        moneyFortmat.getFortMat(FinalDrop) + ",2,'Final Drop'," +
                //        ShiftID + ")");
                //}
                LoadCashInAndCashOut();
                //LoadSaleDaily();
                LoadLastShift();
                mConnection.BeginTransaction();
                mConnection.ExecuteNonQuery("update shifts set completed=1,tsEnd=now(),ts=ts,subTotal="
                    + frmLogin.dblRealSubtotal.ToString() + ",CashOut = "
                    + frmLogin.dblRealCashOut.ToString() + ",RealTotalInSafeLastShift = "
                    + frmLogin.dblRealTotalInSafe.ToString() + ",CashIn = "
                    //+ frmLogin.dblRealTotalInSafeLastShift.ToString() + ",CashIn = "
                    + frmLogin.dblRealCashIn.ToString() + ",RealTotalInSafe = "
                    //+((frmLogin.dblRealSubtotal - frmLogin.dblSubtotalLastShift)+frmLogin.dblRealTotalInSafe-frmLogin.dblRealCashOut+frmLogin.dblRealCashIn).ToString()
                    + (frmLogin.dblRealSubtotal).ToString() + ",CashFloat = "
                    + moneyFortmat.getFortMat(CashFloat) + ",CashDrop = "
                    + moneyFortmat.getFortMat(CashDrop)
                    + " where shiftID=" + ShiftID);
                strRealSubtotal = (frmLogin.dblRealSubtotal).ToString();
                InitShift();
                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        public void EndOfShift(MoneyFortmat moneyFortmat)
        {
            try
            {
                //LoadCashInAndCashOut();
                //LoadSaleDaily();
                //LoadLastShift();
                //mConnection = new Connection.Connection();
                //mConnection.Open();
                //mConnection.BeginTransaction();
                //mConnection.ExecuteNonQuery("update shifts set completed=1,tsEnd=now(),ts=ts,subTotal="
                //    + frmLogin.dblRealSubtotal.ToString() +",CashOut = "
                //    + frmLogin.dblRealCashOut.ToString() + ",RealTotalInSafeLastShift = "
                //    + frmLogin.dblRealTotalInSafe.ToString() + ",CashIn = "
                //    + frmLogin.dblRealCashIn.ToString()+ ",RealTotalInSafe = "
                //    //+((frmLogin.dblRealSubtotal - frmLogin.dblSubtotalLastShift)+frmLogin.dblRealTotalInSafe-frmLogin.dblRealCashOut+frmLogin.dblRealCashIn).ToString()
                //    + (frmLogin.dblRealSubtotal + frmLogin.dblRealTotalInSafe - frmLogin.dblRealCashOut + frmLogin.dblRealCashIn).ToString()
                //    +" where shiftID=" + ShiftID);
                //strRealSubtotal = (frmLogin.dblRealSubtotal + frmLogin.dblRealTotalInSafe - frmLogin.dblRealCashOut + frmLogin.dblRealCashIn).ToString();
                //InitShift();
                //mConnection.Commit();

                //NEW UPDATE 02-02-2012
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                LoadCashInAndCashOut();
                //LoadSaleDaily();
                LoadLastShift();
                mConnection = new Connection.Connection();
                mConnection.Open();
                mConnection.BeginTransaction();
                mConnection.ExecuteNonQuery("update shifts set completed=1,tsEnd=now(),ts=ts,subTotal="
                    + frmLogin.dblRealSubtotal.ToString() + ",CashOut = "
                    + frmLogin.dblRealCashOut.ToString() + ",RealTotalInSafeLastShift = "
                    + frmLogin.dblRealTotalInSafe.ToString() + ",CashIn = "
                    //+ frmLogin.dblRealTotalInSafeLastShift.ToString() + ",CashIn = "
                    + frmLogin.dblRealCashIn.ToString() + ",RealTotalInSafe = "
                    //+((frmLogin.dblRealSubtotal - frmLogin.dblSubtotalLastShift)+frmLogin.dblRealTotalInSafe-frmLogin.dblRealCashOut+frmLogin.dblRealCashIn).ToString()
                    + (frmLogin.dblRealSubtotal).ToString() + ",CashFloat = "
                    + moneyFortmat.getFortMat(CashFloat) + ",CashDrop = "
                    + moneyFortmat.getFortMat(CashDrop)
                    + " where shiftID=" + ShiftID);
                strRealSubtotal = (frmLogin.dblRealSubtotal).ToString();
                InitShift();
                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadLastShift()
        {
            dtSource = new DataTable();
            dtSource = mConnection.Select("select subTotal,CashOut,CashIn,RealTotalInSafe,RealTotalInSafeLastShift from shifts order by shiftID DESC LIMIT 0,2");
            if (dtSource.Rows.Count == 2)
            {
                frmLogin.dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[1]["RealTotalInSafe"].ToString());
                frmLogin.dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[1]["subTotal"].ToString());
                frmLogin.dblRealTotalInSafeLastShift = Convert.ToDouble(dtSource.Rows[1]["RealTotalInSafeLastShift"].ToString());
            }
            else
                if (dtSource.Rows.Count == 1)
                {
                    frmLogin.dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafe"].ToString());
                    frmLogin.dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[0]["subTotal"].ToString());
                    frmLogin.dblRealTotalInSafeLastShift = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafeLastShift"].ToString());
                }
                else
                {
                    frmLogin.dblRealTotalInSafe = 0;
                    frmLogin.dblSubtotalLastShift = 0;
                    frmLogin.dblRealTotalInSafeLastShift = 0;
                }
        }

        private DataTable dtSource;

        private void LoadSaleDaily()
        {
            string sql = "select SUM(subTotal) AS Subtotal " +
                         "from ordersdaily " +
                         "where (completed=1 or completed=2 or completed=0) AND shiftID = " + ShiftID;
            dtSource = new DataTable();
            dtSource = mConnection.Select(sql);

            if (dtSource.Rows.Count != 0)
            {
                frmLogin.dblRealSubtotal = Convert.ToDouble(dtSource.Rows[0]["Subtotal"].ToString() == "" ? "0" : dtSource.Rows[0]["Subtotal"].ToString());
            }
        }

        private void LoadCashInAndCashOut()
        {
            string sql = "select SUM(CashIn) AS CashIn,SUM(CashOut) AS CashOut " +
                            "FROM " +
                            "( " +
                            "SELECT TotalAmount AS CashIn,0 AS CashOut FROM cashinandcashout c where CashType = 1 AND shiftID = " + ShiftID + " " +
                            "UNION ALL " +
                            "SELECT 0 AS CashIn,TotalAmount AS CashOut FROM cashinandcashout c where (CashType = 2 OR CashType = 3 OR CashType = 4 OR CashType = 5) AND shiftID = " + ShiftID + " " +
                            ")AS Source";
            dtSource = new DataTable();
            dtSource = mConnection.Select(sql);

            if (dtSource.Rows.Count != 0)
            {
                frmLogin.dblRealCashIn = Convert.ToDouble(dtSource.Rows[0]["CashIn"].ToString() == "" ? 0 : dtSource.Rows[0]["CashIn"]);
                frmLogin.dblRealCashOut = Convert.ToDouble(dtSource.Rows[0]["CashOut"].ToString() == "" ? 0 : dtSource.Rows[0]["CashOut"]);
            }
            else
            {
                frmLogin.dblRealCashIn = 0;
                frmLogin.dblRealCashOut = 0;
            }
        }
    }
}