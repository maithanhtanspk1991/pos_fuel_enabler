﻿namespace POS.Class
{
    public class Bulk
    {
        public int BulkID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Size { get; set; }

        public int SupplierID { get; set; }

        public Bulk(int bulkID, string name, string desc, int size, int supplierID)
        {
            BulkID = bulkID;
            Name = name;
            Description = desc;
            Size = size;
            SupplierID = supplierID;
        }
    }
}