﻿namespace POS.Class
{
    public class SubItemline
    {
        public int subitemlineID { get; set; }

        public string subitemlineName { get; set; }

        public SubItemline(int id, string name)
        {
            subitemlineID = id;
            subitemlineName = name;
        }
    }
}