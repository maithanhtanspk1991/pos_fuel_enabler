﻿using System;

namespace POS.Class
{
    public class CustomerDisplayVFD
    {
        private string CLEARSCREEN = (char)12 + "";
        private string PRINTLINE1 = "" + (char)31 + (char)36 + (char)1 + (char)1;
        private string PRINTLINE2 = "" + (char)31 + (char)36 + (char)1 + (char)2;
        private System.IO.Ports.SerialPort mSerialPort;
        private Connection.clsReadAndWriteINI mInit;
        private string mPortName;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private int mbaudRate;

        public CustomerDisplayVFD()
        {
            string path = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            path += "\\config.ini";
            // mInit = new Connection.clsReadAndWriteINI("config.ini");
            mInit = new Connection.clsReadAndWriteINI(path);
            mPortName = mInit.ReadValue("CustomerDisplayVFD", "Comport");
            if (mPortName == null || mPortName == "")
            {
                mPortName = "COM4";
            }
            mInit.WriteValue("CustomerDisplayVFD", "Comport", mPortName);

            try
            {
                mbaudRate = Convert.ToInt32(mInit.ReadValue("CustomerDisplayVFD", "mbaudRate"));
            }
            catch
            {
                mbaudRate = 9600;
                mInit.WriteValue("CustomerDisplayVFD", "mbaudRate", mbaudRate.ToString());
            }

            mSerialPort = new System.IO.Ports.SerialPort(mPortName, mbaudRate, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            mSerialPort.Open();
        }

        public void ClearScreen()
        {
            mSerialPort.WriteLine(CLEARSCREEN);
        }

        public void DisplayWelcome()
        {
            PrintLine1("WELCOME TO");
            string str = dbconfig.Header1.ToString();
            string str1 = str;
            if (str.Length > 20)
                str1 = str.Substring(0, 20);
            PrintLine2(str1);
        }

        public void WriteLine(string line1, string line2)
        {
            try
            {
                ClearScreen();
                PrintLine1(line1);
                PrintLine2(line2);
            }
            catch (Exception)
            {
            }
        }

        public void PrintLine1(string data)
        {
            mSerialPort.WriteLine(PRINTLINE1 + data);
        }

        public void PrintLine2(string data)
        {
            mSerialPort.WriteLine(PRINTLINE2 + data);
        }

        public void ClosePort()
        {
            try
            {
                if (mSerialPort.IsOpen)
                {
                    mSerialPort.Close();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}