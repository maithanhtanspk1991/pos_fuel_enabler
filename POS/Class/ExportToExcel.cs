﻿using System.Collections;
using System.Data;
using System.IO;
using System.Web.UI;

namespace POS.Class
{
    internal class ExportToExcel
    {
        public void dataTable2Excel(DataTable dt, string fileName)
        {
            dt = plusSingleQuote(dt);
            //DataSet ds = new DataSet();
            //ds.Tables.Add(dt);
            System.Web.UI.WebControls.DataGrid grid = new System.Web.UI.WebControls.DataGrid();
            grid.HeaderStyle.Font.Bold = true;
            grid.DataSource = dt;
            grid.DataMember = dt.TableName;
            grid.DataBind();
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    grid.RenderControl(hw);
                }
            }
        }

        private static DataTable plusSingleQuote(DataTable dt)
        {
            ArrayList arr = new ArrayList();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Columns[i].DataType == typeof(string))
                {
                    arr.Add(i);
                }
            }
            if (arr == null)
            {
                return dt;
            }
            int rowCount = dt.Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < arr.Count; j++)
                {
                    int colIndex = (int)arr[j];
                    dt.Rows[i][colIndex] = " " + dt.Rows[i][colIndex];
                }
            }
            return dt;
        }
    }
}