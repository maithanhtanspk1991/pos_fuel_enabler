﻿namespace POS.Class
{
    internal class Company
    {
        public int CompanyID { get; set; }

        public string CompanyName { get; set; }

        public string AccountLimit { get; set; }

        public string Debt { get; set; }

        public Company(int companyID, string companyName, string accountLimit, string debt)
        {
            CompanyID = companyID;
            CompanyName = companyName;
            AccountLimit = accountLimit;
            Debt = debt;
        }
    }
}