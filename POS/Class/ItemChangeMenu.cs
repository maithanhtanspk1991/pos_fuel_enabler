﻿namespace POS.Class
{
    internal class ItemChangeMenu
    {
        public int groupid { get; set; }

        public string groupdesc { get; set; }

        public string groupshort { get; set; }

        public int groupshortcut { get; set; }

        public int groupoption { get; set; }

        public int groupdisplay { get; set; }

        public int groupvisible { get; set; }

        public ItemChangeMenu(int gid, string desc, string shor, int scut, int op, int dp, int vi)
        {
            groupid = gid;
            groupdesc = desc;
            groupshort = shor;
            groupshortcut = scut;
            groupoption = op;
            groupdisplay = dp;
            groupvisible = vi;
        }
    }
}