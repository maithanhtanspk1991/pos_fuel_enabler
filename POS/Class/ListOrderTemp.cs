﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class ListOrderTemp
    {
        private List<Class.ProcessOrderNew.Order> listOrderTemp;
        private int mCurren = 0;

        public ListOrderTemp()
        {
            listOrderTemp = new List<Class.ProcessOrderNew.Order>();
        }

        public void AddOrder(Class.ProcessOrderNew.Order order)
        {
            if (!listOrderTemp.Contains(order))
            {
                listOrderTemp.Add(order);
            }
        }

        public int CountListOrderTemp()
        {
            return listOrderTemp.Count;
        }
        public Class.ProcessOrderNew.Order CurrenOrder()
        {
            if (listOrderTemp.Count > 0 && mCurren < listOrderTemp.Count)
            {
                return listOrderTemp[mCurren];
            }
            else
            {
                return null;
            }
        }

        public Class.ProcessOrderNew.Order NextOrder()
        {
            if (listOrderTemp.Count > 0)
            {
                mCurren++;
                if (mCurren >= listOrderTemp.Count)
                {
                    mCurren = 0;
                }
                return listOrderTemp[mCurren];
            }
            return null;
        }

        public void RemoveOrdrt(Class.ProcessOrderNew.Order order)
        {
            if (listOrderTemp.Contains(order))
            {
                listOrderTemp.Remove(order);
            }
        }
    }
}
