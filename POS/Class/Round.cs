﻿using System;

namespace POS.Class
{
    public class Round
    {
        public static double RoundSubTotal(double value)
        {
            return Math.Round(value, 3);
            //value = Math.Abs(value);
            //int num = (int)(value * 100);
            //int a = num / 10;
            //int b = num % 10;
            //switch (b)
            //{
            //    case 0: b = 0; break;
            //    case 1: b = 0; break;
            //    case 2: b = 0; break;
            //    case 3: b = 5; break;
            //    case 4: b = 5; break;
            //    case 5: b = 5; break;
            //    case 6: b = 5; break;
            //    case 7: b = 5; break;
            //    case 8: b = 10; break;
            //    case 9: b = 10; break;
            //    default:
            //        break;
            //}
            //return ((double)a * 10 + b) / 100;
        }

        public static double Round3to2(double value)
        {
            return Math.Round(value, 3);
            //value = value / 10;
            //return RoundValue(value) * 10;
        }

        public static double RoundValue(double value)
        {
            return Math.Round(value, 3);
            //value = Math.Abs(value);
            //int num = (int)(value);
            //int a = num / 10;
            //int b = num % 10;
            //switch (b)
            //{
            //    case 0: b = 0; break;
            //    case 1: b = 0; break;
            //    case 2: b = 0; break;
            //    case 3: b = 5; break;
            //    case 4: b = 5; break;
            //    case 5: b = 5; break;
            //    case 6: b = 5; break;
            //    case 7: b = 5; break;
            //    case 8: b = 10; break;
            //    case 9: b = 10; break;
            //    default:
            //        break;
            //}
            ////return (a + b)/100;
            //return ((double)a * 10 + b);
        }
    }
}