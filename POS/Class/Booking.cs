﻿using System;

namespace POS.Class
{
    internal class Booking
    {
        public int BookID { get; set; }

        public int CustID { get; set; }

        public System.DateTime CurentDate { get; set; }

        public System.DateTime BookDate { get; set; }

        public int SubTotal { get; set; }

        public int BookDeposit { get; set; }

        public int Person { get; set; }

        public int BookType { get; set; }

        public Booking(int bookid, int custid, DateTime curent, DateTime bookdate, int subtotal, int deposit, int person, int booktype)
        {
            BookID = bookid;
            CustID = custid;
            CurentDate = curent;
            BookDate = bookdate;
            SubTotal = subtotal;
            BookDeposit = deposit;
            Person = person;
            BookType = booktype;
        }

        public Booking(int bookid, int custid, DateTime bookdate, int person, int booktype)
        {
            BookID = bookid;
            CustID = custid;
            BookDate = bookdate;
            Person = person;
            BookType = booktype;
        }
    }
}