﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class Fuel
    {
        public int FuelID { get; set; }
        public string FuelName { get; set; }
        public double Price { get; set; }
        public int GST { get; set; }

        public Fuel(int fuelid, string fuelname, double price, int gst)
        {
            FuelID = fuelid;
            FuelName = fuelname;
            Price = price;
            GST = gst;
        }
    }
}
