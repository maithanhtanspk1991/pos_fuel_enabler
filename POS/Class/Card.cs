﻿namespace POS.Class
{
    public class Card
    {
        public int CardID { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Description { get; set; }

        public int TypeCard { get; set; }

        public int Checked { get; set; }

        public int IsSurchart { get; set; }

        public int PercentSurchart { get; set; }
    }
}