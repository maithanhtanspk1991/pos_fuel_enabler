﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Class
{
    public class ItemClick
    {
        public ItemClick(int itemIndex, int itemOptionIndex)
        {
            ItemIndex = itemIndex;
            ItemOptionIndex = itemOptionIndex;
        }

        public int ItemIndex { get; set; }

        public int ItemOptionIndex { get; set; }
    }
}
