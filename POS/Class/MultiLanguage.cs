﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace POS.Class
{
    public class MultiLanguage
    {
        private Class.ReadConfig mReadconfig;
        private clsReadAndWriteXml xml;
        private string sPath;

        public MultiLanguage()
        {
            mReadconfig = new ReadConfig();
            sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            if (mReadconfig.LanguageCode == "en")
            {
                sPath += "\\MultiLanguage\\en.xml";
            }
            else if (mReadconfig.LanguageCode == "vi")
            {
                sPath += "\\MultiLanguage\\vi.xml";
            }           
            xml = new clsReadAndWriteXml(sPath, "Language");
            ReadLanguage();
        }
        #region
        /// <summary>
        /// Đọc file xml Language        
        /// </summary>
        /// <param name="Element">Section</param>
        /// <param name="Attribute">Key</param>
        /// <param name="value">Giá trị mặc định</param>
        /// <param name="xml">File xml Language</param>
        /// <returns>Trả về kiểu string</returns>
        private string Read(string Element, string Attributes, string value, clsReadAndWriteXml xml)
        {
            string result = xml.ReadXml(Element, Attributes);
            if (result == "")
            {
                result = value;
                xml.WriteXml(Element, Attributes, value);
            }
            return result;
        }
        #endregion

        private void ReadLanguage()
        {
            switch (mReadconfig.LanguageCode)
            {
                case "en":
                    frmLogin_btnLogin = Read("frmLogin", "btnLogin", "OK", xml);
                    frmLogin_btnLicense = Read("frmLogin", "btnLicense", "LICENSE", xml);
                    frmLogin_btnExit = Read("frmLogin", "btnExit", "EXIT", xml);
                    frmLogin_btnClear = Read("frmLogin", "btnClear", "CLEAR", xml);
                    frmLogin_lbPass = Read("frmLogin", "lbPass", "PIN :", xml);
                    frmLogin_lbStatus = Read("frmLogin", "lbStatus", "Please Sign On", xml);
                    frmLogin_lbUser = Read("frmLogin", "lbUser", "Staff ID:", xml);
                break;
                case "vi":          

                    frmLogin_btnLogin = Read("frmLogin", "btnLogin", "OK", xml);
                    frmLogin_btnLicense = Read("frmLogin", "btnLicense", "BẢN QUYỀN", xml);
                    frmLogin_btnExit = Read("frmLogin", "btnExit", "THOÁT", xml);
                    frmLogin_btnClear = Read("frmLogin", "btnClear", "XÓA", xml);
                    frmLogin_lbPass = Read("frmLogin", "lbPass", "MẬT KHẨU", xml);
                    frmLogin_lbStatus = Read("frmLogin", "lbStatus", "ĐĂNG NHẬP", xml);
                    frmLogin_lbUser = Read("frmLogin", "lbUser", "TÀI KHOẢN", xml);
                break;
            }
        }
        public string frmLogin_btnLogin { get; set; }
        public string frmLogin_btnExit { get; set; }
        public string frmLogin_btnLicense { get; set; }
        public string frmLogin_btnClear { get; set; }
        public string frmLogin_lbUser { get; set; }
        public string frmLogin_lbPass { get; set; }
        public string frmLogin_lbStatus { get; set; }

    }
    public class clsReadAndWriteXml
    {
        string sPath;
        string Root;
        XmlDocument xDocument = new XmlDocument();
        public clsReadAndWriteXml(string Path,string rootName)
        {
            sPath = Path;
            Root = rootName;
            if (!File.Exists(sPath))
            {
                xDocument.LoadXml("<" + rootName + "></" + rootName + ">");
                xDocument.Save(sPath);
            }
        }
        public void WriteXml(string nodeName,string Attname,string value)
        {
            XmlNode xNode = null;
            xDocument.Load(sPath);           
            XmlNode xRoot = xDocument.SelectSingleNode("/" + this.Root);
            xNode = xDocument.SelectSingleNode("//" + nodeName);
            if (xNode == null)
            {
                xNode = xDocument.CreateNode(XmlNodeType.Element, nodeName, null);
                XmlAttribute xAtt = xDocument.CreateAttribute(Attname);
                xAtt.Value = value;
                xNode.Attributes.Append(xAtt);
                xRoot.AppendChild(xNode);
            }
            else
            {
                XmlNode xNodeOld = xNode;
                XmlAttribute xAtt = null;
                if (xNode.Attributes[Attname] == null)
                {
                    xAtt = xDocument.CreateAttribute(Attname);
                    xAtt.Value = value;
                    xNode.Attributes.Append(xAtt);
                }
                else
                {
                    xNode.Attributes[Attname].Value = value;                   
                }                
                xRoot.ReplaceChild(xNode, xNodeOld);
            }
            xDocument.Save(sPath);
        }
        public string ReadXml(string nodeName,string Attname)
        {
            string value = "";
            XmlNode xNode = null;
            xDocument.Load(sPath);
            xNode = xDocument.SelectSingleNode("//" + nodeName);
            if (xNode == null)
            {
                value = "";
            }
            else
            {
                XmlAttribute xAtt = null;
                xAtt = xNode.Attributes[Attname];
                if (xAtt == null)
                {
                    value = "";
                }
                else
                {
                    value = xNode.Attributes[Attname].Value; 
                }                
            }
            return value;
        }
    }
}
