﻿using System;
using System.Collections.Generic;
using System.Data;

namespace POS.Class
{
    public class ProcessOrderNew
    {
        private string _nameClass = "POS::Class::ProcessOrderNew";
        private string[] arrayDataReceive;
        private Connection.Connection connection;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private double dblSubtotal;
        private DataTable dtSource;
        private MoneyFortmat money = new MoneyFortmat(1);
        private BarPrinterServer printServer;

        public ProcessOrderNew(Connection.Connection con, BarPrinterServer print)
        {
            connection = con;
            printServer = print;
        }

        public bool IsNewOrder { get; set; }

        public void billOrder(Order order)
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.billOrder.");
            try
            {
                connection.Open();
                connection.BeginTransaction();
                printServer.printBuilt(true, order);
                if (order.Completed == 0)
                {
                    order.Completed = 3;
                    connection.ExecuteNonQuery("update ordersdaily set completed=3 where orderID=" + order.OrderID);
                }
                connection.Commit();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.billOrder" + ex.Message);
                connection.Rollback();
            }
            finally
            {
                connection.Close();
            }
        }

        public bool closeOrder(Order order)
        {
            bool resuilt = false;
            if (order.Completed == 2)
            {
                try
                {
                    order.Completed = 1;
                    connection.Open();
                    connection.ExecuteNonQuery("update ordersdaily set completed=" + order.Completed + " where orderID=" + order.OrderID);
                    resuilt = true;
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("ProcessOrderNew.closeOrder:::" + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return resuilt;
        }

        public void DiscountOrder(Order order)
        {
            try
            {
                connection.Open();
                connection.BeginTransaction();
                connection.ExecuteNonQuery("update ordersdaily set subTotal=" + (order.getSubTotal() + money.getFortMat(order.DeliveryFee)) + ",discount=" + order.Discount + ",clients=" + order.NumPeople + " where orderID=" + order.OrderID);
                connection.ExecuteNonQuery("delete from ordersdailyline where orderID=" + order.OrderID);
                foreach (Item item in order.ListItem)
                {
                    if (connection.ExecuteNonQuery("insert into ordersdailyline(itemID,qty,price,subTotal,orderID,pumpID,pumpHistoryID) values(" +
                                                    item.ItemID + "," +
                                                    item.Qty + "," +
                                                    item.Price + "," +
                                                    item.getTotal() + "," +
                                                    order.OrderID + "," +
                                                    item.PumID + "," +
                                                    item.IDPumpHistory +
                                                    ")") != 1)
                    {
                        throw new Exception();
                    }
                    foreach (SubItem subItem in item.ListSubItem)
                    {
                        if (connection.ExecuteNonQuery("insert into ordersdailyline(optionID,qty,price,subTotal,orderID,pumpID,pumpHistoryID) values(" +
                                                    subItem.ItemID + "," +
                                                    subItem.Qty + "," +
                                                    subItem.Price + "," +
                                                    subItem.SubTotal + "," +
                                                    order.OrderID + "," +
                                                    item.PumID + "," +
                                                    item.IDPumpHistory +
                                                    ")") != 1)
                        {
                            throw new Exception();
                        }
                    }
                }
                connection.Commit();
            }
            catch (Exception exs)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.Send Old Order:::" + exs.Message);
                connection.Rollback();
            }
            finally
            {
                connection.Close();
            }
        }

        public int GetitemIDbyName(string name)
        {
            int itemID = 0;           
            try
            {               
                connection.Open();
                string sql = "select itemID from itemsmenu where isFuel = 1 and itemShort = '" + name + "'";
                itemID = Convert.ToInt32(connection.ExecuteScalar(sql));
            }
            catch(Exception exs)
            {
                itemID = 0;
                Class.LogPOS.WriteLog("ProcessOrderNew.Send Old Order:::" + exs.Message);
            }
            finally
            {
                connection.Close();
            }
            return itemID;
        }

        public Order GetLastOrder(Class.MoneyFortmat money)
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.GetLastOrder.");
            Connection.Connection con = new Connection.Connection();
            Order order = null;
            try
            {
                string sql = "SELECT * FROM ordersdaily o where orderID=(SELECT max(orderID) FROM ordersdaily)";
                con.Open();
                DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    order = new Order();
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.ChangeByCash = Convert.ToDouble(tbl.Rows[0]["changebycash"]);
                    order.ShiftID = Convert.ToInt32(tbl.Rows[0]["shiftID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Change = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.ChangeAmount = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.Tendered = order.Cash + order.ChangeByCash;
                    order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"]);
                    order.Gst = Convert.ToDouble(tbl.Rows[0]["gst"]);
                    order.Customer.CustID = Convert.ToInt32(tbl.Rows[0]["custID"]);
                    order.OrderByCardID = tbl.Rows[0]["orderbycardID"].ToString();
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Surchart = Convert.ToDouble(tbl.Rows[0]["Surchart"]);
                    order.DeliveryFee = Convert.ToDouble(tbl.Rows[0]["FeeDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                    order.IsPreOrder = true;
                    System.Data.DataTable tblItem = con.Select(
                        "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil, " +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + order.OrderID);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Item item = new Item(0,
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Item item = new Item(0,
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                order.ListItem.Add(item);
                            }
                            else
                            {
                                SubItem subitem = new SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                            }
                        }
                    }
                }
                System.Data.DataTable tblCard = con.Select("SELECT obc.orderbycardID AS orderbycardID,obc.cardID AS cardID,obc.subtotal AS subtotal,obc.cashOut as cashOut,obc.orderID AS orderID,c.cardName FROM orderbycard as obc inner join card as c on obc.cardID = c.cardID where obc.ID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                if (tblCard.Rows.Count > 0)
                {
                    order.CashOut = Convert.ToDouble(tblCard.Rows[0]["cashOut"]);
                    order.MoreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.MoreOrderByCard.Add(new OrderByCard()
                        {
                            orderbycardID = drRow["orderbycardID"].ToString(),
                            cardID = Convert.ToInt32(drRow["cardID"].ToString()),
                            subtotal = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["subtotal"].ToString()))),
                            cashOut = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["cashOut"].ToString()))),
                            orderID = Convert.ToInt32(drRow["orderID"].ToString()),
                            nameCard = drRow["cardName"].ToString()
                        }
                        );
                    }
                }
                try
                {
                    tblCard = con.Select("SELECT obc.AuthCode,obc.AccountType,obc.Pan,obc.DateExpiry,obc.Date,obc.Time,obc.CardType,IF(obc.AmtPurchase IS NULL,0,obc.AmtPurchase) AS AmtPurchase,IF(obc.AmtCash IS NULL,0,obc.AmtCash) AS AmtCash FROM orderbycard as obc where obc.orderbycardID = '" + order.OrderByCardID + "'");
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    if (tblCard.Rows.Count > 0)
                    {
                        foreach (DataRow drRow in tblCard.Rows)
                        {
                            order.AuthCode = drRow["AuthCode"].ToString();
                            order.CardType = drRow["CardType"].ToString();
                            order.AccountType = drRow["AccountType"].ToString();
                            order.Pan = drRow["Pan"].ToString();
                            order.DateExpiry = drRow["DateExpiry"].ToString();
                            order.Date = drRow["Date"].ToString();
                            order.Time = drRow["Time"].ToString();
                            order.AmtPurchase = Convert.ToDecimal(drRow["AmtPurchase"]);
                            order.AmtCash = Convert.ToDecimal(drRow["AmtCash"]);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.GetLastOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return order;
        }

        public Order GetLastOrderByID(Class.MoneyFortmat money, int OrderID)
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.GetLastOrder.");
            Connection.Connection con = new Connection.Connection();
            Order order = null;
            try
            {
                string sql = "SELECT * FROM ordersdaily o where orderID=" + OrderID;
                con.Open();
                DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    order = new Order();
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.ChangeByCash = Convert.ToDouble(tbl.Rows[0]["changebycash"]);
                    order.ShiftID = Convert.ToInt32(tbl.Rows[0]["shiftID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Change = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.ChangeAmount = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.Tendered = order.Cash + order.ChangeByCash;
                    order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"]);
                    order.Gst = Convert.ToDouble(tbl.Rows[0]["gst"]);
                    order.Customer.CustID = Convert.ToInt32(tbl.Rows[0]["custID"]);
                    order.CustCode = tbl.Rows[0]["CustCode"].ToString();
                    order.OrderByCardID = tbl.Rows[0]["orderbycardID"].ToString();
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Surchart = Convert.ToDouble(tbl.Rows[0]["Surchart"]);
                    order.DeliveryFee = Convert.ToDouble(tbl.Rows[0]["FeeDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    System.Data.DataTable tblItem = con.Select(
                        "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil, " +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + order.OrderID);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Item item = new Item(0,
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Item item = new Item(0,
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                order.ListItem.Add(item);
                            }
                            else
                            {
                                SubItem subitem = new SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                            }
                        }
                    }
                }
                System.Data.DataTable tblCard = con.Select("SELECT obc.orderbycardID AS orderbycardID,obc.cardID AS cardID,obc.subtotal AS subtotal,obc.cashOut as cashOut,obc.orderID AS orderID,c.cardName FROM orderbycard as obc inner join card as c on obc.cardID = c.cardID where obc.orderbycardID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CashOut = Convert.ToDouble(tblCard.Rows[0]["cashOut"]);
                if (tblCard.Rows.Count > 0)
                {
                    order.MoreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.MoreOrderByCard.Add(new OrderByCard()
                        {
                            orderbycardID = drRow["orderbycardID"].ToString(),
                            cardID = Convert.ToInt32(drRow["cardID"].ToString()),
                            subtotal = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["subtotal"].ToString()))),
                            cashOut = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["cashOut"].ToString()))),
                            orderID = Convert.ToInt32(drRow["orderID"].ToString()),
                            nameCard = drRow["cardName"].ToString()
                        }
                        );
                    }
                }
                try
                {
                    tblCard = con.Select("SELECT obc.AuthCode,obc.AccountType,obc.Pan,obc.DateExpiry,obc.Date,obc.Time,obc.CardType,IF(obc.AmtPurchase IS NULL,0,obc.AmtPurchase) AS AmtPurchase,IF(obc.AmtCash IS NULL,0,obc.AmtCash) AS AmtCash FROM orderbycard as obc where obc.orderbycardID = '" + order.OrderByCardID + "'");
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    if (tblCard.Rows.Count > 0)
                    {
                        foreach (DataRow drRow in tblCard.Rows)
                        {
                            order.AuthCode = drRow["AuthCode"].ToString();
                            order.CardType = drRow["CardType"].ToString();
                            order.AccountType = drRow["AccountType"].ToString();
                            order.Pan = drRow["Pan"].ToString();
                            order.DateExpiry = drRow["DateExpiry"].ToString();
                            order.Date = drRow["Date"].ToString();
                            order.Time = drRow["Time"].ToString();
                            order.AmtPurchase = Convert.ToDecimal(drRow["AmtPurchase"]);
                            order.AmtCash = Convert.ToDecimal(drRow["AmtCash"]);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.GetLastOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return order;
        }

        public Order getOrderByOrderID(int orderID)
        {
            if (orderID <= 0)
            {
                return null;
            }
            Order order = new Order();
            order.OrderID = orderID;
            try
            {
                connection.Open();
                connection.BeginTransaction();
                order.OrderID = orderID;
                System.Data.DataTable tbl = connection.Select("select * from ordersdaily where orderID=" + order.OrderID);
                if (tbl.Rows.Count > 0)
                {
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                }
                tbl = connection.Select("select ol.itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName,ol.pumpID from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID=0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["itemID"].ToString() != "0")
                    {
                        order.ListItem.Add(new Item(0,
                        Convert.ToInt32(row["itemID"]),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"]),
                        Convert.ToDouble(row["subTotal"]),
                        Convert.ToDouble(row["price"]),
                        Convert.ToDouble(row["gst"]),
                        0,
                        0,
                        row["pumpID"].ToString()
                        ));
                    }
                    else
                    {
                        order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(new SubItem(
                            Convert.ToInt32(row["optionID"].ToString()),
                            row["optionName"].ToString(),
                            Convert.ToInt32(row["qty"].ToString()),
                            Convert.ToDouble(row["subTotal"].ToString()),
                            Convert.ToDouble(row["price"].ToString())
                            ));
                    }
                }
                tbl = connection.Select("select ol.dynID as itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from dynitemsmenu i where i.dynID=ol.dynID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName,ol.pumpID from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID<>0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    Item item = new Item(0,
                        Convert.ToInt32(row["itemID"].ToString()),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"].ToString()),
                        Convert.ToDouble(row["subTotal"].ToString()),
                        Convert.ToDouble(row["price"].ToString()),
                        0,
                        0,
                        0,
                        row["pumpID"].ToString()
                        );
                    item.ItemType = 1;
                    order.addItem(item);
                }
                //order.shortItem();
                return order;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getOrderByOrderID:::" + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return null;
        }

        public Order GetOrderByOrderID(System.Windows.Forms.ListView listView, int orderID, Class.MoneyFortmat money)
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.GetOrderByOrderID.");
            Order order = null;
            Connection.Connection con = new Connection.Connection();
            try
            {
                con.Open();
                //order.OrderID = Convert.ToInt32(connection.ExecuteScalar("select max(orderID) from ordersdaily where completed=1 and (" + order.OrderID + "=0 or orderID<" + order.OrderID + ")").ToString());
                System.Data.DataTable tbl = con.Select("select * from ordersdaily where orderID=" + orderID);
                if (tbl.Rows.Count > 0)
                {
                    order = new Order();
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.ChangeByCash = Convert.ToDouble(tbl.Rows[0]["changebycash"]);
                    order.ShiftID = Convert.ToInt32(tbl.Rows[0]["shiftID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Change = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.ChangeAmount = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.Tendered = order.Cash + order.ChangeByCash;
                    order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"]);
                    order.Gst = Convert.ToDouble(tbl.Rows[0]["gst"]);
                    order.Customer.CustID = Convert.ToInt32(tbl.Rows[0]["custID"]);
                    order.OrderByCardID = tbl.Rows[0]["orderbycardID"].ToString();
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);

                    order.CustCode = tbl.Rows[0]["CustCode"].ToString();
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);

                    System.Data.DataTable tblItem = con.Select(
                        "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "ol.gst," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil, " +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +

                        "where ol.orderID=" + order.OrderID);

                    order.ClearItemToListAndListView(listView);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Item item = new Item(0,
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                Convert.ToDouble(row["gst"]),
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                            //order.ListItem.Add(item);
                            order.AddItemNoCheck(item, listView, money, true);
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Item item = new Item(0,
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                Convert.ToDouble(row["gst"]),
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                order.AddItemNoCheck(item, listView, money, true);
                            }
                            else
                            {
                                SubItem subitem = new SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                                order.InsertSubItemListView(order.ListItem[order.ListItem.Count - 1], subitem, -1, listView, money, true);
                            }
                        }
                    }
                }
                System.Data.DataTable tblCard = con.Select("SELECT obc.orderbycardID AS orderbycardID,obc.cardID AS cardID,obc.subtotal AS subtotal,obc.cashOut as cashOut,obc.orderID AS orderID,c.cardName FROM orderbycard as obc inner join card as c on obc.cardID = c.cardID where obc.orderbycardID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CashOut = Convert.ToDouble(tblCard.Rows[0]["cashOut"]);
                if (tblCard.Rows.Count > 0)
                {
                    order.MoreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.MoreOrderByCard.Add(new OrderByCard()
                        {
                            orderbycardID = drRow["orderbycardID"].ToString(),
                            cardID = Convert.ToInt32(drRow["cardID"].ToString()),
                            subtotal = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["subtotal"].ToString()))),
                            cashOut = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["cashOut"].ToString()))),
                            orderID = Convert.ToInt32(drRow["orderID"].ToString()),
                            nameCard = drRow["cardName"].ToString()
                        }
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return order;
            //return order;
        }

        public Order getOrderByTable(string tableID)
        {
            try
            {
                Order order = new Order();
                System.Data.DataTable tbl = connection.Select("select * from ordersdaily where completed<>1 and tableID=" + "'" + tableID + "'" + " order by orderID limit 0,1");
                order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"].ToString());
                order.TableID = tableID;
                order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"].ToString());
                order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"].ToString());
                order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"].ToString());
                order.CustCode = tbl.Rows[0]["CustCode"].ToString();
                tbl = connection.Select("select ol.dynID,ol.itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,ol.gst,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName,ol.pumpID from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID=0");
                //Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                //Console.WriteLine("select ol.dynID,ol.itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID=0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["itemID"].ToString() != "0")
                    {
                        order.addItem(new Item(0,
                        Convert.ToInt32(row["itemID"]),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"]),
                        Convert.ToDouble(row["subTotal"]),
                        Convert.ToDouble(row["price"]),
                        Convert.ToDouble(row["gst"]),
                        0,
                        0,
                        row["pumpID"].ToString()
                        ));
                        /*
                        order.ListItem.Add(new Item(
                        Convert.ToInt32(row["itemID"].ToString()),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"].ToString()),
                        Convert.ToDouble(row["subTotal"].ToString()),
                        Convert.ToDouble(row["price"].ToString()),
                        0,
                        0
                        ));
                         * */
                    }
                    else
                    {
                        /*
                        order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(new SubItem(
                            Convert.ToInt32(row["optionID"].ToString()),
                            row["optionName"].ToString(),
                            Convert.ToInt32(row["qty"].ToString()),
                            Convert.ToDouble(row["subTotal"].ToString()),
                            Convert.ToDouble(row["price"].ToString())
                            ));
                         */
                        order.ListItem[order.ListItem.Count - 1].addSubItem(new SubItem(
                            Convert.ToInt32(row["optionID"].ToString()),
                            row["optionName"].ToString(),
                            Convert.ToInt32(row["qty"].ToString()),
                            Convert.ToDouble(row["subTotal"].ToString()),
                            Convert.ToDouble(row["price"].ToString())
                            ));
                    }
                }
                tbl = connection.Select("select ol.dynID as itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from dynitemsmenu i where i.dynID=ol.dynID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID<>0");
                //Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                //Console.WriteLine("select ol.dynID as itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from dynitemsmenu i where i.dynID=ol.dynID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName from ordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID<>0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    Item item = new Item(0,
                        Convert.ToInt32(row["itemID"].ToString()),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"].ToString()),
                        Convert.ToDouble(row["subTotal"].ToString()),
                        Convert.ToDouble(row["price"].ToString()),
                        0,
                        0,
                        0, "0"
                        );
                    item.ItemType = 1;
                    //order.addItem(item);
                    order.ListItem.Add(item);
                }
                //order.shortItem();
                return order;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getOrderByTable:::" + ex.Message);
            }
            return null;
        }

        public Order getPrevOrder(System.Windows.Forms.Button btn, System.Windows.Forms.ListView listView, Class.MoneyFortmat money)
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder.");
            Connection.Connection con = new Connection.Connection();
            Order order = null;
            int index = 0;
            if (btn.Tag != null)
            {
                index = Convert.ToInt16(btn.Tag);
                if (index > 9)
                {
                    index = 0;
                }
                else
                {
                    index++;
                }
            }
            try
            {
                con.Open();
                //order.OrderID = Convert.ToInt32(connection.ExecuteScalar("select max(orderID) from ordersdaily where completed=1 and (" + order.OrderID + "=0 or orderID<" + order.OrderID + ")").ToString());
                int count = Convert.ToInt32(con.ExecuteScalar("select count(orderID) from (select orderID from ordersdaily where (completed = 1 || completed = 2) order by ts desc limit 0,10) as tmp"));
                if (btn.Tag == null && count > 0)
                {
                    index = 0;
                }
                if (index >= count)
                {
                    index = 0;
                }
                System.Data.DataTable tbl = con.Select("select * from (select * from ordersdaily where (completed = 1 || completed = 2) order by ts desc limit 0,10) as tmp order by ts desc limit " + index + ",1");
                if (tbl.Rows.Count > 0)
                {
                    order = new Order();
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.ChangeByCash = Convert.ToDouble(tbl.Rows[0]["changebycash"]);
                    order.ShiftID = Convert.ToInt32(tbl.Rows[0]["shiftID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Change = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.ChangeAmount = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.Tendered = order.Cash + order.ChangeByCash;
                    order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"]);
                    order.Gst = Convert.ToDouble(tbl.Rows[0]["gst"]);
                    order.Customer.CustID = Convert.ToInt32(tbl.Rows[0]["custID"]);
                    order.OrderByCardID = tbl.Rows[0]["orderbycardID"].ToString();
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Surchart = Convert.ToDouble(tbl.Rows[0]["Surchart"]);
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    order.DeliveryFee = Convert.ToDouble(tbl.Rows[0]["FeeDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                    order.CustCode = tbl.Rows[0]["CustCode"].ToString();
                    order.IsPreOrder = true;
                    if (order.Delivery != 0)
                    {
                        order.Paid = 1;
                    }
                    System.Data.DataTable tblItem = con.Select(
                        "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "ol.weight," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil, " +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + order.OrderID);

                    order.ClearItemToListAndListView(listView);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Item item = new Item(0,
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                            item.Weight = Convert.ToInt16(row["weight"].ToString());
                            //order.ListItem.Add(item);
                            order.AddItemNoCheck(item, listView, money, true);
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Item item = new Item(0,
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                item.Weight = Convert.ToInt16(row["weight"].ToString());
                                order.AddItemNoCheck(item, listView, money, true);
                            }
                            else
                            {
                                SubItem subitem = new SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                                order.InsertSubItemListView(order.ListItem[order.ListItem.Count - 1], subitem, -1, listView, money, true);
                            }
                        }
                    }
                    btn.Tag = index;
                }
                System.Data.DataTable tblCard = con.Select("SELECT obc.orderbycardID AS orderbycardID,obc.cardID AS cardID,obc.subtotal AS subtotal,obc.cashOut as cashOut,obc.orderID AS orderID,c.cardName FROM orderbycard as obc inner join card as c on obc.cardID = c.cardID where obc.ID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CashOut = Convert.ToDouble(tblCard.Rows[0]["cashOut"]);
                if (tblCard.Rows.Count > 0)
                {
                    order.MoreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.MoreOrderByCard.Add(new OrderByCard()
                        {
                            orderbycardID = drRow["orderbycardID"].ToString(),
                            cardID = Convert.ToInt32(drRow["cardID"].ToString()),
                            subtotal = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["subtotal"].ToString()))),
                            cashOut = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["cashOut"].ToString()))),
                            orderID = Convert.ToInt32(drRow["orderID"].ToString()),
                            nameCard = drRow["cardName"].ToString()
                        }
                        );
                    }
                }
                tblCard = con.Select("SELECT obc.AuthCode,obc.AccountType,obc.Pan,obc.DateExpiry,obc.Date,obc.Time,obc.CardType,IF(obc.AmtPurchase IS NULL,0,obc.AmtPurchase) AS AmtPurchase,IF(obc.AmtCash IS NULL,0,obc.AmtCash) AS AmtCash FROM orderbycard as obc where obc.orderbycardID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                if (tblCard.Rows.Count > 0)
                {
                    //frmSubTotal.moreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.AuthCode = drRow["AuthCode"].ToString();
                        order.CardType = drRow["CardType"].ToString();
                        order.AccountType = drRow["AccountType"].ToString();
                        order.Pan = drRow["Pan"].ToString();
                        order.DateExpiry = drRow["DateExpiry"].ToString();
                        order.Date = drRow["Date"].ToString();
                        order.Time = drRow["Time"].ToString();
                        order.AmtPurchase = Convert.ToDecimal(drRow["AmtPurchase"]);
                        order.AmtCash = Convert.ToDecimal(drRow["AmtCash"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return order;
        }

        public Order getSaveOrderByOrderID(int orderID)
        {
            if (orderID <= 0)
            {
                return null;
            }
            Order order = new Order();
            order.OrderID = orderID;
            try
            {
                connection.Open();
                connection.BeginTransaction();
                order.OrderID = orderID;
                System.Data.DataTable tbl = connection.Select("select * from saveordersdaily where orderID=" + order.OrderID);
                if (tbl.Rows.Count > 0)
                {
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                }
                tbl = connection.Select("select ol.itemID,ol.qty,ol.subTotal,ol.price,ol.optionID, ol.gst, ol.weight,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName,ol.pumpID from saveordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID=0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["itemID"].ToString() != "0")
                    {
                        Item i = new Item(0,
                        Convert.ToInt32(row["itemID"]),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"]),
                        Convert.ToDouble(row["subTotal"]),
                        Convert.ToDouble(row["price"]),
                        Convert.ToDouble(row["gst"]),
                        0,
                        0,
                        row["pumpID"].ToString()
                        );
                        i.Weight = Convert.ToInt32(row["weight"]);
                        i.Weight = i.Weight > 1 ? i.Weight : 1;
                        if (i.Weight > 1)
                            i.IsFuel = true;
                        order.ListItem.Add(i);
                    }
                    else
                    {
                        order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(new SubItem(
                            Convert.ToInt32(row["optionID"].ToString()),
                            row["optionName"].ToString(),
                            Convert.ToInt32(row["qty"].ToString()),
                            Convert.ToDouble(row["subTotal"].ToString()),
                            Convert.ToDouble(row["price"].ToString())
                            ));
                    }
                }
                tbl = connection.Select("select ol.dynID as itemID,ol.qty,ol.subTotal,ol.price,ol.optionID,(select i.itemDesc from dynitemsmenu i where i.dynID=ol.dynID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName,ol.pumpID from saveordersdailyline ol where ol.orderID=" + order.OrderID + " and dynID<>0");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    Item item = new Item(0,
                        Convert.ToInt32(row["itemID"].ToString()),
                        row["itemDesc"].ToString(),
                        Convert.ToInt32(row["qty"].ToString()),
                        Convert.ToDouble(row["subTotal"].ToString()),
                        Convert.ToDouble(row["price"].ToString()),
                        0,
                        0,
                        0,
                        row["pumpID"].ToString()
                        );
                    item.ItemType = 1;
                    order.addItem(item);
                }
                //order.shortItem();
                return order;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getOrderByOrderID:::" + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return null;
        }

        public void printDriverOff(Order order)
        {
            printServer.printDriverOff(order);
        }

        public void printOrder(Order order)
        {
            if (order.Completed != 1)
            {
                printServer.print(order.TableID + "", 15);
            }
            else
            {
                printServer.printBuilt(true, order);
            }
        }

        public void printTaxtInvoice(Order order)
        {
            if (order.Completed == 1)
            {
                printServer.printBuilt(false, order);
            }
        }

        public void RePrintOrder(Order order)
        {
            printServer.print(order.TableID, 15);
        }

        public bool SendOrder(Order order)
        {
            IsNewOrder = false;
            Class.LogPOS.WriteLog("ProcessOrderNew.SendOrder.");
            order.refreshItemChange();
            bool resuilt = true;
            string strSQL = "";
            //Order orderOld = getOrderByTable(order.TableID);
            Order orderOld = getOrderByTable(order.TableID);
            //Order orderOld = null;
            //Order orderOld = getOrderByOrderID(order.OrderID);
            //new or der
            if (orderOld == null)
            {
                try
                {
                    connection.Open();
                    connection.BeginTransaction();
                    strSQL = "insert into ordersdaily(tableID,cableID,subTotal,completed,discount,clients,CustCode,IsDelivery,IsPickup,shiftID,custID,gst, managecar) values(" +
                                                "'" + order.TableID + "'" + "," +
                                                order.CableID + "," +
                        //(Convert.ToDouble(order.getSubTotal()) + order.Surchart + money.getFortMat(order.DeliveryFee)) + "," +
                                                (Convert.ToDouble(order.getSubTotal()) + money.getFortMat(order.DeliveryFee)) + "," +
                        //Convert.ToDouble(order.getSubTotal()) + "," +
                                                order.Completed + "," +
                                                order.Discount + "," +
                                                order.NumPeople + ",'" +
                                                order.CustCode + "'," +
                                                order.Delivery + "," +
                                                order.Pickup + "," +
                                                order.ShiftID + "," +
                                                order.Customer.CustID + "," +
                                                order.GetTotalGST() + "," +
                                                order.RegoID +
                                                ");";
                    strSQL += "set @tnew = LAST_INSERT_ID();";
                    for (int i = 0; i < order.ListItem.Count; i++)
                    //foreach (Item item in order.ListItem)
                    {
                        Item item = order.ListItem[i];
                        order.PumpID = item.PumID;
                        if (item.ItemType == 1)
                        {
                            strSQL += "insert into ordersdailyline(dynID,qty,price,subTotal,weight,gst,orderID,pumpID,pumpHistoryID) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.Price + "," +
                                                        item.SubTotal + "," +
                                                        item.Weight + "," +
                                                        item.GST + "," +
                                                        "@tnew" + "," +
                                                        (item.PumID == "" ? "0" : item.PumID) + "," +
                                                        item.IDPumpHistory +
                                                        ");";
                            strSQL += "CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + item.ItemID + "," + item.Qty + ",2);";
                        }
                        else
                        {
                            strSQL += "insert into ordersdailyline(itemID,qty,price,subTotal,weight,gst,orderID,pumpID,pumpHistoryID) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.Price + "," +
                                                        item.SubTotal + "," +
                                                        item.Weight + "," +
                                                        item.GST + "," +
                                                        "@tnew" + "," +
                                                        (item.PumID == "" ? "0" : item.PumID) + "," +
                                                        item.IDPumpHistory +
                                                        ");";
                            strSQL += "CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + item.ItemID + "," + item.Qty + ",2);";
                            foreach (Item itemLoyalty in order.ListLoyalTy)
                            {
                                if (itemLoyalty.OptionCount == i)
                                {
                                    strSQL += "insert into ordersdailyline(dynID,qty,price,subTotal,weight,gst,orderID,pumpID,pumpHistoryID) values(" +
                                                        itemLoyalty.ItemID + "," +
                                                        itemLoyalty.Qty + "," +
                                                        itemLoyalty.Price + "," +
                                                        itemLoyalty.SubTotal + "," +
                                                        item.Weight + "," +
                                                        item.GST + "," +
                                                        "@tnew" + "," +
                                                        (item.PumID == "" ? "0" : item.PumID) + "," +
                                                        item.IDPumpHistory +
                                                        ");";
                                }
                                strSQL += "CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + itemLoyalty.ItemID + "," + itemLoyalty.Qty + ",2);";
                            }
                            foreach (SubItem subItem in item.ListSubItem)
                            {
                                strSQL += "insert into ordersdailyline(optionID,qty,price,subTotal,weight,orderID,pumpID,pumpHistoryID) values(" +
                                                            subItem.ItemID + "," +
                                                            subItem.Qty + "," +
                                                            subItem.Price + "," +
                                                            subItem.SubTotal + "," +
                                                            item.Weight + "," +
                                                            "@tnew" + "," +
                                                            (item.PumID == "" ? "0" : item.PumID) + "," +
                                                            item.IDPumpHistory +
                                                            ");";
                                strSQL += "CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + subItem.ItemID + "," + subItem.Qty + ",2);";
                            }
                        }
                    }
                    connection.ExecuteScript(strSQL);
                    order.OrderID = Convert.ToInt32(connection.ExecuteScalar("select max(orderID) from ordersdaily"));
                    connection.ExecuteNonQuery("update fuelhistory as fh inner join ordersdailyline as odl on fh.ID = odl.pumpHistoryID set fh.IsComplete = 1 where odl.pumpID <> 0 AND odl.orderID = (select max(orderID) from ordersdaily)");

                    printServer.print(order.TableID + "", 15);
                    IsNewOrder = true;
                    connection.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("ProcessOrderNew.Send New Order:::" + ex.Message);
                    connection.Rollback();
                }
                finally
                {
                    connection.Close();
                }
                resuilt = false;
            }
            //old order
            else
            {
                try
                {
                    connection.Open();
                    connection.BeginTransaction();
                    strSQL = "update ordersdaily set subTotal=" + (order.getSubTotal() + money.getFortMat(order.DeliveryFee)) + ",discount=" + order.Discount + ",clients=" + order.NumPeople + ", managecar = " + order.RegoID + " where orderID=" + order.OrderID + ";";
                    strSQL += "delete from ordersdailyline where orderID=" + order.OrderID + ";";
                    foreach (Item item in order.ListItem)
                    {
                        if (item.ItemType == 1)
                        {
                            strSQL += "insert into ordersdailyline(dynID,qty,price,subTotal,gst,orderID,pumpID,pumpHistoryID) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.Price + "," +
                                                        item.SubTotal + "," +
                                                        item.GST + "," +
                                                        order.OrderID + "," +
                                                        (item.PumID == "" ? "0" : item.PumID) + "," +
                                                        item.IDPumpHistory +
                                                        ");";
                        }
                        else
                        {
                            strSQL += "insert into ordersdailyline(itemID,qty,price,subTotal,gst,orderID,pumpID,pumpHistoryID) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.Price + "," +
                                                        item.SubTotal + "," +
                                                        item.GST + "," +
                                                        order.OrderID + "," +
                                                        (item.PumID == "" ? "0" : item.PumID) + "," +
                                                        item.IDPumpHistory +
                                                        ");";
                            foreach (SubItem subItem in item.ListSubItem)
                            {
                                strSQL += "insert into ordersdailyline(optionID,qty,price,subTotal,orderID,pumpID,pumpHistoryID) values(" +
                                                            subItem.ItemID + "," +
                                                            subItem.Qty + "," +
                                                            subItem.Price + "," +
                                                            subItem.SubTotal + "," +
                                                            order.OrderID + "," +
                                                            (item.PumID == "" ? "0" : item.PumID) + "," +
                                                            item.IDPumpHistory +
                                                            ");";
                            }
                        }
                    }
                    strSQL += "delete from changedorders where orderID=" + order.OrderID + ";";
                    List<Item> listChange = getChangeOrder(orderOld.ListItem, order.ListItem);
                    foreach (Item item in listChange)
                    {
                        if (item.ItemType == 1)
                        {
                            strSQL += "insert into changedorders(dynID,qty,subTotal,orderID,changed) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.SubTotal + "," +
                                                        order.OrderID + "," +
                                                        item.Changed +
                                                        ");";
                        }
                        else
                        {
                            strSQL += "insert into changedorders(itemID,qty,subTotal,orderID,changed) values(" +
                                                        item.ItemID + "," +
                                                        item.Qty + "," +
                                                        item.SubTotal + "," +
                                                        order.OrderID + "," +
                                                        item.Changed +
                                                        ");";
                            foreach (SubItem subItem in item.ListSubItem)
                            {
                                strSQL += "insert into changedorders(optionID,qty,subTotal,orderID,changed) values(" +
                                                            subItem.ItemID + "," +
                                                            subItem.Qty + "," +
                                                            subItem.SubTotal + "," +
                                                            order.OrderID + "," +
                                                            item.Changed +
                                                            ");";
                            }
                        }
                    }
                    connection.ExecuteScript(strSQL);
                    printServer.print(order.TableID + "", 16);
                    connection.Commit();
                }
                catch (Exception exs)
                {
                    Class.LogPOS.WriteLog("ProcessOrderNew.Send Old Order:::" + exs.Message);
                    connection.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return resuilt;
        }

        public void subTotal(Order order, string strDataReceive)
        {
            arrayDataReceive = strDataReceive.Split('\\');
            if (order.Completed == 1)
            {
                return;
            }
            if (order.Completed == 0)
            {
                SendOrder(order);
                if (!IsNewOrder)
                {
                    order.Completed = 1;
                }
                else
                {
                    order.Completed = 2;
                }
            }
            else if (order.Completed == 3)
            {
                order.Completed = 1;
            }
            else
            {
                order.Completed = 2;
            }
            try
            {
                connection.Open();
                connection.BeginTransaction();
                connection.ExecuteNonQuery("update ordersdaily set completed=" + order.Completed + ",cash=" + ((order.Cash - order.ChangeAmount) < 0 ? 0 : (order.Cash - order.ChangeAmount)) + ",eftpos=" + order.Card + ",discount=" + order.Discount + ",staffID=" + order.StaffID + ",creditNote=" + (order.CreditNoteTotal - order.Balance) + ",orderbycardID=" + order.OrderByCardID + ",custID=" + order.Customer.CustID + " where orderID=" + order.OrderID);
                connection.ExecuteNonQuery("update creditnote set balance=" + order.Balance + " where creditNoteCode='" + order.CreditNoteCode + "'");
                if (order.Customer.CustID > 0)
                {
                    DataTable dtSource = connection.Select("Select Subtotal from accountpayment where CustomerNo = '" + order.Customer.MemberNo + "' order by PayDate DESC limit 1 ");

                    double dblSubtotal;
                    if (dtSource.Rows.Count != 0)
                    {
                        dblSubtotal = Convert.ToDouble(dtSource.Rows[0][0]);
                    }
                    else
                    {
                        dblSubtotal = 0;
                    }

                    connection.ExecuteNonQuery("insert into accountpayment(CustID, CustomerNo,PayAmount,EmployeeID,cash,card,Subtotal,OrderID) values('" +
                    order.Customer.CustID +
                    "," + order.Customer.MemberNo +
                    "',0," + order.StaffID +
                    "," + order.Cash +
                    "," + order.Card +
                    "," + (dblSubtotal + order.Account) +
                    ",'" + (order.OrderID) +
                    "')");
                    connection.ExecuteNonQuery("update customers set debt=" + (dblSubtotal + order.Account) + " where memberNo=" + "'" + order.Customer.MemberNo + "'");
                }
                if (Convert.ToBoolean(dbconfig.AllowPrint))
                {
                    printServer.printBuilt(false, order, arrayDataReceive);
                }
                connection.Commit();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.subTotal:::" + ex.Message);
                connection.Rollback();
            }
            finally
            {
                connection.Close();
            }
        }

        public void subTotalTackeAway(Order order, string strDataReceive)
        {
            string sql = "";
            if (order.Completed == 1)
            {
                return;
            }
            arrayDataReceive = strDataReceive.Split('\\');
            SendOrder(order);
            order.Completed = 1;
            try
            {
                connection.Open();
                connection.BeginTransaction();
                double change = order.Change = order.ChangeAmount;
                if (change <= order.Cash)
                {
                    change = 0;
                }
                sql = "update ordersdaily set ts=ts,completed=" + order.Completed + ",cash=" + ((order.Cash - order.ChangeAmount) < 0 ? 0 : (order.Cash - order.ChangeAmount)) + ",eftpos=" + order.Card + ",discount=" + order.Discount + ",staffID=" + order.StaffID + ",creditNote=" + order.CreditNoteTotal + ",account=" + order.Account + ",`change`=" + change + ",gst='" + order.GetTotalGST() + "',orderbycardID=" + order.OrderByCardID + ",custID=" + order.Customer.CustID + ",changebycash=" + order.ChangeByCash + ",Surchart=" + order.Surchart + ",FeeDelivery=" + money.getFortMat(order.DeliveryFee) + " where orderID=" + order.OrderID;
                connection.ExecuteNonQuery(sql);
                //Xuất kho
                DataAccess.DADelivering.InsertConnection(OrderToDelivering(order), connection);
                //hieu them parent line vao ordersdailyline
                foreach (ProcessOrderNew.Item item in order.ListItem)
                {
                    string parentLine = "0";
                    if (item.ListSubItem.Count > 0)
                    {
                        parentLine = item.ListSubItem[0].ItemID.ToString();
                        sql = "update ordersdailyline set parentLine=" + parentLine + " where orderID=" + order.OrderID + " and itemID=" + item.ItemID.ToString();
                        connection.ExecuteNonQuery(sql);
                    }
                    //Cập nhật lại stocklevel
                    sql = String.Format("select ItemID FROM stocklevel Where ItemID = {0};", item.ItemID);
                    if (!(Convert.ToInt32(connection.ExecuteScalar(sql)) > 0))
                    {
                        sql = String.Format("Insert Into stocklevel(ItemID) Values({0})", item.ItemID);
                        connection.ExecuteNonQuery(sql);
                    }
                    item.Weight = Convert.ToInt32(connection.ExecuteScalar("SELECT sellSize FROM itemsmenu i where itemID = " + item.ItemID + ";"));
                    item.Weight = item.IsFuel ? 1000 : 1;
                    sql = "Update stocklevel set QtySale =  QtySale + " + item.Qty + ", QtyOnHand =QtyOnHand - " + item.Qty + "  Where ItemID = " + item.ItemID + ";";
                    SystemLog.LogPOS.WriteLog(sql);
                    connection.ExecuteNonQuery(sql);
                }
                if (order.Customer.CustID > 0)
                {
                    double dblSubtotalHasPay;
                    sql = "insert into accountpayment(CustID,CustomerNo,PayAmount,EmployeeID,cash,card,Subtotal,typePayment,OrderID,CableID, RegoID) values(" +
                     order.Customer.CustID +
                     ",'" + order.Customer.MemberNo +
                     "',0," + order.StaffID +
                     "," + order.Cash +
                     "," + order.Card +
                     "," + (order.Account) +
                     ",1,'" +
                     order.OrderID + "'," +
                     dbconfig.CableID + ", " +
                    order.RegoID
                    + ")";
                    connection.ExecuteNonQuery(sql);
                    string MaxPaymetID = connection.Select("Select Max(PaymetID) As MaxPaymetID from accountpayment;").Rows[0][0].ToString();
                    if (order.Customer.Company != 0)
                    {
                        dtSource = connection.Select("select SUM(Subtotal) AS Total from accountpayment AS ap inner join company AS c on ap.CustomerNo = c.idCompany where ap.CustomerNo = " + order.Customer.Company + "");
                        try
                        {
                            dblSubtotalHasPay = Convert.ToDouble(dtSource.Rows[0][0]);
                        }
                        catch (Exception)
                        {
                            dblSubtotalHasPay = 0;
                        }
                        dtSource = connection.Select("select SUM(Subtotal) AS Subtotal from accountpayment as ap inner join customers as c on ap.CustomerNo = c.memberNo where c.Company =" + order.Customer.Company);
                        dblSubtotal = Convert.ToDouble(dtSource.Rows[0][0]);
                        connection.ExecuteNonQuery("Update company set debt=" + (dblSubtotal - dblSubtotalHasPay) + " where idCompany=" + "'" + order.Customer.Company + "'");
                        connection.ExecuteNonQuery("Update accountpayment Set balance = " + (dblSubtotal - dblSubtotalHasPay) + " Where PaymetID = " + MaxPaymetID);
                        dtSource = connection.Select("SELECT debt FROM customers where custID = " + order.Customer.CustID);
                        //if (dtSource.Rows.Count != 0)
                        //{
                        //  double decDebtAccMember = Convert.ToDouble(dtSource.Rows[0]["debt"].ToString());
                        // connection.ExecuteNonQuery("Update customers set debt = " + Convert.ToDouble(decDebtAccMember + order.Account) + " where custID = " + order.Customer.CustID);
                        //}
                    }
                    else
                    {
                        dtSource = connection.Select("SELECT debt FROM customers where custID = " + order.Customer.CustID);
                        dblSubtotal = Convert.ToDouble(dtSource.Rows[0]["debt"]);
                        connection.ExecuteNonQuery("update customers set debt = " + (dblSubtotal + order.Account) + " where memberNo=" + "'" + order.Customer.MemberNo + "'");
                        connection.ExecuteNonQuery("Update accountpayment Set balance = " + (dblSubtotal + order.Account) + " Where PaymetID = " + MaxPaymetID);
                    }
                }

                sql = "update shifts set ts=ts, numofOrders = (select count(*) from ordersdaily where shiftID = " + order.ShiftID + " ),subtotal = (SELECT SUM(subtotal) AS Subtotal FROM ordersdaily where shiftID = " + order.ShiftID + "),TotalCash = (SELECT SUM(cash) AS cash FROM ordersdaily where shiftID = " + order.ShiftID + ") where shiftID = " + order.ShiftID;
                connection.ExecuteNonQuery(sql);
                if (dbconfig.AllowSalesFuel == 1)
                {
                    try
                    {
                        dtSource = connection.Select("SELECT fh.PumID AS PumID FROM fuelhistory as fh inner join ordersdailyline as odl on fh.ID = odl.pumpHistoryID where odl.orderID = " + order.OrderID);
                        if (dtSource.Rows.Count != 0)
                        {
                            order.PumpID = "";
                            foreach (DataRow item in dtSource.Rows)
                            {
                                order.PumpID += item["PumID"].ToString() + "-";
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    connection.ExecuteNonQuery("update fuelhistory as fh inner join ordersdailyline as odl on fh.ID = odl.pumpHistoryID set fh.IsComplete = 1 where odl.pumpID <> 0 AND odl.orderID = " + order.OrderID);
                }
                connection.Commit();
                if (Convert.ToBoolean(dbconfig.AllowPrint))
                {
                    int n = 1;
                    if (order.Customer.CustID != 0)
                        n = 2;
                    while (n-- > 0)
                    {
                        if (order.IsMechanic)
                        {
                            Forms.frmReport frm = new Forms.frmReport(order);
                            ProcessOrderMechanic(order);
                        }
                        else
                            printServer.printBuilt(false, order, arrayDataReceive);
                    }

                }
                else
                {
                    int n = 1;
                    if (order.Customer.CustID != 0)
                        n = 2;
                    while (n-- > 0)
                    {
                        if (order.IsMechanic)
                        {
                            ProcessOrderMechanic(order);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.subTotalTackeAway:::" + ex.Message);
                connection.Rollback();
            }
            finally
            {
                connection.Close();
            }
        }

        internal void prinCreditNote()
        {
            try
            {
            }
            catch (Exception)
            {
            }
        }

        internal void PrintCashInOrCashOut(double dblCashOut)
        {
            printServer.PrintCashInOrCashOut(dblCashOut);
        }

        private bool checkItemInList(Item item, List<Item> list)
        {
            foreach (Item i in list)
            {
                if (i.compare(item))
                {
                    return true;
                }
            }
            return false;
        }

        private Item coppyItem(Item item)
        {
            Item resuilt;
            resuilt = new Item(0, item.ItemID, item.ItemName, item.Qty, item.SubTotal, item.Price, item.GST, item.Changed, item.OptionCount, item.PumID);

            foreach (SubItem sub in item.ListSubItem)
            {
                resuilt.addSubItem(new SubItem(sub.ItemID, sub.ItemName, sub.Qty, sub.SubTotal, sub.Price));
            }
            return resuilt;
        }

        private List<Item> getChangeOrder(List<Item> listOld, List<Item> listNew)
        {
            List<Item> listOldCopy = new List<Item>();
            List<Item> listNewCopy = new List<Item>();

            foreach (Item item in listOld)
            {
                listOldCopy.Add(item.Coppy());
            }
            foreach (Item item in listNew)
            {
                listNewCopy.Add(item.Coppy());
            }
            listOldCopy = splitListItem(listOldCopy);
            listNewCopy = splitListItem(listNewCopy);
            List<Item> resuilt = new List<Item>();
            for (int i = 0; i < listOldCopy.Count; i++)
            {
                for (int j = 0; j < listNewCopy.Count; j++)
                {
                    if (listOldCopy[i].compare(listNewCopy[j]))
                    {
                        listOldCopy.RemoveAt(i);
                        listNewCopy.RemoveAt(j);
                        i--;
                        //j--;
                        break;
                    }
                }
            }
            foreach (Item item in listOldCopy)
            {
                item.Changed = 2;
                resuilt.Add(item);
            }
            foreach (Item item in listNewCopy)
            {
                item.Changed = 1;
                resuilt.Add(item);
            }
            for (int i = 0; i < resuilt.Count; i++)
            {
                for (int j = i + 1; j < resuilt.Count; j++)
                {
                    if (resuilt[i].compare(resuilt[j]) && resuilt[i].Changed == resuilt[j].Changed)
                    {
                        resuilt[i].addItem(resuilt[j]);
                        resuilt.RemoveAt(j);
                        j--;
                    }
                }
            }
            return resuilt;
        }

        private DataObject.Delivering OrderToDelivering(Order order)
        {
            DataObject.Delivering item = new DataObject.Delivering();
            item.OrderID = order.OrderID;
            item.Type = 1;
            item.SubTotal = order.SubTotal;
            item.CoutItem = order.ListItem.Count;
            foreach (Item line in order.ListItem)
            {
                if (line.ItemID > 0)
                {
                    DataObject.DeliveringLine l = new DataObject.DeliveringLine();
                    l.Qty = line.Qty;
                    l.Total = line.Qty;
                    l.ItemID = line.ItemID;
                    item.DeliveringLine.Add(l);
                }
            }
            return item;
        }

        //.DataTable dtSource;
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //dtSource = connection.Select("select balance,creditnotecode from creditnote where creditnotecode = '" + frmSubTotal.strCreditCode + "'");
            //float y = 0;
            //y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            //y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            //y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            //y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            //y += 100;

            //y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            //y += 100;

            //DateTime dateTime = DateTime.Now;
            //y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);
            //mPrinter.DrawString("ORDER#" + mOrderIDText, e, new Font("Arial", 11), y, 1);
            //y = mPrinter.DrawString("CABLE#" + mCableIDText, e, new Font("Arial", 11), y, 3);

            //y += 100;

            //y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            //y += 100;

            //y = mPrinter.DrawString("CREDIT NOTE", e, new Font("Arial", 18, FontStyle.Bold), y, 2);

            //y += 100;

            //y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            //y += 100;

            //y = mPrinter.DrawString("Code # " + dtSource.Rows[0]["creditnotecode"].ToString(), e, new Font("Arial", 11), y, 1);
            //y = mPrinter.DrawString("Amount : $" + mMoney.FormatCurenMoney(Convert.ToDouble(dtSource.Rows[0]["balance"].ToString())), e, new Font("Arial", 11), y, 1);
        }

        private void ProcessOrderMechanic(Order order)
        {
            try
            {
                string sql = "Update slot Set complete = 1 Where id = " + order.SlotID + ";";
                connection.ExecuteNonQuery(sql);
                int OrderID = Convert.ToInt32(connection.ExecuteScalar("Select orderID FROM slot s Where id = " + order.SlotID + ";"));
                sql = "Delete from saveordersdaily Where OrderID = " + OrderID + ";";
                connection.ExecuteNonQuery(sql);
                sql = "Delete from saveordersdailyline Where OrderID = " + OrderID + ";";
                connection.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ProcessOrderMechanic::" + ex.Message);
            }
        }

        //private List<Item> getChangeOrder(List<Item> listOld, List<Item> listNew)
        //{
        //    List<Item> listOldCopy = new List<Item>();
        //    List<Item> listNewCopy = new List<Item>();

        //    foreach (Item item in listOld)
        //    {
        //        listOldCopy.Add(item.Coppy());
        //    }
        //    foreach (Item item in listNew)
        //    {
        //        listNewCopy.Add(item.Coppy());
        //    }
        //    listOldCopy = splitListItem(listOldCopy);
        //    listNewCopy = splitListItem(listNewCopy);
        //    List<Item> resuilt = new List<Item>();
        //    for (int i = 0; i < listOldCopy.Count; i++)
        //    {
        //        for (int j = 0; j < listNewCopy.Count; j++)
        //        {
        //            if (listOldCopy[i].compare(listNewCopy[j]))
        //            {
        //                listOldCopy.RemoveAt(i);
        //                listNewCopy.RemoveAt(j);
        //                i--;
        //                //j--;
        //                break;
        //            }
        //        }
        //    }
        //    foreach (Item item in listOldCopy)
        //    {
        //        item.Changed = 2;
        //        resuilt.Add(item);
        //    }
        //    foreach (Item item in listNewCopy)
        //    {
        //        item.Changed = 1;
        //        resuilt.Add(item);
        //    }
        //    for (int i = 0; i < resuilt.Count; i++)
        //    {
        //        for (int j = i + 1; j < resuilt.Count; j++)
        //        {
        //            if (resuilt[i].compare(resuilt[j]) && resuilt[i].Changed == resuilt[j].Changed)
        //            {
        //                resuilt[i].addItem(resuilt[j]);
        //                resuilt.RemoveAt(j);
        //                j--;
        //            }
        //        }
        //    }
        //    return resuilt;
        //}
        private List<Item> splitListItem(List<Item> listItem)
        {
            List<Item> resuilt = new List<Item>();
            foreach (Item item in listItem)
            {
                int qty = item.Qty;
                item.Qty = 1;
                for (int i = 0; i < qty; i++)
                {
                    resuilt.Add(item.Coppy());
                }
            }
            return resuilt;
            //for (int i = listItem.Count-1; i >= 0; i--)
            //{
            //    int max = listItem[i].Qty;
            //    for (int j = 0; j < listItem[i].ListSubItem.Count; j++)
            //    {
            //        if (listItem[i].ListSubItem[j].Qty>max)
            //        {
            //            max = listItem[i].ListSubItem[j].Qty;
            //        }
            //    }
            //    for (int k = max; k >= 1; k--)
            //    {
            //        if (listItem[i].Qty%k==0)
            //        {
            //            bool key = true;
            //            foreach (SubItem sub in listItem[i].ListSubItem)
            //            {
            //                if (sub.Qty%k!=0)
            //                {
            //                    key = false;
            //                    break;
            //                }
            //            }
            //            if (key)
            //            {
            //                listItem[i].Qty /= k;
            //                listItem[i].SubTotal /= k;
            //                foreach (SubItem sub in listItem[i].ListSubItem)
            //                {
            //                    sub.Qty /= k;
            //                    sub.SubTotal /= k;
            //                }
            //                for (int l = 0; l < k; l++)
            //                {
            //                    listItem.Add(listItem[i].Coppy());
            //                }
            //                listItem.RemoveAt(i);
            //                break;
            //            }
            //        }
            //    }
            //}
            //return listItem;
        }

        public class Item
        {
            public Item(
                    int idPumpHistory,
                    int itemID,
                string name,
                    int qty,
                    double subTotal,
                    double price,
                    double gst,
                    int changed,
                    int optionCount,
                    string pumpID
                )
            {
                IDPumpHistory = idPumpHistory;
                ItemID = itemID;
                ItemName = name;
                Qty = qty;
                SubTotal = subTotal;
                Price = price;
                GST = gst;
                Changed = changed;
                OptionCount = optionCount;
                PumID = pumpID;
                BarCode = "";
                EnableRemove = true;
                IsMechanic = false;
                ListSubItem = new List<SubItem>();
                this.ListKeyItemPrent = new List<int>();
            }
            public Item()
            {
                PumID = "";
            }

            public string BarCode { get; set; }

            public int Changed { get; set; }

            public int EnableFuelDiscount { get; set; }

            public bool EnableRemove { get; set; }

            public double GST { get; set; }

            public double GSTDiscount { get; set; }

            public int IDPumpHistory { get; set; }

            public bool IsFuel { get; set; }

            public bool IsLineDicount { get; set; }

            public bool IsMechanic { get; set; }

            public bool IsOption { get; set; }

            public int ItemID { get; set; }

            public string ItemName { get; set; }

            public int ItemType { get; set; }

            public int KeyItem { get; set; }

            public List<int> ListKeyItemPrent { get; set; }

            public List<SubItem> ListSubItem { get; set; }

            public int OptionCount { get; set; }

            public int ParentItem { get; set; }

            public double Price { get; set; }

            public string PumID { get; set; }

            public int Qty { get; set; }

            public int Qty_Tmp { get; set; }

            public int Refund { get; set; }

            public double SubTotal { get; set; }

            public int VoidID { get; set; }

            public int Weight { get; set; }
            public int Liter { get; set; }
            public void addItem(Item item)
            {
                //shortListSubItem();
                //item.shortListSubItem();
                this.Qty += item.Qty;
                this.SubTotal += item.SubTotal;
                for (int i = 0; i < ListSubItem.Count; i++)
                {
                    ListSubItem[i].Qty += item.ListSubItem[i].Qty;
                    ListSubItem[i].SubTotal += item.ListSubItem[i].SubTotal;
                }
            }

            public void addSubItem(SubItem sub)
            {
                //foreach (SubItem s in ListSubItem)
                //{
                //    if (s.ItemID==sub.ItemID)
                //    {
                //        s.Qty += sub.Qty;
                //        s.SubTotal += sub.SubTotal;
                //        return;
                //    }
                //}
                ListSubItem.Add(sub);
            }

            public bool compare(Item item)
            {
                shortListSubItem();
                item.shortListSubItem();
                //if (item.ListSubItem.Count>0 || this.ListSubItem.Count>0)
                //{
                //    return false;
                //}
                if (item.Weight != Weight)
                {
                    return false;
                }
                if (ItemID != item.ItemID || ItemType != item.ItemType)
                {
                    return false;
                }
                if (ListSubItem.Count != item.ListSubItem.Count)
                {
                    return false;
                }
                for (int i = 0; i < ListSubItem.Count; i++)
                {
                    if (ListSubItem[i].ItemID != item.ListSubItem[i].ItemID || (ListSubItem[i].Qty * item.Qty) != (Qty * item.ListSubItem[i].Qty))
                    {
                        return false;
                    }
                }
                return true;
            }

            public bool compare2(Item item)
            {
                shortListSubItem();
                item.shortListSubItem();
                if (this.IsFuel || item.IsFuel)
                {
                    return false;
                }
                if (item.Weight != Weight)
                {
                    return false;
                }
                if (ItemID != item.ItemID || ItemType != item.ItemType)
                {
                    return false;
                }
                if (PumID != item.PumID)
                {
                    return false;
                }
                if (ListSubItem.Count != item.ListSubItem.Count)
                {
                    return false;
                }
                for (int i = 0; i < ListSubItem.Count; i++)
                {
                    if (ListSubItem[i].ItemID != item.ListSubItem[i].ItemID || (ListSubItem[i].Qty * item.Qty) != (Qty * item.ListSubItem[i].Qty))
                    {
                        return false;
                    }
                }
                return true;
            }

            public Item Coppy()
            {
                Item item = new Item(IDPumpHistory, ItemID, ItemName, Qty, SubTotal, Price, GST, Changed, OptionCount, PumID);
                item.ItemType = ItemType;
                item.BarCode = BarCode;
                item.IsFuel = IsFuel;
                item.KeyItem = KeyItem;
                item.ParentItem = ParentItem;
                item.Weight = Weight;
                item.EnableFuelDiscount = EnableFuelDiscount;
                item.EnableRemove = EnableRemove;
                item.GSTDiscount = this.GSTDiscount;
                item.IsLineDicount = this.IsLineDicount;
                item.IsMechanic = this.IsMechanic;

                foreach (SubItem sub in ListSubItem)
                {
                    item.ListSubItem.Add(sub.Coppy());
                }
                foreach (var key in this.ListKeyItemPrent)
                {
                    item.ListKeyItemPrent.Add(key);
                }
                return item;
            }

            public double getTotal()
            {
                double resuilt = 0;
                Weight = Weight > 1 ? Weight : 1;
                if (this.Weight > 1)
                {
                    //resuilt += (double)Qty / Weight * Price;
                    //float w =(float)( Weight / 1000);
                    //double w = Convert.ToDouble(Weight);

                    resuilt += ((double)Qty / 1000) * Price;
                }
                else
                {
                    resuilt += Qty * Price;
                }
                foreach (SubItem sub in ListSubItem)
                {
                    resuilt += sub.Qty * sub.Price;
                }
                return resuilt;
            }

            public void shortListSubItem()
            {
                for (int i = 0; i < ListSubItem.Count; i++)
                {
                    for (int j = i + 1; j < ListSubItem.Count; j++)
                    {
                        if (ListSubItem[i].ItemID > ListSubItem[j].ItemID)
                        {
                            SubItem s = ListSubItem[i];
                            ListSubItem[i] = ListSubItem[j];
                            ListSubItem[j] = s;
                        }
                    }
                }
            }
        }

        public class Order
        {
            public List<Class.ComboDiscount> cboDisc;

            public List<Class.ComboDiscount> resultDisc;

            public Order()
            {
                ListItem = new List<Item>();
                ListLoyalTy = new List<Item>();
                ListKeyLoyaltyItem = new List<int>();
                MoreOrderByCard = new List<OrderByCard>();
                MaxKey = 1;
                cboDisc = new List<ComboDiscount>();
                resultDisc = new List<ComboDiscount>();
                Customer = new DataObject.Customers();
                Rego = new DataObject.Rego();
                DateOrder = DateTime.Now;
            }

            public Order(
                int orderID,
                string tableID,
                int cableID,
                double subTotal,
                double discount,
                int completed,
                int numPeople
                )
            {
                OrderID = orderID;
                TableID = tableID;
                CableID = cableID;
                SubTotal = subTotal;
                Discount = discount;
                Completed = completed;
                NumPeople = numPeople;
                DateOrder = DateTime.Now;
                ListItem = new List<Item>();
                ListLoyalTy = new List<Item>();
                ListKeyLoyaltyItem = new List<int>();
                MoreOrderByCard = new List<OrderByCard>();
                MaxKey = 1;
                cboDisc = new List<ComboDiscount>();
                resultDisc = new List<ComboDiscount>();
                Customer = new DataObject.Customers();
                Rego = new DataObject.Rego();
            }

            public double Account { get; set; }

            public string AccountType { get; set; }

            public decimal AmtCash { get; set; }

            public decimal AmtPurchase { get; set; }

            public string AuthCode { get; set; }

            public double Balance { get; set; }

            public int CableID { get; set; }

            public double Card { get; set; }

            //For Swipe card
            public string CardType { get; set; }

            public double Cash { get; set; }

            public string CashOrCard
            {
                get
                {
                    string str = "";
                    if (Cash > 0 && Card > 0 && Account > 0)
                        str = "Cash & Card & Account";
                    else if (Cash > 0 && Card > 0)
                        str = "Cash & Card";
                    else if (Cash > 0 && Account > 0)
                        str = "Cash & Account";
                    else if (Card > 0 && Account > 0)
                        str = "Card & Account";
                    else if (Cash > 0)
                        str = "Cash";
                    else if (Card > 0)
                        str = "Card";
                    else if (Account > 0)
                        str = "Account";
                    return str;
                }
            }

            public double CashOut { get; set; }

            public double Change { get; set; }

            public double ChangeAmount { get; set; }

            public double ChangeByCash { get; set; }

            public int Completed { get; set; }

            public string CreditNoteCode { get; set; }

            public double CreditNoteTotal { get; set; }

            public string CustCode { get; set; }

            public DataObject.Customers Customer { get; set; }

            public string Date { get; set; }

            public string DateExpiry { get; set; }

            public DateTime DateOrder { get; set; }

            public string DateShort { get { return DateOrder.ToLongDateString(); } }

            public int Delivery { get; set; }

            public double DeliveryAmount { get; set; }

            public double DeliveryFee { get; set; }

            public double Deposit { get; set; }

            public double Discount { get; set; }

            public int EmployeeID { get; set; }

            public double Gst { get; set; }

            public bool IsMechanic
            {
                get
                {
                    if (ListItem != null)
                    {
                        foreach (Item item in ListItem)
                        {
                            if (item.IsMechanic)
                                return true;
                        }
                    }
                    return false;
                }
            }

            public bool IsNonSubtotal { get; set; }

            public bool IsPreOrder { get; set; }

            public int IsSwipeCard { get; set; }

            public List<Item> ListItem { get; set; }

            public List<int> ListKeyLoyaltyItem { get; set; }

            public List<Item> ListLoyalTy { get; set; }

            public int MaxKey { get; set; }

            public List<OrderByCard> MoreOrderByCard { get; set; }

            public int NumPeople { get; set; }

            public string OrderByCardID { get; set; }

            public int OrderID { get; set; }

            public int PagerNo { get; set; }

            public int Paid { get; set; }

            public string Pan { get; set; }

            public int Pickup { get; set; }

            public string PriterAccount { get; set; }

            public string PriterCard { get; set; }

            public string PriterCash { get; set; }

            public string PriterChange { get; set; }

            public string PriterDiscount { get; set; }

            public string PriterGST { get; set; }

            public string PriterSubTotal { get; set; }

            public string PriterTotal { get; set; }

            public string PumpID { get; set; }

            public Class.ReadConfig ReadConfig { get; set; }

            public DataObject.Rego Rego { get; set; }

            public int RegoID { get; set; }

            public string SalesPerson { get; set; }

            public int ShiftID { get; set; }

            public int SlotID { get; set; }

            public int StaffID { get; set; }

            public double SubTotal { get; set; }

            public double Surchart { get; set; }

            public string TableID { get; set; }

            public double Tendered { get; set; }

            public string Time { get; set; }

            public void addItem(Item item)
            {
                //foreach (Item i in ListItem)
                //{
                //    if (i.compare(item))
                //    {
                //        i.Qty += item.Qty;
                //        i.SubTotal += item.SubTotal;
                //        for (int k = 0; k < i.ListSubItem.Count; k++)
                //        {
                //            i.ListSubItem[k].Qty += item.ListSubItem[k].Qty;
                //            i.ListSubItem[k].SubTotal += item.ListSubItem[k].SubTotal;
                //        }
                //        return;
                //    }
                //}
                Item i = new Item(item.IDPumpHistory, item.ItemID, item.ItemName, item.Qty, item.SubTotal, item.Price, item.GST, item.Changed, item.OptionCount, item.PumID);
                i.ItemType = item.ItemType;
                i.Weight = item.Weight;
                ListItem.Add(i);
            }

            public Item AddItem(Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money, bool selected)
            {
                Item itemtemp = null;
                if (ListItem.Count > 0 && item.ItemType == 0 && item.IsFuel == false)
                {
                    for (int i = ListItem.Count - 1; i >= 0; i--)
                    {
                        if (ListItem[i].ItemType == 0)
                        {
                            if (item.ItemID == ListItem[i].ItemID)
                            {
                                if (CheckContainChildrent(ListItem[i].KeyItem))
                                    break;
                                itemtemp = ListItem[i];
                            }
                            break;
                        }
                    }
                }
                if (itemtemp != null)
                {
                    if (itemtemp.Liter != 1)
                    {
                        itemtemp.Qty++;
                    }

                    foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                    {
                        if (Convert.ToInt32(li.Tag) == itemtemp.KeyItem)
                        {
                            if (itemtemp.Liter == 1)
                            {
                                itemtemp.SubTotal = itemtemp.SubTotal + item.SubTotal;
                                itemtemp.Qty = itemtemp.Qty + item.Qty;
                                li.SubItems[0].Text = itemtemp.Qty.ToString();
                                li.SubItems[2].Text = money.Convert3to2(Convert.ToDouble(money.Format(itemtemp.SubTotal))).ToString();

                            }
                            else
                            {
                                itemtemp.SubTotal = itemtemp.Qty * itemtemp.Price;
                                li.SubItems[0].Text = itemtemp.Qty.ToString();
                                li.SubItems[2].Text = money.Convert3to2(Convert.ToDouble(money.Format(itemtemp.SubTotal))).ToString();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    MaxKey++;
                    item.KeyItem = MaxKey;
                    ListItem.Add(item);
                    InsertItemListView(item, -1, listView1, money, selected);
                }
                this.CheckFuelDiscount(listView1, money);
                return item;
            }

            public int AddItemNoCheck(Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money, bool selected)
            {
                MaxKey++;
                item.KeyItem = MaxKey;
                ListItem.Add(item);
                InsertItemListView(item, -1, listView1, money, selected);
                return MaxKey;
            }

            public void AddItemToList(Class.ProcessOrderNew.Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                string qty = item.Qty + "";
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    qty = money.FormatCurenMoney((double)item.Qty / item.Weight);
                }
                System.Windows.Forms.ListViewItem li = new System.Windows.Forms.ListViewItem(qty);
                li.SubItems.Add(item.ItemName);
                li.SubItems.Add(money.Format(item.SubTotal));
                li.Tag = item.KeyItem;

                li.EnsureVisible();
                li.Selected = true;
                listView1.Items.Add(li);
            }

            public void AddSubItem(Item item, SubItem sub)
            {
                MaxKey++;
                sub.KeyItem = MaxKey;
                item.addSubItem(sub);
            }

            public void CheckComboDiscount(System.Windows.Forms.ListView listView, Class.MoneyFortmat money, List<ComboDiscount> lstComboDisc, List<ComboDiscount> resultDisc, List<Item> ListItem)
            {
                Connection.Connection con = new Connection.Connection();
                try
                {
                    //List<Class.ComboDiscount> listComboDiscount = Class.ProcessComboDiscount.GetListComboDiscount(this.ListItem);
                    con.Open();
                    foreach (System.Windows.Forms.ListViewItem lstItem in listView.Items)
                    {
                        if (lstItem.SubItems[4].Text == "-1")
                        {
                            listView.Items.Remove(lstItem);
                        }
                    }
                    int j = 0;
                    while (j < ListItem.Count)
                    {
                        if (ListItem[j].IDPumpHistory == -1)
                        {
                            con.ExecuteNonQuery("delete from dynitemsmenu where dynID=" + ListItem[j].ItemID);
                            ListItem.Remove(ListItem[j]);
                        }
                        else
                            j++;
                    }
                    resultDisc = Class.ProcessComboDiscount.GetListComboDiscount(this.ListItem);
                    for (int mid = 0; mid < resultDisc.Count; mid++)
                    {
                        Item it = new Item(-1, 0, resultDisc[mid].Name, 1, -1 * resultDisc[mid].qtyRule * resultDisc[mid].PriceDiscount, -1 * resultDisc[mid].qtyRule * resultDisc[mid].PriceDiscount, 0, 0, 0, "0");
                        string sql = "insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice,groupID, printerType,PumID) values(" + this.ShiftID + ",'" + it.ItemName + "','" + it.ItemName + "'," + it.Price + ",0,0,0)";
                        con.ExecuteNonQuery(sql);

                        it.ItemID = Convert.ToInt32(con.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                        it.ItemType = 1;
                        it.EnableFuelDiscount = 1;
                        ListItem.Add(it);
                        System.Windows.Forms.ListViewItem li = new System.Windows.Forms.ListViewItem("1");
                        li.SubItems.Add(resultDisc[mid].Name);
                        li.SubItems.Add(money.Format2(-1 * resultDisc[mid].qtyRule * resultDisc[mid].PriceDiscount));
                        li.SubItems.Add("0");
                        li.SubItems.Add("-1");
                        li.Tag = MaxKey++;
                        listView.Items.Add(li);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    con.Close();
                }
            }

            /// <summary>
            /// kiem tra neu co con thi tra lai true con lai ko thi thoi
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
            public bool CheckContainChildrent(int key)
            {
                foreach (Item item in ListItem)
                {
                    if (item.ParentItem == key)
                    {
                        return true;
                    }
                }
                return false;
            }

            public void CheckFuelDiscount(System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    for (int i = 0; i < ListItem.Count; i++)
                    //foreach (Item item in ListItem)
                    {
                        Item item = ListItem[i];
                        if (item.IsFuel)
                        {
                            double discount = money.getFortMat(Class.FuelDiscountConfig.GetDiscount() / 100);
                            double spend = money.getFortMat(Class.FuelDiscountConfig.GetSpend());
                            this.RemoveAlChildren(item.KeyItem, listView1);
                            if (this.GetSubTotalRetail() >= spend)
                            {
                                double maxQty = FuelDiscountConfig.GetMaxQtyDiscount();
                                if (item.Qty < maxQty)
                                {
                                    maxQty = (double)item.Qty;
                                }
                                double price = (maxQty / 1000 * discount);
                                if (price > item.SubTotal)
                                {
                                    price = item.SubTotal;
                                }
                                if (price > 0)
                                {
                                    price = -1 * price;
                                    Item dyn = new Item(0, 0, "Discount when spend $" + money.Format(spend) + " or more", 1, price, price, 0, 0, 0, "0"); // LienFeb12
                                    con.ExecuteNonQuery("insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice) values(" + this.ShiftID + ",'" + dyn.ItemName + "','" + dyn.ItemName + "'," + dyn.Price + ")");
                                    dyn.ItemID = Convert.ToInt32(con.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                                    dyn.ParentItem = item.KeyItem;
                                    dyn.ItemType = 1;
                                    this.InsertAfterKey(dyn, item.KeyItem, listView1, money);
                                }
                            }
                        }
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("CheckFuelDiscount::" + ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }

            public void CheckLoyalty(Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                System.Data.DataTable tbl;
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    DateTime dt = DateTime.Now;
                    if (item.ItemType == 0)
                    {
                        int itemQty = item.Qty;
                        tbl = con.Select("select itemID,name,qty,discount from loyaltybyitem where itemID=" + item.ItemID + " and date(dateStart)<=date('" + dt.Year + "-" + dt.Month + "-" + dt.Day + "') and( date(dateEnd)=date('0000-00-00') or date(dateEnd)>=date('" + dt.Year + "-" + dt.Month + "-" + dt.Day + "')) and `enable`=1 order by qty desc");
                        foreach (System.Data.DataRow row in tbl.Rows)
                        {
                            int qty = Convert.ToInt32(row["qty"]);
                            if (itemQty >= qty)
                            {
                                double price = 0 - Convert.ToDouble(row["discount"]) * ((double)itemQty / qty);
                                itemQty = itemQty % qty;
                                Item dyn = new Item(0, 0, row["name"].ToString(), 1, price, price, 0, 0, 0, "0");
                                con.ExecuteNonQuery("insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice) values(" + this.ShiftID + ",'" + dyn.ItemName + "','" + dyn.ItemName + "'," + dyn.Price + ")");
                                dyn.ItemID = Convert.ToInt32(con.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                                dyn.ParentItem = item.KeyItem;
                                dyn.ItemType = 1;
                                dyn.EnableFuelDiscount = 1;
                                this.InsertAfterKey(dyn, item.KeyItem, listView1, money);
                                //order.AddItem(dyn, listView1, money);
                            }
                        }
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("CheckFuelDiscount::" + ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }

            public void CheckLoyaltyByListItem(System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    List<ItemLoyatyByListItemDetail> list = new List<ItemLoyatyByListItemDetail>();
                    List<ItemLoyatyByListItem> listLoyalty = new List<ItemLoyatyByListItem>();

                    foreach (int item in ListKeyLoyaltyItem)
                    {
                        this.RemoveItemToListAndListView(item, listView1);
                    }
                    ListKeyLoyaltyItem.Clear();
                    int insertKey = 0;
                    foreach (Item item in ListItem)
                    {
                        //chi khuyen mai item don le ko cho item khac
                        if (item.ListSubItem.Count == 0 && item.ParentItem == 0)
                        {
                            list.Add(new ItemLoyatyByListItemDetail(item.ItemID, item.Qty, item.Qty, item.KeyItem));
                            if (item.ItemType == 0 && item.KeyItem > insertKey && !item.IsFuel)
                            {
                                insertKey = item.KeyItem;
                            }
                        }
                    }
                    if (list.Count > 0)
                    {
                        string s = "";
                        foreach (ItemLoyatyByListItemDetail item in list)
                        {
                            s += "" + item.ItemID + ",";
                        }
                        if (s.Length > 0)
                        {
                            s = s.Substring(0, s.Length - 1);
                            System.Data.DataTable tbl = con.Select("select loyID,loyName,discount,qty from loyaltybylistitem where loyID in (select loyID from loyaltybylistitemdetail where itemID in (" + s + ")) and `enable`=1 and date(dateStart)<=date(now()) and( date(dateEnd)=date('0000-00-00') or date(dateEnd)>=date(now()))");
                            if (tbl.Rows.Count > 0)
                            {
                                //foreach (System.Data.DataRow row in tbl.Rows)
                                for (int i = 0; i < tbl.Rows.Count; i++)
                                {
                                    System.Data.DataRow row = tbl.Rows[i];
                                    int qtyDis = Convert.ToInt32(row["qty"]);
                                    foreach (ItemLoyatyByListItemDetail item in list)
                                    {
                                        item.QtyTmp = item.Qty;
                                    }
                                    System.Data.DataTable tblList = con.Select("select * from loyaltybylistitemdetail where loyID=" + row["loyID"].ToString());
                                    //bool key = false;
                                    //int qty = 0;
                                    List<int> listItemKey = new List<int>();
                                    if (CheckQtyLoyaltyByListItem(list, listItemKey, tblList, qtyDis))
                                    {
                                        ItemLoyatyByListItem il = new ItemLoyatyByListItem(Convert.ToInt16(row["loyID"]), row["loyName"].ToString(), -1 * Convert.ToDouble(row["discount"]));
                                        il.LitstItemKey = listItemKey;
                                        listLoyalty.Add(il);
                                        foreach (ItemLoyatyByListItemDetail item in list)
                                        {
                                            item.QtyTmp = item.Qty;
                                        }
                                        i--;
                                    }
                                    else
                                    {
                                        foreach (ItemLoyatyByListItemDetail item in list)
                                        {
                                            item.Qty = item.QtyTmp;
                                        }
                                    }
                                    /*
                                    foreach (System.Data.DataRow rowItem in tblList.Rows)
                                    {
                                        qty += GetQtyItemIDAndQty(list, listItemKey,Convert.ToInt16(rowItem["itemID"]),qty,qtyDis);
                                        if (qty==qtyDis)
                                        {
                                            key = true;
                                            break;
                                        }
                                    }
                                     * */
                                    //if (key)
                                    //{
                                    //    ItemLoyatyByListItem il = new ItemLoyatyByListItem(Convert.ToInt16(row["loyID"]), row["loyName"].ToString(), -1 * Convert.ToDouble(row["discount"]));
                                    //    il.LitstItemKey=listItemKey;
                                    //    listLoyalty.Add(il);
                                    //    foreach (ItemLoyatyByListItemDetail item in list)
                                    //    {
                                    //        item.QtyTmp = item.Qty;
                                    //    }
                                    //    i--;
                                    //}
                                    //else
                                    //{
                                    //    foreach (ItemLoyatyByListItemDetail item in list)
                                    //    {
                                    //        item.Qty = item.QtyTmp;
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                    for (int i = 0; i < listLoyalty.Count; i++)
                    {
                        for (int j = i + 1; j < listLoyalty.Count; j++)
                        {
                            if (listLoyalty[i].Compare(listLoyalty[j]))
                            {
                                //listLoyalty[i].AddSameItemLoyatyByListItem(listLoyalty[j]);
                                listLoyalty[i].Price += listLoyalty[j].Price;
                                listLoyalty.RemoveAt(j);
                                j--;
                            }
                        }
                    }
                    foreach (ItemLoyatyByListItem item in listLoyalty)
                    {
                        Item dyn = new Item(0, 0, item.Name, 1, item.Price, item.Price, 0, 0, 0, "0");
                        dyn.ListKeyItemPrent = item.LitstItemKey;
                        con.ExecuteNonQuery("insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice) values(" + this.ShiftID + ",'" + dyn.ItemName + "','" + dyn.ItemName + "'," + dyn.Price + ")");
                        dyn.ItemID = Convert.ToInt32(con.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                        dyn.ItemType = 1;
                        dyn.EnableFuelDiscount = 1;
                        //tinh gst discount
                        double sum = 0;
                        foreach (int k in dyn.ListKeyItemPrent)
                        {
                            Item i = this.GetItemByKey(k);
                            if (i != null)
                            {
                                sum += i.getTotal();
                            }
                        }
                        if (sum > 0)
                        {
                            foreach (int k in dyn.ListKeyItemPrent)
                            {
                                Item i = this.GetItemByKey(k);
                                if (i != null)
                                {
                                    dyn.GSTDiscount += (Math.Abs(dyn.SubTotal) * i.getTotal() / sum) * (i.GST / (i.GST + 100));
                                }
                            }
                        }
                        //MaxKey++;
                        //dyn.KeyItem = MaxKey;
                        //this.AddItemNoCheck(dyn, listView1, money,false);
                        //insert vao cuoi
                        if (listView1.Items.Count > 0)
                        {
                            insertKey = Convert.ToInt32(listView1.Items[listView1.Items.Count - 1].Tag);
                            this.InsertAfterKey(dyn, insertKey, listView1, money);
                            ListKeyLoyaltyItem.Add(dyn.KeyItem);
                        }
                        //insert vao mon discount
                        //this.InsertAfterKey(dyn, insertKey, listView1, money);
                        //ListKeyLoyaltyItem.Add(dyn.KeyItem);
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("CheckFuelDiscount::" + ex.Message);
                    con.Rollback();
                }
                finally
                {
                    con.Close();
                }
            }

            public void ClearItemToListAndListView(System.Windows.Forms.ListView listView1)
            {
                MaxKey = 0;
                foreach (Item item in ListItem)
                {
                    AddVoidItem(item, 0);
                }
                ListItem.Clear();
                listView1.Items.Clear();
            }

            public bool Contain(Item item)
            {
                foreach (Item i in ListItem)
                {
                    if (i.compare(item))
                    {
                        return true;
                    }
                }
                return false;
            }

            public bool ContainFuelItem()
            {
                foreach (Item item in ListItem)
                {
                    if (item.IsFuel)
                    {
                        return true;
                    }
                }
                return false;
            }

            public Order CoppyOrder()
            {
                Order o = new Order(this.OrderID, TableID, CableID, SubTotal, Discount, Completed, 0);
                o.StaffID = StaffID;
                o.MaxKey = MaxKey;
                o.Account = Account;
                o.Balance = Balance;
                o.Card = Card;
                o.Cash = Cash;
                o.Change = Change;
                o.ChangeAmount = ChangeAmount;
                o.CreditNoteCode = CreditNoteCode;
                o.CreditNoteTotal = CreditNoteTotal;
                o.Deposit = Deposit;
                o.ShiftID = ShiftID;
                o.CashOut = CashOut;
                o.DateOrder = DateOrder;
                o.SlotID = SlotID;
                o.IsNonSubtotal = IsNonSubtotal;
                o.IsPreOrder = this.IsPreOrder;
                o.Customer = Customer.Copy();
                o.Rego = this.Rego.Copy();
                foreach (int item in ListKeyLoyaltyItem)
                {
                    o.ListKeyLoyaltyItem.Add(item);
                }
                foreach (Item item in ListItem)
                {
                    o.ListItem.Add(item.Coppy());
                    foreach (SubItem sub in item.ListSubItem)
                    {
                        o.ListItem[o.ListItem.Count - 1].ListSubItem.Add(sub.Coppy());
                    }
                }
                foreach (ComboDiscount cb in cboDisc)
                {
                    o.cboDisc.Add(cb);
                }
                foreach (ComboDiscount cb in resultDisc)
                {
                    o.resultDisc.Add(cb);
                }
                foreach (Item item in ListLoyalTy)
                {
                    o.ListLoyalTy.Add(item.Coppy());
                }
                return o;
            }

            public void EditItem(Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                AddVoidItem(item, 1);
                RemoveAlChildren(item.KeyItem, listView1);
                //CheckLoyalty(item, listView1, money);
                this.CheckFuelDiscount(listView1, money);
                this.EditItemListView(item, listView1, money);
            }

            public void EditItemListView(Item item, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                {
                    if (Convert.ToInt32(li.Tag) == item.KeyItem)
                    {
                        string qty = item.Qty + "";
                        item.Weight = item.Weight > 1 ? item.Weight : 1;
                        if (item.Weight > 1)
                        {
                            qty = String.Format("{0:0.000}", (double)item.Qty / item.Weight);
                        }
                        li.SubItems[0].Text = qty;
                        li.SubItems[1].Text = item.ItemName;
                        li.SubItems[2].Text = money.Convert3to2(Convert.ToDouble(money.Format(item.SubTotal))).ToString();
                        li.Selected = true;
                        return;
                    }
                }
            }

            public void EditSubItem(SubItem sub, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                AddVoidSubItem(sub, 1);
                this.CheckFuelDiscount(listView1, money);
                this.EditSubItemListView(sub, listView1, money);
            }

            public void EditSubItemListView(SubItem sub, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                {
                    if (Convert.ToInt32(li.Tag) == sub.KeyItem)
                    {
                        string qty = sub.Qty + "";
                        li.SubItems[0].Text = qty;
                        li.SubItems[1].Text = sub.ItemName;
                        li.SubItems[2].Text = money.Convert3to2(Convert.ToDouble(money.Format(sub.SubTotal))).ToString();
                        li.Selected = true;
                        return;
                    }
                }
            }

            /// <summary>
            /// Lay vi tri cua item tu key ko co tra ve -1
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
            public int GetIndexByKey(int key)
            {
                for (int i = 0; i < ListItem.Count; i++)
                {
                    if (ListItem[i].KeyItem == key)
                    {
                        return i;
                    }
                }
                return ListItem.Count;
            }

            public Item GetItemByKey(int key)
            {
                foreach (Item item in ListItem)
                {
                    if (item.KeyItem == key)
                    {
                        return item;
                    }
                }
                return null;
            }

            public Item GetItemBySub(SubItem sub)
            {
                foreach (var item in ListItem)
                {
                    foreach (var s in item.ListSubItem)
                    {
                        if (s.KeyItem == sub.KeyItem)
                        {
                            return item;
                        }
                    }
                }
                return null;
            }

            public SubItem GetSubItemByKey(int key)
            {
                foreach (var item in ListItem)
                {
                    foreach (var sub in item.ListSubItem)
                    {
                        if (sub.KeyItem == key)
                        {
                            return sub;
                        }
                    }
                }
                return null;
            }

            public double getSubTotal()
            {
                if (IsNonSubtotal)
                {
                    return SubTotal;
                }
                double resuilt = 0;
                foreach (Item item in ListItem)
                {
                    resuilt += item.getTotal();
                }
                foreach (Item item in ListLoyalTy)
                {
                    resuilt += item.getTotal();
                }
                return resuilt;
            }

            public double GetSubTotalNoDiscount()
            {
                double resuilt = 0;
                foreach (Item item in ListItem)
                {
                    double total = item.getTotal();
                    if (total > 0)
                    {
                        resuilt += total;
                    }
                }
                return resuilt;
            }

            public double GetSubTotalRetail()
            {
                double resuilt = 0;
                foreach (Item item in ListItem)
                {
                    if (!item.IsFuel && item.EnableFuelDiscount == 1)
                    {
                        resuilt += item.SubTotal;
                        foreach (SubItem sub in item.ListSubItem)
                        {
                            resuilt += sub.SubTotal;
                        }
                    }
                }
                foreach (Item item in ListLoyalTy)
                {
                    resuilt += item.SubTotal;
                }
                return resuilt;
            }

            public double getTotal()
            {
                double resuilt = 0;
                foreach (Item item in ListItem)
                {
                    //resuilt += item.Qty * item.Price;
                    resuilt += item.SubTotal;
                    foreach (SubItem sub in item.ListSubItem)
                    {
                        resuilt += sub.Price * sub.Qty;
                    }
                }
                return resuilt;
            }

            public double GetTotalGST()
            {
                double totalGST = 0;
                double subTotal = 0;
                double percent = 0;

                subTotal = this.getSubTotal();
                if (subTotal > 0)
                {
                    percent = Discount / subTotal;
                }
                subTotal = 0;

                foreach (var item in this.ListItem)
                {
                    if (item.SubTotal > 0)
                    {
                        subTotal += item.getTotal();
                    }
                }
                //ting gst tat ca ko chua co khuyen mai
                foreach (Item item in ListItem)
                {
                    double total = item.getTotal();
                    if (total > 0)
                    {
                        total = total * (1 - percent);
                        totalGST += total / (100 + item.GST) * item.GST;
                    }
                }
                //tinh gst khuyen mai combo
                foreach (var key in this.ListKeyLoyaltyItem)
                {
                    Item iteamDis = this.GetItemByKey(key);
                    if (iteamDis != null)
                    {
                        double lTotal = 0;
                        foreach (var k in iteamDis.ListKeyItemPrent)
                        {
                            Item i = this.GetItemByKey(k);
                            if (i != null)
                            {
                                lTotal += i.getTotal();
                            }
                        }
                        if (lTotal > 0)
                        {
                            foreach (var k in iteamDis.ListKeyItemPrent)
                            {
                                Item i = this.GetItemByKey(k);
                                if (i != null)
                                {
                                    double total = i.getTotal() * iteamDis.getTotal() / lTotal;
                                    total = total * (1 - percent);
                                    totalGST += total / (100 + i.GST) * i.GST;
                                }
                            }
                        }
                    }
                }
                //tinh gst khuyen mai discount
                foreach (var item in ListItem)
                {
                    if (item.getTotal() <= 0 && item.ParentItem != 0)
                    {
                        Item item_Prent = this.GetItemByKey(item.ParentItem);
                        if (item_Prent != null)
                        {
                            double total = item.getTotal();
                            total = total * (1 - percent);
                            totalGST += total / (100 + item_Prent.GST) * item_Prent.GST;
                        }
                    }
                }
                return totalGST;
            }

            public double GetTotalItemIncluedDiscount(Item item)
            {
                double resuilt = item.getTotal();
                foreach (var i in this.ListItem)
                {
                }
                return resuilt;
            }

            public void InsertAfterKey(Item item, int itemKey, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money)
            {
                MaxKey++;
                item.KeyItem = MaxKey;
                int index = this.GetIndexByKey(itemKey);
                if (index >= ListItem.Count || index < 0)
                {
                    ListItem.Add(item);
                }
                else
                {
                    ListItem.Insert(index + 1, item);
                }
                this.InsertItemListView(item, itemKey, listView1, money, true);
            }

            /// <summary>
            /// them item vao sau itemKey neu key =-1 thi them vao cuoi
            /// </summary>
            /// <param name="item"></param>
            /// <param name="itemKey"></param>
            /// <param name="listView1"></param>
            /// <returns></returns>
            public void InsertItemListView(Item item, int itemKey, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money, bool selected)
            {
                //foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                string qty;
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    System.Windows.Forms.ListViewItem li = listView1.Items[i];
                    if (Convert.ToInt32(li.Tag) == itemKey)
                    {
                        qty = item.Qty + "";
                        item.Weight = item.Weight > 1 ? item.Weight : 1;
                        if (item.Weight > 1)
                        {
                            qty = String.Format("{0:0.000}", (double)item.Qty / item.Weight);
                        }
                        System.Windows.Forms.ListViewItem newLi = new System.Windows.Forms.ListViewItem(qty);
                        newLi.SubItems.Add(item.ItemName);
                        newLi.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(item.SubTotal))).ToString());
                        newLi.SubItems.Add("0");
                        newLi.SubItems.Add("0");
                        newLi.Tag = item.KeyItem;
                        listView1.Items.Insert(i + 1, newLi);
                        return;
                    }
                }
                qty = String.Format("{0:#,###}",item.Qty);
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    //qty = String.Format("{0:0.000}", (double)item.Qty / item.Weight);
                }
                System.Windows.Forms.ListViewItem newLi1 = new System.Windows.Forms.ListViewItem(qty);
                newLi1.SubItems.Add(item.ItemName);
                newLi1.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(item.SubTotal))).ToString());
                newLi1.SubItems.Add(item.IDPumpHistory.ToString());
                newLi1.SubItems.Add(Convert.ToString(Convert.ToInt32(item.PumID.ToString() == "" ? "0" : item.PumID.ToString())));
                newLi1.Tag = item.KeyItem;
                listView1.Items.Add(newLi1);
                foreach (SubItem sub in item.ListSubItem)
                {
                    newLi1 = new System.Windows.Forms.ListViewItem(sub.Qty + "");
                    newLi1.SubItems.Add(sub.ItemName);
                    newLi1.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(sub.SubTotal))).ToString());
                    newLi1.SubItems.Add(item.IDPumpHistory.ToString());
                    newLi1.SubItems.Add(Convert.ToString(Convert.ToInt32(item.PumID.ToString() == "" ? "0" : item.PumID.ToString())));
                    newLi1.Tag = sub.KeyItem;
                    listView1.Items.Add(newLi1);
                }
                newLi1.Selected = selected;
            }

            public void InsertSubItemListView(Item item, SubItem subitem, int subkey, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money, bool selected)
            {
                foreach (SubItem sub in item.ListSubItem)
                {
                    System.Windows.Forms.ListViewItem newLi1 = new System.Windows.Forms.ListViewItem(sub.Qty + "");
                    newLi1.SubItems.Add(sub.ItemName);
                    newLi1.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(sub.SubTotal))).ToString());
                    newLi1.SubItems.Add(item.IDPumpHistory.ToString());
                    newLi1.SubItems.Add(Convert.ToString(Convert.ToInt32(item.PumID.ToString() == "" ? "0" : item.PumID.ToString())));
                    newLi1.Tag = sub.KeyItem;
                    listView1.Items.Add(newLi1);
                    newLi1.Selected = selected;
                }
            }

            public void LoadListView(System.Windows.Forms.ListView listView, Class.MoneyFortmat money, float size)
            {
                listView.Items.Clear();
                //order.shortItem();
                for (int i = 0; i < ListItem.Count; i++)
                {
                    Class.ProcessOrderNew.Item item = ListItem[i];
                    System.Windows.Forms.ListViewItem li = new System.Windows.Forms.ListViewItem(item.Qty + "");
                    li.SubItems.Add(item.ItemName);
                    li.SubItems.Add(money.Format2(item.SubTotal));
                    li.Tag = new ItemClick(i, -1);
                    li.Font = new System.Drawing.Font(listView.Font.SystemFontName, size);

                    listView.Items.Add(li);

                    for (int j = 0; j < item.ListSubItem.Count; j++)
                    {
                        Class.ProcessOrderNew.SubItem sub = item.ListSubItem[j];
                        li = new System.Windows.Forms.ListViewItem(sub.Qty + "");
                        li.SubItems.Add(sub.ItemName);
                        li.SubItems.Add(money.Format(sub.SubTotal));
                        li.Tag = new ItemClick(i, j);
                        li.Font = new System.Drawing.Font(listView.Font.SystemFontName, size);
                        listView.Items.Add(li);
                    }
                    li.EnsureVisible();
                }
                if (listView.Items.Count > 0)
                {
                    listView.Items[listView.Items.Count - 1].Selected = true;
                }
            }

            public bool LoadPanel(System.Windows.Forms.Panel pn, Class.MoneyFortmat money, int count)
            {
                bool result = true;
                int n = count - 1;
                for (int i = 0; i < ListItem.Count; i++)
                {
                    result = false;
                    if (i + count >= ListItem.Count)
                    {
                        UCItemCustomerDisplay uc = (UCItemCustomerDisplay)pn.Controls[n];
                        Class.ProcessOrderNew.Item item = ListItem[i];
                        uc.ItemName = item.ItemName;
                        if (item.IsFuel)
                            uc.Qty = money.Format(item.Qty);
                        else
                            uc.Qty = money.FormatNumber(item.Qty);
                        uc.Subtotal = money.Format2(item.SubTotal);
                        n--;
                    }
                }
                return result;
            }

            public void refreshItemChange()
            {
                foreach (Item item in ListItem)
                {
                    item.Changed = 0;
                }
            }

            /// <summary>
            /// Xoa tat ca con cua itemParentkey
            /// </summary>
            /// <param name="itemParentKey"></param>
            /// <param name="listView1"></param>
            public void RemoveAlChildren(int itemParentKey, System.Windows.Forms.ListView listView1)
            {
                for (int i = 0; i < ListItem.Count; i++)
                {
                    if (ListItem[i].ParentItem == itemParentKey && ListItem[i].EnableRemove)
                    {
                        RemoveListViewItem(ListItem[i].KeyItem, listView1);
                        ListItem.RemoveAt(i);
                        i--;
                    }
                }
            }

            public void RemoveItem(int key, System.Windows.Forms.ListView listView1, Class.MoneyFortmat money, int intIDPumpHistory)
            {
                for (int i = 0; i < ListItem.Count; i++)
                //foreach (Item item in ListItem)
                {
                    Item item = ListItem[i];
                    if (item.KeyItem == key || item.ParentItem == key)
                    {
                        AddVoidItem(item, 0, intIDPumpHistory);
                        ListItem.Remove(item);
                        RemoveListViewItem(item.KeyItem, listView1);
                        i--;
                        for (int j = 0; j < item.ListSubItem.Count; j++)
                        {
                            SubItem sub = item.ListSubItem[j];
                            AddVoidSubItem(sub, 0);
                            RemoveListViewItem(sub.KeyItem, listView1);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < item.ListSubItem.Count; j++)
                        {
                            SubItem sub = item.ListSubItem[j];
                            if (sub.KeyItem == key)
                            {
                                AddVoidSubItem(sub, 0);
                                RemoveListViewItem(sub.KeyItem, listView1);
                                item.ListSubItem.RemoveAt(j);
                                j--;
                                if (item.SubTotal == 0 && item.ListSubItem.Count == 0)
                                {
                                    AddVoidItem(item, 0, intIDPumpHistory);
                                    ListItem.Remove(item);
                                    RemoveListViewItem(item.KeyItem, listView1);
                                    i--;
                                }
                            }
                        }
                    }
                }
                this.CheckFuelDiscount(listView1, money);
            }

            public void RemoveItemToList(int key, System.Windows.Forms.ListView listView1)
            {
                foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                {
                    if (Convert.ToInt32(li.Tag) == key)
                    {
                        listView1.Items.Remove(li);
                    }
                }
            }

            public void RemoveItemToListAndListView(int key, System.Windows.Forms.ListView listView1)
            {
                for (int i = 0; i < ListItem.Count; i++)
                //foreach (Item item in ListItem)
                {
                    Item item = ListItem[i];
                    if (item.KeyItem == key || item.ParentItem == key)
                    {
                        ListItem.RemoveAt(i);
                        RemoveItemToList(item.KeyItem, listView1);
                        i--;
                    }
                }
            }

            /// <summary>
            /// Xoa item o co key la itemkey
            /// </summary>
            /// <param name="itemKey"></param>
            /// <param name="listView1"></param>
            /// <param name="money"></param>
            public void RemoveListViewItem(int itemKey, System.Windows.Forms.ListView listView1)
            {
                foreach (System.Windows.Forms.ListViewItem li in listView1.Items)
                {
                    if (Convert.ToInt32(li.Tag) == itemKey)
                    {
                        listView1.Items.Remove(li);
                        return;
                    }
                }
            }

            public void shortItem()
            {
                for (int i = 0; i < ListItem.Count; i++)
                {
                    for (int j = i + 1; j < ListItem.Count; j++)
                    {
                        if (ListItem[i].compare(ListItem[j]))
                        {
                            ListItem[i].addItem(ListItem[j]);
                            /*
                            ListItem[i].Qty += ListItem[j].Qty;
                            ListItem[i].SubTotal += ListItem[j].SubTotal;
                            for (int k = 0; k < ListItem[i].ListSubItem.Count; k++)
                            {
                                ListItem[i].ListSubItem[k].Qty += ListItem[j].ListSubItem[k].Qty;
                                ListItem[i].ListSubItem[k].SubTotal += ListItem[j].ListSubItem[k].SubTotal;
                            }
                             **/
                            ListItem.RemoveAt(j);
                            j--;
                        }
                    }
                }
            }

            private void AddVoidItem(Item item, int type)
            {
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();

                    if (item.VoidID == 0)
                    {
                        int itemID = 0;
                        int dynID = 0;
                        if (item.ItemType == 0)
                        {
                            itemID = item.ItemID;
                        }
                        else
                        {
                            dynID = item.ItemID;
                        }
                        con.ExecuteNonQuery("insert into voiditemhistory(itemID,dynID,orderID,cableID,type,qty,shiftID,subTotal,staffID) values(" + itemID + "," + dynID + "," + this.OrderID + "," + this.CableID + "," + type + "," + item.Qty + "," + this.ShiftID + "," + item.SubTotal + ",'" + this.StaffID + "')");

                        item.VoidID = Convert.ToInt16(con.ExecuteScalar("select max(vID) from voiditemhistory"));
                    }
                    else
                    {
                        con.ExecuteNonQuery("update voiditemhistory set qty=" + item.Qty + ",staffID='" + this.StaffID + "' where vID=" + item.VoidID);
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("AddVoidItem::" + ex.Message);
                    con.Rollback();
                }
                finally
                {
                    con.Close();
                }
            }

            private void AddVoidItem(Item item, int type, int intIDPumpHistory)
            {
                if (item.IsFuel && item.IDPumpHistory > 0)
                {
                    return;
                }
                Class.LogPOS.WriteLog("Void Item::" + item.ItemID + "====" + item.ItemName + "====" + item.SubTotal);
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    if (item.VoidID == 0)
                    {
                        int itemID = 0;
                        int dynID = 0;
                        if (item.ItemType == 0)
                        {
                            itemID = item.ItemID;
                        }
                        else
                        {
                            dynID = item.ItemID;
                        }
                        con.ExecuteNonQuery("insert into voiditemhistory(itemID,dynID,orderID,cableID,type,qty,shiftID,subTotal,staffID) values(" + itemID + "," + dynID + "," + this.OrderID + "," + this.CableID + "," + type + "," + item.Qty + "," + this.ShiftID + "," + item.SubTotal + ",'" + this.StaffID + "')");
                        con.ExecuteNonQuery("DELETE FROM itemshistory WHERE ItemID = " + (itemID == 0 ? dynID : itemID) + " AND pumpHistoryID = " + intIDPumpHistory);
                        //con.ExecuteNonQuery("DELETE FROM fuelhistory WHERE KindOfFuel = " + itemID + " AND ID = " + intIDPumpHistory);
                        item.VoidID = Convert.ToInt16(con.ExecuteScalar("select max(vID) from voiditemhistory"));
                    }
                    else
                    {
                        con.ExecuteNonQuery("update voiditemhistory set qty=" + item.Qty + ",staffID='" + this.StaffID + "' where vID=" + item.VoidID);
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("AddVoidItem::" + item.ItemID + "====" + item.ItemName + ex.Message);
                    con.Rollback();
                }
                finally
                {
                    con.Close();
                }
            }

            private void AddVoidSubItem(SubItem sub, int type)
            {
                Class.LogPOS.WriteLog("Void Sub Item::" + sub.ItemID + "====" + sub.ItemName + "====" + sub.SubTotal);
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    if (sub.VoidID == 0)
                    {
                        con.ExecuteNonQuery("insert into voiditemhistory(itemID,dynID,optionID,orderID,cableID,type,qty,shiftID,subTotal,staffID) values(0,0," + sub.ItemID + "," + this.OrderID + "," + this.CableID + "," + type + "," + sub.Qty + "," + this.ShiftID + "," + sub.SubTotal + ",'" + this.StaffID + "')");
                        sub.VoidID = Convert.ToInt16(con.ExecuteScalar("select max(vID) from voiditemhistory"));
                    }
                    else
                    {
                        con.ExecuteNonQuery("update voiditemhistory set qty=" + sub.Qty + ",staffID='" + this.StaffID + "' where vID=" + sub.VoidID);
                    }
                    con.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("AddVoidSubItem::" + sub.ItemID + "====" + sub.ItemName + ex.Message);
                    con.Rollback();
                }
                finally
                {
                    con.Close();
                }
            }

            private bool CheckQtyLoyaltyByListItem(List<ItemLoyatyByListItemDetail> list, List<int> listKey, System.Data.DataTable tbl, int qtyDis)
            {
                int resuilt = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    ItemLoyatyByListItemDetail item = list[i];
                    if (item.Qty == 0)
                    {
                        continue;
                    }
                    foreach (System.Data.DataRow rowItem in tbl.Rows)
                    {
                        int itemID = Convert.ToInt16(rowItem["itemID"]);
                        if (itemID == item.ItemID)
                        {
                            listKey.Add(item.KeyItem);
                            if ((item.Qty + resuilt) < qtyDis)
                            {
                                resuilt += item.Qty;
                                item.Qty = 0;
                            }
                            else if ((item.Qty + resuilt) > qtyDis)
                            {
                                //resuilt = qtyDis;
                                item.Qty = item.Qty + resuilt - qtyDis;
                                return true;
                            }
                            else
                            {
                                item.Qty = 0;
                                return true;
                            }
                        }
                    }
                }
                return false;
            }

            private bool ContainItemIDAndQty(List<ItemLoyatyByListItemDetail> list, int itemID, int qty)
            {
                foreach (ItemLoyatyByListItemDetail item in list)
                {
                    if (item.ItemID == itemID && item.Qty >= qty)
                    {
                        //item.QtyTmp = item.Qty;
                        item.Qty = item.Qty - qty;
                        return true;
                    }
                }
                return false;
            }

            private int GetQtyItemIDAndQty(List<ItemLoyatyByListItemDetail> list, List<int> listKey, int itemID, int qtyCurent, int qtyDis)
            {
                int resuilt = 0;
                foreach (ItemLoyatyByListItemDetail item in list)
                {
                    if (item.ItemID == itemID)
                    {
                        listKey.Add(item.KeyItem);
                        if ((item.Qty + qtyCurent) <= qtyDis)
                        {
                            resuilt = item.Qty;
                            item.Qty = 0;
                        }
                        else
                        {
                            resuilt = qtyDis - qtyCurent;
                            item.Qty = item.Qty - resuilt;
                        }
                    }
                }
                return resuilt;
            }

            public class ItemClick
            {
                public ItemClick(int itemIndex, int itemOptionIndex)
                {
                    ItemIndex = itemIndex;
                    ItemOptionIndex = itemOptionIndex;
                }

                public int ItemIndex { get; set; }

                public int ItemOptionIndex { get; set; }
            }

            public class ItemLoyatyByListItem
            {
                public ItemLoyatyByListItem(int loyID, string name, double price)
                {
                    LoyID = loyID;
                    Name = name;
                    Price = price;
                    this.LitstItemKey = new List<int>();
                }

                public List<int> LitstItemKey { get; set; }

                public int LoyID { get; set; }

                public string Name { get; set; }

                public double Price { get; set; }

                public void AddSameItemLoyatyByListItem(ItemLoyatyByListItem item)
                {
                    this.Price += item.Price;
                    foreach (var i in item.LitstItemKey)
                    {
                        if (!this.CheckKey(i))
                        {
                            this.Price += item.Price;
                            this.LitstItemKey.Add(i);
                        }
                    }
                }

                public bool Compare(ItemLoyatyByListItem item)
                {
                    if (this.LoyID != item.LoyID)
                    {
                        return false;
                    }
                    //if (this.LitstItemKey.Count!=item.LitstItemKey.Count)
                    //{
                    //    return false;
                    //}
                    //for (int i = 0; i < this.LitstItemKey.Count; i++)
                    //{
                    //    Item item1 = order.GetItemByKey(this.LitstItemKey[i]);
                    //    Item item2 = order.GetItemByKey(item.LitstItemKey[i]);
                    //    if (item1==null || item2==null)
                    //    {
                    //        return false;
                    //    }
                    //    if (item1.ItemID!=item2.ItemID)
                    //    {
                    //        return false;
                    //    }
                    //}
                    return true;
                }

                public ItemLoyatyByListItem Coppy()
                {
                    ItemLoyatyByListItem loy = new ItemLoyatyByListItem(LoyID, Name, Price);
                    foreach (int item in this.LitstItemKey)
                    {
                        loy.LitstItemKey.Add(item);
                    }
                    return loy;
                }

                private bool CheckKey(int key)
                {
                    foreach (int k in this.LitstItemKey)
                    {
                        if (k == key)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }

            private class ItemLoyatyByListItemDetail
            {
                public ItemLoyatyByListItemDetail(int itemID, int qty, int qtyTmp, int keyitem)
                {
                    ItemID = itemID;
                    Qty = qty;
                    QtyTmp = qtyTmp;
                    this.KeyItem = keyitem;
                }

                public int Group { get; set; }

                public int ItemID { get; set; }

                public int KeyItem { get; set; }

                public int Qty { get; set; }

                public int QtyTmp { get; set; }
            }
        }

        public class SubItem
        {
            public SubItem(
                    int itemID,
                string name,
                    int qty,
                    double subTotal,
                    double price
                )
            {
                ItemID = itemID;
                ItemName = name;
                Qty = qty;
                SubTotal = subTotal;
                Price = price;
            }

            public string BarCode { get; set; }

            public int ItemID { get; set; }

            public string ItemName { get; set; }

            public int KeyItem { get; set; }

            public double Price { get; set; }

            public int Qty { get; set; }

            public int StaffID { get; set; }

            public double SubTotal { get; set; }

            public int VoidID { get; set; }

            public SubItem Coppy()
            {
                SubItem sub = new SubItem(ItemID, ItemName, Qty, SubTotal, Price);
                sub.KeyItem = this.KeyItem;
                sub.VoidID = VoidID;
                sub.StaffID = StaffID;
                return sub;
            }
        }
    }
}