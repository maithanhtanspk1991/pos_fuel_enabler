﻿using System;

namespace POS.Class
{
    public class Departnemt
    {
        public string SortName { get; set; }

        public string FullName { get; set; }

        public int GST { get; set; }

        public Departnemt(string sortName, string fullName, int gst)
        {
            SortName = sortName;
            FullName = fullName;
            GST = gst;
        }

        public static Departnemt getDepartment(string str)
        {
            try
            {
                string[] s = str.Split(';');
                return new Departnemt(s[0], s[1], Convert.ToInt32(s[2]));
            }
            catch (Exception)
            {
            }
            return new Departnemt("Departnemt", "Departnemt", 0);
        }

        public string getDepartmentString()
        {
            return SortName + ";" + FullName + ";" + GST;
        }
    }
}