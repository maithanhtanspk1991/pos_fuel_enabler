﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace CSharpExample
{
    static class AdditionalDataUtility
    {
        public static string AdditionalDataXmlToFormattedString(string additionalDataXml)
        {
            StringBuilder output = new StringBuilder();
            // additional data from transaction doesn't have enclosing tags to make it valid xml
            using (XmlReader reader = XmlReader.Create(new StringReader("<wrapper>" + additionalDataXml + "</wrapper>")))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (!"wrapper".Equals(reader.Name))
                            {
                                output.Append(reader.Name + " : ");
                            }
                            break;
                        case XmlNodeType.Text:
                            output.Append(reader.Value);
                            break;
                        case XmlNodeType.EndElement:
                            output.Append(Environment.NewLine);
                            break;
                    }
                }
            }
            return output.ToString();
        }        
    }
}
