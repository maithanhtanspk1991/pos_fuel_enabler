﻿using System;

namespace POS.Class
{
    public class ReadConfig
    {
        #region ReadWriteConfig

        /// <summary>
        /// Đọc file config
        /// </summary>
        /// <param name="Section">Section</param>
        /// <param name="Key">Key</param>
        /// <param name="value">Giá trị mặc định</param>
        /// <param name="ini">File config</param>
        /// <returns>Trả về kiểu int</returns>
        private int Get(string Section, string Key, int value, clsReadAndWriteINI ini)
        {
            return Convert.ToInt32(GetObject(Section, Key, value, ini));
        }

        /// <summary>
        /// Đọc file config
        /// </summary>
        /// <param name="Section">Section</param>
        /// <param name="Key">Key</param>
        /// <param name="value">Giá trị mặc định</param>
        /// <param name="ini">File config</param>
        /// <returns>Trả về kiểu double</returns>
        private double Get(string Section, string Key, double value, clsReadAndWriteINI ini)
        {
            return Convert.ToDouble(GetObject(Section, Key, value, ini));
        }

        /// <summary>
        /// Đọc file config
        /// </summary>
        /// <param name="Section">Section</param>
        /// <param name="Key">Key</param>
        /// <param name="value">Giá trị mặc định</param>
        /// <param name="ini">File config</param>
        /// <returns>Trả về kiểu string</returns>
        public string Get(string Section, string Key, string value, clsReadAndWriteINI ini)
        {
            return GetObject(Section, Key, value, ini).ToString();
        }

        /// <summary>
        /// Đọc file config
        /// </summary>
        /// <param name="Section">Section</param>
        /// <param name="Key">Key</param>
        /// <param name="value">Giá trị mặc định</param>
        /// <param name="ini">File config</param>
        /// <returns>Trả về kiểu bool</returns>
        private bool Get(string Section, string Key, bool value, clsReadAndWriteINI ini)
        {
            return Convert.ToBoolean(GetObject(Section, Key, value, ini));
        }

        private object GetObject(string Section, string Key, object value, clsReadAndWriteINI ini)
        {
            object result = null;
            try
            {
                result = ini.ReadValue(Section, Key);
                if (!(value is string))
                {
                    if (result == null || result.ToString() == String.Empty)
                    {
                        ini.WriteValue(Section, Key, value.ToString());
                        result = value;
                    }
                }
                else
                {
                    if (result.ToString() == "")
                        ini.WriteValue(Section, Key, value.ToString());
                }
            }
            catch (Exception ex)
            {
                result = value;
                ini.WriteValue(Section, Key, value.ToString());
                Class.LogPOS.WriteLog(String.Format("POS.Class::ReadConfig::GetObject::Section={0}, Key = {1}, Error={2}", Section, Key, ex.Message));
            }
            return result;
        }

        private void SetObject(string Section, string Key, object value)
        {
            clsReadAndWriteINI ini;
            string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            sPath += "\\config.ini";
            ini = new clsReadAndWriteINI(sPath);
            try
            {
                ini.WriteValue(Section, Key, value.ToString());
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(String.Format("POS.Class::ReadConfig::SetObject::Section={0}, Key = {1}, Error={2}", Section, Key, ex.Message));
            }
        }

        #endregion ReadWriteConfig

        public ReadConfig()
        {
            clsReadAndWriteINI ini;
            string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
            sPath += "\\config.ini";
            ini = new clsReadAndWriteINI(sPath);

            NationCode = Get("Barcode", "Nation", "893", ini);
            AccountLimit = Get("Account", "AccountLimit", "1000", ini);
            IsPrinterAccount = Get("Account", "EnablePrinter", false, ini);
            IsEnableCompany = Get("Account", "EnableCompany", true, ini);
            FastFood = Get("FastFood", "isFF", "false", ini);
            BarCode = Get("Barcode", "COMBarCode", "COM1", ini);
            EnableMultiOption = Get("BillConfig", "EnableMultiOption", "0", ini);
            PrinterModel = Get("PrinterModel", "Model", "PRP-085", ini);
            GST = Get("SetGstforItem", "ItemGst", "10", ini);
            DeliveryAmount = Get("DeliveryAmount", "Total", "2", ini);
            IsEnableDeliveryListTab = Get("EnableDeliveryListTab", "Enable", false, ini);
            IsEnablepickupTab = Get("EnablePickupTab", "Enable", false, ini);
            IsEnableStockTab = Get("EnableStockTab", "Enable", false, ini);
            IsEnableWeeklyreport = Get("EnableWeeklyreport", "Enable", false, ini);
            IsEnablePayout = Get("EnablePayout", "Enable", false, ini);
            ImageCustomerDisplay = Get("CustomerDisplay", "PathImage", @"C:\ImageCustomer\", ini);
            iInteval = Get("CustomerDisplay", "Interval", 5000, ini);
            IsEnableShift = Get("Shift", "Enable", true, ini);
            IsAutoStartup = Get("dbconfig", "EnableAutoStartup", false, ini);
            IsAutoShift = Get("Shift", "Auto", true, ini);
            iHourEndShift = Get("TimeShift", "Hour", 4, ini);
            iMinuteEndShift = Get("TimeShift", "Minute", 0, ini);
            iSecondEndShift = Get("TimeShift", "Second", 0, ini);
            IsEnableExitShift = Get("Shift", "EnableExitShift", true, ini);
            iSizeFontCustomer = Get("CustomerDisplay", "FontSize", 15, ini);
            IsAutoSafeDropShift = Get("Shift", "EnableAutoSafeDrop", true, ini);
            MoneyMinAfterSafeDropAuto = Get("Shift", "MoneyAfterAutoSafeDrop", 300000, ini);
            iCableID = Get("dbconfig", "CableID", 0, ini);
            IsServer = Get("Cable", "Server", true, ini);
            IsTyreMenu = Get("Menu", "IsTyreMenu", true, ini);
            IsMechanic = Get("Menu", "IsMechanic", false, ini);

            PrinterTaxinvoice = Get("dbconfig", "BillPrinter", "BP-003", ini);
            PrinterTaxinvoiceA4 = Get("dbconfig", "BillPrinterA4", "BP-003", ini);
            CountTaxtInvoiceForCustomer = Get("dbconfig", "CountTaxtInvoiceForCustomer", 2, ini);

            InfoCustomer = new DataObject.InfoCustomer();
            InfoCustomer.Name = Get("InfoCustomer", "Name", InfoCustomer.Name, ini);
            InfoCustomer.Address = Get("InfoCustomer", "Address", InfoCustomer.Address, ini);
            InfoCustomer.ABN = Get("InfoCustomer", "ABN", InfoCustomer.ABN, ini);
            InfoCustomer.Phone = Get("InfoCustomer", "Phone", InfoCustomer.Phone, ini);
            InfoCustomer.Fax = Get("InfoCustomer", "Fax", InfoCustomer.Fax, ini);
            InfoCustomer.Email = Get("InfoCustomer", "Email", InfoCustomer.Email, ini);
            InfoCustomer.Website = Get("InfoCustomer", "Website", InfoCustomer.Website, ini);
            InfoCustomer.Thank = Get("InfoCustomer", "Thank", InfoCustomer.Thank, ini);
            InfoCustomer.Suport = Get("InfoCustomer", "Suport", InfoCustomer.Suport, ini);

            //Enabler Config
            EnablerConfig = new DataObject.EnablerConfig();
            EnablerConfig.ServerName = Get("Enabler", "ServerName", EnablerConfig.ServerName, ini);
            EnablerConfig.TerminalID = Convert.ToInt32(Get("Enabler", "TerminalID", EnablerConfig.TerminalID, ini));
            EnablerConfig.TerminalPassword = Get("Enabler","TerminalPassword",EnablerConfig.TerminalPassword,ini);
            EnablerConfig.myApplicationName = Get("Enabler","myApplicationName",EnablerConfig.myApplicationName,ini);
            EnablerConfig.Enabler = Convert.ToBoolean(Get("Enabler", "Enable", EnablerConfig.Enabler, ini));
            EnablerConfig.authorizeModePump = Convert.ToBoolean(Get("Enabler", "authorizeModePump", EnablerConfig.authorizeModePump, ini));
            //multi language
            LanguageCode = Get("dbconfig", "LanguageCode", "en", ini);
        }

        public string LanguageCode { get; set; }
        public DataObject.EnablerConfig EnablerConfig { get; set; }
        public int CountTaxtInvoiceForCustomer { get; set; }

        public bool IsAutoStartup { get; set; }
        public string PrinterTaxinvoice { get; set; }
        public string PrinterTaxinvoiceA4 { get; set; }

        public bool IsPrinterAccount { get; set; }
        public bool IsEnableCompany { get; set; }

        public DataObject.InfoCustomer InfoCustomer { get; set; }
        public bool IsServer { get; set; }

        public bool IsTyreMenu { get; set; }

        public bool IsMechanic { get; set; }

        public int iCableID { get; set; }

        public string AccountLimit { get; set; }

        public string BarCode { get; set; }

        public string DeliveryAmount { get; set; }

        public string EnableMultiOption { get; set; }

        public string FastFood { get; set; }

        public string GST { get; set; }

        public int iHourEndShift { get; set; }

        public int iInteval { get; set; }

        public string ImageCustomerDisplay { get; set; }

        public int iMinuteEndShift { get; set; }

        public bool IsAutoSafeDropShift { get; set; }

        public bool IsAutoShift { get; set; }

        public int iSecondEndShift { get; set; }

        public bool IsEnableDeliveryListTab { get; set; }

        public bool IsEnableExitShift { get; set; }

        public bool IsEnablePayout { get; set; }

        public bool IsEnablepickupTab { get; set; }

        public bool IsEnableShift { get; set; }

        public bool IsEnableStockTab { get; set; }

        public bool IsEnableWeeklyreport { get; set; }

        public int iSizeFontCustomer { get; set; }

        public double MoneyMinAfterSafeDropAuto { get; set; }

        public string NationCode { get; set; }

        public string PrinterModel { get; set; }

        public void WritePrinterTaxinvoice(string str)
        {
            SetObject("dbconfig", "BillPrinter", str);
        }

        public void WritePrinterTaxinvoiceA4(string str)
        {
            SetObject("dbconfig", "BillPrinterA4", str);
        }
    }
}