﻿namespace POS.Class
{
    public class CardSubtotal
    {

        public UCCardButton CardButton { get; set; }
        public DataObject.Card Card { get; set; }

        public CardSubtotal(DataObject.Card card, UCCardButton cardButton)
        {
            Card = card;
            CardButton = cardButton;
        }

        public double getSurChar(bool enableSurcharge)
        {
            if (enableSurcharge)
            {
                return (double)Card.Surchart / 100;
            }
            return 0;
        }
    }
}