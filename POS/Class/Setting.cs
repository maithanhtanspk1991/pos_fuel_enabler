﻿using System;

namespace POS.Class
{
    internal class Setting
    {
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Connection.Connection conn = new Connection.Connection();

        public class setprinter
        {
            public string printname { get; set; }

            public string value { get; set; }

            public setprinter(string pn, string val)
            {
                printname = pn;
                value = val;
            }
        }

        public string GetBillPrinter()
        {
            string billprinter = dbconfig.BillPrinter.ToString();
            return billprinter;
        }

        public string getkitchenprinter()
        {
            //try
            //{
            //    conn.Open();
            //    string data = conn.ExecuteScalar("select value from config where varname='Kitchen Printer'").ToString();
            //    string[] s = data.Split(':');
            //    if (s.Length > 1)
            //    {
            //        return "\\\\" + s[0] + "\\" + s[1];
            //    }
            //    return data;
            //}
            //catch (Exception)
            //{
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //return "";
            string kitchenprinter = dbconfig.KitchenPrinter.ToString();
            return kitchenprinter;
        }

        public string getbarprinter()
        {
            //try
            //{
            //    conn.Open();
            //    string data = conn.ExecuteScalar("select value from config where varname='Bar Printer'").ToString();

            //    string[] s = data.Split(':');
            //    if (s.Length>1)
            //    {
            //        return "\\\\" + s[0] + "\\" + s[1];
            //    }
            //    return data;
            //}
            //catch (Exception)
            //{
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //return "";
            try
            {
                string barprinter = dbconfig.BarPrinter.ToString();
                return barprinter;
            }
            catch (Exception)
            {
            }
            return "";
        }
    }
}