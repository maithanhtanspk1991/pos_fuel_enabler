﻿using System.Collections.Generic;

namespace POS.Class
{
    public class ListOrder
    {
        private List<Class.ProcessOrderNew.Order> listOrder;
        private int mCurren = 0;

        public ListOrder()
        {
            listOrder = new List<Class.ProcessOrderNew.Order>();
        }

        public void AddOrder(Class.ProcessOrderNew.Order order)
        {
            if (!listOrder.Contains(order))
            {
                listOrder.Add(order);
            }
        }

        public Class.ProcessOrderNew.Order CurrenOrder()
        {
            if (listOrder.Count > 0 && mCurren < listOrder.Count)
            {
                return listOrder[mCurren];
            }
            else
            {
                return null;
            }
        }

        public Class.ProcessOrderNew.Order NextOrder()
        {
            if (listOrder.Count > 0)
            {
                mCurren++;
                if (mCurren >= listOrder.Count)
                {
                    mCurren = 0;
                }
                return listOrder[mCurren];
            }
            return null;
        }

        public void RemoveOrdrt(Class.ProcessOrderNew.Order order)
        {
            if (listOrder.Contains(order))
            {
                listOrder.Remove(order);
            }
        }

        public string GetIDFuel()
        {
            string result = "";
            foreach (Class.ProcessOrderNew.Order item in listOrder)
            {
                foreach (var sub in item.ListItem)
                {
                    if (sub.IsFuel)
                    {
                        result += sub.IDPumpHistory + ",";
                    }
                }
            }
            if (result != "")
                result = result.Substring(0, result.Length - 1);
            return result;
        }
    }
}