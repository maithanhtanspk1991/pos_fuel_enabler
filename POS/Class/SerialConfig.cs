﻿using System;
using System.Text;

namespace POS.Class
{
    internal class SerialConfig
    {
        private clsReadAndWriteINI ini;

        private string activeCode;
        private string dateStart;
        private string hashCode;

        public int NumOfDay { get; set; }

        public string HashCode
        {
            get { return hashCode; }
            set { hashCode = value; }
        }

        public string DateStart
        {
            get { return dateStart; }
            set { dateStart = value; }
        }

        public string ActiveCode
        {
            get { return activeCode; }
            set { activeCode = value; }
        }

        //public SerialConfig()
        //{
        //    activeCode = "";
        //    try
        //    {
        //        string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
        //        sPath += "\\config.ini";
        //        ini = new clsReadAndWriteINI(sPath);
        //        //strServer = ini.ReadValue("dbconfig", "Server");
        //        //machineCode = ini.ReadValue("Active", "MachineCode");
        //        activeCode = ini.ReadValue("Active", "ActiveCode");
        //    }
        //    catch (Exception)
        //    {
        //        //ini.WriteValue("Active", "MachineCode", "LRS");
        //        ini.WriteValue("Active", "ActiveCode", "");
        //    }
        //}

        public SerialConfig()
        {
            NumOfDay = -1;
            activeCode = "";
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                sPath += "\\config.ini";
                ini = new clsReadAndWriteINI(sPath);
                //strServer = ini.ReadValue("dbconfig", "Server");
                //machineCode = ini.ReadValue("Active", "MachineCode");
                activeCode = ini.ReadValue("Active", "ActiveCode");
            }
            catch (Exception)
            {
                //ini.WriteValue("Active", "MachineCode", "LRS");
                ini.WriteValue("Active", "ActiveCode", "");
            }
        }

        public void WriteActiveCode(string activeCode)
        {
            try
            {
                ini.WriteValue("Active", "ActiveCode", activeCode);
            }
            catch (Exception)
            {
            }
        }

        public string ReadActiveCode()
        {
            try
            {
                activeCode = ini.ReadValue("Active", "ActiveCode");
            }
            catch (Exception)
            {
            }
            return activeCode;
        }

        public bool CheckSerial()
        {
            return new Class.Serial().CheckSerial(ReadActiveCode());
        }

        public void SetupTrial()
        {
            try
            {
                string dateStart = DateTime.Now.ToShortDateString();
                ini.WriteValue("Trial", "DateStart", dateStart);
                ini.WriteValue("Trial", "HashCode", HashString(dateStart));
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private string HashString(string value)
        {
            //StringBuilder sBuilder = new StringBuilder();
            // Create a new instance of the MD5CryptoServiceProvider object.
            System.Security.Cryptography.MD5 md5Hasher = System.Security.Cryptography.MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public bool CheckTrial()
        {
            int days = 0;
            string dateS;
            string HashValue;
            DateTime dtStart;

            try
            {
                days = Convert.ToInt32(ini.ReadValue("Trial", "NumOfDate"));
                if (HashString(days + "") != ini.ReadValue("Trial", "HashNumOfDate"))
                {
                    days = 0;
                }
            }
            catch (Exception)
            {
                ini.WriteValue("Trial", "NumOfDate", days + "");
                ini.WriteValue("Trial", "HashNumOfDate", HashString(days + ""));
            }

            try
            {
                dateS = ini.ReadValue("Trial", "DateStart");
                HashValue = ini.ReadValue("Trial", "HashCode");
                dtStart = Convert.ToDateTime(dateS);
            }
            catch (Exception)
            {
                return false;
            }
            NumOfDay = (int)((TimeSpan)(DateTime.Now - dtStart)).TotalDays;
            int count = 0;
            string HashDate = HashString(dateS);
            if (HashDate != HashValue)
            {
                return false;
            }
            if (NumOfDay < days)
            {
                NumOfDay = days - NumOfDay;
                return true;
            }

            return false;
        }

        public bool LimitTrial()
        {
            Connection.Connection conn = new Connection.Connection();
            string sql = "SELECT if(DATEDIFF(max(ts),min(ts)) is null,0,DATEDIFF(max(ts),min(ts))) from ordersall;";

            bool IsOK = false;
            try
            {
                if (Convert.ToInt32(conn.ExecuteScalar(sql)) <= 30)
                {
                    IsOK = true;
                }
            }
            catch (Exception)
            {
                IsOK = false;
            }
            return IsOK;
        }
    }
}