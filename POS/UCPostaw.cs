﻿using System;
using System.Windows.Forms;

namespace POS
{
    public partial class UCPostaw : UserControl
    {
        public UCPostaw()
        {
            InitializeComponent();
        }

        private void UCPostaw_BackColorChanged(object sender, EventArgs e)
        {
            label3.BackColor = this.BackColor;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }
    }
}