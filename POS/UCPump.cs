﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCPump : UserControl
    {
        public UCPump()
        {
            InitializeComponent();
            PumpID = 0;
        }

        public int PumpID { get; set; }
        protected override void OnBackColorChanged(EventArgs e)
        {
            if (this.BackColor == Color.White)
            {
                lbLitre.Visible = false;
                lbTotal.Visible = false;
            }
            else
            {
                lbLitre.Visible = true;
                lbTotal.Visible = true;
            }
            base.OnBackColorChanged(e);
        }

        private void UCPump_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void UCPump_Load(object sender, EventArgs e)
        {
            lbPumpID.Text = PumpID.ToString();
        }
    }
}