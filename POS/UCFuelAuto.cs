﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace POS
{
    public partial class UCFuelAuto : UserControl
    {
        Class.ReadConfig mReadConfig = new Class.ReadConfig();
        public UCFuelAuto()
        {
            InitializeComponent();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnRestart.Text = "Khởi động lại";
                return;
            }
        }

        private void UCFuelAuto_Load(object sender, EventArgs e)
        {
            GetProcess();
        }

        private void GetProcess()
        {
            if (Class.AutoStartup.GetProcess("FuelAutoBecas"))
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.ForeColor = System.Drawing.Color.Green;
                    lbStatus.Text = "Trạng thái: Bật";
                    return;
                }
                lbStatus.ForeColor = System.Drawing.Color.Green;
                lbStatus.Text = "Status: Enable";
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.ForeColor = System.Drawing.Color.Green;
                    lbStatus.Text = "Trạng thái: Tắt";
                    return;
                }
                lbStatus.ForeColor = System.Drawing.Color.Red;
                lbStatus.Text = "Status: Disable";
            }
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            Class.AutoStartup.Restart("FuelAutoBecas", "FuelAutoBecas.exe");
            GetProcess();
        }
    }
}
