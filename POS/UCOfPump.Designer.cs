﻿namespace POS
{
    partial class UCOfPump
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbName = new System.Windows.Forms.Label();
            this.lbWeight = new System.Windows.Forms.Label();
            this.lbMoney = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lbName.Location = new System.Drawing.Point(0, 0);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(173, 40);
            this.lbName.TabIndex = 0;
            this.lbName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbName.Click += new System.EventHandler(this.lbName_Click);
            // 
            // lbWeight
            // 
            this.lbWeight.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWeight.ForeColor = System.Drawing.Color.Blue;
            this.lbWeight.Location = new System.Drawing.Point(0, 77);
            this.lbWeight.Name = "lbWeight";
            this.lbWeight.Size = new System.Drawing.Size(173, 40);
            this.lbWeight.TabIndex = 1;
            this.lbWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbWeight.Click += new System.EventHandler(this.lbWeight_Click);
            // 
            // lbMoney
            // 
            this.lbMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMoney.ForeColor = System.Drawing.Color.Red;
            this.lbMoney.Location = new System.Drawing.Point(0, 40);
            this.lbMoney.Name = "lbMoney";
            this.lbMoney.Size = new System.Drawing.Size(173, 37);
            this.lbMoney.TabIndex = 2;
            this.lbMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbMoney.Click += new System.EventHandler(this.lbMoney_Click);
            // 
            // UCOfPump
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbMoney);
            this.Controls.Add(this.lbWeight);
            this.Controls.Add(this.lbName);
            this.Name = "UCOfPump";
            this.Size = new System.Drawing.Size(173, 117);
            this.Load += new System.EventHandler(this.UCOfPump_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbWeight;
        private System.Windows.Forms.Label lbMoney;
    }
}
