﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmComboDisc : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat mMoneyFortmat;
        public Barcode.SerialPort mSerialPort;
        private int cboIdDisc = -1;
        int x = 10, width = 344, height = 262;
        //Panel pnListItem = new Panel();
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmComboDisc(Barcode.SerialPort ser)
        {
            InitializeComponent();
            mSerialPort = ser;
            SetMultiLanguage();
        }
        private void frmComboDisc_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            LoadListComboDisc();
            
            //pnListItem.Dock = DockStyle.Fill;
            //pnListItem.AutoScroll = true;
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Gói giảm giá";
                label1.Text = "Tên:";
                label2.Text = "Giảm giá:";
                label3.Text = "Ưu tiên:";
                label4.Text = "Ngày bắt đầu:";
                chkEndDate.Text = "Ngày kết thúc";
                chkEnable.Text = "Cho phép";
                btnNewGroup.Text = "Tạo nhóm mới";
                btnSaveGroup.Text = "Lưu lại";
                btnDeleteGroup.Text = "Xóa";
                colName.Text = "Tên";
                colDiscount.Text = "Giảm giá";
                colDateStart.Text = "Ngày bắt đầu";
                chkEndDate.Text = "Ngày KT";
                colEndDate.Text = "Ngày kết thúc";
                chkEndDate.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                colEnable.Text = "Kích hoạt";
                colPriority.Text = "Ưu tiên";
                btnNewQty.Text = "Tạo số lượng mới";
                btnBack.Text = "QUAY LẠI";
                btnBack.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnBack.TextAlign = ContentAlignment.MiddleCenter;
                return;
            }
        }
        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {                
                //mSerialPort.SetText("", txtBarcode);
                //mSerialPort.SetText(data, txtBarcode);
            }
        }

        private void LoadListComboDisc()
        {
            try
            {
                try
                {
                    mConnection.Open();
                }
                catch { }
                string sql = "select id,name,priceDiscount,dateStart,priority,if(dateEnd='0000-00-00 00:00:00',null,date(dateEnd)) as dateEnd,enable from combodiscount c order by priority asc";
                DataTable tbl = mConnection.Select(sql);
                lvGroup.Items.Clear();
                foreach (DataRow row in tbl.Rows)
                {
                    bool enable;
                    if (row["enable"].ToString() == "1")
                    {
                        enable = true;
                    }
                    else
                    {
                        enable = false;
                    }
                    Class.ComboDiscount comboDiscount;
                    if (row["dateEnd"] != DBNull.Value)
                    {
                        comboDiscount = new Class.ComboDiscount(Convert.ToInt32(row["id"]), row["name"].ToString(), Convert.ToDouble(row["priceDiscount"]), Convert.ToDateTime(row["dateStart"]), Convert.ToDateTime(Encoding.Default.GetString((byte[])row["dateEnd"])), enable, true);
                        //comboDiscount = new Class.ComboDiscount(Convert.ToInt32(row["id"]), row["name"].ToString(), Convert.ToDouble(row["priceDiscount"]), Convert.ToDateTime(row["dateStart"]), Convert.ToDateTime(row["dateEnd"]), enable, true);
                        comboDiscount.Priority = Convert.ToInt32(row["priority"]);
                        
                    }
                    else
                    {
                        comboDiscount = new Class.ComboDiscount(Convert.ToInt32(row["id"]), row["name"].ToString(), Convert.ToDouble(row["priceDiscount"]), Convert.ToDateTime(row["dateStart"]), DateTime.Now, enable, false);
                        comboDiscount.Priority = Convert.ToInt32(row["priority"]);
                    }
                    AddItemListView(comboDiscount);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void AddItemListView(Class.ComboDiscount comboDiscount)
        {
            ListViewItem li = new ListViewItem(comboDiscount.Name);
            li.SubItems.Add("$" + mMoneyFortmat.Format2(comboDiscount.PriceDiscount));
            li.SubItems.Add(GetDate(comboDiscount.DateStart));
            if (comboDiscount.IsDateEnd)
            {
                li.SubItems.Add(GetDate(comboDiscount.DateEnd));
            }
            else
            {
                li.SubItems.Add("");
            }
            li.SubItems.Add(comboDiscount.Priority.ToString());
            if (comboDiscount.Enable)
            {
                li.SubItems.Add("Enable");
            }
            else
            {
                li.SubItems.Add("Disable");
            }
            li.Tag = comboDiscount;
            //li.Selected = true;
            lvGroup.Items.Add(li);
        }

        private string GetDate(DateTime dt)
        {
            return dt.Day + "/" + dt.Month + "/" + dt.Year;
        }

        private void lvGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvGroup.SelectedIndices.Count > 0)
            {
                Class.ComboDiscount comboDiscount = (Class.ComboDiscount)lvGroup.SelectedItems[0].Tag;
                SetTextData(comboDiscount);
                LoadQuantity(comboDiscount.Id);
                cboIdDisc = comboDiscount.Id;
            }
            else
            {
                cboIdDisc = -1;
            }
        }
        private void SetTextData(Class.ComboDiscount comboDiscount)
        {
            btnSaveGroup.Tag = comboDiscount.Priority;
            txtName.Text = comboDiscount.Name;
            txtDiscount.Text = mMoneyFortmat.Format2(comboDiscount.PriceDiscount);
            dtStartDate.Value = comboDiscount.DateStart;
            chkEndDate.Checked = comboDiscount.IsDateEnd;
            dtEndDate.Enabled = comboDiscount.IsDateEnd;
            dtEndDate.Value = comboDiscount.DateEnd;
            chkEnable.Checked = comboDiscount.Enable;
            txtPriority.Text = comboDiscount.Priority.ToString();
        }

        public void LoadQuantity(int id)
        {
            try
            {
                try
                {
                    mConnection.Open();
                }
                catch { }
                
                string sqlQty = "select * from combodiscountrule where comboDiscountId = " + id + " order by qty asc";
                DataTable tblQty = mConnection.Select(sqlQty);
                //344, 262
                int y = 5;
                UCListItem[] _ucListItem = new UCListItem[tblQty.Rows.Count];
                int i = 0;
                foreach (DataRow row in tblQty.Rows)
                {
                    _ucListItem[i] = new UCListItem(Convert.ToInt32(row["id"].ToString()), id, this);
                    _ucListItem[i].Location = new System.Drawing.Point(x, y);
                    _ucListItem[i].Size = new System.Drawing.Size(width, height);
                    _ucListItem[i].txtQuantity.Text = row["qty"].ToString();
                    _ucListItem[i].txtQuantity.Tag = row["qty"].ToString();
                    y += 280;
                    string sqlItem = "select c.*,(select i.itemDesc from itemsmenu i where i.itemID=c.itemId) as Name," +
                                     "(select i.unitPrice from itemsmenu i where i.itemID=c.itemID) as Price " +
                                     "from combodiscountitem c where c.comboDiscountRuleId = " + row["id"];
                    DataTable tblListItem = mConnection.Select(sqlItem);
                    foreach (DataRow rowItem in tblListItem.Rows)
                    {
                        Class.ItemComboDisc item = new Class.ItemComboDisc(Convert.ToInt32(rowItem["id"]), Convert.ToInt32(rowItem["itemId"]), rowItem["Name"].ToString(), Convert.ToDouble(rowItem["Price"]));
                        AddItemListView(item, _ucListItem[i]);
                    }
                    //pnListItem.Controls.Add(_ucListItem[i]);
                    i++;
                }
                pnQuantity.Controls.Clear();
                pnQuantity.Controls.AddRange(_ucListItem);
                btnNewGroup.Tag = y;
            }
            catch(Exception ex)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }
        private void AddItemListView(Class.ItemComboDisc item, UCListItem _ucListItem)
        {
            ListViewItem li = new ListViewItem();
            li.SubItems.Add(item.ItemName);
            li.SubItems.Add("$" + mMoneyFortmat.Format2(item.Price));
            li.Tag = item;
            li.Selected = true;
            _ucListItem.lvListItem.Items.Add(li);
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnNewGroup_Click(object sender, EventArgs e)
        {
            ClearTextData();
            txtPriority.Text = "0";
        }
        private void ClearTextData()
        {
            lvGroup.SelectedIndices.Clear();
            btnSaveGroup.Tag = null;
            txtName.Text = "";
            txtDiscount.Text = "";
            txtPriority.Text = "";
            chkEnable.Checked = true;
            chkEndDate.Checked = false;
            dtStartDate.Value = dtEndDate.Value = DateTime.Now;
            pnQuantity.Controls.Clear();
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (lvGroup.SelectedIndices.Count > 0)
            {
                Class.ComboDiscount comboDiscount = (Class.ComboDiscount)lvGroup.SelectedItems[0].Tag;
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();

                    mConnection.ExecuteNonQuery("delete from combodiscount where id=" + comboDiscount.Id);
                    string sql_rule = "select id from combodiscountrule where comboDiscountId = " + comboDiscount.Id;
                    DataTable tbl_rule = mConnection.Select(sql_rule);
                    foreach (DataRow row in tbl_rule.Rows)
                    {
                        string sql_item = "delete from combodiscountitem where comboDiscountRuleId=" + row["id"];
                        mConnection.ExecuteNonQuery(sql_item);                        
                    }
                    string sql_deleteRule = "delete from combodiscountrule where comboDiscountId=" + comboDiscount.Id;
                    mConnection.ExecuteNonQuery(sql_deleteRule); 
                    lvGroup.Items.RemoveAt(lvGroup.SelectedIndices[0]);
                    mConnection.Commit();
                    ClearTextData();
                    
                }
                catch (Exception)
                {
                    mConnection.Rollback();
                }
                finally
                {
                    mConnection.Close();
                }
            }
            LoadListComboDisc();
        }

        private void btnSaveGroup_Click(object sender, EventArgs e)
        {
            if (btnSaveGroup.Tag == null)
            {//New group
                Class.ComboDiscount comboDiscount = new Class.ComboDiscount();
                if (CheckTextData())
                {
                    GetTextData(comboDiscount);
                    try
                    {
                        mConnection.Open();
                        //priority có thể trùng
                        string sql = "insert into combodiscount(name,priceDiscount,dateStart,dateEnd,enable,priority) " +
                                        "values('" + comboDiscount.Name + "'," + comboDiscount.PriceDiscount + ",'" + GetDateData(comboDiscount.DateStart, true) + "','" + GetDateData(comboDiscount.DateEnd, comboDiscount.IsDateEnd) + "'," + comboDiscount.Enable + "," + comboDiscount.Priority + ")";                        
                        mConnection.ExecuteNonQuery(sql);
                        comboDiscount.Id = Convert.ToInt32(mConnection.ExecuteScalar("select max(id) from combodiscount"));
                        AddItemListView(comboDiscount);
                        ClearTextData();
                        LoadListComboDisc();

                        //priority không được trùng
                        //string sqlCheck = "select id from combodiscount where priority = " + comboDiscount.Priority;
                        //object mObj = mConnection.ExecuteScalar(sqlCheck);
                        //if (mObj == null)
                        //{
                        //    string sql = "insert into combodiscount(name,priceDiscount,dateStart,dateEnd,enable,priority) " +
                        //                "values('" + comboDiscount.Name + "'," + comboDiscount.PriceDiscount + ",'" + GetDateData(comboDiscount.DateStart, true) + "','" + GetDateData(comboDiscount.DateEnd, comboDiscount.IsDateEnd) + "'," + comboDiscount.Enable + "," + comboDiscount.Priority + ")";

                        //    mConnection.ExecuteNonQuery(sql);
                        //    comboDiscount.Id = Convert.ToInt32(mConnection.ExecuteScalar("select max(id) from combodiscount"));
                        //    AddItemListView(comboDiscount);
                        //    ClearTextData();
                        //    LoadListComboDisc();
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Priority is existed!");
                        //}
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog(":::" + ex.Message);
                        mConnection.Rollback();
                    }
                    finally
                    {
                        mConnection.Close();
                    }
                }
                else
                {
                    MessageBox.Show("invalid!");
                }
            }
            else
            {//Update group
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();

                    if (lvGroup.SelectedIndices.Count > 0)
                    {
                        Class.ComboDiscount comboDiscount = (Class.ComboDiscount)lvGroup.SelectedItems[0].Tag;
                        if (CheckTextData())
                        {
                            GetTextData(comboDiscount);
                            //priority có thể trùng
                            int enable = 0;
                            if (comboDiscount.Enable)
                            {
                                enable = 1;
                            }
                            mConnection.ExecuteNonQuery("update combodiscount set name='" + comboDiscount.Name + "',priceDiscount=" + comboDiscount.PriceDiscount + ",dateStart='" + GetDateData(comboDiscount.DateStart, true) + "',dateEnd='" + GetDateData(comboDiscount.DateEnd, comboDiscount.IsDateEnd) + "',enable=" + enable + ",priority=" + comboDiscount.Priority + " where id=" + comboDiscount.Id);
                            mConnection.Commit();
                            ClearTextData();
                            LoadListComboDisc();

                            //priority không được trùng
                            //string sqlCheck = "select id from combodiscount where priority = " + comboDiscount.Priority + " and priority != " + btnSaveGroup.Tag;
                            //object mObj = mConnection.ExecuteScalar(sqlCheck);
                            //if (mObj == null)
                            //{
                            //    int enable = 0;
                            //    if (comboDiscount.Enable)
                            //    {
                            //        enable = 1;
                            //    }
                            //    mConnection.ExecuteNonQuery("update combodiscount set name='" + comboDiscount.Name + "',priceDiscount=" + comboDiscount.PriceDiscount + ",dateStart='" + GetDateData(comboDiscount.DateStart, true) + "',dateEnd='" + GetDateData(comboDiscount.DateEnd, comboDiscount.IsDateEnd) + "',enable=" + enable + ",priority=" + comboDiscount.Priority + " where id=" + comboDiscount.Id);
                            //    mConnection.Commit();
                            //    ClearTextData();
                            //    LoadListComboDisc();
                            //}
                            //else
                            //{
                            //    MessageBox.Show("Priority is existed!");
                            //}
                        }
                        else
                        {
                            MessageBox.Show("invalid!");
                        }
                    } 
                }
                catch (Exception ex)
                {
                    mConnection.Rollback();
                    Class.LogPOS.WriteLog(":::" + ex.Message);
                }
                finally
                {
                    mConnection.Close();
                }
            }
            //LoadListComboDisc();
        }
        private bool CheckTextData()
        {
            if (txtName.Text.Trim().Length > 0 && txtDiscount.Text.Trim().Length > 0 && txtPriority.Text.Trim().Length > 0)
            {
                return true;
            }
            return false;
        }
        private void GetTextData(Class.ComboDiscount comboDiscount)
        {
            comboDiscount.Name = txtName.Text;
            comboDiscount.PriceDiscount = mMoneyFortmat.getFortMat(txtDiscount.Text);
            comboDiscount.DateStart = dtStartDate.Value;
            comboDiscount.DateEnd = dtEndDate.Value;
            comboDiscount.Enable = chkEnable.Checked;
            comboDiscount.IsDateEnd = chkEndDate.Checked;
            comboDiscount.Priority = Convert.ToInt32(txtPriority.Text);
        }

        private string GetDateData(DateTime dt, bool isDate)
        {
            if (isDate)
            {
                return dt.Year + "-" + dt.Month + "-" + dt.Day;
            }
            else
            {
                return "0000-00-00";
            }
        }

        private void chkEndDate_CheckedChanged(object sender, EventArgs e)
        {
            dtEndDate.Enabled = chkEndDate.Checked;
        }

        private void chkEnable_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lvGroup_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            // Mặc định là vẽ cột tiêu đề
            e.DrawDefault = true;
        }

        private void lvGroup_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            // Xác định điều kiện cột cần vẽ
            if (e.Header == this.colEnable)
            {
                // Tạo ảnh Icon tùy theo giới tính
                Image icon = Image.FromFile("UnCheck.png");
               // Image icon;// = Image.FromFile("checkbox.png");
                Class.ComboDiscount cbDisc = (Class.ComboDiscount)e.Item.Tag;
                if (cbDisc.Enable)
                    icon = Image.FromFile("checkbox.png");

                // Tạo kích thước vẽ ảnh, cách viền 10px
                var imageRect = new Rectangle(e.Bounds.X + 22, e.Bounds.Y, e.Bounds.Height, e.Bounds.Height);
               //  Vẽ ảnh
                e.Graphics.DrawImage(icon, imageRect);
            }
              else 
            {
                // Mặc định là vẽ Subitem này như thông thường
                e.DrawDefault = true;
            }           
        }

        private void btnNewQty_Click(object sender, EventArgs e)
        {
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                if (cboIdDisc != -1)
                {
                    string sqltest = "select * from combodiscountrule where  qty = 0 and comboDiscountId = " + cboIdDisc + " limit 0,1";
                    DataTable tblTest = mConnection.Select(sqltest);
                    if (tblTest.Rows.Count <= 0)
                    {
                        string sql = "insert into combodiscountrule(comboDiscountId) values(" + cboIdDisc + ")";
                        mConnection.ExecuteNonQuery(sql);
                        mConnection.Commit();
                        LoadQuantity(cboIdDisc);
                    }
                }
                
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }
    }
}
