﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmKeyboardNew : Form
    {
        private bool ktdot;
        private bool capslock;
        private bool IsShift;
        private TextBox mTextBox;

        public frmKeyboardNew()
        {
            InitializeComponent();
        }

        public frmKeyboardNew(TextBox txt)
        {
            InitializeComponent();
            mTextBox = txt;
            capslock = false;
            IsShift = true;
            ChangerCaplock(!capslock);
            //IsShift = false;

            mTextBox = txt;
            if (mTextBox.Text != "" || mTextBox.Text != null)
            {
                this.Tag = "1";
            }
            ChangerCaplock(!capslock);
        }

        private void btnthan_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnthan.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnthan.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "*";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnacong_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnacong.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnacong.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "@";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnthang_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnthang.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnthang.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "#";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btndola_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btndola.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btndola.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "$";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnphantram_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnphantram.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("{" + btnphantram.Text + "}");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "%";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnbang_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnbang.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnbang.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "=";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnmu_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnmu.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("{" + btnmu.Text + "}");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "^";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnsao_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnsao.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnsao.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "*";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btntronmo_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btntronmo.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("{" + btntronmo.Text + "}");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "(";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btntrondong_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btntrondong.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("{" + btntrondong.Text + "}");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += ")";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            //int l = mTextBox.Text.Length;
            //if (l > 0)
            //{
            //    //mTextBox.Text = mTextBox.Text.Remove(l - 1);
            //    if (mTextBox == null)
            //    {
            //        return;
            //    }
            //    mTextBox.Focus();
            //    SendKeys.Send("{BS}");
            //    if (mTextBox.Text.Contains("."))
            //    {
            //        ktdot = true;
            //    }
            //    else
            //    {
            //        ktdot=false;
            //    }
            //}
            //if (mTextBox.Text.Length > 0)
            //{
            //    if (mTextBox.Text[mTextBox.Text.Length - 1] == ' ')
            //    {
            //        if (!capslock)
            //        {
            //            IsShift = true;
            //            ChangerCaplock(!capslock);
            //        }
            //    }
            //    else
            //    {
            //        ChangerCaplock(capslock);
            //    }
            //}
            //else
            //{
            //    if (!capslock)
            //    {
            //        IsShift = true;
            //        ChangerCaplock(!capslock);
            //    }
            //}

            if (mTextBox.Text.Length > 0 || this.Tag == "1")
            {
                string text = mTextBox.Text;
                if (text.Length > 0)
                {
                    mTextBox.Text = text.Remove(text.Length - 1, 1);
                }
                this.Tag = "0";
            }
            else
            {
                this.Tag = "1";
            }
        }

        private void btnnhonmo_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnnhonmo.Text;
            if (mTextBox == null)
            {
                return;
            }
            mTextBox.Focus();
            SendKeys.Send("{LEFT}");
        }

        private void btnnhondong_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnnhondong.Text;
            if (mTextBox == null)
            {
                return;
            }
            mTextBox.Focus();
            SendKeys.Send("{RIGHT}");
        }

        private void btnhoac_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnhoac.Text;
            if (mTextBox == null)
            {
                return;
            }
            mTextBox.Focus();
            SendKeys.Send("{END}");
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            mTextBox.Text = "";
            ktdot = false;
            if (!capslock)
            {
                IsShift = true;
                ChangerCaplock(!capslock);
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "7";
            //Tuan xoa
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("7");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 7;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "8";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("8");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 8;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "9";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("9");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 9;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "6";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("6");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 6;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "5";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("5");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 5;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "4";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("4");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 4;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "3";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("3");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 3;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "2";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("2");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 2;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "1";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("1");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 1;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += "0";
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("0");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += 0;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnchia_Click(object sender, EventArgs e)
        {
            ////mTextBox.Text += btnchia.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnchia.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "/";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnchamphay_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnchamphay.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnchamphay.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += ";";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnphay_Click(object sender, EventArgs e)
        {
            ////mTextBox.Text += btnphay.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnphay.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += ",";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btncapslock_Click(object sender, EventArgs e)
        {
            if (capslock == true)
            {
                capslock = false;
                ChangerCaplock(capslock);
            }
            else
            {
                capslock = true;
                ChangerCaplock(capslock);
            }
        }

        private void ChangerCaplock(bool key)
        {
            foreach (Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    char ch = '.';
                    try
                    {
                        ch = Convert.ToChar(control.Tag);
                    }
                    catch (Exception)
                    { }
                    if (ch <= 'z' && ch >= 'a')
                    {
                        if (key)
                        {
                            control.Text = (char)(ch - 'a' + 'A') + "";
                        }
                        else
                        {
                            control.Text = ch + "";
                        }
                    }
                }
            }
        }

        private void btnhaicham_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnhaicham.Text;\
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnhaicham.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += ":";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnenter_Click(object sender, EventArgs e)
        {
            try
            {
                mTextBox.Text = mTextBox.Text;
                btnexit_Click(sender, e);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmKeyboard.btnenter_Click:::" + ex.Message);
            }
        }

        private void btnnhohon_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnnhohon.Text;
            if (mTextBox == null)
            {
                return;
            }
            mTextBox.Focus();
            SendKeys.Send("{HOME}");
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(".");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += ".";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += ".";
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnvuongdong_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnvuongdong.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnvuongdong.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "]";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btndauhoi_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btndauhoi.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btndauhoi.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "?";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btncong_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btncong.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send("{add}");
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "+";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnspace_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += " ";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += " ";
            }
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(" ");
            if (!capslock)
            {
                IsShift = true;
                ChangerCaplock(!capslock);
            }
        }

        private void btntru_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btntru.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btntru.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "-";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnvuongmo_Click(object sender, EventArgs e)
        {
            //mTextBox.Text += btnvuongmo.Text;
            //if (mTextBox == null)
            //{
            //    return;
            //}
            //mTextBox.Focus();
            //SendKeys.Send(btnvuongmo.Text);
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += "[";
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnshift_Click(object sender, EventArgs e)
        {
            IsShift = true;
            ChangerCaplock(!capslock);
        }

        private void frmKeyboard_Load(object sender, EventArgs e)
        {
            if (mTextBox != null)
            {
                mTextBox.Text = mTextBox.Text;
            }
        }

        private void mTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnKey_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += btn.Text;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
            if (mTextBox == null)
            {
                return;
            }
            mTextBox.Focus();
            SendKeys.Send(btn.Text);
            if (IsShift)
            {
                ChangerCaplock(capslock);
                IsShift = false;
            }
        }
    }
}