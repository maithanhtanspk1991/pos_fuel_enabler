﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Forms.FindObject;
using POS.Properties;
using Tyro.Integ;
using Tyro.Integ.Domain;
using CSharpExample;

namespace POS.Forms
{
    public partial class frmSubTotal : Form
    {
        private const string _nameClass = "POS::Forms::frmSubTotal::";
        public static bool blnIsUseCreditNote = false;

        public static string strCreditCode = "";

        public Barcode.SerialPort mSerialPort;

        public string strDataReceive = "";

        //private AxCSDEFTLib.AxCsdEft axCsdEft;
        private CardInterface.POSAxCsdEft axCsdEft;

        private bool blnEnd = false;

        private bool blnIsFocus = false;

        private Connection.Connection con;

        private int depositID = 0;

        private frmCustomerDisplay frmCustomerDisplay;

        private Button mButtonCurent;

        private double mChange = 0;

        private int mCustID = 0;

        private Class.CustomerDisplayVFD mCustomerDisplayVFD;

        private SystemConfig.DBConfig mDBConfig;

        private bool mIsLockBarCode = false;

        private bool mLockTextChange = false;

        private Class.MoneyFortmat mMoney;

        private double mTotal = 0;

        private DataObject.ShiftReport mshift;

        private DataObject.Customers mCustomers = null;

        private Class.ReadConfig mReadConfig;
        //Tyro Swipe Card
        private TerminalAdapter _tyroAdapter;
        private POSInformation _tyroPosInfo;
        public frmSubTotal(frmCustomerDisplay frm, Class.MoneyFortmat m, Class.CustomerDisplayVFD cus, Barcode.SerialPort barcode, CardInterface.POSAxCsdEft axC, SystemConfig.DBConfig dBConfig, DataObject.ShiftReport shift, Class.ReadConfig readConfig)
        {
            InitializeComponent();
            SetMultiLanguage();
            mReadConfig = readConfig;
            blnEnd = false;
            this.axCsdEft = axC;
            mSerialPort = barcode;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            con = new Connection.Connection();
            frmCustomerDisplay = frm;
            mMoney = m;
            mCustomerDisplayVFD = cus;
            LoadCard();
            InitCardAx();
            mDBConfig = dBConfig;
            mshift = shift;
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Thành Tiền";
                btnAccept.Text = "CHẤP NHẬN";
                btnCancel.Text = "THOÁT";
                //btnUP.Text = "LÊN";
                //btnDown.Text = "XUỐNG";
                btnClear.Text = "XÓA";
                btnCustomer.Text = "KHÁCH HÀNG";
                buttonCard.Text = "THẺ";
                buttonCash2.Text = "TIỀN MẶT";
                buttonDeposit.Text = "GHI NỢ";
                label1.Text = "ĐANG NỢ";
                label3.Text = "TỔNG CỘNG";
                label2.Text = "GIẢM GIÁ";
                label11.Text = "RÚT TIỀN";
                label6.Text = "THẺ";
                label4.Text = "PHỤ THU";
                checkBoxEnableShurchart.Text = "KÍCH HOẠT PHỤ THU";
                label7.Text = "TIỀN DƯ";
                label13.Text = "SỐ XE";
                lblChoseDebtObject.Text = "KHÁCH HÀNG";
                buttonCash.Text = "TIỀN MẶT";
                label5.Text = "TIỀN KH ĐƯA";
                btnDoTransaction.Text = "KÉO THẺ";
                btnOpenTill.Text = "MỞ KÉT TIỀN";

                return;
            }
        }

        public Class.ProcessOrderNew.Order Order { get; set; }

        public void ShowFromManagerCar(int CustID)
        {
            if (CustID != 0)
            {
                DataObject.Rego item = new DataObject.Rego();
                item.CustID = CustID;
                List<DataObject.Rego> lsArray = new List<DataObject.Rego>();
                lsArray = BusinessObject.BORego.GetAll(CustID);
                if (lsArray.Count > 0)
                {
                    if (lsArray.Count == 1)
                    {
                        txtPlate.Text = lsArray[0].CarNumber;
                        txtPlate.Tag = lsArray[0].ID;
                        Order.RegoID = lsArray[0].ID;
                    }
                    else
                    {
                        frmChoseCar frmChoseCar = new frmChoseCar(lsArray, mMoney);
                        if (frmChoseCar.ShowDialog() == DialogResult.OK)
                        {
                            txtPlate.Text = frmChoseCar.mRego.CarNumber;
                            txtPlate.Tag = frmChoseCar.mRego.ID;
                            Order.RegoID = frmChoseCar.mRego.ID;
                        }
                    }
                }
            }
        }

        private void LoadDisplayCustomer()
        {
            if (Order.Customer.CustID > 0)
            {
                IsLockCustomerCodeTextChanged = true;
                btnCustomer.Enabled = false;
                this.txtPlate.Text = Order.Rego.CarNumber;
                this.txtCustomerCode.Text = Order.Customer.MemberNo;
                txtPlate.Tag = Order.RegoID;
                mCustomers = BusinessObject.BOCustomers.GetByID(Order.Customer.CustID);
            }
            else
            {
                btnCustomer.Enabled = true;
            }
        }

        private void axCsdEft_PrintReceiptEvent(object sender, AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEvent e)
        {
            if (e.receiptType.Length < 1) return;
            switch (e.receiptType[0])
            {
                case 'R': // Receipt data is ready to print!
                    strDataReceive += axCsdEft.Receipt + " \\ ";
                    break;

                case 'C':
                    strDataReceive += "Customer Receipt \\ ";
                    break;

                case 'M':
                    strDataReceive += "Merchant Receipt \\ ";
                    break;

                case 'S':
                    strDataReceive += "Settlement Receipt \\ ";
                    break;

                case 'L':
                    strDataReceive += "Logon Receipt \\ ";
                    break;

                case 'A':
                    strDataReceive += "Audit Receipt \\ ";
                    break;

                default:
                    strDataReceive += "Unknown Receipt \\ ";
                    break;
            }
        }

        private void axCsdEft_TransactionEvent(object sender, EventArgs e)
        {
            try
            {
                Class.LogPOS.WriteLog(_nameClass + "axCsdEft_TransactionEvent::" + axCsdEft.Success + "--" + blnEnd);
                if (axCsdEft.Success == true)
                {
                    Order.AuthCode = axCsdEft.AuthCode;
                    Order.CardType = axCsdEft.CardType;
                    Order.AccountType = axCsdEft.AccountType;
                    Order.Pan = axCsdEft.Pan;
                    Order.DateExpiry = axCsdEft.DateExpiry;
                    Order.Date = axCsdEft.Date;
                    Order.Time = axCsdEft.Time;
                    Order.AmtPurchase = axCsdEft.AmtPurchase;
                    Order.AmtCash = axCsdEft.AmtCash;
                    btnCancel.Enabled = false;
                    btnCustomer.Enabled = false;
                    btnAccept.Enabled = true;
                    if (blnEnd == false)
                    {
                        try
                        {
                            btnAccept_Click(sender, e);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    blnEnd = true;
                }
                else
                    if (axCsdEft.Success == false)
                    {
                        string strSQL = "insert into cardreject(AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,ShiftID) " +
                                        "values('" + axCsdEft.AuthCode + "','"
                                                   + axCsdEft.CardType + "','"
                                                   + axCsdEft.Pan + "','"
                                                   + axCsdEft.DateExpiry + "','"
                                                   + axCsdEft.DateExpiry + "','"
                                                   + axCsdEft.Time + "','"
                                                   + axCsdEft.CardType + "','"
                                                   + mMoney.getFortMat(Convert.ToDouble(axCsdEft.AmtPurchase)) + "','"
                                                   + mMoney.getFortMat(Convert.ToDouble(axCsdEft.AmtCash)) + "','"
                                                   + Order.ShiftID + "')";
                    }
                lbStatus.Text = axCsdEft.ResponseText;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "axCsdEft_TransactionEvent::" + ex.Message);
            }
        }

        private void btnCard_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = (Button)sender;
                mButtonCurent = btn;
                ExecuteCardClick(btn, false);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnCard_Click::" + ex.Message);
            }
        }
        private Class.ListCardType mListCardType = new ListCardType();

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnCustomer_Click");
            btnCustomer.Image = (Bitmap)Resources.checkbox;
            btnEmployee.Image = (Bitmap)Resources.UnCheck;
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                lblChoseDebtObject.Text = "KHÁCH HÀNG";               
            }
            else
            {
                lblChoseDebtObject.Text = "Customer";
            }
            txtCustomerCode.Text = "";
            txtCustomerCode.Focus();
            frmFindCustomer frm = new frmFindCustomer(mReadConfig);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                mIsLockBarCode = true;
                txtCustomerCode.Text = frm.mCustomers.MemberNo;
                mIsLockBarCode = false;
                mCustID = frm.mCustomers.CustID;
                Order.Customer = frm.mCustomers;
                mCustomers = Order.Customer;
                btnCustomer.Tag = Order.Customer;
                ShowFromManagerCar(mCustID);
            }
        }
        private TerminalAdapter tyroAdapter
        {
            get
            {
                if (_tyroAdapter == null)
                {
                    _tyroAdapter = new TerminalAdapter(_tyroPosInfo);
                    _tyroAdapter.ReceiptReturned += ReceiptReturned;
                    _tyroAdapter.ErrorOccured += ErrorOccured;
                    _tyroAdapter.TransactionCompleted += TransactionCompleted;
                }
                return _tyroAdapter;
            }
        }
        private static void ReceiptReturned(Receipt receipt)
        {
            //receipt.Text for receit content
            MessageBox.Show(receipt.Text);
            //Signature
            //receipt.SignatureRequired; (bool)
        }
        private void TransactionCompleted(Transaction transaction)
        {
            MessageBox.Show(String.Format("Status: {0} {1}Result: {2} {1}Authorisation Code: {3} {1}Transaction Reference: {4} {1}Card Type: {5} {1}Generated TransactionID: {6} {1}Tip Amount: {7} {1}Tip Completion Reference: {8} {1} Extra Data: {9} \n",
                                          transaction.Status, Environment.NewLine, transaction.Result, transaction.AuthorisationCode, transaction.ReferenceNumber, transaction.CardType, transaction.ID, transaction.TipAmount,
                                          transaction.TipCompletionReference, AdditionalDataUtility.AdditionalDataXmlToFormattedString(transaction.GetExtraData())));

            if (transaction.GetResult().Equals("APPROVED"))
            {
                MessageBox.Show("POS approves the sale", "Infomation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textBoxCard.Text = this.lblBalance.Text;
            }
            else if (transaction.GetResult().Equals("DECLINED"))
            {
                MessageBox.Show("POS does not approve the sale (declined)","Warnning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            else if (transaction.GetResult().Equals("CANCELLED"))
            {
                MessageBox.Show("POS does not approve the sale (cancelled)", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (transaction.GetResult().Equals("REVERSED "))
            {
                MessageBox.Show("POS does not approve the sale (reversed)", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (transaction.GetResult().Equals("SYSTEM ERROR"))
            {
                MessageBox.Show("POS does not approve the sale (system error)", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
           
        }
        private static void ErrorOccured(Error error)
        {
            MessageBox.Show(String.Format("ERROR: {0} {1}Transaction Started: {2}",
                                          error.ErrorMessage, Environment.NewLine, error.TransactionStarted));
        }
        public void Purchase(double amount, double cashOut, int transactionId = 0)
        {
            if (transactionId != 0)
            {
                tyroAdapter.Purchase(Convert.ToInt32(amount * 100), Convert.ToInt32(cashOut * 100), transactionId.ToString());
            }
            else
            {
                tyroAdapter.Purchase(Convert.ToInt32(amount * 100), Convert.ToInt32(cashOut * 100));
            }
        }

        public void Refund(double amount, int transactionId = 0)
        {
            if (transactionId > 0)
            {
                tyroAdapter.Refund(Convert.ToInt32(amount * 100), transactionId.ToString());
            }
            else
            {
                tyroAdapter.Refund(Convert.ToInt32(amount * 100));
            }
        }
        private void btnDoTransaction_Click(object sender, EventArgs e)
        {
            _tyroPosInfo = new POSInformation("ACME", "ACME POS", "1.2.3", "123 Happy Lane");
            double amount = Convert.ToDouble(lblSubTotal.Text);
            double cashOut = 0;
            if (txtCashOut.Text != "")
            {
                cashOut = Convert.ToDouble(this.txtCashOut.Text);
            }
            this.Purchase(amount, cashOut);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnDown_Click");
            try
            {
                this.flCard.VerticalScroll.Value += 100;
                flCard.PerformLayout();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnDown_Click::" + ex.Message);
            }
        }

        private void btnUP_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnUP_Click");
            try
            {
                this.flCard.VerticalScroll.Value -= 100;
                flCard.PerformLayout();
            }
            catch (Exception)
            {
                this.flCard.VerticalScroll.Value = 0;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnCancel_Click");
            try
            {
                frmCustomerDisplay.CancelSubtotal();
                if (mCustomerDisplayVFD != null)
                {
                    mCustomerDisplayVFD.ClearScreen();
                    mCustomerDisplayVFD.DisplayWelcome();
                }
                axCsdEft.Tag = null;
                if (btnCustomer.Tag is DataObject.Customers)
                {
                    Order.Customer = new DataObject.Customers();
                    Order.CustCode = "";
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnCancel_Click::" + ex.Message);
            }
            this.Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click");
            Order.Tendered = 0;
            Order.ChangeAmount = 0;
            if (CheckSubTotal())
            {
                try
                {
                    double discount = 0;
                    try
                    {
                        discount = Convert.ToDouble(txtDiscount.Text);
                    }
                    catch { }

                    discount = Math.Round(discount * Convert.ToDouble(lblBalance.Text) / 100, 2);
                    Order.Discount = mMoney.getFortMat(discount);
                    Order.CashOut = mMoney.getFortMat(txtCashOut.Text);
                    Order.Card = mMoney.getFortMat(textBoxCard.Text);
                    Order.Cash = mMoney.getFortMat(textBoxTendered.Text);
                    Order.Tendered = mMoney.getFortMat(textBoxTendered.Text);
                    Order.SubTotal = mMoney.getFortMat(lblBalance.Text);

                    if (txtPlate.Tag is int)
                        Order.RegoID = Convert.ToInt32(txtPlate.Tag);
                    else
                        Order.RegoID = 0;

                    if (Order.Delivery != 0)
                    {
                        Order.CustCode = txtCustomerCode.Text;
                        Order.Paid = 1;
                    }

                    if (Order.Cash > 0)
                    {
                        try
                        {
                            Order.ChangeByCash = mMoney.getFortMat(lblChange.Text);
                        }
                        catch (Exception ex)
                        {
                            Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                        }
                    }
                    int i = 0;
                    Order.MoreOrderByCard = new List<OrderByCard>();
                    foreach (UCCardButton uccCard in this.flCard.Controls)
                    {
                        i++;
                    }
                    double dblCountSurchart = 0;
                    if (i != 0)
                    {
                        foreach (UCCardButton uccCard in this.flCard.Controls)
                        {
                            CardSubtotal cardSubTotal = (CardSubtotal)uccCard.btnCard.Tag;
                            if (cardSubTotal.Card.IsActive)
                            {
                                OrderByCard orderBC = new OrderByCard();
                                orderBC.orderbycardID = DateTime.Now.Year + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                                orderBC.cardID = cardSubTotal.Card.CardID;
                                orderBC.subtotal = Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text);
                                orderBC.cashOut = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                                orderBC.orderID = Order.OrderID;
                                orderBC.nameCard = uccCard.lblNameCard.Text;
                                orderBC.shiftID = Order.ShiftID;
                                orderBC.surcharge = Convert.ToDouble(txtSurcharge.Text == "" ? "0" : txtSurcharge.Text);
                                try
                                {
                                    if (Order.AuthCode != "" || Order.AuthCode != null) { orderBC.AuthCode = Order.AuthCode; } else { orderBC.AuthCode = ""; }
                                    if (Order.AccountType != "" || Order.AccountType != null) { orderBC.AccountType = Order.AccountType; } else { orderBC.AccountType = ""; }
                                    if (Order.Pan != "" || Order.Pan != null) { orderBC.Pan = Order.Pan; } else { orderBC.Pan = ""; }
                                    if (Order.DateExpiry != "" || Order.DateExpiry != null) { orderBC.DateExpiry = Order.DateExpiry; } else { orderBC.DateExpiry = ""; }
                                    if (Order.Date != "" || Order.Date != null) { orderBC.Date = Order.Date; } else { orderBC.Date = ""; }
                                    if (Order.Time != "" || Order.Time != null) { orderBC.Time = Order.Time; } else { orderBC.Time = ""; }
                                    if (Order.CardType != "" || Order.CardType != null) { orderBC.CardType = Order.CardType; } else { orderBC.CardType = ""; }
                                    if (Order.AmtPurchase != 0) { orderBC.AmtPurchase = Order.AmtPurchase; } else { orderBC.AmtPurchase = 0; }
                                    if (Order.AmtCash != 0) { orderBC.AmtCash = Order.AmtPurchase; } else { orderBC.AmtCash = 0; }
                                }
                                catch (Exception ex)
                                {
                                    Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                                }
                                Order.MoreOrderByCard.Add(orderBC);

                                dblCountSurchart += Convert.ToDouble(txtSurcharge.Text == "" ? "0" : txtSurcharge.Text);
                            }
                        }
                    }
                    else
                    {
                        Order.OrderByCardID = "0";
                    }
                    if (dblCountSurchart != 0)
                    {
                        Order.Surchart = mMoney.getFortMat(dblCountSurchart);
                    }
                    Order.CreditNoteCode = txtCodeCreditNote.Text;
                    Order.ChangeAmount = mMoney.getFortMat(lblChange.Text);
                    if ((Order.SubTotal - Order.Discount + Order.Tendered + Order.Card + Order.CreditNoteTotal) >= 0 && (Order.CreditNoteTotal < Order.SubTotal))
                    {
                        Order.Balance = 0;
                        blnIsUseCreditNote = true;
                        strCreditCode = txtCodeCreditNote.Text;
                    }

                    UpdateDeposit();
                    try
                    {
                        mCustomerDisplayVFD.ClearScreen();
                        mCustomerDisplayVFD.DisplayWelcome();
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                    }
                    if (Order.Tendered > 0 || Order.ChangeAmount > 0)
                    {
                        Class.LogPOS.WriteLog("Open cash drawer");
                        Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                    }
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                }
            }
            else if (txtCustomerCode.Text != "" && Order.Delivery == 0)
            {
                if (lblChoseDebtObject.Text == "Customer" || lblChoseDebtObject.Text == "KHÁCH HÀNG")
                {
                    if (CheckCustomerAccount())
                    {
                        try
                        {
                            double discount = 0;
                            try
                            {
                                discount = Convert.ToDouble(txtDiscount.Text);
                            }
                            catch (Exception) { }
                            discount = Math.Round(discount * Convert.ToDouble(lblBalance.Text) / 100, 2);
                            Order.Discount = mMoney.getFortMat(discount);
                            Order.Card = mMoney.getFortMat(textBoxCard.Text);
                            Order.Cash = mMoney.getFortMat(textBoxTendered.Text);
                            Order.Tendered = mMoney.getFortMat(textBoxTendered.Text);
                            Order.SubTotal = mMoney.getFortMat(lblBalance.Text);
                            Order.StaffID = Convert.ToInt32(frmLogin.intEmployeeID);

                            if (txtPlate.Tag is int)
                                Order.RegoID = Convert.ToInt32(txtPlate.Tag);
                            else
                                Order.RegoID = 0;

                            int i = 0;
                            Order.MoreOrderByCard = new List<OrderByCard>();
                            foreach (UCCardButton uccCard in this.flCard.Controls)
                            {
                                i++;
                            }

                            double dblCountSurchart = 0;

                            #region i!=0;

                            if (i != 0)
                            {
                                // code duc

                                #region change orderbycardID

                                string sql = "select * from orderbycard order by ID DESC LIMIT 1";
                                con = new Connection.Connection();
                                con.Open();
                                DataTable dt = con.Select(sql);
                                long s = long.Parse(dt.Rows[dt.Rows.Count - 1]["ID"].ToString());
                                s++;
                                con.Close();
                                Order.OrderByCardID = s.ToString();

                                #endregion change orderbycardID

                                //Order.OrderByCardID = DateTime.Now.Year + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                                foreach (UCCardButton uccCard in this.flCard.Controls)
                                {
                                    CardSubtotal cardSubtotal = (CardSubtotal)uccCard.btnCard.Tag;
                                    if (cardSubtotal.Card.IsActive)
                                    {
                                        if (Convert.ToBoolean(uccCard.Tag))
                                        {
                                            Order.MoreOrderByCard.Add(new OrderByCard()
                                            {
                                                orderbycardID = DateTime.Now.Year + "" + DateTime.Now.Month + "" + DateTime.Now.Day + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second,
                                                // Duc comment 4-4-2013
                                                //orderbycardID = Order.OrderByCardID,
                                                cardID = cardSubtotal.Card.CardID,
                                                subtotal = Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text) * cardSubtotal.getSurChar(checkBoxEnableShurchart.Checked) + Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text),
                                                cashOut = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text),
                                                orderID = Order.OrderID,
                                                nameCard = uccCard.lblNameCard.Text,
                                                shiftID = Order.ShiftID
                                            }
                                            );
                                            dblCountSurchart += Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text) * cardSubtotal.getSurChar(checkBoxEnableShurchart.Checked);
                                        }
                                        else
                                        {
                                            Order.MoreOrderByCard.Add(new OrderByCard()
                                            {
                                                orderbycardID = Order.OrderByCardID,
                                                cardID = cardSubtotal.Card.CardID,
                                                subtotal = Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text),
                                                cashOut = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text),
                                                orderID = Order.OrderID,
                                                nameCard = uccCard.lblNameCard.Text,
                                                shiftID = Order.ShiftID
                                            });
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Order.OrderByCardID = "0";
                            }

                            #endregion i!=0;

                            if (dblCountSurchart != 0)
                            {
                                //Order.Card += money.getFortMat(dblCountSurchart);
                                Order.Surchart = mMoney.getFortMat(dblCountSurchart);
                            }
                            //Order.CreditNoteTotal = money.getFortMat(txtEftPOS.Text);
                            Order.CreditNoteCode = txtCodeCreditNote.Text;
                            Order.ChangeAmount = mMoney.getFortMat(lblChange.Text);
                            //Balance
                            if ((Order.SubTotal - Order.Discount + Order.Tendered + Order.Card + Order.CreditNoteTotal) >= 0 && (Order.CreditNoteTotal < Order.SubTotal))
                            {
                                Order.Balance = 0;
                                blnIsUseCreditNote = true;
                                strCreditCode = txtCodeCreditNote.Text;
                            }  

                            UpdateDeposit();
                            try
                            {
                                mCustomerDisplayVFD.ClearScreen();
                                mCustomerDisplayVFD.PrintLine1("WELCOME TO");
                                mCustomerDisplayVFD.PrintLine2(new Connection.ReadDBConfig().Header1);
                            }
                            catch (Exception ex)
                            {
                                Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                            }
                            Order.Account = mMoney.getFortMat(lblSubTotal.Text);
                            if (Order.Tendered > 0 || Order.ChangeAmount > 0)
                            {
                                Class.LogPOS.WriteLog("Open cash drawer");
                                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                            }
                            this.DialogResult = DialogResult.OK;
                        }
                        catch (Exception ex)
                        {
                            Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + ex.Message);
                        }
                        axCsdEft.Tag = null;
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }
            License.BOLicense.CheckLicense();
          
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Class.LogPOS.WriteLog(_nameClass + "frmSubTotal." + btn.Text + ".");
            double total = 0.00;
            try
            {
                if (blnIsFocus == true)
                    total += Convert.ToDouble(txtCashOut.Text);
                else
                    total += Convert.ToDouble(textBoxTendered.Text);
            }
            catch (Exception)
            {
            }
            total += Convert.ToDouble(btn.Tag.ToString());

            foreach (UCCardButton uccCard in this.flCard.Controls)
            {
                CardSubtotal card = (CardSubtotal)uccCard.btnCard.Tag;
                if (card.Card.IsActive)
                {
                    return;
                }
            }

            if (blnIsFocus == true)
            {
                txtCashOut.Text = String.Format("{0:0.00}", total);
            }
            else
            {
                textBoxTendered.Text = String.Format("{0:0.00}", total);
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                listView1.Items.Clear();
                DataTable dt = new DataTable();
                dt = con.Select("select * from creditnote cn where (cn.creditNoteCode LIKE " + "'%'" + " '" + txtCodeCreditNote.Text.Trim() + "' " + "'%')");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    CreditNote depos = new CreditNote();
                    depos.creditNoteCode = row["creditNoteCode"].ToString();
                    depos.creditNoteTotal = mMoney.Format(row["balance"].ToString()).ToString();
                    depos.dateCreditNote = Convert.ToDateTime(row["ts"]);
                    depos.creditNoteID = Convert.ToInt32(row["creditNoteID"]);

                    ListViewItem lvi = new ListViewItem(depos.dateCreditNote.ToShortDateString());
                    lvi.SubItems.Add(depos.creditNoteCode);
                    lvi.SubItems.Add(mMoney.Format(Convert.ToDouble(depos.creditNoteTotal)));
                    listView1.Items.Add(lvi);
                    lvi.Tag = depos;

                    strCreditCode = depos.creditNoteCode;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnFind_Click::" + ex.Message);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnClear_Click");
            textBoxTendered.Text = "";
        }

        private void btnOpenTill_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnOpenTill_Click");
            Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
        }

        private void buttonCard_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "buttonCard_Click");
            mLockTextChange = true;
            txtDiscount.Enabled = false;
            double card = CalcultorDiscountForCard();

            int intCard = (int)(card * 1000);
            if ((intCard % 10) >= 5)
            {
                intCard = ((intCard / 10) + 1) * 10;
            }
            card = (double)intCard / 1000;

            if (card < 0)
            {
                card = 0;
            }
            if (Class.Round.RoundSubTotal(card) > 0)
            {
                textBoxCard.Text = mMoney.FormatCurenMoney2(card);
            }
            CheckSubTotal();
            mLockTextChange = false;
        }

        private void buttonCash_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "buttonCash_Click");
            mLockTextChange = true;
            txtDiscount.Enabled = false;
            double discountPercent = 0;
            double deposit = 0;

            double card = 0;
            try
            {
                card = Convert.ToDouble(textBoxCard.Text);
            }
            catch (Exception) { }
            double discount = 0;
            try
            {
                discount = Convert.ToDouble(txtDiscount.Text);
                if (discount > 100)
                {
                    discount = 0;
                    txtDiscount.Text = discount + "";
                }
                discountPercent = discount;
            }
            catch (Exception) { }
            double balance = 0;
            try
            {
                balance = Convert.ToDouble(lblBalance.Text);
            }
            catch (Exception) { }
            discount = discount * balance / 100;
            discount = Math.Round(discount, 3);
            double cash = ((100 - discountPercent) * balance / 100) - deposit - card;
            //cash = Math.Round(cash,2);
            int intCash = (int)(cash * 1000);
            if ((intCash % 10) >= 5)
            {
                intCash = ((intCash / 10) + 1) * 10;
            }
            cash = (double)intCash / 1000;
            if (cash < 0)
            {
                cash = 0;
            }
            textBoxTendered.Text = mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal(cash));
            CheckSubTotal();
            mLockTextChange = false;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            txtCodeCreditNote.Text = "";
        }

        private void buttonDeposit_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "buttonDeposit_Click");
            this.Height = 678;
            listView1.Items.Clear();
            DataTable dt = new DataTable();
            dt = con.Select("SELECT * FROM creditnote c where balance <> 0");
            foreach (System.Data.DataRow row in dt.Rows)
            {
                CreditNote depos = new CreditNote();
                depos.creditNoteCode = row["creditNoteCode"].ToString();
                depos.creditNoteTotal = row["balance"].ToString().ToString();
                depos.dateCreditNote = Convert.ToDateTime(row["ts"]);
                depos.creditNoteID = Convert.ToInt32(row["creditNoteID"]);

                ListViewItem lvi = new ListViewItem(depos.dateCreditNote.ToShortDateString());
                lvi.SubItems.Add(depos.creditNoteCode);
                lvi.SubItems.Add(mMoney.Format(Convert.ToDouble(depos.creditNoteTotal)));
                listView1.Items.Add(lvi);
                lvi.Tag = depos;
            }
        }

        private void buttonDriverOff_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "buttonDriverOff_Click");
            this.DialogResult = DialogResult.Abort;
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
        }

        private double CalcultorDiscountForCard()
        {
            double balance = 0;
            try
            {
                balance = Convert.ToDouble(lblSubTotal.Text);
            }
            catch (Exception) { }
            double cashOut = 0;
            try
            {
                cashOut = Convert.ToDouble(txtCashOut.Text);
            }
            catch (Exception) { }
            double card = balance + cashOut;
            return card;
        }

        private double CardAmound()
        {
            double discountPercent = 0;
            double deposit = 0;
            double cash = 0;
            try
            {
                cash = Convert.ToDouble(textBoxTendered.Text);
            }
            catch (Exception) { }
            double discount = 0;
            try
            {
                discount = Convert.ToDouble(txtDiscount.Text);
                if (discount > 100)
                {
                    discount = 0;
                    txtDiscount.Text = discount + "";
                }
                discountPercent = discount;
            }
            catch (Exception) { }
            double balance = 0;
            try
            {
                balance = Convert.ToDouble(lblBalance.Text);
            }
            catch (Exception) { }
            discount = discount * balance / 100;
            double card = ((100 - discountPercent) * balance / 100) - deposit - cash;
            //cash = Math.Round(cash,2);
            int intCard = (int)(card * 1000);
            if ((intCard % 10) >= 5)
            {
                intCard = ((intCard / 10) + 1) * 10;
            }
            card = (double)intCard / 1000;
            if (card < 0)
            {
                card = 0;
            }
            return card;
        }

        private void CashOutTextChange()
        {
            lbStatus.Text = "";
            mLockTextChange = true;

            //textBoxTendered.Enabled = false;
            txtDiscount.Enabled = false;
            textBoxCard.Enabled = false;
            double total = 0;
            if (lblBalance.Text != "")
            {
                //total = Convert.ToDouble(lblBalance.Text);
                total = Convert.ToDouble(lblSubTotal.Text);
            }
            double discount = 0;
            if (txtDiscount.Text != "")
            {
                discount = Convert.ToDouble(txtDiscount.Text);
            }
            discount = discount * total / 100;
            double cashout = 0;
            if (txtCashOut.Text != "")
            {
                cashout = Convert.ToDouble(txtCashOut.Text);
            }

            textBoxCard.Text = "";
            lblSubTotal.Text = "0";

            mLockTextChange = false;
            //UCCardButton.dblAmount = Convert.ToDouble(txtEftPOS.Text);
            UCCardButton.dblAmount = Convert.ToDouble(mMoney.FormatCurenMoney2(total - discount + cashout));
        }

        private void CashOutTextChange2()
        {
            lbStatus.Text = "";
            mLockTextChange = true;

            //textBoxTendered.Enabled = false;
            txtDiscount.Enabled = false;
            textBoxCard.Enabled = false;
            double total = 0;
            if (lblBalance.Text != "")
            {
                total = Convert.ToDouble(lblBalance.Text);
            }
            double discount = 0;
            if (txtDiscount.Text != "")
            {
                discount = Convert.ToDouble(txtDiscount.Text);
            }
            discount = discount * total / 100;
            double cashout = 0;
            if (txtCashOut.Text != "")
            {
                cashout = Convert.ToDouble(txtCashOut.Text);
            }

            if (cashout == 0)
            {
                txtDiscount.Enabled = true;
                buttonCash2.Enabled = true;
                textBoxTendered.Enabled = true;
                if (Order.Customer.CustID == 0)
                    btnCustomer.Enabled = true;
                btnDoTransaction.Enabled = true;
            }
            else
            {
                txtDiscount.Enabled = false;
                buttonCash2.Enabled = false;
                textBoxTendered.Enabled = false;
                if (Order.Customer.CustID == 0)
                    btnCustomer.Enabled = false;
                //btnDoTransaction.Enabled = false;
            }

            textBoxTendered.Text = "";
            textBoxCard.Text = "";

            mLockTextChange = false;
            UCCardButton.dblAmount = Convert.ToDouble(mMoney.FormatCurenMoney2(total - discount + cashout));

            foreach (UCCardButton uccCard in this.flCard.Controls)
            {
                CardSubtotal card = (CardSubtotal)uccCard.btnCard.Tag;
                if (card.Card.IsActive)
                {
                    UCCardButton.dblAmount = Convert.ToDouble(lblBalance.Text == "" ? "0" : lblBalance.Text) + Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text) - discount;
                    uccCard.txtTotalAmount.Text = Convert.ToString(UCCardButton.dblAmount);
                    textBoxCard.Text = uccCard.txtTotalAmount.Text;
                }
                else
                {
                    uccCard.btnCard.Image = (Bitmap)Resources.UnCheck;
                    card.Card.IsActive = false;
                    uccCard.txtTotalAmount.Text = "";
                }
            }
        }

        private void EnableCashOut(bool value)
        {
            if (value)
            {
                txtCashOut.Enabled = true;
                txtCashOut.Text = "";
                textBoxCard.Text = "";
            }
            else
            {
                txtCashOut.Enabled = false;
                txtCashOut.Text = "";
                textBoxCard.Text = "";
            }
        }

        private void checkBoxEnableShurchart_CheckedChanged(object sender, EventArgs e)
        {
            if (mButtonCurent != null)
            {
                CardSubtotal card1 = (CardSubtotal)mButtonCurent.Tag;
                ExecuteCardClick(mButtonCurent, false);
            }
        }

        private bool CheckCustomerAccount()
        {
            bool blnFlag = false;
            if (mCustomers != null)
            {
                if (mCustomers != null)
                {
                    double decAccountLimit = 0, decDebt = 0, decDebtAccMember = 0;
                    decAccountLimit = mCustomers.Accountlimit;
                    decDebt = mCustomers.Balance;
                    decDebtAccMember = mCustomers.Balance;
                    double decSubTotal = mMoney.getFortMat(lblSubTotal.Text);

                    if (mCustomers.Company != 0)
                    {
                        DataObject.Company mCompany = BusinessObject.BOCompany.GetByID(mCustomers.Company);
                        decAccountLimit = mCompany.AccountLimit;
                        decDebt = mCompany.Debt;
                    }
                    if (decAccountLimit >= (decDebt + decSubTotal))
                    {
                        blnFlag = true;
                    }
                    else
                    {
                        lbStatus.Text = "Overcoming debt limited";
                        blnFlag = false;
                    }
                }
                else
                {
                    lbStatus.Text = "Not exist customer in database";
                }

            }
            else
            {
                blnFlag = true;
            }
            return blnFlag;
        }

        private bool CheckSubTotal()
        {
            lbStatus.Text = "";
            double deposit = 0;
            double discountPercent = 0;
            double tendered = 0;
            if (textBoxTendered.Text != "")
            {
                tendered = Convert.ToDouble(textBoxTendered.Text);
                frmCustomerDisplay.ChangeTenter = tendered;
            }
            else
            {
                frmCustomerDisplay.ChangeTenter = 0;
            }
            double surcharge = 0;
            if (txtSurcharge.Text != "")
            {
                surcharge = Convert.ToDouble(txtSurcharge.Text);
            }
            double card = 0;
            if (textBoxCard.Text != "")
            {
                card = Convert.ToDouble(textBoxCard.Text) - surcharge;
                frmCustomerDisplay.ChangeCard = card;
            }
            else
            {
                frmCustomerDisplay.ChangeCard = 0;
            }
            double discount = 0;
            if (txtDiscount.Text != "")
            {
                discount = Convert.ToDouble(txtDiscount.Text);
                if (discount > 100)
                {
                    discount = 0;
                    txtDiscount.Text = discount + "";
                }
                discountPercent = discount;
            }
            double balance = 0;
            if (lblBalance.Text != "")
            {
                balance = Convert.ToDouble(lblBalance.Text);
            }
            discount = discount * balance / 100;
            frmCustomerDisplay.ChangeDiscount = discount;
            double subtotal = balance - discount;
            double sub = 0;
            if (Math.Round(deposit, 2) >= Math.Round(subtotal, 2))
            {
                sub = 0;
                lblChange.Text = mMoney.Format2(mMoney.getFortMat(deposit + tendered - subtotal));
                lblSubTotal.Text = "0.00";
                return true;
            }
            else
            {
                sub = subtotal - deposit;
            }
            sub = sub - card - tendered;

            if (Math.Round(sub, 2) < 0)
            {
                sub = 0;
            }
            lblSubTotal.Text = mMoney.Convert3to2(Class.Round.RoundSubTotal(sub));
            double check = Math.Round(((100 - discountPercent) * balance / 100) - (deposit + card + tendered), 2);
            double checkRound = Class.Round.RoundSubTotal(check);
            if (check > 0 && checkRound != 0)
            {
                lbStatus.Text = "Tendered not enough!!!";
                lblChange.Text = "0";
                frmCustomerDisplay.ChangeChange = 0;
                return false;
            }
            else
            {
                //textBoxCard.Text = money.Format(money.getFortMat(0));
                lblChange.Text = mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal((discount + deposit + card + tendered) - balance));
                frmCustomerDisplay.ChangeChange = Convert.ToDouble(lblChange.Text);
            }
            try
            {
                mCustomerDisplayVFD.ClearScreen();
                mCustomerDisplayVFD.PrintLine1("Total:" + mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal(Convert.ToDouble(lblBalance.Text))));
                if (lblChange.Text != "")
                {
                    mCustomerDisplayVFD.PrintLine2("TE:" + mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal(Convert.ToDouble(textBoxTendered.Text))) + " CH:" + mMoney.FormatCurenMoney(Class.Round.RoundSubTotal(Convert.ToDouble(lblChange.Text))));
                }
                else
                {
                    mCustomerDisplayVFD.PrintLine2("TE:" + mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal(Convert.ToDouble(textBoxTendered.Text))));
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "CheckSubTotal::" + ex.Message);
            }
            try
            {
                double total = mshift.CashFloatIn + mshift.CashIn + mshift.cash + mshift.cashAccount - mshift.CashOut - mshift.PayOut - mshift.SafeDrop;
                Class.LogPOS.WriteLog(_nameClass + "CashOutMoney::" + total + "====" + txtCashOut.Text);
                if (Convert.ToDouble(mMoney.Format2(total)) < Convert.ToDouble(txtCashOut.Text))
                {
                    lbStatus.Text = "Invalid Cash Out!!!";
                    return false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "CashOut::" + ex.Message);
            }

            return true;
        }

        private bool CheckSubToTalCompleted()
        {
            double mBalance = 0;
            double mdiscount = 0;
            double mdeposit = 0;
            double mcard = 0;
            double mcash = 0;
            double mchange = 0;
            try
            {
                mTotal = Convert.ToDouble(lblBalance.Text);
                Order.SubTotal = mTotal;
            }
            catch (Exception) { }
            try
            {
                if (Convert.ToDouble(txtDiscount.Text) > 100)
                {
                    label10.Text = "Discount must be small than 100%";
                    label10.Visible = true;
                }
                else
                {
                    label10.Visible = false;
                }
                if (txtDiscount.Text == "" || Convert.ToDouble(txtDiscount.Text) > 100)
                {
                    mdiscount = 0;
                }
                else
                {
                    mdiscount = Convert.ToDouble(txtDiscount.Text) * mTotal / 100;
                }
                Order.Discount = mdiscount;
            }
            catch (Exception) { }
            try
            {
                if (textBoxCard.Text == "")
                {
                    mcard = 0;
                }
                else
                {
                    mcard = Convert.ToDouble(textBoxCard.Text);
                }
                Order.Card = mcard;
            }
            catch (Exception) { }
            try
            {
                if (textBoxTendered.Text == "")
                {
                    mcash = 0;
                }
                else
                {
                    mcash = Convert.ToDouble(textBoxTendered.Text);
                }
                Order.Cash = mcash;
            }
            catch (Exception) { }
            try
            {
                if (mTotal - (mdeposit + mdiscount) >= 0)
                {
                    mBalance = mTotal - (mdiscount + mdeposit);
                }
                else
                {
                    mBalance = 0;
                }
                Order.Tendered = mBalance;
            }
            catch (Exception) { }
            try
            {
                if ((mcash + mcard + mdeposit + mdiscount) - mTotal > 0)
                {
                    mchange = (mcard + mcash + mdiscount + mdeposit) - mTotal;
                }
                else
                {
                    mchange = 0;
                }
                mChange = mchange;
            }
            catch (Exception) { }
            if ((mcard + mcash + mdeposit + mdiscount) - mTotal > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool checkTotal()
        {
            lbStatus.Text = "";
            try
            {
                double tendered = mMoney.getFortMat(textBoxTendered.Text);
                double deposit = 0;
                if (tendered < (Order.getSubTotal() - Order.Discount - deposit))
                {
                    lbStatus.Text = "Not enough money";
                    lblChange.Text = "";
                }
                else
                {
                    double change = (tendered - Order.getSubTotal() + Order.Discount + deposit);

                    lbStatus.Text = "";
                    lblChange.Text = mMoney.Format(change);
                    if (frmCustomerDisplay != null)
                    {
                        frmCustomerDisplay.ChangeTenter = tendered;
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                lblChange.Text = "";
                lbStatus.Text = "Number Invalid";
            }
            return false;
        }

        private void ExecuteCardClick(Button btn, bool blnEnableTextBox)
        {
            txtCashOut.Enabled = false;
            txtSurcharge.Text = "";
            double dblDiscount = 0;
            double dblSurcharge = 0;
            Order.Surchart = 0;
            lblBalance.Text = mMoney.Convert3to2(Convert.ToDouble(mMoney.Format(Order.getSubTotal() + mMoney.getFortMat(Order.DeliveryFee))));
            CardSubtotal cardSubtotal = (CardSubtotal)btn.Tag;
            dblSurcharge = Convert.ToDouble(txtSurcharge.Text == "" ? "0" : txtSurcharge.Text);
            foreach (UCCardButton uccCard in this.flCard.Controls)
            {
                CardSubtotal card1 = (CardSubtotal)uccCard.btnCard.Tag;
                if (card1.Card.CardID != cardSubtotal.Card.CardID)
                {
                    uccCard.btnCard.Image = (Bitmap)Resources.UnCheck;
                    card1.Card.IsActive = false;
                    uccCard.txtTotalAmount.Enabled = blnEnableTextBox;
                    uccCard.txtTotalAmount.Text = "0.00";
                }
            }
            EnableCashOut(false);
            if (cardSubtotal.Card.IsActive)
            {
                CashOutTextChange();
                cardSubtotal.CardButton.btnCard.Image = (Bitmap)Resources.UnCheck;
                cardSubtotal.Card.IsActive = !cardSubtotal.Card.IsActive;
                cardSubtotal.CardButton.txtTotalAmount.Enabled = blnEnableTextBox;
                cardSubtotal.CardButton.txtTotalAmount.Text = "0.00";

                if (Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text) != 0)
                {
                    if (Order.Customer.CustID == 0)
                        btnCustomer.Enabled = false;
                }
                else
                {
                    if (Order.Customer.CustID == 0)
                        btnCustomer.Enabled = true;
                }
                textBoxCard.Text = "0";
                if (cardSubtotal.Card.IsSurchart)
                {
                    //Card khong Check
                    lblBalance.Text = mMoney.Format2(Order.getSubTotal());
                    dblSurcharge = 0;
                    txtSurcharge.Text = "";
                    txtSurcharge.Text = mMoney.Format2(Convert.ToDouble(mMoney.getFortMat(textBoxCard.Text)) * cardSubtotal.getSurChar(checkBoxEnableShurchart.Checked));
                    CheckSubTotal();
                }
            }
            else
            {
                EnableCashOut(cardSubtotal.Card.IsCashOut);
                textBoxCard.Text = "";
                txtSurcharge.Text = "";
                btn.Image = (Bitmap)Resources.checkbox;
                cardSubtotal.Card.IsActive = !cardSubtotal.Card.IsActive;
                if (cardSubtotal.Card.IsSurchart && checkBoxEnableShurchart.Checked)
                {
                    double cashOut = 0;
                    if (txtCashOut.Text != "")
                    {
                        cashOut = Convert.ToDouble(txtCashOut.Text);
                    }
                    double card = CalcultorDiscountForCard();
                    txtSurcharge.Text = mMoney.FormatCurenMoney((card) * cardSubtotal.getSurChar(checkBoxEnableShurchart.Checked));
                    double surcharge = 0;
                    if (txtSurcharge.Text != "")
                    {
                        surcharge = Convert.ToDouble(txtSurcharge.Text);
                    }
                    lblSubTotal.Text = "0";
                    cardSubtotal.CardButton.txtTotalAmount.Text = mMoney.FormatCurenMoney2(card + surcharge);
                    textBoxCard.Text = cardSubtotal.CardButton.txtTotalAmount.Text;
                    CheckSubTotal();
                }
                else
                {
                    double cashOut = 0;
                    if (txtCashOut.Text != "")
                    {
                        cashOut = Convert.ToDouble(txtCashOut.Text);
                    }
                    double card = CalcultorDiscountForCard();
                    cardSubtotal.CardButton.txtTotalAmount.Text = mMoney.FormatCurenMoney2(card);
                    textBoxCard.Text = cardSubtotal.CardButton.txtTotalAmount.Text;
                    CheckSubTotal();
                }

                dblDiscount = Convert.ToDouble(txtDiscount.Text == "" ? "0" : txtDiscount.Text);
                dblDiscount = Math.Round(dblDiscount * Convert.ToDouble(lblBalance.Text) / 100, 3);
                if (Convert.ToDouble(textBoxCard.Text == "" ? "0" : textBoxCard.Text) + dblDiscount + Convert.ToDouble(textBoxTendered.Text == "" ? "0" : textBoxTendered.Text) >= Convert.ToDouble(lblBalance.Text))
                {
                    if (Order.Customer.CustID == 0)
                    {
                        btnCustomer.Enabled = false;
                        txtCustomerCode.Text = "";
                    }
                }
                else
                {
                    if (Order.Customer.CustID == 0)
                        btnCustomer.Enabled = true;
                }
            }
        }

        private void frmSubTotal_FormClosed(object sender, FormClosedEventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void frmSubTotal_Load(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "frmSubTotal_Load");
            try
            {
                LoadDisplayCustomer();
                //lblBalance.Text = mMoney.Convert3to2(Convert.ToDouble(mMoney.Format(Order.getSubTotal() + mMoney.getFortMat(Order.DeliveryFee))));
                double thanhtoan = 0;
                foreach (ProcessOrderNew.Item item in Order.ListItem)
                {
                    thanhtoan += item.SubTotal;
                }
                lblBalance.Text = mMoney.Convert3to2(Convert.ToDouble(mMoney.Format(thanhtoan + mMoney.getFortMat(Order.DeliveryFee))));
                try
                {
                    lblSubTotal.Text = mMoney.Convert3to2(Class.Round.RoundSubTotal(Convert.ToDouble(lblBalance.Text)));
                    mCustomerDisplayVFD.ClearScreen();
                    mCustomerDisplayVFD.PrintLine1("Total:" + mMoney.Convert3to2(Class.Round.RoundSubTotal(Convert.ToDouble(lblBalance.Text))));
                }
                catch (Exception)
                {
                }
                this.Location = new Point(this.Location.X, 0);
                UCCardButton.dblAmount = Convert.ToDouble(lblSubTotal.Text) + Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                TextBoxPOS_Enter(textBoxTendered, null);

                if (Order.Delivery == 1)
                {
                    txtCustomerCode.Text = Order.CustCode;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "frmSubTotal_Load::" + ex.Message);
            }
        }

        private void InitCardAx()
        {
            foreach (CardInterface.AXCSEventObject obj in axCsdEft.POSListEventObject)
            {
                this.axCsdEft.TransactionEvent -= obj.TransactionEvent;
                this.axCsdEft.PrintReceiptEvent -= obj.PrintReceiptEvent;
            }
            axCsdEft.POSListEventObject.Clear();
            CardInterface.AXCSEventObject objNew = new CardInterface.AXCSEventObject(new AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEventHandler(axCsdEft_PrintReceiptEvent), new EventHandler(axCsdEft_TransactionEvent));
            this.axCsdEft.PrintReceiptEvent += objNew.PrintReceiptEvent;
            this.axCsdEft.TransactionEvent += objNew.TransactionEvent;
            axCsdEft.POSListEventObject.Add(objNew);
        }

        private void labelError_Click(object sender, EventArgs e)
        {
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                CreditNote mdeposit = (CreditNote)listView1.SelectedItems[0].Tag;
                txtCodeCreditNote.Text = mdeposit.creditNoteCode;
            }
        }

        private void LoadCard()
        {
            try
            {
                flCard.Controls.Clear();
                List<DataObject.Card> lsArray = BusinessObject.BOCard.GetAll(1);

                UCCardButton[] lsUCCardButton = new UCCardButton[lsArray.Count];
                int n = 0;
                foreach (DataObject.Card item in lsArray)
                {
                    item.IsActive = false;
                    lsUCCardButton[n] = new UCCardButton();
                    lsUCCardButton[n].POSKeyPad = uCkeypad1;
                    lsUCCardButton[n].lblNameCard.Text = item.CardName;
                    lsUCCardButton[n].btnCard.Tag = new Class.CardSubtotal(item.Copy(), lsUCCardButton[n]);
                    lsUCCardButton[n].btnCard.Click += new EventHandler(btnCard_Click);
                    lsUCCardButton[n].txtTotalAmount.Enabled = false;
                    lsUCCardButton[n].Tag = item.IsSurchart.ToString();
                    n++;
                }
                flCard.Controls.AddRange(lsUCCardButton);

            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadCard::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPort.SetText("", txtCustomerCode);
                mSerialPort.SetText(data, txtCustomerCode);
            }
        }

        private void textBoxCard_KeyPress(object sender, KeyPressEventArgs e)
        {
            double discount = 0;
            if (txtDiscount.Text != "")
            {
                discount = Convert.ToDouble(txtDiscount.Text);
            }
            double balance = 0;
            if (lblBalance.Text != "")
            {
                balance = Math.Round(Convert.ToDouble(lblBalance.Text), 3);
            }
            double card = 0;
            if (e.KeyChar != '.' && !Char.IsControl(e.KeyChar))
            {
                card = Convert.ToDouble(textBoxCard.Text + e.KeyChar);
                discount = discount * balance / 100;
                discount = Math.Round(discount, 3);
                double subtotal = balance - discount;
                if (card > subtotal)
                {
                    lbStatus.Text = "Invalid Card Amount";
                    e.Handled = true;
                }
            }
        }

        private void TextBoxPOS_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void TextBoxPOS_Leave(object sender, EventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;
                txt.BackColor = (Color)txt.Tag;
            }
            catch { }
        }

        private void textBoxPOS1_TextChanged(object sender, EventArgs e)
        {
            TextBox btn = (TextBox)sender;
            Class.LogPOS.WriteLog(_nameClass + "textBoxPOS1_TextChanged.(" + btn.Name.ToString() + ").");
            double dblDiscount = 0;
            dblDiscount = Convert.ToDouble(txtDiscount.Text == "" ? "0" : txtDiscount.Text);
            dblDiscount = Math.Round(dblDiscount * Convert.ToDouble(lblBalance.Text) / 100, 3);

            if (Convert.ToDouble(textBoxCard.Text == "" ? "0" : textBoxCard.Text) != 0)
            {
                txtDiscount.Enabled = false;
            }
            else
            {
                txtDiscount.Enabled = true;
            }

            if (!mLockTextChange)
            {
                CheckSubTotal();
            }

            double card = CardAmound();
            UCCardButton.dblAmount = card + Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
        }

        private void textBoxTendered_MouseClick(object sender, MouseEventArgs e)
        {
            blnIsFocus = false;
        }

        private void textBoxTendered_TextChanged(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "textBoxTendered_TextChanged");

            double dblDiscount = 0;
            dblDiscount = Convert.ToDouble(txtDiscount.Text == "" ? "0" : txtDiscount.Text);
            dblDiscount = Math.Round(dblDiscount * Convert.ToDouble(lblBalance.Text) / 100, 2);
            if (Convert.ToDouble(textBoxTendered.Text == "" ? "0" : textBoxTendered.Text) + dblDiscount + Convert.ToDouble(textBoxCard.Text == "" ? "0" : textBoxCard.Text) >= Convert.ToDouble(lblBalance.Text) && Order.Delivery == 0)
            {
                if (Order.Customer.CustID == 0)
                {
                    btnCustomer.Enabled = false;
                    txtCustomerCode.Text = "";
                }
            }
            else
            {
                if (Order.Customer.CustID == 0)
                    btnCustomer.Enabled = true;
            }
            if (!mLockTextChange)
            {
                //textBoxPOS1.Enabled = false;
                CheckSubTotal();
                double card = CardAmound();
                UCCardButton.dblAmount = card;
            }
        }

        private void txtCashOut_MouseClick(object sender, MouseEventArgs e)
        {
            blnIsFocus = true;
        }

        private void txtCashOut_TextChanged(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "txtCashOut_TextChanged");
            if (Order.Customer.CustID == 0)
                btnCustomer.Enabled = false;
            CashOutTextChange2();
        }

        private bool IsLockCustomerCodeTextChanged = false;
        private void txtCustomerCode_TextChanged(object sender, EventArgs e)
        {
            if (IsLockCustomerCodeTextChanged)
                return;
            try
            {
                con.Open();
                string strQuery = "SELECT if(count(custID)>0,1,0) AS IsExistCustomer FROM customers where memberNo='" + txtCustomerCode.Text + "' And IsActive=1 and isLock =0 And Deleted = 0;";
                bool blnHaveCustomer = Convert.ToBoolean(con.ExecuteScalar(strQuery));

                if (!blnHaveCustomer)
                {
                    strQuery = "SELECT if(COUNT(itemID)>0,1,0) AS IsExistItem FROM itemsmenu WHERE scanCode='" + txtCustomerCode.Text + "'";
                    blnHaveCustomer = Convert.ToBoolean(con.ExecuteScalar(strQuery));
                    if (blnHaveCustomer)
                    {
                        lbStatus.Text = "No more item allow !!!";
                        txtCustomerCode.Text = "";
                    }
                    else
                    {
                        lbStatus.Text = "Not existed customer !!!";
                        txtCustomerCode.Text = "";
                    }
                }
                else
                {
                    if (!mIsLockBarCode)
                    {
                        mCustomers = BusinessObject.BOCustomers.GetByMemberNo(txtCustomerCode.Text);
                        Order.Customer = mCustomers;
                        ShowFromManagerCar(mCustomers.CustID);
                    }
                    lbStatus.Text = "";
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "txtCustomerCode_TextChanged" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }


        private void txtPlate_Click(object sender, EventArgs e)
        {
            ShowFromManagerCar(mCustID);
        }

        private void UpdateDeposit()
        {
            if (depositID > 0)
            {
                try
                {
                    con.Open();
                    con.ExecuteNonQuery("update depositorders set status=1 where depositID=" + depositID);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "UpdateDeposit::" + ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public class ComboItem : object
        {
            protected String m_Name;
            protected int m_Value;

            public ComboItem(String name, int in_value)
            {
                m_Name = name;
                m_Value = in_value;
            }

            public override string ToString()
            {
                return m_Name;
            }
        };

        private class Deposit
        {
            public Deposit(string name, string phone, string mobile, string deposit, string depositCode)
            {
                Name = name;
                Phone = phone;
                Mobile = mobile;
                DepoSit = deposit;
                DepositCode = depositCode;
            }

            public string DepoSit { get; set; }

            public string DepositCode { get; set; }

            public string Mobile { get; set; }

            public string Name { get; set; }

            public string Phone { get; set; }
        }

        private void flCard_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}