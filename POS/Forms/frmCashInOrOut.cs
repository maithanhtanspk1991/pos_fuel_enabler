﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmCashInOrOut : Form
    {
        public static int iIsRefesh;
        public DataObject.ShiftObject mShiftObject;
        private Connection.Connection conn;
        private Connection.ReadDBConfig mReadDBConfig;
        private double dTotalInSafeLastShift;
        private DataTable dtSource;
        private int iShiftID;
        private int iType;
        private int iTypeCash;
        private Class.MoneyFortmat mMoneyFortmat;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private int p;
        Class.ReadConfig mReadConfig = new ReadConfig();

        public frmCashInOrOut()
        {
            InitializeComponent();
        }

        public frmCashInOrOut(int intType, int intTypeCash, double dblTotalInSafeLastShift, MoneyFortmat moneyFortmat, int intShiftID, DataObject.ShiftObject shiftobject)
        {
            InitializeComponent();
            SetMultiLanguge();
            this.iType = intType;
            this.iTypeCash = intTypeCash;
            this.iShiftID = intShiftID;
            this.dTotalInSafeLastShift = dblTotalInSafeLastShift;
            this.mShiftObject = shiftobject;
            if (intType == 1)
            {
                btnCashDrop.Visible = false;
                btnPay.Visible = false;
                intTypeCash = 1;
            }
            else if (intType == 2)
            {
                intTypeCash = 2;
            }
            else if (intType == 6)
            {
                intTypeCash = 6;
            }

            conn = new Connection.Connection();
            this.mMoneyFortmat = moneyFortmat;
            mReadDBConfig = new Connection.ReadDBConfig();
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mSetting = new Class.Setting();
        }

        public frmCashInOrOut(int intType, MoneyFortmat moneyFortmat, int intShiftID, DataObject.ShiftObject shiftobject)
        {
            InitializeComponent();
            SetMultiLanguge();
            this.iType = intType;
            this.iShiftID = intShiftID;
            conn = new Connection.Connection();
            this.mMoneyFortmat = moneyFortmat;
            mReadDBConfig = new Connection.ReadDBConfig();
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mSetting = new Class.Setting();
            if (intType == 1)
            {
                btnCashDrop.Visible = false;
                btnPay.Visible = false;
                iTypeCash = 1;
            }
            else if (intType == 2)
            {
                iTypeCash = 2;
            }
            this.mShiftObject = shiftobject;
        }

        private void SetMultiLanguge()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnCancel.Text = "Thoát";
                label1.Text = "Tổng số tiền";
                label2.Text = "Mô tả";
                return;
            }
        }

        private void btnDes1_Click(object sender, EventArgs e)
        {
            txtDesciption.Text = "";
            txtDesciption.Text = btnCashDrop.Text;
            iTypeCash = 3;
        }

        private void btnDes2_Click(object sender, EventArgs e)
        {
            txtDesciption.Text = "";
            txtDesciption.Text = btnPay.Text;
            iTypeCash = 4;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            iIsRefesh = 0;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                if (txtDesciption.Text == "" && iType != 3 && iType != 6)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Visible = true;
                        lbStatus.Text = "Vui lòng nhập vào mô tả";
                        this.DialogResult = DialogResult.None;
                        return; 
                    }
                    lbStatus.Visible = true;
                    lbStatus.Text = "Please input description";
                    this.DialogResult = DialogResult.None;
                    return; 
                }
                else
                    if (iType == 1) //Cash In
                    {
                        if (mShiftObject != null)
                        {
                            if (mShiftObject.bActiveShift == false && mShiftObject.continueShift == false)
                            {
                                BusinessObject.BOShift.InsertShift(ref mShiftObject);
                                BusinessObject.BOConfig.SetCurrentShift(mShiftObject.ShiftID);
                                mShiftObject.ShiftID = mShiftObject.ShiftID;
                                mShiftObject.bActiveShift = true;
                            }
                        }
                        ExecuteFunction("Cash In", "Cash In");
                    }
                    else if (iType == 2) //Cash Out
                    {
                        double dblTotalAmount = Convert.ToDouble(txtTotalAmount.Text);
                        if (dTotalInSafeLastShift >= dblTotalAmount)
                        {
                            ExecuteFunction("Cash Out", "Cash Out");
                        }

                        else
                        {
                            MessageBox.Show("Money in safe is : " + dTotalInSafeLastShift + ",you can't cash out greater than money in safe !!! ", "Attention !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.DialogResult = DialogResult.Cancel;
                            return;
                        }
                        iIsRefesh = 1;
                    }
                    else if (iType == 3) //SAFE DROP
                    {
                        double dblTotalAmount = Convert.ToDouble(txtTotalAmount.Text);
                        if (dTotalInSafeLastShift >= dblTotalAmount)
                        {
                            ExecuteFunction("Safe Drop", "Safe Drop");
                        }
                        else
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                MessageBox.Show("Số tiền hiện có là : " + dTotalInSafeLastShift + ", bạn không thể lấy ra số tiền lớn hơn số tiền hiện có!!! ", "Cảnh báo !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.DialogResult = DialogResult.Cancel;
                                return;
                            }
                            MessageBox.Show("Money in safe is : " + dTotalInSafeLastShift + ",you can't cash out greater than money in safe !!! ", "Attention !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.DialogResult = DialogResult.Cancel;
                            return;
                        }
                        iIsRefesh = 1;
                    }
                    else if (iType == 6) //Pay Out
                    {
                        double dblTotalAmount = Convert.ToDouble(txtTotalAmount.Text);
                        if (dTotalInSafeLastShift >= dblTotalAmount)
                        {
                            ExecuteFunction("Pay Out", "Pay Out");
                        }
                        else
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                MessageBox.Show("Số tiền hiện có là : " + dTotalInSafeLastShift + ", bạn không thể lấy ra số tiền lớn hơn số tiền hiện có!!! ", "Cảnh báo !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.DialogResult = DialogResult.Cancel;
                                return;
                            }
                            MessageBox.Show("Money in safe is : " + dTotalInSafeLastShift + ",you can't cash out greater than money in safe !!! ", "Attention !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.DialogResult = DialogResult.Cancel;
                            return;
                        }
                        iIsRefesh = 1;
                    }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    Class.LogPOS.WriteLog("LoadLastShift:::" + ex.Message);
                    lbStatus.Text = "Lỗi";
                    return;
                }
                Class.LogPOS.WriteLog("LoadLastShift:::" + ex.Message);
                lbStatus.Text = "Error";
            }
            finally
            {
                conn.Close();
            }
        }

        private float DrawLine(string s, System.Drawing.Font font, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Drawing2D.DashStyle dashStyle, float y, int textAlign)
        {
            float x;
            float width;
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Black);
            if (dashStyle != System.Drawing.Drawing2D.DashStyle.Custom)
            {
                pen.DashStyle = dashStyle;
            }
            if (s == "" || s == null)
            {
                width = e.PageBounds.Width;
            }
            else
            {
                width = e.Graphics.MeasureString(s, font).Width;
            }
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawLine(pen, x, y, x + width, y);
            y += 2;
            return y;
        }

        private float DrawString(string s, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font, float y, int textAlign)
        {
            float x;
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawString(s, font, System.Drawing.Brushes.Black, x, y);
            y += e.Graphics.MeasureString(s, font).Height;

            return y;
        }

        private void ExecuteFunction(String strCashType, String strDescription)
        {
            conn.ExecuteNonQuery("insert into cashinandcashout(EmployeeID,TotalAmount,CashType,Description,ShiftID, CableID) values('" + frmOrdersAll.mStaffID + "'," + mMoneyFortmat.getFortMat(txtTotalAmount.Text) + "," + iTypeCash + ",'" + strDescription + "'," + this.iShiftID + "," + mReadDBConfig.CableID + ")");
            lbStatus.Visible = true;
            lbStatus.Text = strCashType + " successful";            
            try
            {
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                //mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Priner " + strCashType + " ::" + ex.Message);
            }
        }

        private void frmCashInOrOut_Load(object sender, EventArgs e)
        {
            txtTotalAmount.SetForcus();
            //LoadLastShift();
        }

        private void LoadLastShift()
        {
            try
            {
                conn.Open();
                dtSource = new DataTable();
                dtSource = conn.Select("select subTotal,CashOut,CashIn,RealTotalInSafe from shifts order by shiftID DESC LIMIT 0,2");
                if (dtSource.Rows.Count == 2)
                {
                    frmLogin.dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[1]["RealTotalInSafe"].ToString());
                    frmLogin.dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[1]["subTotal"].ToString());
                }
                else
                    if (dtSource.Rows.Count == 1)
                    {
                        frmLogin.dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafe"].ToString());
                        frmLogin.dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[0]["subTotal"].ToString());
                    }
                    else
                    {
                        frmLogin.dblRealTotalInSafe = 0;
                        frmLogin.dblSubtotalLastShift = 0;
                    }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadLastShift:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = mReadDBConfig.Header1.ToString();
            string bankCode = mReadDBConfig.Header2.ToString();
            string address = mReadDBConfig.Header3.ToString();
            string tell = mReadDBConfig.Header4.ToString();

            string website = mReadDBConfig.FootNode1.ToString();
            string thankyou = mReadDBConfig.FootNode2.ToString();

            l_y = DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();

            l_y = DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            DateTime dateTime = DateTime.Now;
            l_y = DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            if (this.mShiftObject.ShiftID != 0)
            {
                l_y = DrawString("SHIFT #" + BusinessObject.BOShift.GetShiftMaxWithShiftID(this.mShiftObject).ShiftName.ToString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            }
            if (iType == 1)
            {
                l_y = mPrinter.DrawString("Cash In ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else if (iType == 2)
            {
                l_y = mPrinter.DrawString("Cash Out ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else if (iType == 3)
            {
                l_y = mPrinter.DrawString("SAFE DROP ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }
            else if (iType == 6)
            {
                l_y = mPrinter.DrawString("Pay Out ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            }

            l_y += mPrinter.GetHeightPrinterLine() / 5;

            mPrinter.DrawString("Total Cash ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + mMoneyFortmat.Format2(Convert.ToDouble(txtTotalAmount.Text) * 1000), e, new Font("Arial", 10), l_y, 3);
            if (iType != 3)
            {
                l_y = DrawString("Description ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawMessenge(txtDesciption.Text, e, new Font("Arial", 10), l_y);
                l_y = DrawString("Signature ", e, new Font("Arial", 10), l_y, 1);
            }
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = DrawLine("***", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 2);
        }
    }
}