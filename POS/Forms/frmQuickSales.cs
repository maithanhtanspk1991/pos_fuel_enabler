﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmQuickSales : Form
    {
        private static Color[] listColor = { Color.Tomato, Color.Orange, Color.HotPink, Color.Green, Color.Blue, Color.Purple, Color.Gray };
        private Connection.Connection conn = new Connection.Connection();
        private Class.MoneyFortmat money;
        private int mIndex = 0;
        private int zIndex = 0;
        private int yIndex = 0;
        private Group mGroup;
        private Item mItem;
        private ItemLine mLine;
        private int mGroupIndex = 0;
        private int mGroupShortCut = 0;
        private bool mLocketText = true;
        private int intIDPumpHistory;
        private int chkoption = 0;
        private int intPumpID;
        private ListView mlist;
        private frmOrdersAll frmorder;

        public frmQuickSales(Class.MoneyFortmat mMoney, ListView list, frmOrdersAll frm, int intPumpID, int intIDPumpHistory)
        {
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            InitializeComponent();
            money = mMoney;
            mlist = list;
            frmorder = frm;
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            initMenuButton();
        }

        public frmQuickSales()
        {
            InitializeComponent();
        }

        private class Group
        {
            public string GroupID { get; set; }

            public string GroupName { get; set; }

            public int Visual { get; set; }
        }

        private class Item
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public double ItemPrice { get; set; }

            public string ScanCode { get; set; }

            public double Price1 { get; set; }

            public double Price2 { get; set; }

            public double GST { get; set; }

            public int EnableFuelDiscount { get; set; }

            public int SellSize { get; set; }

            public int IsFuel { get; set; }
        }

        private class ItemLine
        {
            public string lineID { get; set; }

            public string lineName { get; set; }
        }

        private void initMenuButton()
        {
            for (int i = 0; i < 7; i++)
            {
                int bgcolor = i;
                Button btn = new Button();
                btn.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
                btn.ForeColor = Color.LightGray;
                btn.Width = 150;
                btn.Height = 100;
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tlp_groups.Controls.Add(btn);
                btn.Click += new EventHandler(btn_Click);
            }

            for (int i = 0; i < 21; i++)
            {
                Button btni = new Button();
                btni.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
                btni.Width = 100;
                btni.Height = 100;
                btni.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tlp_items.Controls.Add(btni);
                btni.Click += new EventHandler(btni_Click);
                btni.Enabled = false;
            }
            mIndex = 0;
            mGroupShortCut = 2;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Name);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            button24.Tag = null;
            Button btn = (Button)sender;
            zIndex = 0;
            mGroupIndex = tlp_groups.Controls.GetChildIndex(btn);
            if (LoadItem(btn.Name) == false)
            {
                for (int i = 0; i < 21; i++)
                {
                    tlp_items.Controls[i].Text = "";
                    tlp_items.Controls[i].Tag = null;
                    tlp_items.Controls[i].Enabled = false;
                    tlp_items.Controls[i].BackColor = Color.White;
                }
            }
            mGroup.GroupID = btn.Name;
            mGroup.GroupName = btn.Text;
        }

        private void btni_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Name == "Item")
            {
                try
                {
                    Class.ProcessOrderNew.Item itemsend = (Class.ProcessOrderNew.Item)btn.Tag;
                    mItem.ItemID = itemsend.ItemID.ToString();
                    mItem.ItemName = btn.Text;
                    LoadLineItem(mItem.ItemID);
                    AddItemToListView(mItem.ItemName, mItem.ItemID.ToString(), "itemsmenu", "itemID");
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmQuickSales:::Item Click:::" + ex.Message);
                }
            }
            else
            {
                try
                {
                    mLine.lineID = btn.Tag.ToString();
                    mLine.lineName = btn.Text;
                    AddItemToListView(mLine.lineName, mLine.lineID.ToString(), "itemslinemenu", "lineID");
                }
                catch (Exception exs)
                {
                    Class.LogPOS.WriteLog("frmQuickSales:::Item Line Click:::" + exs.Message);
                }
            }
        }

        private void AddItemToListView(string itemname, string itemid, string fieldtableName, string itemIDName)
        {
            try
            {
                conn.Open();
                string price = conn.ExecuteScalar("select unitPrice from " + fieldtableName + " where " + itemIDName + "=" + itemid).ToString();
                ListViewItem lvi = new ListViewItem("1");
                lvi.SubItems.Add(itemname);
                lvi.SubItems.Add(money.Format(Convert.ToDouble(price)));
                lvi.SubItems.Add(itemid);
                if (String.Compare(fieldtableName, "itemsmenu") == 0 && String.Compare(itemIDName, "itemID") == 0)
                {
                    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, Convert.ToInt32(itemid), itemname, 1, Convert.ToDouble(price), Convert.ToDouble(price), 0, 0, 0, "0");
                    lvi.Tag = item;
                }
                else if (String.Compare(fieldtableName, "itemslinemenu") == 0 && String.Compare(itemIDName, "lineID") == 0)
                {
                    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, 0, itemname, 1, Convert.ToDouble(price), Convert.ToDouble(price), 0, 0, Convert.ToInt32(itemid), "0");
                    lvi.Tag = item;
                }
                listView1.Items.Add(lvi);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmQuickSales:::AddItemToListView:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void LoadGroup()
        {
            try
            {
                button24.Tag = null;
                if (mIndex < 0)
                {
                    mIndex = 0;
                }
                DataTable dt = conn.Select("SELECT groupID,groupShort FROM groupsmenu where `visual`=0 and `isFF`=1 order by displayOrder limit " + mIndex + ",7");
                if (dt.Rows.Count <= 0)
                {
                    mIndex -= 7;
                    return;
                }
                mGroup.GroupID = dt.Rows[0]["groupID"].ToString();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string btn_text = Convert.ToString(dr["groupShort"]);
                    string btn_name = Convert.ToString(dr["groupID"]);
                    tlp_groups.Controls[i].Name = btn_name;
                    tlp_groups.Controls[i].Text = btn_text;
                    tlp_groups.Controls[i].Tag = dr["groupID"].ToString();
                    tlp_groups.Controls[i].Enabled = true;
                    tlp_groups.Controls[i].BackColor = listColor[i];
                }
                for (int i = dt.Rows.Count; i < 7; i++)
                {
                    tlp_groups.Controls[i].Text = "";
                    tlp_groups.Controls[i].Tag = null;
                    tlp_groups.Controls[i].Enabled = false;
                    tlp_groups.Controls[i].BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmQuickSales.LoadGroup:::" + ex.Message);
            }
        }

        private bool LoadItem(string groupID)
        {
            int grShortCut = 0;
            if (zIndex < 0)
            {
                zIndex = 0;
            }
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                grShortCut = Convert.ToInt16(conn.ExecuteScalar("select grShortCut from groupsmenu where groupID=" + groupID));
                int enable = Convert.ToInt16(conn.ExecuteScalar("select count(loyaltyID) from loyaltyscheme where `enable`=1"));
                string sql = "select i.itemID,i.itemShort,i.unitPrice,(select count(il.lineID) from itemslinemenu il where il.itemID=i.itemID) as optionCount from itemsmenu i where `visual`=0 and groupID=" + groupID + " order by displayOrder limit " + zIndex + ",21";
                if (enable > 0)
                {
                    sql = "select i.itemID,i.itemShort,(i.unitPrice*1.1) as unitPrice,(select count(il.lineID) from itemslinemenu il where il.itemID=i.itemID) as optionCount from itemsmenu i where `visual`=0 and groupID=" + groupID + " order by displayOrder limit " + zIndex + ",21";
                }
                dt = conn.Select(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmOrderAll.LoadItem:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }

            if (dt.Rows.Count <= 0)
            {
                zIndex -= 21;
                return false;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(intIDPumpHistory, Convert.ToInt32(dr["itemID"].ToString()), dr["itemShort"].ToString(), 1, Convert.ToDouble(dr["unitPrice"].ToString()), Convert.ToDouble(dr["unitPrice"].ToString()), mItem.GST, 0, 0, intPumpID.ToString());
                if (grShortCut == 1)
                {
                    item.IsOption = true;
                }
                tlp_items.Controls[i].Text = item.ItemName;
                tlp_items.Controls[i].Tag = item;
                tlp_items.Controls[i].Name = "Item";
                tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                tlp_items.Controls[i].Enabled = true;
            }
            for (int i = dt.Rows.Count; i < 21; i++)
            {
                tlp_items.Controls[i].Text = "";
                tlp_items.Controls[i].Tag = null;
                tlp_items.Controls[i].BackColor = Color.White;
                tlp_items.Controls[i].Enabled = false;
            }
            yIndex = 0;
            chkoption = 0;
            return true;
        }

        private void LoadLineItem(string itemID)
        {
            if (yIndex < 0)
            {
                yIndex = 0;
            }
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                int enable = Convert.ToInt16(conn.ExecuteScalar("select count(loyaltyID) from loyaltyscheme where `enable`=1"));
                string sql = "select lineID,itemShort,unitPrice from itemslinemenu where itemID=" + itemID + " limit " + yIndex + ",21";
                if (enable > 0)
                {
                    sql = "select lineID,itemShort,(unitPrice*1.1) as unitPrice from itemslinemenu where itemID=" + itemID + " limit " + yIndex + ",21";
                }
                dt = conn.Select(sql);
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
            if (dt.Rows.Count <= 0)
            {
                yIndex -= 21;
                return;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                string btn_text = Convert.ToString(dr["itemShort"]);
                string btn_name = Convert.ToString(dr["lineID"]);
                Class.ProcessOrderNew.SubItem sub = new Class.ProcessOrderNew.SubItem(
                                                    Convert.ToInt32(dr["lineID"].ToString()),
                                                    dr["itemShort"].ToString(),
                                                    1,
                                                    Convert.ToDouble(dr["unitPrice"].ToString()),
                                                    Convert.ToDouble(dr["unitPrice"].ToString())
                                                );
                tlp_items.Controls[i].Name = "SubItem";
                tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                tlp_items.Controls[i].Text = sub.ItemName;
                tlp_items.Controls[i].Tag = sub;
                tlp_items.Controls[i].Enabled = true;
            }
            for (int i = dt.Rows.Count; i < 21; i++)
            {
                tlp_items.Controls[i].Text = "";
                tlp_items.Controls[i].Tag = null;
                tlp_items.Controls[i].Enabled = false;
                tlp_items.Controls[i].BackColor = Color.White;
            }
            mItem.ItemID = itemID;
            chkoption = 1;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btngroupup_Click(object sender, EventArgs e)
        {
            mIndex -= 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                if (LoadItem(tlp_groups.Controls[mGroupIndex].Name) == false)
                {
                    for (int i = 0; i < 21; i++)
                    {
                        tlp_items.Controls[i].Text = "";
                        tlp_items.Controls[i].Tag = null;
                        tlp_items.Controls[i].BackColor = Color.White;
                        tlp_items.Controls[i].Enabled = false;
                    }
                }
            }
        }

        private void btngroupdown_Click(object sender, EventArgs e)
        {
            mIndex += 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                if (LoadItem(tlp_groups.Controls[mGroupIndex].Name) == false)
                {
                    for (int i = 0; i < 21; i++)
                    {
                        tlp_items.Controls[i].Text = "";
                        tlp_items.Controls[i].Tag = null;
                        tlp_items.Controls[i].BackColor = Color.White;
                        tlp_items.Controls[i].Enabled = false;
                    }
                }
            }
        }

        private void btnitemup_Click(object sender, EventArgs e)
        {
            if (chkoption == 1)
            {
                if (button24.Tag == null || button24.Tag.ToString() == "1" || button24.Tag.ToString() == "2")
                {
                    yIndex -= 21;
                    LoadLineItem(mItem.ItemID);
                }
            }
            else
            {
                zIndex -= 21;
                LoadItem(mGroup.GroupID);
            }
        }

        private void btnitemdown_Click(object sender, EventArgs e)
        {
            if (chkoption == 1)
            {
                if (button24.Tag == null || button24.Tag.ToString() == "1" || button24.Tag.ToString() == "2")
                {
                    yIndex += 21;
                    LoadLineItem(mItem.ItemID);
                }
            }
            else
            {
                zIndex += 21;
                LoadItem(mGroup.GroupID);
            }
        }

        private void Clear()
        {
            txtqty.Text = "";
            txtunitprice.Text = "";
            txtitemname.Text = "";
            txtsubtotal.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                listView1.Items.Clear();
                Clear();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (listView1.SelectedIndices.Count > 0)
                {
                    listView1.FocusedItem.Remove();
                    if (listView1.Items.Count == 0)
                    {
                        Clear();
                    }
                    else
                    {
                        listView1.Focus();
                        int itemindex = (listView1.FocusedItem.Index - 1) > 0 ? (listView1.FocusedItem.Index - 1) : 0;
                        listView1.Items[itemindex].Selected = true;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                Class.ProcessOrderNew.Item item = (Class.ProcessOrderNew.Item)listView1.SelectedItems[0].Tag;
                txtqty.Text = item.Qty + "";
                txtitemname.Text = item.ItemName;
                txtunitprice.Text = money.Format(item.Price);
                txtsubtotal.Text = money.Format(item.SubTotal);
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                if (txtqty.Text == "")
                {
                    txtqty.Text = "1";
                }
                Class.ProcessOrderNew.Item item = (Class.ProcessOrderNew.Item)listView1.SelectedItems[0].Tag;
                item.Qty = Convert.ToInt32(txtqty.Text);
                item.SubTotal = Convert.ToDouble(item.Qty) * item.Price;
                txtsubtotal.Text = money.Format(item.SubTotal);
                listView1.SelectedItems[0].SubItems[0].Text = txtqty.Text;
                listView1.SelectedItems[0].SubItems[2].Text = txtsubtotal.Text;
            }
        }

        private void txtqty_Enter(object sender, EventArgs e)
        {
            mLocketText = false;
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void txtqty_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)46)
            {
                posLabelStatus1.Text = "Invalid Qty";
                e.Handled = true;
            }
        }

        private void frmQuickSales_Load(object sender, EventArgs e)
        {
            txtqty.Focus();
        }

        private void frmQuickSales_VisibleChanged(object sender, EventArgs e)
        {
            txtqty.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                try
                {
                    conn.Open();
                    conn.BeginTransaction();
                    foreach (ListViewItem items in listView1.Items)
                    {
                        Class.ProcessOrderNew.Item item = (Class.ProcessOrderNew.Item)items.Tag;
                        item.EnableFuelDiscount = mItem.EnableFuelDiscount;
                        item.BarCode = mItem.ScanCode;
                        item.ItemType = 0;
                        frmorder.AddNewItems(item);
                        conn.ExecuteNonQuery("insert into itemshistory(PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,IsMenuItems,IsNewItems,IsFindItems,pumpHistoryID) value(" +
                                                    intPumpID + "," + item.ItemID + ",'" + item.ItemName + "'," + item.Qty + ",'" + item.Price + "','" + item.SubTotal + "','" + item.GST + "',0,0,0," + item.EnableFuelDiscount + ",'" + item.BarCode + "',1,0,0," + item.IDPumpHistory + ")");
                    }
                    conn.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmQuickSales:::ButtonSelect_Click:::" + ex.Message);
                    conn.Rollback();
                }
                finally
                {
                    conn.Close();
                }
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
            else
            {
                posLabelStatus1.Text = "Please select item first.";
            }
        }
    }
}