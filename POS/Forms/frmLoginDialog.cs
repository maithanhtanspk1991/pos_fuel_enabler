﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLoginDialog : Form
    {
        public static int MANAGER = 3;
        public static int STAFF = 1;
        public static int SUPERVISOR = 2;

        //public int MyProperty { get; set; }
        public int Permission { get; set; }

        public Staff POSStaff { get; set; }

        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmLoginDialog()
        {
            InitializeComponent();
            SetMultiLanguage();
        }

        public frmLoginDialog(int staffID)
        {
            InitializeComponent();
            txtUser.Text = staffID + "";
            txtUser.Enabled = false;
        }

        private void btnCanel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Đăng nhập";
                btnCanel.Text = "Thoát";
                label1.Text = "Mật khẩu:";
                label1.Font = new Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label4.Text = "Tài khoản:";
                label4.Font = new Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            //if (txtPass.Text == "2305")
            if (BusinessObject.BOConfig.CheckPassWord(new SystemConfig.DBConfig(), txtPass.Text))
            {
                POSStaff = new Staff(0, "Manager", 3);
                Permission = MANAGER;
                this.DialogResult = DialogResult.OK;
                return;
            }

            Connection.Connection con = new Connection.Connection();
            try
            {
                con.Open();
                string sql = "";
                if (Permission == MANAGER)
                {
                    sql = "select * from staffs where staffID=" + txtUser.Text + " and `password`=password('" + txtPass.Text + "') and `permission`=" + Permission;
                }
                else
                {
                    sql = "select * from staffs where staffID=" + txtUser.Text + " and `password`=password('" + txtPass.Text + "')";
                }
                DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    POSStaff = new Staff(Convert.ToInt16(tbl.Rows[0]["staffID"]), tbl.Rows[0]["Name"].ToString(), Convert.ToInt16(tbl.Rows[0]["permission"]));
                    this.DialogResult = DialogResult.OK;
                    return;
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Tài khoản hay mật khẩu không đúng!";
                        return;
                    }
                    lblError.Text = "The login user or password do not match!";
                }
            }
            catch
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Tài khoản hay mật khẩu không đúng!";
                    return;
                }
                lblError.Text = "The login user or password do not match!";
            }
            finally
            {
                con.Close();
            }
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
        }

        public class Staff
        {
            public int StaffID { get; set; }

            public string Name { get; set; }

            public int Permission { get; set; }

            public Staff(int sId, string sName, int sPer)
            {
                StaffID = sId;
                Name = sName;
                Permission = sPer;
            }
        }
    }
}