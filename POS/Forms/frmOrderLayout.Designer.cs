﻿namespace POS.Forms
{
    partial class frmOrderLayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.flow_layout_order_delovery = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flow_layout_order_pickup = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 450);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(753, 77);
            this.panel1.TabIndex = 0;
            // 
            // flow_layout_order_delovery
            // 
            this.flow_layout_order_delovery.AutoScroll = true;
            this.flow_layout_order_delovery.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flow_layout_order_delovery.Location = new System.Drawing.Point(0, 43);
            this.flow_layout_order_delovery.Name = "flow_layout_order_delovery";
            this.flow_layout_order_delovery.Padding = new System.Windows.Forms.Padding(5);
            this.flow_layout_order_delovery.Size = new System.Drawing.Size(372, 402);
            this.flow_layout_order_delovery.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 31);
            this.label1.TabIndex = 38;
            this.label1.Text = "Delivery List";
            // 
            // flow_layout_order_pickup
            // 
            this.flow_layout_order_pickup.AutoScroll = true;
            this.flow_layout_order_pickup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flow_layout_order_pickup.Location = new System.Drawing.Point(378, 43);
            this.flow_layout_order_pickup.Name = "flow_layout_order_pickup";
            this.flow_layout_order_pickup.Padding = new System.Windows.Forms.Padding(5);
            this.flow_layout_order_pickup.Size = new System.Drawing.Size(372, 402);
            this.flow_layout_order_pickup.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(372, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 31);
            this.label2.TabIndex = 40;
            this.label2.Text = "Pickup List";
            // 
            // frmOrderLayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 527);
            this.Controls.Add(this.flow_layout_order_pickup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flow_layout_order_delovery);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOrderLayout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Layout";
            this.Load += new System.EventHandler(this.frmOrderLayout_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.FlowLayoutPanel flow_layout_order_delovery;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.FlowLayoutPanel flow_layout_order_pickup;
        private System.Windows.Forms.Label label2;
    }
}