﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmSplitbill : Form
    {
        private Class.ProcessOrderNew.Order mOrder;
        private Class.MoneyFortmat Money;

        public frmSplitbill(Class.ProcessOrderNew.Order order, Class.MoneyFortmat money)
        {
            InitializeComponent();
            mOrder = order;
            Money = money;
        }

        public frmSplitbill()
        {
            InitializeComponent();
        }

        private void frmSplitbill_Load(object sender, EventArgs e)
        {
            textBox3.Text = textBox1.Text = Money.Format(mOrder.getSubTotal());
            LoadListView(mOrder);
        }

        private void LoadListView(Class.ProcessOrderNew.Order lorder)
        {
            listView1.Items.Clear();
            //txtorderid.Text = order.OrderID + "";
            //txttableid.Text = order.TableID + "";
            //order.shortItem();
            for (int i = 0; i < lorder.ListItem.Count; i++)
            {
                Class.ProcessOrderNew.Item item = lorder.ListItem[i];
                ListViewItem li = new ListViewItem(item.Qty + "");
                li.SubItems.Add(item.ItemName);
                li.SubItems.Add(String.Format("{0:0.00}", Money.Format(item.SubTotal)));
                li.Tag = new ItemClick(i, -1);
                listView1.Items.Add(li);
                for (int j = 0; j < item.ListSubItem.Count; j++)
                {
                    Class.ProcessOrderNew.SubItem sub = item.ListSubItem[j];
                    li = new ListViewItem(sub.Qty + "");
                    li.SubItems.Add(sub.ItemName);
                    li.SubItems.Add(String.Format("{0:0.00}", Money.Format(sub.SubTotal)));
                    li.Tag = new ItemClick(i, j);
                    listView1.Items.Add(li);
                }
            }
            if (listView1.Items.Count > 0)
            {
                listView1.Items[listView1.Items.Count - 1].Selected = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private class ItemClick
        {
            public int ItemIndex { get; set; }

            public int ItemOptionIndex { get; set; }

            public ItemClick(int itemIndex, int itemOptionIndex)
            {
                ItemIndex = itemIndex;
                ItemOptionIndex = itemOptionIndex;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem li = listView1.SelectedItems[0];
                textBox2.Text = li.SubItems[1].Text;
                textBox5.Text = li.SubItems[0].Text;
                textBox4.Text = li.SubItems[2].Text;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem li = listView1.SelectedItems[0];
                listView1.Items.Remove(li);
                listView2.Items.Add(li);
                Change();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {
                ListViewItem li = listView2.SelectedItems[0];
                listView2.Items.Remove(li);
                listView1.Items.Add(li);
                Change();
            }
        }

        private void Change()
        {
            double balance = 0;
            foreach (ListViewItem li in listView1.Items)
            {
                balance += Convert.ToDouble(li.SubItems[2].Text);
            }
            textBox3.Text = String.Format("{0:0.00}", balance);
            double sub = 0;
            foreach (ListViewItem li in listView2.Items)
            {
                sub += Convert.ToDouble(li.SubItems[2].Text);
            }
            lblSplit.Text = String.Format("{0:0.00}", sub);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();
            textBox6.Text = "";
            textBox7.Text = "";
            lblSplit.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void textBox7_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void textBox7_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            if (lblSplit.Text == "")
            {
                textBox7.Text = "";
            }
            else
            {
                try
                {
                    textBox6.Text = String.Format("{0:0.00}", (Convert.ToDouble(textBox7.Text) - Convert.ToDouble(lblSplit.Text)));
                }
                catch (Exception)
                {
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
        }
    }
}