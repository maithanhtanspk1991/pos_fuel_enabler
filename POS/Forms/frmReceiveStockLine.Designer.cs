﻿namespace POS.Forms
{
    partial class frmReceiveStockLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lvItemChoses = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnRemoveList = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lvItemMenu = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtTotal = new POS.Controls.TextBoxPOSKeyPad();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUnitPrice = new POS.Controls.TextBoxPOSKeyPad();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrice = new POS.Controls.TextBoxPOSKeyPad();
            this.label7 = new System.Windows.Forms.Label();
            this.txtItemName = new POS.Controls.TextBoxPOSKeyBoard();
            this.label9 = new System.Windows.Forms.Label();
            this.txtQty = new POS.Controls.TextBoxPOSKeyPad();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbStatus = new System.Windows.Forms.Label();
            this.btnExit = new POS.Controls.ButtonExit();
            this.btnAccept = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtBarcodeSerialPort = new System.Windows.Forms.TextBox();
            this.btnFuel = new System.Windows.Forms.Button();
            this.btnListAll = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cbbGroup = new System.Windows.Forms.ComboBox();
            this.txtItemNameSearch = new POS.Controls.TextBoxPOSKeyBoard();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcodeSearch = new POS.Controls.TextBoxPOSKeyPad();
            this.label3 = new System.Windows.Forms.Label();
            this.lbBarcode = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 730);
            this.panel1.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.lvItemChoses);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(248, 678);
            this.panel9.TabIndex = 2;
            // 
            // lvItemChoses
            // 
            this.lvItemChoses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvItemChoses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvItemChoses.FullRowSelect = true;
            this.lvItemChoses.GridLines = true;
            this.lvItemChoses.Location = new System.Drawing.Point(0, 0);
            this.lvItemChoses.MultiSelect = false;
            this.lvItemChoses.Name = "lvItemChoses";
            this.lvItemChoses.Size = new System.Drawing.Size(248, 678);
            this.lvItemChoses.TabIndex = 0;
            this.lvItemChoses.UseCompatibleStateImageBehavior = false;
            this.lvItemChoses.View = System.Windows.Forms.View.Details;
            this.lvItemChoses.SelectedIndexChanged += new System.EventHandler(this.lvItemChoses_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item Name";
            this.columnHeader1.Width = 156;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Qty";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 71;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnRemoveList);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 678);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(248, 50);
            this.panel8.TabIndex = 1;
            // 
            // btnRemoveList
            // 
            this.btnRemoveList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnRemoveList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRemoveList.Location = new System.Drawing.Point(0, 0);
            this.btnRemoveList.Name = "btnRemoveList";
            this.btnRemoveList.Size = new System.Drawing.Size(246, 48);
            this.btnRemoveList.TabIndex = 0;
            this.btnRemoveList.Text = "Remove List";
            this.btnRemoveList.UseVisualStyleBackColor = false;
            this.btnRemoveList.Click += new System.EventHandler(this.btnRemoveList_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(250, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 730);
            this.panel2.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 111);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(758, 569);
            this.panel5.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lvItemMenu);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 74);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 495);
            this.panel7.TabIndex = 1;
            // 
            // lvItemMenu
            // 
            this.lvItemMenu.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader6});
            this.lvItemMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvItemMenu.FullRowSelect = true;
            this.lvItemMenu.GridLines = true;
            this.lvItemMenu.Location = new System.Drawing.Point(0, 0);
            this.lvItemMenu.MultiSelect = false;
            this.lvItemMenu.Name = "lvItemMenu";
            this.lvItemMenu.Size = new System.Drawing.Size(756, 493);
            this.lvItemMenu.TabIndex = 0;
            this.lvItemMenu.UseCompatibleStateImageBehavior = false;
            this.lvItemMenu.View = System.Windows.Forms.View.Details;
            this.lvItemMenu.SelectedIndexChanged += new System.EventHandler(this.lvItemMenu_SelectedIndexChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Barcode";
            this.columnHeader4.Width = 222;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Item Name";
            this.columnHeader3.Width = 202;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Unit Price";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 125;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "SellType";
            this.columnHeader6.Width = 113;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnAdd);
            this.panel6.Controls.Add(this.txtTotal);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.txtUnitPrice);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.txtPrice);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.txtItemName);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.txtQty);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 74);
            this.panel6.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAdd.Location = new System.Drawing.Point(647, 5);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(98, 58);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtTotal.IsLockDot = false;
            this.txtTotal.Location = new System.Drawing.Point(117, 37);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(117, 26);
            this.txtTotal.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 24);
            this.label8.TabIndex = 8;
            this.label8.Text = "Total ";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtUnitPrice.Enabled = false;
            this.txtUnitPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtUnitPrice.IsLockDot = false;
            this.txtUnitPrice.Location = new System.Drawing.Point(338, 37);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(101, 26);
            this.txtUnitPrice.TabIndex = 10;
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Unit Price: ";
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtPrice.IsLockDot = false;
            this.txtPrice.Location = new System.Drawing.Point(514, 37);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(127, 26);
            this.txtPrice.TabIndex = 1;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(445, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 24);
            this.label7.TabIndex = 7;
            this.label7.Text = "Price: ";
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtItemName.Enabled = false;
            this.txtItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtItemName.Location = new System.Drawing.Point(117, 5);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(322, 26);
            this.txtItemName.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 24);
            this.label9.TabIndex = 4;
            this.label9.Text = "Item Name:";
            // 
            // txtQty
            // 
            this.txtQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtQty.IsLockDot = true;
            this.txtQty.Location = new System.Drawing.Point(514, 5);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(127, 26);
            this.txtQty.TabIndex = 0;
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(445, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 24);
            this.label6.TabIndex = 3;
            this.label6.Text = "Qty: ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbStatus);
            this.panel4.Controls.Add(this.btnExit);
            this.panel4.Controls.Add(this.btnAccept);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 680);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(758, 50);
            this.panel4.TabIndex = 1;
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(132, 0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(478, 50);
            this.lbStatus.TabIndex = 13;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(132, 50);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAccept.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAccept.Location = new System.Drawing.Point(610, 0);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(148, 50);
            this.btnAccept.TabIndex = 11;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtBarcodeSerialPort);
            this.panel3.Controls.Add(this.btnFuel);
            this.panel3.Controls.Add(this.btnListAll);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Controls.Add(this.cbbGroup);
            this.panel3.Controls.Add(this.txtItemNameSearch);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtBarcodeSearch);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.lbBarcode);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(758, 111);
            this.panel3.TabIndex = 0;
            // 
            // txtBarcodeSerialPort
            // 
            this.txtBarcodeSerialPort.Location = new System.Drawing.Point(96, 35);
            this.txtBarcodeSerialPort.Name = "txtBarcodeSerialPort";
            this.txtBarcodeSerialPort.Size = new System.Drawing.Size(15, 29);
            this.txtBarcodeSerialPort.TabIndex = 12;
            this.txtBarcodeSerialPort.Visible = false;
            this.txtBarcodeSerialPort.TextChanged += new System.EventHandler(this.txtBarcodeSerialPort_TextChanged);
            // 
            // btnFuel
            // 
            this.btnFuel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnFuel.Location = new System.Drawing.Point(551, 2);
            this.btnFuel.Name = "btnFuel";
            this.btnFuel.Size = new System.Drawing.Size(98, 98);
            this.btnFuel.TabIndex = 3;
            this.btnFuel.Text = "Fuel";
            this.btnFuel.UseVisualStyleBackColor = false;
            this.btnFuel.Click += new System.EventHandler(this.btnFuel_Click);
            // 
            // btnListAll
            // 
            this.btnListAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnListAll.Location = new System.Drawing.Point(655, 3);
            this.btnListAll.Name = "btnListAll";
            this.btnListAll.Size = new System.Drawing.Size(98, 98);
            this.btnListAll.TabIndex = 4;
            this.btnListAll.Text = "List All";
            this.btnListAll.UseVisualStyleBackColor = false;
            this.btnListAll.Click += new System.EventHandler(this.btnListAll_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(583, -46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "button1";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSearch.Location = new System.Drawing.Point(449, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(98, 98);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Seacrch";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbGroup
            // 
            this.cbbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbGroup.FormattingEnabled = true;
            this.cbbGroup.Location = new System.Drawing.Point(117, 68);
            this.cbbGroup.Name = "cbbGroup";
            this.cbbGroup.Size = new System.Drawing.Size(322, 32);
            this.cbbGroup.TabIndex = 7;
            this.cbbGroup.SelectedIndexChanged += new System.EventHandler(this.cbbGroup_SelectedIndexChanged);
            // 
            // txtItemNameSearch
            // 
            this.txtItemNameSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtItemNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtItemNameSearch.Location = new System.Drawing.Point(117, 3);
            this.txtItemNameSearch.Name = "txtItemNameSearch";
            this.txtItemNameSearch.Size = new System.Drawing.Size(322, 26);
            this.txtItemNameSearch.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Item Name:";
            // 
            // txtBarcodeSearch
            // 
            this.txtBarcodeSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtBarcodeSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtBarcodeSearch.IsLockDot = false;
            this.txtBarcodeSearch.Location = new System.Drawing.Point(117, 35);
            this.txtBarcodeSearch.Name = "txtBarcodeSearch";
            this.txtBarcodeSearch.Size = new System.Drawing.Size(322, 26);
            this.txtBarcodeSearch.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "Group: ";
            // 
            // lbBarcode
            // 
            this.lbBarcode.AutoSize = true;
            this.lbBarcode.Location = new System.Drawing.Point(6, 35);
            this.lbBarcode.Name = "lbBarcode";
            this.lbBarcode.Size = new System.Drawing.Size(91, 24);
            this.lbBarcode.TabIndex = 3;
            this.lbBarcode.Text = "Barcode: ";
            // 
            // frmReceiveStockLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmReceiveStockLine";
            this.Text = "ReceiveStock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmReceiveStockLine_Load);
            this.panel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lvItemChoses;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView lvItemMenu;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private Controls.TextBoxPOSKeyBoard txtItemName;
        private System.Windows.Forms.Label label9;
        private Controls.TextBoxPOSKeyPad txtQty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private Controls.TextBoxPOSKeyPad txtTotal;
        private System.Windows.Forms.Label label8;
        private Controls.TextBoxPOSKeyPad txtPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbbGroup;
        private Controls.TextBoxPOSKeyBoard txtItemNameSearch;
        private System.Windows.Forms.Label label1;
        private Controls.TextBoxPOSKeyPad txtBarcodeSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbBarcode;
        private System.Windows.Forms.Button btnAdd;
        private Controls.ButtonExit btnExit;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnListAll;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRemoveList;
        private System.Windows.Forms.TextBox txtBarcodeSerialPort;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private Controls.TextBoxPOSKeyPad txtUnitPrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFuel;
    }
}