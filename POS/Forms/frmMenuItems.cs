﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMenuItems : Form
    {
        private SystemConfig.DBConfig mDBConfig = new SystemConfig.DBConfig();
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Connection.Connection mconnect = new Connection.Connection();
        private Class.MoneyFortmat money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        private Printer mPrinter;
        private Item mItem;
        private frmOrdersAll frmorder;
        private frmOrdersAllQuickSales frmorderQS;
        private ListView mlist;
        private Barcode.SerialPort mSerialPort;
        private bool mLockText = false;
        private int intPumpID;
        private int intIDPumpHistory;

        public frmMenuItems(ListView list, frmOrdersAll frm, Barcode.SerialPort port)
        {
            InitializeComponent();
            mlist = list;
            frmorder = frm;
            mSerialPort = port;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            mPrinter = new Printer();
            mPrinter.SetPrinterName(Class.PrintConfig.GetLabelPrinter());
            //mPrinter.SetPrinterName(new Class.Setting().GetBillPrinter());
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            timer1.Start();
        }

        public frmMenuItems(ListView list, frmOrdersAll frm, Barcode.SerialPort port, int intPumpID, int intIDPumpHistory)
        {
            InitializeComponent();
            mlist = list;
            frmorder = frm;
            mSerialPort = port;
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            mPrinter = new Printer();
            mPrinter.SetPrinterName(Class.PrintConfig.GetLabelPrinter());
            //mPrinter.SetPrinterName(new Class.Setting().GetBillPrinter());
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            timer1.Start();
        }

        public frmMenuItems(ListView list, frmOrdersAllQuickSales frm, Barcode.SerialPort port, int intPumpID, int intIDPumpHistory)
        {
            InitializeComponent();
            mlist = list;
            frmorderQS = frm;
            mSerialPort = port;
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            mPrinter = new Printer();
            mPrinter.SetPrinterName(Class.PrintConfig.GetLabelPrinter());
            //mPrinter.SetPrinterName(new Class.Setting().GetBillPrinter());
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            timer1.Start();
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 15;
            //l_y= mPrinter.DrawString(txtitemname.Text, e, new Font("Arial", 10), l_y, 3);
            l_y = mPrinter.DrawMessenge(txtitemname.Text, e, new Font("Arial", 8), l_y);
            string barcode = txtbarcode.Text;
            if (barcode.Length > 12)
            {
                barcode = barcode.Substring(0, 12);
            }
            l_y = mPrinter.DrawBarcode(barcode, e, l_y, 1);
            //l_y= mPrinter.DrawBarcode(barcode, e, l_y, 2);
            //l_y = mPrinter.DrawBarcode(barcode, e, l_y, 3);
            l_y = mPrinter.DrawString("$" + txtunit.Text, e, new Font("Arial", 10, FontStyle.Bold), l_y, 3);
        }

        private void mSerialPort_Received(string data)
        {
            mSerialPort.SetText("", txtbarcode);
            mSerialPort.SetText(data, txtbarcode);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private class Group
        {
            public string GroupID { get; set; }

            public string GroupName { get; set; }

            public int Visual { get; set; }
        }

        private class Item
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public double ItemPrice { get; set; }

            public string ScanCode { get; set; }

            public double Price1 { get; set; }

            public double Price2 { get; set; }

            public double GST { get; set; }

            public int EnableFuelDiscount { get; set; }

            public int SellSize { get; set; }

            public int IsFuel { get; set; }
        }

        private class ItemLine
        {
            public string lineID { get; set; }

            public string lineName { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lvListGroup.Items.Clear();
            List<DataObject.GroupMenu> lsArray = new List<DataObject.GroupMenu>();
            lsArray = BusinessObject.BOGroupMenu.GetGroupMenu(-1, -1, 0, true, mDBConfig);
            int no = 1;
            foreach (DataObject.GroupMenu item in lsArray)
            {
                ListViewItem lvi = new ListViewItem(no++ + "");
                lvi.SubItems.Add(item.GroupShort);
                lvi.SubItems.Add(item.Visual ? "Yes" : "No");
                lvi.Tag = item;
                lvListGroup.Items.Add(lvi);
            }
        }

        private DataTable GetGroup()
        {
            try
            {
                DataTable dt = new DataTable();
                mconnect.Open();
                dt = mconnect.Select("select groupID,groupShort,groupDesc,visual from groupsmenu where deleted<>1 and visual=0");
                mconnect.Close();
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private DataTable GetItems(string groupID)
        {
            try
            {
                DataTable dt = new DataTable();
                mconnect.Open();
                dt = mconnect.Select("select visual,sellSize,enableFuelDiscount,itemID,scanCode,itemShort,itemDesc,unitPrice,price1,price2,gst from itemsmenu where deleted<>1 and groupID=" + groupID);
                mconnect.Close();
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvListGroup.SelectedIndices.Count > 0)
            {
                listView2.Items.Clear();
                DataTable dt = new DataTable();
                DataObject.GroupMenu gm = (DataObject.GroupMenu)lvListGroup.SelectedItems[0].Tag;
                dt = GetItems(gm.GroupID + "");
                txtcategory.Text = gm.GroupShort;
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Item item = new Item();
                        item.ItemID = row["itemID"].ToString();
                        item.ItemName = row["itemDesc"].ToString();
                        item.ItemPrice = Convert.ToDouble(row["unitPrice"].ToString());
                        item.ScanCode = row["scanCode"].ToString();
                        item.GST = Convert.ToDouble(row["gst"].ToString());
                        item.EnableFuelDiscount = Convert.ToInt32(row["enableFuelDiscount"]);
                        item.SellSize = Convert.ToInt32(row["sellSize"]);
                        item.IsFuel = Convert.ToInt16(row["visual"]);
                        item.SellSize = 1000;
                        ListViewItem lvi = new ListViewItem(item.ItemName);
                        lvi.SubItems.Add(money.Format(item.ItemPrice));
                        lvi.Tag = item;
                        listView2.Items.Add(lvi);
                    }
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            mLockText = true;
            if (listView2.SelectedIndices.Count > 0)
            {
                Item item = (Item)listView2.SelectedItems[0].Tag;
                mItem = item;
                txtbarcode.Text = item.ScanCode;
                txtitemname.Text = item.ItemName;
                txtprice1.Text = item.Price1.ToString();
                txtprice2.Text = item.Price1.ToString();
                txtunit.Text = money.Format(item.ItemPrice);
                txtqty.Text = "";
                txtstock.Text = "";
                txtprint.Text = "1";
                button3_Click(button3, null);
            }
            mLockText = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                try
                {
                    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(intIDPumpHistory, Convert.ToInt32(mItem.ItemID), mItem.ItemName, 1, mItem.ItemPrice, mItem.ItemPrice, mItem.GST, 0, 0, intPumpID.ToString());
                    item.EnableFuelDiscount = mItem.EnableFuelDiscount;
                    item.BarCode = mItem.ScanCode;
                    item.ItemType = 0;

                    if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                    {
                        frmorderQS.Addnewitems(item);
                    }
                    else
                    {
                        frmorder.AddNewItems(item);
                    }

                    mconnect.Open();
                    mconnect.ExecuteNonQuery("insert into itemshistory(PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,IsMenuItems,IsNewItems,IsFindItems,pumpHistoryID) value(" +
                                                intPumpID + "," + Convert.ToInt32(mItem.ItemID) + ",'" + mItem.ItemName + "',1,'" + mItem.ItemPrice + "','" + mItem.ItemPrice + "','" + mItem.GST + "',0,0,0," + mItem.EnableFuelDiscount + ",'" + mItem.ScanCode + "',1,0,0," + intIDPumpHistory + ")");
                    mconnect.Close();
                }
                catch (Exception)
                {
                }
                mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
            else
            {
                posLabelStatus1.Text = "Please select item first.";
            }
        }

        private void txtbarcode_TextChanged(object sender, EventArgs e)
        {
            if (mLockText)
            {
                return;
            }
            mLockText = true;
            if (txtbarcode.Text != "")
            {
                try
                {
                    mconnect.Open();
                    DataTable dt = mconnect.Select("select * from itemsmenu where scanCode like '%" + txtbarcode.Text + "%'");
                    if (dt.Rows.Count > 0)
                    {
                        listView2.Items.Clear();
                        foreach (DataRow row in dt.Rows)
                        {
                            Item item = new Item();
                            item.ItemID = row["itemID"].ToString();
                            item.ItemName = row["itemDesc"].ToString();
                            item.ItemPrice = Convert.ToDouble(row["unitPrice"].ToString());
                            item.ScanCode = row["scanCode"].ToString();
                            item.GST = Convert.ToDouble(row["gst"].ToString());
                            item.EnableFuelDiscount = Convert.ToInt16(row["enableFuelDiscount"]);
                            ListViewItem lvi = new ListViewItem(item.ItemName);
                            lvi.SubItems.Add(money.Format(item.ItemPrice));
                            lvi.Tag = item;
                            lvi.Selected = true;
                            listView2.Items.Add(lvi);
                        }
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    mconnect.Close();
                }
            }
            mLockText = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                Item item = (Item)listView2.SelectedItems[0].Tag;
                double stockLevel = CheckStock(item.ItemID + "");
                if (item.IsFuel == 1)
                {
                    stockLevel = stockLevel / item.SellSize;
                }
                txtstock.Text = stockLevel + "";
            }
        }

        private double CheckStock(string itemID)
        {
            double qty = 0;
            try
            {
                mconnect.Open();
                mconnect.BeginTransaction();
                double bulkID = Convert.ToDouble(mconnect.ExecuteScalar("select if(bulkID is null,0,bulkID) from bulkmenulines where itemID=" + itemID + " limit 0,1"));
                if (bulkID > 0)
                {
                    //========================
                    DataTable tbl = mconnect.Select("select itemID,(select i.sellSize from itemsmenu i where i.itemID=bl.itemID) as size from bulkmenulines bl where bulkID=" + bulkID);
                    foreach (DataRow row in tbl.Rows)
                    {
                        qty += LoadQty(row["itemID"].ToString()) * Convert.ToInt16(row["size"]);
                    }
                    object obj = mconnect.ExecuteScalar("select unitsOnHand from products where bulkID=" + bulkID);
                    if (obj != null)
                    {
                        qty += Convert.ToDouble(obj);
                    }
                    double size = Convert.ToDouble(mconnect.ExecuteScalar("select i.sellSize from itemsmenu i where i.itemID=" + itemID));
                    if (size > 0)
                    {
                        qty = qty / size;
                    }
                }
                else
                {
                    qty += LoadQty(itemID);
                }
                mconnect.Commit();
            }
            catch (Exception)
            {
                mconnect.Rollback();
            }
            finally
            {
                mconnect.Close();
            }
            return qty;
        }

        private double LoadQty(string itemID)
        {
            double qty = 0;
            double qtySale = 0;
            double qtyReceived = 0;
            double qtyOnhand = 0;
            //qty ordersdaily
            object obj = mconnect.ExecuteScalar("select sum(qty) from ordersdailyline where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtySale += Convert.ToDouble(obj);
            }
            //qty ordersall
            obj = mconnect.ExecuteScalar("select sum(qty) from ordersallline where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtySale += Convert.ToDouble(obj);
            }
            //qtyreCeived
            obj = mconnect.ExecuteScalar("select sum(qty) from recvstocklines where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtyReceived += Convert.ToDouble(obj);
            }
            obj = mconnect.ExecuteScalar("select sum(unitsOnHand) from products where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtyOnhand += Convert.ToDouble(obj);
            }
            qty += (qtyReceived + qtyOnhand - qtySale);
            return qty;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtstock.Text == "")
                return;
            if (Convert.ToInt32(txtstock.Text) != 0)
            {
                if (listView2.SelectedItems.Count > 0)
                {
                    Item i = (Item)listView2.SelectedItems[0].Tag;
                    int areaID = 0;
                    //int qty = CheckStock(textBoxPOS_OSK_ItemID.Text);
                    ItemQty item = GetItemQty(i.ItemID + "");
                    try
                    {
                        mconnect.Open();
                        mconnect.BeginTransaction();
                        string itemID = i.ItemID + "";
                        double qtyCurent = Convert.ToDouble(txtstock.Text);
                        if (i.IsFuel == 1)
                        {
                            qtyCurent = qtyCurent * i.SellSize;
                        }
                        string sql = "select eventID from products where itemID=" + itemID;
                        if (item.BulkID != 0)
                        {
                            sql = "select eventID from products where bulkID=" + item.BulkID;
                        }
                        object obj = mconnect.ExecuteScalar(sql);
                        if (obj == DBNull.Value || obj == null)
                        {
                            sql = "insert into products(itemID) values(" + itemID + ")";
                            if (item.BulkID != 0)
                            {
                                sql = "insert into products(bulkID) values(" + item.BulkID + ")";
                            }
                            mconnect.ExecuteNonQuery(sql);
                            obj = mconnect.ExecuteScalar("select max(eventID) from products");
                        }
                        //mconnect.ExecuteNonQuery("update products set unitsOnHand=" + (qtyCurent * item.Size - item.Qty) + "+unitsOnHand where eventID=" + obj.ToString());
                        //mconnect.ExecuteNonQuery("CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + item.ItemID + "," + (qtyCurent * item.Size - item.Qty)*(-1) + ",3);");
                        //mconnect.Commit();
                        mconnect.ExecuteNonQuery("INSERT INTO products(itemID,unitsOnHand,EmployeeID,unitCurrent,unitUpdate) values(" + item.ItemID + "," + (qtyCurent * item.Size - item.Qty) + "," + frmLogin.intEmployeeID + "," + qtyCurent + "," + item.Qty + ")");
                        //mconnect.ExecuteNonQuery("update products set unitsOnHand=" + (qtyCurent * item.Size - item.Qty) + "+unitsOnHand where eventID=" + obj.ToString());
                        mconnect.ExecuteNonQuery("CALL SP_CALCULATOR_CHOSE_BALANCE_ITEM_QUANTITY_EXIST(" + item.ItemID + "," + (qtyCurent * item.Size - item.Qty) * (-1) + ",3);");
                        mconnect.Commit();
                        posLabelStatus1.Text = "Stock take SUCCESSFULLY " + (qtyCurent * item.Size - item.Qty) + " pcs " + i.ItemName;
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("UCCheckStock.buttonStockTake_Click:::" + ex.Message);
                        mconnect.Rollback();
                    }
                    finally
                    {
                        mconnect.Close();
                    }
                }
            }
        }

        private ItemQty GetItemQty(string itemID)
        {
            ItemQty item = new ItemQty(Convert.ToInt16(itemID), 1);
            try
            {
                mconnect.Open();
                mconnect.BeginTransaction();
                int bulkID = Convert.ToInt16(mconnect.ExecuteScalar("select if(bulkID is null,0,bulkID) from bulkmenulines where itemID=" + itemID + " limit 0,1"));
                if (bulkID > 0)
                {
                    //========================
                    DataTable tbl = mconnect.Select("select itemID,(select i.sellSize from itemsmenu i where i.itemID=bl.itemID) as size from bulkmenulines bl where bulkID=" + bulkID);
                    foreach (DataRow row in tbl.Rows)
                    {
                        item.Qty += LoadQty(row["itemID"].ToString()) * Convert.ToInt16(row["size"]);
                    }
                    object obj = mconnect.ExecuteScalar("select unitsOnHand from products where bulkID=" + bulkID);
                    if (obj != null)
                    {
                        item.Qty += Convert.ToDouble(obj);
                    }
                    item.Size = Convert.ToInt16(mconnect.ExecuteScalar("select i.sellSize from itemsmenu i where i.itemID=" + itemID));
                    item.BulkID = bulkID;
                }
                else
                {
                    item.Qty += LoadQty(itemID);
                }
                mconnect.Commit();
            }
            catch (Exception)
            {
                item = new ItemQty(Convert.ToInt16(itemID), 1);
                mconnect.Rollback();
            }
            finally
            {
                mconnect.Close();
            }
            return item;
        }

        public class ItemQty
        {
            public int ItemID { get; set; }

            public int Size { get; set; }

            public double Qty { get; set; }

            public int BulkID { get; set; }

            public ItemQty(int itemID, int size)
            {
                ItemID = itemID;
                Size = size;
            }
        }

        private void buttonPrintLabel_Click(object sender, EventArgs e)
        {
        }

        private void txtprint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
            }
        }

        private void buttonSearchItem_Click(object sender, EventArgs e)
        {
            frmSearchBarCode frm = new frmSearchBarCode(DataObject.FindItemType.FindItemInOrder);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                listView2.Items.Clear();
                Item item = new Item();
                item.ItemID = frm.ItemMenu.ItemID + "";
                item.ItemName = frm.ItemMenu.ItemName;
                item.ItemPrice = frm.ItemMenu.Price;
                item.ScanCode = frm.ItemMenu.BarCode;
                item.EnableFuelDiscount = frm.ItemMenu.EnableFuelDiscount;
                //ListViewItem li = new ListViewItem("1");
                //li.SubItems.Add(item.ItemName);
                ListViewItem li = new ListViewItem(item.ItemName);
                li.SubItems.Add(money.Format(item.ItemPrice));
                li.Tag = item;
                li.Selected = true;
                listView2.Items.Add(li);
            }
        }

        private void buttonReceivedStock_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                Item item = (Item)listView2.SelectedItems[0].Tag;
                try
                {
                    mconnect.Open();
                    mconnect.BeginTransaction();
                    double qty = Convert.ToDouble(txtqty.Text);
                    if (item.IsFuel == 1)
                    {
                        qty = qty * item.SellSize;
                    }
                    //mconnect.ExecuteNonQuery("insert into receivedstock(areaID,TypeReceive) values(" + ucInfoTop1.Tag.ToString() + ",1)");
                    //int receivedID = Convert.ToInt16(mconnect.ExecuteScalar("select max(receivedID) from receivedstock"));
                    //mconnect.ExecuteNonQuery(
                    //    //"insert into recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) values(" + receivedID + "," + item.ItemID + "," + qty + "," + money.getFortMat(item.ItemPrice * qty) + "," + qty + "," +item.ItemPrice + ")"
                    //    "insert into recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) values(" + receivedID + "," + item.ItemID + "," + qty + "," + money.getFortMat(txtCosPrice.Text) + "," + qty + "," + item.ItemPrice + ")"
                    //);
                    //mconnect.Commit();
                    mconnect.ExecuteNonQuery("insert into receivedstock(areaID,TypeReceive,EmployeeID) values(" + ucInfoTop1.Tag.ToString() + ",1," + frmLogin.intEmployeeID + ")");
                    int receivedID = Convert.ToInt16(mconnect.ExecuteScalar("select max(receivedID) from receivedstock"));
                    mconnect.ExecuteNonQuery(
                        //"insert into recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) values(" + receivedID + "," + item.ItemID + "," + qty + "," + money.getFortMat(item.ItemPrice * qty) + "," + qty + "," +item.ItemPrice + ")"
                        "insert into recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) values(" + receivedID + "," + item.ItemID + "," + qty + "," + money.getFortMat(txtCosPrice.Text) + "," + qty + "," + item.ItemPrice + ")"
                    );
                    mconnect.Commit();
                    posLabelStatus1.Text = "Receive SUCCESSFULLY " + txtqty.Text + " pcs " + item.ItemName;
                }
                catch (Exception)
                {
                    mconnect.Rollback();
                }
                finally
                {
                    mconnect.Close();
                }
                button3_Click(button3, null);
            }
        }

        private void frmMenuItems_Load(object sender, EventArgs e)
        {
            button1_Click(null, null);
        }

        private int i = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (i == 50)
            {
                posLabelStatus1.Text = "";
                i = 0;
            }
            i++;
        }

        private void btnPrintLabel_Click(object sender, EventArgs e)
        {
            mPrinter.Print();
        }        
        
    }
}