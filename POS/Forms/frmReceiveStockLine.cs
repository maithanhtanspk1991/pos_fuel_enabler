﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmReceiveStockLine : Form
    {
        private SystemConfig.DBConfig mDBConfig;
        private DataObject.Transit mTransit;
        private Barcode.SerialPort mSerialPortBarcode;
        private Class.MoneyFortmat mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

        public DataObject.ItemsMenu mItemsMenu { get; set; }

        public DataObject.ReceiveLine mReceiveLine { get; set; }

        public List<DataObject.ReceiveLine> lsReceiveLine { get; set; }

        public bool LockChangeGroup = true;

        public frmReceiveStockLine(SystemConfig.DBConfig dBConfig, DataObject.Transit transit, Barcode.SerialPort serialPortBarcode)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
            mTransit = transit;
            mSerialPortBarcode = serialPortBarcode;
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPortBarcode.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            lsReceiveLine = new List<DataObject.ReceiveLine>();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        #region LoadData

        private void mSerialPort_Received(string data)
        {
            if (mSerialPortBarcode.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPortBarcode.SetText("", txtBarcodeSerialPort);
                mSerialPortBarcode.SetText(data, txtBarcodeSerialPort);
            }
        }

        public void LoadGroup()
        {
            List<DataObject.GroupMenu> lsArray = new List<DataObject.GroupMenu>();
            lsArray = BusinessObject.BOGroupMenu.GetGroupMenu(-1, -1, 0, false, mDBConfig);
            DataObject.GroupMenu item = new DataObject.GroupMenu();
            item.GroupID = -1;
            item.GroupShort = "[All]";
            lsArray.Insert(0, item);
            cbbGroup.DataSource = lsArray;
            cbbGroup.DisplayMember = "GroupShort";
            cbbGroup.ValueMember = "GroupID";
            cbbGroup.SelectedIndex = 0;
            LockChangeGroup = false;
        }

        private void LoadListView(string ItemName, string Barcode, int GroupID, int isFuel)
        {
            lvItemMenu.Items.Clear();
            List<DataObject.ItemsMenu> lsArray = new List<DataObject.ItemsMenu>();
            lsArray = BusinessObject.BOItemsMenu.GetFindItem(GroupID, Barcode, ItemName, isFuel, mDBConfig);
            foreach (DataObject.ItemsMenu item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.Barcode);
                li.SubItems.Add(item.ItemShort);
                li.SubItems.Add(mMoney.Format2(item.UnitPrice));
                li.SubItems.Add(BusinessObject.FunctionEnum.GetSellType(item.SellType).ToString());
                li.Tag = item;
                lvItemMenu.Items.Add(li);
            }
        }

        private void SetTextBoxItemsMenu()
        {
            txtPrice.Text = "";
            txtQty.Text = "";
            txtTotal.Text = "";
            txtItemName.Text = mItemsMenu.ItemShort;
            txtUnitPrice.Text = mMoney.Format2(mItemsMenu.UnitPrice);
        }

        private void SetTextBoxReceiveLine()
        {
            txtItemName.Text = mReceiveLine.ItemsMenu.ItemShort;
            txtUnitPrice.Text = mMoney.Format2(mReceiveLine.ItemsMenu.UnitPrice);
            txtPrice.Text = mReceiveLine.Price.ToString();
            txtQty.Text = mReceiveLine.Qty.ToString();
            Total();
        }

        private void AddListViewChose(DataObject.ReceiveLine item)
        {
            ListViewItem li = new ListViewItem(item.ItemsMenu.ItemShort);
            li.SubItems.Add(item.Qty.ToString());
            li.Tag = item;
            lvItemChoses.Items.Add(li);
        }

        public void Total()
        {
            if (txtPrice.Text != "" && txtQty.Text != "")
            {
                double Price = mMoney.getFortMatNew(txtPrice.Text.Trim());
                int Qty = Convert.ToInt32(txtQty.Text.Trim());
                txtTotal.Text = mMoney.FormatNew2(Price * Qty);
            }
            else
            {
                txtTotal.Text = "0";
            }
        }

        private void SetSizeListView()
        {
            int lvItemMenuWidth = lvItemMenu.Width;
            lvItemMenu.Columns[0].Width = 20 * lvItemMenuWidth / 100;
            lvItemMenu.Columns[1].Width = 50 * lvItemMenuWidth / 100;
            lvItemMenu.Columns[2].Width = 17 * lvItemMenuWidth / 100;
            lvItemMenu.Columns[3].Width = 10 * lvItemMenuWidth / 100;

            int lvItemChosesWidth = lvItemChoses.Width;
            lvItemChoses.Columns[0].Width = 70 * lvItemChosesWidth / 100;
            lvItemChoses.Columns[1].Width = 30 * lvItemChosesWidth / 100;
        }

        #endregion LoadData

        private void frmReceiveStockLine_Load(object sender, EventArgs e)
        {
            LoadGroup();
            SetSizeListView();
            btnFuel_Click(sender, e);
        }

        private void txtBarcodeSerialPort_TextChanged(object sender, EventArgs e)
        {
            string ItemName = "";
            string Barcode = txtBarcodeSerialPort.Text.Trim();
            int GroupID = Convert.ToInt32(cbbGroup.SelectedValue);
            LoadListView(ItemName, Barcode, GroupID, -1);
        }

        private void LoadItem()
        {
            string ItemName = txtItemNameSearch.Text.Trim();
            string Barcode = txtBarcodeSearch.Text.Trim();
            int GroupID = Convert.ToInt32(cbbGroup.SelectedValue.ToString());
            List<DataObject.ItemsMenu> lsArray = new List<DataObject.ItemsMenu>();
            LoadListView(ItemName, Barcode, GroupID, -1);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadItem();
        }

        private void btnListAll_Click(object sender, EventArgs e)
        {
            txtItemNameSearch.Text = "";
            txtBarcodeSearch.Text = "";
            cbbGroup.SelectedIndex = 0;
            LoadItem();
        }

        private void cbbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!LockChangeGroup)
                LoadItem();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (btnAdd.Text == "Add")
            {
                if (txtQty.Text == "" || txtPrice.Text == "")
                {
                    lbStatus.Text = "Imformation Empty";
                    return;
                }
                DataObject.ReceiveLine item = new DataObject.ReceiveLine();
                item.ItemsMenu = mItemsMenu.Copy();
                item.ItemID = mItemsMenu.ItemID;
                item.Qty = Convert.ToInt32(txtQty.Text.Trim());
                item.Price = Convert.ToDouble(txtPrice.Text.Trim());
                item.Total = item.Qty * item.Price;
                lbStatus.Text = "Add Successfull";
                AddListViewChose(item);
            }
            else
            {
                if (lvItemChoses.SelectedItems.Count > 0)
                {
                    if (txtQty.Text == "" || txtPrice.Text == "")
                    {
                        lbStatus.Text = "Imformation Empty";
                        return;
                    }
                    ListViewItem li = lvItemChoses.SelectedItems[0];
                    DataObject.ReceiveLine item = ((DataObject.ReceiveLine)lvItemChoses.SelectedItems[0].Tag);
                    item.Qty = Convert.ToInt32(txtQty.Text.Trim());
                    item.Price = Convert.ToDouble(txtPrice.Text.Trim());
                    item.Total = item.Qty * item.Price;
                    li.SubItems[1].Text = txtQty.Text;
                    lbStatus.Text = "Update Successfull";
                }
            }
        }

        private void lvItemMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvItemMenu.SelectedItems.Count > 0)
            {
                lbStatus.Text = "";
                btnAdd.Text = "Add";
                mItemsMenu = ((DataObject.ItemsMenu)lvItemMenu.SelectedItems[0].Tag);
                SetTextBoxItemsMenu();
            }
        }

        private void lvItemChoses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvItemChoses.SelectedItems.Count > 0)
            {
                mReceiveLine = ((DataObject.ReceiveLine)lvItemChoses.SelectedItems[0].Tag);
                lbStatus.Text = "";
                btnAdd.Text = "Update";
                SetTextBoxReceiveLine();
            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            Total();
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            Total();
        }

        private void btnRemoveList_Click(object sender, EventArgs e)
        {
            if (lvItemChoses.SelectedItems.Count > 0)
            {
                lvItemChoses.SelectedItems[0].Remove();
                lbStatus.Text = "Remove Successfull";
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (lvItemChoses.Items.Count == 0)
            {
                lbStatus.Text = "List empty";
                return;
            }
            lsReceiveLine.Clear();
            foreach (ListViewItem li in lvItemChoses.Items)
            {
                DataObject.ReceiveLine item = (DataObject.ReceiveLine)li.Tag;
                lsReceiveLine.Add(item.Copy());
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnFuel_Click(object sender, EventArgs e)
        {
            string ItemName = "";
            string Barcode = "";
            int GroupID = -1;
            LoadListView(ItemName, Barcode, GroupID, 1);
        }
    }
}