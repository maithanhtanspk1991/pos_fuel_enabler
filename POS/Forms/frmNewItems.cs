﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmNewItems : Form
    {
        private Printer mPrinter;
        private frmOrdersAll frmorder;
        private frmOrdersAllQuickSales frmorderQS;
        private double GST;
        private Connection.Connection mConnection;
        private Class.MoneyFortmat mMoney;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Class.ProcessOrderNew.Order order;
        private ListView listnew;
        private int intPumpID;
        private int intIDPumpHistory;
        public int CableID = 0;
        private Class.ReadConfig mReafConfig = new Class.ReadConfig();

        public frmNewItems(ListView listold, Class.MoneyFortmat money, frmOrdersAll frm, int cableID)
        {
            InitializeComponent();
            SetMutiLanguage();
            mConnection = new Connection.Connection();
            mMoney = money;
            listnew = listold;
            frmorder = frm;
            CableID = cableID;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        public frmNewItems(ListView listold, Class.MoneyFortmat money, frmOrdersAllQuickSales frm)
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            mMoney = money;
            listnew = listold;
            frmorderQS = frm;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        public frmNewItems(ListView listold, Class.MoneyFortmat money, int intPumpID, int intIDPumpHistory, frmOrdersAll frm)
        {
            InitializeComponent();
            SetMutiLanguage();
            mConnection = new Connection.Connection();
            mMoney = money;
            listnew = listold;
            frmorder = frm;
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        public frmNewItems(ListView listold, Class.MoneyFortmat money, int intPumpID, int intIDPumpHistory, frmOrdersAllQuickSales frm)
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            mMoney = money;
            listnew = listold;
            frmorderQS = frm;
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        private void SetMutiLanguage()
        {
            if (mReafConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Thêm sản phẩm mới";
                label1.Text = "Tên SP:";
                //label1.Font = new Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label4.Text = "Đơn vị giá:";
                button1.Text = "CHỌN";
                button2.Text = "QUAY LẠI";
                return;
            }
        }

        public void Department(string name, double gst)
        {
            txtitemname.Text = name;
            GST = gst;
            txtunitprice_Enter(txtunitprice, null);
        }

        private void Newitem(Class.ProcessOrderNew.Item item)
        {
            try
            {
                order.ListItem.Add(item.Coppy());
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(ex.Message);
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;
            string header = "BECAS POS DEMO";
            string bankCode = "ABN. 1234567890";
            string address = "133 Alexander Street, Crows Nest 2065";
            string tell = "T:94327867 M:0401950967";

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 100;

            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            l_y += 100;

            if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
            {
                mPrinter.DrawString("ORDER# " + frmorderQS.txtorderid.Text, e, font11, l_y, 1);
                if (frmorderQS.txttableid.Text == "")
                {
                    frmorderQS.txttableid.Text = "TKA";
                }
                l_y = mPrinter.DrawString("TABLE# " + frmorderQS.txttableid.Text, e, font11, l_y, 3);
                l_y += 300;
            }
            else
            {
                mPrinter.DrawString("ORDER# " + frmorder.txtOrderID.Text, e, font11, l_y, 1);
                if (frmorder.txtTableID.Text == "")
                {
                    frmorder.txtTableID.Text = "TKA";
                }
                l_y = mPrinter.DrawString("TABLE# " + frmorder.txtTableID.Text, e, font11, l_y, 3);
                l_y += 300;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (mReafConfig.LanguageCode.Equals("vi"))
            {
                if (txtitemname.Text == "")
                {

                    posLabelStatus1.Text = "Tên sản phẩm không hợp lệ";

                }
                else if (txtunitprice.Text == "" || Convert.ToDouble(txtunitprice.Text) >= 10000)
                {

                    posLabelStatus1.Text = "Invalid Unit Price";
                }
                else
                {
                    try
                    {
                        string sql = "";
                        mConnection.Open();
                        mConnection.BeginTransaction();

                        if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                        {
                            sql = "insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice,groupID,PumID,cableID) value(" + frmorderQS.order.ShiftID + "," + "'" + txtitemname.Text + "'" + "," + "'" + txtitemname.Text + "'" + "," + mMoney.getFortMat(txtunitprice.Text) + "," + "0," + intPumpID + ", " + CableID + ")";
                            mConnection.ExecuteNonQuery(sql);
                        }
                        else
                        {
                            sql = "insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice,groupID,PumID,cableID) value(" + frmorder.mOrderMain.ShiftID + "," + "'" + txtitemname.Text + "'" + "," + "'" + txtitemname.Text + "'" + "," + mMoney.getFortMat(txtunitprice.Text) + "," + "0," + intPumpID + ", " + CableID + ")";
                            mConnection.ExecuteNonQuery(sql);
                        }

                        int itemID = Convert.ToInt16(mConnection.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                        Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(intIDPumpHistory, itemID, txtitemname.Text, 1, Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)), Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)), 0, 0, 0, intPumpID.ToString());
                        item.GST = GST;

                        item.ItemType = 1;

                        if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                        {
                            frmorderQS.Addnewitems(item);
                        }
                        else
                        {
                            frmorder.AddNewItems(item);
                        }

                        mConnection.ExecuteNonQuery("insert into itemshistory(PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,IsMenuItems,IsNewItems,IsFindItems,pumpHistoryID) value(" +
                                                    intPumpID + "," + itemID + ",'" + txtitemname.Text + "',1,'" + Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)) + "','" + Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)) + "',0,0,0,1,0,'',0,1,0," + intIDPumpHistory + ")");
                        mConnection.Commit();
                    }
                    catch (Exception)
                    {
                        mConnection.Rollback();
                    }
                    finally
                    {
                        mConnection.Close();
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
            else
            {
                if (txtitemname.Text == "")
                {

                    posLabelStatus1.Text = "Invalid Item Name";
                }
                else if (txtunitprice.Text == "" || Convert.ToDouble(txtunitprice.Text) >= 10000)
                {
                    posLabelStatus1.Text = "Invalid Unit Price";
                }
                else
                {
                    try
                    {
                        string sql = "";
                        mConnection.Open();
                        mConnection.BeginTransaction();

                        if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                        {
                            sql = "insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice,groupID,PumID,cableID) value(" + frmorderQS.order.ShiftID + "," + "'" + txtitemname.Text + "'" + "," + "'" + txtitemname.Text + "'" + "," + mMoney.getFortMat(txtunitprice.Text) + "," + "0," + intPumpID + ", " + CableID + ")";
                            mConnection.ExecuteNonQuery(sql);
                        }
                        else
                        {
                            sql = "insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice,groupID,PumID,cableID) value(" + frmorder.mOrderMain.ShiftID + "," + "'" + txtitemname.Text + "'" + "," + "'" + txtitemname.Text + "'" + "," + mMoney.getFortMat(txtunitprice.Text) + "," + "0," + intPumpID + ", " + CableID + ")";
                            mConnection.ExecuteNonQuery(sql);
                        }

                        int itemID = Convert.ToInt16(mConnection.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                        Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(intIDPumpHistory, itemID, txtitemname.Text, 1, Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)), Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)), 0, 0, 0, intPumpID.ToString());
                        item.GST = GST;

                        item.ItemType = 1;

                        if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                        {
                            frmorderQS.Addnewitems(item);
                        }
                        else
                        {
                            frmorder.AddNewItems(item);
                        }

                        mConnection.ExecuteNonQuery("insert into itemshistory(PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,IsMenuItems,IsNewItems,IsFindItems,pumpHistoryID) value(" +
                                                    intPumpID + "," + itemID + ",'" + txtitemname.Text + "',1,'" + Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)) + "','" + Convert.ToDouble(mMoney.getFortMat(txtunitprice.Text)) + "',0,0,0,1,0,'',0,1,0," + intIDPumpHistory + ")");
                        mConnection.Commit();
                    }
                    catch (Exception)
                    {
                        mConnection.Rollback();
                    }
                    finally
                    {
                        mConnection.Close();
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
        }

        private void txtitemname_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtitemname);
            txtitemname.Tag = txtitemname.BackColor;
            txtitemname.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void txtitemname_Leave(object sender, EventArgs e)
        {
            if (txtitemname.Tag != null)
            {
                txtitemname.BackColor = (Color)txtitemname.Tag;
            }
        }

        private void txtunitprice_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void txtunitprice_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {

        }
    }
}