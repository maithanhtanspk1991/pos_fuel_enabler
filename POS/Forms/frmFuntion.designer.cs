﻿namespace POS.Forms
{
    partial class frmFuntion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFuntion));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pnTitle = new System.Windows.Forms.Panel();
            this.ucInfoTop1 = new POS.UCInfoTop();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnContent = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnStockBalance = new System.Windows.Forms.Button();
            this.btnStockCheck = new System.Windows.Forms.Button();
            this.btnProfitAndLoss = new System.Windows.Forms.Button();
            this.btnFuelItemChange = new System.Windows.Forms.Button();
            this.btnMenuChange = new System.Windows.Forms.Button();
            this.btnComboDisc = new System.Windows.Forms.Button();
            this.btnCashIn = new System.Windows.Forms.Button();
            this.btnReportDaily = new System.Windows.Forms.Button();
            this.btnReportWeekly = new System.Windows.Forms.Button();
            this.btnPayOut = new System.Windows.Forms.Button();
            this.btnStaffMan = new System.Windows.Forms.Button();
            this.btnSafeDrop = new System.Windows.Forms.Button();
            this.btnChangeStaff = new System.Windows.Forms.Button();
            this.btnVoidHistory = new System.Windows.Forms.Button();
            this.btnFuelAutoBecas = new System.Windows.Forms.Button();
            this.btnPersonalAccount = new System.Windows.Forms.Button();
            this.btnCompanyAccount = new System.Windows.Forms.Button();
            this.btnShutDown = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnShiftReport = new System.Windows.Forms.Button();
            this.btnSetPump = new System.Windows.Forms.Button();
            this.btnCard = new System.Windows.Forms.Button();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.btnControlPanel = new System.Windows.Forms.Button();
            this.btnOpenTill = new System.Windows.Forms.Button();
            this.btnStock = new System.Windows.Forms.Button();
            this.btnSetTime = new System.Windows.Forms.Button();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.btnSetBarcodeItem = new System.Windows.Forms.Button();
            this.btnTank = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnTitle.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pnTitle);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(860, 509);
            this.splitContainer1.SplitterDistance = 30;
            this.splitContainer1.TabIndex = 0;
            // 
            // pnTitle
            // 
            this.pnTitle.Controls.Add(this.ucInfoTop1);
            this.pnTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTitle.Location = new System.Drawing.Point(0, 0);
            this.pnTitle.Name = "pnTitle";
            this.pnTitle.Size = new System.Drawing.Size(858, 28);
            this.pnTitle.TabIndex = 0;
            // 
            // ucInfoTop1
            // 
            this.ucInfoTop1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(85)))), ((int)(((byte)(160)))));
            this.ucInfoTop1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucInfoTop1.Location = new System.Drawing.Point(0, 0);
            this.ucInfoTop1.Name = "ucInfoTop1";
            this.ucInfoTop1.Size = new System.Drawing.Size(858, 28);
            this.ucInfoTop1.TabIndex = 0;
            this.ucInfoTop1.Tag = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnContent);
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(858, 420);
            this.panel2.TabIndex = 4;
            // 
            // pnContent
            // 
            this.pnContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(240, 0);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(618, 420);
            this.pnContent.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.btnStockBalance, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.btnStockCheck, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.btnProfitAndLoss, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.btnFuelItemChange, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnMenuChange, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnComboDisc, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnCashIn, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnReportDaily, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnReportWeekly, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPayOut, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnStaffMan, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnSafeDrop, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnChangeStaff, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnVoidHistory, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFuelAutoBecas, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnPersonalAccount, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnCompanyAccount, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnShutDown, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnSetting, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnShiftReport, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnSetPump, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnCard, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnChangePass, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnControlPanel, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnOpenTill, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnStock, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnSetTime, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnSendMail, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.btnSetBarcodeItem, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.btnTank, 2, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(240, 420);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // btnStockBalance
            // 
            this.btnStockBalance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnStockBalance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStockBalance.Enabled = false;
            this.btnStockBalance.FlatAppearance.BorderSize = 0;
            this.btnStockBalance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockBalance.ForeColor = System.Drawing.Color.White;
            this.btnStockBalance.Location = new System.Drawing.Point(2, 371);
            this.btnStockBalance.Margin = new System.Windows.Forms.Padding(1);
            this.btnStockBalance.Name = "btnStockBalance";
            this.btnStockBalance.Size = new System.Drawing.Size(76, 47);
            this.btnStockBalance.TabIndex = 26;
            this.btnStockBalance.UseVisualStyleBackColor = false;
            this.btnStockBalance.Click += new System.EventHandler(this.btnStockBalance_Click);
            // 
            // btnStockCheck
            // 
            this.btnStockCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnStockCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStockCheck.Enabled = false;
            this.btnStockCheck.FlatAppearance.BorderSize = 0;
            this.btnStockCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockCheck.ForeColor = System.Drawing.Color.White;
            this.btnStockCheck.Location = new System.Drawing.Point(160, 371);
            this.btnStockCheck.Margin = new System.Windows.Forms.Padding(1);
            this.btnStockCheck.Name = "btnStockCheck";
            this.btnStockCheck.Size = new System.Drawing.Size(78, 47);
            this.btnStockCheck.TabIndex = 18;
            this.btnStockCheck.UseVisualStyleBackColor = false;
            this.btnStockCheck.Click += new System.EventHandler(this.btnStockCheck_Click);
            // 
            // btnProfitAndLoss
            // 
            this.btnProfitAndLoss.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnProfitAndLoss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnProfitAndLoss.Enabled = false;
            this.btnProfitAndLoss.FlatAppearance.BorderSize = 0;
            this.btnProfitAndLoss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfitAndLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfitAndLoss.ForeColor = System.Drawing.Color.White;
            this.btnProfitAndLoss.Location = new System.Drawing.Point(81, 371);
            this.btnProfitAndLoss.Margin = new System.Windows.Forms.Padding(1);
            this.btnProfitAndLoss.Name = "btnProfitAndLoss";
            this.btnProfitAndLoss.Size = new System.Drawing.Size(76, 47);
            this.btnProfitAndLoss.TabIndex = 25;
            this.btnProfitAndLoss.UseVisualStyleBackColor = false;
            this.btnProfitAndLoss.Click += new System.EventHandler(this.btnProfitAndLoss_Click);
            // 
            // btnFuelItemChange
            // 
            this.btnFuelItemChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnFuelItemChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuelItemChange.FlatAppearance.BorderSize = 0;
            this.btnFuelItemChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuelItemChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuelItemChange.ForeColor = System.Drawing.Color.White;
            this.btnFuelItemChange.Location = new System.Drawing.Point(81, 125);
            this.btnFuelItemChange.Margin = new System.Windows.Forms.Padding(1);
            this.btnFuelItemChange.Name = "btnFuelItemChange";
            this.btnFuelItemChange.Size = new System.Drawing.Size(76, 38);
            this.btnFuelItemChange.TabIndex = 10;
            this.btnFuelItemChange.Text = "FUEL ITEM CHANGE";
            this.btnFuelItemChange.UseVisualStyleBackColor = false;
            this.btnFuelItemChange.Click += new System.EventHandler(this.btnFuelItemChange_Click);
            // 
            // btnMenuChange
            // 
            this.btnMenuChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnMenuChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMenuChange.FlatAppearance.BorderSize = 0;
            this.btnMenuChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuChange.ForeColor = System.Drawing.Color.White;
            this.btnMenuChange.Location = new System.Drawing.Point(2, 125);
            this.btnMenuChange.Margin = new System.Windows.Forms.Padding(1);
            this.btnMenuChange.Name = "btnMenuChange";
            this.btnMenuChange.Size = new System.Drawing.Size(76, 38);
            this.btnMenuChange.TabIndex = 9;
            this.btnMenuChange.Text = "MENU CHANGE";
            this.btnMenuChange.UseVisualStyleBackColor = false;
            this.btnMenuChange.Click += new System.EventHandler(this.btnMenuChange_Click);
            // 
            // btnComboDisc
            // 
            this.btnComboDisc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnComboDisc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnComboDisc.FlatAppearance.BorderSize = 0;
            this.btnComboDisc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComboDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboDisc.ForeColor = System.Drawing.Color.White;
            this.btnComboDisc.Location = new System.Drawing.Point(160, 125);
            this.btnComboDisc.Margin = new System.Windows.Forms.Padding(1);
            this.btnComboDisc.Name = "btnComboDisc";
            this.btnComboDisc.Size = new System.Drawing.Size(78, 38);
            this.btnComboDisc.TabIndex = 17;
            this.btnComboDisc.Text = "COMBO DISC";
            this.btnComboDisc.UseVisualStyleBackColor = false;
            this.btnComboDisc.Click += new System.EventHandler(this.btnComboDisc_Click);
            // 
            // btnCashIn
            // 
            this.btnCashIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnCashIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCashIn.FlatAppearance.BorderSize = 0;
            this.btnCashIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCashIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCashIn.ForeColor = System.Drawing.Color.White;
            this.btnCashIn.Location = new System.Drawing.Point(2, 43);
            this.btnCashIn.Margin = new System.Windows.Forms.Padding(1);
            this.btnCashIn.Name = "btnCashIn";
            this.btnCashIn.Size = new System.Drawing.Size(76, 38);
            this.btnCashIn.TabIndex = 28;
            this.btnCashIn.Text = "CASH IN";
            this.btnCashIn.UseVisualStyleBackColor = false;
            this.btnCashIn.Click += new System.EventHandler(this.btnCashIn_Click);
            // 
            // btnReportDaily
            // 
            this.btnReportDaily.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnReportDaily.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReportDaily.FlatAppearance.BorderSize = 0;
            this.btnReportDaily.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportDaily.ForeColor = System.Drawing.Color.White;
            this.btnReportDaily.Location = new System.Drawing.Point(2, 2);
            this.btnReportDaily.Margin = new System.Windows.Forms.Padding(1);
            this.btnReportDaily.Name = "btnReportDaily";
            this.btnReportDaily.Size = new System.Drawing.Size(76, 38);
            this.btnReportDaily.TabIndex = 3;
            this.btnReportDaily.Text = "REPORT DAILY";
            this.btnReportDaily.UseVisualStyleBackColor = false;
            this.btnReportDaily.Click += new System.EventHandler(this.btnReportDaily_Click);
            // 
            // btnReportWeekly
            // 
            this.btnReportWeekly.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnReportWeekly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReportWeekly.FlatAppearance.BorderSize = 0;
            this.btnReportWeekly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportWeekly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportWeekly.ForeColor = System.Drawing.Color.White;
            this.btnReportWeekly.Location = new System.Drawing.Point(81, 2);
            this.btnReportWeekly.Margin = new System.Windows.Forms.Padding(1);
            this.btnReportWeekly.Name = "btnReportWeekly";
            this.btnReportWeekly.Size = new System.Drawing.Size(76, 38);
            this.btnReportWeekly.TabIndex = 4;
            this.btnReportWeekly.Text = "REPORT WEEKLY";
            this.btnReportWeekly.UseVisualStyleBackColor = false;
            this.btnReportWeekly.Click += new System.EventHandler(this.btnReportWeekly_Click);
            // 
            // btnPayOut
            // 
            this.btnPayOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnPayOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPayOut.FlatAppearance.BorderSize = 0;
            this.btnPayOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayOut.ForeColor = System.Drawing.Color.White;
            this.btnPayOut.Location = new System.Drawing.Point(81, 43);
            this.btnPayOut.Margin = new System.Windows.Forms.Padding(1);
            this.btnPayOut.Name = "btnPayOut";
            this.btnPayOut.Size = new System.Drawing.Size(76, 38);
            this.btnPayOut.TabIndex = 27;
            this.btnPayOut.Text = "PAY OUT";
            this.btnPayOut.UseVisualStyleBackColor = false;
            this.btnPayOut.Click += new System.EventHandler(this.btnPayOut_Click);
            // 
            // btnStaffMan
            // 
            this.btnStaffMan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnStaffMan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStaffMan.FlatAppearance.BorderSize = 0;
            this.btnStaffMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStaffMan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffMan.ForeColor = System.Drawing.Color.White;
            this.btnStaffMan.Location = new System.Drawing.Point(2, 84);
            this.btnStaffMan.Margin = new System.Windows.Forms.Padding(1);
            this.btnStaffMan.Name = "btnStaffMan";
            this.btnStaffMan.Size = new System.Drawing.Size(76, 38);
            this.btnStaffMan.TabIndex = 8;
            this.btnStaffMan.Text = "STAFF MAN";
            this.btnStaffMan.UseVisualStyleBackColor = false;
            this.btnStaffMan.Click += new System.EventHandler(this.btnStaffMan_Click);
            // 
            // btnSafeDrop
            // 
            this.btnSafeDrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSafeDrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSafeDrop.FlatAppearance.BorderSize = 0;
            this.btnSafeDrop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSafeDrop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSafeDrop.ForeColor = System.Drawing.Color.White;
            this.btnSafeDrop.Location = new System.Drawing.Point(160, 43);
            this.btnSafeDrop.Margin = new System.Windows.Forms.Padding(1);
            this.btnSafeDrop.Name = "btnSafeDrop";
            this.btnSafeDrop.Size = new System.Drawing.Size(78, 38);
            this.btnSafeDrop.TabIndex = 2;
            this.btnSafeDrop.Text = "SAFE DROP";
            this.btnSafeDrop.UseVisualStyleBackColor = false;
            this.btnSafeDrop.Click += new System.EventHandler(this.btnSafeDrop_Click);
            // 
            // btnChangeStaff
            // 
            this.btnChangeStaff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnChangeStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChangeStaff.FlatAppearance.BorderSize = 0;
            this.btnChangeStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeStaff.ForeColor = System.Drawing.Color.White;
            this.btnChangeStaff.Location = new System.Drawing.Point(81, 84);
            this.btnChangeStaff.Margin = new System.Windows.Forms.Padding(1);
            this.btnChangeStaff.Name = "btnChangeStaff";
            this.btnChangeStaff.Size = new System.Drawing.Size(76, 38);
            this.btnChangeStaff.TabIndex = 5;
            this.btnChangeStaff.Text = "CHANGE STAFF";
            this.btnChangeStaff.UseVisualStyleBackColor = false;
            this.btnChangeStaff.Click += new System.EventHandler(this.btnChangeStaff_Click);
            // 
            // btnVoidHistory
            // 
            this.btnVoidHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnVoidHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVoidHistory.FlatAppearance.BorderSize = 0;
            this.btnVoidHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoidHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoidHistory.ForeColor = System.Drawing.Color.White;
            this.btnVoidHistory.Location = new System.Drawing.Point(160, 2);
            this.btnVoidHistory.Margin = new System.Windows.Forms.Padding(1);
            this.btnVoidHistory.Name = "btnVoidHistory";
            this.btnVoidHistory.Size = new System.Drawing.Size(78, 38);
            this.btnVoidHistory.TabIndex = 7;
            this.btnVoidHistory.Text = "VOID HISTORY";
            this.btnVoidHistory.UseVisualStyleBackColor = false;
            this.btnVoidHistory.Click += new System.EventHandler(this.btnVoidHistory_Click);
            // 
            // btnFuelAutoBecas
            // 
            this.btnFuelAutoBecas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnFuelAutoBecas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuelAutoBecas.FlatAppearance.BorderSize = 0;
            this.btnFuelAutoBecas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuelAutoBecas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuelAutoBecas.ForeColor = System.Drawing.Color.White;
            this.btnFuelAutoBecas.Location = new System.Drawing.Point(81, 166);
            this.btnFuelAutoBecas.Margin = new System.Windows.Forms.Padding(1);
            this.btnFuelAutoBecas.Name = "btnFuelAutoBecas";
            this.btnFuelAutoBecas.Size = new System.Drawing.Size(76, 38);
            this.btnFuelAutoBecas.TabIndex = 20;
            this.btnFuelAutoBecas.Text = "Fuel Auto Becas";
            this.btnFuelAutoBecas.UseVisualStyleBackColor = false;
            this.btnFuelAutoBecas.Click += new System.EventHandler(this.btnFuelAutoBecas_Click);
            // 
            // btnPersonalAccount
            // 
            this.btnPersonalAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnPersonalAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPersonalAccount.FlatAppearance.BorderSize = 0;
            this.btnPersonalAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPersonalAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPersonalAccount.ForeColor = System.Drawing.Color.White;
            this.btnPersonalAccount.Location = new System.Drawing.Point(2, 207);
            this.btnPersonalAccount.Margin = new System.Windows.Forms.Padding(1);
            this.btnPersonalAccount.Name = "btnPersonalAccount";
            this.btnPersonalAccount.Size = new System.Drawing.Size(76, 38);
            this.btnPersonalAccount.TabIndex = 13;
            this.btnPersonalAccount.Text = "ACCOUNT";
            this.btnPersonalAccount.UseVisualStyleBackColor = false;
            this.btnPersonalAccount.Click += new System.EventHandler(this.btnPersonalAccount_Click);
            // 
            // btnCompanyAccount
            // 
            this.btnCompanyAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnCompanyAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCompanyAccount.FlatAppearance.BorderSize = 0;
            this.btnCompanyAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompanyAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompanyAccount.ForeColor = System.Drawing.Color.White;
            this.btnCompanyAccount.Location = new System.Drawing.Point(81, 207);
            this.btnCompanyAccount.Margin = new System.Windows.Forms.Padding(1);
            this.btnCompanyAccount.Name = "btnCompanyAccount";
            this.btnCompanyAccount.Size = new System.Drawing.Size(76, 38);
            this.btnCompanyAccount.TabIndex = 14;
            this.btnCompanyAccount.Text = "COMPANY ACCOUNT";
            this.btnCompanyAccount.UseVisualStyleBackColor = false;
            this.btnCompanyAccount.Click += new System.EventHandler(this.btnCompanyAccount_Click);
            // 
            // btnShutDown
            // 
            this.btnShutDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnShutDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShutDown.FlatAppearance.BorderSize = 0;
            this.btnShutDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShutDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShutDown.ForeColor = System.Drawing.Color.White;
            this.btnShutDown.Location = new System.Drawing.Point(160, 289);
            this.btnShutDown.Margin = new System.Windows.Forms.Padding(1);
            this.btnShutDown.Name = "btnShutDown";
            this.btnShutDown.Size = new System.Drawing.Size(78, 38);
            this.btnShutDown.TabIndex = 16;
            this.btnShutDown.Text = "SHUT DOWN";
            this.btnShutDown.UseVisualStyleBackColor = false;
            this.btnShutDown.Click += new System.EventHandler(this.btnShutDown_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting.FlatAppearance.BorderSize = 0;
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetting.ForeColor = System.Drawing.Color.White;
            this.btnSetting.Location = new System.Drawing.Point(160, 166);
            this.btnSetting.Margin = new System.Windows.Forms.Padding(1);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(78, 38);
            this.btnSetting.TabIndex = 29;
            this.btnSetting.Text = "SETTING";
            this.btnSetting.UseVisualStyleBackColor = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnShiftReport
            // 
            this.btnShiftReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnShiftReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShiftReport.FlatAppearance.BorderSize = 0;
            this.btnShiftReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShiftReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShiftReport.ForeColor = System.Drawing.Color.White;
            this.btnShiftReport.Location = new System.Drawing.Point(160, 207);
            this.btnShiftReport.Margin = new System.Windows.Forms.Padding(1);
            this.btnShiftReport.Name = "btnShiftReport";
            this.btnShiftReport.Size = new System.Drawing.Size(78, 38);
            this.btnShiftReport.TabIndex = 11;
            this.btnShiftReport.Text = "SHIFT REPORT";
            this.btnShiftReport.UseVisualStyleBackColor = false;
            this.btnShiftReport.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnSetPump
            // 
            this.btnSetPump.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSetPump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetPump.FlatAppearance.BorderSize = 0;
            this.btnSetPump.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetPump.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetPump.ForeColor = System.Drawing.Color.White;
            this.btnSetPump.Location = new System.Drawing.Point(2, 166);
            this.btnSetPump.Margin = new System.Windows.Forms.Padding(1);
            this.btnSetPump.Name = "btnSetPump";
            this.btnSetPump.Size = new System.Drawing.Size(76, 38);
            this.btnSetPump.TabIndex = 21;
            this.btnSetPump.Text = "SET PUMP";
            this.btnSetPump.UseVisualStyleBackColor = false;
            this.btnSetPump.Click += new System.EventHandler(this.btnSetPump_Click);
            // 
            // btnCard
            // 
            this.btnCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCard.FlatAppearance.BorderSize = 0;
            this.btnCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCard.ForeColor = System.Drawing.Color.White;
            this.btnCard.Location = new System.Drawing.Point(2, 248);
            this.btnCard.Margin = new System.Windows.Forms.Padding(1);
            this.btnCard.Name = "btnCard";
            this.btnCard.Size = new System.Drawing.Size(76, 38);
            this.btnCard.TabIndex = 15;
            this.btnCard.Text = "CARD";
            this.btnCard.UseVisualStyleBackColor = false;
            this.btnCard.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnChangePass
            // 
            this.btnChangePass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnChangePass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChangePass.FlatAppearance.BorderSize = 0;
            this.btnChangePass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePass.ForeColor = System.Drawing.Color.White;
            this.btnChangePass.Location = new System.Drawing.Point(160, 84);
            this.btnChangePass.Margin = new System.Windows.Forms.Padding(1);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(78, 38);
            this.btnChangePass.TabIndex = 6;
            this.btnChangePass.Text = "CHANGE PASS";
            this.btnChangePass.UseVisualStyleBackColor = false;
            this.btnChangePass.Click += new System.EventHandler(this.btnShiftReport_Click);
            // 
            // btnControlPanel
            // 
            this.btnControlPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnControlPanel.FlatAppearance.BorderSize = 0;
            this.btnControlPanel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnControlPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlPanel.ForeColor = System.Drawing.Color.White;
            this.btnControlPanel.Location = new System.Drawing.Point(2, 289);
            this.btnControlPanel.Margin = new System.Windows.Forms.Padding(1);
            this.btnControlPanel.Name = "btnControlPanel";
            this.btnControlPanel.Size = new System.Drawing.Size(76, 38);
            this.btnControlPanel.TabIndex = 12;
            this.btnControlPanel.Text = "CONTROL PANEL";
            this.btnControlPanel.UseVisualStyleBackColor = false;
            this.btnControlPanel.Click += new System.EventHandler(this.btnControlPanel_Click);
            // 
            // btnOpenTill
            // 
            this.btnOpenTill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnOpenTill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenTill.FlatAppearance.BorderSize = 0;
            this.btnOpenTill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenTill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenTill.ForeColor = System.Drawing.Color.White;
            this.btnOpenTill.Location = new System.Drawing.Point(81, 248);
            this.btnOpenTill.Margin = new System.Windows.Forms.Padding(1);
            this.btnOpenTill.Name = "btnOpenTill";
            this.btnOpenTill.Size = new System.Drawing.Size(76, 38);
            this.btnOpenTill.TabIndex = 23;
            this.btnOpenTill.Text = "OPEN TILL";
            this.btnOpenTill.UseVisualStyleBackColor = false;
            this.btnOpenTill.Click += new System.EventHandler(this.btnOpenTill_Click);
            // 
            // btnStock
            // 
            this.btnStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStock.FlatAppearance.BorderSize = 0;
            this.btnStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock.ForeColor = System.Drawing.Color.White;
            this.btnStock.Location = new System.Drawing.Point(81, 289);
            this.btnStock.Margin = new System.Windows.Forms.Padding(1);
            this.btnStock.Name = "btnStock";
            this.btnStock.Size = new System.Drawing.Size(76, 38);
            this.btnStock.TabIndex = 22;
            this.btnStock.Text = "STOCK";
            this.btnStock.UseVisualStyleBackColor = false;
            this.btnStock.Click += new System.EventHandler(this.btnStock_Click);
            // 
            // btnSetTime
            // 
            this.btnSetTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSetTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetTime.FlatAppearance.BorderSize = 0;
            this.btnSetTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetTime.ForeColor = System.Drawing.Color.White;
            this.btnSetTime.Location = new System.Drawing.Point(160, 248);
            this.btnSetTime.Margin = new System.Windows.Forms.Padding(1);
            this.btnSetTime.Name = "btnSetTime";
            this.btnSetTime.Size = new System.Drawing.Size(78, 38);
            this.btnSetTime.TabIndex = 24;
            this.btnSetTime.Text = "SET TIME";
            this.btnSetTime.UseVisualStyleBackColor = false;
            this.btnSetTime.Click += new System.EventHandler(this.btnSetTime_Click);
            // 
            // btnSendMail
            // 
            this.btnSendMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSendMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSendMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMail.ForeColor = System.Drawing.Color.White;
            this.btnSendMail.Location = new System.Drawing.Point(1, 329);
            this.btnSendMail.Margin = new System.Windows.Forms.Padding(0);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(78, 40);
            this.btnSendMail.TabIndex = 30;
            this.btnSendMail.Text = "SETTING MAIL";
            this.btnSendMail.UseVisualStyleBackColor = false;
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // btnSetBarcodeItem
            // 
            this.btnSetBarcodeItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnSetBarcodeItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetBarcodeItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetBarcodeItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetBarcodeItem.ForeColor = System.Drawing.Color.White;
            this.btnSetBarcodeItem.Location = new System.Drawing.Point(80, 329);
            this.btnSetBarcodeItem.Margin = new System.Windows.Forms.Padding(0);
            this.btnSetBarcodeItem.Name = "btnSetBarcodeItem";
            this.btnSetBarcodeItem.Size = new System.Drawing.Size(78, 40);
            this.btnSetBarcodeItem.TabIndex = 31;
            this.btnSetBarcodeItem.Text = "Barcode Items";
            this.btnSetBarcodeItem.UseVisualStyleBackColor = false;
            this.btnSetBarcodeItem.Click += new System.EventHandler(this.btnSetBarcodeItem_Click);
            // 
            // btnTank
            // 
            this.btnTank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnTank.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTank.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTank.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTank.ForeColor = System.Drawing.Color.White;
            this.btnTank.Location = new System.Drawing.Point(159, 329);
            this.btnTank.Margin = new System.Windows.Forms.Padding(0);
            this.btnTank.Name = "btnTank";
            this.btnTank.Size = new System.Drawing.Size(80, 40);
            this.btnTank.TabIndex = 32;
            this.btnTank.Text = "TANK";
            this.btnTank.UseVisualStyleBackColor = false;
            this.btnTank.Click += new System.EventHandler(this.btnTank_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnBack);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 420);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(858, 53);
            this.panel4.TabIndex = 3;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Red;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(90, 51);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "BACK";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmFuntion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 509);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFuntion";
            this.ShowInTaskbar = false;
            this.Text = "FUNCTIONS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFuntion_FormClosed);
            this.Load += new System.EventHandler(this.frmFuntion_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnTitle.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel pnTitle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnStockBalance;
        private System.Windows.Forms.Button btnReportWeekly;
        private System.Windows.Forms.Button btnStockCheck;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.Button btnShiftReport;
        private System.Windows.Forms.Button btnProfitAndLoss;
        private System.Windows.Forms.Button btnStock;
        private System.Windows.Forms.Button btnShutDown;
        private System.Windows.Forms.Button btnCard;
        private System.Windows.Forms.Button btnCompanyAccount;
        private System.Windows.Forms.Button btnPersonalAccount;
        private System.Windows.Forms.Button btnControlPanel;
        private System.Windows.Forms.Button btnFuelItemChange;
        private System.Windows.Forms.Button btnMenuChange;
        private System.Windows.Forms.Button btnStaffMan;
        private System.Windows.Forms.Button btnVoidHistory;
        private System.Windows.Forms.Button btnChangeStaff;
        private System.Windows.Forms.Button btnReportDaily;
        private System.Windows.Forms.Button btnSafeDrop;
        private System.Windows.Forms.Button btnPayOut;
        private System.Windows.Forms.Button btnCashIn;
        private System.Windows.Forms.Button btnOpenTill;
        private System.Windows.Forms.Button btnFuelAutoBecas;
        private System.Windows.Forms.Button btnSetPump;
        private System.Windows.Forms.Button btnComboDisc;
        private System.Windows.Forms.Button btnSetTime;
        private System.Windows.Forms.Button btnSetting;
        private UCInfoTop ucInfoTop1;
        private System.Windows.Forms.Button btnSendMail;
        private System.Windows.Forms.Button btnSetBarcodeItem;
        private System.Windows.Forms.Button btnTank;

    }
}