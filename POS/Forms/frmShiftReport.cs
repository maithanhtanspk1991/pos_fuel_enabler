﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmShiftReport : Form
    {
        public static int intNoneOfShift;
        public static bool IsActive;
        public static bool IsHasEndShift;
        public double CashTotal;
        public DataObject.ShiftObject so;
        private bool IsLoadData = false;
        private Connection.Connection mConnection = new Connection.Connection();
        private frmOrdersAll mfrmOrdersAll;
        private bool mIsLoad = false;
        private Class.MoneyFortmat mMoney;
        private Class.ReadConfig mReadConfig;
        private Connection.ReadDBConfig mReadDBConfig = new Connection.ReadDBConfig();
        private DataObject.TypeFromShift mTypeFromShift;

        public frmShiftReport(Class.MoneyFortmat mmoney, frmOrdersAll frmall, DataObject.TypeFromShift type)
        {
            InitializeComponent();
            SetMultiLanguage();
            mMoney = mmoney;
            mfrmOrdersAll = frmall;
            mTypeFromShift = type;
            mReadConfig = new Class.ReadConfig();
            mIsLoad = false;
        }

        public frmShiftReport(Class.MoneyFortmat mmoney, frmOrdersAll frmall, DataObject.TypeFromShift type, bool IsLoad)
        {
            InitializeComponent();
            mMoney = mmoney;
            mfrmOrdersAll = frmall;
            mTypeFromShift = type;
            mReadConfig = new Class.ReadConfig();
            mIsLoad = IsLoad;
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Báo cáo Ca làm việc";
                label11.Text = "Ngày xem:";
                label2.Text = "Ca LV:";
                label2.Font = new Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label3.Text = "Mã:";
                btnEndshift.Text = "KẾT THÚC CA LV";
                btnExit.Text = "THOÁT";
                btnSafeDrop.Text = "HOÀN THÀNH";
                return;
            }
        }

        private void btnEndshift_Click(object sender, EventArgs e)
        {
            frmMessageBox frm = new frmMessageBox("Waring", "Close Current Shift");
            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadEndShiftReportShift();
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            so = mfrmOrdersAll.mShiftObject;
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnFresh_Click(object sender, EventArgs e)
        {
            if (IsLoadData)
                LoadReportShift();
        }

        private void btnSafeDrop_Click(object sender, EventArgs e)
        {
            pnShift.Controls.Clear();
            if (mfrmOrdersAll.mShiftObject.bActiveShift == false && mfrmOrdersAll.mShiftObject.continueShift == false && mTypeFromShift == DataObject.TypeFromShift.Staff)
            {
                return;
            }
            try
            {
                DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                shiftrp.datetime = mfrmOrdersAll.mShiftObject.datetime;
                shiftrp.ShiftID = mfrmOrdersAll.mShiftObject.ShiftID;
                shiftrp.ShiftName = mfrmOrdersAll.mShiftObject.ShiftName;

                UCReportShift ucrpshift = new UCReportShift(mMoney);
                DataObject.ShiftReport shiftrpd = BusinessObject.BOShiftReport.GetReportShift(shiftrp).CopyDateAndShiftName(shiftrp);
                FormFinal frm = new FormFinal(shiftrpd, mfrmOrdersAll.mTransit.StaffID, mfrmOrdersAll.Money);
                frm.ShowDialog();
                LoadReportShift();
            }
            catch { }
        }

        private void cbbCableID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoadData)
            {
                LoadShift();
                LoadReportShift();
            }
        }

        private void cbbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoadData)
            {
                LoadReportShift();
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsLoadData)
            {
                LoadShift();
                LoadReportShift();
            }
        }

        private void ExecuteFuntionCash(int Type, int TypeCash, int Shift, bool blnIsHaveDescription, string strTitleForm)
        {
            Connection.Connection con = new Connection.Connection();
            double total = 0;
            if (mfrmOrdersAll.mShiftObject.bActiveShift == false && mfrmOrdersAll.mShiftObject.continueShift == false) //shift chua hoat dong
            {
                total += BusinessObject.BOConfig.GetCurrentCashFloatIn(new SystemConfig.DBConfig());
                BusinessObject.BOShift.InsertShift(ref mfrmOrdersAll.mShiftObject);
                BusinessObject.BOConfig.SetCurrentShift(mfrmOrdersAll.mShiftObject.ShiftID);
                mfrmOrdersAll.mShiftObject.bActiveShift = true;
            }
            else
            {
                DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                shiftrp.datetime = mfrmOrdersAll.mShiftObject.datetime;
                shiftrp.ShiftID = mfrmOrdersAll.mShiftObject.ShiftID;
                shiftrp = BusinessObject.BOShiftReport.GetReportShift(shiftrp);
                total += shiftrp.CashFloatIn + shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.PayOut;
            }
            frmCashInOrOut frm = new frmCashInOrOut(Type, TypeCash,
               Convert.ToDouble(mMoney.Format(total))
                , mMoney, mfrmOrdersAll.mShiftObject.ShiftID, mfrmOrdersAll.mShiftObject);
            frm.Text = strTitleForm;
            if (blnIsHaveDescription)
            {
                frm.Width = 420;
                frm.txtDesciption.Text = strTitleForm;
                frm.btnCashDrop.Visible = false;
                frm.btnPay.Visible = false;
            }
            DialogResult dlg = frm.ShowDialog();
            if (dlg == DialogResult.OK)
            {
                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
            }
        }

        private void frmShiftReport_Activated(object sender, EventArgs e)
        {
        }

        private void frmShiftReport_Load(object sender, EventArgs e)
        {
            if (mIsLoad == true || mfrmOrdersAll.mShiftObject.continueShift == true)
            {
                LoadExit();
                try
                {
                    SystemLog.LogPOS.WriteLog("frmShiftReport::" + mfrmOrdersAll.mShiftObject.ShiftID);
                    dtpDate.Value = BusinessObject.BOShift.GetShiftMaxWithShiftID(mfrmOrdersAll.mShiftObject).datetime;
                }
                catch
                {
                    dtpDate.Value = DateTime.Now;
                }
            }

            LoadCableID();
            LoadShift();
            LoadReportShift();
            IsLoadData = true;
            mfrmOrdersAll.EnableCheckShift = false;

            if (mTypeFromShift == DataObject.TypeFromShift.Manager)
            {
                btnSafeDrop.Visible = false;
                btnEndshift.Visible = false;
            }
        }

        private void LoadCableID()
        {
            if (mTypeFromShift == DataObject.TypeFromShift.Manager)
            {
                List<string> lsArray = BusinessObject.BOShift.GetCableID();
                cbbCableID.DataSource = lsArray;
                if (lsArray.Count > 0)
                    cbbCableID.SelectedIndex = 0;
            }
            else
            {
                cbbCableID.Items.Add(mReadDBConfig.CableID);
                cbbCableID.SelectedIndex = 0;
            }
        }

        private void LoadEndShiftReportShift()
        {
            pnShift.Controls.Clear();
            if (mfrmOrdersAll.mShiftObject.bActiveShift == false && mfrmOrdersAll.mShiftObject.continueShift == false && mTypeFromShift == DataObject.TypeFromShift.Staff)
            {
                return;
            }
            try
            {
                DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                shiftrp.datetime = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, dtpDate.Value.Day, mReadConfig.iHourEndShift, mReadConfig.iMinuteEndShift, mReadConfig.iSecondEndShift);
                shiftrp.ShiftID = mfrmOrdersAll.mShiftObject.ShiftID;
                shiftrp.ShiftName = mfrmOrdersAll.mShiftObject.ShiftName;

                UCReportShift ucrpshift = new UCReportShift(mMoney);

                DataObject.ShiftReport shiftrpd = BusinessObject.BOShiftReport.GetReportShift(shiftrp).CopyDateAndShiftName(shiftrp);
                shiftrpd.CableID = cbbCableID.SelectedItem.ToString().Length > 3 ? 0 : Convert.ToInt32(cbbCableID.SelectedItem);
                ucrpshift.SetValue(shiftrpd, BusinessObject.BOShiftReport.GetListCardReportShift(shiftrp));                
                pnShift.Controls.Add(ucrpshift);
                ucrpshift.Print();
            }
            catch { }
        }

        private void LoadExit()
        {
            if (mReadConfig.IsEnableExitShift == false)
            {
                btnExit.Visible = false;
            }
        }

        private void LoadReportShift()
        {
            pnShift.Controls.Clear();
            if (mfrmOrdersAll.mShiftObject.bActiveShift == false && mfrmOrdersAll.mShiftObject.continueShift == false && mTypeFromShift == DataObject.TypeFromShift.Staff && this.cbbShift.SelectedValue == null)
            {
                return;
            }
            try
            {
                DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                shiftrp.datetime = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, dtpDate.Value.Day, mReadConfig.iHourEndShift, mReadConfig.iMinuteEndShift, mReadConfig.iSecondEndShift);
                if (cbbShift.SelectedValue != "" && cbbShift.SelectedValue != null)
                {
                    try
                    {
                        shiftrp.ShiftID = int.Parse(cbbShift.SelectedValue.ToString());

                        shiftrp.ShiftName = int.Parse(cbbShift.Text);
                    }
                    catch
                    {
                    }
                    UCReportShift ucrpshift = new UCReportShift(mMoney);
                    int Cable = cbbCableID.SelectedItem.ToString().Length > 3 ? 0 : Convert.ToInt32(cbbCableID.SelectedItem);
                    if (cbbShift.SelectedValue.ToString() != "0")
                    {
                        DataObject.ShiftReport shiftrpd = BusinessObject.BOShiftReport.GetReportShift(shiftrp).CopyDateAndShiftName(shiftrp);
                        shiftrpd.CableID = Cable;
                        ucrpshift.SetValue(shiftrpd, BusinessObject.BOShiftReport.GetListCardReportShift(shiftrp));
                    }
                    else
                    {
                        
                        DataObject.ShiftObject so = BusinessObject.BOShift.GetListShiftWithDate(shiftrp, Cable).OrderBy(s => s.ShiftID).ElementAt(0);                        
                        DataObject.ShiftReport shiftrpd = BusinessObject.BOShiftReport.GetReportShiftAll(shiftrp, Cable).CopyDateAndShiftName(shiftrp);                        
                        shiftrpd.CashFloatIn = so.CashFloatIn;
                        shiftrpd.strShiftName = cbbShift.Text;
                        shiftrpd.CableID = Cable;
                        ucrpshift.SetValue(shiftrpd, BusinessObject.BOShiftReport.GetListCardReportShiftAll(shiftrp));
                    }
                    
                    pnShift.Controls.Add(ucrpshift);
                }
            }
            catch { }
        }

        private void LoadShift()
        {
            DataObject.ShiftObject shiftobject = new DataObject.ShiftObject();
            shiftobject.datetime = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, dtpDate.Value.Day, mReadConfig.iHourEndShift, mReadConfig.iMinuteEndShift, mReadConfig.iSecondEndShift);
            int Cable = cbbCableID.SelectedItem.ToString().Length > 3 ? 0 : Convert.ToInt32(cbbCableID.SelectedItem);
            List<DataObject.ShiftObject> lst = BusinessObject.BOShift.GetListShiftWithDate(shiftobject, Cable);
            if (mTypeFromShift == DataObject.TypeFromShift.Manager && lst.Count > 0)
            {
                DataObject.ShiftObject shift = new DataObject.ShiftObject();
                shift.strShiftName = "[All]";
                shift.ShiftName = 0;
                shift.ShiftID = 0;
                lst.Insert(0, shift);
            }

            cbbShift.DataSource = lst;
            cbbShift.DisplayMember = "strShiftName";
            cbbShift.ValueMember = "ShiftID";
            if (lst.Count > 0)
            {
                if (mTypeFromShift == DataObject.TypeFromShift.Staff && (dtpDate.Value.ToShortDateString() == DateTime.Now.ToShortDateString() || mIsLoad == true || mfrmOrdersAll.mShiftObject.continueShift == true))
                {
                    cbbShift.SelectedValue = mfrmOrdersAll.mShiftObject.ShiftID;
                }
                else
                {
                    cbbShift.SelectedIndex = 0;
                }
            }
        }
    }
}