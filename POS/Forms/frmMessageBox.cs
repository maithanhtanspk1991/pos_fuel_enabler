﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMessageBox : Form
    {
        private string mTitle;
        private string mMessage;

        public frmMessageBox()
        {
            InitializeComponent();
        }

        public void SetFont()
        {
            
        }


        public frmMessageBox(string title, string message)
        {
            InitializeComponent();
            mTitle = title;
            mMessage = message;
            SetFont();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if(mReadConfig.LanguageCode.Equals("vi"))
            {
                btnCancel.Text = "Thoát";
            }
            return;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmMessageBox_Load(object sender, EventArgs e)
        {
            gbTitle.Text = mTitle;
            lbMessage.Text = mMessage;
        }
    }
}