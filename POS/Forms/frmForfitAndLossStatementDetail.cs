﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmForfitAndLossStatementDetail : Form
    {
        public int ItemID { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private ExportToExcel mExportToExcel;
        private DataTable dtSourceForExcel;
        private string sql;

        public frmForfitAndLossStatementDetail()
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            money = new Class.MoneyFortmat(1);
            mExportToExcel = new ExportToExcel();
        }

        public void LoadDetailProfitAndLoss()
        {
            try
            {
                mConnection.Open();
                sql = "CALL SP_PROFIT_AND_LOSS_STATEMENT('" + FromDate + "','" + ToDate + "','" + ItemID + "')";
                DataTable dtSource = mConnection.Select(sql);

                dtSourceForExcel = new DataTable();
                dtSourceForExcel.Columns.Add("Type Stock", typeof(String));
                dtSourceForExcel.Columns.Add("Item Name", typeof(String));
                dtSourceForExcel.Columns.Add("Datetime", typeof(String));
                dtSourceForExcel.Columns.Add("Total Pcs", typeof(String));
                dtSourceForExcel.Columns.Add("Pcs Cost Price", typeof(String));
                dtSourceForExcel.Columns.Add("Pcs Sell Price", typeof(String));
                dtSourceForExcel.Columns.Add("Profit And Loss", typeof(String));

                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(drItem["TypeStock"].ToString());
                    li.SubItems.Add(drItem["ItemName"].ToString());
                    li.SubItems.Add(drItem["ts"].ToString());
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["TotalPcs"].ToString())));
                    li.SubItems.Add("$ " + money.Format(drItem["PcsCostPrice"].ToString()));
                    li.SubItems.Add("$ " + money.Format(drItem["PcsSellPrice"].ToString()));
                    li.SubItems.Add("$ " + money.Format(drItem["ProfitAndLoss"].ToString()));
                    lvwProfitAndLossListDetail.Items.Add(li);

                    dtSourceForExcel.Rows.Add(
                        drItem["TypeStock"].ToString(),
                        drItem["ItemName"].ToString(),
                        drItem["ts"].ToString(),
                        money.getFortMat3(Convert.ToDouble(drItem["TotalPcs"].ToString())),
                        "$ " + money.Format(drItem["PcsCostPrice"].ToString()),
                        "$ " + money.Format(drItem["PcsSellPrice"].ToString()),
                        "$ " + money.Format(drItem["ProfitAndLoss"].ToString())
                        );
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmProfitAndLossStatement:::LoadWeeklyButton:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            txtNameExport.Text = "";
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtNameExport);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowSaveFileDialog(txtNameExport.Text, "Microsoft Excel Document", "Excel 2003|*.xls");
            }
        }

        public void ShowSaveFileDialog(string filename, string title, string filter)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Export To " + title;
                dlg.FileName = filename;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    mExportToExcel.dataTable2Excel(dtSourceForExcel, dlg.FileName);
                    MessageBox.Show("Export to excel success !");
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.ShowSaveFileDialog:::" + ex.Message);
            }
        }
    }
}