﻿namespace POS.Forms
{
    partial class frmOrdersAllQuickSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdersAllQuickSales));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.axCsdEft = new POS.CardInterface.POSAxCsdEft();
            this.txtCusName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbChangeName = new System.Windows.Forms.Label();
            this.lbChangeAmount = new System.Windows.Forms.Label();
            this.flp_takeaway = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtunitprice = new POS.TextBoxNumbericPOS();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtstaffid = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.txttableid = new System.Windows.Forms.TextBox();
            this.txtorderid = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_orderid = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnitemdown = new System.Windows.Forms.Button();
            this.btnitemup = new System.Windows.Forms.Button();
            this.btngroupdown = new System.Windows.Forms.Button();
            this.btngroupup = new System.Windows.Forms.Button();
            this.tlp_items = new System.Windows.Forms.TableLayoutPanel();
            this.tlp_groups = new System.Windows.Forms.TableLayoutPanel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblStatus = new POS.POSLabelStatus();
            this.button9 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uCkeypad1 = new POS.UCkeypad();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSendOrder = new System.Windows.Forms.Button();
            this.btnListDelivery = new System.Windows.Forms.Button();
            this.buttonRefun = new System.Windows.Forms.Button();
            this.btnBill = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.btnFindAcc = new System.Windows.Forms.Button();
            this.btnNextCust = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.btnMergeOrder = new System.Windows.Forms.Button();
            this.btnoption = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.btnSubtotal = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDepartment1 = new System.Windows.Forms.Button();
            this.btnDepartment2 = new System.Windows.Forms.Button();
            this.btnDepartment3 = new System.Windows.Forms.Button();
            this.btnDepartment4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axCsdEft)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.axCsdEft);
            this.splitContainer1.Panel1.Controls.Add(this.txtCusName);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.lbChangeName);
            this.splitContainer1.Panel1.Controls.Add(this.lbChangeAmount);
            this.splitContainer1.Panel1.Controls.Add(this.flp_takeaway);
            this.splitContainer1.Panel1.Controls.Add(this.lblTotal);
            this.splitContainer1.Panel1.Controls.Add(this.txtunitprice);
            this.splitContainer1.Panel1.Controls.Add(this.txtBarcode);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.txtstaffid);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.textBox1);
            this.splitContainer1.Panel1.Controls.Add(this.txtPort);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.txtqty);
            this.splitContainer1.Panel1.Controls.Add(this.txttableid);
            this.splitContainer1.Panel1.Controls.Add(this.txtorderid);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.lb_orderid);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.txtitemname);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.listView1);
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(1036, 720);
            this.splitContainer1.SplitterDistance = 156;
            this.splitContainer1.TabIndex = 0;
            // 
            // axCsdEft
            // 
            this.axCsdEft.Enabled = true;
            this.axCsdEft.Location = new System.Drawing.Point(1005, 32);
            this.axCsdEft.Name = "axCsdEft";
            this.axCsdEft.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCsdEft.OcxState")));
            this.axCsdEft.Size = new System.Drawing.Size(26, 25);
            this.axCsdEft.TabIndex = 36;
            // 
            // txtCusName
            // 
            this.txtCusName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtCusName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusName.Location = new System.Drawing.Point(897, 66);
            this.txtCusName.Name = "txtCusName";
            this.txtCusName.Size = new System.Drawing.Size(134, 26);
            this.txtCusName.TabIndex = 35;
            this.txtCusName.TextChanged += new System.EventHandler(this.txtCusName_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(803, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 16);
            this.label9.TabIndex = 34;
            this.label9.Text = "Cust Name : ";
            // 
            // lbChangeName
            // 
            this.lbChangeName.BackColor = System.Drawing.SystemColors.Control;
            this.lbChangeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangeName.ForeColor = System.Drawing.Color.Black;
            this.lbChangeName.Location = new System.Drawing.Point(818, 27);
            this.lbChangeName.Name = "lbChangeName";
            this.lbChangeName.Size = new System.Drawing.Size(190, 33);
            this.lbChangeName.TabIndex = 33;
            this.lbChangeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbChangeAmount
            // 
            this.lbChangeAmount.BackColor = System.Drawing.SystemColors.Control;
            this.lbChangeAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangeAmount.ForeColor = System.Drawing.Color.Red;
            this.lbChangeAmount.Location = new System.Drawing.Point(818, 62);
            this.lbChangeAmount.Name = "lbChangeAmount";
            this.lbChangeAmount.Size = new System.Drawing.Size(190, 33);
            this.lbChangeAmount.TabIndex = 33;
            this.lbChangeAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flp_takeaway
            // 
            this.flp_takeaway.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_takeaway.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.flp_takeaway.ColumnCount = 16;
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.25F));
            this.flp_takeaway.Location = new System.Drawing.Point(2, 98);
            this.flp_takeaway.Name = "flp_takeaway";
            this.flp_takeaway.RowCount = 1;
            this.flp_takeaway.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.flp_takeaway.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.flp_takeaway.Size = new System.Drawing.Size(1030, 53);
            this.flp_takeaway.TabIndex = 32;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Red;
            this.lblTotal.Location = new System.Drawing.Point(367, 31);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(169, 33);
            this.lblTotal.TabIndex = 30;
            this.lblTotal.Text = "Total";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtunitprice
            // 
            this.txtunitprice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtunitprice.Enabled = false;
            this.txtunitprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunitprice.Location = new System.Drawing.Point(242, 65);
            this.txtunitprice.Name = "txtunitprice";
            this.txtunitprice.Size = new System.Drawing.Size(122, 29);
            this.txtunitprice.TabIndex = 31;
            this.txtunitprice.ucKeypad = null;
            this.txtunitprice.TextChanged += new System.EventHandler(this.txtunitprice_TextChanged);
            // 
            // txtBarcode
            // 
            this.txtBarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtBarcode.Enabled = false;
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(638, 65);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.ReadOnly = true;
            this.txtBarcode.Size = new System.Drawing.Size(165, 29);
            this.txtBarcode.TabIndex = 29;
            this.txtBarcode.Click += new System.EventHandler(this.txtBarcode_Click);
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(551, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "Bar Code :";
            // 
            // txtstaffid
            // 
            this.txtstaffid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtstaffid.Enabled = false;
            this.txtstaffid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstaffid.Location = new System.Drawing.Point(638, 31);
            this.txtstaffid.Name = "txtstaffid";
            this.txtstaffid.ReadOnly = true;
            this.txtstaffid.Size = new System.Drawing.Size(165, 29);
            this.txtstaffid.TabIndex = 27;
            this.txtstaffid.Enter += new System.EventHandler(this.txtqty_Enter);
            this.txtstaffid.Leave += new System.EventHandler(this.txtqty_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(569, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 26;
            this.label7.Text = "Staff :";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Location = new System.Drawing.Point(1, 1);
            this.panel4.Margin = new System.Windows.Forms.Padding(1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1030, 25);
            this.panel4.TabIndex = 24;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(890, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(88, 26);
            this.textBox1.TabIndex = 23;
            this.textBox1.Visible = false;
            this.textBox1.Enter += new System.EventHandler(this.txtqty_Enter);
            this.textBox1.Leave += new System.EventHandler(this.txtqty_Leave);
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(890, 65);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(88, 29);
            this.txtPort.TabIndex = 22;
            this.txtPort.Visible = false;
            this.txtPort.Enter += new System.EventHandler(this.txtqty_Enter);
            this.txtPort.Leave += new System.EventHandler(this.txtqty_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(833, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "People:";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(833, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Pager:";
            this.label6.Visible = false;
            // 
            // txtqty
            // 
            this.txtqty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(89, 65);
            this.txtqty.MaxLength = 9;
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(76, 29);
            this.txtqty.TabIndex = 19;
            this.txtqty.Click += new System.EventHandler(this.txtqty_Enter);
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.Enter += new System.EventHandler(this.txtqty_Enter);
            this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
            this.txtqty.Leave += new System.EventHandler(this.txtqty_Leave);
            // 
            // txttableid
            // 
            this.txttableid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txttableid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttableid.Location = new System.Drawing.Point(434, 38);
            this.txttableid.Name = "txttableid";
            this.txttableid.Size = new System.Drawing.Size(86, 26);
            this.txttableid.TabIndex = 17;
            this.txttableid.Visible = false;
            this.txttableid.TextChanged += new System.EventHandler(this.txttableid_TextChanged);
            // 
            // txtorderid
            // 
            this.txtorderid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtorderid.Enabled = false;
            this.txtorderid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtorderid.Location = new System.Drawing.Point(450, 66);
            this.txtorderid.Name = "txtorderid";
            this.txtorderid.ReadOnly = true;
            this.txtorderid.Size = new System.Drawing.Size(86, 29);
            this.txtorderid.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(168, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Unit Price :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Qty :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(366, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Table ID :";
            this.label2.Visible = false;
            // 
            // lb_orderid
            // 
            this.lb_orderid.AutoSize = true;
            this.lb_orderid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_orderid.Location = new System.Drawing.Point(372, 72);
            this.lb_orderid.Name = "lb_orderid";
            this.lb_orderid.Size = new System.Drawing.Size(74, 16);
            this.lb_orderid.TabIndex = 2;
            this.lb_orderid.Text = "Order ID :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Item name :";
            // 
            // txtitemname
            // 
            this.txtitemname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtitemname.Enabled = false;
            this.txtitemname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(89, 32);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.ReadOnly = true;
            this.txtitemname.Size = new System.Drawing.Size(275, 29);
            this.txtitemname.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnitemdown);
            this.panel2.Controls.Add(this.btnitemup);
            this.panel2.Controls.Add(this.btngroupdown);
            this.panel2.Controls.Add(this.btngroupup);
            this.panel2.Controls.Add(this.tlp_items);
            this.panel2.Controls.Add(this.tlp_groups);
            this.panel2.Location = new System.Drawing.Point(1, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(413, 503);
            this.panel2.TabIndex = 6;
            // 
            // btnitemdown
            // 
            this.btnitemdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnitemdown.Image = global::POS.Properties.Resources.down;
            this.btnitemdown.Location = new System.Drawing.Point(314, 449);
            this.btnitemdown.Name = "btnitemdown";
            this.btnitemdown.Size = new System.Drawing.Size(95, 50);
            this.btnitemdown.TabIndex = 10;
            this.btnitemdown.UseVisualStyleBackColor = true;
            this.btnitemdown.Click += new System.EventHandler(this.btnitemdown_Click);
            // 
            // btnitemup
            // 
            this.btnitemup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnitemup.Image = global::POS.Properties.Resources.up1;
            this.btnitemup.Location = new System.Drawing.Point(219, 449);
            this.btnitemup.Name = "btnitemup";
            this.btnitemup.Size = new System.Drawing.Size(95, 50);
            this.btnitemup.TabIndex = 9;
            this.btnitemup.UseVisualStyleBackColor = true;
            this.btnitemup.Click += new System.EventHandler(this.btnitemup_Click);
            // 
            // btngroupdown
            // 
            this.btngroupdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btngroupdown.Image = global::POS.Properties.Resources.gui_dow;
            this.btngroupdown.Location = new System.Drawing.Point(61, 449);
            this.btngroupdown.Name = "btngroupdown";
            this.btngroupdown.Size = new System.Drawing.Size(62, 50);
            this.btngroupdown.TabIndex = 8;
            this.btngroupdown.UseVisualStyleBackColor = true;
            this.btngroupdown.Click += new System.EventHandler(this.btngroupdown_Click);
            // 
            // btngroupup
            // 
            this.btngroupup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btngroupup.Image = global::POS.Properties.Resources.gui_up;
            this.btngroupup.Location = new System.Drawing.Point(2, 449);
            this.btngroupup.Name = "btngroupup";
            this.btngroupup.Size = new System.Drawing.Size(58, 50);
            this.btngroupup.TabIndex = 7;
            this.btngroupup.UseVisualStyleBackColor = true;
            this.btngroupup.Click += new System.EventHandler(this.btngroupup_Click);
            // 
            // tlp_items
            // 
            this.tlp_items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlp_items.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tlp_items.ColumnCount = 3;
            this.tlp_items.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_items.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_items.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp_items.Location = new System.Drawing.Point(125, 1);
            this.tlp_items.Name = "tlp_items";
            this.tlp_items.RowCount = 7;
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_items.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_items.Size = new System.Drawing.Size(285, 446);
            this.tlp_items.TabIndex = 3;
            // 
            // tlp_groups
            // 
            this.tlp_groups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tlp_groups.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tlp_groups.ColumnCount = 1;
            this.tlp_groups.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_groups.Location = new System.Drawing.Point(1, 1);
            this.tlp_groups.Name = "tlp_groups";
            this.tlp_groups.RowCount = 7;
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_groups.Size = new System.Drawing.Size(121, 445);
            this.tlp_groups.TabIndex = 1;
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(416, 3);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(325, 501);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Qty";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 171;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "SubTotal";
            this.columnHeader3.Width = 132;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "PumpID";
            this.columnHeader4.Width = 78;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ID";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lblStatus);
            this.panel5.Controls.Add(this.button9);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 505);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1034, 53);
            this.panel5.TabIndex = 5;
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(83, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.POSTimeMessenge = 1;
            this.lblStatus.Size = new System.Drawing.Size(944, 49);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(0, 0);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 50);
            this.button9.TabIndex = 0;
            this.button9.Text = "EXIT";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.uCkeypad1);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Location = new System.Drawing.Point(742, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(289, 519);
            this.panel3.TabIndex = 4;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(1, 1);
            this.uCkeypad1.Margin = new System.Windows.Forms.Padding(4);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 55;
            this.uCkeypad1.txtResult = null;
            this.uCkeypad1.Load += new System.EventHandler(this.uCkeypad1_Load);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btnSendOrder, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnListDelivery, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRefun, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnBill, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.button12, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnFindAcc, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnNextCust, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.button37, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnMergeOrder, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnoption, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button5, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.button29, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.button8, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.button7, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.button36, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnSubtotal, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.button6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.button4, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.button1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.button27, 2, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 271);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(286, 232);
            this.tableLayoutPanel2.TabIndex = 54;
            // 
            // btnSendOrder
            // 
            this.btnSendOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSendOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSendOrder.Enabled = false;
            this.btnSendOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendOrder.ForeColor = System.Drawing.Color.Black;
            this.btnSendOrder.Location = new System.Drawing.Point(0, 0);
            this.btnSendOrder.Margin = new System.Windows.Forms.Padding(0);
            this.btnSendOrder.Name = "btnSendOrder";
            this.btnSendOrder.Size = new System.Drawing.Size(71, 46);
            this.btnSendOrder.TabIndex = 23;
            this.btnSendOrder.Text = "SEND ORDER";
            this.btnSendOrder.UseVisualStyleBackColor = false;
            this.btnSendOrder.Click += new System.EventHandler(this.btnDepartment8_Click);
            // 
            // btnListDelivery
            // 
            this.btnListDelivery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnListDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnListDelivery.Enabled = false;
            this.btnListDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.btnListDelivery.ForeColor = System.Drawing.Color.Black;
            this.btnListDelivery.Location = new System.Drawing.Point(71, 0);
            this.btnListDelivery.Margin = new System.Windows.Forms.Padding(0);
            this.btnListDelivery.Name = "btnListDelivery";
            this.btnListDelivery.Size = new System.Drawing.Size(71, 46);
            this.btnListDelivery.TabIndex = 40;
            this.btnListDelivery.Text = "PHONE ORDERS";
            this.btnListDelivery.UseVisualStyleBackColor = false;
            this.btnListDelivery.Click += new System.EventHandler(this.btnDepartment7_Click);
            // 
            // buttonRefun
            // 
            this.buttonRefun.BackColor = System.Drawing.Color.Gray;
            this.buttonRefun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefun.Location = new System.Drawing.Point(213, 184);
            this.buttonRefun.Margin = new System.Windows.Forms.Padding(0);
            this.buttonRefun.Name = "buttonRefun";
            this.buttonRefun.Size = new System.Drawing.Size(73, 48);
            this.buttonRefun.TabIndex = 52;
            this.buttonRefun.Text = "REFUND";
            this.buttonRefun.UseVisualStyleBackColor = false;
            this.buttonRefun.Click += new System.EventHandler(this.buttonRefun_Click);
            // 
            // btnBill
            // 
            this.btnBill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBill.Enabled = false;
            this.btnBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill.ForeColor = System.Drawing.Color.Black;
            this.btnBill.Location = new System.Drawing.Point(142, 0);
            this.btnBill.Margin = new System.Windows.Forms.Padding(0);
            this.btnBill.Name = "btnBill";
            this.btnBill.Size = new System.Drawing.Size(71, 46);
            this.btnBill.TabIndex = 39;
            this.btnBill.Text = "BILL";
            this.btnBill.UseVisualStyleBackColor = false;
            this.btnBill.Click += new System.EventHandler(this.btnDepartment6_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Gray;
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(142, 184);
            this.button12.Margin = new System.Windows.Forms.Padding(0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(71, 48);
            this.button12.TabIndex = 51;
            this.button12.Text = "NEW ITEM";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnFindAcc
            // 
            this.btnFindAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnFindAcc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFindAcc.Enabled = false;
            this.btnFindAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindAcc.ForeColor = System.Drawing.Color.Black;
            this.btnFindAcc.Location = new System.Drawing.Point(213, 0);
            this.btnFindAcc.Margin = new System.Windows.Forms.Padding(0);
            this.btnFindAcc.Name = "btnFindAcc";
            this.btnFindAcc.Size = new System.Drawing.Size(73, 46);
            this.btnFindAcc.TabIndex = 33;
            this.btnFindAcc.Text = "FIND ACC";
            this.btnFindAcc.UseVisualStyleBackColor = false;
            this.btnFindAcc.Click += new System.EventHandler(this.btnDepartment5_Click);
            // 
            // btnNextCust
            // 
            this.btnNextCust.BackColor = System.Drawing.Color.Gray;
            this.btnNextCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextCust.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextCust.Location = new System.Drawing.Point(71, 184);
            this.btnNextCust.Margin = new System.Windows.Forms.Padding(0);
            this.btnNextCust.Name = "btnNextCust";
            this.btnNextCust.Size = new System.Drawing.Size(71, 48);
            this.btnNextCust.TabIndex = 50;
            this.btnNextCust.Text = "NEXT CUST";
            this.btnNextCust.UseVisualStyleBackColor = false;
            this.btnNextCust.Click += new System.EventHandler(this.button11_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Location = new System.Drawing.Point(0, 46);
            this.button37.Margin = new System.Windows.Forms.Padding(0);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(71, 46);
            this.button37.TabIndex = 35;
            this.button37.Text = "FIND ITEM";
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // btnMergeOrder
            // 
            this.btnMergeOrder.BackColor = System.Drawing.Color.Gray;
            this.btnMergeOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMergeOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMergeOrder.Location = new System.Drawing.Point(0, 184);
            this.btnMergeOrder.Margin = new System.Windows.Forms.Padding(0);
            this.btnMergeOrder.Name = "btnMergeOrder";
            this.btnMergeOrder.Size = new System.Drawing.Size(71, 48);
            this.btnMergeOrder.TabIndex = 49;
            this.btnMergeOrder.Text = "FUEL DISC";
            this.btnMergeOrder.UseVisualStyleBackColor = false;
            this.btnMergeOrder.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnoption
            // 
            this.btnoption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnoption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnoption.Enabled = false;
            this.btnoption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnoption.ForeColor = System.Drawing.Color.Black;
            this.btnoption.Location = new System.Drawing.Point(71, 46);
            this.btnoption.Margin = new System.Windows.Forms.Padding(0);
            this.btnoption.Name = "btnoption";
            this.btnoption.Size = new System.Drawing.Size(71, 46);
            this.btnoption.TabIndex = 24;
            this.btnoption.Text = "OPT.";
            this.btnoption.UseVisualStyleBackColor = false;
            this.btnoption.Click += new System.EventHandler(this.button26_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(213, 138);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(73, 46);
            this.button5.TabIndex = 42;
            this.button5.Text = "PRINT";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.ForeColor = System.Drawing.Color.Black;
            this.button29.Location = new System.Drawing.Point(142, 138);
            this.button29.Margin = new System.Windows.Forms.Padding(0);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(71, 46);
            this.button29.TabIndex = 27;
            this.button29.Text = "PREV ORDER";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(71, 138);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(71, 46);
            this.button8.TabIndex = 46;
            this.button8.Text = "RECALL ORDER";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(0, 138);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(71, 46);
            this.button7.TabIndex = 47;
            this.button7.Text = "SAVE ORDER";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Location = new System.Drawing.Point(142, 46);
            this.button36.Margin = new System.Windows.Forms.Padding(0);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(71, 46);
            this.button36.TabIndex = 34;
            this.button36.Text = "MENU ITEM";
            this.button36.UseVisualStyleBackColor = false;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // btnSubtotal
            // 
            this.btnSubtotal.BackColor = System.Drawing.Color.Salmon;
            this.btnSubtotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtotal.ForeColor = System.Drawing.Color.Black;
            this.btnSubtotal.Location = new System.Drawing.Point(213, 46);
            this.btnSubtotal.Margin = new System.Windows.Forms.Padding(0);
            this.btnSubtotal.Name = "btnSubtotal";
            this.btnSubtotal.Size = new System.Drawing.Size(73, 46);
            this.btnSubtotal.TabIndex = 26;
            this.btnSubtotal.Text = "SUB TOTAL";
            this.btnSubtotal.UseVisualStyleBackColor = false;
            this.btnSubtotal.Click += new System.EventHandler(this.button28_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Location = new System.Drawing.Point(0, 92);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 46);
            this.button6.TabIndex = 41;
            this.button6.Text = "FUNCS";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Salmon;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(213, 92);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(73, 46);
            this.button4.TabIndex = 43;
            this.button4.Text = "DIS - COUNT";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(71, 92);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 46);
            this.button1.TabIndex = 45;
            this.button1.Text = "VOID ALL";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Location = new System.Drawing.Point(142, 92);
            this.button27.Margin = new System.Windows.Forms.Padding(0);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(71, 46);
            this.button27.TabIndex = 25;
            this.button27.Text = "VOID  ITEM";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment4, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(213, 1);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(73, 271);
            this.tableLayoutPanel1.TabIndex = 53;
            // 
            // btnDepartment1
            // 
            this.btnDepartment1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment1.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment1.Location = new System.Drawing.Point(0, 0);
            this.btnDepartment1.Margin = new System.Windows.Forms.Padding(0);
            this.btnDepartment1.Name = "btnDepartment1";
            this.btnDepartment1.Size = new System.Drawing.Size(73, 67);
            this.btnDepartment1.TabIndex = 29;
            this.btnDepartment1.Text = "Gifts";
            this.btnDepartment1.UseVisualStyleBackColor = false;
            this.btnDepartment1.Click += new System.EventHandler(this.btnDepartment1_Click);
            // 
            // btnDepartment2
            // 
            this.btnDepartment2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment2.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment2.Location = new System.Drawing.Point(0, 67);
            this.btnDepartment2.Margin = new System.Windows.Forms.Padding(0);
            this.btnDepartment2.Name = "btnDepartment2";
            this.btnDepartment2.Size = new System.Drawing.Size(73, 67);
            this.btnDepartment2.TabIndex = 30;
            this.btnDepartment2.Text = "Cards";
            this.btnDepartment2.UseVisualStyleBackColor = false;
            this.btnDepartment2.Click += new System.EventHandler(this.btnDepartment1_Click);
            // 
            // btnDepartment3
            // 
            this.btnDepartment3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment3.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment3.Location = new System.Drawing.Point(0, 134);
            this.btnDepartment3.Margin = new System.Windows.Forms.Padding(0);
            this.btnDepartment3.Name = "btnDepartment3";
            this.btnDepartment3.Size = new System.Drawing.Size(73, 67);
            this.btnDepartment3.TabIndex = 31;
            this.btnDepartment3.Text = "VASE";
            this.btnDepartment3.UseVisualStyleBackColor = false;
            this.btnDepartment3.Click += new System.EventHandler(this.btnDepartment1_Click);
            // 
            // btnDepartment4
            // 
            this.btnDepartment4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment4.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment4.Location = new System.Drawing.Point(0, 201);
            this.btnDepartment4.Margin = new System.Windows.Forms.Padding(0);
            this.btnDepartment4.Name = "btnDepartment4";
            this.btnDepartment4.Size = new System.Drawing.Size(73, 70);
            this.btnDepartment4.TabIndex = 22;
            this.btnDepartment4.Text = "CASH DROP";
            this.btnDepartment4.UseVisualStyleBackColor = false;
            this.btnDepartment4.Click += new System.EventHandler(this.btnDepartment1_Click);
            // 
            // frmOrdersAllQuickSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 720);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmOrdersAllQuickSales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order All QS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmOrdersAll_FormClosed);
            this.Load += new System.EventHandler(this.frmOrdersAll_Load);
            this.VisibleChanged += new System.EventHandler(this.frmOrdersAll_VisibleChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axCsdEft)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lb_orderid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button btnDepartment1;
        private System.Windows.Forms.Button btnoption;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button btnFindAcc;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button btnDepartment3;
        private System.Windows.Forms.Button btnDepartment4;
        private System.Windows.Forms.Button btnSendOrder;
        private System.Windows.Forms.Button btnDepartment2;
        private System.Windows.Forms.Button btnSubtotal;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button btnListDelivery;
        private System.Windows.Forms.Button btnBill;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonRefun;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnMergeOrder;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label8;
        private POSLabelStatus lblStatus;
        private System.Windows.Forms.Label lblTotal;
        private TextBoxNumbericPOS txtunitprice;
        private UCkeypad uCkeypad1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txttableid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel flp_takeaway;
        private System.Windows.Forms.TextBox txtstaffid;
        public System.Windows.Forms.TextBox txtorderid;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label lbChangeAmount;
        private System.Windows.Forms.Label lbChangeName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnitemdown;
        private System.Windows.Forms.Button btnitemup;
        private System.Windows.Forms.Button btngroupdown;
        private System.Windows.Forms.Button btngroupup;
        private System.Windows.Forms.TableLayoutPanel tlp_items;
        private System.Windows.Forms.TableLayoutPanel tlp_groups;
        public System.Windows.Forms.TextBox txtCusName;
        private System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Button btnNextCust;
        internal System.Windows.Forms.TextBox txtqty;
        //private AxCSDEFTLib.AxCsdEft axCsdEft;
        private CardInterface.POSAxCsdEft axCsdEft;





    }
}