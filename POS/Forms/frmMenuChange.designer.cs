﻿namespace POS.Forms
{
    partial class frmMenuChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSubmitUpdate = new System.Windows.Forms.Button();
            this.btnNewItem = new System.Windows.Forms.Button();
            this.btnNewGroup = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.lbStatus = new System.Windows.Forms.Label();
            this.pnLeft = new System.Windows.Forms.Panel();
            this.pnRight = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.pnContent = new System.Windows.Forms.Panel();
            this.lbNavigation = new System.Windows.Forms.Label();
            this.pnShortCutMenu = new System.Windows.Forms.Panel();
            this.tlpButtonTypeMenu = new System.Windows.Forms.TableLayoutPanel();
            this.btnAllMenu = new System.Windows.Forms.Button();
            this.btnMechanic = new System.Windows.Forms.Button();
            this.btnTyreMenu = new System.Windows.Forms.Button();
            this.btnMagMenu = new System.Windows.Forms.Button();
            this.ucMenuChange = new POS.Controls.UCMenu();
            this.btnExit = new POS.Controls.ButtonExit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.pnLeft.SuspendLayout();
            this.pnRight.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnShortCutMenu.SuspendLayout();
            this.tlpButtonTypeMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(0, 639);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(596, 70);
            this.panel2.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.btnSubmitUpdate, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNewItem, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNewGroup, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDelete, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(596, 70);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnSubmitUpdate
            // 
            this.btnSubmitUpdate.BackColor = System.Drawing.Color.Magenta;
            this.btnSubmitUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSubmitUpdate.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSubmitUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmitUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSubmitUpdate.Location = new System.Drawing.Point(447, 0);
            this.btnSubmitUpdate.Margin = new System.Windows.Forms.Padding(0);
            this.btnSubmitUpdate.Name = "btnSubmitUpdate";
            this.btnSubmitUpdate.Size = new System.Drawing.Size(149, 70);
            this.btnSubmitUpdate.TabIndex = 5;
            this.btnSubmitUpdate.Text = "Save";
            this.btnSubmitUpdate.UseVisualStyleBackColor = false;
            this.btnSubmitUpdate.Click += new System.EventHandler(this.btnSubmitUpdate_Click);
            // 
            // btnNewItem
            // 
            this.btnNewItem.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.btnNewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewItem.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNewItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewItem.ForeColor = System.Drawing.Color.White;
            this.btnNewItem.Location = new System.Drawing.Point(298, 0);
            this.btnNewItem.Margin = new System.Windows.Forms.Padding(0);
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(149, 70);
            this.btnNewItem.TabIndex = 2;
            this.btnNewItem.Text = "New Item";
            this.btnNewItem.UseVisualStyleBackColor = false;
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // btnNewGroup
            // 
            this.btnNewGroup.BackColor = System.Drawing.Color.Lime;
            this.btnNewGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewGroup.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNewGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewGroup.ForeColor = System.Drawing.Color.White;
            this.btnNewGroup.Location = new System.Drawing.Point(149, 0);
            this.btnNewGroup.Margin = new System.Windows.Forms.Padding(0);
            this.btnNewGroup.Name = "btnNewGroup";
            this.btnNewGroup.Size = new System.Drawing.Size(149, 70);
            this.btnNewGroup.TabIndex = 1;
            this.btnNewGroup.Text = "New Group";
            this.btnNewGroup.UseVisualStyleBackColor = false;
            this.btnNewGroup.Click += new System.EventHandler(this.btnNewGroup_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(0, 0);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(149, 70);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnBottom.Controls.Add(this.lbStatus);
            this.pnBottom.Controls.Add(this.btnExit);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 711);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1024, 57);
            this.pnBottom.TabIndex = 4;
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(99, 0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(923, 55);
            this.lbStatus.TabIndex = 2;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnLeft
            // 
            this.pnLeft.Controls.Add(this.ucMenuChange);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(426, 711);
            this.pnLeft.TabIndex = 5;
            // 
            // pnRight
            // 
            this.pnRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnRight.Controls.Add(this.panel1);
            this.pnRight.Controls.Add(this.pnShortCutMenu);
            this.pnRight.Controls.Add(this.panel2);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRight.Location = new System.Drawing.Point(426, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(598, 711);
            this.pnRight.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtBarcode);
            this.panel1.Controls.Add(this.pnContent);
            this.panel1.Controls.Add(this.lbNavigation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(596, 563);
            this.panel1.TabIndex = 8;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(6, 3);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(13, 20);
            this.txtBarcode.TabIndex = 2;
            this.txtBarcode.Visible = false;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // pnContent
            // 
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(0, 54);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(596, 509);
            this.pnContent.TabIndex = 1;
            // 
            // lbNavigation
            // 
            this.lbNavigation.BackColor = System.Drawing.Color.Purple;
            this.lbNavigation.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNavigation.ForeColor = System.Drawing.Color.White;
            this.lbNavigation.Location = new System.Drawing.Point(0, 0);
            this.lbNavigation.Name = "lbNavigation";
            this.lbNavigation.Size = new System.Drawing.Size(596, 54);
            this.lbNavigation.TabIndex = 0;
            this.lbNavigation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnShortCutMenu
            // 
            this.pnShortCutMenu.Controls.Add(this.tlpButtonTypeMenu);
            this.pnShortCutMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnShortCutMenu.Location = new System.Drawing.Point(0, 0);
            this.pnShortCutMenu.Name = "pnShortCutMenu";
            this.pnShortCutMenu.Size = new System.Drawing.Size(596, 76);
            this.pnShortCutMenu.TabIndex = 7;
            // 
            // tlpButtonTypeMenu
            // 
            this.tlpButtonTypeMenu.ColumnCount = 4;
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtonTypeMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtonTypeMenu.Controls.Add(this.btnAllMenu, 0, 0);
            this.tlpButtonTypeMenu.Controls.Add(this.btnMechanic, 3, 0);
            this.tlpButtonTypeMenu.Controls.Add(this.btnTyreMenu, 2, 0);
            this.tlpButtonTypeMenu.Controls.Add(this.btnMagMenu, 1, 0);
            this.tlpButtonTypeMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtonTypeMenu.Location = new System.Drawing.Point(0, 0);
            this.tlpButtonTypeMenu.Name = "tlpButtonTypeMenu";
            this.tlpButtonTypeMenu.RowCount = 1;
            this.tlpButtonTypeMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtonTypeMenu.Size = new System.Drawing.Size(596, 76);
            this.tlpButtonTypeMenu.TabIndex = 0;
            // 
            // btnAllMenu
            // 
            this.btnAllMenu.BackColor = System.Drawing.Color.Aqua;
            this.btnAllMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllMenu.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAllMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnAllMenu.ForeColor = System.Drawing.Color.White;
            this.btnAllMenu.Location = new System.Drawing.Point(3, 3);
            this.btnAllMenu.Name = "btnAllMenu";
            this.btnAllMenu.Size = new System.Drawing.Size(143, 70);
            this.btnAllMenu.TabIndex = 0;
            this.btnAllMenu.Text = "ALL MENU";
            this.btnAllMenu.UseVisualStyleBackColor = false;
            this.btnAllMenu.Click += new System.EventHandler(this.btnAllMenu_Click);
            // 
            // btnMechanic
            // 
            this.btnMechanic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnMechanic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMechanic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMechanic.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnMechanic.ForeColor = System.Drawing.Color.White;
            this.btnMechanic.Location = new System.Drawing.Point(450, 3);
            this.btnMechanic.Name = "btnMechanic";
            this.btnMechanic.Size = new System.Drawing.Size(143, 70);
            this.btnMechanic.TabIndex = 2;
            this.btnMechanic.Text = "MECHANIC MENU";
            this.btnMechanic.UseVisualStyleBackColor = false;
            this.btnMechanic.Click += new System.EventHandler(this.btnMechanic_Click);
            // 
            // btnTyreMenu
            // 
            this.btnTyreMenu.BackColor = System.Drawing.Color.Fuchsia;
            this.btnTyreMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTyreMenu.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnTyreMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTyreMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnTyreMenu.ForeColor = System.Drawing.Color.White;
            this.btnTyreMenu.Location = new System.Drawing.Point(301, 3);
            this.btnTyreMenu.Name = "btnTyreMenu";
            this.btnTyreMenu.Size = new System.Drawing.Size(143, 70);
            this.btnTyreMenu.TabIndex = 1;
            this.btnTyreMenu.Text = "TYRE MENU";
            this.btnTyreMenu.UseVisualStyleBackColor = false;
            this.btnTyreMenu.Click += new System.EventHandler(this.btnTyreMenu_Click);
            // 
            // btnMagMenu
            // 
            this.btnMagMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(195)))), ((int)(((byte)(45)))));
            this.btnMagMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMagMenu.FlatAppearance.BorderSize = 0;
            this.btnMagMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMagMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnMagMenu.ForeColor = System.Drawing.Color.White;
            this.btnMagMenu.Location = new System.Drawing.Point(152, 3);
            this.btnMagMenu.Name = "btnMagMenu";
            this.btnMagMenu.Size = new System.Drawing.Size(143, 70);
            this.btnMagMenu.TabIndex = 3;
            this.btnMagMenu.Text = "MAG MENU";
            this.btnMagMenu.UseVisualStyleBackColor = false;
            this.btnMagMenu.Click += new System.EventHandler(this.btnMagMenu_Click);
            // 
            // ucMenuChange
            // 
            this.ucMenuChange._Barcode = null;
            this.ucMenuChange._CheckConnectionServer = true;
            this.ucMenuChange._ClickGroup = true;
            this.ucMenuChange._ClickItems = true;
            this.ucMenuChange._ConnectionServer = true;
            this.ucMenuChange._Delete = DataObject.DeleteType.NoDelete;
            this.ucMenuChange._ExistsItem = false;
            this.ucMenuChange._GroupID = 0;
            this.ucMenuChange._GroupMenu = null;
            this.ucMenuChange._IsFuel = DataObject.IsFuelType.No;
            this.ucMenuChange._ItemID = 0;
            this.ucMenuChange._ItemsMenu = null;
            this.ucMenuChange._LoadMenu = true;
            this.ucMenuChange._Type = DataObject.ListItemType.Items;
            this.ucMenuChange._Visual = DataObject.VisualType.None;
            this.ucMenuChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucMenuChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMenuChange.GroupDisplayOrderMax = 0;
            this.ucMenuChange.ItemDisplayOrderMax = 0;
            this.ucMenuChange.Location = new System.Drawing.Point(0, 0);
            this.ucMenuChange.mDBConfig = null;
            this.ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
            this.ucMenuChange.Name = "ucMenuChange";
            this.ucMenuChange.Size = new System.Drawing.Size(426, 711);
            this.ucMenuChange.TabIndex = 0;
            this.ucMenuChange.Load += new System.EventHandler(this.ucMenuChange_Load);
            this.ucMenuChange.Click += new System.EventHandler(this.ucMenuChange_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(99, 55);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmMenuChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnRight);
            this.Controls.Add(this.pnLeft);
            this.Controls.Add(this.pnBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenuChange";
            this.Text = "frmMenuChange";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMenuChange_FormClosing);
            this.Load += new System.EventHandler(this.frmMenuChange_Load);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pnBottom.ResumeLayout(false);
            this.pnLeft.ResumeLayout(false);
            this.pnRight.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnShortCutMenu.ResumeLayout(false);
            this.tlpButtonTypeMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.UCMenu ucMenuChange;
        private Controls.ButtonExit btnExit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnSubmitUpdate;
        private System.Windows.Forms.Button btnNewItem;
        private System.Windows.Forms.Button btnNewGroup;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel pnLeft;
        private System.Windows.Forms.Panel pnRight;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Label lbNavigation;
        private System.Windows.Forms.Panel pnShortCutMenu;
        private System.Windows.Forms.TableLayoutPanel tlpButtonTypeMenu;
        private System.Windows.Forms.Button btnAllMenu;
        private System.Windows.Forms.Button btnTyreMenu;
        private System.Windows.Forms.Button btnMechanic;
        private System.Windows.Forms.Button btnMagMenu;
    }
}