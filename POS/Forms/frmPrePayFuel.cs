﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmPrePayFuel : Form
    {
        private int pumpID;
        private Class.ReadConfig mReadConfig;
        private Class.MoneyFortmat money;
        private List<FuelGrade> grades = new List<FuelGrade>();
        internal FuelGrade inputGrade { get; private set; }
        internal double inputAmount { get; private set; }
        internal double inputVolumn { get; private set; }
        int _currentbutton;
        List<Button> lsbutton;
        public frmPrePayFuel(int pumpID, List<FuelGrade> grades,Class.ReadConfig rConfig,Class.MoneyFortmat mmoney)
        {
            InitializeComponent();
            txtAmount.Select();
            this.pumpID = pumpID;
            lsbutton = new List<Button>();
            _currentbutton = -1;
            this.grades = grades;
            mReadConfig = rConfig;
            this.money = mmoney;            
            this.lblTitle.Text += "Dispenser " + this.pumpID + " | PREPAY";
            
            int maxGrades = this.grades.Count > 4 ? 4 : this.grades.Count;
            for (int i = 0; i < maxGrades; i++)
            {
                Button button = new Button();               
                button.Name = "btnGrade" + this.grades[i].gradeId;
                button.BackColor = System.Drawing.SystemColors.ControlDarkDark;
                button.ForeColor = System.Drawing.Color.White;
                button.Text = this.grades[i].gradeName;
                button.Width = 90;
                button.Height = 50;
                button.Tag = i;
                button.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                button.MouseClick += button_MouseClick;
                //this.Controls.Add(button);
                //left += button.Width + 2;
                lsbutton.Add(button);
            }
            LoadListbutton(lsbutton);
            SetMultiLanguage();
        }

        void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "XĂNG";
                label2.Text = "ĐƠN GIÁ";
                label3.Text = "TIỀN MẶT";
                label4.Text = "SỐ LÍT";
                btnAccept.Text = "CHẤP NHẬN";
                btnCancel.Text = "HỦY";
                btnClearAmount.Text = "XÓA";
                btnClearVolumn.Text = "XÓA";
                label1.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label2.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label3.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label4.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lblTitle.Text = "TRỤ BƠM " + this.pumpID + " | TRẢ TIỀN TRƯỚC";
            }
        }
        void LoadListbutton(List<Button> list)
        {
            int top = 45;
            int left = 110;
            foreach (Button btn in list)
            {
                if (_currentbutton == Convert.ToInt32(btn.Tag))
                {
                    btn.BackColor = Color.DarkMagenta;
                }
                else
                {
                    btn.BackColor = System.Drawing.SystemColors.ControlDarkDark;
                }
                this.Controls.Add(btn);
                btn.Top = top;
                btn.Left = left;
                left += btn.Width + 2;
            }
        }

        void button_MouseClick(object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            int gradeButtonIndex = Convert.ToInt32(b.Tag.ToString());
            lblPrice.Text = money.FormatNumber(Convert.ToInt32(this.grades[gradeButtonIndex].gradePrice));
            inputGrade = this.grades[gradeButtonIndex];
            txtAmount.Focus();
            txtAmount.Clear();
            txtVolumn.Clear();
            _currentbutton = Convert.ToInt32(b.Tag);
            LoadListbutton(lsbutton);
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (this.inputGrade == null)
            {
                lblErrorMessage.Text = "";
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblErrorMessage.Text += "Hãy chọn loại xăng!";
                }
                else
                {
                    lblErrorMessage.Text += "Please select a Grade!";
                }
                return;
            }
            this.inputAmount = txtAmount.Text != "" ? Convert.ToDouble(txtAmount.Text) : 0;
            this.inputVolumn = txtVolumn.Text != "" ? Convert.ToDouble(txtVolumn.Text) : 0;

            if(this.inputAmount == 0 && this.inputVolumn == 0)
            {
                lblErrorMessage.Text = "";
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblErrorMessage.Text += "Hãy điền số tiền thanh toán hoặc số lít xăng!";
                }
                else
                {
                    lblErrorMessage.Text += "Please add Amount or Volumn!";
                }
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnClearAmount_Click(object sender, EventArgs e)
        {
            txtAmount.Text = "";
        }

        private void btnClearVolumn_Click(object sender, EventArgs e)
        {
            txtVolumn.Text = "";
        }

        private void TextBoxPOS_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void TextBoxPOS_Leave(object sender, EventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;
                txt.BackColor = (Color)txt.Tag;
            }
            catch { }
        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void txtVolumn_KeyUp(object sender, KeyEventArgs e)
        {
            double price = 0;
            double.TryParse(lblPrice.Text, out price);
            double volumn = 0;
            double.TryParse(txtVolumn.Text, out volumn);
            txtAmount.Text = (volumn * price).ToString();
        }

        private void txtAmount_KeyUp(object sender, KeyEventArgs e)
        {
            double amount = 0;
            double.TryParse(txtAmount.Text, out amount);
            double price = 0;
            double.TryParse(lblPrice.Text, out price);
            txtVolumn.Text = Math.Round((amount / price),3).ToString();
        }
    }
}
