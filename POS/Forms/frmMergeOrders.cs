﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMergeOrders : Form
    {
        private Connection.Connection conn;

        public frmMergeOrders()
        {
            InitializeComponent();
        }

        public frmMergeOrders(Connection.Connection conn)
        {
            InitializeComponent();
            this.conn = conn;
            LoadOrder();
        }

        private void LoadOrder()
        {
            try
            {
                conn.Open();
                DataTable dtSource = conn.Select("");
                for (int i = 0; i < dtSource.Rows.Count; i++)
                {
                    ucOrder ucOrder = new ucOrder();
                    flow_layout_orders.Controls.Add(ucOrder);
                    ucOrder.lblOrder.Text = "";
                    ucOrder.lblDiscount.Text = "";
                    ucOrder.lblGST.Text = "";
                    ucOrder.lblSubtotal.Text = "";
                    ucOrder.btnOrder.Click += new EventHandler(btnOrder_Click);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
        }
    }
}