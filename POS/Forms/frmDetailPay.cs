﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmViewDetailPay : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private int intCustomerID;
        Class.ReadConfig mReadConfig = new Class.ReadConfig();
        public frmViewDetailPay(Connection.Connection mConnection, Class.MoneyFortmat money, int intCustomerID)
        {
            InitializeComponent();
            SetMultiLanguage();
            this.mConnection = mConnection;
            mSetting = new Class.Setting();
            mPrinter = new Printer();
            this.money = money;
            this.intCustomerID = intCustomerID;
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageReportAccountPayment);
        }

        public void ViewDetailAccountPay(string weekfrom, string weekto)
        {
            mConnection.Open();
            string sql = "SELECT PayDate,PayAmount AS TotalAmount FROM accountpayment AS AP inner join customers C ON C.memberNo = AP.CustomerNo where C.custID = " + intCustomerID;
            System.Data.DataTable tblSourceSaleInDay = mConnection.Select(sql);
            DateTime dt = DateTime.Now;
            double decTotal = 0;
            foreach (System.Data.DataRow row in tblSourceSaleInDay.Rows)
            {
                ListViewItem li = new ListViewItem(row["PayDate"].ToString());
                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["TotalAmount"]))));
                decTotal += Convert.ToDouble(row["TotalAmount"]);
                lstAccountDetail.Items.Add(li);
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                lblTotal2.Text = "Tổng số tài khoản : " + money.Format(decTotal);
                return;
            }
            lblTotal2.Text = "Total Account : " + money.Format(decTotal);
        }

        private void SetMultiLanguage()
        {
           
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Xem chi tiết thanh toán";
                label6.Text = "TỪ";
                label8.Text = "ĐẾN";
                button5.Text = "Đóng";
                button6.Text = "In";
                columnHeader1.Text = "Ngày thanh toán";
                columnHeader2.Text = "Tổng số tiền thanh toán";

                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(0);
                lstAccountDetail.Items[0].Selected = true;
                lstAccountDetail.Items[0].EnsureVisible();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstAccountDetail.SelectedIndices[0];
                    lstAccountDetail.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstAccountDetail.SelectedIndices.Add(lstAccountDetail.Items.Count - 1);
                        lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].Selected = true;
                        lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstAccountDetail.SelectedIndices.Add(oldselection - 1);
                        lstAccountDetail.Items[oldselection - 1].Selected = true;
                        lstAccountDetail.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstAccountDetail.SelectedIndices[0];
                    lstAccountDetail.SelectedIndices.Clear();
                    if (oldselection + 2 > lstAccountDetail.Items.Count)
                    {
                        lstAccountDetail.SelectedIndices.Add(0);
                        lstAccountDetail.Items[0].Selected = true;
                        lstAccountDetail.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstAccountDetail.SelectedIndices.Add(oldselection + 1);
                        lstAccountDetail.Items[oldselection + 1].Selected = true;
                        lstAccountDetail.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(lstAccountDetail.Items.Count - 1);
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].Selected = true;
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].EnsureVisible();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmViewDetailDeptAccount_Load(object sender, EventArgs e)
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Print Report");
            try
            {
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.button1_Click::" + ex.Message);
            }
        }

        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private string sql;
        private double dblTotalAmount;

        private void printDocument_PrintPageReportAccountPayment(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            l_y = mPrinter.DrawString("Account payment", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            //int i = 0;

            foreach (ListViewItem lvi in lstAccountDetail.Items)
            {
                mPrinter.DrawString(lvi.SubItems[0].Text, e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(lvi.SubItems[1].Text, e, new Font("Arial", 10), l_y, 0);
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDotDot, l_y, 1);
                dblTotalAmount += Convert.ToDouble(money.Format(lvi.SubItems[1].Text));
            }

            l_y += 10;
            l_y = mPrinter.DrawLine("Total Pay:" + money.Format(dblTotalAmount * 100), new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);
            l_y = mPrinter.DrawString("Total Pay:" + money.Format(dblTotalAmount * 100), e, new Font("Arial", 13, FontStyle.Bold), l_y, 1);
            l_y = mPrinter.DrawLine("Total Pay:" + money.Format(dblTotalAmount * 100), new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.Solid, l_y, 1);

            l_y += 100;
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
        }

        private string SplitDateFrom(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "00:00:00";
        }
    }
}