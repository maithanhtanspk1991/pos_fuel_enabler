﻿namespace POS.Forms
{
    partial class frmDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxPOSKeyPadPerCent = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyPadPrice = new POS.TextBoxPOSKeyPad();
            this.lblError = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.textBoxPOSKeyBoardItemName = new POS.TextBoxPOSKeyBoard();
            this.textBoxPOSKeyPadQty = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyPadTotal = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyPadUnitPrice = new POS.TextBoxPOSKeyPad();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Item name :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "Unit Price:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(399, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Qty :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(399, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "Total:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(138, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 26);
            this.label5.TabIndex = 21;
            this.label5.Text = "Percent";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(333, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 26);
            this.label6.TabIndex = 21;
            this.label6.Text = "Discount";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(304, 332);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(220, 80);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(41, 332);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(220, 80);
            this.btnSubmit.TabIndex = 31;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxPOSKeyPadPerCent);
            this.groupBox1.Controls.Add(this.textBoxPOSKeyPadPrice);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(557, 100);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Discount";
            // 
            // textBoxPOSKeyPadPerCent
            // 
            this.textBoxPOSKeyPadPerCent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyPadPerCent.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyPadPerCent.IsLockDot = true;
            this.textBoxPOSKeyPadPerCent.Location = new System.Drawing.Point(115, 51);
            this.textBoxPOSKeyPadPerCent.Name = "textBoxPOSKeyPadPerCent";
            this.textBoxPOSKeyPadPerCent.Size = new System.Drawing.Size(141, 38);
            this.textBoxPOSKeyPadPerCent.TabIndex = 0;
            this.textBoxPOSKeyPadPerCent.TextChanged += new System.EventHandler(this.textBoxPOSKeyPadPerCent_TextChanged);
            // 
            // textBoxPOSKeyPadPrice
            // 
            this.textBoxPOSKeyPadPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyPadPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyPadPrice.IsLockDot = false;
            this.textBoxPOSKeyPadPrice.Location = new System.Drawing.Point(311, 50);
            this.textBoxPOSKeyPadPrice.Name = "textBoxPOSKeyPadPrice";
            this.textBoxPOSKeyPadPrice.Size = new System.Drawing.Size(149, 38);
            this.textBoxPOSKeyPadPrice.TabIndex = 0;
            this.textBoxPOSKeyPadPrice.TextChanged += new System.EventHandler(this.textBoxPOSKeyPadPrice_TextChanged);
            // 
            // lblError
            // 
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 429);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(578, 46);
            this.lblError.TabIndex = 35;
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(222, 248);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(131, 62);
            this.btnLogin.TabIndex = 36;
            this.btnLogin.Text = "Log In";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // textBoxPOSKeyBoardItemName
            // 
            this.textBoxPOSKeyBoardItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyBoardItemName.Enabled = false;
            this.textBoxPOSKeyBoardItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyBoardItemName.Location = new System.Drawing.Point(115, 9);
            this.textBoxPOSKeyBoardItemName.Name = "textBoxPOSKeyBoardItemName";
            this.textBoxPOSKeyBoardItemName.Size = new System.Drawing.Size(278, 38);
            this.textBoxPOSKeyBoardItemName.TabIndex = 34;
            // 
            // textBoxPOSKeyPadQty
            // 
            this.textBoxPOSKeyPadQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyPadQty.Enabled = false;
            this.textBoxPOSKeyPadQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyPadQty.IsLockDot = false;
            this.textBoxPOSKeyPadQty.Location = new System.Drawing.Point(466, 9);
            this.textBoxPOSKeyPadQty.Name = "textBoxPOSKeyPadQty";
            this.textBoxPOSKeyPadQty.Size = new System.Drawing.Size(100, 38);
            this.textBoxPOSKeyPadQty.TabIndex = 0;
            // 
            // textBoxPOSKeyPadTotal
            // 
            this.textBoxPOSKeyPadTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyPadTotal.Enabled = false;
            this.textBoxPOSKeyPadTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyPadTotal.IsLockDot = false;
            this.textBoxPOSKeyPadTotal.Location = new System.Drawing.Point(466, 53);
            this.textBoxPOSKeyPadTotal.Name = "textBoxPOSKeyPadTotal";
            this.textBoxPOSKeyPadTotal.Size = new System.Drawing.Size(100, 38);
            this.textBoxPOSKeyPadTotal.TabIndex = 0;
            // 
            // textBoxPOSKeyPadUnitPrice
            // 
            this.textBoxPOSKeyPadUnitPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyPadUnitPrice.Enabled = false;
            this.textBoxPOSKeyPadUnitPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBoxPOSKeyPadUnitPrice.IsLockDot = false;
            this.textBoxPOSKeyPadUnitPrice.Location = new System.Drawing.Point(115, 53);
            this.textBoxPOSKeyPadUnitPrice.Name = "textBoxPOSKeyPadUnitPrice";
            this.textBoxPOSKeyPadUnitPrice.Size = new System.Drawing.Size(161, 38);
            this.textBoxPOSKeyPadUnitPrice.TabIndex = 0;
            // 
            // frmDiscount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 475);
            this.ControlBox = false;
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.textBoxPOSKeyBoardItemName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPOSKeyPadQty);
            this.Controls.Add(this.textBoxPOSKeyPadTotal);
            this.Controls.Add(this.textBoxPOSKeyPadUnitPrice);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmDiscount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Discount";
            this.Load += new System.EventHandler(this.frmDiscount_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private TextBoxPOSKeyPad textBoxPOSKeyPadUnitPrice;
        private TextBoxPOSKeyPad textBoxPOSKeyPadQty;
        private System.Windows.Forms.Label label2;
        private TextBoxPOSKeyPad textBoxPOSKeyPadTotal;
        private TextBoxPOSKeyPad textBoxPOSKeyPadPerCent;
        private System.Windows.Forms.Label label5;
        private TextBoxPOSKeyPad textBoxPOSKeyPadPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.GroupBox groupBox1;
        private TextBoxPOSKeyBoard textBoxPOSKeyBoardItemName;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Button btnLogin;
    }
}