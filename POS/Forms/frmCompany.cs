﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using POS.Forms.FindObject;

namespace POS.Forms
{
    public partial class frmCompany : Form
    {
        public Barcode.SerialPort mSerialPort;
        private CardInterface.POSAxCsdEft axCsdEft;
        private string CustCode = "";
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private bool IsCompanyCode = false;
        private DataObject.Company mCompany = null;
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private Printer mprinter;
        private Class.ReadConfig mReadConfig;
        private int shiftID;
        private string _nameClass = "POS::Forms::frmCompany::";

        public frmCompany(int shiftID, Barcode.SerialPort barcode, CardInterface.POSAxCsdEft axCsdEft, Class.ReadConfig readConfig)
        {
            InitializeComponent();
            SetMultiLanguage();
            mReadConfig = readConfig;
            this.shiftID = shiftID;
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mprinter = new Printer();
            mprinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            this.mSerialPort = barcode;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            this.axCsdEft = axCsdEft;
            ucLimit.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit_EventPage);
            SetValue();
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label3.Text = "Số công ty:";
                label1.Text = "Tên công ty:";
                label2.Text = "Tài khoản giới hạn:";
                label2.Font = new Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label7.Text = "Số điện thoại:";
                label5.Text = "Đường số:";
                label6.Text = "Tên đường:";
                btnSearch.Text = "Tìm kiếm";
                btnNew.Text = "Tạo mới";
                btnSubmit.Text = "Lưu lại";
                btnDelete.Text = "Xóa";
                btnHistory.Text = "Lịch sử";
                btnDepostit.Text = "Tiền bo";
                btnCredit.Text = "Thẻ tín dụng";
                columnHeader1.Text = "Số công ty";
                columnHeader2.Text = "Tên công ty";
                columnHeader3.Text = "Tài khoản giới hạn";
                columnHeader6.Text = "Đường số";
                columnHeader7.Text = "Tên đường";
                columnHeader9.Text = "Số điện thoại";
                btnBack.Text = "Quay lại";
                return;
            }
        }

        public string CalculateChecksumDigit()
        {
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 9; i++)
            {
                resuilt += rd.Next(10);
            }
            string sTemp = "893" + resuilt;
            int iSum = 0;
            int iDigit = 0;

            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                if (i % 2 == 0)
                {
                    iSum += iDigit * 3;
                }
                else
                {
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
        }

        public string GetCustCode()
        {
            string result = CalculateChecksumDigit();
            while (BusinessObject.BOCompany.GetCode(result) > 0)
            {
                result = CalculateChecksumDigit();
            }
            return result;
        }

        private void AddCompany(string companyName, string accountLimit)
        {
            try
            {
                mConnection.Open();
                CustCode = CalculateChecksumDigit();
                while (Convert.ToInt16(mConnection.ExecuteScalar("select count(memberNo) from company where companyCode=" + CustCode)) > 0)
                {
                    CustCode = CalculateChecksumDigit();
                }
                Double dbl = money.getFortMat(accountLimit);
                mConnection.ExecuteNonQuery("insert into company(companyName,accountLimit,Active,companyCode) value(" + "'" + companyName + "'" + "," + "'" + money.getFortMat(Convert.ToDouble(accountLimit)) + "',1,'" + CustCode + "')");
                mprinter.SetPrinterName(dbconfig.BillPrinter.ToString());
                mprinter.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "AddCompany::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            if (mCompany != null)
            {
                frmAccountPayment frmAccPay = new frmAccountPayment(money, 0, mCompany.IdCompany.ToString(), Convert.ToString(Convert.ToDouble(mCompany.AccountLimit) / 1000), Convert.ToString(Convert.ToDouble(mCompany.Debt) / 1000), mCompany.CompanyName, 0, 3, shiftID, axCsdEft);
                if (frmAccPay.ShowDialog() == DialogResult.OK)
                {
                    mCompany = BusinessObject.BOCompany.GetByID(mCompany.IdCompany);
                    SetValue();
                    LoadListview();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mReadConfig = new Class.ReadConfig();
            if (mCompany != null)
            {
                if (BusinessObject.BOCompany.GetCountAccountByID(mCompany.IdCompany) > 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Hãy gỡ bỏ các thành viên công ty từ tài khoản này trước khi xóa!";
                        return;
                    }
                    lbStatus.Text = "Please remove company members from this Account before delete!";
                    return;
                }
                if (mCompany.Debt != 0)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Yêu cầu thanh toán hết số nợ hiện tại. Không thể xóa công ty này!";
                        return;
                    }
                    lbStatus.Text = "Payment required to clear current balance. Can not Delete this Company!";
                    return;
                }
                frmMessageBox frmMessageBox = new frmMessageBox("Information", "Do you want to delete Company ?");
                if (frmMessageBox.ShowDialog() == DialogResult.OK)
                {
                    BusinessObject.BOCompany.Deleted(mCompany);
                    LoadListview();
                }
            }
        }

        private void btnDepostit_Click(object sender, EventArgs e)
        {
            if (mCompany != null)
            {
                frmAccountPayment frmAccPay = new frmAccountPayment(money, 0, mCompany.IdCompany.ToString(), Convert.ToString(Convert.ToDouble(mCompany.AccountLimit) / 1000), Convert.ToString(Convert.ToDouble(mCompany.Debt) / 1000), mCompany.CompanyName, 0, 4, shiftID, axCsdEft);
                if (frmAccPay.ShowDialog() == DialogResult.OK)
                {
                    LoadListview();
                }
            }
        }

        private void btnListEmployee_Click(object sender, EventArgs e)
        {
            if (mCompany != null)
            {
                frmFindPersonalCustomer.Instance("NEWCUSTOMER", money, mCompany.IdCompany.ToString()).ShowDialog();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            mCompany = null;
            SetValue();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ucLimit.Default();
            LoadListview();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (CheckValue())
            {
                if (mCompany == null)
                {
                    mCompany = new DataObject.Company();
                    mCompany.CompanyName = txtCompanyName.Text;
                    mCompany.CompanyCode = txtCompanyCode.Text;
                    mCompany.AccountLimit = Convert.ToInt32(DataObject.Utilities.ReFormatStringToNumber(txtAccountLimit.Text)) * 1000;
                    mCompany.Email = txtEmail.Text;
                    mCompany.StreetNo = txtStreetNo.Text;
                    mCompany.StreetName = txtStreetName.Text;
                    mCompany.Phone = txtPhone.Text;
                    BusinessObject.BOCompany.Insert(mCompany);
                    LoadListview();
                }
                else
                {
                    mCompany.CompanyName = txtCompanyName.Text;
                    mCompany.CompanyCode = txtCompanyCode.Text;
                    mCompany.AccountLimit = Convert.ToInt32(DataObject.Utilities.ReFormatStringToNumber(txtAccountLimit.Text)) * 1000;
                    mCompany.Email = txtEmail.Text;
                    mCompany.StreetNo = txtStreetNo.Text;
                    mCompany.StreetName = txtStreetName.Text;
                    mCompany.Phone = txtPhone.Text;
                    BusinessObject.BOCompany.Update(mCompany);
                    LoadListview();
                }
                mCompany = null;
                SetValue();
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private bool CheckValue()
        {
            mReadConfig = new Class.ReadConfig();
            txtAccountLimit.Text = DataObject.Utilities.ReFormatStringToNumber(txtAccountLimit.Text);
            if (txtCompanyName.Text == "" || txtAccountLimit.Text == "" || txtPhone.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Tất cả thông tin không được trống!";
                    return false;
                }
                lbStatus.Text = "All information not empty!";
                return false;
            }
            if (BusinessObject.BOCompany.GetCountByPhone((mCompany == null ? -1 : mCompany.IdCompany), txtPhone.Text) == 1)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Số liên lạc này đã được đăng ký, vui lòng sử dụng số liên lạc khác";
                    return false;
                }
                lbStatus.Text = " This contact has been registered, please use other contact number!";
                return false;
            }

            return true;
        }

        private void frmCompany_FormClosed(object sender, FormClosedEventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void frmSupplier_Load(object sender, EventArgs e)
        {
            btnCredit.Visible = mReadConfig.IsPrinterAccount;
            //btnDepostit.Visible = mReadConfig.IsPrinterAccount;
            btnHistory.Visible = mReadConfig.IsPrinterAccount;
            LoadListview();
        }

        private void LoadListview()
        {
            lvCompany.Items.Clear();
            List<DataObject.Company> lsArray = BusinessObject.BOCompany.GetAll(txtSearch.Text, 1, ucLimit.mLimit);
            ucLimit.PageNumReload();
            foreach (DataObject.Company item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.CompanyCode);
                li.SubItems.Add(item.CompanyName);
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.AccountLimit, 3));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Debt * -1, 3));
                li.SubItems.Add(item.Email);
                li.SubItems.Add(item.StreetNo);
                li.SubItems.Add(item.StreetName);
                li.SubItems.Add(item.Phone);
                li.Tag = item;
                lvCompany.Items.Add(li);
            }
        }

        private void LockButton()
        {
            if (mCompany != null)
            {
                btnDelete.Enabled = true;
                btnCredit.Enabled = true;
                btnDepostit.Enabled = true;
                btnHistory.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
                btnCredit.Enabled = false;
                btnDepostit.Enabled = false;
                btnHistory.Enabled = false;
            }
        }

        private void lvCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCompany.SelectedItems.Count > 0)
            {
                mCompany = (DataObject.Company)lvCompany.SelectedItems[0].Tag;
                SetValue();
            }
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPort.SetText("", txtCompanyCode);
                IsCompanyCode = true;
                mSerialPort.SetText(data, txtCompanyCode);
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;

            if (CustCode.Length > 12)
            {
                CustCode = CustCode.Substring(0, 12);
            }

            string header = "";
            string bankCode = "";
            string address = "";
            string address1 = "";
            string tell = "";
            string website = "";
            string thankyou = "";
            string terminal = "";

            try
            {
                header = dbconfig.Header1.ToString();
                bankCode = dbconfig.Header2.ToString();
                address = dbconfig.Header3.ToString();
                tell = dbconfig.Header4.ToString();
                address1 = dbconfig.Header5.ToString();
                website = dbconfig.FootNode1.ToString();
                thankyou = dbconfig.FootNode2.ToString();
                terminal = dbconfig.CableID.ToString();
            }
            catch (Exception ex)
            {

                Class.LogPOS.WriteLog(_nameClass + "printDocument_PrintPage::" + ex.Message);
            }

            l_y = mprinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mprinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mprinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mprinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mprinter.DrawString(address1, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += 100; // was 100
            l_y = mprinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += 100;

            DateTime dateTime = DateTime.Now;
            mprinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            l_y += 100;

            l_y = mprinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y = mprinter.DrawString("Company Account", e, new System.Drawing.Font("Arial", 15), l_y, 2);
            mprinter.DrawBarcode(CustCode, e, l_y, 3);
            l_y += 100;
            l_y = mprinter.DrawString("Company Name: " + txtCompanyName.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Account Limit: " + txtAccountLimit.Text + "$", e, font11, l_y, 1);
            l_y = mprinter.DrawString("Email: " + txtEmail.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Street No: " + txtStreetNo.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Street Name: " + txtStreetName.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Phone: " + txtPhone.Text, e, font11, l_y, 1);
        }

        private string RandomBarcode()
        {
            string scancode = CalculateChecksumDigit();
            int ishave = 1;
            try
            {
                mConnection.Open();
                while (ishave > 0)
                {
                    ishave = Convert.ToInt32(mConnection.ExecuteScalar("select count(memberNo) from company where companyCode=" + scancode));
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "RandomBarcode::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            return scancode;
        }

        private void SetValue()
        {
            IsCompanyCode = false;
            LockButton();
            if (mCompany == null)
            {
                txtAccountLimit.Text = "";
                txtCompanyName.Text = "";
                txtEmail.Text = "";
                txtStreetName.Text = "";
                txtStreetNo.Text = "";
                txtPhone.Text = "";
                lbStatus.Text = "";
                txtCompanyCode.Text = CalculateChecksumDigit();
            }
            else
            {
                txtAccountLimit.Text = DataObject.MonneyFormat.Format0(mCompany.AccountLimit / 1000);
                txtCompanyName.Text = mCompany.CompanyName;
                txtCompanyCode.Text = mCompany.CompanyCode;
                txtEmail.Text = mCompany.Email;
                txtStreetName.Text = mCompany.StreetName;
                txtStreetNo.Text = mCompany.StreetNo;
                txtPhone.Text = mCompany.Phone;
            }
            lbStatus.Text = "";
        }

        private void txtCompanyCode_TextChanged(object sender, EventArgs e)
        {
            if (IsCompanyCode)
            {
                mCompany = BusinessObject.BOCompany.GetByCode(txtCompanyCode.Text);
                SetValue();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            btnSearch_Click(sender, e);
        }

        private void ucLimit_EventPage(DataObject.Limit limit)
        {
            LoadListview();
        }
    }
}