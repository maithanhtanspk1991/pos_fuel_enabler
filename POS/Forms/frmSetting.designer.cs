﻿namespace POS.Forms
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtserver = new System.Windows.Forms.TextBox();
            this.txtdatabase = new System.Windows.Forms.TextBox();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbPrinterNameA4 = new System.Windows.Forms.ComboBox();
            this.lbkitchen = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbbill = new System.Windows.Forms.Label();
            this.cboSecond = new System.Windows.Forms.ComboBox();
            this.cboMinute = new System.Windows.Forms.ComboBox();
            this.cboHour = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboAllowSalesFastFood = new System.Windows.Forms.ComboBox();
            this.cboAllowSalesFuel = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboAllowUseShift = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboAllowPrintTaxInvoice = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbPrinterName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rbText = new System.Windows.Forms.RadioButton();
            this.rbHex = new System.Windows.Forms.RadioButton();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.cbbBaudRate = new System.Windows.Forms.ComboBox();
            this.cbbStopBits = new System.Windows.Forms.ComboBox();
            this.lblBaudRate = new System.Windows.Forms.Label();
            this.lblDataBits = new System.Windows.Forms.Label();
            this.cbbParity = new System.Windows.Forms.ComboBox();
            this.cbbDataBits = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cboBarCode = new System.Windows.Forms.ComboBox();
            this.cbbFuelAuto = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtserver);
            this.groupBox1.Controls.Add(this.txtdatabase);
            this.groupBox1.Controls.Add(this.txtuser);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtpass);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(523, 133);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MySQL Connection Paramaters";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(375, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 101);
            this.button1.TabIndex = 8;
            this.button1.Text = "Test Connection";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtserver
            // 
            this.txtserver.Location = new System.Drawing.Point(96, 25);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new System.Drawing.Size(279, 22);
            this.txtserver.TabIndex = 7;
            this.txtserver.Text = "192.168.1.101";
            // 
            // txtdatabase
            // 
            this.txtdatabase.Location = new System.Drawing.Point(96, 51);
            this.txtdatabase.Name = "txtdatabase";
            this.txtdatabase.Size = new System.Drawing.Size(279, 22);
            this.txtdatabase.TabIndex = 6;
            this.txtdatabase.Text = "vnrest";
            // 
            // txtuser
            // 
            this.txtuser.Location = new System.Drawing.Point(96, 78);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(279, 22);
            this.txtuser.TabIndex = 5;
            this.txtuser.Text = "root";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "UseName:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database:";
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(96, 104);
            this.txtpass.Name = "txtpass";
            this.txtpass.PasswordChar = '*';
            this.txtpass.Size = new System.Drawing.Size(279, 22);
            this.txtpass.TabIndex = 1;
            this.txtpass.Text = "becastek";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Host:";
            // 
            // cbbPrinterNameA4
            // 
            this.cbbPrinterNameA4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbPrinterNameA4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPrinterNameA4.FormattingEnabled = true;
            this.cbbPrinterNameA4.Location = new System.Drawing.Point(136, 42);
            this.cbbPrinterNameA4.Name = "cbbPrinterNameA4";
            this.cbbPrinterNameA4.Size = new System.Drawing.Size(363, 28);
            this.cbbPrinterNameA4.TabIndex = 0;
            this.cbbPrinterNameA4.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lbkitchen
            // 
            this.lbkitchen.AutoSize = true;
            this.lbkitchen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbkitchen.Location = new System.Drawing.Point(6, 48);
            this.lbkitchen.Name = "lbkitchen";
            this.lbkitchen.Size = new System.Drawing.Size(124, 16);
            this.lbkitchen.TabIndex = 5;
            this.lbkitchen.Text = "Printer Name A4:";
            this.lbkitchen.Click += new System.EventHandler(this.lbkitchen_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(2, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(523, 325);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Printer Configuration";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(10, 21);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(513, 214);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbbill);
            this.tabPage1.Controls.Add(this.cboSecond);
            this.tabPage1.Controls.Add(this.cbbPrinterNameA4);
            this.tabPage1.Controls.Add(this.cboMinute);
            this.tabPage1.Controls.Add(this.lbkitchen);
            this.tabPage1.Controls.Add(this.cboHour);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.cboAllowSalesFastFood);
            this.tabPage1.Controls.Add(this.cboAllowSalesFuel);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.cboAllowUseShift);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cboAllowPrintTaxInvoice);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.cbbPrinterName);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(505, 185);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "System";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // lbbill
            // 
            this.lbbill.AutoSize = true;
            this.lbbill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbbill.Location = new System.Drawing.Point(6, 14);
            this.lbbill.Name = "lbbill";
            this.lbbill.Size = new System.Drawing.Size(102, 16);
            this.lbbill.TabIndex = 11;
            this.lbbill.Text = "Printer Name:";
            // 
            // cboSecond
            // 
            this.cboSecond.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSecond.FormattingEnabled = true;
            this.cboSecond.Location = new System.Drawing.Point(206, 145);
            this.cboSecond.Name = "cboSecond";
            this.cboSecond.Size = new System.Drawing.Size(45, 28);
            this.cboSecond.TabIndex = 15;
            // 
            // cboMinute
            // 
            this.cboMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMinute.FormattingEnabled = true;
            this.cboMinute.Location = new System.Drawing.Point(159, 145);
            this.cboMinute.Name = "cboMinute";
            this.cboMinute.Size = new System.Drawing.Size(45, 28);
            this.cboMinute.TabIndex = 14;
            // 
            // cboHour
            // 
            this.cboHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHour.FormattingEnabled = true;
            this.cboHour.Location = new System.Drawing.Point(111, 145);
            this.cboHour.Name = "cboHour";
            this.cboHour.Size = new System.Drawing.Size(45, 28);
            this.cboHour.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Hour End Shift";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // cboAllowSalesFastFood
            // 
            this.cboAllowSalesFastFood.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAllowSalesFastFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAllowSalesFastFood.FormattingEnabled = true;
            this.cboAllowSalesFastFood.Items.AddRange(new object[] {
            "True",
            "False"});
            this.cboAllowSalesFastFood.Location = new System.Drawing.Point(417, 151);
            this.cboAllowSalesFastFood.Name = "cboAllowSalesFastFood";
            this.cboAllowSalesFastFood.Size = new System.Drawing.Size(82, 28);
            this.cboAllowSalesFastFood.TabIndex = 13;
            // 
            // cboAllowSalesFuel
            // 
            this.cboAllowSalesFuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAllowSalesFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAllowSalesFuel.FormattingEnabled = true;
            this.cboAllowSalesFuel.Items.AddRange(new object[] {
            "True",
            "False"});
            this.cboAllowSalesFuel.Location = new System.Drawing.Point(417, 122);
            this.cboAllowSalesFuel.Name = "cboAllowSalesFuel";
            this.cboAllowSalesFuel.Size = new System.Drawing.Size(82, 28);
            this.cboAllowSalesFuel.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(257, 157);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(163, 16);
            this.label12.TabIndex = 12;
            this.label12.Text = "Allow Sales Fast Food";
            // 
            // cboAllowUseShift
            // 
            this.cboAllowUseShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAllowUseShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAllowUseShift.FormattingEnabled = true;
            this.cboAllowUseShift.Items.AddRange(new object[] {
            "True",
            "False"});
            this.cboAllowUseShift.Location = new System.Drawing.Point(417, 93);
            this.cboAllowUseShift.Name = "cboAllowUseShift";
            this.cboAllowUseShift.Size = new System.Drawing.Size(82, 28);
            this.cboAllowUseShift.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(257, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 16);
            this.label10.TabIndex = 12;
            this.label10.Text = "Allow Sales Fuel";
            // 
            // cboAllowPrintTaxInvoice
            // 
            this.cboAllowPrintTaxInvoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAllowPrintTaxInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAllowPrintTaxInvoice.FormattingEnabled = true;
            this.cboAllowPrintTaxInvoice.Location = new System.Drawing.Point(170, 106);
            this.cboAllowPrintTaxInvoice.Name = "cboAllowPrintTaxInvoice";
            this.cboAllowPrintTaxInvoice.Size = new System.Drawing.Size(82, 28);
            this.cboAllowPrintTaxInvoice.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(257, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = "Allow Use Shift";
            // 
            // cbbPrinterName
            // 
            this.cbbPrinterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbPrinterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPrinterName.FormattingEnabled = true;
            this.cbbPrinterName.Location = new System.Drawing.Point(136, 8);
            this.cbbPrinterName.Name = "cbbPrinterName";
            this.cbbPrinterName.Size = new System.Drawing.Size(363, 28);
            this.cbbPrinterName.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Allow print Tax Invoice";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rbText);
            this.tabPage2.Controls.Add(this.rbHex);
            this.tabPage2.Controls.Add(this.lblStopBits);
            this.tabPage2.Controls.Add(this.cbbBaudRate);
            this.tabPage2.Controls.Add(this.cbbStopBits);
            this.tabPage2.Controls.Add(this.lblBaudRate);
            this.tabPage2.Controls.Add(this.lblDataBits);
            this.tabPage2.Controls.Add(this.cbbParity);
            this.tabPage2.Controls.Add(this.cbbDataBits);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.cboBarCode);
            this.tabPage2.Controls.Add(this.cbbFuelAuto);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(505, 185);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Device extension";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rbText
            // 
            this.rbText.AutoSize = true;
            this.rbText.Checked = true;
            this.rbText.Enabled = false;
            this.rbText.Location = new System.Drawing.Point(253, 60);
            this.rbText.Name = "rbText";
            this.rbText.Size = new System.Drawing.Size(56, 20);
            this.rbText.TabIndex = 18;
            this.rbText.TabStop = true;
            this.rbText.Text = "Text";
            this.rbText.Visible = false;
            // 
            // rbHex
            // 
            this.rbHex.AutoSize = true;
            this.rbHex.Enabled = false;
            this.rbHex.Location = new System.Drawing.Point(253, 80);
            this.rbHex.Name = "rbHex";
            this.rbHex.Size = new System.Drawing.Size(53, 20);
            this.rbHex.TabIndex = 19;
            this.rbHex.Text = "Hex";
            this.rbHex.Visible = false;
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(250, 14);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(74, 16);
            this.lblStopBits.TabIndex = 16;
            this.lblStopBits.Text = "Stop Bits:";
            // 
            // cbbBaudRate
            // 
            this.cbbBaudRate.FormattingEnabled = true;
            this.cbbBaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "28800",
            "36000",
            "115000"});
            this.cbbBaudRate.Location = new System.Drawing.Point(414, 66);
            this.cbbBaudRate.Name = "cbbBaudRate";
            this.cbbBaudRate.Size = new System.Drawing.Size(85, 24);
            this.cbbBaudRate.TabIndex = 11;
            // 
            // cbbStopBits
            // 
            this.cbbStopBits.FormattingEnabled = true;
            this.cbbStopBits.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbbStopBits.Location = new System.Drawing.Point(330, 11);
            this.cbbStopBits.Name = "cbbStopBits";
            this.cbbStopBits.Size = new System.Drawing.Size(169, 24);
            this.cbbStopBits.TabIndex = 17;
            // 
            // lblBaudRate
            // 
            this.lblBaudRate.AutoSize = true;
            this.lblBaudRate.Location = new System.Drawing.Point(318, 69);
            this.lblBaudRate.Name = "lblBaudRate";
            this.lblBaudRate.Size = new System.Drawing.Size(85, 16);
            this.lblBaudRate.TabIndex = 10;
            this.lblBaudRate.Text = "Baud Rate:";
            // 
            // lblDataBits
            // 
            this.lblDataBits.AutoSize = true;
            this.lblDataBits.Location = new System.Drawing.Point(252, 39);
            this.lblDataBits.Name = "lblDataBits";
            this.lblDataBits.Size = new System.Drawing.Size(75, 16);
            this.lblDataBits.TabIndex = 14;
            this.lblDataBits.Text = "Data Bits:";
            // 
            // cbbParity
            // 
            this.cbbParity.FormattingEnabled = true;
            this.cbbParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.cbbParity.Location = new System.Drawing.Point(414, 92);
            this.cbbParity.Name = "cbbParity";
            this.cbbParity.Size = new System.Drawing.Size(85, 24);
            this.cbbParity.TabIndex = 13;
            // 
            // cbbDataBits
            // 
            this.cbbDataBits.FormattingEnabled = true;
            this.cbbDataBits.Items.AddRange(new object[] {
            "7",
            "8",
            "9"});
            this.cbbDataBits.Location = new System.Drawing.Point(330, 36);
            this.cbbDataBits.Name = "cbbDataBits";
            this.cbbDataBits.Size = new System.Drawing.Size(169, 24);
            this.cbbDataBits.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(318, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Parity:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(10, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Barcode";
            this.label8.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(10, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 16);
            this.label11.TabIndex = 7;
            this.label11.Text = "Fuel Auto";
            // 
            // cboBarCode
            // 
            this.cboBarCode.ForeColor = System.Drawing.Color.Red;
            this.cboBarCode.FormattingEnabled = true;
            this.cboBarCode.Location = new System.Drawing.Point(89, 41);
            this.cboBarCode.Name = "cboBarCode";
            this.cboBarCode.Size = new System.Drawing.Size(145, 24);
            this.cboBarCode.TabIndex = 6;
            this.cboBarCode.Visible = false;
            // 
            // cbbFuelAuto
            // 
            this.cbbFuelAuto.ForeColor = System.Drawing.Color.Red;
            this.cbbFuelAuto.FormattingEnabled = true;
            this.cbbFuelAuto.Location = new System.Drawing.Point(89, 11);
            this.cbbFuelAuto.Name = "cbbFuelAuto";
            this.cbbFuelAuto.Size = new System.Drawing.Size(145, 24);
            this.cbbFuelAuto.TabIndex = 6;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(267, 241);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(250, 78);
            this.button3.TabIndex = 9;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(13, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(250, 78);
            this.button2.TabIndex = 6;
            this.button2.Text = "Submit All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 479);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SETTING";
            this.Load += new System.EventHandler(this.frmSetting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtserver;
        private System.Windows.Forms.TextBox txtdatabase;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbkitchen;
        private System.Windows.Forms.ComboBox cbbPrinterNameA4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lbbill;
        private System.Windows.Forms.ComboBox cbbPrinterName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboAllowPrintTaxInvoice;
        private System.Windows.Forms.ComboBox cboHour;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboMinute;
        private System.Windows.Forms.ComboBox cboSecond;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbbFuelAuto;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.ComboBox cbbBaudRate;
        private System.Windows.Forms.ComboBox cbbStopBits;
        private System.Windows.Forms.Label lblBaudRate;
        private System.Windows.Forms.Label lblDataBits;
        private System.Windows.Forms.ComboBox cbbParity;
        private System.Windows.Forms.ComboBox cbbDataBits;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbText;
        private System.Windows.Forms.RadioButton rbHex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboBarCode;
        private System.Windows.Forms.ComboBox cboAllowUseShift;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboAllowSalesFuel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboAllowSalesFastFood;
        private System.Windows.Forms.Label label12;

    }
}