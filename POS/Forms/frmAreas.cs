﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmAreas : Form
    {
        private Connection.Connection mConnection;
        private Class.Areas mAreas;

        public frmAreas()
        {
            InitializeComponent();
        }

        private void frmAreas_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            LoadListview();
            splitContainer1.Panel1Collapsed = true;
        }

        private void LoadListview()
        {
            try
            {
                System.Data.DataTable tbl = mConnection.Select("select * from areas");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    Class.Areas area = new Class.Areas(Convert.ToInt16(row["areaID"].ToString()), row["name"].ToString(), Convert.ToInt32(row["franchiseID"].ToString()), row["location"].ToString(), row["country"].ToString());
                    ListViewItem li = new ListViewItem(area.Name);
                    li.Tag = area;
                    li.SubItems.Add(area.FranchiseID.ToString());
                    li.SubItems.Add(area.Location);
                    li.SubItems.Add(area.Country);
                    listView1.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmAreas.LoadListview:::" + ex.Message);
            }
        }

        private void AddAreas(int franchiseID, string name, string location, string country)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("insert into areas(franchiseID,name,location,country) value(" + "'" + franchiseID + "'" + "," + "'" + name + "'" + "," + "'" + location + "'" + "," + "'" + country + "'" + ")");
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("AddAreas:::" + ex.Message);
            }
        }

        private void DeleteAreas(int areaID)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("delete from areas where areaID=" + areaID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("DeleteAreas:::" + ex.Message);
            }
        }

        private void UpdateAreas(int areaID, int franchiseID, string name, string location, string country)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("update areas set franchiseID=" + "'" + franchiseID + "'" + ",name=" + "'" + name + "'" + ",location=" + "'" + location + "'" + ",country=" + "'" + country + "'" + " where areaID=" + areaID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UpdateAreas:::" + ex.Message);
            }
        }

        private class ItemClick
        {
            public int ItemIndex { get; set; }

            public int ItemOptionIndex { get; set; }

            public ItemClick(int itemIndex, int itemOptionIndex)
            {
                ItemIndex = itemIndex;
                ItemOptionIndex = itemOptionIndex;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (splitContainer1.Panel1Collapsed == true)
            {
                splitContainer1.Panel1Collapsed = false;
            }
            else
            {
                splitContainer1.Panel1Collapsed = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection + 2 > listView1.Items.Count)
                    {
                        listView1.SelectedIndices.Add(0);
                        listView1.Items[0].Selected = true;
                        listView1.Items[0].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection + 1);
                        listView1.Items[oldselection + 1].Selected = true;
                        listView1.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                        listView1.Items[listView1.Items.Count - 1].Selected = true;
                        listView1.Items[listView1.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection - 1);
                        listView1.Items[oldselection - 1].Selected = true;
                        listView1.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(0);
                listView1.Items[0].Selected = true;
                listView1.Items[0].EnsureVisible();
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                Class.Areas areas = (Class.Areas)listView1.SelectedItems[0].Tag;
                mAreas = areas;
                textBoxPOS_OSKName.Text = areas.Name;
                textBoxPOS_OSK1.Text = areas.FranchiseID.ToString();
                textBoxPOS_OSK2.Text = areas.Location;
                textBoxPOS_OSK3.Text = areas.Country;
                listView1.Tag = areas.AreaID;
                button6.Enabled = true;
                button7.Enabled = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBoxPOS_OSKName.Text = "";
            textBoxPOS_OSK1.Text = "";
            textBoxPOS_OSK2.Text = "";
            textBoxPOS_OSK3.Text = "";
            button9.Enabled = true;
            button6.Enabled = false;
            button7.Enabled = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (textBoxPOS_OSKName.Text == "" || textBoxPOS_OSK1.Text == "" || textBoxPOS_OSK2.Text == "" || textBoxPOS_OSK3.Text == "")
            {
                label5.Text = "All information not empty!";
                label5.Visible = true;
            }
            else
            {
                try
                {
                    AddAreas(Convert.ToInt32(textBoxPOS_OSK1.Text), textBoxPOS_OSKName.Text, textBoxPOS_OSK2.Text, textBoxPOS_OSK3.Text);
                    listView1.Items.Clear();
                    LoadListview();
                    button9.Enabled = false;
                    button6.Enabled = true;
                    button7.Enabled = true;
                    label5.Visible = false;
                }
                catch (Exception)
                {
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                UpdateAreas(Convert.ToInt32(listView1.Tag.ToString()), Convert.ToInt32(textBoxPOS_OSK1.Text), textBoxPOS_OSKName.Text, textBoxPOS_OSK2.Text, textBoxPOS_OSK3.Text);
                listView1.Items.Clear();
                LoadListview();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                DeleteAreas(Convert.ToInt32(listView1.Tag.ToString()));
                listView1.Items.Clear();
                LoadListview();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void textBoxPOS_OSKName_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKName);
            textBoxPOS_OSKName.Tag = textBoxPOS_OSKName.BackColor;
            textBoxPOS_OSKName.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKName_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKName.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSK2_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSK2);
            textBoxPOS_OSK2.Tag = textBoxPOS_OSK2.BackColor;
            textBoxPOS_OSK2.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSK2_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSK2.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSK1_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSK1);
            textBoxPOS_OSK1.Tag = textBoxPOS_OSK1.BackColor;
            textBoxPOS_OSK1.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSK1_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSK1.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSK3_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSK3);
            textBoxPOS_OSK3.Tag = textBoxPOS_OSK3.BackColor;
            textBoxPOS_OSK3.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSK3_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSK3.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void button11_Click(object sender, EventArgs e)
        {
        }
    }
}