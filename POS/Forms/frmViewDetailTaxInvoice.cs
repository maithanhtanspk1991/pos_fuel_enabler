﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmViewDetailTaxInvoice : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private Connection.ReadDBConfig dbconfig;

        public frmViewDetailTaxInvoice(Connection.Connection mConnection, Class.MoneyFortmat money)
        {
            InitializeComponent();
            this.mConnection = mConnection;
            mSetting = new Class.Setting();
            mPrinter = new Printer();
            this.money = money;
            dbconfig = new Connection.ReadDBConfig();
        }

        private string GetDateString(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        public void ViewDetailAccountPay()
        {
            mConnection.Open();
            lstAccountDetail.Items.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(0);
                lstAccountDetail.Items[0].Selected = true;
                lstAccountDetail.Items[0].EnsureVisible();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(0);
                lstAccountDetail.Items[0].Selected = true;
                lstAccountDetail.Items[0].EnsureVisible();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstAccountDetail.SelectedIndices[0];
                    lstAccountDetail.SelectedIndices.Clear();
                    if (oldselection + 2 > lstAccountDetail.Items.Count)
                    {
                        lstAccountDetail.SelectedIndices.Add(0);
                        lstAccountDetail.Items[0].Selected = true;
                        lstAccountDetail.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstAccountDetail.SelectedIndices.Add(oldselection + 1);
                        lstAccountDetail.Items[oldselection + 1].Selected = true;
                        lstAccountDetail.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(lstAccountDetail.Items.Count - 1);
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].Selected = true;
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].EnsureVisible();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void LoadList(int intbkID, int intOrderID)
        {
            try
            {
                lstAccountDetail.Items.Clear();
                string sql = "";
                string sqlDetail = "";
                if (intbkID > 0)
                {
                    sql = "select * from ordersall where bkId=" + intbkID;
                    sqlDetail =
                        "select " +
                        //"o.clients," +
                            "ol.subTotal," +
                        //"o.discount," +
                        //"o.orderID," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                        //"if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID))) as itemDesc," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),'Dym Item')) as itemDesc," +
                            "ol.qty " +
                        //"from ordersall o inner join ordersallline ol on o.orderID=ol.orderID " +
                        "from ordersallline ol " +
                        "where ol.bkId=" + intbkID;
                }
                else
                {
                    sql = "select * from ordersdaily where orderID=" + intOrderID;
                    sqlDetail =
                        "select " +
                        //"o.clients," +
                            "ol.subTotal," +
                        //"o.discount," +
                        //"o.orderID," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "ol.qty " +
                        //"from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + intOrderID;
                }

                DataTable tbl = mConnection.Select(sql);
                Class.ProcessOrderNew.Order order = new Class.ProcessOrderNew.Order();
                if (tbl.Rows.Count > 0)
                {
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    //order.Deposit = Convert.ToDouble(tbl.Rows[0]["deposit"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Customer.CustID = Convert.ToInt16(tbl.Rows[0]["custID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                }

                List<Class.ItemOrderK> list = new List<Class.ItemOrderK>();
                DataTable tblListItem = mConnection.Select(sqlDetail);

                foreach (DataRow row in tblListItem.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        list.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 1));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                        }
                        else
                        {
                            if (list.Count == 0 || list[list.Count - 1].GRShortCut == 1)
                            {
                                list.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), "", 0, 0, 0, Convert.ToInt32(row["grShortCut"].ToString()), 0));
                            }
                            else
                            {
                                list[list.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if (list[i].Compare(list[j]))
                        {
                            list[i].AddSameItem(list[j]);
                            list.RemoveAt(j);
                            j--;
                        }
                    }
                }

                foreach (Class.ItemOrderK item in list)
                {
                    ListViewItem li = new ListViewItem();
                    if (item.ItemType != 1 || item.Qty != 0 || item.Price != 0)
                    {
                        string itemName = item.Name;
                        string optionName = "";
                        foreach (Class.ItemOptionID option in item.Option)
                        {
                            item.Price += option.Price;
                            optionName += option.Name + ",";
                        }
                        if (optionName.Length > 0)
                        {
                            optionName = optionName.Substring(0, optionName.Length - 1);
                            optionName = "(" + optionName + ")";
                        }
                        li = new ListViewItem(item.Qty + new String(' ', 3) + itemName + optionName);
                        li.SubItems.Add(money.Format(item.Price));
                        lstAccountDetail.Items.Add(li);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void frmViewDetailTaxInvoice_Load(object sender, EventArgs e)
        {
            lstAccountDetail.Columns[1].Width = 200;
            lstAccountDetail.Columns[0].Width = lstAccountDetail.Width - lstAccountDetail.Columns[1].Width;
        }
    }
}