﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmKeyPadNew : Form
    {
        private TextBox mTextBox;

        public bool IsNegative { get; set; }

        public bool mIsLockDot;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmKeyPadNew(TextBox textBox, bool isLockDot)
        {
            InitializeComponent();
            //Tuan xoa
            //mTextBox = textBox;
            //btndot.Enabled = !isLockDot;
            //Point p = GetPositionInForm(textBox);
            //this.Location = p;
            IsNegative = false;
            mTextBox = textBox;
            btndot.Enabled = !isLockDot;
            mIsLockDot = isLockDot;
            Point p = GetPositionInForm(textBox);
            this.Location = p;
            if (mTextBox.Text != "" || mTextBox.Text != null)
            {
                this.Tag = "1";
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnclear.Text = "Xóa";
                    return;
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            //Tuan xoa
            //Button btn = (Button)sender;
            //mTextBox.Text += btn.Text;

            Button btn = (Button)sender;
            if (this.Tag == "1")
            {
                mTextBox.Text = "";
                mTextBox.Text += btn.Text;
                this.Tag = "0";
            }
            else
            {
                mTextBox.Text += btn.Text;
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            //Da bi xoa tu truoc
            //if (mTextBox.Text=="")
            //{
            //    mTextBox.Text = "0";
            //}
            //this.Close();

            if (mTextBox.Text == "")
            {
                mTextBox.Text = "";
            }
            this.Close();
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (!mIsLockDot && !IsNegative)
            {
                if (mTextBox.Text.Length == 0)
                {
                    mTextBox.Text += "0.";
                }
                else
                {
                    if (mTextBox.Text.Contains('.'))
                    {
                        return;
                    }
                    else
                    {
                        mTextBox.Text += ".";
                    }
                }
            }
            else if (mIsLockDot && IsNegative)
            {
                if (mTextBox.Text.Length == 0)
                {
                    mTextBox.Text = "-";
                }
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            mTextBox.Text = "";
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            //Tuan xoa
            //if (mTextBox.Text.Length>0)
            //{
            //    string text = mTextBox.Text;
            //    mTextBox.Text = text.Remove(text.Length - 1, 1);
            //}

            if (mTextBox.Text.Length > 0 || this.Tag == "1")
            {
                string text = mTextBox.Text;
                if (text.Length > 0)
                {
                    mTextBox.Text = text.Remove(text.Length - 1, 1);
                }
                this.Tag = "0";
            }
            else
            {
                this.Tag = "1";
            }
        }

        public Point GetPositionInForm(Control ctrl)
        {
            //Tuan xoa
            //Point p = new Point();
            //p= ctrl.Parent.PointToScreen(ctrl.Location);
            //p.X = p.X + (ctrl.Width - this.Width)/2;
            //p.Y = p.Y + ctrl.Height;
            //return p;
            Point p = new Point();
            p = ctrl.Parent.PointToScreen(ctrl.Location);
            p.X = p.X + (ctrl.Width - this.Width) / 2;
            p.Y = p.Y + ctrl.Height;
            return p;
        }

        private void frmKeyPadNew_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        public void LoadNegative()
        {
        }
    }
}