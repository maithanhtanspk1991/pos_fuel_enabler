﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace POS.Forms
{
    public partial class frmReport : Form
    {
        private int m_currentPageIndex;

        private IList<Stream> m_streams;
        private Class.MoneyFortmat money = new Class.MoneyFortmat(1);
        private string _nameClass = "POS::Forms::frmReport::";
        private Class.ReadConfig mRead = new Class.ReadConfig();
        private void Init(Class.ProcessOrderNew.Order mOrders)
        {
            mOrders.Balance = mOrders.SubTotal - mOrders.Discount;

            mOrders.PriterCash = money.Format2(mOrders.Cash);
            mOrders.PriterCard = money.Format2(mOrders.Card);
            mOrders.PriterChange = money.Format2(mOrders.Change);
            mOrders.PriterDiscount = money.Format2(mOrders.Discount);
            mOrders.PriterSubTotal = money.Format2(mOrders.SubTotal);
            mOrders.PriterTotal = money.Format2(mOrders.Balance);
            mOrders.PriterGST = money.Format2(mOrders.SubTotal / 11);
            mOrders.PriterAccount = money.Format2(mOrders.Account);
            mOrders.SalesPerson = BusinessObject.BOStaff.GetByStaffID(mOrders.StaffID.ToString()).StaffName;
            mOrders.ReadConfig = mRead;
        }

        public frmReport(Class.ProcessOrderNew.Order mOrders)
        {
            try
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "frmReport");
                InitializeComponent();

                Init(mOrders);
                this.CustomersBindingSource.DataSource = mOrders.Customer;
                this.InfoCustomerBindingSource.DataSource = mOrders.ReadConfig.InfoCustomer;
                this.PartsBindingSource.DataSource = LoadData(mOrders);
                this.OrderBindingSource.DataSource = mOrders;
                this.RegoBindingSource.DataSource = BusinessObject.BORego.GetByID(mOrders.RegoID);
                rvOrder.PrinterSettings.PrinterName = mOrders.ReadConfig.PrinterTaxinvoiceA4;
                System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
                pg.Margins.Top = 0;
                pg.Margins.Bottom = 0;
                pg.Margins.Left = 0;
                pg.Margins.Right = 0;
                pg.PaperSize.RawKind = (int)PaperKind.A4;
                rvOrder.SetPageSettings(pg);
                rvOrder.RenderingComplete += new RenderingCompleteEventHandler(rvOrder_RenderingComplete);
                this.rvOrder.RefreshReport();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "frmReport::" + ex.Message);
            }
        }

        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        private void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.27in</PageWidth>
                <PageHeight>11.69in</PageHeight>
                <MarginTop>0.5in</MarginTop>
                <MarginLeft>0.0in</MarginLeft>
                <MarginRight>0.0in</MarginRight>
                <MarginBottom>0.5in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
        }

        private void Print()
        {
            SystemLog.LogPOS.WriteLog(_nameClass + "Print");
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            printDoc.DocumentName = DateTime.Now.ToString("yyyy-MM-dd HHmmss");
            printDoc.PrinterSettings = rvOrder.PrinterSettings;
            printDoc.PrintController = new StandardPrintController();
            if (!printDoc.PrinterSettings.IsValid)
            {
                Class.LogPOS.WriteLog(_nameClass + "Print::Cannot find the default printer");
            }
            else
            {
                try
                {
                    printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                    printDoc.Print();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "Print::" + ex.Message);
                }                
            }
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);
            ev.Graphics.DrawImage(pageImage, adjustedRect);
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void rvOrder_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {

            Export(rvOrder.LocalReport);
            Print();

        }

        private List<DataObject.Parts> LoadData(Class.ProcessOrderNew.Order mOrders)
        {
            List<Class.ItemOrderK> ls = Class.PrinterListItem.GetListItemPrinter(mOrders.OrderID);
            List<DataObject.Parts> lsArray = new List<DataObject.Parts>();
            int n = 1;
            foreach (Class.ItemOrderK item in ls)
            {
                lsArray.Add(new DataObject.Parts() { No = n++, Quantity = item.PrinterQty, Description = item.PrinterName, UnitPrice = money.Format2(item.Price), LineTotal = money.Format2(item.Total) });
            }
            return lsArray;
        }
    }

}