﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmKeyPad : Form
    {
        private bool ktdot;
        private TextBox mTextBox;

        public frmKeyPad()
        {
            InitializeComponent();
        }

        public frmKeyPad(TextBox txt)
        {
            InitializeComponent();
            mTextBox = txt;
        }

        public frmKeyPad(TextBox txt, bool isLockDot)
        {
            InitializeComponent();
            mTextBox = txt;
            btndot.Enabled = !isLockDot;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SendKey("0");      
        }

        private void SendKey(string str)
        {
            txtresult.Focus();
            SendKeys.Send(str);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            SendKey("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            SendKey("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            SendKey("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            SendKey("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            SendKey("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            SendKey("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            SendKey("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            SendKey("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            SendKey("9");
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txtresult.Text = "";
            ktdot = false;
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            int l = txtresult.Text.Length;
            if (l > 0)
            {
                txtresult.Text = txtresult.Text.Remove(l - 1);
                if (txtresult.Text.Contains("."))
                {
                    ktdot = true;
                }
                else
                {
                    ktdot = false;
                }
            }
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (txtresult.Text.Length == 0)
                SendKey("0.");
            else if (ktdot == false)
                SendKey(".");
            ktdot = true;
        }

        private void btnenter_Click(object sender, EventArgs e)
        {
            try
            {
                mTextBox.Text = txtresult.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;                
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmKeyPad.btnenter_Click:::" + ex.Message);
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmKeyPad_Load(object sender, EventArgs e)
        {
            if (mTextBox != null)
            {
                txtresult.Text = mTextBox.Text;
            }
        }
    }
}