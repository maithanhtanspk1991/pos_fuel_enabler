﻿namespace POS.Forms
{
    partial class frmAccountPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ckEnableChange = new System.Windows.Forms.CheckBox();
            this.ckEnableSurcharge = new System.Windows.Forms.CheckBox();
            this.btnDoTransaction = new System.Windows.Forms.Button();
            this.btnDepositAccount = new System.Windows.Forms.Button();
            this.txtChangePay = new POS.TextBoxNumbericPOS();
            this.uCkeypad1 = new POS.UCkeypad();
            this.txtBalance = new POS.TextBoxNumbericPOS();
            this.txtAccountLimit = new POS.TextBoxNumbericPOS();
            this.txtSurcharge = new POS.TextBoxNumbericPOS();
            this.txtCashOut = new POS.TextBoxNumbericPOS();
            this.txtTendered = new POS.TextBoxNumbericPOS();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCard = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCash = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblChangeText = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lbStatus = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblServeEmployee = new System.Windows.Forms.Label();
            this.btnUP = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.flCard = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ckEnableChange);
            this.groupBox2.Controls.Add(this.ckEnableSurcharge);
            this.groupBox2.Controls.Add(this.btnDoTransaction);
            this.groupBox2.Controls.Add(this.btnDepositAccount);
            this.groupBox2.Controls.Add(this.txtChangePay);
            this.groupBox2.Controls.Add(this.txtBalance);
            this.groupBox2.Controls.Add(this.txtAccountLimit);
            this.groupBox2.Controls.Add(this.txtSurcharge);
            this.groupBox2.Controls.Add(this.txtCashOut);
            this.groupBox2.Controls.Add(this.txtTendered);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.buttonCard);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnCash);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lblChangeText);
            this.groupBox2.Controls.Add(this.lblChange);
            this.groupBox2.Location = new System.Drawing.Point(6, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 441);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // ckEnableChange
            // 
            this.ckEnableChange.AutoSize = true;
            this.ckEnableChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckEnableChange.Location = new System.Drawing.Point(25, 302);
            this.ckEnableChange.Name = "ckEnableChange";
            this.ckEnableChange.Size = new System.Drawing.Size(151, 24);
            this.ckEnableChange.TabIndex = 37;
            this.ckEnableChange.Text = "Enable Change";
            this.ckEnableChange.UseVisualStyleBackColor = true;
            this.ckEnableChange.Visible = false;
            this.ckEnableChange.CheckedChanged += new System.EventHandler(this.checkBoxEnableChange_CheckedChanged);
            // 
            // ckEnableSurcharge
            // 
            this.ckEnableSurcharge.AutoSize = true;
            this.ckEnableSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckEnableSurcharge.Location = new System.Drawing.Point(25, 272);
            this.ckEnableSurcharge.Name = "ckEnableSurcharge";
            this.ckEnableSurcharge.Size = new System.Drawing.Size(172, 24);
            this.ckEnableSurcharge.TabIndex = 37;
            this.ckEnableSurcharge.Text = "Enable Surcharge";
            this.ckEnableSurcharge.UseVisualStyleBackColor = true;
            this.ckEnableSurcharge.Visible = false;
            this.ckEnableSurcharge.CheckedChanged += new System.EventHandler(this.checkBoxEnableSurcharge_CheckedChanged);
            // 
            // btnDoTransaction
            // 
            this.btnDoTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDoTransaction.FlatAppearance.BorderSize = 0;
            this.btnDoTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDoTransaction.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoTransaction.Location = new System.Drawing.Point(103, 364);
            this.btnDoTransaction.Name = "btnDoTransaction";
            this.btnDoTransaction.Size = new System.Drawing.Size(103, 51);
            this.btnDoTransaction.TabIndex = 36;
            this.btnDoTransaction.Text = "SWIPE CARD";
            this.btnDoTransaction.UseVisualStyleBackColor = false;
            this.btnDoTransaction.Visible = false;
            this.btnDoTransaction.Click += new System.EventHandler(this.btnDoTransaction_Click);
            // 
            // btnDepositAccount
            // 
            this.btnDepositAccount.BackColor = System.Drawing.Color.White;
            this.btnDepositAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepositAccount.Location = new System.Drawing.Point(4, 365);
            this.btnDepositAccount.Name = "btnDepositAccount";
            this.btnDepositAccount.Size = new System.Drawing.Size(84, 51);
            this.btnDepositAccount.TabIndex = 16;
            this.btnDepositAccount.UseVisualStyleBackColor = false;
            this.btnDepositAccount.Visible = false;
            this.btnDepositAccount.Click += new System.EventHandler(this.btnDepositAccount_Click);
            // 
            // txtChangePay
            // 
            this.txtChangePay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtChangePay.Enabled = false;
            this.txtChangePay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtChangePay.Location = new System.Drawing.Point(209, 234);
            this.txtChangePay.Name = "txtChangePay";
            this.txtChangePay.Size = new System.Drawing.Size(108, 32);
            this.txtChangePay.TabIndex = 15;
            this.txtChangePay.Text = "0";
            this.txtChangePay.ucKeypad = this.uCkeypad1;
            this.txtChangePay.uckeypadDiv = null;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(6, 14);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(213, 272);
            this.uCkeypad1.TabIndex = 4;
            this.uCkeypad1.txtResult = null;
            // 
            // txtBalance
            // 
            this.txtBalance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtBalance.Enabled = false;
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtBalance.Location = new System.Drawing.Point(1, 234);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(202, 32);
            this.txtBalance.TabIndex = 14;
            this.txtBalance.Text = "0";
            this.txtBalance.ucKeypad = this.uCkeypad1;
            this.txtBalance.uckeypadDiv = null;
            // 
            // txtAccountLimit
            // 
            this.txtAccountLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtAccountLimit.Enabled = false;
            this.txtAccountLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtAccountLimit.Location = new System.Drawing.Point(1, 183);
            this.txtAccountLimit.Name = "txtAccountLimit";
            this.txtAccountLimit.Size = new System.Drawing.Size(316, 32);
            this.txtAccountLimit.TabIndex = 13;
            this.txtAccountLimit.Text = "0";
            this.txtAccountLimit.ucKeypad = this.uCkeypad1;
            this.txtAccountLimit.uckeypadDiv = null;
            // 
            // txtSurcharge
            // 
            this.txtSurcharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtSurcharge.Enabled = false;
            this.txtSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtSurcharge.Location = new System.Drawing.Point(1, 132);
            this.txtSurcharge.Name = "txtSurcharge";
            this.txtSurcharge.Size = new System.Drawing.Size(172, 32);
            this.txtSurcharge.TabIndex = 12;
            this.txtSurcharge.ucKeypad = null;
            this.txtSurcharge.uckeypadDiv = null;
            // 
            // txtCashOut
            // 
            this.txtCashOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtCashOut.Location = new System.Drawing.Point(1, 81);
            this.txtCashOut.Name = "txtCashOut";
            this.txtCashOut.Size = new System.Drawing.Size(316, 32);
            this.txtCashOut.TabIndex = 12;
            this.txtCashOut.ucKeypad = this.uCkeypad1;
            this.txtCashOut.uckeypadDiv = null;
            this.txtCashOut.TextChanged += new System.EventHandler(this.txtCashOut_TextChanged);
            // 
            // txtTendered
            // 
            this.txtTendered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtTendered.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtTendered.Location = new System.Drawing.Point(1, 30);
            this.txtTendered.Name = "txtTendered";
            this.txtTendered.Size = new System.Drawing.Size(316, 32);
            this.txtTendered.TabIndex = 11;
            this.txtTendered.ucKeypad = this.uCkeypad1;
            this.txtTendered.uckeypadDiv = null;
            this.txtTendered.TextChanged += new System.EventHandler(this.txtTendered_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(212, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "CREDIT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "BALANCE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "ACCOUNT LIMIT";
            // 
            // buttonCard
            // 
            this.buttonCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCard.Location = new System.Drawing.Point(103, 365);
            this.buttonCard.Name = "buttonCard";
            this.buttonCard.Size = new System.Drawing.Size(103, 51);
            this.buttonCard.TabIndex = 5;
            this.buttonCard.Text = "Pay Off By Card";
            this.buttonCard.UseVisualStyleBackColor = false;
            this.buttonCard.Visible = false;
            this.buttonCard.Click += new System.EventHandler(this.buttonCard_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SURCHARGE";
            // 
            // btnCash
            // 
            this.btnCash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnCash.FlatAppearance.BorderSize = 0;
            this.btnCash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCash.Location = new System.Drawing.Point(211, 364);
            this.btnCash.Name = "btnCash";
            this.btnCash.Size = new System.Drawing.Size(104, 51);
            this.btnCash.TabIndex = 5;
            this.btnCash.Text = "Pay Off By Cash";
            this.btnCash.UseVisualStyleBackColor = false;
            this.btnCash.Visible = false;
            this.btnCash.Click += new System.EventHandler(this.buttonCash_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "CARD";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "TENDERED";
            // 
            // lblChangeText
            // 
            this.lblChangeText.AutoSize = true;
            this.lblChangeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChangeText.ForeColor = System.Drawing.Color.Red;
            this.lblChangeText.Location = new System.Drawing.Point(5, 329);
            this.lblChangeText.Name = "lblChangeText";
            this.lblChangeText.Size = new System.Drawing.Size(101, 25);
            this.lblChangeText.TabIndex = 0;
            this.lblChangeText.Text = "CHANGE";
            // 
            // lblChange
            // 
            this.lblChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange.ForeColor = System.Drawing.Color.Red;
            this.lblChange.Location = new System.Drawing.Point(103, 330);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(212, 24);
            this.lblChange.TabIndex = 0;
            this.lblChange.Text = "0.00";
            this.lblChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(221, 14);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(3);
            this.button1.Size = new System.Drawing.Size(96, 67);
            this.button1.TabIndex = 3;
            this.button1.Tag = "100";
            this.button1.Text = "$100";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.uCkeypad1);
            this.groupBox3.Controls.Add(this.btnAccept);
            this.groupBox3.Controls.Add(this.btnCancel);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(336, 60);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(420, 390);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Location = new System.Drawing.Point(156, 289);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(259, 79);
            this.btnAccept.TabIndex = 1;
            this.btnAccept.Text = "ACCEPT";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(6, 289);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(144, 79);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(221, 219);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(3);
            this.button8.Size = new System.Drawing.Size(194, 67);
            this.button8.TabIndex = 3;
            this.button8.Tag = "";
            this.button8.Text = "Clear";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(319, 150);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(3);
            this.button7.Size = new System.Drawing.Size(96, 67);
            this.button7.TabIndex = 3;
            this.button7.Tag = "1";
            this.button7.Text = "$1";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button5_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(221, 150);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(3);
            this.button5.Size = new System.Drawing.Size(96, 67);
            this.button5.TabIndex = 3;
            this.button5.Tag = "5";
            this.button5.Text = "$5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(221, 82);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(3);
            this.button4.Size = new System.Drawing.Size(96, 67);
            this.button4.TabIndex = 3;
            this.button4.Tag = "10";
            this.button4.Text = "$10";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(319, 82);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(3);
            this.button3.Size = new System.Drawing.Size(96, 67);
            this.button3.TabIndex = 3;
            this.button3.Tag = "20";
            this.button3.Text = "$20";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(319, 14);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(3);
            this.button2.Size = new System.Drawing.Size(96, 67);
            this.button2.TabIndex = 3;
            this.button2.Tag = "50";
            this.button2.Text = "$50";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button5_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(0, 454);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(1020, 25);
            this.lbStatus.TabIndex = 29;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 24);
            this.label14.TabIndex = 32;
            this.label14.Text = "Payment to:";
            // 
            // lblServeEmployee
            // 
            this.lblServeEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServeEmployee.ForeColor = System.Drawing.Color.Blue;
            this.lblServeEmployee.Location = new System.Drawing.Point(131, 14);
            this.lblServeEmployee.Name = "lblServeEmployee";
            this.lblServeEmployee.Size = new System.Drawing.Size(283, 29);
            this.lblServeEmployee.TabIndex = 33;
            this.lblServeEmployee.Text = "label1";
            // 
            // btnUP
            // 
            this.btnUP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnUP.FlatAppearance.BorderSize = 0;
            this.btnUP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUP.Location = new System.Drawing.Point(762, 412);
            this.btnUP.Name = "btnUP";
            this.btnUP.Size = new System.Drawing.Size(126, 38);
            this.btnUP.TabIndex = 44;
            this.btnUP.Text = "UP";
            this.btnUP.UseVisualStyleBackColor = false;
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDown.FlatAppearance.BorderSize = 0;
            this.btnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDown.Location = new System.Drawing.Point(891, 412);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(126, 38);
            this.btnDown.TabIndex = 43;
            this.btnDown.Text = "DOWN";
            this.btnDown.UseVisualStyleBackColor = false;
            // 
            // flCard
            // 
            this.flCard.AutoScroll = true;
            this.flCard.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flCard.Location = new System.Drawing.Point(762, 9);
            this.flCard.Name = "flCard";
            this.flCard.Size = new System.Drawing.Size(255, 397);
            this.flCard.TabIndex = 42;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lblServeEmployee);
            this.groupBox1.Location = new System.Drawing.Point(336, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 56);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            // 
            // frmAccountPayment
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1020, 479);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnUP);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.flCard);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmAccountPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment account";
            this.Load += new System.EventHandler(this.frmSubTotal_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblChangeText;
        private System.Windows.Forms.Label lblChange;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button button8;
        private UCkeypad uCkeypad1;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonCard;
        private System.Windows.Forms.Button btnCash;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblServeEmployee;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private TextBoxNumbericPOS txtChangePay;
        private TextBoxNumbericPOS txtBalance;
        private TextBoxNumbericPOS txtAccountLimit;
        private TextBoxNumbericPOS txtCashOut;
        private TextBoxNumbericPOS txtTendered;
        private System.Windows.Forms.Button btnDepositAccount;
        private System.Windows.Forms.Button btnUP;
        private System.Windows.Forms.Button btnDown;
        public System.Windows.Forms.FlowLayoutPanel flCard;
        private System.Windows.Forms.Button btnDoTransaction;
        private System.Windows.Forms.CheckBox ckEnableChange;
        private System.Windows.Forms.CheckBox ckEnableSurcharge;
        private TextBoxNumbericPOS txtSurcharge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}