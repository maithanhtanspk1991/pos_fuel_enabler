﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLoyatyControl : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat mMoneyFortmat;
        private Barcode.SerialPort mSerialPort;

        private delegate void SetListViewCallback(int itemID);

        private Item mItem;
        private bool mIsLockText;

        public frmLoyatyControl(Barcode.SerialPort serial)
        {
            InitializeComponent();
            mSerialPort = serial;
        }

        private void frmLoyatyControl_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            LoadListView(0);
            ClearData();
            mIsLockText = false;
        }

        public void LoadListViewCallBack(int itemID)
        {
            if (listView1.InvokeRequired)
            {
                SetListViewCallback d = new SetListViewCallback(LoadListViewCallBack);
                listView1.Invoke(d, new object[] { itemID });
            }
            else
            {
                LoadListView(itemID);
            }
        }

        private void mSerialPort_Received(string data)
        {
            mSerialPort.SetText("", textBoxPOSKeyBoardItemName);
            mSerialPort.SetText("", labelError);
            try
            {
                DataTable tbl = mConnection.Select("select itemID,itemDesc,unitPrice from itemsmenu where scanCode like '%" + data + "%' limit 0,1");
                if (tbl.Rows.Count > 0)
                {
                    mItem = new Item(Convert.ToInt32(tbl.Rows[0]["itemID"]), tbl.Rows[0]["itemDesc"].ToString(), Convert.ToDouble(tbl.Rows[0]["unitPrice"]));
                    //LoadListViewCallBack(mItem.ItemID);
                    mSerialPort.SetText(mItem.ItemName, textBoxPOSKeyBoardItemName);
                }
                else
                {
                    mSerialPort.SetText("Item not found!", labelError);
                }
            }
            catch (Exception)
            {
            }
        }

        private void LoadListView(int itemID)
        {
            //mIsLockText = true;
            try
            {
                mConnection.Open();
                DataTable tbl = mConnection.Select(
                        "select " +
                            "loyaltyID," +
                            "itemID," +
                            "if((select itemDesc from itemsmenu i where i.itemID=l.itemID) is null,'Unknown',(select itemDesc from itemsmenu i where i.itemID=l.itemID)) as itemName," +
                            "if((select unitPrice from itemsmenu i where i.itemID=l.itemID) is null,0,(select unitPrice from itemsmenu i where i.itemID=l.itemID)) as price," +
                            "name," +
                            "qty," +
                            "discount," +
                            "dateStart," +
                            "if(dateEnd='0000-00-00 00:00:00',null,date(dateEnd)) as dateEnd," +
                    //"dateEnd," +
                            "enable " +
                        "from loyaltybyitem l " +
                        "where itemID=" + itemID + " or 0=" + itemID
                    );
                listView1.Items.Clear();
                foreach (DataRow row in tbl.Rows)
                {
                    Item item = new Item(Convert.ToInt32(row["itemID"]), row["itemName"].ToString(), Convert.ToDouble(row["price"]));
                    Loyalty loyalty;
                    bool enable = false;
                    if (row["enable"].ToString() == "1")
                    {
                        enable = true;
                    }
                    if (row["dateEnd"] != DBNull.Value)
                    {
                        loyalty = new Loyalty(Convert.ToInt32(row["loyaltyID"]), row["name"].ToString(), Convert.ToDouble(row["discount"]), Convert.ToInt32(row["qty"]), Convert.ToDateTime(row["dateStart"]), Convert.ToDateTime(Encoding.Default.GetString((byte[])row["dateEnd"])), enable, item, true);
                    }
                    else
                    {
                        loyalty = new Loyalty(Convert.ToInt32(row["loyaltyID"]), row["name"].ToString(), Convert.ToDouble(row["discount"]), Convert.ToInt32(row["qty"]), Convert.ToDateTime(row["dateStart"]), enable, item, false);
                    }
                    AddLoyaltyToListView(loyalty);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
            //mIsLockText = false;
        }

        private void checkBoxEndDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxEndDate.Checked;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mIsLockText = true;
            if (listView1.SelectedIndices.Count > 0)
            {
                //Item item = ((loyalty)listView1.SelectedItems[0].Tag).Item;
                //textBoxPOSKeyBoardName.Text = listView1.SelectedItems[0].SubItems[0].Text;
                //textBoxPOSKeyBoardItemName.Text = listView1.SelectedItems[0].SubItems[1].Text;
                //textBoxPOSKeyBoardQty.Text = listView1.SelectedItems[0].SubItems[2].Text;
                ////textBoxPOSKeyBoardPrice.Text = listView1.SelectedItems[0].SubItems[0].Text;
                //textBoxPOSKeyBoardPrice.Text = mMoneyFortmat.Format(Convert.ToInt32(textBoxPOSKeyBoardQty.Text)*item.Price);
                //textBoxPOSKeyBoardDiscount.Text = listView1.SelectedItems[0].SubItems[3].Text;
                //textBoxPOSKeyBoardSalesPrice.Text = mMoneyFortmat.FormatCurenMoney(Convert.ToDouble(textBoxPOSKeyBoardPrice.Text)-Convert.ToDouble(textBoxPOSKeyBoardDiscount.Text));

                //dateTimePickerfrom.Value = Convert.ToDateTime(listView1.SelectedItems[0].SubItems[4].Text);
                Loyalty loyalty = (Loyalty)listView1.SelectedItems[0].Tag;
                textBoxPOSKeyBoardName.Text = loyalty.Name;
                textBoxPOSKeyBoardItemName.Text = loyalty.Item.ItemName;
                textBoxPOSKeyBoardQty.Text = loyalty.Qty + "";
                //textBoxPOSKeyBoardPrice.Text = listView1.SelectedItems[0].SubItems[0].Text;
                textBoxPOSKeyBoardPrice.Text = mMoneyFortmat.Format(loyalty.Item.Price * loyalty.Qty);
                textBoxPOSKeyBoardDiscount.Text = mMoneyFortmat.Format(loyalty.Discount);
                textBoxPOSKeyBoardSalesPrice.Text = mMoneyFortmat.Format(loyalty.Qty * loyalty.Item.Price - loyalty.Discount);
                dateTimePickerfrom.Value = loyalty.DateStart;
                dateTimePickerEnd.Enabled = checkBoxEndDate.Checked = loyalty.IsEndDate;
                if (loyalty.IsEndDate)
                {
                    dateTimePickerEnd.Value = loyalty.DateEnd;
                }
                checkBoxEnable.Checked = loyalty.Enable;
                mItem = loyalty.Item;
                buttonSave.Tag = 1;
            }
            mIsLockText = false;
        }

        private class Item
        {
            public int ItemID { get; set; }

            public string ItemName { get; set; }

            public double Price { get; set; }

            public Item(int itemID, string name, double price)
            {
                ItemID = itemID;
                ItemName = name;
                Price = price;
            }
        }

        private class Loyalty
        {
            public int LoyaltyID { get; set; }

            public string Name { get; set; }

            public double Discount { get; set; }

            public int Qty { get; set; }

            public DateTime DateStart { get; set; }

            public DateTime DateEnd { get; set; }

            public bool Enable { get; set; }

            public Item Item { get; set; }

            public bool IsEndDate { get; set; }

            public Loyalty()
            { }

            public Loyalty(int loyaltyID, string name, double discount, int qty, DateTime dateStart, DateTime dateEnd, bool enable, Item item, bool isEndDate)
            {
                LoyaltyID = loyaltyID;
                Name = name;
                Discount = discount;
                Qty = qty;
                DateStart = dateStart;
                DateEnd = dateEnd;
                Enable = enable;
                Item = item;
                IsEndDate = isEndDate;
                //IsEndDate = isEndDate;
            }

            public Loyalty(int loyaltyID, string name, double discount, int qty, DateTime dateStart, bool enable, Item item, bool isEndDate)
            {
                LoyaltyID = loyaltyID;
                Name = name;
                Discount = discount;
                Qty = qty;
                DateStart = dateStart;
                //DateEnd = dateEnd;
                Enable = enable;
                Item = item;
                IsEndDate = isEndDate;
                //IsEndDate = isEndDate;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                //listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                try
                {
                    mConnection.Open();
                    ListViewItem li = listView1.SelectedItems[0];
                    Loyalty loyalty = (Loyalty)li.Tag;
                    mConnection.ExecuteNonQuery("delete from loyaltybyitem where loyaltyID=" + loyalty.LoyaltyID);
                    listView1.Items.Remove(li);
                    ClearData();
                }
                catch (Exception)
                {
                }
                finally
                {
                    mConnection.Close();
                }
            }
        }

        private void ClearData()
        {
            labelError.Text = "";
            mItem = null;
            buttonSave.Tag = null;
            textBoxPOSKeyBoardDiscount.Text = "";
            textBoxPOSKeyBoardItemName.Text = "";
            textBoxPOSKeyBoardName.Text = "";
            textBoxPOSKeyBoardPrice.Text = "";
            textBoxPOSKeyBoardQty.Text = "";
            textBoxPOSKeyBoardSalesPrice.Text = "";
            checkBoxEnable.Checked = true;
            checkBoxEndDate.Checked = false;
            dateTimePickerEnd.Enabled = false;
            dateTimePickerfrom.Value = DateTime.Now;
        }

        private void buttonAddNew_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        private string ConverDate(DateTime date, bool isEndDate)
        {
            if (isEndDate)
            {
                return date.Year + "-" + date.Month + "-" + date.Day;
            }
            else
            {
                return "00-00-00";
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!CheckError())
            {
                return;
            }
            try
            {
                mConnection.Open();
                if (buttonSave.Tag == null)
                {
                    Loyalty loyalt = new Loyalty();
                    GetLoyalTy(loyalt);
                    int ennable = 0;
                    if (loyalt.Enable)
                    {
                        ennable = 1;
                    }
                    mConnection.ExecuteNonQuery(
                            "insert into loyaltybyitem(itemID,name,qty,discount,dateStart,dateEnd,enable) " +
                            "values(" +
                                loyalt.Item.ItemID + ",'" +
                                loyalt.Name + "'," +
                                loyalt.Qty + "," +
                                loyalt.Discount + ",'" +
                                ConverDate(loyalt.DateStart, true) + "','" +
                                ConverDate(loyalt.DateEnd, loyalt.IsEndDate) + "'," +
                                ennable + ")"
                            );
                    loyalt.LoyaltyID = Convert.ToInt32(mConnection.ExecuteScalar("select max(loyaltyID) from loyaltybyitem"));
                    AddLoyaltyToListView(loyalt);
                }
                else
                {
                    if (listView1.SelectedIndices.Count > 0)
                    {
                        Loyalty loyalty = (Loyalty)listView1.SelectedItems[0].Tag;
                        GetLoyalTy(loyalty);
                        int ennable = 0;
                        if (loyalty.Enable)
                        {
                            ennable = 1;
                        }
                        GetLoyalTy(loyalty);
                        UpdateListView(loyalty);
                        mConnection.ExecuteNonQuery(
                                "update loyaltybyitem set " +
                                    "itemID=" + loyalty.Item.ItemID + "," +
                                    "name='" + loyalty.Name + "'" + "," +
                                    "qty=" + loyalty.Qty + "," +
                                    "discount=" + loyalty.Discount + "," +
                            //"discount=" + loyalty.Discount + "," +
                                    "dateStart='" + ConverDate(loyalty.DateStart, true) + "'" + "," +
                                    "dateEnd='" + ConverDate(loyalty.DateEnd, loyalty.IsEndDate) + "'," +
                                    "`enable`=" + ennable + " " +
                                "where loyaltyID=" + loyalty.LoyaltyID
                            );
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void GetLoyalTy(Loyalty loyalty)
        {
            loyalty.Name = textBoxPOSKeyBoardName.Text;
            loyalty.Discount = mMoneyFortmat.getFortMat(textBoxPOSKeyBoardDiscount.Text);
            loyalty.Qty = Convert.ToInt32(textBoxPOSKeyBoardQty.Text);
            loyalty.DateStart = dateTimePickerfrom.Value;
            loyalty.DateEnd = dateTimePickerEnd.Value;
            loyalty.Enable = checkBoxEnable.Checked;
            loyalty.Item = mItem;
            loyalty.IsEndDate = checkBoxEndDate.Checked;
        }

        private void AddLoyaltyToListView(Loyalty loyalty)
        {
            ListViewItem li = new ListViewItem(loyalty.Name);
            li.SubItems.Add(loyalty.Item.ItemName);
            li.SubItems.Add(loyalty.Qty + "");
            li.SubItems.Add(mMoneyFortmat.Format(loyalty.Discount));
            li.SubItems.Add(loyalty.DateStart.Day + "/" + loyalty.DateStart.Month + "/" + loyalty.DateStart.Year);
            if (loyalty.IsEndDate)
            {
                li.SubItems.Add(loyalty.DateEnd.Day + "/" + loyalty.DateEnd.Month + "/" + loyalty.DateEnd.Year);
            }
            else
            {
                li.SubItems.Add("");
            }
            if (loyalty.Enable)
            {
                li.SubItems.Add("Enable");
            }
            else
            {
                li.SubItems.Add("");
            }
            li.Tag = loyalty;
            listView1.Items.Insert(0, li);
            //li.Selected = true;
            buttonSave.Tag = 1;
        }

        private void UpdateListView(Loyalty loyalty)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedItems[0].SubItems[0].Text = loyalty.Name;
                listView1.SelectedItems[0].SubItems[1].Text = loyalty.Item.ItemName;
                listView1.SelectedItems[0].SubItems[2].Text = loyalty.Qty + "";
                listView1.SelectedItems[0].SubItems[3].Text = mMoneyFortmat.Format(loyalty.Discount);
                listView1.SelectedItems[0].SubItems[4].Text = loyalty.DateStart.Day + "/" + loyalty.DateStart.Month + "/" + loyalty.DateStart.Year;
                if (loyalty.IsEndDate)
                {
                    listView1.SelectedItems[0].SubItems[5].Text = loyalty.DateEnd.Day + "/" + loyalty.DateEnd.Month + "/" + loyalty.DateEnd.Year;
                }
                else
                {
                    listView1.SelectedItems[0].SubItems[5].Text = "";
                }
                if (loyalty.Enable)
                {
                    listView1.SelectedItems[0].SubItems[6].Text = "Enable";
                }
                else
                {
                    listView1.SelectedItems[0].SubItems[6].Text = "";
                }
            }
        }

        private bool CheckError()
        {
            labelError.Text = "";
            if (textBoxPOSKeyBoardName.Text == "")
            {
                labelError.Text = "Field Name empty";
                return false;
            }
            if (textBoxPOSKeyBoardItemName.Text == "")
            {
                labelError.Text = "Field ItemName empty";
                return false;
            }
            if (mItem == null)
            {
                labelError.Text = "Item null";
                return false;
            }
            return true;
        }

        private void textBoxPOSKeyBoardQty_TextChanged(object sender, EventArgs e)
        {
            labelError.Text = "";
            if (mIsLockText)
            {
                return;
            }
            mIsLockText = true;

            if (textBoxPOSKeyBoardQty.Text != "")
            {
                if (mItem == null)
                {
                    labelError.Text = "Item empty";
                    return;
                }
                textBoxPOSKeyBoardPrice.Text = mMoneyFortmat.Format(mItem.Price * Convert.ToInt32(textBoxPOSKeyBoardQty.Text));
            }
            textBoxPOSKeyBoardSalesPrice.Text = "";
            textBoxPOSKeyBoardDiscount.Text = "";
            mIsLockText = false;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            frmSearchBarCode frm = new frmSearchBarCode(DataObject.FindItemType.FindItemInOrder);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                mItem = new Item(frm.ItemMenu.ItemID, frm.ItemMenu.ItemName, frm.ItemMenu.Price);
                //LoadListView(mItem.ItemID);
                textBoxPOSKeyBoardItemName.Text = mItem.ItemName;
            }
        }

        private void textBoxPOSKeyBoardItemName_TextChanged(object sender, EventArgs e)
        {
            if (mIsLockText)
            {
                return;
            }
            mIsLockText = true;
            textBoxPOSKeyBoardQty.Text = "";
            mIsLockText = false;
        }

        private void textBoxPOSKeyBoardSalesPrice_TextChanged(object sender, EventArgs e)
        {
            labelError.Text = "";
            if (mIsLockText)
            {
                return;
            }

            if (textBoxPOSKeyBoardSalesPrice.Text != "")
            {
                if (textBoxPOSKeyBoardPrice.Text == "")
                {
                    labelError.Text = "Item empty";
                    return;
                }
                double salePrice = Convert.ToDouble(textBoxPOSKeyBoardSalesPrice.Text);
                double price = Convert.ToDouble(textBoxPOSKeyBoardPrice.Text);
                if (salePrice > price)
                {
                    string s = textBoxPOSKeyBoardSalesPrice.Text;
                    mIsLockText = true;
                    textBoxPOSKeyBoardSalesPrice.Text = s.Remove(s.Length - 1, 1);
                    mIsLockText = false;
                    labelError.Text = "The Sales Price is not greater than Total Price!";
                    return;
                }
                else
                {
                    mIsLockText = true;
                    textBoxPOSKeyBoardDiscount.Text = mMoneyFortmat.FormatCurenMoney(price - salePrice);
                    mIsLockText = false;
                }
            }
        }

        private void textBoxPOSKeyBoardPrice_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBoxPOSKeyBoardDiscount_TextChanged(object sender, EventArgs e)
        {
            labelError.Text = "";
            if (mIsLockText)
            {
                return;
            }
            if (textBoxPOSKeyBoardDiscount.Text != "")
            {
                if (textBoxPOSKeyBoardPrice.Text == "")
                {
                    labelError.Text = "Item empty";
                    return;
                }
                double discount = Convert.ToDouble(textBoxPOSKeyBoardDiscount.Text);
                double price = Convert.ToDouble(textBoxPOSKeyBoardPrice.Text);
                if (discount > price)
                {
                    string s = textBoxPOSKeyBoardDiscount.Text;
                    mIsLockText = true;
                    textBoxPOSKeyBoardDiscount.Text = s.Remove(s.Length - 1, 1);
                    mIsLockText = false;
                    labelError.Text = "The Discount is not greater than Total Price!";
                    return;
                }
                else
                {
                    mIsLockText = true;
                    textBoxPOSKeyBoardSalesPrice.Text = mMoneyFortmat.FormatCurenMoney(price - discount);
                    mIsLockText = false;
                }
            }
            mIsLockText = false;
        }

        private void frmLoyatyControl_FormClosed(object sender, FormClosedEventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            this.DialogResult = DialogResult.Cancel;
        }

        private void buttonViewFullList_Click(object sender, EventArgs e)
        {
            LoadListView(0);
        }

        private void buttonViewItem_Click(object sender, EventArgs e)
        {
            labelError.Text = "";
            if (mItem != null)
            {
                LoadListView(mItem.ItemID);
            }
            else
            {
                labelError.Text = "Item empty!";
            }
        }
    }
}