﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmViewDetailSaleDaily : Form
    {
        public frmViewDetailSaleDaily()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmViewDetailSaleDaily_Load(object sender, EventArgs e)
        {
        }

        public void ViewSaleByGroup(Connection.Connection mConnection, Class.MoneyFortmat money, int intGroupID, string weekfrom, string weekto)
        {
            mConnection.Open();
            string sql = "SELECT itemDesc,SUM(Quantity) AS Quantity,SUM(Total) AS Total " +
                        "from ( " +
                        "SELECT I.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersdailyline O " +
                        "inner join ordersdaily OD on OD.orderID = O.orderID " +
                        "inner join itemsmenu I on I.itemID = O.itemID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND I.groupID = " + intGroupID +
                        " group by I.itemDesc " +
                        "UNION ALL " +
                        "SELECT IL.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersdailyline O " +
                        "inner join ordersdaily OD on OD.orderID = O.orderID " +
                        "inner join itemslinemenu IL on IL.lineID = O.optionID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND IL.groupID = " + intGroupID +
                        " group by IL.itemDesc " +
                        "UNION ALL " +
                        "SELECT DM.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersdailyline O " +
                        "inner join ordersdaily OD on OD.orderID = O.orderID " +
                        "inner join dynitemsmenu DM on DM.dynID = O.dynID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND DM.groupID = " + intGroupID +
                        " group by DM.itemDesc " +
                        "UNION ALL " +
                        "SELECT I.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersallline O " +
                        "inner join ordersall OD on OD.bkId = O.bkId " +
                        "inner join itemsmenu I on I.itemID = O.itemID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND I.groupID = " + intGroupID +
                        " group by I.itemDesc " +
                        "UNION ALL " +
                        "SELECT IL.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersallline O " +
                        "inner join ordersall OD on OD.bkId = O.bkId " +
                        "inner join itemslinemenu IL on IL.lineID = O.optionID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND IL.groupID = " + intGroupID +
                        " group by IL.itemDesc " +
                        "UNION ALL " +
                        "SELECT DM.itemDesc,SUM(O.qty) AS Quantity,SUM(O.subTotal) AS Total " +
                        "FROM ordersallline O " +
                        "inner join ordersall OD on OD.bkId = O.bkId " +
                        "inner join dynitemsmenu DM on DM.dynID = O.dynID " +
                        "where date(OD.ts)>=" + "'" + weekfrom + "'" + " and date(OD.ts)<=" + "'" + weekto + "'" + " and (OD.completed=1 or OD.completed=2) AND DM.groupID = " + intGroupID +
                        " group by DM.itemDesc " +
                        ")AS S " +
                        "group by itemDesc";
            System.Data.DataTable tblSourceSaleInDay = mConnection.Select(sql);
            DateTime dt = DateTime.Now;
            double decTotal = 0;
            foreach (System.Data.DataRow row in tblSourceSaleInDay.Rows)
            {
                ListViewItem li = new ListViewItem(row["itemDesc"].ToString());
                li.SubItems.Add(string.Format("{0:0,0}", Convert.ToDouble(row["Quantity"])));
                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Total"]))));
                decTotal += Convert.ToDouble(row["Total"]);
                lstQuantityReport.Items.Add(li);
            }
            lblTotalSalse.Text = "Total : " + money.Format(decTotal);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstQuantityReport.SelectedIndices.Count > 0)
            {
                lstQuantityReport.SelectedIndices.Clear();
                lstQuantityReport.SelectedIndices.Add(0);
                lstQuantityReport.Items[0].Selected = true;
                lstQuantityReport.Items[0].EnsureVisible();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstQuantityReport.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReport.SelectedIndices[0];
                    lstQuantityReport.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstQuantityReport.SelectedIndices.Add(lstQuantityReport.Items.Count - 1);
                        lstQuantityReport.Items[lstQuantityReport.Items.Count - 1].Selected = true;
                        lstQuantityReport.Items[lstQuantityReport.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReport.SelectedIndices.Add(oldselection - 1);
                        lstQuantityReport.Items[oldselection - 1].Selected = true;
                        lstQuantityReport.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstQuantityReport.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstQuantityReport.SelectedIndices[0];
                    lstQuantityReport.SelectedIndices.Clear();
                    if (oldselection + 2 > lstQuantityReport.Items.Count)
                    {
                        lstQuantityReport.SelectedIndices.Add(0);
                        lstQuantityReport.Items[0].Selected = true;
                        lstQuantityReport.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstQuantityReport.SelectedIndices.Add(oldselection + 1);
                        lstQuantityReport.Items[oldselection + 1].Selected = true;
                        lstQuantityReport.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstQuantityReport.SelectedIndices.Count > 0)
            {
                lstQuantityReport.SelectedIndices.Clear();
                lstQuantityReport.SelectedIndices.Add(lstQuantityReport.Items.Count - 1);
                lstQuantityReport.Items[lstQuantityReport.Items.Count - 1].Selected = true;
                lstQuantityReport.Items[lstQuantityReport.Items.Count - 1].EnsureVisible();
            }
        }
    }
}