﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmCheckServerConnection : Form
    {
        private string _nameClass = "POS::Forms::frmCheckServerConnection::";
        private Connection.Connection mConn;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private int mTime = 0;
        Class.ReadConfig mReadConfig;
        public frmCheckServerConnection(Class.ReadConfig readConfig)
        {
            InitializeComponent();
            mReadConfig = readConfig;
        }

        private void frmCheckServerConnection_Load(object sender, EventArgs e)
        {
            mConn = new Connection.Connection();
            mConn.ChangeLocalConnection(0);
            ConnectionServer();
        }

        private DateTime dtDate;

        private void ConnectionServer()
        {
            bool isSuccess = false;
            lblTimeOut.Text = "Connecting...";
            mConn.SetConnectionString();
            try
            {
                mConn.Open();
                object obj = mConn.ExecuteScalar("select now()");
                if (obj != null)
                {
                    Class.SystemTime.SetTime(Convert.ToDateTime(obj));
                    dtDate = Convert.ToDateTime(obj);
                }
                isSuccess = true;
                timer1.Stop();
            }
            catch (Exception)
            {
                mTime = 10;
                timer1.Start();
            }
            finally
            {
                mConn.Close();
            }
            if (isSuccess)
            {
                UpdateToServer();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

        }

        private void UpdateToServer()
        {
            Connection.Connection lConnection = new Connection.Connection("localhost");
            if (mConn.GetCableID() == "1")
            {
                Class.LogPOS.WriteLog("Update database to all");

                Class.UpdateToServer.UpDatabaseToServer(lConnection, mReadConfig);
                return;
            }
            if (!mConn.CheckLocalhost())
            {
                try
                {
                    lConnection.Open();
                    lConnection.BeginTransaction();
                    lblTimeOut.Text = "Connection server successed! Waiting for us update data...";
                    //string sql = "CALL SP_UpdateMenuData('" + mConn.GetServer() + "', " + lConnection.GetCableID() + ")";
                    //lConnection.ExecuteNonQuery(sql);
                    //lConnection.ExecuteNonQuery("CALL SP_UpdatabaseToServer_all_shift_dynitem_ordersdaily('" + mConn.GetServer() + "', " + lConnection.GetCableID() + ")");
                    lConnection.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "UpdateToServer::" + ex.Message);
                    lConnection.Rollback();
                }
                finally
                {
                    lConnection.Close();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mTime == 0)
            {
                timer1.Stop();
                ConnectionServer();
                //button1_Click(null, null);
            }
            else
            {
                lblTimeOut.Text = "Connection to server failed ! Try to reconnect in " + mTime + " second... \r\nClick \"Using Localhost\" to connect";
                //lblTimeOut.Text = "Connect fail try connection server in " + mTime + " second... ";
                mTime--;
            }
            Console.WriteLine(mTime);
        }

        private void btnUsingLocalhost_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            mConn.ChangeLocalConnection(1);
            ConnectionServer();
        }
    }
}