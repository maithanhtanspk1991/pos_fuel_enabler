﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmProfitAndLossStatement : Form
    {
        private Connection.Connection mConnection;
        private ExportToExcel mExportToExcel;
        private Class.MoneyFortmat money;
        private DataTable dtSourceForExcel;
        private string sql;
        private string strFromDate;
        private string strToDate;

        public frmProfitAndLossStatement()
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            money = new Class.MoneyFortmat(1);
            mExportToExcel = new ExportToExcel();
        }

        private void frmProfitAndLossStatement_Load(object sender, EventArgs e)
        {
            LoadGroupItems();
        }

        private void LoadGroupItems()
        {
            try
            {
                mConnection.Open();
                sql = "SELECT 0 AS groupID,'----ALL GROUP----' AS groupShort " +
                    "UNION ALL " +
                    "SELECT groupID, groupShort " +
                    "FROM groupsmenu " +
                    "WHERE visual = 0 or isFuel = 1";
                DataTable dtSource = mConnection.Select(sql);
                cboGroupItem.DataSource = dtSource;
                cboGroupItem.DisplayMember = "groupShort";
                cboGroupItem.ValueMember = "groupID";
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmProfitAndLossStatement:::LoadWeeklyButton:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void cboGroupItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lvwProfitAndLossList.Items.Clear();
                mConnection.Open();
                strFromDate = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + dateTimePickerfrom.Value.Hour + ":" + dateTimePickerfrom.Value.Minute + ":" + dateTimePickerfrom.Value.Second;
                strToDate = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day + " " + dateTimePickerto.Value.Hour + ":" + dateTimePickerto.Value.Minute + ":" + dateTimePickerto.Value.Second;
                if (lvwProfitAndLossList.SelectedIndices.Count > 0)
                {
                    sql = "CALL SP_PROFIT_AND_LOSS_STATEMENT_BY_GROUP('" + strFromDate + "','" + strToDate + "','" + cboGroupItem.SelectedValue.ToString() + "',0,0,0)";
                    DataTable dtSource = mConnection.Select(sql);
                    foreach (DataRow drItem in dtSource.Rows)
                    {
                        ListViewItem li = new ListViewItem(drItem["ItemName"].ToString());
                        li.SubItems.Add(drItem["ProfitAndLoss"].ToString());
                        li.Tag = drItem["ItemID"].ToString();
                        lvwProfitAndLossList.Items.Add(li);
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboGroupItem_SelectedIndexChanged:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void lvwProfitAndLossList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lvwProfitAndLossList.SelectedIndices.Count > 0)
                {
                    frmForfitAndLossStatementDetail frm = new frmForfitAndLossStatementDetail();
                    frm.FromDate = strFromDate;
                    frm.ToDate = strToDate;
                    frm.ItemID = Convert.ToInt32(lvwProfitAndLossList.SelectedItems[0].Tag);
                    frm.LoadDetailProfitAndLoss();
                    frm.ShowDialog();
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                lvwProfitAndLossList.Items.Clear();
                mConnection.Open();
                strFromDate = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + dateTimePickerfrom.Value.Hour + ":" + dateTimePickerfrom.Value.Minute + ":" + dateTimePickerfrom.Value.Second;
                strToDate = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day + " " + dateTimePickerto.Value.Hour + ":" + dateTimePickerto.Value.Minute + ":" + dateTimePickerto.Value.Second;
                sql = "CALL SP_PROFIT_AND_LOSS_STATEMENT_BY_GROUP('" + strFromDate + "','" + strToDate + "','" + cboGroupItem.SelectedValue.ToString() + "',0,0,0)";
                DataTable dtSource = mConnection.Select(sql);

                dtSourceForExcel = new DataTable();
                dtSourceForExcel.Columns.Add("Item Name", typeof(String));
                dtSourceForExcel.Columns.Add("Profit and loss", typeof(String));

                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(drItem["ItemName"].ToString());
                    li.SubItems.Add("$ " + money.Format(drItem["ProfitAndLoss"].ToString()));
                    li.Tag = drItem["ItemID"].ToString();
                    lvwProfitAndLossList.Items.Add(li);

                    dtSourceForExcel.Rows.Add(
                        drItem["ItemName"].ToString(),
                        "$ " + money.Format(drItem["ProfitAndLoss"].ToString())
                        );
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboGroupItem_SelectedIndexChanged:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            txtNameExport.Text = "";
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtNameExport);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowSaveFileDialog(txtNameExport.Text, "Microsoft Excel Document", "Excel 2003|*.xls");
            }
        }

        public void ShowSaveFileDialog(string filename, string title, string filter)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Export To " + title;
                dlg.FileName = filename;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    mExportToExcel.dataTable2Excel(dtSourceForExcel, dlg.FileName);
                    MessageBox.Show("Export to excel success !");
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.ShowSaveFileDialog:::" + ex.Message);
            }
        }
    }
}