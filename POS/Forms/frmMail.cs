﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMail : Form
    {
        Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmMail()
        {           InitializeComponent();
        }

        private void frmMail_Load(object sender, EventArgs e)
        {
            SetMultiLanguage();
            try
            {
                Class.SendMail sm = new Class.SendMail();
                txtMail.Text = sm.getInfoMail("Mail");
                txtPass.Text = sm.getInfoMail("Pass");
                txtDisplay.Text = sm.getInfoMail("Display");
                txtSubject.Text = sm.getInfoMail("Subject");
                txtSmtp.Text = sm.getInfoMail("SmtpClient");
                txtPort.Text = sm.getInfoMail("Port");
                txtTimeout.Text = sm.getInfoMail("Timeout");
            }
            catch (Exception ex)
            {
                POS.Class.LogPOS.WriteLog("POS.Forms.frmMail::frmMail_Load:Error::" + ex.Message);
            }            
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "CẤU HÌNH GỬI MAIL";
                label1.Text = "Tên hiển thị:";
                label3.Text = "Mật khẩu:";
                label4.Text = "Chủ đề:";
                label5.Text = "Máy khách SMTP:";
                label6.Text = "Cổng:";
                label7.Text = "Hết giờ:";
                btnBack.Text = "QUAY LẠI";
                btnUpdate.Text = "CẬP NHẬT";
                return;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Class.SendMail sm = new Class.SendMail();
                sm.updateInfoMail("Mail", txtMail.Text);
                sm.updateInfoMail("Pass", txtPass.Text);
                sm.updateInfoMail("Display", txtDisplay.Text);
                sm.updateInfoMail("Subject", txtSubject.Text);
                sm.updateInfoMail("SmtpClient", txtSmtp.Text);
                sm.updateInfoMail("Port", txtPort.Text);
                sm.updateInfoMail("Timeout", txtTimeout.Text);
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    MessageBox.Show("HOÀN THÀNH!", "THÔNG TIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                MessageBox.Show("DONE!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);                
            }
            catch (Exception ex)
            {                
                POS.Class.LogPOS.WriteLog("POS.Forms.frmMail::btnUpdate_Click:Error::" + ex.Message);
            }            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
