﻿namespace POS.Forms
{
    partial class frmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.OrderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CustomersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InfoCustomerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PartsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RegoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rvOrder = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.OrderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoCustomerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // OrderBindingSource
            // 
            this.OrderBindingSource.DataSource = typeof(POS.Class.ProcessOrderNew.Order);
            // 
            // CustomersBindingSource
            // 
            this.CustomersBindingSource.DataSource = typeof(DataObject.Customers);
            // 
            // InfoCustomerBindingSource
            // 
            this.InfoCustomerBindingSource.DataSource = typeof(DataObject.InfoCustomer);
            // 
            // PartsBindingSource
            // 
            this.PartsBindingSource.DataSource = typeof(DataObject.Parts);
            // 
            // RegoBindingSource
            // 
            this.RegoBindingSource.DataSource = typeof(DataObject.Rego);
            // 
            // rvOrder
            // 
            this.rvOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Order";
            reportDataSource1.Value = this.OrderBindingSource;
            reportDataSource2.Name = "Customer";
            reportDataSource2.Value = this.CustomersBindingSource;
            reportDataSource3.Name = "Info";
            reportDataSource3.Value = this.InfoCustomerBindingSource;
            reportDataSource4.Name = "Parts";
            reportDataSource4.Value = this.PartsBindingSource;
            reportDataSource5.Name = "Rego";
            reportDataSource5.Value = this.RegoBindingSource;
            this.rvOrder.LocalReport.DataSources.Add(reportDataSource1);
            this.rvOrder.LocalReport.DataSources.Add(reportDataSource2);
            this.rvOrder.LocalReport.DataSources.Add(reportDataSource3);
            this.rvOrder.LocalReport.DataSources.Add(reportDataSource4);
            this.rvOrder.LocalReport.DataSources.Add(reportDataSource5);
            this.rvOrder.LocalReport.ReportEmbeddedResource = "POS.Forms.rpOrder.rdlc";
            this.rvOrder.Location = new System.Drawing.Point(0, 0);
            this.rvOrder.Name = "rvOrder";
            this.rvOrder.Size = new System.Drawing.Size(1232, 733);
            this.rvOrder.TabIndex = 0;
            // 
            // frmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 733);
            this.Controls.Add(this.rvOrder);
            this.Name = "frmReport";
            this.Text = "frmReport";
            this.Load += new System.EventHandler(this.frmReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OrderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoCustomerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvOrder;
        private System.Windows.Forms.BindingSource OrderBindingSource;
        private System.Windows.Forms.BindingSource CustomersBindingSource;
        private System.Windows.Forms.BindingSource InfoCustomerBindingSource;
        private System.Windows.Forms.BindingSource PartsBindingSource;
        private System.Windows.Forms.BindingSource RegoBindingSource;
    }
}