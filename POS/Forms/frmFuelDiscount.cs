﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmFuelDiscount : Form
    {
        private Class.MoneyFortmat mMoneyFortmat;
        private bool mIsLockText = false;

        public bool IsManager
        {
            set { mIsManager = value; }
        }

        private bool mIsManager = false;

        public string ItemName { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public double Total { get; set; }

        public double Discount { get; set; }

        public string DiscountName { get; set; }

        public int DiscountID { get; set; }

        public int ItemKey { get; set; }

        public int ShiftID { get; set; }

        public int Weight { get; set; }

        private Connection.Connection mConnection;

        public frmFuelDiscount(Connection.Connection con, Class.MoneyFortmat money)
        {
            InitializeComponent();
            mConnection = con;
            mMoneyFortmat = money;
        }

        private void frmFuelDiscount_Load(object sender, EventArgs e)
        {
            int[] listDiscount = Class.FuelDiscountConfig.GetListFuelDiscount();
            if (listDiscount != null)
            {
                //int a = (int)Math.Sqrt(listDiscount.Length);
                int a = (int)Math.Sqrt(listDiscount.Length);
                int b = listDiscount.Length / a;
                if (listDiscount.Length % a > 0)
                {
                    b++;
                }
                tableLayoutPanel1.ColumnCount = b;
                tableLayoutPanel1.RowCount = a;
                for (int i = 0; i < listDiscount.Length; i++)
                {
                    Button btn = new Button();
                    btn.FlatStyle = FlatStyle.Flat;
                    btn.FlatAppearance.BorderSize = 0;
                    btn.ForeColor = System.Drawing.Color.White;
                    btn.BackColor = System.Drawing.Color.FromArgb(0, 170, 130);
                    btn.Text = "Discount " + listDiscount[i] + " cents per litre";
                    btn.Width = 100;
                    btn.Height = 100;
                    btn.Dock = DockStyle.Fill;
                    btn.Font = new System.Drawing.Font(btn.Font.FontFamily, 13);
                    btn.Tag = listDiscount[i];                    
                    btn.Click += new EventHandler(btn_Click);
                    tableLayoutPanel1.Controls.Add(btn);
                }
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                DiscountName = btn.Text;
                double maxQtyDiscount = Class.FuelDiscountConfig.GetMaxQtyDiscount();
                if (this.Qty > maxQtyDiscount && maxQtyDiscount > 0)
                {
                    this.Qty = (int)maxQtyDiscount;
                }
                Discount = mMoneyFortmat.getFortMat(Convert.ToDouble(btn.Tag) / 100);
                Discount = (double)this.Total / this.Weight * Discount;
                mConnection.ExecuteNonQuery("insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice) value(" + ShiftID + ",'" + DiscountName + "','" + DiscountName + "'," + (-1 * Discount) + ")");
                DiscountID = Convert.ToInt32(mConnection.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}