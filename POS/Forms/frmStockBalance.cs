﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmStockBalance : Form
    {
        public frmStockBalance()
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            money = new Class.MoneyFortmat(1);
            mExportToExcel = new ExportToExcel();
        }

        private ExportToExcel mExportToExcel;
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private DataTable dtSourceForExcel;
        private string sql;
        private string strFromDate;
        private string strToDate;

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                lvwStockBalance.Items.Clear();
                mConnection.Open();
                strFromDate = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + dateTimePickerfrom.Value.Hour + ":" + dateTimePickerfrom.Value.Minute + ":" + dateTimePickerfrom.Value.Second;
                strToDate = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day + " " + dateTimePickerto.Value.Hour + ":" + dateTimePickerto.Value.Minute + ":" + dateTimePickerto.Value.Second;
                sql = "CALL SP_STOCK_CARD_MOVEMENT_BY_GROUP('" + strFromDate + "','" + strToDate + "','" + cboGroupItem.SelectedValue.ToString() + "',0,0,0)";
                DataTable dtSource = mConnection.Select(sql);

                dtSourceForExcel = new DataTable();
                dtSourceForExcel.Columns.Add("Item Name", typeof(String));
                dtSourceForExcel.Columns.Add("Stock Start", typeof(String));
                dtSourceForExcel.Columns.Add("Stock Receive", typeof(String));
                dtSourceForExcel.Columns.Add("Stock Transfer", typeof(String));
                dtSourceForExcel.Columns.Add("Stock End", typeof(String));

                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(drItem["ItemName"].ToString());
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["BeginningPcs"].ToString() == "" ? "0" : drItem["BeginningPcs"].ToString())));
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["StockInPcs"].ToString() == "" ? "0" : drItem["StockInPcs"].ToString())));
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["StockOutPcs"].ToString() == "" ? "0" : drItem["StockOutPcs"].ToString())));
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["EndingPcs"].ToString() == "" ? "0" : drItem["EndingPcs"].ToString())));

                    dtSourceForExcel.Rows.Add(
                        drItem["ItemName"].ToString(),
                        money.getFortMat3(Convert.ToDouble(drItem["BeginningPcs"].ToString() == "" ? "0" : drItem["BeginningPcs"].ToString())),
                        money.getFortMat3(Convert.ToDouble(drItem["StockInPcs"].ToString() == "" ? "0" : drItem["StockInPcs"].ToString())),
                        money.getFortMat3(Convert.ToDouble(drItem["StockOutPcs"].ToString() == "" ? "0" : drItem["StockOutPcs"].ToString())),
                        money.getFortMat3(Convert.ToDouble(drItem["EndingPcs"].ToString() == "" ? "0" : drItem["EndingPcs"].ToString()))
                        );

                    li.Tag = drItem["ItemID"].ToString();
                    lvwStockBalance.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboGroupItem_SelectedIndexChanged:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void cboGroupItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lvwStockBalance.Items.Clear();
                mConnection.Open();
                strFromDate = dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + " " + dateTimePickerfrom.Value.Hour + ":" + dateTimePickerfrom.Value.Minute + ":" + dateTimePickerfrom.Value.Second;
                strToDate = dateTimePickerto.Value.Year + "-" + dateTimePickerto.Value.Month + "-" + dateTimePickerto.Value.Day + " " + dateTimePickerto.Value.Hour + ":" + dateTimePickerto.Value.Minute + ":" + dateTimePickerto.Value.Second;
                if (lvwStockBalance.SelectedIndices.Count > 0)
                {
                    sql = "CALL SP_STOCK_CARD_MOVEMENT_BY_GROUP('" + strFromDate + "','" + strToDate + "','" + cboGroupItem.SelectedValue.ToString() + "',0,0,0)";
                    DataTable dtSource = mConnection.Select(sql);
                    foreach (DataRow drItem in dtSource.Rows)
                    {
                        ListViewItem li = new ListViewItem(drItem["ItemName"].ToString());
                        li.SubItems.Add(money.Format(drItem["BeginningPcs"].ToString()));
                        li.SubItems.Add(money.Format(drItem["StockInPcs"].ToString()));
                        li.SubItems.Add(money.Format(drItem["StockOutPcs"].ToString()));
                        li.SubItems.Add(money.Format(drItem["EndingPcs"].ToString()));

                        dtSourceForExcel.Rows.Add(
                            drItem["ItemName"].ToString(),
                            money.getFortMat3(Convert.ToDouble(drItem["BeginningPcs"].ToString())),
                            money.getFortMat3(Convert.ToDouble(drItem["StockInPcs"].ToString())),
                            money.getFortMat3(Convert.ToDouble(drItem["StockOutPcs"].ToString())),
                            money.getFortMat3(Convert.ToDouble(drItem["EndingPcs"].ToString()))
                        );

                        li.Tag = drItem["ItemID"].ToString();
                        lvwStockBalance.Items.Add(li);
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboGroupItem_SelectedIndexChanged:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void frmStockBalance_Load(object sender, EventArgs e)
        {
            LoadGroupItems();
        }

        private void LoadGroupItems()
        {
            try
            {
                mConnection.Open();
                sql = "SELECT 0 AS groupID,'----ALL GROUP----' AS groupShort " +
                    "UNION ALL " +
                    "SELECT groupID, groupShort " +
                    "FROM groupsmenu " +
                    "WHERE visual = 0 or isFuel = 1";
                DataTable dtSource = mConnection.Select(sql);
                cboGroupItem.DataSource = dtSource;
                cboGroupItem.DisplayMember = "groupShort";
                cboGroupItem.ValueMember = "groupID";
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmProfitAndLossStatement:::LoadWeeklyButton:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void lvwProfitAndLossList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lvwStockBalance.SelectedIndices.Count > 0)
                {
                    frmStockBalanceDetail frm = new frmStockBalanceDetail(lvwStockBalance.Items[0].Text.ToString());
                    frm.FromDate = strFromDate;
                    frm.ToDate = strToDate;
                    frm.ItemID = Convert.ToInt32(lvwStockBalance.SelectedItems[0].Tag);
                    frm.LoadDetailStockBalance();
                    frm.ShowDialog();
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            txtNameExport.Text = "";
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtNameExport);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowSaveFileDialog(txtNameExport.Text, "Microsoft Excel Document", "Excel 2003|*.xls");
            }
        }

        public void ShowSaveFileDialog(string filename, string title, string filter)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Export To " + title;
                dlg.FileName = filename;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    mExportToExcel.dataTable2Excel(dtSourceForExcel, dlg.FileName);
                    MessageBox.Show("Export to excel success !");
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.ShowSaveFileDialog:::" + ex.Message);
            }
        }
    }
}