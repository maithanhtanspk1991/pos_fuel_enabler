﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmHistoryReceiveStock : Form
    {
        private SystemConfig.DBConfig mDBConfig;

        public frmHistoryReceiveStock(SystemConfig.DBConfig dBConfig)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
        }

        private void frmHistoryReceiveStock_Load(object sender, EventArgs e)
        {
            SetSizeListView();
            SetTime();
            LoadReceive();
        }

        #region LoadData

        private void SetTime()
        {
            dtpStart.Value = DateTime.Now.AddDays(-7);
            dtpEnd.Value = DateTime.Now;
        }

        private void SetSizeListView()
        {
            int Width = lvReceive.Width;
            lvReceive.Columns[0].Width = 20 * Width / 100;
            lvReceive.Columns[1].Width = 13 * Width / 100;
            lvReceive.Columns[2].Width = 20 * Width / 100;
            lvReceive.Columns[3].Width = 20 * Width / 100;
            lvReceive.Columns[4].Width = 10 * Width / 100;
            lvReceive.Columns[5].Width = 15 * Width / 100;

            Width = lvReceiveLine.Width;
            lvReceiveLine.Columns[0].Width = 50 * Width / 100;
            lvReceiveLine.Columns[1].Width = 15 * Width / 100;
            lvReceiveLine.Columns[2].Width = 15 * Width / 100;
            lvReceiveLine.Columns[3].Width = 15 * Width / 100;
        }

        private void LoadReceive()
        {
            lvReceive.Items.Clear();
            List<DataObject.Receive> lsArray = new List<DataObject.Receive>();
            lsArray = BusinessObject.BOReceive.GetReceive(-1, -1, -1, dtpStart.Value, dtpEnd.Value, mDBConfig);
            int ReceiveID = 0;
            foreach (DataObject.Receive item in lsArray)
            {
                ListViewItem li = new ListViewItem(DataObject.DateTimeFormat.DisplayDate(item.Date));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.SubTotal));
                li.SubItems.Add(item.StaffName);
                li.SubItems.Add(item.Suppliers.SupplierName);
                li.SubItems.Add(item.CableID.ToString());
                li.SubItems.Add(item.TicketID);
                li.Tag = item;
                if (ReceiveID == 0)
                    ReceiveID = item.ID;
                lvReceive.Items.Add(li);
            }
            if (ReceiveID != 0)
                LoadReceiveLine(ReceiveID);
        }

        private void LoadReceiveLine(int ReceiveID)
        {
            lvReceiveLine.Items.Clear();
            List<DataObject.ReceiveLine> lsArray = new List<DataObject.ReceiveLine>();
            lsArray = BusinessObject.BOReceiveLine.GetReceiveLine(ReceiveID, -1, mDBConfig);
            foreach (DataObject.ReceiveLine item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.ItemsMenu.ItemShort);
                li.SubItems.Add(DataObject.MonneyFormat.FormatNumber(item.Qty));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Price));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Total));
                li.Tag = item;
                lvReceiveLine.Items.Add(li);
            }
        }

        #endregion LoadData

        private void btnView_Click(object sender, EventArgs e)
        {
            LoadReceive();
        }

        private void lvReceive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                DataObject.Receive item = (DataObject.Receive)lvReceive.SelectedItems[0].Tag;
                LoadReceiveLine(item.ID);
            }
        }

        private void buttonExit1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}