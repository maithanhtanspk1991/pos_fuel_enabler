﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmViewDetailCreditAccount : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private string strMemberNo;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmViewDetailCreditAccount(Connection.Connection mConnection, Class.MoneyFortmat money, string strMemberNo)
        {
            InitializeComponent();
            this.mConnection = mConnection;
            mPrinter = new Printer();
            this.money = money;
            this.strMemberNo = strMemberNo;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Xem chi tiết tài khoản tín dụng";
                label6.Text = "TỪ";
                label8.Text = "ĐẾN";
                button5.Text = "Đóng";
                button6.Text = "In";
                columnHeader4.Text = "Ngày phục vụ";
                columnHeader3.Text = "Nhân viên phục vụ";
                columnHeader5.Text ="Thanh toán";
                return;
            }
        }

        public void ViewDetailAccountCredit(string weekfrom, string weekto)
        {
            mConnection.Open();
            string sql = "SELECT c.memberNo,a.PayDate,a.cash + a.card AS Subtotal,if(s.name is null,'Manager',s.name) AS EmployeeName " +
                        "FROM accountpayment a " +
                        "inner join customers c on a.CustomerNo = c.memberNo " +
                        "left join staffs s on a.EmployeeID = s.stfId " +
                        "where a.isCredit = 1 and a.CustomerNo = '" + strMemberNo + "'";
            System.Data.DataTable tblSourceSaleInDay = mConnection.Select(sql);
            DateTime dt = DateTime.Now;
            double decTotal = 0;
            foreach (System.Data.DataRow row in tblSourceSaleInDay.Rows)
            {
                ListViewItem li = new ListViewItem(Convert.ToDateTime(row["PayDate"]).ToString());
                li.SubItems.Add(row["EmployeeName"].ToString());
                li.SubItems.Add(string.Format("{0:0,0}", money.Format(Convert.ToDouble(row["Subtotal"]))));
                decTotal += Convert.ToDouble(row["Subtotal"]);
                lstAccountDetail.Items.Add(li);
            }
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                lblTotal2.Text = "Tổng số tài khoản : " + money.Format(decTotal);
                return;
            }
            lblTotal2.Text = "Total Account : " + money.Format(decTotal);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(0);
                lstAccountDetail.Items[0].Selected = true;
                lstAccountDetail.Items[0].EnsureVisible();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstAccountDetail.SelectedIndices[0];
                    lstAccountDetail.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        lstAccountDetail.SelectedIndices.Add(lstAccountDetail.Items.Count - 1);
                        lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].Selected = true;
                        lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        lstAccountDetail.SelectedIndices.Add(oldselection - 1);
                        lstAccountDetail.Items[oldselection - 1].Selected = true;
                        lstAccountDetail.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = lstAccountDetail.SelectedIndices[0];
                    lstAccountDetail.SelectedIndices.Clear();
                    if (oldselection + 2 > lstAccountDetail.Items.Count)
                    {
                        lstAccountDetail.SelectedIndices.Add(0);
                        lstAccountDetail.Items[0].Selected = true;
                        lstAccountDetail.Items[0].EnsureVisible();
                    }
                    else
                    {
                        lstAccountDetail.SelectedIndices.Add(oldselection + 1);
                        lstAccountDetail.Items[oldselection + 1].Selected = true;
                        lstAccountDetail.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lstAccountDetail.SelectedIndices.Count > 0)
            {
                lstAccountDetail.SelectedIndices.Clear();
                lstAccountDetail.SelectedIndices.Add(lstAccountDetail.Items.Count - 1);
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].Selected = true;
                lstAccountDetail.Items[lstAccountDetail.Items.Count - 1].EnsureVisible();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmViewDetailDeptAccount_Load(object sender, EventArgs e)
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("Print Report");
            try
            {
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.button1_Click::" + ex.Message);
            }
        }

        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Class.ProcessOrderNew.Order mOrder;
        private string m_table;
        private string sql;

        private List<Class.ItemOrderK> getOrderBill(int table, int orderID)
        {
            List<Class.ItemOrderK> result = new List<Class.ItemOrderK>();
            try
            {
                //string sql =
                //        "select o.clients," +
                //            "ol.subTotal," +
                //            "o.discount," +
                //            "o.orderID," +
                //            "if(ol.itemID<>0,ol.itemID,ol.dynID) as itemID," +
                //            "ol.optionID," +
                //            "(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as grShortCut," +
                //            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                //            "ol.qty " +
                //        "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                //        "where o.completed<>2 and o.completed<>1 and o." + "'" + m_table + "'";
                //string sql = "select o.discount,o.orderID,ol.itemID,ol.subTotal,ol.optionID,(select i.optionEnable+g.grOption from groupsmenu g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as optionEnable,(select itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as itemOption,ol.qty from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID where o.completed<>1 and o.completed<>2 and o.tableID=" + table;

                if (orderID != 0)
                {
                    //sql = "select o.discount,o.orderID,ol.itemID,ol.subTotal,ol.optionID,(select i.optionEnable+g.grOption from groupsmenu g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) as optionEnable,(select itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as itemOption,ol.qty from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID where o.orderID=" + orderID;
                    sql =
                        "select " +
                            "o.clients," +
                            "ol.subTotal," +
                            "o.discount," +
                            "o.orderID," +
                            "o.IsDelivery," +
                            "o.IsPickup," +
                            "ol.dynID," +
                            "ol.dynID," +
                            "ol.itemID," +
                            "ol.optionID," +
                            "if((select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID) is null,2,(select g.grShortCut from groupsmenu  g inner join itemsmenu i on g.groupID=i.groupID where i.itemID=ol.itemID)) as grShortCut," +
                            "if(itemID<>0,(select itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.optionID<>0,(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID),(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID))) as itemDesc," +
                            "ol.qty " +
                        "from ordersall o inner join ordersallline ol on o.orderID=ol.orderID " +
                        "where o.orderID=" + orderID + " AND o.tableID = " + table;
                }
                System.Data.DataTable tbl = mConnection.Select(sql);
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    if (row["dynID"].ToString() != "0")
                    {
                        result.Add(new Class.ItemOrderK(Convert.ToInt32(row["dynID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 1, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                    }
                    else
                    {
                        if (row["itemID"].ToString() != "0" && (row["grShortCut"].ToString() != "1"))
                        {
                            result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                        }
                        else
                        {
                            if (result.Count == 0 || result[result.Count - 1].GRShortCut == 1)
                            {
                                result.Add(new Class.ItemOrderK(Convert.ToInt32(row["itemID"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["qty"].ToString()), row["itemDesc"].ToString(), row["orderID"].ToString(), 0, Convert.ToDouble(row["discount"].ToString()), Convert.ToInt32(row["clients"].ToString()), Convert.ToInt32(row["grShortCut"].ToString()), 0, row["IsDelivery"].ToString(), row["IsPickup"].ToString()));
                            }
                            else
                            {
                                result[result.Count - 1].Option.Add(new Class.ItemOptionID(row["itemDesc"].ToString(), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToInt32(row["optionID"].ToString()), Convert.ToInt32(row["itemID"].ToString()), Convert.ToInt32(row["qty"].ToString())));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("BarPrinterServer.getOrderBill:::" + ex.Message);
            }
            //return ProcessList(result);
            return result;
        }

        private string header;
        private string bankCode;
        private string address;
        private string tell;

        private string website;
        private string thankyou;

        private void printDocument_PrintPageDetailAccount(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;

            //string header = "BECAS POS DEMO";
            //string bankCode = "ABN. 1234567890";
            //string address = "133 Alexander Street, Crows Nest 2065";
            //string tell = "T:94327867 M:0401950967";

            try
            {
                header = dbconfig.Header1.ToString();
                bankCode = dbconfig.Header2.ToString();
                address = dbconfig.Header3.ToString();
                tell = dbconfig.Header4.ToString();
                website = dbconfig.FootNode1.ToString();
                thankyou = dbconfig.FootNode2.ToString();
            }
            catch
            {
            }

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += 100;

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;
            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            //if (!m_table.Contains("TKA-") && m_table != "")
            //{
            //    mPrinter.DrawString("TABLE# " + m_table, e, font11, l_y, 3);
            //}
            List<Class.ItemOrderK> list = getOrderBill(tableID, orderID);
            if (list.Count <= 0)
            {
                e.Cancel = true;
                return;
            }
            l_y = mPrinter.DrawString("ORDER# " + list[0].OrderID, e, font11, l_y, 1);
            l_y = mPrinter.DrawString("OPERATOR# " + frmLogin.strEmployeeName, e, font11, l_y, 1);

            l_y += 100;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;
            string sBuilt = "BILL";
            l_y = mPrinter.DrawString(sBuilt, e, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), l_y, 2);

            l_y += 100;

            double subTotal = 0;
            foreach (Class.ItemOrderK item in list)
            {
                l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);
                float yStart = l_y;
                l_y = mPrinter.DrawString(item.Qty + new String(' ', 3) + item.Name, e, font11, l_y, 1);
                foreach (Class.ItemOptionID option in item.Option)
                {
                    l_y = mPrinter.DrawString(new String(' ', 6) + option.Name + " (" + option.Qty + ")", e, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Italic), l_y, 1);
                    item.Price += option.Price;
                }
                mPrinter.DrawString(money.Format(item.Price), e, new System.Drawing.Font("Arial", 11), yStart, 3);
                subTotal += item.Price;
            }
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, l_y, 1);

            l_y += 50;

            System.Drawing.Font fontTotal = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold);
            mPrinter.DrawString("Total:", e, fontTotal, l_y, 1);
            l_y = mPrinter.DrawString(money.Format(mOrder.SubTotal), e, fontTotal, l_y, 3);
            l_y += 50;

            if (mOrder.Discount > 0)
            {
                l_y += 50;
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Discount:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.Discount), e, fontTotal, l_y, 3);

                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Balance:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.SubTotal - mOrder.Discount), e, fontTotal, l_y, 3);
            }

            fontTotal = new System.Drawing.Font("Arial", 11);
            mPrinter.DrawString("GST(included in total):", e, fontTotal, l_y, 1);
            l_y = mPrinter.DrawString(money.Format((mOrder.SubTotal - mOrder.Discount) / 11), e, fontTotal, l_y, 3);

            if (mOrder.Deposit > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Deposit:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.Deposit), e, fontTotal, l_y, 3);
            }

            if (mOrder.Card > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Card:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.Card), e, fontTotal, l_y, 3);
            }

            if (mOrder.Account > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Account:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.Account), e, fontTotal, l_y, 3);
            }

            if (mOrder.Tendered > 0)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Tendered:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(mOrder.Tendered) + "", e, fontTotal, l_y, 3);
            }

            double change = mOrder.Tendered + mOrder.Deposit + mOrder.Card - (mOrder.SubTotal - mOrder.Discount);

            if (change > 0.01)
            {
                fontTotal = new System.Drawing.Font("Arial", 11);
                mPrinter.DrawString("Change:", e, fontTotal, l_y, 1);
                l_y = mPrinter.DrawString(money.Format(Math.Abs(change)), e, fontTotal, l_y, 3);
            }

            l_y += 200;

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;

            l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
        }

        private string[] strOrder;
        private int orderID;
        private int tableID;

        private void lstAccountDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstAccountDetail.SelectedIndices.Count > 0)
                {
                    strOrder = lstAccountDetail.SelectedItems[0].Tag.ToString().Split('-');
                    orderID = Convert.ToInt32(strOrder[0]);
                    tableID = Convert.ToInt32(strOrder[1]);
                    mSetting = new Class.Setting();
                    mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDetailAccount);
                }
            }
            catch
            {
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            string weekfrom = SplitDateFrom(Convert.ToDateTime(dateTimePickerfrom.Value.ToString()));
            string weekto = SplitDateTo(Convert.ToDateTime(dateTimePickerto.Value.ToString()));
            lstAccountDetail.Items.Clear();
            ViewDetailAccountCredit(weekfrom, weekto);
        }

        private string SplitDateTo(DateTime date)
        {
            if (date.Day == DateTime.Now.Day)
            {
                date = DateTime.Now;
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
            }
            else
            {
                string strdate = date.ToShortDateString();
                string strtime = date.ToLongTimeString();
                string[] listdate = strdate.Split('/');
                string[] listtime = strtime.Split(':');
                if (listdate[0].ToString().Length == 1)
                {
                    string tam = listdate[0].ToString();
                    string str = "0" + tam;
                    listdate[0] = str;
                }
                if (listdate[1].ToString().Length == 1)
                {
                    string dtam = listdate[1].ToString();
                    string dstr = "0" + dtam;
                    listdate[1] = dstr;
                }
                return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "23:59:59";
            }
        }

        private string SplitDateFrom(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + "00:00:00";
        }
    }
}