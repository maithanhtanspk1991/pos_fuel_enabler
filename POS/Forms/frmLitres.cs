﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLitres : Form
    {
        frmOrdersAll frm;
        ListView lst;
        private Class.MoneyFortmat money;
        Class.ProcessOrderNew.Item item;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmLitres(ListView _lst, Class.MoneyFortmat mMoney, frmOrdersAll m_frm, Class.ProcessOrderNew.Item m_item)
        {
            InitializeComponent();
            SetMultiLanguage();
            lst = _lst;
            frm = m_frm;
            money = mMoney;
            item = m_item;
            this.txtprice.Text = money.Format(item.Price);
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Lít";
                label1.Text = "Lít";
                label2.Text = "Giá";
                label3.Text = "Thành tiền";
                btncancel.Text = "Thoát";
                return;
            }
        }
        private void CheckSubTotalbyQty(double unit)
        {
            double unitprice = unit;
            double qty = 0;
            double subtotal = 0;
            if (txtLitres.Text != "")
            {
                try
                {
                    qty = Convert.ToDouble(txtLitres.Text);
                    subtotal = qty * unitprice;
                    txtsubtotal.Text = money.FormatCurenMoney(subtotal);
                    
                }
                catch (Exception ex) 
                {
                    Class.LogPOS.WriteLog("frmLitres::CheckSubTotalbyQty:::" + ex.Message);
                }
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtLitres.Text == "")
                {
                    return;
                }
                int weight = 1000;
                double w = Convert.ToDouble(this.txtLitres.Text);
                item.Weight = weight;
                item.Qty = (int)(w * weight);
                item.SubTotal = Convert.ToDouble(this.txtsubtotal.Text) * 1000;
                item.Liter = 1;
                item.IsFuel = true;
                frm.AddNewItems(item);
                this.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmLitres::btnOK_Click::" + ex.Message);
            }
        }

        private void txtLitres_TextChanged(object sender, EventArgs e)
        {
            this.CheckSubTotalbyQty(Convert.ToDouble(txtprice.Text));
            
           // int d = w * 1000;
            
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
