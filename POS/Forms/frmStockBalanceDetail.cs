﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmStockBalanceDetail : Form
    {
        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public int ItemID { get; set; }

        private Connection.Connection mConnection;
        private Class.MoneyFortmat money;
        private ExportToExcel mExportToExcel;
        private DataTable dtSourceForExcel;
        private string sql;
        private string strNameItem;

        public frmStockBalanceDetail()
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            money = new Class.MoneyFortmat(1);
            mExportToExcel = new ExportToExcel();
        }

        public frmStockBalanceDetail(string strNameItem)
        {
            InitializeComponent();
            mConnection = new Connection.Connection();
            money = new Class.MoneyFortmat(1);
            mExportToExcel = new ExportToExcel();
            lblItemName.Text = strNameItem;
        }

        public void LoadDetailStockBalance()
        {
            try
            {
                mConnection.Open();
                sql = "CALL SP_STOCK_CARD_MOVEMENT('" + FromDate + "','" + ToDate + "','" + ItemID + "')";
                DataTable dtSource = mConnection.Select(sql);

                dtSourceForExcel = new DataTable();
                dtSourceForExcel.Columns.Add("Description", typeof(String));
                dtSourceForExcel.Columns.Add("Datetime Receive And Transfer", typeof(String));
                dtSourceForExcel.Columns.Add("Stock Receive", typeof(String));
                dtSourceForExcel.Columns.Add("Stock Transfer", typeof(String));
                dtSourceForExcel.Columns.Add("Stock End", typeof(String));

                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(drItem["Description"].ToString());
                    li.SubItems.Add(drItem["DatetimeInOut"].ToString());
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["StockInPcs"].ToString() == "" ? "0" : drItem["StockInPcs"].ToString())));
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["StockOutPcs"].ToString() == "" ? "0" : drItem["StockOutPcs"].ToString())));
                    li.SubItems.Add(money.getFortMat3(Convert.ToDouble(drItem["EndingPcs"].ToString() == "" ? "0" : drItem["EndingPcs"].ToString())));

                    dtSourceForExcel.Rows.Add(
                        drItem["Description"].ToString(),
                        drItem["DatetimeInOut"].ToString(),
                        money.getFortMat3(Convert.ToDouble(drItem["StockInPcs"].ToString() == "" ? "0" : drItem["StockInPcs"].ToString())),
                        money.getFortMat3(Convert.ToDouble(drItem["StockOutPcs"].ToString() == "" ? "0" : drItem["StockOutPcs"].ToString())),
                        money.getFortMat3(Convert.ToDouble(drItem["EndingPcs"].ToString() == "" ? "0" : drItem["EndingPcs"].ToString()))
                        );

                    lvwStockBalanceListDetail.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmProfitAndLossStatement:::LoadWeeklyButton:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            txtNameExport.Text = "";
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtNameExport);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowSaveFileDialog(txtNameExport.Text, "Microsoft Excel Document", "Excel 2003|*.xls");
            }
        }

        public void ShowSaveFileDialog(string filename, string title, string filter)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Export To " + title;
                dlg.FileName = filename;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    mExportToExcel.dataTable2Excel(dtSourceForExcel, dlg.FileName);
                    MessageBox.Show("Export to excel success !");
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCReportDaily.ShowSaveFileDialog:::" + ex.Message);
            }
        }

        private void frmStockBalanceDetail_Load(object sender, EventArgs e)
        {
        }
    }
}