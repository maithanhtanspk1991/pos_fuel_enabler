﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmCreateStaff : Form
    {
        private Connection.Connection conn = new Connection.Connection();
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmCreateStaff()
        {
            InitializeComponent();
            SetMultiLanguage();
        }

        public class Staff
        {
            public int StfID { get; set; }

            public string StaffName { get; set; }

            public string StaffID { get; set; }

            public string Password { get; set; }

            public int Permission { get; set; }

            public Staff(int stfid, string staffname, string staffid, string password, int permission)
            {
                StfID = stfid;
                StaffName = staffname;
                StaffID = staffid;
                Password = password;
                Permission = permission;
            }
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnDelete.Text = "Xóa";
                btnNew.Text = "Tạo mới";
                btnSaveNew.Text = "Lưu lại";
                btnUpdate.Text = "Sửa";
                button8.Text = "Quay lại";
                button8.Font = new Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                button8.TextAlign = ContentAlignment.MiddleCenter;
                columnHeader1.Text = "Mã nhân viên";
                columnHeader2.Text = "Tên nhân viên";
                columnHeader3.Text = "Quyền hạn";
                label1.Text = "Mã nhân viên:";
                label2.Text = "Mật khẩu:";
                label3.Text = "Xác thực mật khẩu:";
                label4.Text = "Tên nhân viên:";
                label6.Text = "Quyền hạn:";
                return;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmCreateStaff_Load(object sender, EventArgs e)
        {
            GetStaff();
        }

        private DataTable GetStaff()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from staffs";
                conn.Open();
                dt = conn.Select(sql);
                conn.Close();
                lvStaff.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    Staff staff = new Staff(Convert.ToInt32(row["stfId"].ToString()), row["Name"].ToString(), row["staffID"].ToString(), row["password"].ToString(), Convert.ToInt32(row["permission"].ToString()));
                    ListViewItem lvi = new ListViewItem(staff.StaffID);
                    lvi.SubItems.Add(staff.StaffName);
                    string permiss = "";
                    if (staff.Permission == 1)
                    {
                        //if (mReadConfig.LanguageCode.Equals("vi"))
                        //{
                        //    permiss = "Nhân viên bình thường";
                        //}
                        permiss = "General Staff";
                    }
                    else if (staff.Permission == 2)
                    {
                        //if (mReadConfig.LanguageCode.Equals("vi"))
                        //{
                        //    permiss = "Giám sát viên";
                        //}
                        permiss = "Supervisor";
                    }
                    else if (staff.Permission == 3)
                    {
                        //if (mReadConfig.LanguageCode.Equals("vi"))
                        //{
                        //    permiss = "Quản lý";
                        //}
                        permiss = "Manager";
                    }
                    lvi.SubItems.Add(permiss);
                    lvi.Tag = staff;
                    lvStaff.Items.Add(lvi);
                }
                return dt;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmCreateStaff:::GetStaff:::" + ex.Message);
                return dt;
            }
        }

        private void lvStaff_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvStaff.SelectedIndices.Count > 0)
            {
                Staff staff = (Staff)lvStaff.SelectedItems[0].Tag;
                txtName.Text = staff.StaffName;
                txtStaffId.Text = staff.StaffID;
                if (staff.Permission == 1)
                {
                    //if (mReadConfig.LanguageCode.Equals("vi"))
                    //{
                    //    cbbPermission.Text = "Nhân viên bình thường";
                    //    return;
                    //}
                    cbbPermission.Text = "General Staff";
                }
                else if (staff.Permission == 2)
                {
                    //if (mReadConfig.LanguageCode.Equals("vi"))
                    //{
                    //    cbbPermission.Text = "Giám sát viên";
                    //    return;
                    //}
                    cbbPermission.Text = "Supervisor";
                }
                else if (staff.Permission == 3)
                {
                    //if (mReadConfig.LanguageCode.Equals("vi"))
                    //{
                    //    cbbPermission.Text = "Quản lý";
                    //    return;
                    //}
                    cbbPermission.Text = "Manager";
                }
                btnDelete.Enabled = true;
                btnUpdate.Enabled = true;
                txtConfirmPassword.Text = "";
                txtPassword.Text = "";
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            txtPassword.Text = "";
            txtConfirmPassword.Text = "";
            txtStaffId.Text = "";
            cbbPermission.Text = "";
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnSaveNew.Enabled = true;
        }

        private bool CheckPass(string str1, string str2)
        {
            if (String.Compare(str1, str2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckStaffID(string staffid)
        {
            string sql = "select count(staffID) from staffs where staffID=" + staffid;
            conn.Open();
            int count = Convert.ToInt32(conn.ExecuteScalar(sql).ToString());
            conn.Close();
            if (count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void AddStaff(string staffid, string pass, string name, string per)
        {
            try
            {
                string permiss = "";
                if (String.Compare(per, "General Staff") == 0)
                {
                    permiss = "1";
                }
                else if (String.Compare(per, "Supervisor") == 0)
                {
                    permiss = "2";
                }
                else if (String.Compare(per, "Manager") == 0)
                {
                    permiss = "3";
                }
                string sql = "insert into staffs(`staffID`,`password`,`permission`,`Name`) values(" + "'" + txtStaffId.Text + "',password('" + txtPassword.Text + "'),'" + permiss + "','" + txtName.Text + "')";
                conn.Open();
                conn.ExecuteNonQuery(sql);
                conn.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmCreateStaff:::AddStaff:::" + ex.Message);
            }
        }

        private void DeleteStaff(string stfid)
        {
            try
            {
                string sql = "delete from staffs where stfId=" + stfid;
                conn.Open();
                conn.ExecuteNonQuery(sql);
                conn.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmCreateStaff:::DeleteStaff:::" + ex.Message);
            }
        }

        private void UpdateStaff(string name, string password, string staffid, string per, string stfid)
        {
            try
            {
                string permiss = "";
                if (String.Compare(per, "General Staff") == 0)
                {
                    permiss = "1";
                }
                else if (String.Compare(per, "Supervisor") == 0)
                {
                    permiss = "2";
                }
                else if (String.Compare(per, "Manager") == 0)
                {
                    permiss = "3";
                }
                string sql = "update staffs set staffID='" + staffid + "',Name='" + name + "',`password`=password('" + password + "'),permission=" + permiss + " where stfid=" + stfid;
                if (password == "")
                {
                    sql = "update staffs set staffID='" + staffid + "',Name='" + name + "',permission=" + permiss + " where stfid=" + stfid;
                }
                conn.Open();
                conn.ExecuteNonQuery(sql);
                conn.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmCreateStaff:::UpdateStaff:::" + ex.Message);
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "" || txtStaffId.Text == "" || txtPassword.Text == "" || txtConfirmPassword.Text == "" || cbbPermission.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Tất cả thông tin không được trống!";
                    lbStatus.Visible = true;
                    return;
                }
                lbStatus.Text = "All information not empty!";
                lbStatus.Visible = true;
            }
            else if (CheckPass(txtPassword.Text, txtConfirmPassword.Text) == false)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Mật khẩu xác thực không đúng";
                    lbStatus.Visible = true;
                    txtPassword.Text = "";
                    txtConfirmPassword.Text = "";
                    return;
                }
                lbStatus.Text = "Confirmed password dose not match";
                lbStatus.Visible = true;
                txtPassword.Text = "";
                txtConfirmPassword.Text = "";
            }
            else if (CheckStaffID(txtStaffId.Text) == false)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Mã nhân viên đã tồn tại";
                    lbStatus.Visible = true;
                    txtStaffId.Text = "";
                    return;
                }
                lbStatus.Text = "Staff ID existed";
                lbStatus.Visible = true;
                txtStaffId.Text = "";
            }
            else
            {
                try
                {
                    AddStaff(txtStaffId.Text, txtPassword.Text, txtName.Text, cbbPermission.Text);
                    GetStaff();
                    btnDelete.Enabled = true;
                    btnUpdate.Enabled = true;
                    btnSaveNew.Enabled = false;
                    lbStatus.Visible = false;
                }
                catch (Exception)
                {
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvStaff.SelectedIndices.Count > 0)
            {
                Staff staff = (Staff)lvStaff.SelectedItems[0].Tag;
                if (txtName.Text == "" || txtStaffId.Text == "" || cbbPermission.Text == "")
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Tất cả thông tin không được trống!";
                        lbStatus.Visible = true;
                        return;
                    }
                    lbStatus.Text = "All information not empty!";
                    lbStatus.Visible = true;
                }
                else if (CheckPass(txtPassword.Text, txtConfirmPassword.Text) == false)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Mật khẩu xác thực không đúng";
                        lbStatus.Visible = true;
                        txtPassword.Text = "";
                        txtConfirmPassword.Text = "";
                        return;
                    }
                    lbStatus.Text = "Confirmed password dose not match";
                    lbStatus.Visible = true;
                    txtPassword.Text = "";
                    txtConfirmPassword.Text = "";
                }
                else
                {
                    try
                    {
                        UpdateStaff(txtName.Text, txtPassword.Text, txtStaffId.Text, cbbPermission.Text, staff.StfID.ToString());
                        GetStaff();
                        lbStatus.Visible = false;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvStaff.SelectedIndices.Count > 0)
            {
                Staff staff = (Staff)lvStaff.SelectedItems[0].Tag;
                DeleteStaff(staff.StfID.ToString());
                GetStaff();
            }
        }
    }
}