﻿namespace POS.Forms
{
    partial class frmOrdersAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdersAll));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel_Pumps = new System.Windows.Forms.FlowLayoutPanel();
            this.lbChangeName = new System.Windows.Forms.Label();
            this.lbChangeAmount = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTableID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbTableID = new System.Windows.Forms.Label();
            this.lbOrderID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvOrder = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnEndOfShift = new System.Windows.Forms.Button();
            this.btnSaveOrder = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDepartment7 = new System.Windows.Forms.Button();
            this.btnMechanic = new System.Windows.Forms.Button();
            this.btnMagTyre = new System.Windows.Forms.Button();
            this.btnAllMenu = new System.Windows.Forms.Button();
            this.buttonRefun = new System.Windows.Forms.Button();
            this.btnDepartment6 = new System.Windows.Forms.Button();
            this.btnFindItem = new System.Windows.Forms.Button();
            this.btnNewItem = new System.Windows.Forms.Button();
            this.btnDepartment5 = new System.Windows.Forms.Button();
            this.btnNextCus = new System.Windows.Forms.Button();
            this.btnFuelDisc = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnPreOrder = new System.Windows.Forms.Button();
            this.btnRecallOrder = new System.Windows.Forms.Button();
            this.btnFunction = new System.Windows.Forms.Button();
            this.btnDiscount = new System.Windows.Forms.Button();
            this.btnVoidAll = new System.Windows.Forms.Button();
            this.btnVoidItem = new System.Windows.Forms.Button();
            this.btnSubtotal = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDepartment1 = new System.Windows.Forms.Button();
            this.btnDepartment2 = new System.Windows.Forms.Button();
            this.btnDepartment3 = new System.Windows.Forms.Button();
            this.btnDepartment4 = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.ucFuelControls = new POS.Fuel.UCFuelControls();
            this.axCsdEft = new POS.CardInterface.POSAxCsdEft();
            this.txtUnitPrice = new POS.TextBoxNumbericPOS();
            this.txtBarcode = new POS.Controls.TextBoxBecas();
            this.txtStaffID = new POS.Controls.TextBoxBecas();
            this.textBox1 = new POS.Controls.TextBoxBecas();
            this.txtPort = new POS.Controls.TextBoxBecas();
            this.txtQty = new POS.Controls.TextBoxBecas();
            this.uCkeypad1 = new POS.UCkeypad();
            this.lblStatus = new POS.POSLabelStatus();
            this.txtOrderID = new POS.Controls.TextBoxBecas();
            this.txtItemName = new POS.Controls.TextBoxBecas();
            this.ucMenuChange = new POS.Controls.UCMenu();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axCsdEft)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel_Pumps);
            this.splitContainer1.Panel1.Controls.Add(this.ucFuelControls);
            this.splitContainer1.Panel1.Controls.Add(this.axCsdEft);
            this.splitContainer1.Panel1.Controls.Add(this.lbChangeName);
            this.splitContainer1.Panel1.Controls.Add(this.lbChangeAmount);
            this.splitContainer1.Panel1.Controls.Add(this.lblTotal);
            this.splitContainer1.Panel1.Controls.Add(this.txtUnitPrice);
            this.splitContainer1.Panel1.Controls.Add(this.txtBarcode);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.txtStaffID);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.textBox1);
            this.splitContainer1.Panel1.Controls.Add(this.txtPort);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.txtQty);
            this.splitContainer1.Panel1.Controls.Add(this.txtTableID);
            this.splitContainer1.Panel1.Controls.Add(this.txtOrderID);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.lbTableID);
            this.splitContainer1.Panel1.Controls.Add(this.lbOrderID);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.txtItemName);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ucMenuChange);
            this.splitContainer1.Panel2.Controls.Add(this.lvOrder);
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(1000, 768);
            this.splitContainer1.SplitterDistance = 170;
            this.splitContainer1.TabIndex = 0;
            // 
            // flowLayoutPanel_Pumps
            // 
            this.flowLayoutPanel_Pumps.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel_Pumps.Location = new System.Drawing.Point(0, 18);
            this.flowLayoutPanel_Pumps.Name = "flowLayoutPanel_Pumps";
            this.flowLayoutPanel_Pumps.Size = new System.Drawing.Size(998, 82);
            this.flowLayoutPanel_Pumps.TabIndex = 38;
            // 
            // lbChangeName
            // 
            this.lbChangeName.BackColor = System.Drawing.Color.White;
            this.lbChangeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangeName.ForeColor = System.Drawing.Color.Black;
            this.lbChangeName.Location = new System.Drawing.Point(818, 27);
            this.lbChangeName.Name = "lbChangeName";
            this.lbChangeName.Size = new System.Drawing.Size(176, 33);
            this.lbChangeName.TabIndex = 33;
            this.lbChangeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbChangeAmount
            // 
            this.lbChangeAmount.BackColor = System.Drawing.Color.White;
            this.lbChangeAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangeAmount.ForeColor = System.Drawing.Color.Red;
            this.lbChangeAmount.Location = new System.Drawing.Point(818, 62);
            this.lbChangeAmount.Name = "lbChangeAmount";
            this.lbChangeAmount.Size = new System.Drawing.Size(176, 33);
            this.lbChangeAmount.TabIndex = 33;
            this.lbChangeAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Red;
            this.lblTotal.Location = new System.Drawing.Point(367, 31);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(169, 33);
            this.lblTotal.TabIndex = 30;
            this.lblTotal.Text = "Total";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(551, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "Bar Code :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(551, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 26;
            this.label7.Text = "Staff :";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Location = new System.Drawing.Point(1, 1);
            this.panel4.Margin = new System.Windows.Forms.Padding(1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(994, 25);
            this.panel4.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(833, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "People:";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(833, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Pager:";
            this.label6.Visible = false;
            // 
            // txtTableID
            // 
            this.txtTableID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTableID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTableID.Location = new System.Drawing.Point(434, 38);
            this.txtTableID.Name = "txtTableID";
            this.txtTableID.Size = new System.Drawing.Size(86, 26);
            this.txtTableID.TabIndex = 17;
            this.txtTableID.Visible = false;
            this.txtTableID.TextChanged += new System.EventHandler(this.txttableid_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(168, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Unit Price :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Qty :";
            // 
            // lbTableID
            // 
            this.lbTableID.AutoSize = true;
            this.lbTableID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTableID.Location = new System.Drawing.Point(366, 40);
            this.lbTableID.Name = "lbTableID";
            this.lbTableID.Size = new System.Drawing.Size(69, 15);
            this.lbTableID.TabIndex = 4;
            this.lbTableID.Text = "Table ID :";
            this.lbTableID.Visible = false;
            // 
            // lbOrderID
            // 
            this.lbOrderID.AutoSize = true;
            this.lbOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderID.Location = new System.Drawing.Point(372, 71);
            this.lbOrderID.Name = "lbOrderID";
            this.lbOrderID.Size = new System.Drawing.Size(74, 16);
            this.lbOrderID.TabIndex = 2;
            this.lbOrderID.Text = "Order ID :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Item name :";
            // 
            // lvOrder
            // 
            this.lvOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvOrder.BackColor = System.Drawing.Color.White;
            this.lvOrder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6});
            this.lvOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvOrder.FullRowSelect = true;
            this.lvOrder.GridLines = true;
            this.lvOrder.HideSelection = false;
            this.lvOrder.Location = new System.Drawing.Point(369, 0);
            this.lvOrder.MultiSelect = false;
            this.lvOrder.Name = "lvOrder";
            this.lvOrder.Size = new System.Drawing.Size(333, 539);
            this.lvOrder.TabIndex = 0;
            this.lvOrder.UseCompatibleStateImageBehavior = false;
            this.lvOrder.View = System.Windows.Forms.View.Details;
            this.lvOrder.SelectedIndexChanged += new System.EventHandler(this.lvOrder_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Qty";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "SubTotal";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 132;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "PumpID";
            this.columnHeader4.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ID";
            this.columnHeader6.Width = 0;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lblStatus);
            this.panel5.Controls.Add(this.btnExit);
            this.panel5.Controls.Add(this.btnEndOfShift);
            this.panel5.Controls.Add(this.btnSaveOrder);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 539);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(998, 53);
            this.panel5.TabIndex = 5;
            // 
            // btnEndOfShift
            // 
            this.btnEndOfShift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnEndOfShift.FlatAppearance.BorderSize = 0;
            this.btnEndOfShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEndOfShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEndOfShift.ForeColor = System.Drawing.Color.Black;
            this.btnEndOfShift.Location = new System.Drawing.Point(747, -1);
            this.btnEndOfShift.Margin = new System.Windows.Forms.Padding(1);
            this.btnEndOfShift.Name = "btnEndOfShift";
            this.btnEndOfShift.Size = new System.Drawing.Size(69, 49);
            this.btnEndOfShift.TabIndex = 40;
            this.btnEndOfShift.Text = "SHIFT MAN";
            this.btnEndOfShift.UseVisualStyleBackColor = false;
            this.btnEndOfShift.Visible = false;
            this.btnEndOfShift.Click += new System.EventHandler(this.btnEndOfShift_Click);
            // 
            // btnSaveOrder
            // 
            this.btnSaveOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.btnSaveOrder.FlatAppearance.BorderSize = 0;
            this.btnSaveOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveOrder.ForeColor = System.Drawing.Color.Black;
            this.btnSaveOrder.Location = new System.Drawing.Point(918, 1);
            this.btnSaveOrder.Margin = new System.Windows.Forms.Padding(1);
            this.btnSaveOrder.Name = "btnSaveOrder";
            this.btnSaveOrder.Size = new System.Drawing.Size(69, 49);
            this.btnSaveOrder.TabIndex = 47;
            this.btnSaveOrder.Text = "SAVE ORDER";
            this.btnSaveOrder.UseVisualStyleBackColor = false;
            this.btnSaveOrder.Click += new System.EventHandler(this.btnSaveOrder_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.uCkeypad1);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Location = new System.Drawing.Point(706, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(289, 531);
            this.panel3.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btnDepartment7, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnMechanic, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnMagTyre, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnAllMenu, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRefun, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnDepartment6, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnFindItem, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnNewItem, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnDepartment5, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnNextCus, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnFuelDisc, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnPrint, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnPreOrder, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnRecallOrder, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnFunction, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnDiscount, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnVoidAll, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnVoidItem, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnSubtotal, 2, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 271);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(286, 257);
            this.tableLayoutPanel2.TabIndex = 54;
            // 
            // btnDepartment7
            // 
            this.btnDepartment7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment7.FlatAppearance.BorderSize = 0;
            this.btnDepartment7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment7.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment7.Location = new System.Drawing.Point(72, 1);
            this.btnDepartment7.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment7.Name = "btnDepartment7";
            this.btnDepartment7.Size = new System.Drawing.Size(69, 49);
            this.btnDepartment7.TabIndex = 53;
            this.btnDepartment7.Text = "Workshop";
            this.btnDepartment7.UseVisualStyleBackColor = false;
            this.btnDepartment7.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnMechanic
            // 
            this.btnMechanic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.btnMechanic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMechanic.FlatAppearance.BorderSize = 0;
            this.btnMechanic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMechanic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnMechanic.Location = new System.Drawing.Point(1, 154);
            this.btnMechanic.Margin = new System.Windows.Forms.Padding(1);
            this.btnMechanic.Name = "btnMechanic";
            this.btnMechanic.Size = new System.Drawing.Size(69, 49);
            this.btnMechanic.TabIndex = 41;
            this.btnMechanic.Text = "ME- CHANIC";
            this.btnMechanic.UseVisualStyleBackColor = false;
            this.btnMechanic.Click += new System.EventHandler(this.btnMechanic_Click);
            // 
            // btnMagTyre
            // 
            this.btnMagTyre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
            this.btnMagTyre.FlatAppearance.BorderSize = 0;
            this.btnMagTyre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMagTyre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMagTyre.ForeColor = System.Drawing.Color.Black;
            this.btnMagTyre.Location = new System.Drawing.Point(1, 52);
            this.btnMagTyre.Margin = new System.Windows.Forms.Padding(1);
            this.btnMagTyre.Name = "btnMagTyre";
            this.btnMagTyre.Size = new System.Drawing.Size(69, 49);
            this.btnMagTyre.TabIndex = 48;
            this.btnMagTyre.Text = "FIND MAG /TYRE";
            this.btnMagTyre.UseVisualStyleBackColor = false;
            this.btnMagTyre.Click += new System.EventHandler(this.btnMagType_Click);
            // 
            // btnAllMenu
            // 
            this.btnAllMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(130)))));
            this.btnAllMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllMenu.Enabled = false;
            this.btnAllMenu.FlatAppearance.BorderSize = 0;
            this.btnAllMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllMenu.ForeColor = System.Drawing.Color.Black;
            this.btnAllMenu.Location = new System.Drawing.Point(1, 1);
            this.btnAllMenu.Margin = new System.Windows.Forms.Padding(1);
            this.btnAllMenu.Name = "btnAllMenu";
            this.btnAllMenu.Size = new System.Drawing.Size(69, 49);
            this.btnAllMenu.TabIndex = 23;
            this.btnAllMenu.Text = "ALL MENU";
            this.btnAllMenu.UseVisualStyleBackColor = false;
            this.btnAllMenu.Click += new System.EventHandler(this.btnAllMenu_Click);
            // 
            // buttonRefun
            // 
            this.buttonRefun.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.buttonRefun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefun.FlatAppearance.BorderSize = 0;
            this.buttonRefun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefun.Location = new System.Drawing.Point(214, 205);
            this.buttonRefun.Margin = new System.Windows.Forms.Padding(1);
            this.buttonRefun.Name = "buttonRefun";
            this.buttonRefun.Size = new System.Drawing.Size(71, 51);
            this.buttonRefun.TabIndex = 52;
            this.buttonRefun.Text = "REFUND";
            this.buttonRefun.UseVisualStyleBackColor = false;
            this.buttonRefun.Click += new System.EventHandler(this.buttonRefun_Click);
            // 
            // btnDepartment6
            // 
            this.btnDepartment6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment6.FlatAppearance.BorderSize = 0;
            this.btnDepartment6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment6.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment6.Location = new System.Drawing.Point(143, 1);
            this.btnDepartment6.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment6.Name = "btnDepartment6";
            this.btnDepartment6.Size = new System.Drawing.Size(69, 49);
            this.btnDepartment6.TabIndex = 39;
            this.btnDepartment6.Text = "Hardware";
            this.btnDepartment6.UseVisualStyleBackColor = false;
            this.btnDepartment6.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnFindItem
            // 
            this.btnFindItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
            this.btnFindItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFindItem.FlatAppearance.BorderSize = 0;
            this.btnFindItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindItem.ForeColor = System.Drawing.Color.Black;
            this.btnFindItem.Location = new System.Drawing.Point(72, 52);
            this.btnFindItem.Margin = new System.Windows.Forms.Padding(1);
            this.btnFindItem.Name = "btnFindItem";
            this.btnFindItem.Size = new System.Drawing.Size(69, 49);
            this.btnFindItem.TabIndex = 35;
            this.btnFindItem.Text = "FIND ITEM";
            this.btnFindItem.UseVisualStyleBackColor = false;
            this.btnFindItem.Click += new System.EventHandler(this.btnFindItem_Click);
            // 
            // btnNewItem
            // 
            this.btnNewItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnNewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewItem.FlatAppearance.BorderSize = 0;
            this.btnNewItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewItem.Location = new System.Drawing.Point(143, 205);
            this.btnNewItem.Margin = new System.Windows.Forms.Padding(1);
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(69, 51);
            this.btnNewItem.TabIndex = 51;
            this.btnNewItem.Text = "NEW ITEM";
            this.btnNewItem.UseVisualStyleBackColor = false;
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // btnDepartment5
            // 
            this.btnDepartment5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment5.FlatAppearance.BorderSize = 0;
            this.btnDepartment5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment5.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment5.Location = new System.Drawing.Point(214, 1);
            this.btnDepartment5.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment5.Name = "btnDepartment5";
            this.btnDepartment5.Size = new System.Drawing.Size(71, 49);
            this.btnDepartment5.TabIndex = 33;
            this.btnDepartment5.Text = "Garden";
            this.btnDepartment5.UseVisualStyleBackColor = false;
            this.btnDepartment5.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnNextCus
            // 
            this.btnNextCus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnNextCus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextCus.FlatAppearance.BorderSize = 0;
            this.btnNextCus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextCus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextCus.Location = new System.Drawing.Point(72, 205);
            this.btnNextCus.Margin = new System.Windows.Forms.Padding(1);
            this.btnNextCus.Name = "btnNextCus";
            this.btnNextCus.Size = new System.Drawing.Size(69, 51);
            this.btnNextCus.TabIndex = 50;
            this.btnNextCus.Text = "NEXT CUST";
            this.btnNextCus.UseVisualStyleBackColor = false;
            this.btnNextCus.Click += new System.EventHandler(this.btnNextCus_Click);
            // 
            // btnFuelDisc
            // 
            this.btnFuelDisc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnFuelDisc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFuelDisc.FlatAppearance.BorderSize = 0;
            this.btnFuelDisc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuelDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuelDisc.Location = new System.Drawing.Point(1, 205);
            this.btnFuelDisc.Margin = new System.Windows.Forms.Padding(1);
            this.btnFuelDisc.Name = "btnFuelDisc";
            this.btnFuelDisc.Size = new System.Drawing.Size(69, 51);
            this.btnFuelDisc.TabIndex = 49;
            this.btnFuelDisc.Text = "FUEL DISC";
            this.btnFuelDisc.UseVisualStyleBackColor = false;
            this.btnFuelDisc.Click += new System.EventHandler(this.btnFuelDisc_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(214, 154);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(71, 49);
            this.btnPrint.TabIndex = 42;
            this.btnPrint.Text = "PRINT";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnPreOrder
            // 
            this.btnPreOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.btnPreOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreOrder.FlatAppearance.BorderSize = 0;
            this.btnPreOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreOrder.ForeColor = System.Drawing.Color.Black;
            this.btnPreOrder.Location = new System.Drawing.Point(143, 154);
            this.btnPreOrder.Margin = new System.Windows.Forms.Padding(1);
            this.btnPreOrder.Name = "btnPreOrder";
            this.btnPreOrder.Size = new System.Drawing.Size(69, 49);
            this.btnPreOrder.TabIndex = 27;
            this.btnPreOrder.Text = "PREV ORDER";
            this.btnPreOrder.UseVisualStyleBackColor = false;
            this.btnPreOrder.Click += new System.EventHandler(this.btnPreOrder_Click);
            // 
            // btnRecallOrder
            // 
            this.btnRecallOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.btnRecallOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRecallOrder.FlatAppearance.BorderSize = 0;
            this.btnRecallOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecallOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecallOrder.ForeColor = System.Drawing.Color.Black;
            this.btnRecallOrder.Location = new System.Drawing.Point(72, 154);
            this.btnRecallOrder.Margin = new System.Windows.Forms.Padding(1);
            this.btnRecallOrder.Name = "btnRecallOrder";
            this.btnRecallOrder.Size = new System.Drawing.Size(69, 49);
            this.btnRecallOrder.TabIndex = 46;
            this.btnRecallOrder.Text = "RECALL ORDER";
            this.btnRecallOrder.UseVisualStyleBackColor = false;
            this.btnRecallOrder.Click += new System.EventHandler(this.btnRecallOrder_Click);
            // 
            // btnFunction
            // 
            this.btnFunction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFunction.FlatAppearance.BorderSize = 0;
            this.btnFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFunction.ForeColor = System.Drawing.Color.Black;
            this.btnFunction.Location = new System.Drawing.Point(1, 103);
            this.btnFunction.Margin = new System.Windows.Forms.Padding(1);
            this.btnFunction.Name = "btnFunction";
            this.btnFunction.Size = new System.Drawing.Size(69, 49);
            this.btnFunction.TabIndex = 41;
            this.btnFunction.Text = "FUNCS";
            this.btnFunction.UseVisualStyleBackColor = false;
            this.btnFunction.Click += new System.EventHandler(this.btnFunction_Click);
            // 
            // btnDiscount
            // 
            this.btnDiscount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscount.FlatAppearance.BorderSize = 0;
            this.btnDiscount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscount.ForeColor = System.Drawing.Color.Black;
            this.btnDiscount.Location = new System.Drawing.Point(214, 103);
            this.btnDiscount.Margin = new System.Windows.Forms.Padding(1);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(71, 49);
            this.btnDiscount.TabIndex = 43;
            this.btnDiscount.Text = "DIS - COUNT";
            this.btnDiscount.UseVisualStyleBackColor = false;
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnVoidAll
            // 
            this.btnVoidAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnVoidAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVoidAll.FlatAppearance.BorderSize = 0;
            this.btnVoidAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoidAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoidAll.ForeColor = System.Drawing.Color.Black;
            this.btnVoidAll.Location = new System.Drawing.Point(72, 103);
            this.btnVoidAll.Margin = new System.Windows.Forms.Padding(1);
            this.btnVoidAll.Name = "btnVoidAll";
            this.btnVoidAll.Size = new System.Drawing.Size(69, 49);
            this.btnVoidAll.TabIndex = 45;
            this.btnVoidAll.Text = "VOID ALL";
            this.btnVoidAll.UseVisualStyleBackColor = false;
            this.btnVoidAll.Click += new System.EventHandler(this.btnVoidAll_Click);
            // 
            // btnVoidItem
            // 
            this.btnVoidItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnVoidItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVoidItem.FlatAppearance.BorderSize = 0;
            this.btnVoidItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoidItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoidItem.ForeColor = System.Drawing.Color.Black;
            this.btnVoidItem.Location = new System.Drawing.Point(143, 103);
            this.btnVoidItem.Margin = new System.Windows.Forms.Padding(1);
            this.btnVoidItem.Name = "btnVoidItem";
            this.btnVoidItem.Size = new System.Drawing.Size(69, 49);
            this.btnVoidItem.TabIndex = 25;
            this.btnVoidItem.Text = "VOID  ITEM";
            this.btnVoidItem.UseVisualStyleBackColor = false;
            this.btnVoidItem.Click += new System.EventHandler(this.btnVoidItem_Click);
            // 
            // btnSubtotal
            // 
            this.btnSubtotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.tableLayoutPanel2.SetColumnSpan(this.btnSubtotal, 2);
            this.btnSubtotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSubtotal.FlatAppearance.BorderSize = 0;
            this.btnSubtotal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtotal.ForeColor = System.Drawing.Color.Black;
            this.btnSubtotal.Location = new System.Drawing.Point(143, 52);
            this.btnSubtotal.Margin = new System.Windows.Forms.Padding(1);
            this.btnSubtotal.Name = "btnSubtotal";
            this.btnSubtotal.Size = new System.Drawing.Size(142, 49);
            this.btnSubtotal.TabIndex = 26;
            this.btnSubtotal.Text = "SUB TOTAL";
            this.btnSubtotal.UseVisualStyleBackColor = false;
            this.btnSubtotal.Click += new System.EventHandler(this.btnSubtotal_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDepartment4, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(213, 1);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(73, 271);
            this.tableLayoutPanel1.TabIndex = 53;
            // 
            // btnDepartment1
            // 
            this.btnDepartment1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment1.FlatAppearance.BorderSize = 0;
            this.btnDepartment1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment1.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment1.Location = new System.Drawing.Point(1, 1);
            this.btnDepartment1.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment1.Name = "btnDepartment1";
            this.btnDepartment1.Size = new System.Drawing.Size(71, 65);
            this.btnDepartment1.TabIndex = 29;
            this.btnDepartment1.Text = "Gifts";
            this.btnDepartment1.UseVisualStyleBackColor = false;
            this.btnDepartment1.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnDepartment2
            // 
            this.btnDepartment2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment2.FlatAppearance.BorderSize = 0;
            this.btnDepartment2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment2.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment2.Location = new System.Drawing.Point(1, 68);
            this.btnDepartment2.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment2.Name = "btnDepartment2";
            this.btnDepartment2.Size = new System.Drawing.Size(71, 65);
            this.btnDepartment2.TabIndex = 30;
            this.btnDepartment2.Text = "Cards";
            this.btnDepartment2.UseVisualStyleBackColor = false;
            this.btnDepartment2.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnDepartment3
            // 
            this.btnDepartment3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment3.FlatAppearance.BorderSize = 0;
            this.btnDepartment3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment3.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment3.Location = new System.Drawing.Point(1, 135);
            this.btnDepartment3.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment3.Name = "btnDepartment3";
            this.btnDepartment3.Size = new System.Drawing.Size(71, 65);
            this.btnDepartment3.TabIndex = 31;
            this.btnDepartment3.Text = "VASE";
            this.btnDepartment3.UseVisualStyleBackColor = false;
            this.btnDepartment3.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnDepartment4
            // 
            this.btnDepartment4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDepartment4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDepartment4.FlatAppearance.BorderSize = 0;
            this.btnDepartment4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartment4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartment4.ForeColor = System.Drawing.Color.Black;
            this.btnDepartment4.Location = new System.Drawing.Point(1, 202);
            this.btnDepartment4.Margin = new System.Windows.Forms.Padding(1);
            this.btnDepartment4.Name = "btnDepartment4";
            this.btnDepartment4.Size = new System.Drawing.Size(71, 68);
            this.btnDepartment4.TabIndex = 22;
            this.btnDepartment4.Text = "FLOWERS";
            this.btnDepartment4.UseVisualStyleBackColor = false;
            this.btnDepartment4.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Image = global::POS.Properties.Resources.Exit;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(124, 49);
            this.btnExit.TabIndex = 0;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // ucFuelControls
            // 
            this.ucFuelControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucFuelControls.Location = new System.Drawing.Point(0, 100);
            this.ucFuelControls.Name = "ucFuelControls";
            this.ucFuelControls.Size = new System.Drawing.Size(998, 68);
            this.ucFuelControls.TabIndex = 37;
            this.ucFuelControls.Visible = false;
            // 
            // axCsdEft
            // 
            this.axCsdEft.Enabled = true;
            this.axCsdEft.Location = new System.Drawing.Point(1005, 34);
            this.axCsdEft.Name = "axCsdEft";
            this.axCsdEft.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCsdEft.OcxState")));
            this.axCsdEft.Size = new System.Drawing.Size(100, 50);
            this.axCsdEft.TabIndex = 36;
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtUnitPrice.Enabled = false;
            this.txtUnitPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnitPrice.Location = new System.Drawing.Point(257, 65);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(107, 29);
            this.txtUnitPrice.TabIndex = 31;
            this.txtUnitPrice.ucKeypad = null;
            this.txtUnitPrice.uckeypadDiv = null;
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // txtBarcode
            // 
            this.txtBarcode._Keypad = null;
            this.txtBarcode._LabelStatusError = null;
            this.txtBarcode._Precision = 2;
            this.txtBarcode._StatusError = "Invalid";
            this.txtBarcode._TypeNumber = POS.Controls.TypeNumber.Text;
            this.txtBarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtBarcode.Enabled = false;
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(638, 65);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.ReadOnly = true;
            this.txtBarcode.Size = new System.Drawing.Size(165, 29);
            this.txtBarcode.TabIndex = 29;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // txtStaffID
            // 
            this.txtStaffID._Keypad = null;
            this.txtStaffID._LabelStatusError = null;
            this.txtStaffID._Precision = 2;
            this.txtStaffID._StatusError = "Invalid";
            this.txtStaffID._TypeNumber = POS.Controls.TypeNumber.Text;
            this.txtStaffID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtStaffID.Enabled = false;
            this.txtStaffID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStaffID.Location = new System.Drawing.Point(638, 32);
            this.txtStaffID.Name = "txtStaffID";
            this.txtStaffID.ReadOnly = true;
            this.txtStaffID.Size = new System.Drawing.Size(165, 29);
            this.txtStaffID.TabIndex = 27;
            // 
            // textBox1
            // 
            this.textBox1._Keypad = null;
            this.textBox1._LabelStatusError = null;
            this.textBox1._Precision = 2;
            this.textBox1._StatusError = "Invalid";
            this.textBox1._TypeNumber = POS.Controls.TypeNumber.Text;
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(890, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(88, 26);
            this.textBox1.TabIndex = 23;
            this.textBox1.Visible = false;
            // 
            // txtPort
            // 
            this.txtPort._Keypad = null;
            this.txtPort._LabelStatusError = null;
            this.txtPort._Precision = 2;
            this.txtPort._StatusError = "Invalid";
            this.txtPort._TypeNumber = POS.Controls.TypeNumber.Text;
            this.txtPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(890, 65);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(88, 29);
            this.txtPort.TabIndex = 22;
            this.txtPort.Visible = false;
            // 
            // txtQty
            // 
            this.txtQty._Keypad = this.uCkeypad1;
            this.txtQty._LabelStatusError = this.lblStatus;
            this.txtQty._Precision = 2;
            this.txtQty._StatusError = "Invalid";
            this.txtQty._TypeNumber = POS.Controls.TypeNumber.Number;
            this.txtQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQty.Location = new System.Drawing.Point(89, 65);
            this.txtQty.MaxLength = 9;
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(76, 29);
            this.txtQty.TabIndex = 19;
            this.txtQty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(1, 1);
            this.uCkeypad1.Margin = new System.Windows.Forms.Padding(4);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 55;
            this.uCkeypad1.txtResult = null;
            // 
            // lblStatus
            // 
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(124, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.POSTimeMessenge = 1;
            this.lblStatus.Size = new System.Drawing.Size(870, 49);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOrderID
            // 
            this.txtOrderID._Keypad = null;
            this.txtOrderID._LabelStatusError = null;
            this.txtOrderID._Precision = 2;
            this.txtOrderID._StatusError = "Invalid";
            this.txtOrderID._TypeNumber = POS.Controls.TypeNumber.Text;
            this.txtOrderID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtOrderID.Enabled = false;
            this.txtOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderID.Location = new System.Drawing.Point(450, 65);
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.ReadOnly = true;
            this.txtOrderID.Size = new System.Drawing.Size(86, 29);
            this.txtOrderID.TabIndex = 16;
            // 
            // txtItemName
            // 
            this.txtItemName._Keypad = null;
            this.txtItemName._LabelStatusError = null;
            this.txtItemName._Precision = 2;
            this.txtItemName._StatusError = "Invalid";
            this.txtItemName._TypeNumber = POS.Controls.TypeNumber.Text;
            this.txtItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtItemName.Enabled = false;
            this.txtItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemName.Location = new System.Drawing.Point(89, 32);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(275, 29);
            this.txtItemName.TabIndex = 0;
            // 
            // ucMenuChange
            // 
            this.ucMenuChange._Barcode = null;
            this.ucMenuChange._CheckConnectionServer = false;
            this.ucMenuChange._ClickGroup = false;
            this.ucMenuChange._ClickItems = true;
            this.ucMenuChange._ConnectionServer = true;
            this.ucMenuChange._Delete = DataObject.DeleteType.NoDelete;
            this.ucMenuChange._ExistsItem = true;
            this.ucMenuChange._GroupID = 0;
            this.ucMenuChange._GroupMenu = null;
            this.ucMenuChange._IsFuel = DataObject.IsFuelType.No;
            this.ucMenuChange._ItemID = 0;
            this.ucMenuChange._ItemsMenu = null;
            this.ucMenuChange._LoadMenu = true;
            this.ucMenuChange._Type = DataObject.ListItemType.Items;
            this.ucMenuChange._Visual = DataObject.VisualType.Display;
            this.ucMenuChange.Dock = System.Windows.Forms.DockStyle.Left;
            this.ucMenuChange.GroupDisplayOrderMax = 0;
            this.ucMenuChange.ItemDisplayOrderMax = 0;
            this.ucMenuChange.Location = new System.Drawing.Point(0, 0);
            this.ucMenuChange.mDBConfig = null;
            this.ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
            this.ucMenuChange.Name = "ucMenuChange";
            this.ucMenuChange.Size = new System.Drawing.Size(364, 539);
            this.ucMenuChange.TabIndex = 6;
            this.ucMenuChange.Click += new System.EventHandler(this.ucMenuChange_Click);
            // 
            // frmOrdersAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1000, 768);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOrdersAll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order All";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmOrdersAll_FormClosed);
            this.Load += new System.EventHandler(this.frmOrdersAll_Load);
            this.VisibleChanged += new System.EventHandler(this.frmOrdersAll_VisibleChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axCsdEft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lbOrderID;
        private System.Windows.Forms.Label label1;
        private POS.Controls.TextBoxBecas txtItemName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvOrder;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnVoidItem;
        private System.Windows.Forms.Button btnDepartment1;
        private System.Windows.Forms.Button btnFindItem;
        private System.Windows.Forms.Button btnDepartment5;
        private System.Windows.Forms.Button btnPreOrder;
        private System.Windows.Forms.Button btnDepartment3;
        private System.Windows.Forms.Button btnDepartment4;
        private System.Windows.Forms.Button btnAllMenu;
        private System.Windows.Forms.Button btnDepartment2;
        private System.Windows.Forms.Button btnSubtotal;
        private System.Windows.Forms.Button btnEndOfShift;
        private System.Windows.Forms.Button btnDepartment6;
        private System.Windows.Forms.Button btnDiscount;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnFunction;
        private System.Windows.Forms.Panel panel3;
        private POS.Controls.TextBoxBecas txtPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnVoidAll;
        private System.Windows.Forms.Button btnRecallOrder;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSaveOrder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonRefun;
        private System.Windows.Forms.Button btnNewItem;
        private System.Windows.Forms.Button btnNextCus;
        private System.Windows.Forms.Button btnFuelDisc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label8;
        private POSLabelStatus lblStatus;
        private TextBoxNumbericPOS txtUnitPrice;
        private UCkeypad uCkeypad1;
        private POS.Controls.TextBoxBecas textBox1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtTableID;
        private System.Windows.Forms.Label lbTableID;
        private POS.Controls.TextBoxBecas txtStaffID;
        public POS.Controls.TextBoxBecas txtOrderID;
        private POS.Controls.TextBoxBecas txtBarcode;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label lbChangeAmount;
        private System.Windows.Forms.Label lbChangeName;
        internal POS.Controls.TextBoxBecas txtQty;
        private CardInterface.POSAxCsdEft axCsdEft;
        private Controls.UCMenu ucMenuChange;
        private System.Windows.Forms.Button btnMagTyre;
        private System.Windows.Forms.Button btnMechanic;
        private Fuel.UCFuelControls ucFuelControls;
        private System.Windows.Forms.Button btnDepartment7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_Pumps;
        public System.Windows.Forms.Label lblTotal;





    }
}