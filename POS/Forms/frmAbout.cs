﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace POS.Forms
{
    public partial class frmAbout : Form
    {
        private frmOrdersAll fMain;

        public bool IsStoped { get; set; }

        public frmAbout(frmOrdersAll f)
        {
            IsStoped = false;
            InitializeComponent();
            fMain = f;
        }
        int count = 0;

        private void frmAbout_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pbAboutBar.Value < 100)
            {
                pbAboutBar.Value += 1;
                pbAboutBar.CreateGraphics().DrawString(pbAboutBar.Value.ToString() + "%", new Font("Arial", (float)12, FontStyle.Regular), Brushes.Black, new PointF(pbAboutBar.Width / 2 - 10,
                pbAboutBar.Height / 2 - 7));
            }
            else
            {
                count++;
                if (IsStoped)
                {
                    Exit();
                }
                else if (count > 100)
                {
                    Exit();
                }
            }
        }

        private void Exit()
        {
            timer1.Stop();
            if (fMain != null)
            {
                fMain.Visible = true;
            }
            else if (pbAboutBar != null)
            {
                pbAboutBar.Visible = true;
            }

            this.Close();
        }
    }
}