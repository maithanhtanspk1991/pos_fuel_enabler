﻿using CSharpExample;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tyro.Integ;
using Tyro.Integ.Domain;

namespace POS.Forms
{
    public partial class frmChooseRefund : Form
    {
        int efposrevice = 0;
        static bool complete = false;
        private TerminalAdapter _adapter;
        private POSInformation _posInfo;

        public frmChooseRefund(int efpos)
        {
            InitializeComponent();
            efposrevice = efpos;
        }

        private TerminalAdapter adapter
        {
            get
            {
                if (_adapter == null)
                {
                    _adapter = new TerminalAdapter(_posInfo);
                    _adapter.ReceiptReturned += ReceiptReturned;
                    _adapter.ErrorOccured += ErrorOccured;
                    _adapter.TransactionCompleted += TransactionCompleted;
                }
                return _adapter;
            }
        }

        private static void TransactionCompleted(Transaction transaction)
        {
            MessageBox.Show(String.Format("Status: {0} {1}Result: {2} {1}Authorisation Code: {3} {1}Transaction Reference: {4} {1}Card Type: {5} {1}Generated TransactionID: {6} {1}Tip Amount: {7} {1}Tip Completion Reference: {8} {1} Extra Data: {9} \n",
                                             transaction.Status, Environment.NewLine, transaction.Result, transaction.AuthorisationCode, transaction.ReferenceNumber, transaction.CardType, transaction.ID, transaction.TipAmount,
                                             transaction.TipCompletionReference, AdditionalDataUtility.AdditionalDataXmlToFormattedString(transaction.GetExtraData())));
            if (transaction.GetResult().Equals("APPROVED"))
            {
                MessageBox.Show("POS refunds the sale", "Infomation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                complete = true;
            }
            else if (transaction.GetResult().Equals("DECLINED"))
            {
                MessageBox.Show("POS does not refund the sale", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                complete = false;
            }
            else if (transaction.GetResult().Equals("CANCELLED "))
            {
                MessageBox.Show("POS does not refund the sale", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                complete = false;
            }
            else if (transaction.GetResult().Equals("REVERSED"))
            {
                MessageBox.Show("POS does not refund the sale", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                complete = false;
            }
            else if (transaction.GetResult().Equals("SYSTEM ERROR"))
            {
                MessageBox.Show("POS does not refund the sale", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                complete = false;
            }
        }

        private static void ErrorOccured(Error error)
        {
            MessageBox.Show(String.Format("ERROR: {0} {1}Transaction Started: {2}",
                                                error.ErrorMessage, Environment.NewLine, error.TransactionStarted));
        }

        private static void ReceiptReturned(Receipt receipt)
        {
            MessageBox.Show(receipt.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (complete == true)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void rdSwipeCard_CheckedChanged(object sender, EventArgs e)
        {
            _posInfo = new POSInformation("ACME", "ACME POS", "1.2.3", "123 Happy Lane");
            int amount = efposrevice;
            adapter.Refund(amount);
        }

        private void rdCash_CheckedChanged(object sender, EventArgs e)
        {
            complete = true;
        }

    }
}
