﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmCust : Form
    {
        public Barcode.SerialPort mSerialPort;
        private readonly string _nameClass = "POS::Forms::frmCust::";
        private readonly int tien = 1000;
        private CardInterface.POSAxCsdEft axCsdEft;

        /// <summary>
        /// Khi in lai Accout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private double dblDepositAccount = 0;
        private bool IsMemberNo = false;
        private List<DataObject.Rego> lsRego = new List<DataObject.Rego>();
        private Connection.Connection mConnection = new Connection.Connection();
        private DataObject.Customers mCustomers = null;
        private Class.MoneyFortmat mMoneyFortmat;
        private Printer mPrinter;
        private ReadConfig mReadConfig;
        private int shiftID;

        public frmCust(int shiftID, Barcode.SerialPort barcode, CardInterface.POSAxCsdEft axCsdEft, ReadConfig readConfig)
        {
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            InitializeComponent();
            SetMultiLanguage();
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            mPrinter.SetPrinterName(dbconfig.BillPrinter.ToString());
            this.shiftID = shiftID;
            this.mSerialPort = barcode;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            LoadCompany();
            ucLimit.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit_EventPage);
            mReadConfig = readConfig;
            this.axCsdEft = axCsdEft;

            //Khóa Company
            if (!mReadConfig.IsEnableCompany)
            {
                lvCustomer.Columns[3].Width = 0;
                lvCustomer.Columns[2].Width = 0;
                lvCustomer.Columns[1].Width = lvCustomer.Columns[1].Width * 2;
                lvCustomer.Columns[4].Width = lvCustomer.Columns[4].Width * 2;
                cbbCompany.Visible = false;
                lbCompany.Visible = false;
            }
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label20.Text = "Tên:";
                label1.Text = "Họ:";
                lbCompany.Text = "Công ty:";
                label12.Text = "Đường số:";
                label14.Text = "Tên đường:";
                label3.Text = "Thành viên số:";
                label2.Text = "Di động:";
                label4.Text = "Tài khoản giới hạn:";
                label4.Font = new Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label16.Text = "Ghi nợ:";
                label18.Text = "Mã bưu điện:";
                btnNew.Text = "Tạo mới";
                btnSubmit.Text = "Lưu lại";
                btnDelete.Text = "Xóa";
                btnPrinter.Text = "In";
                btnCredit.Text = "Thanh toán nợ";
                btnRegoList.Text = "Danh sách Xe";
                btnHistoryTransaction.Text = "Lịch sử giao dịch";
                btnSearch.Text = "Tìm kiếm";
                columnHeader3.Text = "Thành viên số";
                columnHeader1.Text = "Họ";
                columnHeader2.Text = "Tên";
                columnHeader10.Text = "Tên công ty";
                columnHeader8.Text = "Tài khoản giới hạn";
                columnHeader5.Text = "Đường số";
                columnHeader6.Text = "Tên đường";
                columnHeader7.Text = "Ghi nợ";
                columnHeader9.Text = "Số xe";
                btnExit.Text = "Thoát";
                btnExit.Font = new Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }

        private delegate void SetTextCallback(string text, System.Windows.Forms.Control control);

        public void SetText(string text, System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        private void Account()
        {
            lbStatus.Text = "";

            if (mCustomers == null)
            {
                mCustomers = new DataObject.Customers();
                GetValue();
                mCustomers.RegoItem = lsRego;
                if (BusinessObject.BOCustomers.Insert(mCustomers) > -1)
                {
                    if (mReadConfig.IsPrinterAccount)
                        mPrinter.Print();
                    mCustomers = null;
                    LoadCustomer();
                }
            }
            else
            {
                GetValue();
                if (BusinessObject.BOCustomers.Update(mCustomers) > -1)
                {
                    LoadListViewItem();
                }
            }
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            if (mCustomers != null)
            {
                if (mCustomers.Company == 0)
                {
                    frmAccountPayment frmAccPay = new frmAccountPayment(mMoneyFortmat, mCustomers.CustID, mCustomers.MemberNo, txtAccountLimit.Text, txtBalance.Tag.ToString(), txtFirstName.Text, dblDepositAccount, 1, shiftID, axCsdEft);
                    if (frmAccPay.ShowDialog() == DialogResult.OK)
                    {
                        mCustomers = BusinessObject.BOCustomers.GetByID(mCustomers.CustID);
                        SetValue();
                        LoadListViewItem();
                    }
                }
                else
                {
                    frmMessageBoxOK frmMessageBoxOK = new frmMessageBoxOK("Information", "Company " + mCustomers.CompanyName + " will pay for this account!");
                    frmMessageBoxOK.ShowDialog();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (mCustomers != null)
            {
                if (mCustomers.Balance == 0)
                {
                    frmMessageBox frmMessageBox = new frmMessageBox("Information", "Do you want to delete customer ?");
                    if (frmMessageBox.ShowDialog() == DialogResult.OK)
                    {
                        mCustomers.Deleted = true;
                        if (BusinessObject.BOCustomers.Deleted(mCustomers) > -1)
                        {
                            mCustomers = null;
                            SetValue();
                            LoadCustomer();
                        }
                    }
                }
                else
                {
                    frmMessageBoxOK frmMessageBoxOK = new frmMessageBoxOK("Infomation", "This customer current debt :" + mMoneyFortmat.Format2(mCustomers.Balance) + " , can't delete !");
                    frmMessageBoxOK.ShowDialog();
                }
            }
        }

        private void btnHistoryTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                POS.FindObject.frmHistoryTransaction frm = new POS.FindObject.frmHistoryTransaction(mConnection, mMoneyFortmat, mCustomers.CustID);
                frm.Text = "Account Detail - " + mCustomers.FirstName;
                frm.ViewDetailAccountPay();
                frm.ShowDialog();
            }
            catch (Exception)
            {
            }
            /*
            if (mCustomers.Company != 0)
            {
                frmAccountPayment frmAccPay = new frmAccountPayment(money, mCustomers.CustID, mCustomers.MemberNo, txtAccountLimit.Text, txtBalance.Tag.ToString(), txtFirstName.Text, dblDepositAccount, 2, shiftID, axCsdEft);
                if (frmAccPay.ShowDialog() == DialogResult.OK)
                {
                    mCustomers = BusinessObject.BOCustomers.GetByID(mCustomers.CustID);
                    SetValue();
                    LoadListViewItem();
                }
            }
            else
            {
                frmMessageBoxOK frmMessageBoxOK = new frmMessageBoxOK("Information", "The function can't execute for this account,because of account belong to company " + mCustomers.CompanyName + " !");
                frmMessageBoxOK.ShowDialog();
            }*/
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            mCustomers = null;
            SetValue();
        }

        private void VisibleButton()
        {
            VisibleButton(btnPrinter, mReadConfig.IsPrinterAccount);
            VisibleButton(btnCredit, mReadConfig.IsPrinterAccount);
        }

        private void VisibleButton(Button btn, bool value)
        {
            btn.Visible = value;
        }

        private void btnPrinter_Click(object sender, EventArgs e)
        {
            if (mReadConfig.IsPrinterAccount)
                try
                {
                    lbStatus.Text = "";
                    if (mCustomers != null)
                        mPrinter.Print();
                }
                catch (Exception ex)
                {
                    SystemLog.LogPOS.WriteLog(_nameClass + "btnPrinter_Click::" + ex.Message);
                }
        }

        private void btnRegoList_Click(object sender, EventArgs e)
        {
            lsRego = null;
            frmRego frm = new frmRego(mCustomers);
            frm.ShowDialog();
            lsRego = frm.lsRego;
            LoadRego();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ucLimit.Default();
            mCustomers = null;
            LoadCustomer();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (CheckValues())
            {
                Account();
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Thông tin khách hàng không hợp lệ";
                    return;
                }
                lbStatus.Text = "Invalid customer information";
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cbbCompany.SelectedValue) == 0)
                {
                    txtAccountLimit.Enabled = true;
                    txtAccountLimit.Text = mReadConfig.AccountLimit;
                }
                else
                {
                    txtAccountLimit.Enabled = false;
                    txtAccountLimit.Text = "0";
                }
            }
            catch (Exception)
            {
            }
        }

        private bool ChangeAccountbydebt(double debt, double pay, string memberNo)
        {
            bool key = false;
            try
            {
                mConnection.Open();
                double after = debt - pay;
                mConnection.ExecuteNonQuery("update customers set debt=" + mMoneyFortmat.getFortMat(after) + " where memberNo=" + "'" + memberNo + "'");
                key = true;
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
            return key;
        }

        private bool ChangeAccountbyLimit(string limit, string memberNo)
        {
            bool key = false;
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("update customers set accountlimit=" + limit + " where memberNo=" + "'" + memberNo + "'");
                key = true;
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
            return key;
        }

        private bool CheckValues()
        {
            txtAccountLimit.Text = DataObject.Utilities.ReFormatStringToNumber(txtAccountLimit.Text);
            if (txtFirstName.Text == "" || txtAccountLimit.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Thông tin họ và tên bị trống";
                    return false;
                }
                lbStatus.Text = "Information Last Name and First Name Empty";
                return false;
            }
            else
            {
                return true;
            }
        }

        private string CreatRandom()
        {
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 12; i++)
            {
                resuilt += rd.Next(10);
            }
            return resuilt;
        }

        private void frmCust_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (mSerialPort != null)
            {
                mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            }
        }

        private void frmCust_Load(object sender, EventArgs e)
        {
            VisibleButton();
            SetValue();
            cbbType.SelectedIndex = 0;
            LoadCustomer();
        }

        private List<DataObject.ManageCar> GetCarWithCustomerNo(string customerNo)
        {
            List<DataObject.ManageCar> mlstcarreview = new List<DataObject.ManageCar>();
            try
            {
                mConnection.Open();
                string sql = "select m.id, m.CarNumber, m.CarColor, m.CarProduct from customers c left join managecar m on m.custID = c.custID where c.memberNo='" + txtMemberNo.Text + "' and m.deleted=0";

                DataTable dt = mConnection.Select(sql);
                foreach (DataRow row in dt.Rows)
                {
                    DataObject.ManageCar mc = new DataObject.ManageCar();
                    mc.CarNumber = row["CarNumber"].ToString();
                    mc.CarColor = row["CarColor"].ToString();
                    mc.CarProduct = row["CarProduct"].ToString();
                    mc.ObjectID = Convert.ToInt32(row["id"].ToString());
                    mlstcarreview.Add(mc);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("POS.Forms.frmCust::GetCustomerIDAndNameWithCustomerNo::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            return mlstcarreview;
        }

        private void GetValue()
        {
            mCustomers.StreetName = txtStreetName.Text;
            mCustomers.StreetNo = txtStreetNo.Text;
            mCustomers.Mobile = txtMobile.Text;
            mCustomers.FirstName = txtFirstName.Text;
            mCustomers.Email = txtEmail.Text;
            mCustomers.LastName = txtLastName.Text;
            mCustomers.PostCode = txtPostCode.Text;
            mCustomers.Company = Convert.ToInt32(cbbCompany.SelectedValue);
            mCustomers.Accountlimit = Convert.ToInt32(mMoneyFortmat.getFortMat(txtAccountLimit.Text));
            mCustomers.Balance = Convert.ToInt32(mMoneyFortmat.getFortMat(txtBalance.Text) * -1);
            //mCustomers.Accountlimit = Convert.ToInt32(DataObject.Utilities.ReFormatStringToNumber(txtAccountLimit.Text)) * 1000;
            //mCustomers.Balance = Convert.ToInt32(DataObject.Utilities.ReFormatStringToNumber(txtBalance.Text)) * 1000 * -1;
            mCustomers.MemberNo = txtMemberNo.Text;
            mCustomers.IsActive = cbActive.Checked;
        }

        private void LoadCompany()
        {
            try
            {
                mConnection.Open();
                string strSQL = "SELECT idCompany As ID , companyName As Name FROM company where Active = 1 And deleted = 0 Order by Name;";
                DataTable dt = mConnection.Select(strSQL);
                DataRow row = dt.NewRow();
                row[0] = 0;
                row[1] = "None company";
                dt.Rows.InsertAt(row, 0);
                cbbCompany.DataSource = dt;
                cbbCompany.DisplayMember = "Name";
                cbbCompany.ValueMember = "ID";
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadCompany::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadCustomer()
        {
            SetValue();
            string lastName = "";
            string firstName = "";
            string mobile = "";
            string memberNo = "";
            string email = "";
            string streetNo = "";
            string streetName = "";
            switch (cbbType.SelectedIndex)
            {
                case 0:
                    firstName = txtSreach.Text;
                    break;

                case 1:
                    lastName = txtSreach.Text;

                    break;

                case 2:
                    mobile = txtSreach.Text;
                    break;

                case 3:
                    memberNo = txtSreach.Text;
                    break;

                case 4:
                    email = txtSreach.Text;
                    break;

                case 5:
                    streetNo = txtSreach.Text;
                    break;

                case 6:
                    streetName = txtSreach.Text;
                    break;
            }
            lvCustomer.Items.Clear();
            List<DataObject.Customers> lsArray = BusinessObject.BOCustomers.GetAll(lastName, firstName, mobile, memberNo, email, streetNo, streetName, "", ucLimit.mLimit);
            ucLimit.PageNumReload();
            foreach (DataObject.Customers item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.MemberNo);
                if (mReadConfig.IsEnableCompany)
                    li.SubItems.Add(item.FirstName);
                else
                    li.SubItems.Add(item.FirstName + " " + item.LastName);
                li.SubItems.Add(item.LastName);
                li.SubItems.Add(item.CompanyName);
                li.SubItems.Add(item.Email);
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Balance * -1, 3));
                li.SubItems.Add(DataObject.MonneyFormat.Format2(item.Accountlimit, 3));
                li.SubItems.Add(item.StreetNo);
                li.SubItems.Add(item.StreetName);

                li.Tag = item;
                lvCustomer.Items.Add(li);
            }
        }

        private void LoadListViewItem()
        {
            if (lvCustomer.SelectedItems.Count > 0)
            {
                lvCustomer.SelectedItems[0].Tag = mCustomers.Copy();
                ListViewItem li = lvCustomer.SelectedItems[0];
                DataObject.Customers cus = (DataObject.Customers)li.Tag;
                li.SubItems[0].Text = cus.MemberNo;
                li.SubItems[1].Text = cus.FirstName;
                li.SubItems[2].Text = cus.LastName;
                li.SubItems[3].Text = cus.CompanyName;
                li.SubItems[4].Text = cus.Email;
                li.SubItems[5].Text = DataObject.MonneyFormat.Format2(cus.Balance * -1, 3);
                li.SubItems[6].Text = DataObject.MonneyFormat.Format2(cus.Accountlimit, 3);
            }
        }

        private void LoadRego()
        {
            lvRego.Items.Clear();
            foreach (DataObject.Rego item in lsRego)
            {
                ListViewItem li = new ListViewItem(item.CarNumber);
                li.Tag = item;
                lvRego.Items.Add(li);
            }
        }

        private void LockButton()
        {
            if (mCustomers == null)
            {
                btnPrinter.Enabled = false;
                btnHistoryTransaction.Enabled = false;
                btnCredit.Enabled = false;
                btnDelete.Enabled = false;
                txtBalance.Enabled = true;
                cbbCompany.Enabled = true;
            }
            else
            {
                btnPrinter.Enabled = true;
                btnHistoryTransaction.Enabled = true;
                btnDelete.Enabled = true;
                btnCredit.Enabled = true;
                txtBalance.Enabled = false;
                cbbCompany.Enabled = false;
            }
        }

        private void lvCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCustomer.SelectedItems.Count > 0)
            {
                mCustomers = (DataObject.Customers)lvCustomer.SelectedItems[0].Tag;
                SetValue();
            }
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPort.SetText("", txtMemberNo);
                IsMemberNo = true;
                mSerialPort.SetText(data, txtMemberNo);
                SetValue();
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string cc = "";
            cc = mCustomers.MemberNo;
            if (cc.Length > 12)
            {
                cc = cc.Substring(0, 12);
            }

            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;

            string header = "";
            string bankCode = "";
            string address = "";
            string address1 = "";
            string tell = "";
            string website = "";
            string thankyou = "";
            string terminal = "";

            try
            {
                header = dbconfig.Header1.ToString();
                bankCode = dbconfig.Header2.ToString();
                address = dbconfig.Header3.ToString();
                tell = dbconfig.Header4.ToString();
                address1 = dbconfig.Header5.ToString();
                website = dbconfig.FootNode1.ToString();
                thankyou = dbconfig.FootNode2.ToString();
                terminal = dbconfig.CableID.ToString();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "printDocument_PrintPage::" + ex.Message);
            }

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address1, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += mPrinter.GetHeightPrinterLine();

            DateTime dateTime = DateTime.Now;
            mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine() * 2;

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y = mPrinter.DrawString("Account", e, new System.Drawing.Font("Arial", 15), l_y, 2);
            mPrinter.DrawBarcode(cc, e, l_y, 2);
            l_y += mPrinter.GetHeightPrinterLine() * 4;

            if (mCustomers.FirstName != "")
                l_y = mPrinter.DrawString("Name: " + mCustomers.FirstName + " " + mCustomers.LastName, e, font11, l_y, 1);
            //if (mCustomers.LastName != "")
            //    l_y = mPrinter.DrawString("Last Name: " + mCustomers.LastName, e, font11, l_y, 1);
            if (mCustomers.PostCode != "")
                l_y = mPrinter.DrawString("Post Code: " + mCustomers.PostCode, e, font11, l_y, 1);
            if (mReadConfig.IsEnableCompany && cbbCompany.Text != "")
                l_y = mPrinter.DrawString("Company: " + cbbCompany.Text, e, font11, l_y, 1);
            if (mCustomers.Mobile != "")
                l_y = mPrinter.DrawString("Mobile: " + mCustomers.Mobile, e, font11, l_y, 1);
            if (mCustomers.StreetNo != "")
                l_y = mPrinter.DrawString("Street No: " + mCustomers.StreetNo, e, font11, l_y, 1);
            if (mCustomers.StreetName != "")
                l_y = mPrinter.DrawString("Street Name: " + mCustomers.StreetName, e, font11, l_y, 1);
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);

            l_y = mPrinter.DrawString("Balance: $" + DataObject.MonneyFormat.Format2(mCustomers.Balance, 3), e, font11, l_y, 1);
            if (Convert.ToInt32(cbbCompany.SelectedValue) == 0)
            {
                l_y = mPrinter.DrawString("Limit: $" + DataObject.MonneyFormat.Format2(mCustomers.Accountlimit, 3), e, font11, l_y, 1);
            }
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            int i = 1;
            if (mCustomers.RegoItem != null)
                foreach (DataObject.Rego item in mCustomers.RegoItem)
                {
                    l_y = mPrinter.DrawString("Car " + i + "", e, font11, l_y, 2);
                    l_y = mPrinter.DrawString("--Car number: " + item.CarNumber, e, font11, l_y, 1);
                    l_y = mPrinter.DrawString("--Car color: " + item.CarColor, e, font11, l_y, 1);
                    l_y = mPrinter.DrawString("--Car Product: " + item.CarProduct, e, font11, l_y, 1);
                    i++;
                }
        }

        private string RandomNumber()
        {
            DateTime tmeNow = DateTime.Now;
            int mls = tmeNow.Millisecond;
            Random rndNumber = new Random(mls);
            int NewNumber1 = rndNumber.Next(100, 999);
            int NewNumber2 = rndNumber.Next(100, 999);
            int NewNumber3 = rndNumber.Next(100, 999);
            int NewNumber4 = rndNumber.Next(100, 999);
            String strItemNumber = NewNumber1.ToString() + NewNumber2.ToString() + NewNumber3.ToString() + NewNumber4.ToString();
            return strItemNumber;
        }

        private void SetValue()
        {
            IsMemberNo = false;
            LockButton();
            if (mCustomers == null)
            {
                txtLastName.Text = "";
                txtFirstName.Text = "";
                txtMobile.Text = "";
                txtPostCode.Text = "";
                txtStreetName.Text = "";
                txtStreetNo.Text = "";
                txtBalance.Text = "0";
                txtBalance.Tag = 0; //Dùng trong tính tiền cũ, vì Credit sử dụng text này, nếu đổi dấu sẽ ra sai, nên dùng cái biến này lưu giá trị cũ
                txtEmail.Text = "";
                txtAccountLimit.Text = mReadConfig.AccountLimit;
                cbbCompany.SelectedIndex = 0;
                cbActive.Checked = true;
                lvRego.Items.Clear();
                lsRego = null;
                txtMemberNo.Text = GetCustCode();
            }
            else
            {
                txtLastName.Text = mCustomers.LastName;
                txtFirstName.Text = mCustomers.FirstName;
                txtMemberNo.Text = mCustomers.MemberNo;
                txtMobile.Text = mCustomers.Mobile;
                txtEmail.Text = mCustomers.Email;
                txtStreetNo.Text = mCustomers.StreetNo;
                txtStreetName.Text = mCustomers.StreetName;
                txtPostCode.Text = mCustomers.PostCode;
                txtAccountLimit.Text = DataObject.MonneyFormat.Format(mCustomers.Accountlimit);
                txtBalance.Text = DataObject.MonneyFormat.Format(mCustomers.Balance * -1);
                txtBalance.Tag = DataObject.MonneyFormat.Format(mCustomers.Balance);//Dùng trong tính tiền cũ, vì Credit sử dụng text này, nếu đổi dấu sẽ ra sai, nên dùng cái biến này lưu giá trị cũ
                this.Invoke(new MethodInvoker(delegate() { cbbCompany.SelectedValue = mCustomers.Company; }));
                cbActive.Checked = mCustomers.IsActive;
                if (mCustomers.CustID > 0)
                {
                    mCustomers.RegoItem = BusinessObject.BORego.GetAll(mCustomers.CustID);
                    lsRego = mCustomers.RegoItem;
                    LoadRego();
                }
                else
                {
                    lvRego.Items.Clear();
                }
            }
        }

        private void txtMemberNo_TextChanged(object sender, EventArgs e)
        {
            if (IsMemberNo)
            {
                mCustomers = BusinessObject.BOCustomers.GetByMemberNo(txtMemberNo.Text);
                SetValue();
            }
        }

        private void txtSreach_TextChanged(object sender, EventArgs e)
        {
            btnSearch_Click(sender, e);
        }

        private void ucLimit_EventPage(DataObject.Limit limit)
        {
            LoadCustomer();
        }

        #region Barcode

        public string CalculateChecksumDigit()
        {
            string resuilt = "";
            Random rd = new Random();
            for (int i = 0; i < 9; i++)
            {
                resuilt += rd.Next(10);
            }
            string sTemp = "893" + resuilt;
            int iSum = 0;
            int iDigit = 0;
            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                if (i % 2 == 0)
                {
                    iSum += iDigit * 3;
                }
                else
                {
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
        }

        public string GetCustCode()
        {
            string result = CalculateChecksumDigit();
            while (Convert.ToInt16(mConnection.ExecuteScalar("select count(memberNo) from customers where memberNo=" + result)) > 0)
            {
                result = CalculateChecksumDigit();
            }
            return result;
        }

        private string RandomBarcode()
        {
            string scancode = CalculateChecksumDigit();
            int ishave = 1;
            try
            {
                mConnection.Open();
                while (ishave > 0)
                {
                    ishave = Convert.ToInt32(mConnection.ExecuteScalar("select count(memberNo) from customers where memberNo=" + scancode));
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
            return scancode;
        }

        #endregion Barcode

        private DataTable ViewAccount(string custNo)
        {
            DataTable dt = new DataTable();
            try
            {
                mConnection.Open();
                dt = mConnection.Select("select accountlimit,debt from customers where memberNo=" + "'" + custNo + "'");
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ViewAccount:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            return dt;
        }
    }
}