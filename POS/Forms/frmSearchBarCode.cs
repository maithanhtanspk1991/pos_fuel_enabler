﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmSearchBarCode : Form
    {
        private Connection.Connection mConnection = new Connection.Connection();
        private Class.MoneyFortmat mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        private Class.ProcessOrderNew.Item mItem;

        public Class.ProcessOrderNew.Item ItemMenu { get { return mItem; } }

        private Barcode.SerialPort mSerialPort;
        private int intPumpID;
        private int intIDPumpHistory;
        private DataObject.FindItemType mtypefinditem;


        public Class.ItemComboDisc[] cboListItem;
        private int ruleId = -1;
        TextBox _txtBarCode;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmSearchBarCode(DataObject.FindItemType typefinditem)
        {
            InitializeComponent();
            this.mtypefinditem = typefinditem;
        }

        public frmSearchBarCode(Barcode.SerialPort ser, DataObject.FindItemType typefinditem, int rule)
        {
            InitializeComponent();
            mSerialPort = ser;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            this.mtypefinditem = typefinditem;
            ruleId = rule;
            DisplayComboDisc();
        }

        public frmSearchBarCode(Barcode.SerialPort ser, DataObject.FindItemType typefinditem)
        {
            InitializeComponent();
            mSerialPort = ser;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            this.mtypefinditem = typefinditem;
        }

        public frmSearchBarCode(string BarCode)
        {
            InitializeComponent();
            mSerialPort = null;
            label8.Visible = false;
            txtBarcode.Visible = false;
            btnAddItems.Visible = false;
        }

        public frmSearchBarCode(Barcode.SerialPort ser, int intPumpID, int intIDPumpHistory, DataObject.FindItemType typefinditem)
        {
            InitializeComponent();
            SetMultiLanguage();
            this.intPumpID = intPumpID;
            this.intIDPumpHistory = intIDPumpHistory;
            if (ser != null)
            {
                mSerialPort = ser;
                mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            }
            this.mtypefinditem = typefinditem;
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnSearch.Text = "Tìm kiếm";
                btnAddItems.Text = "Thêm sản phẩm";
                btnSelect.Text = "Chọn";
                btnExit.Text = "Thoát";
                label1.Text = "Tên sản phẩm:";
                label8.Text = "Mã vạch:";
                columnHeader1.Text = "Mã SP";
                columnHeader2.Text = "Mã vạch";
                columnHeader3.Text = "Tên sản phẩm";
                columnHeader4.Text = "Đơn vị giá";
                columnHeader5.Text = "Thuế";
                return;
            }
        }

        private void DisplayComboDisc()
        {
            lvMenu.Columns.Clear();
            lvMenu.Columns.Add(" ", 20, HorizontalAlignment.Center);
            lvMenu.Columns.Add("ItemID", 80, HorizontalAlignment.Left);
            lvMenu.Columns.Add("Scan Code", 200, HorizontalAlignment.Left);
            lvMenu.Columns.Add("Item Name", 350, HorizontalAlignment.Left);
            lvMenu.Columns.Add("Unit Price", 100, HorizontalAlignment.Right);
            lvMenu.Columns.Add("GST", 60, HorizontalAlignment.Left);
            lvMenu.View = View.Details;
            lvMenu.CheckBoxes = true;

            btnAddItems.Visible = true;
            btnAddItems.Location = new Point(btnSelect.Location.X, btnSelect.Location.Y);
            btnSelect.Visible = false;
        }

        #region Display Find Stock

        private void DisplayFindStock()
        {
            lvMenu.Columns.Clear();
            lvMenu.Columns.Add("ItemID");
            lvMenu.Columns.Add("Barcode");
            lvMenu.Columns.Add("ItemName");
            lvMenu.Columns.Add("Qty On Hand");
            lvMenu.Columns[0].Width = 1 / 10 * lvMenu.Width;
            lvMenu.Columns[1].Width = 3 / 10 * lvMenu.Width;
            lvMenu.Columns[2].Width = 4 / 10 * lvMenu.Width;
            lvMenu.Columns[3].Width = 2 / 10 * lvMenu.Width;
            lvMenu.View = View.Details;
        }

        #endregion Display Find Stock

        private void mSerialPort_Received(string data)
        {
            mSerialPort.SetText(data, _txtBarCode);
            mSerialPort.SetText(data, txtBarcode);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchBarCode();
        }

        private void SearchBarCode()
        {
            if (txtItemName.Text == "" && txtBarcode.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Mã vạch và tên sản phẩm bị trống!";
                    return;
                }
                lvMenu.Items.Clear();
                lbStatus.Text = "Barcode and item name empty!";
            }
            else if (txtBarcode.Text.Length < 3 && txtItemName.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Từ khóa tìm kiếm của mã vạch phải lớn hơn ít nhất 3 từ.";
                    return;
                }
                lvMenu.Items.Clear();
                lbStatus.Text = "Keyword length search of barcode must be great more than three word.";
            }
            else if (txtBarcode.Text == "" && txtItemName.Text.Length < 3)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Từ khóa tìm kiếm của tên sản phẩm phải lớn hơn ít nhất 3 từ.";
                    return;
                }
                lvMenu.Items.Clear();
                lbStatus.Text = "Keyword length search of Item Name must be great more than three word.";
            }
            else
            {
                try
                {
                    if (mtypefinditem == DataObject.FindItemType.FindItemInOrder)
                    {
                        lvMenu.Items.Clear();
                        DataTable dt = new DataTable();

                        string sql = "select * from itemsmenu where deleted<>1 and isFuel = 0 and scanCode LIKE " + "'%" + txtBarcode.Text.Trim() + "%' and (itemDesc like '%" + txtItemName.Text + "%' or itemShort like '%" + txtItemName.Text + "%')";
                        if (ucLimit.mLimit.IsLimit)
                        {
                            BusinessObject.BOLimit.GetLimit(sql, ucLimit.mLimit);
                            sql += ucLimit.mLimit.SqlLimit;
                        }
                        sql += ";";
                        ucLimit.PageNumReload();
                        dt = mConnection.Select(sql);
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            ListViewItem lvi;
                            if (ruleId == -1)
                            {
                                lvi = new ListViewItem(row["itemID"].ToString());
                            }
                            else
                            {
                                lvi = new ListViewItem();
                                lvi.SubItems.Add(row["itemID"].ToString());
                            }

                            lvi.SubItems.Add(row["scanCode"].ToString());
                            lvi.SubItems.Add(row["itemDesc"].ToString());
                            lvi.SubItems.Add(mMoneyFortmat.Format2(Convert.ToDouble(row["unitPrice"])));
                            lvi.SubItems.Add(row["gst"].ToString());
                            lvi.SubItems.Add(row["sellSize"].ToString());
                            lvi.Tag = row["enableFuelDiscount"];
                            lvMenu.Items.Add(lvi);
                        }
                        lbStatus.Text = "";
                        if (dt.Rows.Count == 0)
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                lbStatus.Text = "Không tìm thấy!";
                                return;
                            }
                            lbStatus.Text = "Not found!";
                        }
                    }
                    else if (mtypefinditem == DataObject.FindItemType.FindItemInStock)
                    {
                        lvMenu.Items.Clear();
                        DataTable dt = new DataTable();
                        dt = mConnection.Select("select i.itemID,i.scanCode,i.itemShort,i.itemDesc,sl.qtyonhand from itemsmenu i left join stocklevel sl on i.itemID= sl.itemID " +
                            " where  i.deleted<>1 and i.isFuel = 0 and i.scanCode LIKE " + "'%" + txtBarcode.Text.Trim() + "%' and (i.itemDesc like '%" + txtItemName.Text + "%' or i.itemShort like '%" + txtItemName.Text + "%')");


                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            ListViewItem lvi;
                            if (ruleId == -1)
                            {
                                lvi = new ListViewItem(row["itemID"].ToString());
                            }
                            else
                            {
                                lvi = new ListViewItem();
                                lvi.SubItems.Add(row["itemID"].ToString());
                            }
                            lvi.SubItems.Add(row["scanCode"].ToString());
                            lvi.SubItems.Add(row["itemDesc"].ToString());
                            lvi.SubItems.Add(row["qtyonhand"].ToString());
                            lvi.Tag = row["enableFuelDiscount"];
                            lvMenu.Items.Add(lvi);
                        }
                        lbStatus.Text = "";
                        if (dt.Rows.Count == 0)
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                lbStatus.Text = "Không tìm thấy!";
                                return;
                            }
                            lbStatus.Text = "Not found!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("POS::Forms::frmSearchBarCode::SearchBarCode:::" + ex.Message);
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (mSerialPort != null)
            {
                mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            }
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (lvMenu.SelectedIndices.Count > 0)
            {
                try
                {
                    ListViewItem li = lvMenu.SelectedItems[0];
                    mItem = new Class.ProcessOrderNew.Item(intIDPumpHistory, Convert.ToInt32(li.SubItems[0].Text), li.SubItems[2].Text, 1, mMoneyFortmat.getFortMat(li.SubItems[3].Text), mMoneyFortmat.getFortMat(li.SubItems[3].Text), Convert.ToDouble(li.SubItems[4].Text), 0, 0, intPumpID.ToString());
                    mItem.EnableFuelDiscount = Convert.ToInt16(li.Tag);
                    mItem.BarCode = li.SubItems[1].Text;
                    if(Convert.ToInt32(li.SubItems[5].Text)==1000)
                    {
                        mItem.Liter = 1;
                    }
                    if (li.SubItems[3].Text != "")
                    {
                        //mItem.Price = mMoneyFortmat.getFortMat Convert.ToDouble(li.SubItems[3].Text.Trim());
                        mItem.Price = mMoneyFortmat.getFortMat(Convert.ToDouble(li.SubItems[3].Text.Trim()));
                    }
                    if (mSerialPort != null)
                    {
                        mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
                    }
                    mConnection.Open();
                    mConnection.ExecuteNonQuery("insert into itemshistory(PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,IsMenuItems,IsNewItems,IsFindItems,pumpHistoryID) value(" +
                                                intPumpID + "," + Convert.ToInt32(li.SubItems[0].Text) + ",'" + li.SubItems[2].Text + "',1,'" + mMoneyFortmat.getFortMat(li.SubItems[3].Text) + "','" + mMoneyFortmat.getFortMat(li.SubItems[3].Text) + "','" + Convert.ToDouble(li.SubItems[4].Text) + "',0,0,0," + Convert.ToInt16(li.Tag) + ",'" + li.SubItems[1].Text + "',0,0,1," + intIDPumpHistory + ")");
                    mConnection.Close();
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    lbStatus.Text = ex.Message;
                }
            }
            else
            {

                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Vui lòng chọn sản phẩm!!";
                    return;
                }
                lbStatus.Text = "Please select item!!";
            }
        }

        private void lvMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem li in lvMenu.Items)
            {
                if (li.Selected)
                {
                    li.Checked = !li.Checked;
                }
            }
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if (_txtBarCode.Text.Trim().Length > 0)
                SearchBarCode();
        }

        private void frmSearchBarCode_Load(object sender, EventArgs e)
        {
            ucLimit.EventPage += new POS.Controls.UCLimit.MyEventPage(ucLimit_EventPage);
            if (mtypefinditem == DataObject.FindItemType.FindItemInStock)
            {
                DisplayFindStock();
            }
            _txtBarCode = new TextBox();
        }

        void ucLimit_EventPage(DataObject.Limit limit)
        {
            SearchBarCode();
        }

        private void btnAddItems_Click(object sender, EventArgs e)
        {
            try
            {
                cboListItem = new Class.ItemComboDisc[lvMenu.Items.Count];
                int i = 0;
                foreach (ListViewItem lstItem in lvMenu.Items)
                {
                    if (lstItem.Checked)
                    {
                        cboListItem[i++] = new Class.ItemComboDisc(Convert.ToInt32(lstItem.SubItems[1].Text), lstItem.SubItems[3].Text, mMoneyFortmat.getFortMat(lstItem.SubItems[4].Text));
                    }
                }
                if (mSerialPort != null)
                {
                    mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                lbStatus.Text = ex.Message;
            }
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            if (txtItemName.Text.Length > 0)
                SearchBarCode();
        }
    }
}