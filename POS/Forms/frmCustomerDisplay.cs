﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmCustomerDisplay : Form
    {
        private string _nameClass = "POS::Forms::frmCustomerDisplay::";
        private Class.MoneyFortmat money;

        private bool IsFullScreen = true;

        public int Second { get; set; }

        private int mTimeCount = 0;

        public frmCustomerDisplay(Class.MoneyFortmat m)
        {
            InitializeComponent();
            money = m;
            rconfig = new Class.ReadConfig();
            LoadFrameItem();
        }

        private POS.Class.ReadConfig rconfig;

        public void ProcessOrder(Class.ProcessOrderNew.Order order)
        {
            if (order != null)
            {                
                IsFullScreen = order.LoadPanel(pnContent, money, CountItem);                                
                lblTotal.Text = money.Format2(order.getSubTotal());
                lblTotal.Tag = order.getSubTotal();
                lbGST.Text = money.Format2(order.GetTotalGST());
                mTimeCount = 0;
            }
        }

        public double ChangeDiscount
        {
            set
            {
                mTimeCount = 0;
                lbDiscount.Text = money.getFortMat2(value);
            }
        }

        public double ChangeTenter
        {
            set
            {
                mTimeCount = 0;
                lbTender.Text = money.getFortMat2(value);
            }
        }

        public double ChangeCard
        {
            set
            {
                mTimeCount = 0;
                lbCard.Text = money.getFortMat2(value);
            }
        }

        public double ChangeTotal
        {
            set
            {
                mTimeCount = 0;
                lblTotal.Text = money.getFortMat2(value);
            }
        }

        public double ChangeChange
        {
            set
            {
                mTimeCount = 0;
                lbChange.Text = money.getFortMat2(value);
            }
        }

        public double ChangeGST
        {
            set
            {
                mTimeCount = 0;
                lbGST.Text = money.getFortMat2(value);
            }
        }

        public void CancelSubtotal()
        {
            lbTender.Text = money.Format2(0);
            lbCard.Text = money.Format2(0);
            lbChange.Text = money.Format2(0);
            lbDiscount.Text = money.Format2(0);
            lbGST.Text = money.Format2(0);
        }

        private void frmCustomerDisplay_Load(object sender, EventArgs e)
        {
            pnRight.Width = 430;
            pnRight.Tag = 430;
            Second = 30;
            timer1.Start();
            FullScreen();
        }

        public int CountItem { get { return pnContent.Height / 35; } }

        private void LoadFrameItem()
        {
            this.SuspendLayout();
            pnRight.SuspendLayout();
            pnContent.SuspendLayout();
            UCItemCustomerDisplay[] lsUCItemCustomerDisplay = new UCItemCustomerDisplay[CountItem];
            for (int i = 0; i < CountItem; i++)
            {
                lsUCItemCustomerDisplay[i] = new UCItemCustomerDisplay();
                lsUCItemCustomerDisplay[i].Height = 35;
                lsUCItemCustomerDisplay[i].Dock = DockStyle.Top;
            }
            pnContent.Controls.AddRange(lsUCItemCustomerDisplay);
            pnContent.ResumeLayout();
            pnRight.ResumeLayout();
            this.ResumeLayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mTimeCount < Second)
            {
                if (IsFullScreen)
                {
                    FullScreen();
                }
                else
                {
                    DetailScreen();
                }
            }
            else if (mTimeCount == Second)
            {
                if (IsFullScreen)
                {
                    FullScreen();
                }
                else
                {
                    DetailScreen();
                }
            }
            mTimeCount++;
        }

        private void frmCustomerDisplay_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
        }

        public void DetailScreen()
        {
            pnRight.Width = Convert.ToInt32(pnRight.Tag);
            imgCustomerDisplay.Hide();
        }

        public void FullScreen()
        {
            pnRight.Width = 0;
            imgCustomerDisplay.Show();
        }

        public void ShowCustomerDisplay()
        {
            try
            {
                Screen[] sc;
                sc = Screen.AllScreens;
                if (sc.Length >= 2)
                {
                    this.Left = sc[1].Bounds.Width;
                    this.Top = sc[1].Bounds.Height;
                    this.StartPosition = FormStartPosition.Manual;
                    this.Location = sc[1].Bounds.Location;
                    Point p = new Point(sc[1].Bounds.Location.X, sc[1].Bounds.Location.Y);
                    this.Location = p;
                    this.WindowState = FormWindowState.Maximized;
                    this.Show();
                }
            }
            catch (Exception ex)
            {
                SystemLog.LogPOS.WriteLog(_nameClass + "ShowCustomerDisplay::" + ex.Message);
            }
            imgCustomerDisplay.Start();
        }

        public void DeleteAll()
        {
            ClearPnConent();
            CancelSubtotal();
            lblTotal.Text = money.getFortMat2(0);
            FullScreen();
        }

        public void ClearPnConent()
        {
            this.SuspendLayout();
            pnRight.SuspendLayout();
            pnContent.SuspendLayout();
            IsFullScreen = true;
            foreach (Control item in pnContent.Controls)
            {
                UCItemCustomerDisplay uc = (UCItemCustomerDisplay)item;
                uc.Qty = "";
                uc.ItemName = "";
                uc.Subtotal = "";
            }

            pnContent.ResumeLayout();
            pnRight.ResumeLayout();
            this.ResumeLayout();

        }

    }
}