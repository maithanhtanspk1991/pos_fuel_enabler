﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmRego : Form
    {
        private readonly string _nameClass = "POS::Forms::frmRego::";
        public DataObject.Customers mCustomers;
        public List<DataObject.Rego> lsRego { get; set; }
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmRego(DataObject.Customers item)
        {
            InitializeComponent();
            SetMultiLanguage();
            mCustomers = item;
            lsRego = null;
            if (mCustomers != null)
            {
                txtCustomerName.Text = mCustomers.FirstName;
                lsRego = mCustomers.RegoItem;
            }
            else
            {
                lsRego = new List<DataObject.Rego>();
            }
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Danh sách xe";
                btnNew.Text = "Tạo mới";
                btnExit.Text = "Thoát";
                btnAdd.Text = "THÊM";
                btnDelete.Text = "XÓA";
                label1.Text = "Số xe";
                label2.Text = "Màu";
                label3.Text = "Sản phẩm";
                label4.Text = "Tên khách hàng";
                cCarNumber.Text = "Số xe";
                cCarProduct.Text = "Sản phẩm";
                cCarColor.Text = "Màu xe";
                return;
            }
        }

        private void GetList()
        {
            lsRego.Clear();
            foreach (ListViewItem li in lvRego.Items)
            {
                lsRego.Add((DataObject.Rego)li.Tag);
            }
        }

        private void LoadListView()
        {
            lvRego.Items.Clear();

            foreach (DataObject.Rego item in lsRego)
            {
                ListViewItem lvi = new ListViewItem(item.CarNumber);
                lvi.SubItems.Add(item.CarColor);
                lvi.SubItems.Add(item.CarProduct);
                lvi.Tag = item;
                lvRego.Items.Add(lvi);
            }
        }

        private void AddItem(DataObject.Rego item)
        {
            ListViewItem lvi = new ListViewItem(item.CarNumber);
            lvi.SubItems.Add(item.CarColor);
            lvi.SubItems.Add(item.CarProduct);
            lvi.Tag = item;
            lvRego.Items.Add(lvi);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DataObject.Rego item = null;
            if (btnAdd.Tag != null)
                item = (DataObject.Rego)btnAdd.Tag;
            else
                item = new DataObject.Rego();
            item.CarNumber = txtCarNumber.Text;
            item.CarProduct = txtProduct.Text;
            item.CarColor = txtColor.Text;
            if (btnAdd.Tag == null)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    AddItem(item);
                    Clear();
                    lbStatus.Text = "Thêm thành công";
                    return;
                }
                AddItem(item);
                Clear();
                lbStatus.Text = "Add Success full";
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Cập nhật thành công";
                    return;
                }
                lbStatus.Text = "Update Success full";
            }
            GetList();
            LoadListView();

        }

        private void Clear()
        {
            txtColor.Text = string.Empty;
            txtCarNumber.Text = string.Empty;
            txtProduct.Text = string.Empty;
            btnAdd.Text = "ADD";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            btnAdd.Tag = null;
            Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvRego.SelectedItems.Count > 0)
            {
                DataObject.Rego item = (DataObject.Rego)lvRego.SelectedItems[0].Tag;
                if (item.ID != 0)
                {
                    item.Deleted = true;
                    BusinessObject.BORego.Update(item);
                }
                lvRego.SelectedItems[0].Remove();
                GetList();
                LoadListView();
            }
        }

        private void lvRego_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvRego.SelectedItems.Count > 0)
            {
                DataObject.Rego item = (DataObject.Rego)lvRego.Items[lvRego.SelectedIndices[0]].Tag;
                txtCarNumber.Text = item.CarNumber;
                txtColor.Text = item.CarColor;
                txtProduct.Text = item.CarProduct;
                btnAdd.Text = "UPDATE";
                btnAdd.Tag = item;
            }
        }

        private void frmManageCar_Load(object sender, EventArgs e)
        {
            LoadListView();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            GetList();
            if (mCustomers != null)
            {
                foreach (DataObject.Rego item in lsRego)
                {
                    item.CustID = mCustomers.CustID;
                    if (item.ID == 0)
                    {
                        BusinessObject.BORego.Insert(item);
                    }
                    else
                        BusinessObject.BORego.Update(item);
                }
            }
            this.DialogResult = DialogResult.Cancel;
        }
    }
}