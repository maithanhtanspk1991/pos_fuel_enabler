﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLoyalty : Form
    {
        private Class.Loyalty loyal = new Class.Loyalty();
        private Class.MoneyFortmat mMoney;

        public frmLoyalty()
        {
            InitializeComponent();
        }

        public frmLoyalty(string name, string qty, string price, string orderid, Class.MoneyFortmat money)
        {
            InitializeComponent();
            mMoney = money;
            txtitemname.Text = name;
            txtqty.Text = qty;
            txtunitprice.Text = money.Format(Convert.ToDouble(price));
            txtorderid.Text = orderid;
        }

        private void frmLoyalty_Load(object sender, EventArgs e)
        {
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                comboBox1.Enabled = true;
            else
            {
                comboBox1.Enabled = false;
                comboBox1.Text = "";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                txtloyaltyprice.Enabled = true;
                txtloyaltyprice.Focus();
            }
            else
            {
                txtloyaltyprice.Enabled = false;
                txtloyaltyprice.Text = "";
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                txtserial.Enabled = true;
                txtserial.Focus();
            }
            else
            {
                txtserial.Enabled = false;
                txtserial.Text = "";
            }
        }

        public double getpriceloyalty()
        {
            Class.LogPOS.WriteLog("frmLoyalty.getpriceloyalty.");
            label5.Visible = false;
            try
            {
                if (radioButton1.Checked)
                {
                    loyal.orderid = txtorderid.Text;
                    loyal.staffid = txtstaffid.Text;
                    loyal.subloyalty = Convert.ToDouble((Convert.ToDouble(txtunitprice.Text) - (Convert.ToDouble(txtunitprice.Text) * (Convert.ToDouble(comboBox1.SelectedItem)) / 100)));
                    loyal.totalafter = Convert.ToDouble(txtunitprice.Text) - loyal.subloyalty;
                    Class.Loyalty.LoyaltyItem item = new Class.Loyalty.LoyaltyItem();
                    item.itemname = txtitemname.Text;
                    item.Qty = Convert.ToInt32(txtqty.Text);
                    item.subtotal = loyal.subloyalty;
                    item.unitprice = item.subtotal / item.Qty;
                    loyal.addloyaltyitem(item);
                }
                else if (radioButton2.Checked)
                {
                    loyal.orderid = txtorderid.Text;
                    loyal.staffid = txtstaffid.Text;
                    if (Convert.ToDouble(txtloyaltyprice.Text) >= Convert.ToDouble(txtunitprice.Text))
                    {
                        loyal.subloyalty = Convert.ToDouble(txtunitprice.Text);
                        //MessageBox.Show();
                        label5.Visible = true;
                        label5.Text = "Loyalty price must be smaller than Unit price.";
                    }
                    else
                    {
                        loyal.subloyalty = Convert.ToDouble(txtloyaltyprice.Text);
                    }
                    loyal.totalafter = Convert.ToDouble(txtunitprice.Text) - loyal.subloyalty;
                    Class.Loyalty.LoyaltyItem item = new Class.Loyalty.LoyaltyItem();
                    item.itemname = txtitemname.Text;
                    item.Qty = Convert.ToInt32(txtqty.Text);
                    item.subtotal = loyal.subloyalty;
                    item.unitprice = item.subtotal / item.Qty;
                    loyal.addloyaltyitem(item);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmLoyalty.getpriceloyalty:::" + ex.Message);
            }
            return mMoney.getFortMat(loyal.subloyalty);
        }

        private void btnsubmit_Click(object sender, EventArgs e)
        {
            getpriceloyalty();
            if (label5.Visible == false)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void txtloyaltyprice_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void txtloyaltyprice_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }
    }
}