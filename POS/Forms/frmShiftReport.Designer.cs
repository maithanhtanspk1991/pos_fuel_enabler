﻿namespace POS.Forms
{
    partial class frmShiftReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.pnShift = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbCableID = new System.Windows.Forms.ComboBox();
            this.cbbShift = new System.Windows.Forms.ComboBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSafeDrop = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnEndshift = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(207, 44);
            this.label11.TabIndex = 42;
            this.label11.Text = "Date view:";
            // 
            // pnShift
            // 
            this.pnShift.AutoScroll = true;
            this.pnShift.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnShift.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnShift.Location = new System.Drawing.Point(0, 100);
            this.pnShift.Name = "pnShift";
            this.pnShift.Size = new System.Drawing.Size(890, 322);
            this.pnShift.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbbCableID);
            this.panel1.Controls.Add(this.cbbShift);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 100);
            this.panel1.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(407, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 44);
            this.label3.TabIndex = 46;
            this.label3.Text = "Cable ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(711, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 44);
            this.label2.TabIndex = 46;
            this.label2.Text = "Shift:";
            // 
            // cbbCableID
            // 
            this.cbbCableID.DisplayMember = "ShiftName";
            this.cbbCableID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCableID.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCableID.FormattingEnabled = true;
            this.cbbCableID.Location = new System.Drawing.Point(602, 42);
            this.cbbCableID.Name = "cbbCableID";
            this.cbbCableID.Size = new System.Drawing.Size(103, 45);
            this.cbbCableID.TabIndex = 45;
            this.cbbCableID.ValueMember = "shiftID";
            this.cbbCableID.SelectedIndexChanged += new System.EventHandler(this.cbbCableID_SelectedIndexChanged);
            // 
            // cbbShift
            // 
            this.cbbShift.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbShift.DisplayMember = "ShiftName";
            this.cbbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbShift.FormattingEnabled = true;
            this.cbbShift.Location = new System.Drawing.Point(831, 43);
            this.cbbShift.Name = "cbbShift";
            this.cbbShift.Size = new System.Drawing.Size(164, 45);
            this.cbbShift.TabIndex = 45;
            this.cbbShift.ValueMember = "shiftID";
            this.cbbShift.SelectedIndexChanged += new System.EventHandler(this.cbbShift_SelectedIndexChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.AllowDrop = true;
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(216, 43);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(185, 44);
            this.dtpDate.TabIndex = 41;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(998, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shift Report";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 422);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1000, 50);
            this.panel2.TabIndex = 12;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnSafeDrop, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnExit, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnEndshift, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(890, 100);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(110, 322);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnSafeDrop
            // 
            this.btnSafeDrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnSafeDrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSafeDrop.FlatAppearance.BorderSize = 0;
            this.btnSafeDrop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSafeDrop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSafeDrop.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSafeDrop.Location = new System.Drawing.Point(1, 1);
            this.btnSafeDrop.Margin = new System.Windows.Forms.Padding(1);
            this.btnSafeDrop.Name = "btnSafeDrop";
            this.btnSafeDrop.Size = new System.Drawing.Size(108, 105);
            this.btnSafeDrop.TabIndex = 47;
            this.btnSafeDrop.Text = "FINAL DROP";
            this.btnSafeDrop.UseVisualStyleBackColor = false;
            this.btnSafeDrop.Click += new System.EventHandler(this.btnSafeDrop_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(1, 215);
            this.btnExit.Margin = new System.Windows.Forms.Padding(1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(108, 106);
            this.btnExit.TabIndex = 44;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnEndshift
            // 
            this.btnEndshift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(150)))), ((int)(((byte)(255)))));
            this.btnEndshift.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEndshift.FlatAppearance.BorderSize = 0;
            this.btnEndshift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEndshift.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEndshift.ForeColor = System.Drawing.SystemColors.Window;
            this.btnEndshift.Location = new System.Drawing.Point(1, 108);
            this.btnEndshift.Margin = new System.Windows.Forms.Padding(1);
            this.btnEndshift.Name = "btnEndshift";
            this.btnEndshift.Size = new System.Drawing.Size(108, 105);
            this.btnEndshift.TabIndex = 44;
            this.btnEndshift.Text = "END SHIFT";
            this.btnEndshift.UseVisualStyleBackColor = false;
            this.btnEndshift.Click += new System.EventHandler(this.btnEndshift_Click);
            // 
            // frmShiftReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 472);
            this.Controls.Add(this.pnShift);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmShiftReport";
            this.Text = "Shift Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.frmShiftReport_Activated);
            this.Load += new System.EventHandler(this.frmShiftReport_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.FlowLayoutPanel pnShift;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbShift;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnSafeDrop;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnEndshift;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbbCableID;
    }
}