﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmDeposit : Form
    {
        private Connection.Connection con;
        private Printer mPrinter;

        public bool IsNewDeposit { get; set; }

        private Class.MoneyFortmat mMoneyFortmat;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        //private double mResuilt;
        //public double Resuilt { get { return mResuilt; } }
        public frmDeposit(Class.MoneyFortmat m)
        {
            InitializeComponent();
            IsNewDeposit = true;
            mMoneyFortmat = m;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Tiền đặt cọc";
                buttonCancel.Text = "Thoát";
                label5.Text = "Mã";
                label1.Text = "Tên";
                label2.Text = "Số điện thoại";
                label3.Text = "Di động";
                label4.Text = "Tiền đặt cọc";
                return;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmDeposit.buttonOk_Click.");
            bool key = false;
            try
            {
                con.Open();
                con.BeginTransaction();
                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                if (Convert.ToInt16(con.ExecuteScalar("select count(custID) from customers where (phone=" + textBoxNumbericPOSPhone.Text + " or mobile=" + textBoxNumbericPOSMobile.Text + ") and Name like '%" + textBoxPOSKeyBoardName.Text + "%'")) > 0)
                {
                    throw new Exception("Name and phone is inserted");
                }
                if (textBoxNumbericPOSMobile.Text == "" && textBoxNumbericPOSPhone.Text == "")
                {
                    throw new Exception("Phone number is empty !!");
                }
                con.ExecuteNonQuery("insert into customers(Name,phone,mobile) values('" + textBoxPOSKeyBoardName.Text + "','" + textBoxNumbericPOSPhone.Text + "','" + textBoxNumbericPOSMobile.Text + "')");
                int custID = Convert.ToInt16(con.ExecuteScalar("select max(custID) from customers"));
                con.ExecuteNonQuery("insert into depositorders(custID,depositTotal,depositCode) values(" + custID + "," + mMoneyFortmat.getFortMat(textBoxNumbericPOSDeposit.Text) + ",' ')");
                string depositID = con.ExecuteScalar("select max(depositID) from depositorders").ToString();
                string depositCode = depositID + "" + DateTime.Now.Millisecond + DateTime.Now.Minute;
                con.ExecuteNonQuery("update depositorders set depositCode=" + depositCode + " where depositID=" + depositID);
                textBoxNumbericPOSCode.Text = depositCode;
                double deposit = 0;
                try
                {
                    deposit = Convert.ToDouble(textBoxNumbericPOSDeposit.Text);
                }
                catch (Exception)
                {
                }
                if (deposit <= 0)
                {
                    throw new Exception("Please insert deposit!");
                }
                if (textBoxNumbericPOSPhone.Text.Length < 1 && textBoxNumbericPOSMobile.Text.Length < 1)
                {
                    throw new Exception("Please insert Phone or Mobile Number!");
                }
                mPrinter.Print();
                key = true;
                con.Commit();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmDeposit.buttonOk_Click:::" + ex.Message);
                labelError.Text = ex.Message;
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            if (key)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void frmDeposit_Load(object sender, EventArgs e)
        {
            con = new Connection.Connection();
            mPrinter = new Printer();
            mPrinter.printDocument.PrinterSettings.PrinterName = new Class.Setting().GetBillPrinter();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            if (IsNewDeposit)
            {
                textBoxNumbericPOSCode.Enabled = false;
            }
            else
            {
                textBoxNumbericPOSDeposit.Enabled = false;
                buttonCancel.Text = "Close";
                buttonOk.Text = "Find";
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;
            string header = "BECAS POS DEMO";
            string bankCode = "ABN. 1234567890";
            string address = "133 Alexander Street, Crows Nest 2065";
            string tell = "T:94327867 M:0401950967";

            l_y = DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = DrawString(bankCode, e, font11, l_y, 2);
            l_y = DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 100;

            DateTime dateTime = DateTime.Now;
            l_y = DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);

            l_y += 200;

            l_y = DrawString(textBoxNumbericPOSCode.Text, e, new System.Drawing.Font("Arial", 18, System.Drawing.FontStyle.Bold), l_y, 2);

            l_y += 200;

            l_y = DrawString("Name: " + textBoxPOSKeyBoardName.Text, e, new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Italic), l_y, 1);
            l_y = DrawString("Phone: " + textBoxNumbericPOSPhone.Text, e, new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Italic), l_y, 1);
            l_y = DrawString("Mobile: " + textBoxNumbericPOSMobile.Text, e, new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Italic), l_y, 1);
            l_y = DrawString("Deposit: " + String.Format("{0:0.00}", Convert.ToDouble(textBoxNumbericPOSDeposit.Text)), e, new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Italic), l_y, 1);
        }

        private float DrawString(string s, System.Drawing.Printing.PrintPageEventArgs e, System.Drawing.Font font, float y, int textAlign)
        {
            float x;
            if (textAlign == 1)
            {
                x = 0;
            }
            else if (textAlign == 2)
            {
                x = (float)Math.Abs(((float)e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width) / 2);
            }
            else
            {
                x = e.PageBounds.Width - e.Graphics.MeasureString(s, font).Width;
            }
            e.Graphics.DrawString(s, font, System.Drawing.Brushes.Black, x, y);
            y += e.Graphics.MeasureString(s, font).Height;

            return y;
        }
    }
}