﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmSetPump : Form
    {
        private const string _nameClass = "POS::Forms::frmSetPump::";
        private Connection.Connection mconnect = new Connection.Connection();
        private Class.MoneyFortmat money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        private string pumpID = "";

        public frmSetPump()
        {
            InitializeComponent();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                button5.Text = "THOÁT";
                columnHeader2.Text = "Tên";
                columnHeader3.Text = "Giá";
                columnHeader5.Text = "Tên";
                columnHeader6.Text = "Giá";
                return;
            }
        }

        private void frmSetPump_Load(object sender, EventArgs e)
        {
            initialize16();
            GetFuelItem();
        }

        private class Items
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public double UnitPrice { get; set; }

            public Items(string itemid, string name, double price)
            {
                ItemID = itemid;
                ItemName = name;
                UnitPrice = price;
            }
        }

        private void GetFuelItem()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select itemID,scanCode,itemShort,itemDesc,unitPrice,groupID from itemsmenu where isFuel=1";
                mconnect.Open();
                dt = mconnect.Select(sql);
                mconnect.Close();
                foreach (DataRow row in dt.Rows)
                {
                    Items item = new Items(row["itemID"].ToString(), row["itemDesc"].ToString(), Convert.ToDouble(row["unitPrice"].ToString()));
                    ListViewItem lvi = new ListViewItem(item.ItemName);
                    lvi.SubItems.Add(money.Format(item.UnitPrice.ToString()));
                    lvi.Tag = item;
                    listView2.Items.Add(lvi);
                }
            }
            catch (Exception)
            {
            }
        }

        private void initialize16()
        {
            for (int i = 1; i <= 16; i++)
            {
                Button btn = new Button();
                btn.Name = "" + i;
                btn.Text = "" + i;
                btn.Tag = 1;
                btn.Font = new System.Drawing.Font("Arial", 24);
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                btn.BackColor = Color.White;
                btn.Dock = System.Windows.Forms.DockStyle.Fill;
                btn.Click += new EventHandler(btn_Click);
                btn.Leave += new EventHandler(btn_Leave);
                flp_takeaway.Controls.Add(btn);
            }
        }

        private void btn_Leave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.Tag = 1;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GetFueltypebypumid(btn.Name);
            pumpID = btn.Name;
            btn.Tag = 0;
            foreach (Button but in flp_takeaway.Controls)
            {
                if (Convert.ToInt32(but.Tag) == 0)
                {
                    but.BackColor = System.Drawing.Color.Blue;
                }
                else
                {
                    but.BackColor = System.Drawing.Color.White;
                }
            }
        }

        private class Pump
        {
            public string PumpID { get; set; }

            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public double UnitPrice { get; set; }

            public double Qty { get; set; }

            public double SubTotal { get; set; }

            public string ScanCode { get; set; }

            public Pump(string pumpid, string itemid, double unitprice, string itemname, string scancode)
            {
                PumpID = pumpid;
                ItemID = itemid;
                UnitPrice = unitprice;
                ItemName = itemname;
                ScanCode = scancode;
            }
        }

        private void GetFueltypebypumid(string pumpid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select h.*,i.scanCode,i.itemShort,i.itemDesc,i.unitPrice,i.groupID from headpump h inner join itemsmenu i on h.itemID=i.itemID where h.pumpID=" + pumpid + " and i.visual=1 group by itemID";
                mconnect.Open();
                dt = mconnect.Select(sql);
                mconnect.Close();
                listView1.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    Pump pum = new Pump(row["pumpID"].ToString(), row["itemID"].ToString(), Convert.ToDouble(row["unitPrice"].ToString()), row["itemDesc"].ToString(), row["scanCode"].ToString());
                    ListViewItem lvi = new ListViewItem(pum.ItemName);
                    lvi.SubItems.Add(money.Format(pum.UnitPrice.ToString()));
                    lvi.Tag = pum;
                    listView1.Items.Add(lvi);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "GetFueltypebypumid::" + ex.Message);
            }
        }

        private void AddAllPump(string pump, string itemid)
        {
            try
            {
                string sql = "insert into headpump(itemID,pumpID) values('" + itemid + "','" + pump + "')";
                mconnect.Open();
                mconnect.BeginTransaction();
                mconnect.ExecuteNonQuery(sql);
                mconnect.Commit();
            }
            catch (Exception ex)
            {
                mconnect.Rollback();
                Class.LogPOS.WriteLog(_nameClass + "AddAllPump::" + ex.Message);
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void RemoveAllPump(string pump)
        {
            try
            {
                string sql = "delete from headpump where pumpID=" + pump;
                mconnect.Open();
                mconnect.BeginTransaction();
                mconnect.ExecuteNonQuery(sql);
                mconnect.Commit();
            }
            catch (Exception ex)
            {
                mconnect.Rollback();
                Class.LogPOS.WriteLog(_nameClass + "RemoveAllPump::" + ex.Message);
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void RemoveOnePump(string pump, string itemid)
        {
            try
            {
                string sql = "delete from headpump where pumpID='" + pump + "' and itemID='" + itemid + "'";
                mconnect.Open();
                mconnect.BeginTransaction();
                mconnect.ExecuteNonQuery(sql);
                mconnect.Commit();
            }
            catch (Exception ex)
            {
                mconnect.Rollback();
                Class.LogPOS.WriteLog(_nameClass + "RemoveOnePump::" + ex.Message);
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pumpID != "")
            {
                RemoveAllPump(pumpID);
                GetFueltypebypumid(pumpID);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (pumpID != "")
            {
                if (listView2.Items.Count > 0)
                {
                    RemoveAllPump(pumpID);
                    for (int i = 0; i < listView2.Items.Count; i++)
                    {
                        Items item = (Items)listView2.Items[i].Tag;
                        AddAllPump(pumpID, item.ItemID);
                    }
                    GetFueltypebypumid(pumpID);
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                Pump pum = (Pump)listView1.SelectedItems[0].Tag;
                RemoveOnePump(pumpID, pum.ItemID);
                GetFueltypebypumid(pumpID);
            }
        }

        private int CheckFuelIShave(string itemID, string pumid)
        {
            int count = 1;
            try
            {
                string sql = "select count(fID) from headpump where itemID=" + itemID + " and pumpID=" + pumid;
                mconnect.Open();
                count = Convert.ToInt16(mconnect.ExecuteScalar(sql));
                mconnect.Close();
                return count;
            }
            catch (Exception)
            {
                return count;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                Items item = (Items)listView2.SelectedItems[0].Tag;
                if (CheckFuelIShave(item.ItemID, pumpID) == 0)
                {
                    AddAllPump(pumpID, item.ItemID);
                    GetFueltypebypumid(pumpID);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}