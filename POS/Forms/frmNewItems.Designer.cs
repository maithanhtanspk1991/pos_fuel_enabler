﻿namespace POS.Forms
{
    partial class frmNewItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.txtunitprice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uCkeypad1 = new POS.UCkeypad();
            this.posLabelStatus1 = new POS.POSLabelStatus();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(12, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(220, 148);
            this.button2.TabIndex = 31;
            this.button2.Text = "GO BACK";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(236, 125);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(220, 148);
            this.button1.TabIndex = 30;
            this.button1.Text = "SELECT";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtitemname
            // 
            this.txtitemname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtitemname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(122, 9);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(334, 38);
            this.txtitemname.TabIndex = 26;
            this.txtitemname.Click += new System.EventHandler(this.txtitemname_Click);
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            this.txtitemname.Leave += new System.EventHandler(this.txtitemname_Leave);
            // 
            // txtunitprice
            // 
            this.txtunitprice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtunitprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunitprice.Location = new System.Drawing.Point(122, 53);
            this.txtunitprice.Name = "txtunitprice";
            this.txtunitprice.Size = new System.Drawing.Size(334, 38);
            this.txtunitprice.TabIndex = 29;
            this.txtunitprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtunitprice.Enter += new System.EventHandler(this.txtunitprice_Enter);
            this.txtunitprice.Leave += new System.EventHandler(this.txtunitprice_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Item name :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Unit Price :";
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(462, 3);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 34;
            this.uCkeypad1.txtResult = null;
            // 
            // posLabelStatus1
            // 
            this.posLabelStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLabelStatus1.ForeColor = System.Drawing.Color.Red;
            this.posLabelStatus1.Location = new System.Drawing.Point(16, 94);
            this.posLabelStatus1.Name = "posLabelStatus1";
            this.posLabelStatus1.POSTimeMessenge = 1;
            this.posLabelStatus1.Size = new System.Drawing.Size(428, 28);
            this.posLabelStatus1.TabIndex = 33;
            this.posLabelStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmNewItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 280);
            this.Controls.Add(this.uCkeypad1);
            this.Controls.Add(this.posLabelStatus1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtitemname);
            this.Controls.Add(this.txtunitprice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewItems";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NEW ITEMS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.TextBox txtunitprice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private POSLabelStatus posLabelStatus1;
        private UCkeypad uCkeypad1;


    }
}