﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmStock : Form
    {
        private UCReceiveStock mUCReceiveStock;
        private UCCheckStock mUCCheckStock;
        private Barcode.SerialPort mSerialPort;
        private Connection.Connection mConnection;

        public int AreasID { get; set; }

        public frmStock(Barcode.SerialPort port)
        {
            InitializeComponent();
            mSerialPort = port;
            mConnection = new Connection.Connection();
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.STOCK;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
        }

        private void mSerialPort_Received(string data)
        {
            mSerialPort.SetText("", txtBarcode);
            mSerialPort.SetText(data, txtBarcode);
            //if (mUCCheckStock!=null)
            //{
            //    if (mUCCheckStock.Visible)
            //    {
            //        try
            //        {
            //            mConnection.Open();
            //            DataTable tbl= mConnection.Select("select itemID,itemDesc from itemsmenu where scanCode ='"+data+"'");
            //            if (tbl.Rows.Count>0)
            //            {
            //                mUCCheckStock.CheckStock(tbl.Rows[0]["itemDesc"].ToString(), tbl.Rows[0]["itemID"].ToString());
            //            }
            //        }
            //        catch (Exception)
            //        {
            //        }
            //        finally
            //        {
            //            mConnection.Close();
            //        }
            //    }
            //}
        }

        private void frmStock_Load(object sender, EventArgs e)
        {
            //ucCheckStock1.InitMenu();
            //ucCheckStock1.ucItem1.ItemClick += new UCItem.MyEventHandler(ucItem1_ItemClick);
            mUCReceiveStock = new UCReceiveStock();
            mUCCheckStock = new UCCheckStock();
            panel2.Controls.Add(mUCReceiveStock);
            panel2.Controls.Add(mUCCheckStock);
            mUCCheckStock.InitMenu();
            mUCReceiveStock.Visible = true;
            mUCCheckStock.Visible = false;
        }

        private void btnReceied_Click(object sender, EventArgs e)
        {
            mUCReceiveStock.Visible = true;
            mUCCheckStock.Visible = false;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            mUCReceiveStock.Visible = false;
            mUCCheckStock.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if (txtBarcode.Text != "")
            {
                if (mUCCheckStock != null)
                {
                    if (mUCCheckStock.Visible)
                    {
                        try
                        {
                            mConnection.Open();
                            DataTable tbl = mConnection.Select("select itemID,itemDesc from itemsmenu where scanCode ='" + txtBarcode.Text + "'");
                            if (tbl.Rows.Count > 0)
                            {
                                mUCCheckStock.CheckStock(tbl.Rows[0]["itemDesc"].ToString(), tbl.Rows[0]["itemID"].ToString());
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            mConnection.Close();
                        }
                    }
                }
            }
        }
    }
}