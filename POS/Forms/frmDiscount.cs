﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmDiscount : Form
    {
        private Class.MoneyFortmat mMoneyFortmat;
        private bool mIsLockText = false;

        public bool IsManager
        {
            set { mIsManager = value; }
        }

        private bool mIsManager = false;

        public string ItemName { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public double Total { get; set; }

        public double Discount { get; set; }

        public string DiscountName { get; set; }

        public int DiscountID { get; set; }

        public int ItemKey { get; set; }

        public int ShiftID { get; set; }

        public int CableID { get; set; }
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmDiscount()
        {
            InitializeComponent();
            SetMultiLanguage();
        }

        public frmDiscount(string itemName, double price, int qty, double total)
        {
            InitializeComponent();
            ItemName = itemName;
            Qty = qty;
            Price = price;
            Total = total;
        }

        private void frmDiscount_Load(object sender, EventArgs e)
        {
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            textBoxPOSKeyBoardItemName.Text = ItemName;
            textBoxPOSKeyPadQty.Text = Qty + "";
            textBoxPOSKeyPadUnitPrice.Text = mMoneyFortmat.Format(Price);
            textBoxPOSKeyPadTotal.Text = mMoneyFortmat.Format(Total);
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Giảm giá";
                label1.Text = "Tên sản phẩm:";
                label4.Text = "Đơn vị giá:";
                label3.Text = "SL:";
                label2.Text = "Tổng cộng:";
                groupBox1.Text = "Giảm giá";
                label5.Text = "Phần trăm";
                label6.Text = "Giảm giá";
                btnLogin.Text = "Đăng nhập";
                btnSubmit.Text = "Lưu lại";
                btnCancel.Text = "Thoát";
                return;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (textBoxPOSKeyPadPerCent.Text == "" || textBoxPOSKeyPadPrice.Text == "" || textBoxPOSKeyPadPerCent.Text == "0" || textBoxPOSKeyPadPerCent.Text == "0")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Sản phẩm bị rỗng!";
                    return;
                }
                lblError.Text = "Field empty!";
                return;
            }
            DiscountName = "Discount " + ItemName;
            Connection.Connection con = new Connection.Connection();
            try
            {
                con.Open();
                con.BeginTransaction();
                con.ExecuteNonQuery("insert into dynitemsmenu(shiftID,itemShort,itemDesc,unitPrice, cableID) value(" + ShiftID + ",'" + DiscountName + "','" + DiscountName + "'," + (-1 * Discount) + ", " + CableID + ")");
                DiscountID = Convert.ToInt32(con.ExecuteScalar("select max(dynID) from dynitemsmenu"));
                con.Commit();
            }
            catch (Exception)
            {
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void textBoxPOSKeyPadPerCent_TextChanged(object sender, EventArgs e)
        {
            if (mIsLockText)
            {
                return;
            }
            lblError.Text = "";
            if (textBoxPOSKeyPadPerCent.Text == "")
            {
                Total = Price * Qty;
                mIsLockText = true;
                textBoxPOSKeyPadPrice.Text = "";
                textBoxPOSKeyPadTotal.Text = mMoneyFortmat.Format(Total);
                mIsLockText = false;
                return;
            }
            int intDis = Convert.ToInt32(textBoxPOSKeyPadPerCent.Text);
            int maxDis = Class.DiscountConfig.GetDiscountConfig();
            Discount = 0;
            if (mIsManager)
            {
                if (intDis > 100)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Giảm giá tối đa 100%";
                        mIsLockText = true;
                        string s1 = textBoxPOSKeyPadPerCent.Text;
                        textBoxPOSKeyPadPerCent.Text = s1.Substring(0, s1.Length - 1);
                        mIsLockText = false;
                        return;
                    }
                    lblError.Text = "Maximum 100% discount";
                    mIsLockText = true;
                    string s = textBoxPOSKeyPadPerCent.Text;
                    textBoxPOSKeyPadPerCent.Text = s.Substring(0, s.Length - 1);
                    mIsLockText = false;
                    return;
                }
            }
            else
            {
                if (intDis > maxDis)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Giảm giá tối đa " + maxDis + "%" + ". Quản lý đăng nhập để được giảm giá nhiều hơn";
                        mIsLockText = true;
                        string s1 = textBoxPOSKeyPadPerCent.Text;
                        textBoxPOSKeyPadPerCent.Text = s1.Substring(0, s1.Length - 1);
                        mIsLockText = false;
                        return;
                    }

                    lblError.Text = "Maximum " + maxDis + "%" + " discount. Manager log for more discount";
                    mIsLockText = true;
                    string s = textBoxPOSKeyPadPerCent.Text;
                    textBoxPOSKeyPadPerCent.Text = s.Substring(0, s.Length - 1);
                    mIsLockText = false;
                    return;
                }
            }
            Discount = Price * Qty * intDis / 100;
            Total = Price * Qty - Discount;
            mIsLockText = true;
            textBoxPOSKeyPadPrice.Text = mMoneyFortmat.Format(Discount);
            textBoxPOSKeyPadTotal.Text = mMoneyFortmat.Format(Total);
            mIsLockText = false;
        }

        private void textBoxPOSKeyPadPrice_TextChanged(object sender, EventArgs e)
        {
            if (mIsLockText)
            {
                return;
            }
            lblError.Text = "";
            if (textBoxPOSKeyPadPrice.Text == "")
            {
                Total = Price * Qty;
                mIsLockText = true;
                textBoxPOSKeyPadPerCent.Text = "";
                textBoxPOSKeyPadTotal.Text = mMoneyFortmat.Format(Total);
                mIsLockText = false;
                return;
                return;
            }
            if (Price == 0)
            {
                textBoxPOSKeyPadPrice.Text = "0";
                return;
            }
            Discount = 0;
            //=========================
            Discount = mMoneyFortmat.getFortMat(textBoxPOSKeyPadPrice.Text);
            int intDis = (int)(Discount * 100 / (Price * Qty));
            int maxDis = Class.DiscountConfig.GetDiscountConfig();
            if (mIsManager)
            {
                if (Discount > (Price * Qty))
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Giảm giá tối đa $" + mMoneyFortmat.Format(Price * Qty);
                        mIsLockText = true;
                        string s1 = textBoxPOSKeyPadPrice.Text;
                        textBoxPOSKeyPadPrice.Text = s1.Substring(0, s1.Length - 1);
                        mIsLockText = false;
                        return;
                    }
                    lblError.Text = "Maximum $" + mMoneyFortmat.Format(Price * Qty) + " discount";
                    mIsLockText = true;
                    string s = textBoxPOSKeyPadPrice.Text;
                    textBoxPOSKeyPadPrice.Text = s.Substring(0, s.Length - 1);
                    mIsLockText = false;
                    return;
                }
            }
            else
            {
                if (intDis > maxDis)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Giảm giá tối đa " + maxDis + "%" + ". Quản lý đăng nhập để được giảm giá nhiều hơn";
                        mIsLockText = true;
                        string s1 = textBoxPOSKeyPadPrice.Text;
                        textBoxPOSKeyPadPrice.Text = s1.Substring(0, s1.Length - 1);
                        mIsLockText = false;
                        return;
                    }
                    lblError.Text = "Maximum " + maxDis + "%" + " discount. Manager log for more discount";
                    mIsLockText = true;
                    string s = textBoxPOSKeyPadPrice.Text;
                    textBoxPOSKeyPadPrice.Text = s.Substring(0, s.Length - 1);
                    mIsLockText = false;
                    return;
                }
            }

            Total = Price * Qty - Discount;
            mIsLockText = true;
            //textBoxPOSKeyPadPrice.Text = mMoneyFortmat.Format(dis);
            textBoxPOSKeyPadPerCent.Text = intDis + "";
            textBoxPOSKeyPadTotal.Text = mMoneyFortmat.Format(Total);
            mIsLockText = false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            frmLoginDialog frm = new frmLoginDialog();
            frm.Permission = frmLoginDialog.MANAGER;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                mIsManager = true;
            }
        }
    }
}