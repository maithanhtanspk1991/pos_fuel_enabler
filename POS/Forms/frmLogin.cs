﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Properties;

namespace POS.Forms
{
    public partial class frmLogin : Form
    {
        public static double dblRealCashIn;
        public static double dblRealCashOut;
        public static double dblRealSubtotal;
        public static double dblRealTotalInSafe;
        public static double dblRealTotalInSafeLastShift;
        public static double dblSubtotalLastShift;
        public static int intEmployeeID;
        public static int intPermission;
        public static string strEmployeeName;
        private Connection.Connection conn;
        private readonly string _nameClass = "POS::Forms::frmLogin::";
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        //private Class.MultiLanguage mLanguage = new Class.MultiLanguage();
        private Class.ReadConfig mReadConfig=new Class.ReadConfig();

        public frmLogin()
        {
            InitializeComponent();            
        }

        public int NumOfDay { get; set; }

        public void Test()
        {
            conn.Open();
            DataTable dtSource = conn.Select("SELECT * FROM staffs s Where stfId = 14;");
            MessageBox.Show(dtSource.Rows[0][5].ToString());
        }

        public void frmLogin_Load(object sender, EventArgs e)
        {
            SetMultiLanguage();
            #region
            //lbStatus.Text = "";
            //NumOfDay = -1;
            //License.SerialConfig mSerialConfig = new License.SerialConfig();
            //if (!mSerialConfig.CheckMasterKey())
            //{
            //    NumOfDay = 0;
            //    LoadLicense(NumOfDay);
            //}
            //if (!mSerialConfig.CheckSerial())
            //{
            //    if (!mSerialConfig.CheckTrial())
            //    {
            //        NumOfDay = 0;
            //        LoadLicense(NumOfDay);
            //    }
            //    else
            //    {
            //        NumOfDay = mSerialConfig.NumOfDay;
            //        if (NumOfDay > 0 && NumOfDay <= 14)
            //        {
            //            mTrialObject = new License.TrialObject(NumOfDay, true);
            //            LoadLicense(NumOfDay);
            //        }
            //    }
            //}
            #endregion
            License.SerialConfig mSerialConfig = new License.SerialConfig();
            if (License.BOLicense.CheckLicense() == License.LicenseDialog.Close)
            {
                License.EnterKey frm = new License.EnterKey();
                frm.ShowDialog();
                if (License.BOLicense.CheckLicense() == License.LicenseDialog.Close)
                {
                    Application.Exit();
                }
            }
            Forms.frmCheckServerConnection frmcheck = new frmCheckServerConnection(new Class.ReadConfig());
            frmcheck.ShowDialog();
        }

        private void SetMultiLanguage()
        {
            #region Ghi đọc file bằng xml
            //btnClear.Text = mLanguage.frmLogin_btnClear;
            //btnExit.Text = mLanguage.frmLogin_btnExit;
            //btnLicense.Text = mLanguage.frmLogin_btnLicense;
            //btnSend.Text = mLanguage.frmLogin_btnLogin;
            //label4.Text = mLanguage.frmLogin_lbStatus;
            //label3.Text = mLanguage.frmLogin_lbUser;
            //label2.Text = mLanguage.frmLogin_lbPass;   
            #endregion         
            if(mReadConfig.LanguageCode.Equals("vi"))
            {
                btnClear.Text="XÓA";
                btnExit.Text = "THOÁT";
                btnSend.Text = "OK";
                btnLicense.Text = "BẢN QUYỀN";
                label4.Text = "Đăng Nhập";
                label3.Text = "Tài khoản :";
                label2.Text = "Mật khẩu :";
                return;
            }
        }

        private License.TrialObject mTrialObject;

        private void LoadLicense(int NumOfDay)
        {
            License.frmRegister frm = new License.frmRegister(NumOfDay);
            switch (frm.ShowDialog())
            {
                case System.Windows.Forms.DialogResult.Cancel:
                    Application.Exit();
                    break;

                case System.Windows.Forms.DialogResult.OK:
                    {
                    }
                    break;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPin.Text = "";
            txtStaffID.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            //License.BOLicense.CheckLicense();
            License.BOLicense.CheckLicense();
            if (!Convert.ToBoolean(dbconfig.AllowSalesFastFood))
            {
                AutoStartup();
                LoginWithShift();
            }
            else
            {
                AutoStartup();
                LogNoneShift();
            }
        }
        private void AutoStartup()
        {
            if (new Class.ReadConfig().IsAutoStartup)
            {
                Class.AutoStartup.Resume("FuelAutoBecas", "FuelAutoBecas.exe");
                Class.AutoStartup.Resume("RestaurantServer", "RestaurantServer.exe");
            }
        }

        private void LoadLastShift()
        {
            try
            {
                conn.Open();
                DataTable dtSource = conn.Select("select subTotal,CashOut,CashIn,RealTotalInSafe from shifts order by shiftID DESC LIMIT 0,2");
                if (dtSource.Rows.Count == 2)
                {
                    dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[1]["RealTotalInSafe"].ToString());
                    dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[1]["subTotal"].ToString());
                }
                else if (dtSource.Rows.Count == 1)
                {
                    dblRealTotalInSafe = Convert.ToDouble(dtSource.Rows[0]["RealTotalInSafe"].ToString());
                    dblSubtotalLastShift = Convert.ToDouble(dtSource.Rows[0]["subTotal"].ToString());
                }
                else
                {
                    dblRealTotalInSafe = 0;
                    dblSubtotalLastShift = 0;
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadLastShift::" + ex.Message);
            }
        }

        private void LoginWithShift()
        {
            Class.LogPOS.WriteLog("==========================================================================");
            Class.LogPOS.WriteLog("==============================Sign On to Pos==============================");
            Class.LogPOS.WriteLog("==========================================================================");
            conn = new Connection.Connection();
            string staffname = string.Empty;
            #region
            try
            {
                conn.Open();
                string sql = "select stfId, Name,staffID,permission from staffs where staffID=" + "'" + txtStaffID.Text + "'" + " and `password`=password('" + txtPin.Text + "')";
                DataTable tbl = conn.Select(sql);
                int intStaffID = 0;
                if (tbl.Rows.Count > 0)
                {
                    try
                    {
                        intStaffID = Convert.ToInt32(txtStaffID.Text);
                        staffname = tbl.Rows[0]["Name"].ToString();
                    }
                    catch (Exception ex)
                    {
                        intStaffID = 0;
                        Class.LogPOS.WriteLog(_nameClass + "LoginWithShift::" + ex.Message);
                    }
                }
                if (Settings.Default.AllowUseShift)
                {
                    DataTable tb2 = new DataTable();
                    if (BusinessObject.BOConfig.CheckPassWord(new SystemConfig.DBConfig(), txtPin.Text))
                    {
                        sql = "SELECT S.shiftID,S.numofOrders " +
                        "FROM shifts as S " +
                        "where (CURDATE() != DATE(S.ts) AND S.completed = 0 AND (hour(CURTIME())>=" + dbconfig.Hour + " AND minute(CURTIME())>=" + dbconfig.Minute + ") AND second(CURTIME())>=" + dbconfig.Second + ") " +
                            "OR(CURDATE() = DATE(S.ts) AND S.completed = 0 AND (hour(CURTIME())>=" + dbconfig.Hour + " AND minute(CURTIME())>=" + dbconfig.Minute + ") AND second(CURTIME())>=" + dbconfig.Second + " AND staffID != " + intStaffID + ")";

                        tbl = conn.Select(sql);
                        if (tbl.Rows.Count == 0)
                        {
                            string sql2 = "SELECT shiftID,staffID,completed,numofOrders FROM shifts ORDER BY shiftID DESC LIMIT 0,1";
                            tb2 = conn.Select(sql2);
                            if (tb2.Rows.Count != 0)
                            {
                                if (Convert.ToInt32(tb2.Rows[0]["staffID"].ToString()) == Convert.ToInt32((txtStaffID.Text == "" ? "0" : txtStaffID.Text)))
                                {
                                    if (txtStaffID.Text == "")
                                    {
                                        ShowFormOrderAll(staffname, intStaffID, 3);
                                    }
                                    else
                                    {
                                        ShowFormOrderAll("Manager", 0, 3);
                                    }
                                }
                                else
                                {
                                    if (txtStaffID.Text != "")
                                    {
                                        if (Convert.ToInt32(tb2.Rows[0]["numofOrders"].ToString()) != 0)
                                        {
                                            ShowFormOrderAll(staffname, intStaffID, 3, 1);
                                        }
                                        else
                                        {
                                            sql = "update shifts set staffID = " + Convert.ToInt32(tb2.Rows[0]["stfId"].ToString()) + ",ts=ts where shiftID = " + Convert.ToInt32(tb2.Rows[0]["shiftID"].ToString()) + "";
                                            conn.ExecuteNonQuery(sql);
                                            ShowFormOrderAll(staffname, intStaffID, 3);
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tb2.Rows[0]["numofOrders"].ToString()) != 0)
                                        {
                                            ShowFormOrderAll("Manager", 0, 3, 1);
                                        }
                                        else
                                        {
                                            sql = "update shifts set ts=ts,staffID = 0 where shiftID = " + Convert.ToInt32(tb2.Rows[0]["shiftID"].ToString()) + "";
                                            conn.ExecuteNonQuery(sql);
                                            ShowFormOrderAll("Manager", 0, 3);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (txtStaffID.Text == "")
                                {
                                    //ShowFormOrderAll(textBox2.Text, 0, 3);
                                    ShowFormOrderAll(staffname, intStaffID, 3);
                                }
                                else
                                {
                                    ShowFormOrderAll("Manager", 0, 3);
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(tbl.Rows[0]["numofOrders"].ToString()) == 0)
                            {
                                if (txtStaffID.Text == "")
                                {
                                    //ShowFormOrderAll(textBox2.Text, 0, 3);
                                    ShowFormOrderAll(staffname, intStaffID, 3);
                                }
                                else
                                {
                                    ShowFormOrderAll("Manager", 0, 3);
                                }
                            }
                            else
                            {
                                if (txtStaffID.Text != "")
                                {
                                    ShowFormOrderAll(staffname, intStaffID, 3, 1);
                                }
                                else
                                {
                                    ShowFormOrderAll("Manager", 0, 3, 1);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (tbl.Rows.Count > 0)
                        {
                            string sql2 = "SELECT S.shiftID,S.numofOrders " +
                                            "FROM shifts as S " +
                                            "where CURDATE() != DATE(S.ts) AND S.completed = 0 AND (hour(CURTIME())>=" + dbconfig.Hour + " AND minute(CURTIME())>=" + dbconfig.Minute + ") AND second(CURTIME())>=" + dbconfig.Second + " " +
                                            "OR CURDATE() = DATE(S.ts) AND S.completed = 0 AND (hour(CURTIME())>=" + dbconfig.Hour + " AND minute(CURTIME())>=" + dbconfig.Minute + ") AND second(CURTIME())>=" + dbconfig.Second + " AND staffID != " + intStaffID + "";

                            tb2 = conn.Select(sql2);
                            if (tb2.Rows.Count == 0)
                            {
                                sql2 = "SELECT shiftID,IF(s.staffID is null,0,s.staffID) AS staffID,completed,numofOrders FROM shifts AS sh left join staffs AS s on sh.staffID = s.stfId ORDER BY shiftID DESC LIMIT 0,1";
                                tb2 = conn.Select(sql2);

                                if (tb2.Rows.Count != 0)
                                {
                                    if (Convert.ToInt32(tb2.Rows[0]["staffID"].ToString()) == Convert.ToInt32((txtStaffID.Text == "" ? "0" : txtStaffID.Text)))
                                    {
                                        ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]));
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tb2.Rows[0]["numofOrders"].ToString()) != 0)
                                        {
                                            ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]), 1);
                                        }
                                        else
                                        {
                                            sql = "update shifts set ts=ts,staffID = " + Convert.ToInt32(tbl.Rows[0]["stfId"].ToString()) + " where shiftID = " + Convert.ToInt32(tb2.Rows[0]["shiftID"].ToString()) + "";
                                            conn.ExecuteNonQuery(sql);
                                            ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]));
                                        }
                                    }
                                }
                                else
                                {
                                    ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]));
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(tb2.Rows[0]["numofOrders"].ToString()) == 0)
                                {
                                    sql = "update shifts set ts=ts,staffID = " + Convert.ToInt32(tbl.Rows[0]["stfId"].ToString()) + " where shiftID = " + Convert.ToInt32(tb2.Rows[0]["shiftID"].ToString()) + "";
                                    conn.ExecuteNonQuery(sql);
                                    ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]));
                                }
                                else
                                {
                                    ShowFormOrderAll(tbl.Rows[0]["Name"].ToString(), Convert.ToInt32(tbl.Rows[0]["stfId"]), Convert.ToInt16(tbl.Rows[0]["permission"]), 1);
                                    //Class.UpdateToServer.UpDatabaseToServer(conn);
                                }
                            }
                        }
                        else
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                lbStatus.ForeColor = Color.Red;
                                lbStatus.Text = "Tài khoản và/hoặc mật khẩu không đúng.";
                                txtPin.Text = null;
                                txtStaffID.Text = null;
                                txtStaffID.Focus();
                                return;
                            }
                            lbStatus.ForeColor = Color.Red;
                            lbStatus.Text = "Staff ID and/or password are incorrect.";
                            txtPin.Text = null;
                            txtStaffID.Text = null;
                            txtStaffID.Focus();
                        }
                    }
                }
                else
                {
                    if (BusinessObject.BOConfig.CheckPassWord(new SystemConfig.DBConfig(), txtPin.Text))
                    {
                        ShowFormOrderAll("Manager", 0, 3);
                    }
                    else
                    {
                        if (tbl.Rows.Count > 0)
                        {
                            ShowFormOrderAll(staffname, intStaffID, 3);
                        }
                        else
                        {
                            if (mReadConfig.LanguageCode.Equals("vi"))
                            {
                                lbStatus.ForeColor = Color.Red;
                                lbStatus.Text = "Tài khoản và/hoặc mật khẩu không đúng.";
                                txtPin.Text = null;
                                txtStaffID.Text = null;
                                txtStaffID.Focus();
                                return;
                            }
                            lbStatus.ForeColor = Color.Red;
                            lbStatus.Text = "Staff ID and/or password are incorrect.";
                            txtPin.Text = null;
                            txtStaffID.Text = null;
                            txtStaffID.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoginWithShift::" + ex.Message);
            }
            #endregion
            finally
            {
                conn.Close();
            }
        }

        private void LogNoneShift()
        {
            Class.LogPOS.WriteLog("==========================================================================");
            Class.LogPOS.WriteLog("==============================Sign On to Pos==============================");
            Class.LogPOS.WriteLog("==========================================================================");
            conn = new Connection.Connection();
            try
            {
                conn.Open();
                string sql = "select stfId,Name,staffID,permission from staffs where staffID=" + "'" + txtStaffID.Text + "'" + " and `password`=password('" + txtPin.Text + "')";
                DataTable tbl = conn.Select(sql);
                int intStaffID = 0;
                if (tbl.Rows.Count > 0)
                {
                    try
                    {
                        intStaffID = Convert.ToInt32(tbl.Rows[0]["stfId"]);
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "LogNoneShift::" + ex.Message);
                        intStaffID = 0;
                    }
                }
                //if (textBox1.Text == "2305")
                if (BusinessObject.BOConfig.CheckPassWord(new SystemConfig.DBConfig(), txtPin.Text))
                {
                    if (Convert.ToInt32(dbconfig.CableID) == 1)
                    {
                        UpdateToServer();
                    }
                    ShowFormOrderAll("Manager", 0, 3);
                }
                else
                {
                    if (tbl.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(dbconfig.CableID) == 1)
                        {
                            UpdateToServer();
                        }

                        ShowFormOrderAll(txtStaffID.Text, 0, 3);
                    }
                    else
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            lbStatus.ForeColor = Color.Red;
                            lbStatus.Text = "Tài khoản và/hoặc mật khẩu không đúng.";
                            txtPin.Text = null;
                            txtStaffID.Text = null;
                            txtStaffID.Focus();
                            return;
                        }
                        lbStatus.ForeColor = Color.Red;
                        lbStatus.Text = "Staff ID and/or password are incorrect.";
                        txtPin.Text = null;
                        txtStaffID.Text = null;
                        txtStaffID.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LogNoneShift::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void MessageConnectAgain(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "MessageConnectAgain");
            frmMessageBox frm = new frmMessageBox("Information", "Not Connect to server " + dbconfig.Server + ".Do you want to retry?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                frmLogin_Load(sender, e);
            }
            else
            {
                Application.Exit();
            }
        }

        private void ShowFormOrderAll(string staffName, int staffID, int permission)
        {
            this.Visible = false;
            strEmployeeName = staffName;
            intEmployeeID = staffID;
            if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
            {
                frmOrdersAllQuickSales frm2 = new frmOrdersAllQuickSales();
                frm2.loadOrder("", 1);
                frm2.SetStaffName(staffName, staffID, permission);
                frm2.ShowDialog();
            }
            else
            {
                frmOrdersAll frm = new frmOrdersAll();
                frm.LoadOrder("", 1);
                frm.SetStaffName(staffName, staffID, permission);
                frm.ShowDialog();
            }
        }

        private void ShowFormOrderAll(string staffName, int staffID, int permission, int noneEndOfShift)
        {
            this.Visible = false;
            if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
            {
                frmOrdersAllQuickSales frm = new frmOrdersAllQuickSales();
                frm.loadOrder("", 1);

                strEmployeeName = staffName;
                intEmployeeID = staffID;

                frm.SetStaffName(staffName, staffID, permission);
                frm.Show();
                panel2.Controls.Clear();
                frm.Init2();
            }
            else
            {
                frmOrdersAll frm = new frmOrdersAll();
                frm.LoadOrder("", 1);

                strEmployeeName = staffName;
                intEmployeeID = staffID;

                frm.SetStaffName(staffName, staffID, permission);
                frm.Show();
                panel2.Controls.Clear();
            }
        }

        private void UpdateToServer()
        {
            Class.LogPOS.WriteLog(_nameClass + "UpdateToServer");
            DataTable tbl = conn.Select("select orderID from ordersdaily where date(ts)<date(now())");
            if (tbl.Rows.Count != 0)
            {
            }
        }

        private void frmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                if (pr.ProcessName == "POS")
                    pr.Kill();
            }
        }

        private void CreateBarcode()
        {
            int n = 360;
            while (n > 0)
            {
                SystemLog.LogPOS.WriteLog(DataObject.CreateBarcode.CalculateChecksumDigit("3941", 16));
                n--;
            }
        }

        private void RefershItemID()
        {
            try
            {
                conn.Open();
                DataTable dt = conn.Select("SELECT itemID FROM itemsmenu Order by itemID ASC;");
                string sql = "";
                int n = 1;
                foreach (DataRow row in dt.Rows)
                {
                    sql = "Update itemsmenu Set itemID = " + n + " Where itemID = " + row[0].ToString() + ";";
                    conn.ExecuteNonQuery(sql);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnLicense_Click(object sender, EventArgs e)
        {
            License.EnterKey frm = new License.EnterKey();
            frm.ShowDialog();
        }
    }
}