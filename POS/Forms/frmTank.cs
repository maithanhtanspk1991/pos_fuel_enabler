﻿using ITL.Enabler.API;
using System.Windows.Forms;
using System.Collections.Generic;
using System;

namespace POS.Forms
{
    public partial class frmTank : Form
    {
        private Class.ReadConfig mReadConfig;
        public frmTank()
        {
            InitializeComponent();
            LoadButton();
            SetMultiLanguage();
            LoadDataFirst();
        }

        public void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                groupBox1.Text = "Thông tin chi tiết";
                label6.Text = "Sức chứa:";
                label3.Text = "Bồn chứa:";
                label5.Text = "Mô tả:";
                label4.Text = "Bồn thứ:";
                label8.Text = "Nhiệt độ:";
                label9.Text = "Lít:";
                label10.Text = "Mức nước:";
                label7.Text = "Dung tích bồn chứa:";
                btnExit.Text = "Quay lại".ToUpper();
            }
        }

        public void LoadDataFirst()
        {
            Tank tank = frmOrdersAll._Forecourt.Tanks[0];
            lblTankName.Text = tank.Name.ToString();
            lblTankNumber.Text = tank.Number.ToString();
            lblDescription.Text = tank.Description.ToString();
            lblCapacity.Text = tank.Capacity.ToString();
            lblGaugelevel.Text = tank.GaugeReading.Level.ToString();
            lblTemperature.Text = tank.GaugeReading.Temperature.ToString();
            lblVolumne.Text = tank.GaugeReading.Volume.ToString();
            lblWaterlevel.Text = tank.GaugeReading.WaterLevel.ToString();
        }
        public void LoadButton()
        {
            int top = 25;
            int left = 25;
            int maxTanks = frmOrdersAll._Forecourt.Tanks.Count;
            for (int i = 0; i < maxTanks; i++)
            {
                Tank tank = frmOrdersAll._Forecourt.Tanks[i];
                Button button = new Button();
                button.Left = left;
                button.Top = top;
                button.Name = "btnTank" + tank.Id;
                button.BackColor = System.Drawing.SystemColors.ControlDarkDark;
                button.ForeColor = System.Drawing.Color.White;
                button.Text = tank.Name;
                button.Width = 90;
                button.Height = 50;
                button.Tag = i;
                button.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                button.MouseClick += new MouseEventHandler(button_MouseClick);
                this.Controls.Add(button);
                left += button.Width + 2;
            }
        }

        void button_MouseClick(object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            int gradeButtonIndex = Convert.ToInt32(b.Tag.ToString());
            Tank tank = frmOrdersAll._Forecourt.Tanks[gradeButtonIndex];
            lblTankName.Text= tank.Name.ToString();
            lblTankNumber.Text=  tank.Number.ToString();
            lblDescription.Text=  tank.Description.ToString();
            lblCapacity.Text=  tank.Capacity.ToString();
            lblGaugelevel.Text = tank.GaugeReading.Level.ToString() ;
            lblTemperature.Text = tank.GaugeReading.Temperature.ToString();
            lblVolumne.Text = tank.GaugeReading.Volume.ToString() ;
            lblWaterlevel.Text= tank.GaugeReading.WaterLevel.ToString();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
