﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Properties;

namespace POS.Forms
{
    public partial class frmFuntion : Form
    {
        public Class.ReadConfig mReadConfig;

        public Connection.ReadDBConfig mReadDBConfig = new Connection.ReadDBConfig();

        public Barcode.SerialPort mSerialPort;

        private CardInterface.POSAxCsdEft axCsdEft;

        private Connection.Connection conn;

        private Connection.ReadDBConfig mReadDBConfig1 = new Connection.ReadDBConfig();

        private int intTotalPump;

        private SystemConfig.DBConfig mDBConfig;

        public Forms.frmOrdersAll mfrmOrdersAll;

        private Forms.frmOrdersAllQuickSales mfrmOrdersAllQuickSalse;

        private Class.MoneyFortmat mMoney;

        private Shifts mShift;

        private DataObject.Transit mTransit;

        public frmFuntion(Forms.frmOrdersAll frm, Class.MoneyFortmat money, CardInterface.POSAxCsdEft axCsdEft, SystemConfig.DBConfig dBConfig)
        {
            InitializeComponent();
            mfrmOrdersAll = frm;
            mMoney = money;
            this.axCsdEft = axCsdEft;
            btnChangePass.Enabled = Settings.Default.AllowUseShift;
            mDBConfig = dBConfig;
            mReadConfig = new ReadConfig();
            checkMenuChange = false;
            so = frm.mShiftObject;
        }

        private void LockButton()
        {
            LockButton(btnComboDisc, true);
            LockButton(btnReportWeekly, true);
            LockButton(btnControlPanel, true);
            LockButton(btnShutDown, true);
            LockButton(btnStock, true);
            LockButton(btnShiftReport, true);
            LockButton(btnStockBalance, true);
            LockButton(btnProfitAndLoss, true);
            LockButton(btnCompanyAccount, true);
            LockButton(btnStockCheck, true);
           // LockButton(btnCompanyAccount, mReadConfig.IsEnableCompany);

            LockButton(btnPayOut, mReadConfig.IsEnablePayout);
            LockButton(btnReportWeekly, mReadConfig.IsEnableWeeklyreport);
        }
        private void LockButton(Button btn, bool value)
        {
            btn.Enabled = value;
            btn.Text = value ? btn.Text : "";
            btn.BackColor = value ? btn.BackColor : System.Drawing.Color.Gray;
        }
        public frmFuntion(frmOrdersAll frm, MoneyFortmat money, int intTotalPump, Barcode.SerialPort serialPort, CardInterface.POSAxCsdEft axCsdEft, SystemConfig.DBConfig dBConfig, DataObject.Transit transit)
        {
            InitializeComponent();
            SetMultiLanguage();
            UCInfoTop uc = new UCInfoTop();
            pnTitle.Controls.Add(uc);
            mfrmOrdersAll = frm;
            mMoney = money;
            this.mShift = mShift;
            this.intTotalPump = intTotalPump;
            this.mSerialPort = serialPort;
            this.axCsdEft = axCsdEft;
            mDBConfig = dBConfig;
            mTransit = transit;
            mReadConfig = new ReadConfig();
            so = frm.mShiftObject;
            LockButton();
        }

        public frmFuntion(frmOrdersAllQuickSales frm, MoneyFortmat Mmoney, int intTotalPump, Barcode.SerialPort barcode, Connection.Connection conn, CardInterface.POSAxCsdEft axCsdEft, SystemConfig.DBConfig dBConfig)
        {
            InitializeComponent();
            UCInfoTop uc = new UCInfoTop();
            pnTitle.Controls.Add(uc);
            mfrmOrdersAllQuickSalse = frm;
            mMoney = Mmoney;
            //this.mShift = mShift;
            this.intTotalPump = intTotalPump;
            this.mSerialPort = barcode;
            this.conn = conn;
            this.axCsdEft = axCsdEft;
            mDBConfig = dBConfig;
            mReadConfig = new ReadConfig();
            LockButton();
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnBack.Text = "QUAY LẠI";
                btnCompanyAccount.Text = "QL KHÁCH HÀNG CÔNG TY";
                btnMenuChange.Text = "DANH MỤC SP";
                btnCard.Text = "THẺ";
                btnSetTime.Text = "CẤU HÌNH THỜI GIAN";
                btnChangePass.Text = "THAY ĐỔI MẬT KHẨU";
                btnChangeStaff.Text = "NHÂN VIÊN";
                btnComboDisc.Text = "GÓI GIẢM GIÁ";
                btnControlPanel.Text = "BẢNG ĐIỀU KHIỂN";
                btnFuelAutoBecas.Text = "NHIÊN LIỆU TỰ ĐỘNG";
                btnFuelItemChange.Text = "QUẢN LÝ NHIÊN LIỆU";
                btnOpenTill.Text = "MỞ KÉT";
                btnPayOut.Text = "THANH TOÁN TIỀN HÀNG";
                btnPersonalAccount.Text = "QL KHÁCH HÀNG";
                btnReportDaily.Text = "BÁO CÁO NGÀY";
                btnReportWeekly.Text = "BÁO CÁO TUẦN";
                btnSafeDrop.Text = "RÚT TIỀN TỪ KÉT";
                btnSendMail.Text = "CẤU HÌNH GỞI THƯ";
                btnSetBarcodeItem.Text = "MÃ VẠCH";
                btnSetPump.Text = "CẤU HÌNH TRỤ BƠM";
                btnSetting.Text = "CẤU HÌNH CHUNG";
                btnShiftReport.Text = "BÁO CÁO CA LV";
                btnShutDown.Text = "ĐÓNG HỆ THỐNG";
                btnStaffMan.Text = "QUẢN LÝ NHÂN VIÊN";
                btnStock.Text = "QUẢN LÝ KHO";
                btnVoidHistory.Text = "LỊCH SỬ XÓA";
                btnCashIn.Text = "ĐƯA TIỀN VÀO";
                btnTank.Text = "BỒN CHỨA XĂNG";
                btnTank.Font = new Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnBack.Font = new Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnCompanyAccount.Font = new Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnCompanyAccount.TextAlign = ContentAlignment.MiddleCenter;
                btnPersonalAccount.TextAlign = ContentAlignment.MiddleCenter;
                btnMenuChange.Font = new Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnMenuChange.TextAlign = ContentAlignment.MiddleCenter;
                btnSetTime.Font = new Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnSetTime.TextAlign = ContentAlignment.MiddleCenter;
                btnFuelItemChange.Font = new Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnFuelItemChange.TextAlign = ContentAlignment.MiddleCenter;
                return;
            }
        }

        public bool checkMenuChange { get; set; }

        private void btnCard_Click(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCard frm = new frmCard();
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCard frmCard = new frmCard();
                        frmCard.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCard frm = new frmCard();
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCard frmCard = new frmCard();
                        frmCard.ShowDialog();
                    }
                }
            }
        }

        private void btnCashIn_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        frmCashInOrOut frm1 = new frmCashInOrOut(1, mfrmOrdersAllQuickSalse.Money, so.ShiftID, so);
                        frm1.Text = "Đưa tiền vào";
                        frm1.txtDesciption.Text = "Đưa tiền vào";
                        frm1.Width = 420;
                        DialogResult dlg1 = frm1.ShowDialog();
                        if (dlg1 == DialogResult.OK)
                        {
                            if (frm1.mShiftObject != null)
                            {
                                so = frm1.mShiftObject;
                                so.bActiveShift = true;
                            }
                            Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                        }
                        return;
                    }
                    frmCashInOrOut frm = new frmCashInOrOut(1, mfrmOrdersAllQuickSalse.Money, so.ShiftID, so);
                    frm.Text = "Cash In";
                    frm.txtDesciption.Text = "Cash In";
                    frm.Width = 420;
                    DialogResult dlg = frm.ShowDialog();
                    if (dlg == DialogResult.OK)
                    {
                        if (frm.mShiftObject != null)
                        {
                            so = frm.mShiftObject;
                            so.bActiveShift = true;
                        }
                        Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                    }
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        frmCashInOrOut frm2 = new frmCashInOrOut(1, mfrmOrdersAll.Money, so.ShiftID, so);
                        frm2.Text = "Đưa tiền vào";
                        frm2.txtDesciption.Text = "Đưa tiền vào";
                        frm2.Width = 420;
                        DialogResult dlg2 = frm2.ShowDialog();
                        if (dlg2 == DialogResult.OK)
                        {
                            if (frm2.mShiftObject != null)
                            {
                                so = frm2.mShiftObject;
                                so.bActiveShift = true;
                            }
                            Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                        }
                        return;

                    }
                    frmCashInOrOut frm = new frmCashInOrOut(1, mfrmOrdersAll.Money, so.ShiftID, so);
                    frm.Text = "Cash In";
                    frm.txtDesciption.Text = "Cash In";
                    frm.Width = 420;
                    DialogResult dlg = frm.ShowDialog();
                    if (dlg == DialogResult.OK)
                    {
                        if (frm.mShiftObject != null)
                        {
                            so = frm.mShiftObject;
                            so.bActiveShift = true;
                        }
                        Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                    }
                }
            }
        }

        private void btnChangeStaff_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel2.Controls.Clear();
                UCChangeStaff uct = new UCChangeStaff();
                uct.Dock = DockStyle.Fill;
                uct.StaffChange += new UCChangeStaff.StaffChangeEventHander(uct_StaffChange);
                panel2.Controls.Add(uct);
            }
        }

        private void btnComboDisc_Click(object sender, EventArgs e)
        {
            #region Quantity Disc
            /*
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmLoyatyByItem frm = new frmLoyatyByItem(mfrmOrdersAllQuickSalse.mSerialPort);
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = frmLoginDialog.MANAGER;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmLoyatyByItem frmLo = new frmLoyatyByItem(mfrmOrdersAllQuickSalse.mSerialPort);
                        frmLo.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmLoyatyByItem frm = new frmLoyatyByItem(mfrmOrdersAll.mSerialPortBarcode);
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmLoyatyByItem frmLo = new frmLoyatyByItem(mfrmOrdersAll.mSerialPortBarcode);
                        frmLo.ShowDialog();
                    }
                }
            }
            */
            #endregion Quantity Disc

            #region Combo Disc
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmComboDisc frm = new frmComboDisc(mfrmOrdersAllQuickSalse.mSerialPort);
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = frmLoginDialog.MANAGER;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmComboDisc frmcbDisc = new frmComboDisc(mfrmOrdersAllQuickSalse.mSerialPort);
                        frmcbDisc.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmComboDisc frm = new frmComboDisc(mfrmOrdersAll.mSerialPortBarcode);
                        frm.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmComboDisc frmcbDisc = new frmComboDisc(mfrmOrdersAll.mSerialPortBarcode);
                        frmcbDisc.ShowDialog();
                    }
                }
            }
            #endregion Combo Disc
        }

        private void btnCompanyAccount_Click(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmCompany frm = new frmCompany(mfrmOrdersAllQuickSalse.mShift.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                    frm.ShowDialog();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCompany frmCom = new frmCompany(mfrmOrdersAllQuickSalse.mShift.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                        frmCom.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmCompany frm = new frmCompany(so.ShiftName, mSerialPort, axCsdEft, mReadConfig);
                    frm.ShowDialog();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCompany frmCom = new frmCompany(so.ShiftName, mSerialPort, axCsdEft, mReadConfig);
                        frmCom.ShowDialog();
                    }
                }
            }
        }

        private void btnControlPanel_Click(object sender, EventArgs e)
        {
        }

        public DataObject.ShiftObject so;

        private void btnExit_Click(object sender, EventArgs e)
        {
            //so = so;
            this.Close();
        }

        private void btnFuelItemChange_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmFuelMan frm = new frmFuelMan(mMoney);
                frm.ShowDialog();
            }
        }

        private void btnMenuChange_Click(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    checkMenuChange = true;
                    frmMenuChange frm = new frmMenuChange(mDBConfig, mfrmOrdersAllQuickSalse.mSerialPort, mReadConfig);
                    frm.Show();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        checkMenuChange = true;
                        frmMenuChange frmMenu = new frmMenuChange(mDBConfig, mfrmOrdersAllQuickSalse.mSerialPort, mReadConfig);
                        frmMenu.Show();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    checkMenuChange = true;
                    frmMenuChange frm = new frmMenuChange(mDBConfig, mfrmOrdersAll.mSerialPortBarcode, mReadConfig);
                    frm.Show();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        checkMenuChange = true;
                        frmMenuChange frmMenu = new frmMenuChange(mDBConfig, mfrmOrdersAll.mSerialPortBarcode, mReadConfig);
                        frmMenu.Show();
                    }
                }
            }
        }

        private void btnOpenTill_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
            }
            //if (mfrmOrdersAll.mStaffPermission == 3)
            //{
            //    if (MessageBox.Show("End of Shift ?","Warning",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning)==DialogResult.OK)
            //    {
            //        string shiftID = mfrmOrdersAll.mShift.ShiftID.ToString();
            //        mfrmOrdersAll.mShift.EndOfShift(mfrmOrdersAll.Money);
            //        panel2.Controls.Clear();
            //        UCReportShift uct = new UCReportShift(shiftID);
            //        panel2.Controls.Add(uct);
            //        mfrmOrdersAll.LoadShift();
            //    }
            //}
            //else
            //{
            //    frmLoginDialog frm = new frmLoginDialog();
            //    frm.Permission = 3;
            //    if (frm.ShowDialog() == DialogResult.OK)
            //    {
            //        if (MessageBox.Show("End of Shift ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            //        {
            //            string shiftID = mfrmOrdersAll.mShift.ShiftID.ToString();
            //            mfrmOrdersAll.mShift.EndOfShift(mfrmOrdersAll.Money);
            //            panel2.Controls.Clear();
            //            UCReportShift uct = new UCReportShift(shiftID);
            //            panel2.Controls.Add(uct);
            //            mfrmOrdersAll.LoadShift();
            //        }
            //    }
            //}
        }

        private void btnPayOut_Click(object sender, EventArgs e)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                try
                {
                    ExecuteFuntionCash(6, 6, so.ShiftID, true, "Thanh toán hết nợ");
                }
                catch (Exception)
                {
                }
                return;
            }
            try
            {
                ExecuteFuntionCash(6, 6, so.ShiftID, true, "Pay Out");
            }
            catch (Exception)
            {
            }
        }

        private void btnPersonalAccount_Click(object sender, EventArgs e)
        {
            if (so.bActiveShift == false && so.continueShift == false) //shift chua hoat dong
            {
                BusinessObject.BOShift.InsertShift(ref so);
                BusinessObject.BOConfig.SetCurrentShift(so.ShiftID);
                so.bActiveShift = true;
            }
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmCust frm = new frmCust(mfrmOrdersAllQuickSalse.mShift.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                    frm.ShowDialog();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCust frmCust = new frmCust(mfrmOrdersAllQuickSalse.mShift.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                        frmCust.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmCust frm = new frmCust(so.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                    frm.ShowDialog();
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCust frmCust = new frmCust(so.ShiftID, mSerialPort, axCsdEft, mReadConfig);
                        frmCust.ShowDialog();
                    }
                }
            }
        }

        private void btnProfitAndLoss_Click(object sender, EventArgs e)
        {
            //day la nut ProfitAndLossStatement
            //frmProfitAndLossStatement frm = new frmProfitAndLossStatement();
            //frm.ShowDialog();
        }

        private void btnFuelAutoBecas_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pnContent.Controls.Clear();
                UCFuelAuto uc = new UCFuelAuto();
                uc.Dock = DockStyle.Fill;
                pnContent.Controls.Add(uc);
            }
        }

        private void btnReportDaily_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
                {
                    if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                    {
                        pnContent.Controls.Clear();
                        UCReportDaily uct = new UCReportDaily(mfrmOrdersAllQuickSalse.Money);
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                    else
                    {
                        frmLoginDialog frm = new frmLoginDialog();
                        frm.Permission = 3;
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            pnContent.Controls.Clear();
                            UCReportDaily uct = new UCReportDaily(mfrmOrdersAllQuickSalse.Money);
                            uct.Dock = DockStyle.Fill;
                            pnContent.Controls.Add(uct);
                        }
                    }
                }
                else
                {
                    if (mfrmOrdersAll.mStaffPermission == 3)
                    {
                        pnContent.Controls.Clear();
                        UCReportDaily uct = new UCReportDaily(mfrmOrdersAll.Money);
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                    else
                    {
                        frmLoginDialog frm = new frmLoginDialog();
                        frm.Permission = 3;
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            pnContent.Controls.Clear();
                            UCReportDaily uct = new UCReportDaily(mfrmOrdersAll.Money);
                            uct.Dock = DockStyle.Fill;
                            pnContent.Controls.Add(uct);
                        }
                    }
                }
            }
        }

        private void btnReportWeekly_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
                {
                    if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                    {
                        pnContent.Controls.Clear();
                        UCReportWeekly uct = new UCReportWeekly(mfrmOrdersAllQuickSalse.Money);
                        pnContent.Controls.Add(uct);
                    }
                    else
                    {
                        frmLoginDialog frm = new frmLoginDialog();
                        frm.Permission = 3;
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            pnContent.Controls.Clear();
                            UCReportWeekly uct = new UCReportWeekly(mfrmOrdersAllQuickSalse.Money);
                            pnContent.Controls.Add(uct);
                        }
                    }
                }
                else
                {
                    if (mfrmOrdersAll.mStaffPermission == 3)
                    {
                        pnContent.Controls.Clear();
                        UCReportWeekly uct = new UCReportWeekly(mfrmOrdersAll.Money);
                        pnContent.Controls.Add(uct);
                    }
                    else
                    {
                        frmLoginDialog frm = new frmLoginDialog();
                        frm.Permission = 3;
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            pnContent.Controls.Clear();
                            UCReportWeekly uct = new UCReportWeekly(mfrmOrdersAll.Money);
                            pnContent.Controls.Add(uct);
                        }
                    }
                }
            }
        }

        private void btnSafeDrop_Click(object sender, EventArgs e)
        {
            ExecuteFuntionCash(3, 3, this.so.ShiftID, false, "Safe Drop");
        }

        private void btnSetPump_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmSetPump frm = new frmSetPump();
                frm.ShowDialog();
            }
        }

        private void btnSetTime_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pnContent.Controls.Clear();
                UCSettime uct = new UCSettime();
                pnContent.Controls.Add(uct);
            }
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmSetting frm = new frmSetting(mReadConfig);
                frm.ShowDialog();
            }
        }

        private void btnShiftReport_Click(object sender, EventArgs e)
        {
            pnContent.Controls.Clear();
            POS.Controls.UCChangePassword UCChangePassword = new Controls.UCChangePassword();
            UCChangePassword.Dock = DockStyle.Fill;
            pnContent.Controls.Add(UCChangePassword);
        }

        private void btnShutDown_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Do you want exit POS System and shutdown this computer? ", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    System.Diagnostics.Process.Start("ShutDown", "-s -t 0");
            //}
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                frmMessageBox frm1 = new frmMessageBox("Cảnh báo", "Bạn có muốn thoát hệ thống POS và tắt máy tính?");
                frm1.ShowDialog();
                if (frm1.DialogResult == DialogResult.OK)
                {
                    //System.Diagnostics.Process.Start("ShutDown", "-s -t 0");
                    Process me = Process.GetCurrentProcess();
                    foreach (Process P in Process.GetProcesses())
                    {
                        if (P.Id != me.Id)
                        {
                            P.CloseMainWindow(); // Sends WM_CLOSE; less gentle methods available too
                            System.Diagnostics.Process.Start("ShutDown", "-s -t 0");
                        }
                    }
                }
                return;
            }
            frmMessageBox frm = new frmMessageBox("Warning", "Do you want exit POS System and shutdown this computer?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                //System.Diagnostics.Process.Start("ShutDown", "-s -t 0");
                Process me = Process.GetCurrentProcess();
                foreach (Process P in Process.GetProcesses())
                {
                    if (P.Id != me.Id)
                    {
                        P.CloseMainWindow(); // Sends WM_CLOSE; less gentle methods available too
                        System.Diagnostics.Process.Start("ShutDown", "-s -t 0");
                    }
                }
            }
        }

        private void btnStaffMan_Click(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCreateStaff frmcr = new frmCreateStaff();
                        frmcr.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = frmLoginDialog.MANAGER;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCreateStaff frmcr = new frmCreateStaff();
                        frmcr.ShowDialog();
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        frmCreateStaff frmcr = new frmCreateStaff();
                        frmcr.ShowDialog();
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = frmLoginDialog.MANAGER;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frmCreateStaff frmcr = new frmCreateStaff();
                        frmcr.ShowDialog();
                    }
                }
            }
        }

        private void btnStock_Click(object sender, EventArgs e)
        {
            //Stock
            frmLoginDialog frmdialog = new frmLoginDialog();
            frmdialog.Permission = 3;
            if (frmdialog.ShowDialog() == DialogResult.OK)
            {
                DataObject.Transit transit = new DataObject.Transit();
                transit = mTransit.Copy();
                transit.StaffID = frmdialog.POSStaff.StaffID;
                pnContent.Controls.Clear();
                POS.Controls.UCStock uct = new POS.Controls.UCStock(mDBConfig, mSerialPort, transit);
                uct.Dock = DockStyle.Fill;
                pnContent.Controls.Add(uct);
            }
        }

        private void btnStockBalance_Click(object sender, EventArgs e)
        {
            //day la nut Stock Balance
            //frmStockBalance frm = new frmStockBalance();
            //frm.ShowDialog();
        }

        private void btnStockCheck_Click(object sender, EventArgs e)
        {
            //Day la nut Qty dícount
            //if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
            //{
            //    if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
            //    {
            //        frmLoyatyControl frm = new frmLoyatyControl(mfrmOrdersAllQuickSalse.mSerialPort);
            //        frm.ShowDialog();
            //    }
            //    else
            //    {
            //        frmLoginDialog frm = new frmLoginDialog();
            //        frm.Permission = 3;
            //        if (frm.ShowDialog() == DialogResult.OK)
            //        {
            //            frmLoyatyControl frmLo = new frmLoyatyControl(mfrmOrdersAllQuickSalse.mSerialPort);
            //            frmLo.ShowDialog();
            //        }
            //    }
            //}
            //else
            //{
            //    if (mfrmOrdersAll.mStaffPermission == 3)
            //    {
            //        frmLoyatyControl frm = new frmLoyatyControl(mfrmOrdersAll.mSerialPort);
            //        frm.ShowDialog();
            //    }
            //    else
            //    {
            //        frmLoginDialog frm = new frmLoginDialog();
            //        frm.Permission = 3;
            //        if (frm.ShowDialog() == DialogResult.OK)
            //        {
            //            frmLoyatyControl frmLo = new frmLoyatyControl(mfrmOrdersAll.mSerialPort);
            //            frmLo.ShowDialog();
            //        }
            //    }
            //}
        }

        private void btnVoidHistory_Click(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(mReadDBConfig1.AllowSalesFastFood))
            {
                if (mfrmOrdersAllQuickSalse.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        pnContent.Controls.Clear();
                        UCVoidHistory uct = new UCVoidHistory();
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = frmLoginDialog.MANAGER;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        pnContent.Controls.Clear();
                        UCVoidHistory uct = new UCVoidHistory();
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                }
            }
            else
            {
                if (mfrmOrdersAll.mStaffPermission == 3)
                {
                    frmLoginDialog frmlogindialog = new frmLoginDialog();
                    frmlogindialog.Permission = frmLoginDialog.MANAGER;
                    if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        pnContent.Controls.Clear();
                        UCVoidHistory uct = new UCVoidHistory();
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                }
                else
                {
                    frmLoginDialog frm = new frmLoginDialog();
                    frm.Permission = 3;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        pnContent.Controls.Clear();
                        UCVoidHistory uct = new UCVoidHistory();
                        uct.Dock = DockStyle.Fill;
                        pnContent.Controls.Add(uct);
                    }
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            frmLoginDialog frm = new frmLoginDialog();
            frm.Permission = 3;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                frmShiftReport frmrp = new frmShiftReport(mMoney, mfrmOrdersAll, DataObject.TypeFromShift.Manager);
                frmrp.ShowDialog();
            }
        }

        private void ExecuteFuntionCash(int Type, int TypeCash, int Shift, bool blnIsHaveDescription, string strTitleForm)
        {
            Connection.Connection con = new Connection.Connection();

            double total = 0;
            if (so.bActiveShift == false && so.continueShift == false) //shift chua hoat dong
            {
                total += BusinessObject.BOConfig.GetCurrentCashFloatIn(new SystemConfig.DBConfig());
                BusinessObject.BOShift.InsertShift(ref so);
                BusinessObject.BOConfig.SetCurrentShift(so.ShiftID);
                so.bActiveShift = true;
            }
            else
            {
                DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                shiftrp.datetime = so.datetime;
                shiftrp.ShiftID = so.ShiftID;
                shiftrp = BusinessObject.BOShiftReport.GetReportShift(shiftrp);
                total += shiftrp.CashFloatIn + shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.PayOut;
            }
            DataObject.ShiftObject sod = so.copy();
            frmCashInOrOut frm = new frmCashInOrOut(Type, TypeCash, Convert.ToDouble(mMoney.Format(total)), mMoney, sod.ShiftID, sod);
            frm.Text = strTitleForm;
            if (blnIsHaveDescription)
            {
                frm.Width = 420;
                frm.txtDesciption.Text = strTitleForm;
                frm.btnCashDrop.Visible = false;
                frm.btnPay.Visible = false;
            }
            DialogResult dlg = frm.ShowDialog();
            if (dlg == DialogResult.OK)
            {
                so = frm.mShiftObject;
                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
            }
        }

        private void frmFuntion_FormClosed(object sender, FormClosedEventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void frmFuntion_Load(object sender, EventArgs e)
        {
            Connection.Connection con = new Connection.Connection();
            if (con.CheckLocalhost())
            {
                LockButton(btnMenuChange, false);
                LockButton(btnChangeStaff, false);
                LockButton(btnStockCheck, false);
                LockButton(btnStaffMan, false);
                LockButton(btnSetPump, false);
            }
        }

        private void uct_StaffChange(UCChangeStaff.Staff staff)
        {
            if (Convert.ToInt16(mReadDBConfig.AllowSalesFastFood) == 1)
            {
                mfrmOrdersAllQuickSalse.SetStaff(staff.StaffID, staff.Name);
            }
            else
            {
                mfrmOrdersAll.SetStaff(staff.StaffID, staff.Name);
            }
        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            frmLoginDialog frmlogindialog = new frmLoginDialog();
            frmlogindialog.Permission = frmLoginDialog.MANAGER;
            if (frmlogindialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmMail frm = new frmMail();
                frm.ShowDialog();
            }
        }

        private void btnSetBarcodeItem_Click(object sender, EventArgs e)
        {
            if (mfrmOrdersAll.mStaffPermission == 3)
            {
                frmSetBarcodeItem frmSetBarcodeItem = new frmSetBarcodeItem(mSerialPort);
                frmSetBarcodeItem.ShowDialog();
            }
            else
            {
                frmLoginDialog frm = new frmLoginDialog();
                frm.Permission = 3;
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    frmSetBarcodeItem frmSetBarcodeItem = new frmSetBarcodeItem(mSerialPort);
                    frmSetBarcodeItem.ShowDialog();
                }
            }
        }

        private void btnTank_Click(object sender, EventArgs e)
        {
            if (mfrmOrdersAll.mStaffPermission == 3)
            {
                frmTank frmTank = new frmTank();
                frmTank.ShowDialog();
            }
            else
            {
                frmLoginDialog frm = new frmLoginDialog();
                frm.Permission = 3;
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    frmTank frmTank = new frmTank();
                    frmTank.ShowDialog();
                }
            }
        }
    }
}