﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmOrderLayout : Form
    {
        private Class.Functions mFunctions;
        private Class.MoneyFortmat mMoneyFortmat;

        public int OrderID { get; set; }

        private int mType;
        private Color[] mListColor = { Color.Blue, Color.White, Color.Orange, Color.Green };

        public frmOrderLayout()
        {
            InitializeComponent();
        }

        private void frmOrderLayout_Load(object sender, EventArgs e)
        {
            mFunctions = new Class.Functions();
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            switch (mType)
            {
                case 0:
                    LoadDelivery();
                    break;

                default:
                    break;
            }
        }

        public void POSLoadDelivery()
        {
            mType = 0;
        }

        public void POSLoadPickup()
        {
            mType = 1;
        }

        private void LoadDelivery()
        {
            List<Class.Functions.TakeAway> list = mFunctions.GetStatusTakeAway();
            foreach (var item in list)
            {
                try
                {
                    //ListViewItem li = new ListViewItem(item.orderid);
                    //li.SubItems.Add(mMoneyFortmat.Format2(item.SubTotal));
                    //listView1.Items.Add(li);
                    //li.BackColor=mListColor[item.complete];
                    //li.Tag = item.orderid;
                    UCChoseOrder ucCard = new UCChoseOrder();
                    ucCard.lblOrderID.Text = item.orderid;
                    ucCard.lblSubtotal.Text = "$ " + mMoneyFortmat.Format2(item.SubTotal);
                    ucCard.lblCustomer.Text = item.Name;

                    ucCard.lblOrderID.BackColor = mListColor[item.complete];
                    ucCard.lblSubtotal.BackColor = mListColor[item.complete];
                    ucCard.lblCustomer.BackColor = mListColor[item.complete];
                    ucCard.btnOrder.BackColor = mListColor[item.complete];

                    ucCard.lblOrderID.ForeColor = Color.White;
                    ucCard.lblSubtotal.ForeColor = Color.White;
                    ucCard.lblCustomer.ForeColor = Color.White;

                    ucCard.btnOrder.Tag = item.orderid;
                    ucCard.Click += new EventHandler(ucCard_Click);

                    ////UCDetailPump ucCard = new UCDetailPump();
                    //ucCard.lblName.Text = Convert.ToString(item.orderid);
                    //ucCard.lblSubtotal.Text = "$ " + mMoneyFortmat.Format2(item.SubTotal);
                    //ucCard.lblVolume.Visible = false;
                    //ucCard.Tag = Convert.ToString(item.orderid);
                    ////ucCard.btnFuelInformation.Tag = item.orderid.ToString() + "-" + "false";
                    //ucCard.btnFuelInformation.Tag = item.orderid.ToString();

                    //ucCard.BackColor = mListColor[item.complete];
                    //ucCard.lblName.ForeColor = mListColor[item.complete];
                    //ucCard.lblSubtotal.ForeColor = mListColor[item.complete];

                    //ucCard.btnFuelInformation.Click += new EventHandler(btnFuelInformation_Click);
                    if (item.type == 1 || item.type == 0)
                    {
                        flow_layout_order_delovery.Controls.Add(ucCard);
                    }
                    else if (item.type == 2)
                    {
                        flow_layout_order_pickup.Controls.Add(ucCard);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void ucCard_Click(object sender, EventArgs e)
        {
            try
            {
                UCChoseOrder uct = (UCChoseOrder)sender;
                this.OrderID = Convert.ToInt32(uct.btnOrder.Tag);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnFuelInformation_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                this.OrderID = Convert.ToInt32(btn.Tag);
                this.DialogResult = DialogResult.OK;
                //string strID = "";
                //string strCheck = "";
                //string str = btn.Tag.ToString();
                //string[] strButtonOrder = btn.Tag.ToString().Split('-');
                //strID = strButtonOrder[0];
                //strCheck = strButtonOrder[1];
                //foreach (UCDetailPump uc in flow_layout_order_delovery.Controls)
                //{
                //    uc.btnFuelInformation.Tag = uc.btnFuelInformation.Tag.ToString().Replace("true", "false");
                //    uc.btnFuelInformation.Image = (Bitmap)Resources.UnCheck;

                //}
                //if (Convert.ToBoolean(strCheck))
                //{
                //    btn.Image = (Bitmap)Resources.UnCheck;
                //    btn.Name = strID;
                //    btn.Tag = strID + "-" + "false";
                //}
                //else
                //{
                //    btn.Image = (Bitmap)Resources.checkbox;
                //    btn.Name = strID;
                //    btn.Tag = strID + "-" + "true";
                //}
            }
            catch (Exception)
            {
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}