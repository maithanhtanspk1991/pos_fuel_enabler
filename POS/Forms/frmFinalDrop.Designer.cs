﻿namespace POS.Forms
{
    partial class FormFinal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uCkeypad1 = new POS.UCkeypad();
            this.txtCFO = new POS.TextBoxNumbericPOS();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFinalDrop = new POS.TextBoxNumbericPOS();
            this.txttotalCFO = new POS.TextBoxNumbericPOS();
            this.lbStatus = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Till Balance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Final Drop";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cash Float Out";
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(438, 12);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 2;
            this.uCkeypad1.txtResult = null;
            // 
            // txtCFO
            // 
            this.txtCFO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtCFO.Enabled = false;
            this.txtCFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtCFO.Location = new System.Drawing.Point(173, 37);
            this.txtCFO.Name = "txtCFO";
            this.txtCFO.Size = new System.Drawing.Size(259, 26);
            this.txtCFO.TabIndex = 3;
            this.txtCFO.ucKeypad = null;
            this.txtCFO.uckeypadDiv = null;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(15, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(201, 74);
            this.button1.TabIndex = 4;
            this.button1.Text = "Accept";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(134, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "FINAL DROP";
            // 
            // txtFinalDrop
            // 
            this.txtFinalDrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtFinalDrop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtFinalDrop.Location = new System.Drawing.Point(173, 78);
            this.txtFinalDrop.Name = "txtFinalDrop";
            this.txtFinalDrop.Size = new System.Drawing.Size(259, 26);
            this.txtFinalDrop.TabIndex = 3;
            this.txtFinalDrop.ucKeypad = this.uCkeypad1;
            this.txtFinalDrop.uckeypadDiv = null;
            this.txtFinalDrop.TextChanged += new System.EventHandler(this.textBoxNumbericPOS3_TextChanged);
            // 
            // txttotalCFO
            // 
            this.txttotalCFO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txttotalCFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txttotalCFO.Location = new System.Drawing.Point(173, 119);
            this.txttotalCFO.Name = "txttotalCFO";
            this.txttotalCFO.Size = new System.Drawing.Size(259, 26);
            this.txttotalCFO.TabIndex = 3;
            this.txttotalCFO.ucKeypad = this.uCkeypad1;
            this.txttotalCFO.uckeypadDiv = null;
            this.txttotalCFO.TextChanged += new System.EventHandler(this.textBoxNumbericPOS3_TextChanged);
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(134, 171);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 20);
            this.lbStatus.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Brown;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(245, 208);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 74);
            this.button2.TabIndex = 4;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormFinal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 296);
            this.ControlBox = false;
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txttotalCFO);
            this.Controls.Add(this.txtFinalDrop);
            this.Controls.Add(this.txtCFO);
            this.Controls.Add(this.uCkeypad1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "FormFinal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Final Drop";
            this.Load += new System.EventHandler(this.FormFinal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private UCkeypad uCkeypad1;
        private TextBoxNumbericPOS txtCFO;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private TextBoxNumbericPOS txtFinalDrop;
        private TextBoxNumbericPOS txttotalCFO;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Button button2;
    }
}