﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmPump : Form
    {
        public string strCashAmount, strVolumeAmount;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private frmOrdersAll frmorder;
        private frmOrdersAllQuickSales frmorderQS;
        private Connection.Connection mconnect = new Connection.Connection();
        private bool mIsLockText = false;
        private Class.MoneyFortmat money;
        private string price = "";
        private string pumpID;

        public frmPump()
        {
            InitializeComponent();
        }

        public frmPump(string fid, Class.MoneyFortmat mmoney, frmOrdersAll frm)
        {
            InitializeComponent();
            pumpID = fid;
            money = mmoney;
            frmorder = frm;
        }

        public frmPump(string fid, Class.MoneyFortmat mmoney, frmOrdersAllQuickSales frm)
        {
            InitializeComponent();
            pumpID = fid;
            money = mmoney;
            frmorderQS = frm;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (txtunitprice.Text == "" || txtqty.Text == "" || txtsubtotal.Text == "")
            {
                posLabelStatus1.Text = "All information dosen't empty";
            }
            else
            {
                try
                {
                    int weight = 1000;
                    double dbQty = Convert.ToDouble(txtqty.Text);
                    int intQty = (int)(dbQty * weight);
                    double gst = Convert.ToDouble(txtunitprice.Tag);
                    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, Convert.ToInt32(cbbFuel.SelectedValue.ToString()), cbbFuel.Text, intQty, money.getFortMat(txtsubtotal.Text), Convert.ToDouble(price), gst, 0, 0, pumpID);
                    item.Weight = weight;
                    item.IsFuel = true;
                    item.PumID = pumpID;
                    if (Convert.ToBoolean(dbconfig.AllowSalesFastFood))
                    {
                        frmorderQS.Addnewitems(item);
                    }
                    else
                    {
                        frmorder.AddNewItems(item);
                    }
                    strCashAmount = txtsubtotal.Text;
                    strVolumeAmount = money.Format2(intQty);
                }
                catch (Exception)
                {
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void cbbFuel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtqty.Text = "";
                txtsubtotal.Text = "";
                string sql = "select unitPrice,gst from itemsmenu where itemID=" + cbbFuel.SelectedValue.ToString();
                mconnect.Open();
                //price = mconnect.ExecuteScalar(sql).ToString();
                DataTable tbl = mconnect.Select(sql);
                price = tbl.Rows[0]["unitPrice"].ToString();
                txtunitprice.Text = money.Format(price);
                txtunitprice.Tag = tbl.Rows[0]["gst"];
            }
            catch (Exception)
            {
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void CheckSubTotalbyQty(double unit)
        {
            double unitprice = unit;
            double qty = 0;
            double subtotal = 0;
            if (txtqty.Text != "")
            {
                try
                {
                    qty = Convert.ToDouble(txtqty.Text);
                    subtotal = qty * unitprice;
                    txtsubtotal.Text = money.FormatCurenMoney(subtotal);
                }
                catch (Exception) { }
            }
        }

        private void CheckSubTotalbySubtotal(double unit)
        {
            double unitprice = unit;
            double qty = 0;
            double subtotal = 0;
            if (txtsubtotal.Text != "")
            {
                try
                {
                    subtotal = Convert.ToDouble(txtsubtotal.Text);
                    qty = subtotal / unitprice;
                    txtqty.Text = money.FormatCurenMoney(qty);
                }
                catch (Exception) { }
            }
        }

        private void frmPump_Load(object sender, EventArgs e)
        {
            txtunitprice.Text = "";
            GetFueltype(pumpID);
        }

        private void GetFueltype(string pumpid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select h.*,i.scanCode,i.itemShort,i.itemDesc,i.unitPrice,i.groupID,gst from headpump h inner join itemsmenu i on h.itemID=i.itemID where h.pumpID=" + pumpid + " and i.visual=1 group by itemID";
                mconnect.Open();
                dt = mconnect.Select(sql);
                mconnect.Close();
                foreach (DataRow row in dt.Rows)
                {
                    Pump pum = new Pump(row["pumpID"].ToString(), row["itemID"].ToString(), Convert.ToDouble(row["unitPrice"].ToString()), row["itemDesc"].ToString(), row["scanCode"].ToString(), Convert.ToDouble(row["gst"]));
                    cbbFuel.DisplayMember = "itemDesc";
                    cbbFuel.ValueMember = "itemID";
                    cbbFuel.DataSource = dt;
                }
                if (dt.Rows.Count > 0)
                {
                    cbbFuel.SelectedValue = dt.Rows[0]["itemID"];
                    cbbFuel_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmPump:::GetFueltype:::" + ex.Message);
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (mIsLockText)
            {
                return;
            }
            mIsLockText = true;
            if (txtunitprice.Text != "")
            {
                CheckSubTotalbyQty(Convert.ToDouble(txtunitprice.Text));
            }
            else
            {
                posLabelStatus1.Text = "Please select fuel type first";
            }
            mIsLockText = false;
        }

        private void txtsubtotal_TextChanged(object sender, EventArgs e)
        {
            if (mIsLockText)
            {
                return;
            }
            mIsLockText = true;
            if (txtunitprice.Text != "")
            {
                CheckSubTotalbySubtotal(Convert.ToDouble(txtunitprice.Text));
            }
            else
            {
                posLabelStatus1.Text = "Please select fuel type first";
            }
            mIsLockText = false;
        }

        private class Pump
        {
            public Pump(string pumpid, string itemid, double unitprice, string itemname, string scancode, double gst)
            {
                PumpID = pumpid;
                ItemID = itemid;
                UnitPrice = unitprice;
                ItemName = itemname;
                ScanCode = scancode;
                Gst = gst;
            }

            public double Gst { get; set; }

            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public string PumpID { get; set; }

            public double Qty { get; set; }

            public string ScanCode { get; set; }

            public double SubTotal { get; set; }

            public double UnitPrice { get; set; }
        }
    }
}