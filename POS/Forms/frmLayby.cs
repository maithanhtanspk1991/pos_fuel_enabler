﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLayby : Form
    {
        private Class.MoneyFortmat money;
        private Class.ProcessOrderNew.Order morder;
        private frmOrdersAll frmorder;
        private Printer mprinter;
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Connection.Connection mconnect = new Connection.Connection();
        private Class.ProcessOrderNew processOrder;
        private string Serial = "";
        private double Tickettotal_ = 0;
        private double Balance_ = 0;

        public frmLayby(Class.MoneyFortmat mney, Class.ProcessOrderNew.Order order, frmOrdersAll frm, Class.ProcessOrderNew process)
        {
            InitializeComponent();
            money = mney;
            morder = order;
            frmorder = frm;
            processOrder = process;
            mprinter = new Printer();
            mprinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;
            string header = "BECAS POS DEMO";
            string bankCode = "ABN. 1234567890";
            string address = "133 Alexander Street, Crows Nest 2065";
            string tell = "T:94327867 M:0401950967";

            l_y = mprinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mprinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mprinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mprinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 100;

            DateTime dateTime = DateTime.Now;
            mprinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            l_y += 100;

            l_y = mprinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += 100;

            l_y = mprinter.DrawString("Name: " + txtcustomer.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Mobile: " + txtmobile.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Home/Work: " + txthomework.Text, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Layby Deposit: " + txtlaybydeposit.Text + "$", e, font11, l_y, 1);
            l_y = mprinter.DrawString("Customer No: " + Serial, e, font11, l_y, 1);
            l_y = mprinter.DrawString("Serial Layby: " + Serial, e, font11, l_y, 1);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (morder.ListItem.Count > 0)
            {
                if (CheckInput() == true)
                {
                    if (SendLayby())
                    {
                        mprinter.SetPrinterName(dbconfig.BarPrinter.ToString());
                        mprinter.Print();
                        this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                        posLabelStatus1.Text = "";
                        frmorder.NextCustomer();
                    }
                }
            }
            else
            {
                posLabelStatus1.Text = "Order not send lay by";
            }
        }

        private void frmLayby_Load(object sender, EventArgs e)
        {
            if (morder.ListItem.Count > 0)
            {
                LoadListView(morder);
            }
        }

        private class ItemClick
        {
            public int ItemIndex { get; set; }

            public int ItemOptionIndex { get; set; }

            public ItemClick(int itemIndex, int itemOptionIndex)
            {
                ItemIndex = itemIndex;
                ItemOptionIndex = itemOptionIndex;
            }
        }

        private string RandomNumber()
        {
            DateTime tmeNow = DateTime.Now;
            int mls = tmeNow.Millisecond;
            // Generate two random numbers between 100 and 999
            Random rndNumber = new Random(mls);
            int NewNumber1 = rndNumber.Next(100, 999);
            int NewNumber2 = rndNumber.Next(100, 999);
            int NewNumber3 = rndNumber.Next(100, 999);
            int NewNumber4 = rndNumber.Next(100, 999);
            // Create an item number from the random numbers
            String strItemNumber = NewNumber1.ToString() + NewNumber2.ToString() + NewNumber3.ToString() + NewNumber4.ToString();
            return strItemNumber;
        }

        private bool CheckInput()
        {
            if (txtcustomer.Text == "")
            {
                posLabelStatus1.Text = "Invalid Customer infomation";
                return false;
            }
            else if (txtmobile.Text == "" || txtmobile.Text.Contains('.'))
            {
                posLabelStatus1.Text = "Invalid Mobile Number";
                return false;
            }
            else if (txthomework.Text == "" || txthomework.Text.Contains('.'))
            {
                posLabelStatus1.Text = "Invalid Home/Work Number";
                return false;
            }
            else if (txtlaybydeposit.Text == "" || txtlaybydeposit.Text.Contains('.') || Convert.ToDouble(txtlaybydeposit.Text) == 0)
            {
                posLabelStatus1.Text = "Invalid Layby deposit";
                return false;
            }
            return true;
        }

        private bool SendLayby()
        {
            string strSQL = "";
            string refID = RandomNumber();
            try
            {
                processOrder.SendOrder(morder);
                mconnect.Open();
                mconnect.BeginTransaction();
                Serial = refID;
                string maxorderID = mconnect.ExecuteScalar("select max(orderID) from ordersdaily").ToString();
                strSQL += "insert into customers(Name,mobile,phone,memberNo) value(" + "'" + txtcustomer.Text + "','" + txtmobile.Text + "','" + txthomework.Text + "','" + refID + "'" + ");";
                strSQL += "set @cnew = LAST_INSERT_ID();";
                strSQL += "update ordersdaily set custID=@cnew,refID=" + "'" + refID + "'" + " where orderID=" + "'" + maxorderID + "';";
                strSQL += "insert into laybyorderspayments(orderID,refID,deposit) value('" + maxorderID + "','" + refID + "','" + money.getFortMat(txtlaybydeposit.Text) + "'" + ");";
                mconnect.ExecuteScript(strSQL);
                mconnect.Commit();
                return true;
            }
            catch (Exception)
            {
                mconnect.Rollback();
                return false;
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void LoadListView(Class.ProcessOrderNew.Order lorder)
        {
            listView1.Items.Clear();
            for (int i = 0; i < lorder.ListItem.Count; i++)
            {
                Class.ProcessOrderNew.Item item = lorder.ListItem[i];
                ListViewItem li = new ListViewItem(item.Qty + "");
                li.SubItems.Add(item.ItemName);
                li.SubItems.Add(money.Format(item.SubTotal));
                li.Tag = new ItemClick(i, -1);
                li.EnsureVisible();
                listView1.Items.Add(li);
                for (int j = 0; j < item.ListSubItem.Count; j++)
                {
                    Class.ProcessOrderNew.SubItem sub = item.ListSubItem[j];
                    li = new ListViewItem(sub.Qty + "");
                    li.SubItems.Add(sub.ItemName);
                    li.SubItems.Add(money.Format(sub.SubTotal));
                    li.Tag = new ItemClick(i, j);
                    listView1.Items.Add(li);
                    li.EnsureVisible();
                }
            }
            if (listView1.Items.Count > 0)
            {
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
        }

        private void txtcustomer_Click(object sender, EventArgs e)
        {
            frmKeyboard frm = new frmKeyboard(txtcustomer);
            frm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtserial.Text != "")
            {
                listView1.Items.Clear();
                listView2.Items.Clear();
                DataTable dt = new DataTable();
                dt = GetOrderBySerial(txtserial.Text);
                if (dt.Rows.Count > 0)
                {
                    double Tickettotal = 0;
                    double Balance = 0;
                    Tickettotal = LoadItemList(dt, Tickettotal);
                    DataTable dtpay = new DataTable();
                    dtpay = GetLaybyOrderPayMentBySerial(txtserial.Text);
                    if (dtpay.Rows.Count > 0)
                    {
                        Balance = LoadPayMentList(Balance, dtpay);
                    }
                    DataTable dtc = new DataTable();
                    dtc = GetCustomerBySerial(txtserial.Text);
                    if (dtc.Rows.Count > 0)
                    {
                        txtcustomer.Text = dtc.Rows[0]["Name"].ToString();
                        txtmobile.Text = dtc.Rows[0]["mobile"].ToString();
                        txthomework.Text = dtc.Rows[0]["phone"].ToString();
                    }
                    //Math.Round(Tickettotal_ = Tickettotal,2);
                    Tickettotal_ = Tickettotal;
                    Balance_ = Tickettotal - Balance;
                    Balance_ = Math.Round(Balance_, 3);
                    txttickettotal.Text = Tickettotal.ToString();
                    txtbalance.Text = Balance_.ToString();
                    posLabelStatus1.Text = "";
                }
                else
                {
                    posLabelStatus1.Text = "Layby not found with this serial";
                    listView1.Items.Clear();
                    listView2.Items.Clear();
                    txtcustomer.Text = "";
                    txtmobile.Text = "";
                    txthomework.Text = "";
                    txttickettotal.Text = "";
                    txtbalance.Text = "";
                }
            }
            else
            {
                posLabelStatus1.Text = "Please insert serial number first.";
            }
        }

        private double LoadPayMentList(double Balance, DataTable dtpay)
        {
            foreach (DataRow rowp in dtpay.Rows)
            {
                ListViewItem lv = new ListViewItem(rowp["payID"].ToString());
                lv.SubItems.Add(rowp["ts"].ToString());
                lv.SubItems.Add(money.Format(Convert.ToDouble(rowp["deposit"].ToString())));
                listView2.Items.Add(lv);
                Balance += Convert.ToDouble(money.Format(Convert.ToDouble(rowp["deposit"].ToString())));
            }
            return Balance;
        }

        private double LoadItemList(DataTable dt, double Tickettotal)
        {
            foreach (DataRow row in dt.Rows)
            {
                ListViewItem lvi = new ListViewItem(row["qty"].ToString());
                lvi.SubItems.Add(row["itemDesc"].ToString());
                lvi.SubItems.Add(money.Format(Convert.ToDouble(row["subTotal"].ToString())));
                listView1.Items.Add(lvi);
                Tickettotal += Convert.ToDouble(money.Format(Convert.ToDouble(row["subTotal"].ToString())));
            }
            return Tickettotal;
        }

        private DataTable GetLaybyOrderPayMentBySerial(string serial)
        {
            DataTable dt = new DataTable();
            try
            {
                mconnect.Open();
                dt = mconnect.Select("select payID,orderID,ts,deposit,bkId from laybyorderspayments where refID='" + serial + "'");
                mconnect.Close();
                return dt;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmLayby:::GetLaybyOrderPayMentBySerial:::" + ex);
            }
            return dt;
        }

        private DataTable GetCustomerBySerial(string serial)
        {
            DataTable dt = new DataTable();
            try
            {
                mconnect.Open();
                dt = mconnect.Select("select Name,mobile,phone from customers where memberNo='" + serial + "'");
                mconnect.Close();
                return dt;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmLayby:::GetCustomerBySerial:::" + ex.Message);
            }
            return dt;
        }

        private DataTable GetOrderBySerial(string serial)
        {
            DataTable dt = new DataTable();
            try
            {
                mconnect.Open();
                string orderid = mconnect.ExecuteScalar("select orderID from ordersdaily where refID='" + serial + "' and completed<>1").ToString();
                if (orderid != null)
                {
                    dt = mconnect.Select(
                        "select " +
                        "qty," +
                        "itemID," +
                        "if(dynID<>0,(select itemDesc from dynitemsmenu where dynID=b.dynID),if(itemID<>0,(select itemDesc from itemsmenu where itemID=b.itemID),(select itemDesc from itemslinemenu where lineID=b.optionID))) as itemDesc," +
                        "dynID," +
                        "price," +
                        "subTotal," +
                        "optionID " +
                    "from ordersdailyline b " +
                    "where orderID=" + orderid);
                }
                else
                {
                    string bkid = mconnect.ExecuteScalar("select bkId from ordersall where refID='" + serial + "' and completed<>1").ToString();
                    if (bkid != null)
                    {
                        dt = mconnect.Select(
                        "select " +
                        "qty," +
                        "itemID," +
                        "if(dynID<>0,(select itemDesc from dynitemsmenu where dynID=b.dynID),if(itemID<>0,(select itemDesc from itemsmenu where itemID=b.itemID),(select itemDesc from itemslinemenu where lineID=b.optionID))) as itemDesc," +
                        "dynID," +
                        "price," +
                        "subTotal," +
                        "optionID " +
                    "from ordersallline b " +
                    "where bkId=" + bkid);
                    }
                }
                mconnect.Close();
                return dt;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmLayby:::GetOrderBySerial:::" + ex.Message);
            }
            return dt;
        }

        private bool PayMore(string serial, double deposit, string bkid, string orderid)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = GetOrderBySerial(serial);
                if (dt.Rows.Count <= 0)
                {
                    return false;
                }
                else
                {
                    mconnect.Open();
                    mconnect.BeginTransaction();
                    mconnect.ExecuteNonQuery("insert into laybyorderspayments(refID,deposit,bkId,orderID) value('" + serial + "','" + deposit + "','" + bkid + "','" + orderid + "')");
                    mconnect.Commit();
                    return true;
                }
            }
            catch (Exception)
            {
                mconnect.Rollback();
                return false;
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtserial.Text != "" && txtlaybydeposit.Text != "")
            {
                if (Convert.ToDouble(txtlaybydeposit.Text) < Convert.ToDouble(txtbalance.Text))
                {
                    DataTable dto = new DataTable();
                    dto = GetLaybyOrderPayMentBySerial(txtserial.Text);
                    if (dto.Rows.Count > 0)
                    {
                        if (PayMore(txtserial.Text, money.getFortMat(txtlaybydeposit.Text), dto.Rows[0]["bkId"].ToString(), dto.Rows[0]["orderID"].ToString()) == true)
                        {
                            listView2.Items.Clear();
                            double Balance = 0;
                            DataTable dtpay = new DataTable();
                            dtpay = GetLaybyOrderPayMentBySerial(txtserial.Text);
                            if (dtpay.Rows.Count > 0)
                            {
                                foreach (DataRow rowp in dtpay.Rows)
                                {
                                    ListViewItem lv = new ListViewItem(rowp["payID"].ToString());
                                    lv.SubItems.Add(rowp["ts"].ToString());
                                    lv.SubItems.Add(money.Format(Convert.ToDouble(rowp["deposit"].ToString())));
                                    listView2.Items.Add(lv);
                                    Balance += Convert.ToDouble(money.Format(Convert.ToDouble(rowp["deposit"].ToString())));
                                }
                            }
                            Math.Round(Balance_ = Tickettotal_ - Balance, 3);
                            txtbalance.Text = Balance_.ToString();
                        }
                        else
                        {
                            posLabelStatus1.Text = "Not pay more with invalid serial";
                        }
                    }
                }
                else
                {
                    posLabelStatus1.Text = "Press Pay OFF to completed lay by";
                }
            }
            else
            {
                posLabelStatus1.Text = "Please insert serial number and layby deposit first";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtserial.Text != "")
            {
                if (PayOff(txtserial.Text) == true)
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    posLabelStatus1.Text = "Not pay off with invalid serial";
                }
            }
            else
            {
                posLabelStatus1.Text = "Please insert serial number first";
            }
        }

        private bool PayOff(string serial)
        {
            try
            {
                mconnect.Open();
                mconnect.BeginTransaction();
                DataTable dt = new DataTable();
                dt = mconnect.Select("select * from ordersdaily where refID=" + serial);
                if (dt.Rows.Count > 0)
                {
                    double Laybydeposit = Convert.ToDouble(mconnect.ExecuteScalar("select sum(deposit) from laybyorderspayments where refID=" + serial));
                    double laybyoff = Convert.ToDouble(dt.Rows[0]["subTotal"].ToString()) - Laybydeposit;
                    mconnect.ExecuteNonQuery("insert into laybyorderspayments(orderID,refID,deposit) value('" + dt.Rows[0]["orderID"].ToString() + "','" + dt.Rows[0]["refID"].ToString() + "','" + laybyoff + "')");
                    mconnect.ExecuteNonQuery("update ordersdaily set completed=1 where refID='" + serial + "'");
                    return true;
                }
                else
                {
                    DataTable dtall = new DataTable();
                    dtall = mconnect.Select("select * from ordersall where refID=" + serial);
                    if (dtall.Rows.Count > 0)
                    {
                        double Laybydeposit = Convert.ToDouble(mconnect.ExecuteScalar("select sum(deposit) from laybyorderspayments where refID=" + serial));
                        double laybyoff = Convert.ToDouble(dtall.Rows[0]["subTotal"].ToString()) - Laybydeposit;
                        mconnect.ExecuteNonQuery("insert into laybyorderspayments(orderID,refID,deposit) value('" + dtall.Rows[0]["orderID"].ToString() + "','" + dtall.Rows[0]["refID"].ToString() + "','" + laybyoff + "')");
                        mconnect.ExecuteNonQuery("update ordersdaily set completed=1 where refID='" + serial + "'");
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                mconnect.Commit();
            }
            catch (Exception ex)
            {
                mconnect.Rollback();
                Class.LogPOS.WriteLog("frmLayby:::PayOff:::" + ex.Message);
                return false;
            }
            finally
            {
                mconnect.Close();
            }
        }
    }
}