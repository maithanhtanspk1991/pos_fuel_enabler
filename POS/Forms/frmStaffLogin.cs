﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmStaffLogin : Form
    {
        private Connection.Connection mConnection;
        private bool mPasswordEnable = true;
        private Staff mStaff;

        public Staff StaffUser { get { return mStaff; } }

        public frmStaffLogin()
        {
            InitializeComponent();
        }

        private void frmStaffLogin_Load(object sender, EventArgs e)
        {
            mPasswordEnable = Class.StaffConfig.PasswordEnable();
            mConnection = new Connection.Connection();
            try
            {
                mConnection.Open();
                DataTable tbl = mConnection.Select("select stfId,staffID,Name from `staffs` order by Name");
                foreach (DataRow row in tbl.Rows)
                {
                    UCStaffButton btn = new UCStaffButton();
                    btn.Width = btn.Height = 100;
                    btn.StaffID = Convert.ToInt32(row["staffID"]);
                    btn.StaffName = row["Name"].ToString();
                    btn.Tag = row["stfId"];
                    btn.BackColor = Color.Blue;
                    btn.Click += new EventHandler(btn_Click);
                    flowLayoutPanel1.Controls.Add(btn);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            UCStaffButton btn = (UCStaffButton)sender;
            if (mPasswordEnable)
            {
                frmLoginDialog frm = new frmLoginDialog(btn.StaffID);
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    mStaff = new Staff(Convert.ToInt32(btn.Tag), btn.StaffName);
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                mStaff = new Staff(Convert.ToInt32(btn.Tag), btn.StaffName);
                this.DialogResult = DialogResult.OK;
            }
        }

        public class Staff
        {
            public int StaffID { get; set; }

            public string Name { get; set; }

            public Staff(int staffID, string name)
            {
                StaffID = staffID;
                Name = name;
            }
        }
    }
}