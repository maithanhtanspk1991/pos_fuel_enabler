﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmCard : Form
    {
        private DataObject.Card mCard = null;
        private Connection.Connection mConnection;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmCard()
        {
            InitializeComponent();
            SetMultiLanguage();
            mConnection = new Connection.Connection();
            SetValue();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "QUẢN LÝ THẺ";
                groupBox1.Text = "Thông tin thẻ";
                label1.Text = "Mã Thẻ";
                label2.Text = "Tên Thẻ";
                label6.Text = "Phần trăm phụ phí";
                label3.Text = "Mô tả";
                cbActive.Text = "Sử dụng thẻ";
                cbSurchart.Text = "Được phụ phí";
                btnNew.Text = "Tạo mới";
                btnSubmit.Text = "Lưu lại";
                btnDelete.Text = "Xóa";
                btnExit.Text = "Thoát";
                columnHeader1.Text = "Mã thẻ";
                columnHeader2.Text = "Tên thẻ";
                columnHeader3.Text = "Được phụ phí";
                columnHeader5.Text = "Phụ phí(%)";
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (mCard != null)
            {
                try
                {
                    mConnection.Open();
                    DataTable dtSource = mConnection.Select("SELECT count(cardID) FROM orderbycard where cardID=" + mCard.CardID);
                    if (Convert.ToInt32(dtSource.Rows[0][0]) == 0)
                    {
                        mConnection.ExecuteNonQuery("delete from card where cardID=" + mCard.CardID);
                    }
                    else
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            MessageBox.Show("Thẻ đã được sử dụng trong các đơn đặt hàng khác, xin vui lòng kiểm tra lại!"); 
                        }else{
							MessageBox.Show("Card has used in another orders , please check again !");
						}
                    }
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("DeleteCard:::" + ex.Message);
                }
                finally
                {
                    mConnection.Close();
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            mCard = null;
            SetValue();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (mCard == null)
            {
                if (CheckValue())
                {
                    GetValue();
                    if (BusinessObject.BOCard.Insert(mCard) > 0)
                    {
                        mCard = null;
                        SetValue();
                        LoadListview();
                    }
                }
            }
            else
            {
                if (CheckValue())
                {
                    GetValue();
                    if (BusinessObject.BOCard.Update(mCard) > 0)
                    {
                        mCard = null;
                        SetValue();
                        LoadListview();
                    }
                }
            }
        }

        private bool CheckValue()
        {
            if (txtCardCode.Text == "" || txtCardName.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Tất cả thông tin không được để trống!";
                    lbStatus.Visible = true;
                    return false;
                }
                lbStatus.Text = "All information not empty!";
                lbStatus.Visible = true;
                return false;
            }
            return true;
        }

        private void frmCard_Load(object sender, EventArgs e)
        {
            LoadListview();
        }

        private void GetValue()
        {
            if (mCard == null)
                mCard = new DataObject.Card();
            txtPercentSurcharge.Text = txtPercentSurcharge.Text != "" ? txtPercentSurcharge.Text : "0";
            mCard.CardName = txtCardName.Text;
            mCard.CardCode = txtCardCode.Text;
            mCard.Surchart = Convert.ToDouble(txtPercentSurcharge.Text);
            mCard.IsActive = cbActive.Checked;
            mCard.IsCashOut = cbCastOut.Checked;
            mCard.IsSurchart = cbSurchart.Checked;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCard.SelectedIndices.Count > 0)
            {
                mCard = (DataObject.Card)lvCard.SelectedItems[0].Tag;
                SetValue();
            }
        }

        private void LoadListview()
        {
            lvCard.Items.Clear();
            List<DataObject.Card> lsArray = BusinessObject.BOCard.GetAll(-1);
            foreach (DataObject.Card item in lsArray)
            {
                ListViewItem li = new ListViewItem(item.CardCode);
                li.SubItems.Add(item.CardName);
                li.SubItems.Add(Convert.ToBoolean(item.IsCashOut).ToString());
                li.SubItems.Add(Convert.ToBoolean(item.IsSurchart).ToString());
                li.SubItems.Add(item.Surchart.ToString() + "%");
                li.Tag = item;
                lvCard.Items.Add(li);
            }
        }

        private void SetValue()
        {
            if (mCard == null)
            {
                txtCardCode.Text = "";
                txtCardName.Text = "";
                txtPercentSurcharge.Text = "0";
                cbCastOut.Checked = false;
                cbActive.Checked = true;
                cbSurchart.Checked = false;

                btnDelete.Enabled = false;
            }
            else
            {
                txtCardCode.Text = mCard.CardCode;
                txtCardName.Text = mCard.CardName;
                txtPercentSurcharge.Text = mCard.Surchart.ToString();
                cbCastOut.Checked = mCard.IsCashOut;
                cbActive.Checked = mCard.IsActive;
                cbSurchart.Checked = mCard.IsSurchart;

                btnDelete.Enabled = true;
            }
        }
    }
}