﻿namespace POS.Forms
{
    partial class frmMenuItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCosPrice = new POS.TextBoxNumbericPOS();
            this.uCkeypad1 = new POS.UCkeypad();
            this.txttoarea = new POS.TextBoxNumbericPOS();
            this.txtprint = new POS.TextBoxNumbericPOS();
            this.txtstock = new POS.TextBoxNumbericPOS();
            this.txtqty = new POS.TextBoxNumbericPOS();
            this.txtunit = new POS.TextBoxNumbericPOS();
            this.txtprice1 = new POS.TextBoxNumbericPOS();
            this.txtprice2 = new POS.TextBoxNumbericPOS();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtarea = new POS.TextBoxNumbericPOS();
            this.txtsupplier = new POS.TextBoxNumbericPOS();
            this.txtitemname = new POS.TextBoxNumbericPOS();
            this.txtbarcode = new POS.TextBoxNumbericPOS();
            this.txtcategory = new POS.TextBoxNumbericPOS();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ucInfoTop1 = new POS.UCInfoTop();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvListGroup = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPrintLabel = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonSearchItem = new System.Windows.Forms.Button();
            this.btnSuppliers = new System.Windows.Forms.Button();
            this.buttonReceivedStock = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.posLabelStatus1 = new POS.POSLabelStatus();
            this.button9 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 740);
            this.panel1.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel1.Controls.Add(this.ucInfoTop1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uCkeypad1);
            this.splitContainer1.Panel2.Controls.Add(this.listView2);
            this.splitContainer1.Panel2.Controls.Add(this.lvListGroup);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Size = new System.Drawing.Size(832, 738);
            this.splitContainer1.SplitterDistance = 162;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 672F));
            this.tableLayoutPanel3.Controls.Add(this.txtCosPrice, 7, 1);
            this.tableLayoutPanel3.Controls.Add(this.txttoarea, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtprint, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtstock, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtqty, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtunit, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtprice1, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtprice2, 6, 1);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label12, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.label13, 7, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 93);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(822, 60);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // txtCosPrice
            // 
            this.txtCosPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtCosPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCosPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtCosPrice.Location = new System.Drawing.Point(143, 31);
            this.txtCosPrice.Margin = new System.Windows.Forms.Padding(0);
            this.txtCosPrice.Name = "txtCosPrice";
            this.txtCosPrice.ReadOnly = true;
            this.txtCosPrice.Size = new System.Drawing.Size(676, 26);
            this.txtCosPrice.TabIndex = 15;
            this.txtCosPrice.ucKeypad = this.uCkeypad1;
            this.txtCosPrice.uckeypadDiv = null;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uCkeypad1.Location = new System.Drawing.Point(617, 1);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 9;
            this.uCkeypad1.txtResult = null;
            // 
            // txttoarea
            // 
            this.txttoarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txttoarea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txttoarea.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txttoarea.Location = new System.Drawing.Point(3, 31);
            this.txttoarea.Margin = new System.Windows.Forms.Padding(0);
            this.txttoarea.Name = "txttoarea";
            this.txttoarea.ReadOnly = true;
            this.txttoarea.Size = new System.Drawing.Size(17, 26);
            this.txttoarea.TabIndex = 0;
            this.txttoarea.ucKeypad = this.uCkeypad1;
            this.txttoarea.uckeypadDiv = null;
            // 
            // txtprint
            // 
            this.txtprint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtprint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtprint.Location = new System.Drawing.Point(23, 31);
            this.txtprint.Margin = new System.Windows.Forms.Padding(0);
            this.txtprint.Name = "txtprint";
            this.txtprint.Size = new System.Drawing.Size(17, 26);
            this.txtprint.TabIndex = 1;
            this.txtprint.ucKeypad = this.uCkeypad1;
            this.txtprint.uckeypadDiv = null;
            this.txtprint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprint_KeyPress);
            // 
            // txtstock
            // 
            this.txtstock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtstock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtstock.Location = new System.Drawing.Point(43, 31);
            this.txtstock.Margin = new System.Windows.Forms.Padding(0);
            this.txtstock.Name = "txtstock";
            this.txtstock.Size = new System.Drawing.Size(17, 26);
            this.txtstock.TabIndex = 2;
            this.txtstock.ucKeypad = this.uCkeypad1;
            this.txtstock.uckeypadDiv = null;
            // 
            // txtqty
            // 
            this.txtqty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtqty.Location = new System.Drawing.Point(63, 31);
            this.txtqty.Margin = new System.Windows.Forms.Padding(0);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(17, 26);
            this.txtqty.TabIndex = 3;
            this.txtqty.ucKeypad = this.uCkeypad1;
            this.txtqty.uckeypadDiv = null;
            // 
            // txtunit
            // 
            this.txtunit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtunit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtunit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtunit.Location = new System.Drawing.Point(83, 31);
            this.txtunit.Margin = new System.Windows.Forms.Padding(0);
            this.txtunit.Name = "txtunit";
            this.txtunit.ReadOnly = true;
            this.txtunit.Size = new System.Drawing.Size(17, 26);
            this.txtunit.TabIndex = 4;
            this.txtunit.ucKeypad = this.uCkeypad1;
            this.txtunit.uckeypadDiv = null;
            // 
            // txtprice1
            // 
            this.txtprice1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtprice1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtprice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtprice1.Location = new System.Drawing.Point(103, 31);
            this.txtprice1.Margin = new System.Windows.Forms.Padding(0);
            this.txtprice1.Name = "txtprice1";
            this.txtprice1.ReadOnly = true;
            this.txtprice1.Size = new System.Drawing.Size(17, 26);
            this.txtprice1.TabIndex = 5;
            this.txtprice1.ucKeypad = this.uCkeypad1;
            this.txtprice1.uckeypadDiv = null;
            // 
            // txtprice2
            // 
            this.txtprice2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtprice2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtprice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtprice2.Location = new System.Drawing.Point(123, 31);
            this.txtprice2.Margin = new System.Windows.Forms.Padding(0);
            this.txtprice2.Name = "txtprice2";
            this.txtprice2.ReadOnly = true;
            this.txtprice2.Size = new System.Drawing.Size(17, 26);
            this.txtprice2.TabIndex = 6;
            this.txtprice2.ucKeypad = this.uCkeypad1;
            this.txtprice2.uckeypadDiv = null;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "To Area";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "Print Copies";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(46, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 25);
            this.label8.TabIndex = 9;
            this.label8.Text = "Stock Level";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label9.Location = new System.Drawing.Point(66, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "Qty received";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(86, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 25);
            this.label10.TabIndex = 11;
            this.label10.Text = "Unit Price";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(106, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 25);
            this.label11.TabIndex = 12;
            this.label11.Text = "Price 1";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(126, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 25);
            this.label12.TabIndex = 13;
            this.label12.Text = "Price 2";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label13.Location = new System.Drawing.Point(146, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(670, 25);
            this.label13.TabIndex = 14;
            this.label13.Text = "Cost Price";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.txtarea, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtsupplier, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtitemname, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtbarcode, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtcategory, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 30);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(822, 61);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // txtarea
            // 
            this.txtarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtarea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtarea.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtarea.Location = new System.Drawing.Point(3, 32);
            this.txtarea.Margin = new System.Windows.Forms.Padding(0);
            this.txtarea.Name = "txtarea";
            this.txtarea.ReadOnly = true;
            this.txtarea.Size = new System.Drawing.Size(160, 26);
            this.txtarea.TabIndex = 0;
            this.txtarea.ucKeypad = this.uCkeypad1;
            this.txtarea.uckeypadDiv = null;
            // 
            // txtsupplier
            // 
            this.txtsupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtsupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtsupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtsupplier.Location = new System.Drawing.Point(166, 32);
            this.txtsupplier.Margin = new System.Windows.Forms.Padding(0);
            this.txtsupplier.Name = "txtsupplier";
            this.txtsupplier.ReadOnly = true;
            this.txtsupplier.Size = new System.Drawing.Size(160, 26);
            this.txtsupplier.TabIndex = 1;
            this.txtsupplier.ucKeypad = this.uCkeypad1;
            this.txtsupplier.uckeypadDiv = null;
            // 
            // txtitemname
            // 
            this.txtitemname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtitemname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtitemname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtitemname.Location = new System.Drawing.Point(329, 32);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(0);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.ReadOnly = true;
            this.txtitemname.Size = new System.Drawing.Size(160, 26);
            this.txtitemname.TabIndex = 2;
            this.txtitemname.ucKeypad = this.uCkeypad1;
            this.txtitemname.uckeypadDiv = null;
            // 
            // txtbarcode
            // 
            this.txtbarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtbarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtbarcode.Location = new System.Drawing.Point(492, 32);
            this.txtbarcode.Margin = new System.Windows.Forms.Padding(0);
            this.txtbarcode.Name = "txtbarcode";
            this.txtbarcode.ReadOnly = true;
            this.txtbarcode.Size = new System.Drawing.Size(160, 26);
            this.txtbarcode.TabIndex = 3;
            this.txtbarcode.ucKeypad = this.uCkeypad1;
            this.txtbarcode.uckeypadDiv = null;
            this.txtbarcode.TextChanged += new System.EventHandler(this.txtbarcode_TextChanged);
            // 
            // txtcategory
            // 
            this.txtcategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtcategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtcategory.Location = new System.Drawing.Point(655, 32);
            this.txtcategory.Margin = new System.Windows.Forms.Padding(0);
            this.txtcategory.Name = "txtcategory";
            this.txtcategory.ReadOnly = true;
            this.txtcategory.Size = new System.Drawing.Size(164, 26);
            this.txtcategory.TabIndex = 4;
            this.txtcategory.ucKeypad = this.uCkeypad1;
            this.txtcategory.uckeypadDiv = null;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 26);
            this.label1.TabIndex = 5;
            this.label1.Text = "Area";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(169, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 26);
            this.label2.TabIndex = 6;
            this.label2.Text = "Supplier";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(332, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "Item Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(495, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Bar Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(658, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 26);
            this.label5.TabIndex = 9;
            this.label5.Text = "Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // ucInfoTop1
            // 
            this.ucInfoTop1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(85)))), ((int)(((byte)(160)))));
            this.ucInfoTop1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucInfoTop1.Location = new System.Drawing.Point(0, 0);
            this.ucInfoTop1.Name = "ucInfoTop1";
            this.ucInfoTop1.Size = new System.Drawing.Size(828, 29);
            this.ucInfoTop1.TabIndex = 0;
            this.ucInfoTop1.Tag = "0";
            // 
            // listView2
            // 
            this.listView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listView2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(413, 2);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(202, 514);
            this.listView2.TabIndex = 8;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Items";
            this.columnHeader5.Width = 220;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "SubTotal";
            this.columnHeader6.Width = 100;
            // 
            // lvListGroup
            // 
            this.lvListGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvListGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader1});
            this.lvListGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvListGroup.FullRowSelect = true;
            this.lvListGroup.GridLines = true;
            this.lvListGroup.HideSelection = false;
            this.lvListGroup.Location = new System.Drawing.Point(-4, 2);
            this.lvListGroup.MultiSelect = false;
            this.lvListGroup.Name = "lvListGroup";
            this.lvListGroup.Size = new System.Drawing.Size(415, 514);
            this.lvListGroup.TabIndex = 7;
            this.lvListGroup.UseCompatibleStateImageBehavior = false;
            this.lvListGroup.View = System.Windows.Forms.View.Details;
            this.lvListGroup.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ID";
            this.columnHeader2.Width = 40;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 280;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Visual";
            this.columnHeader1.Width = 80;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99999F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnPrintLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonSearchItem, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnSuppliers, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonReceivedStock, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(614, 272);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(214, 244);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // btnPrintLabel
            // 
            this.btnPrintLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnPrintLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrintLabel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPrintLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintLabel.ForeColor = System.Drawing.Color.White;
            this.btnPrintLabel.Location = new System.Drawing.Point(0, 183);
            this.btnPrintLabel.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrintLabel.Name = "btnPrintLabel";
            this.btnPrintLabel.Size = new System.Drawing.Size(106, 61);
            this.btnPrintLabel.TabIndex = 7;
            this.btnPrintLabel.Text = "PRINT LABEL";
            this.btnPrintLabel.UseVisualStyleBackColor = false;
            this.btnPrintLabel.Click += new System.EventHandler(this.btnPrintLabel_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(106, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 61);
            this.button2.TabIndex = 1;
            this.button2.Text = "SELECT";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 61);
            this.button1.TabIndex = 0;
            this.button1.Text = "LIST GROUP";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(0, 61);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 61);
            this.button3.TabIndex = 2;
            this.button3.Text = "CHECK STOCK";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(106, 61);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(108, 61);
            this.button4.TabIndex = 3;
            this.button4.Text = "STOCK TAKE";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonSearchItem
            // 
            this.buttonSearchItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.buttonSearchItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSearchItem.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonSearchItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchItem.ForeColor = System.Drawing.Color.White;
            this.buttonSearchItem.Location = new System.Drawing.Point(106, 122);
            this.buttonSearchItem.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSearchItem.Name = "buttonSearchItem";
            this.buttonSearchItem.Size = new System.Drawing.Size(108, 61);
            this.buttonSearchItem.TabIndex = 5;
            this.buttonSearchItem.Text = "SEARCH ITEM";
            this.buttonSearchItem.UseVisualStyleBackColor = false;
            this.buttonSearchItem.Click += new System.EventHandler(this.buttonSearchItem_Click);
            // 
            // btnSuppliers
            // 
            this.btnSuppliers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.btnSuppliers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSuppliers.Enabled = false;
            this.btnSuppliers.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSuppliers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuppliers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuppliers.ForeColor = System.Drawing.Color.White;
            this.btnSuppliers.Location = new System.Drawing.Point(106, 183);
            this.btnSuppliers.Margin = new System.Windows.Forms.Padding(0);
            this.btnSuppliers.Name = "btnSuppliers";
            this.btnSuppliers.Size = new System.Drawing.Size(108, 61);
            this.btnSuppliers.TabIndex = 4;
            this.btnSuppliers.UseVisualStyleBackColor = false;
            // 
            // buttonReceivedStock
            // 
            this.buttonReceivedStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.buttonReceivedStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonReceivedStock.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonReceivedStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReceivedStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReceivedStock.ForeColor = System.Drawing.Color.White;
            this.buttonReceivedStock.Location = new System.Drawing.Point(0, 122);
            this.buttonReceivedStock.Margin = new System.Windows.Forms.Padding(0);
            this.buttonReceivedStock.Name = "buttonReceivedStock";
            this.buttonReceivedStock.Size = new System.Drawing.Size(106, 61);
            this.buttonReceivedStock.TabIndex = 6;
            this.buttonReceivedStock.Text = "RECEIVE STOCK";
            this.buttonReceivedStock.UseVisualStyleBackColor = false;
            this.buttonReceivedStock.Click += new System.EventHandler(this.buttonReceivedStock_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.posLabelStatus1);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 519);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(828, 49);
            this.panel2.TabIndex = 5;
            // 
            // posLabelStatus1
            // 
            this.posLabelStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLabelStatus1.ForeColor = System.Drawing.Color.Red;
            this.posLabelStatus1.Location = new System.Drawing.Point(78, 0);
            this.posLabelStatus1.Name = "posLabelStatus1";
            this.posLabelStatus1.POSTimeMessenge = 1;
            this.posLabelStatus1.Size = new System.Drawing.Size(748, 47);
            this.posLabelStatus1.TabIndex = 2;
            this.posLabelStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(0, -1);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 50);
            this.button9.TabIndex = 1;
            this.button9.Text = "EXIT";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmMenuItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 740);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenuItems";
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenuItems_Load);
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private UCkeypad uCkeypad1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView lvListGroup;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private POSLabelStatus posLabelStatus1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private UCInfoTop ucInfoTop1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private TextBoxNumbericPOS txttoarea;
        private TextBoxNumbericPOS txtprint;
        private TextBoxNumbericPOS txtstock;
        private TextBoxNumbericPOS txtqty;
        private TextBoxNumbericPOS txtunit;
        private TextBoxNumbericPOS txtprice1;
        private TextBoxNumbericPOS txtprice2;
        private TextBoxNumbericPOS txtarea;
        private TextBoxNumbericPOS txtsupplier;
        private TextBoxNumbericPOS txtitemname;
        private TextBoxNumbericPOS txtbarcode;
        private TextBoxNumbericPOS txtcategory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnSuppliers;
        private System.Windows.Forms.Button buttonSearchItem;
        private System.Windows.Forms.Button buttonReceivedStock;
        private TextBoxNumbericPOS txtCosPrice;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnPrintLabel;
    }
}