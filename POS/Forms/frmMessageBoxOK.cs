﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMessageBoxOK : Form
    {
        private string mTitle;
        private string mMessage;

        public frmMessageBoxOK()
        {
            InitializeComponent();
        }

        public frmMessageBoxOK(string title, string message)
        {
            InitializeComponent();
            mTitle = title;
            mMessage = message;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void frmMessageBoxOK_Load(object sender, EventArgs e)
        {
            gbTitle.Text = mTitle;
            lbMessage.Text = mMessage;
        }
    }
}