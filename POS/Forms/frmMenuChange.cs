﻿using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmMenuChange : Form
    {
        private SystemConfig.DBConfig mDBConfig;
        private string _nameFrom = "frmMenuChange";
        private DataObject.ListItemType _Type = DataObject.ListItemType.Group;
        private DataObject.Query _Query = DataObject.Query.None;
        private string strBarcode = "";
        private string strNoticeNewItem = BusinessObject.BOConfig.GetNoticeNewItems;
        private bool checkSubmit = false;
        private bool checkSyns = true;
        private Barcode.SerialPort mSerialPortBarcode;
        private int mResultGroupID = 0;
        private int mResultItemID = 0;
        private DataObject.TypeMenu mShortcut = DataObject.TypeMenu.ALLMENU;
        private Class.ReadConfig mReadConfig;

        private Class.MoneyFortmat mMoney;

        private POS.Controls.UCGroupChange uCGroupChange = new Controls.UCGroupChange();
        private POS.Controls.UCItemsChange uCItemsChange = new Controls.UCItemsChange();

        public frmMenuChange(SystemConfig.DBConfig dBConfig, Barcode.SerialPort serialPortBarcode, Class.ReadConfig readConfig)
        {
            InitializeComponent();
            SetMultiLanguage();
            mDBConfig = dBConfig;
            mReadConfig = readConfig;
            mSerialPortBarcode = serialPortBarcode;
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPortBarcode.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnDelete.Text = "Xóa";
                btnNewItem.Text = "Tạo sản phẩm mới";
                btnNewGroup.Text = "Tạo nhóm mới";
                btnSubmitUpdate.Text = "Lưu lại";
                btnExit.Text = "Thoát";
                return;
            }
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPortBarcode.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPortBarcode.SetText("", txtBarcode);
                mSerialPortBarcode.SetText(data, txtBarcode);
            }
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text != "")
            {
                strBarcode = txt.Text;
                ucMenuChange._Barcode = txtBarcode.Text;
                if (ucMenuChange.RefreshBarcode())
                {
                    mShortcut = ucMenuChange.mShortcut;
                    LoadChangeItems();
                    LoadNavigation();
                }
                else
                {
                    frmItemsNewBarcode frmItemsNewBarcode = new Forms.frmItemsNewBarcode();
                    switch (frmItemsNewBarcode.ShowDialog())
                    {
                        case DialogResult.OK:
                            btnNewItem_Click(sender, e);
                            break;
                        case DialogResult.Yes:
                            frmSearchBarCode frm = new frmSearchBarCode(strBarcode);
                            if (frm.ShowDialog() == DialogResult.OK)
                            {
                                ucMenuChange._ItemID = frm.ItemMenu.ItemID;
                                if (ucMenuChange.RefreshItemID())
                                {
                                    ucMenuChange._ItemsMenu.Barcode = strBarcode;
                                    mShortcut = ucMenuChange.mShortcut;
                                    LoadChangeItems();
                                    LoadNavigation();
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                strBarcode = "";
                txt.Text = "";
            }
        }

        /// <summary>
        /// Thoát
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// sự kiện khi click vào menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucMenuChange_Click(object sender, EventArgs e)
        {
            LoadContent();
        }

        /// <summary>
        /// Load nội dung
        /// </summary>
        private void LoadContent()
        {
            btnDelete.Enabled = true;
            LoadNavigation();
            pnContent.Controls.Clear();
            _Query = DataObject.Query.Update;
            if (ucMenuChange._ItemsMenu != null)
            {
                LoadChangeItems();
                LoadNavigation();
            }
            else if (ucMenuChange._GroupMenu != null)
            {
                LoadChangeGroup();
            }
        }

        /// <summary>
        /// Load change item
        /// </summary>
        private void LoadChangeItems()
        {
            pnContent.Controls.Clear();
            uCItemsChange._ItemsMenu = ucMenuChange._ItemsMenu;
            uCItemsChange.SetValue(mDBConfig);
            uCItemsChange.Dock = DockStyle.Fill;
            uCItemsChange.LockSellType = true;
            _Query = DataObject.Query.Update;
            pnContent.Controls.Add(uCItemsChange);
        }

        /// <summary>
        /// Load change group
        /// </summary>
        private void LoadChangeGroup()
        {
            uCGroupChange._GroupMenu = ucMenuChange._GroupMenu;
            uCGroupChange.SetValue();
            uCGroupChange.Dock = DockStyle.Fill;
            _Query = DataObject.Query.Update;
            pnContent.Controls.Add(uCGroupChange);
        }

        /// <summary>
        /// Load Navigation
        /// </summary>
        private void LoadNavigation()
        {
            lbNavigation.Text = BusinessObject.FunctionEnum.GetTypeMenu(mShortcut) + " >> ";
            if (ucMenuChange._GroupMenu != null)
            {
                _Type = DataObject.ListItemType.Group;
                lbNavigation.Text += ucMenuChange._GroupMenu.GroupDesc;
            }
            if (ucMenuChange._ItemsMenu != null)
            {
                _Type = DataObject.ListItemType.Items;
                lbNavigation.Text += " >> ";
                lbNavigation.Text += ucMenuChange._ItemsMenu.ItemDesc;
            }
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "LoadLabelText::" + lbNavigation.Text);
        }

        /// <summary>
        /// Sự kiện load from
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucMenuChange_Load(object sender, EventArgs e)
        {
            ucMenuChange.InitMenu(mDBConfig);
            LoadContent();
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "ucMenuChange_Load");
        }

        /// <summary>
        /// Sự kiện delete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            frmMessageBox frmMessageBox = new Forms.frmMessageBox("Warning", "Do you want to delete?");
            if (frmMessageBox.ShowDialog() == DialogResult.OK)
            {
                _Query = DataObject.Query.Delete;
                switch (_Type)
                {
                    case DataObject.ListItemType.Group:
                        BusinessObject.BOGroupMenu.Delete(ucMenuChange._GroupMenu, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "btnDelete_Click::" + _Type.ToString() + "::" + ucMenuChange._GroupMenu.GroupDesc);
                        break;

                    case DataObject.ListItemType.Items:
                        BusinessObject.BOItemsMenu.Delete(ucMenuChange._ItemsMenu, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "btnDelete_Click::" + _Type.ToString() + "::" + ucMenuChange._ItemsMenu.ItemDesc);
                        break;
                }
                pnContent.Controls.Clear();
                LoadNavigation();
                ucMenuChange._ItemsMenu = null;
                ucMenuChange.Refresh();
                if (_Type == DataObject.ListItemType.Items)
                    ucMenuChange.LoadGroupbByItem(uCGroupChange._GroupMenu.GroupID, 0);
                LoadContent();
            }
        }

        /// <summary>
        /// New Group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewGroup_Click(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
            _Query = DataObject.Query.Insert;
            _Type = DataObject.ListItemType.Group;
            pnContent.Controls.Clear();
            uCGroupChange.Dock = DockStyle.Fill;
            uCGroupChange.Clear();
            uCGroupChange._GroupMenu.ShowDisplay = ucMenuChange.GroupDisplayOrderMax + 1;
            uCGroupChange._GroupMenu.GrShortCut = (int)mShortcut;
            uCGroupChange.SetValue();
            pnContent.Controls.Add(uCGroupChange);
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "btnNewGroup_Click");
        }

        /// <summary>
        /// New Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewItem_Click(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
            _Query = DataObject.Query.Insert;
            _Type = DataObject.ListItemType.Items;
            pnContent.Controls.Clear();
            uCItemsChange.mDBConfig = mDBConfig;
            uCItemsChange.Dock = DockStyle.Fill;
            uCItemsChange.Clear();
            uCItemsChange._ItemsMenu.Barcode = strBarcode;
            uCItemsChange._ItemsMenu.Gst = 10;
            uCItemsChange._ItemsMenu.ShowDisplay = ucMenuChange.ItemDisplayOrderMax + 1;
            uCItemsChange.SetValue(mDBConfig);
            uCItemsChange.LockSellType = false;

            pnContent.Controls.Add(uCItemsChange);
            SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "NewItem_Click");
        }

        /// <summary>
        /// Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmitUpdate_Click(object sender, EventArgs e)
        {
            switch (_Query)
            {
                case DataObject.Query.Insert:
                    Insert();
                    break;

                case DataObject.Query.Update:
                    Update();
                    break;
            }
            if (checkSubmit)
            {
                ucMenuChange.Refresh();
                if (mResultGroupID > 0)
                {
                    if (mResultItemID <= 0)
                        mResultItemID = 0;
                    ucMenuChange.LoadGroupbByItem(mResultGroupID, mResultItemID);
                    if (mResultItemID > 0)
                        LoadChangeItems();
                    else if (mResultGroupID > 0)
                        LoadChangeGroup();
                    LoadNavigation();

                    mResultItemID = 0;
                    mResultGroupID = 0;
                }
            }
        }

        /// <summary>
        /// Thêm
        /// </summary>
        private void Insert()
        {
            switch (_Type)
            {
                case DataObject.ListItemType.Group:
                    if (uCGroupChange.CheckInput())
                    {
                        DataObject.GroupMenu group = new DataObject.GroupMenu();
                        uCGroupChange.GetValue();
                        group = uCGroupChange._GroupMenu;
                        BusinessObject.BOGroupMenu.ChangeDispalyOrder(group, true, mDBConfig);
                        mResultGroupID = BusinessObject.BOGroupMenu.Insert(group, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "Insert::" + _Type.ToString() + "::" + group.GroupDesc);
                        checkSubmit = true;
                        if (group.DisplayOrder % 1 < 0.000000000001)
                            BusinessObject.BOGroupMenu.RefreshDisplayOrder(mDBConfig);
                    }
                    else
                        checkSubmit = false;
                    break;

                case DataObject.ListItemType.Items:
                    if (uCItemsChange.CheckInput())
                    {
                        DataObject.ItemsMenu item = new DataObject.ItemsMenu();
                        uCItemsChange.GetValue();
                        item = uCItemsChange._ItemsMenu;
                        item.GroupID = ucMenuChange._GroupMenu.GroupID;
                        mResultGroupID = ucMenuChange._GroupMenu.GroupID;
                        BusinessObject.BOItemsMenu.ChangeDispalyOrder(item, true, mDBConfig);
                        mResultItemID = BusinessObject.BOItemsMenu.Insert(item, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "Insert::" + _Type.ToString() + "::" + item.ItemDesc);
                        checkSubmit = true;
                        if (item.DisplayOrder % 1 < 0.000000000001)
                            BusinessObject.BOItemsMenu.RefreshDisplayOrder(item.GroupID, mDBConfig);
                    }
                    else
                        checkSubmit = false;
                    break;
            }
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        private void Update()
        {
            switch (_Type)
            {
                case DataObject.ListItemType.Group:
                    if (uCGroupChange.CheckInput())
                    {
                        DataObject.GroupMenu group = new DataObject.GroupMenu();
                        uCGroupChange.GetValue();
                        group = uCGroupChange._GroupMenu;
                        BusinessObject.BOGroupMenu.ChangeDispalyOrder(group, uCGroupChange._DispalyOrderChange, mDBConfig);
                        mResultGroupID = BusinessObject.BOGroupMenu.Update(group, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "Update::" + _Type.ToString() + "::" + group.GroupDesc);
                        checkSubmit = true;
                        if (group.DisplayOrder % 1 < 0.000000000001)
                            BusinessObject.BOGroupMenu.RefreshDisplayOrder(mDBConfig);
                    }
                    else
                        checkSubmit = false;
                    break;

                case DataObject.ListItemType.Items:
                    if (uCItemsChange.CheckInput())
                    {
                        DataObject.ItemsMenu item = new DataObject.ItemsMenu();
                        uCItemsChange.GetValue();
                        item = uCItemsChange._ItemsMenu;
                        item.GroupID = ucMenuChange._GroupMenu.GroupID;
                        mResultGroupID = ucMenuChange._GroupMenu.GroupID;
                        BusinessObject.BOItemsMenu.ChangeDispalyOrder(item, uCItemsChange._DispalyOrderChange, mDBConfig);
                        mResultItemID = BusinessObject.BOItemsMenu.Update(item, mDBConfig);
                        SystemLog.LogPOS.WriteLog("POS::" + _nameFrom + "::" + "Update::" + _Type.ToString() + "::" + item.ItemDesc);
                        checkSubmit = true;
                        if (item.DisplayOrder % 1 < 0.000000000001)
                            BusinessObject.BOItemsMenu.RefreshDisplayOrder(item.GroupID, mDBConfig);
                    }
                    else
                        checkSubmit = false;
                    break;
            }
        }

        private void frmMenuChange_Load(object sender, EventArgs e)
        {
            pnShortCutMenu.Visible = mReadConfig.IsTyreMenu;
        }

        private void frmMenuChange_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void btnAllMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.ALLMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }

        private void btnTyreMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.TYREMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.TYREMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }


        private void btnMechanic_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.MECHANICMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.MECHANICMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.MECHANICMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }

        private void btnMagMenu_Click(object sender, EventArgs e)
        {
            if (mShortcut != DataObject.TypeMenu.MAGMENU)
            {
                pnContent.Controls.Clear();
                lbNavigation.Text = "";
                mShortcut = DataObject.TypeMenu.MAGMENU;
                ucMenuChange.mShortcut = DataObject.TypeMenu.MAGMENU;
                ucMenuChange.Refresh();
                LoadContent();
            }
        }
    }
}