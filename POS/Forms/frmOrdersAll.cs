﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Forms;
using System.Collections.Generic;
using POS.Class;
using ITL.Enabler.API;
using ITL.Enabler.WFIntegration;
using ITL.Enabler.WPFControls;

namespace POS.Forms
{
    public partial class frmOrdersAll : Form
    {
        public static string dataReceive;
        public static DataTable dtSourceKindOfFuel;
        public static int mStaffID = 0;
        public static string strUserName = "";
        public frmSubTotal frmSubtotal;
        public frmPrePayFuel frmPrePayFuel;
        public int intTotalPump;
        public Class.ClientMessage mClientMessage;
        public Class.CustomerDisplayVFD mCustomerDisplay;
        public Class.ProcessOrderNew.Order mOrderMain;
        public Barcode.SerialPort mSerialPortBarcode;
        public DataObject.ShiftObject mShiftObject;
        public int mStaffPermission = 0;
        public DataObject.Transit mTransit = new DataObject.Transit();
        public TextBox txt;
        private const string _nameClass = "POS::Forms::frmOrdersAll::";
        private bool blnFlagIfLoading;
        private bool blnIsFuel;
        private bool blnIsOpen = true;
        private Class.Functions cFunctions = new Class.Functions();
        private Connection.Connection conn;
        private double dblSubtotalCashOut;
        private DataTable dtSource;
        private frmCustomerDisplay frmCustomer;
        private int intNumrow;
        private int intPumIDItemSelect;
        private int intPumpID;
        private int mCableID = 0;
        private SystemConfig.DBConfig mDBConfig = new SystemConfig.DBConfig();
        private Class.FuelAutoProtocol mFuelAutoProtocol;
        private bool mKeyThreadServerClient = true;
        private ListOrder mListOrder = new ListOrder();
        private ListOrderTemp mListOrderTemp = new ListOrderTemp();
        private bool mLocketText = true;
        private Class.MoneyFortmat money;
        private SerialPort.SerialPort mPortPage;
        private POS.Printer mPrinter;
        private BarPrinterServer mPrintServer;
        private Class.ProcessOrderNew mProcessOrder;
        private Class.ReadConfig mReadConfig = new ReadConfig();
        private Connection.ReadDBConfig mReadDBConfig = new Connection.ReadDBConfig();
        private Class.Setting mSetPrinter;
        private Class.Setting mSetting;
        private System.Threading.Thread mThreadServerClient;
        private UCInfoTop mUCInfoTop;

        //private event GetBarCode =new Barcode.SerialPort.MyPortEvenHandler.Received();
        #region Color

        public static readonly System.Drawing.Color Menu_01 = System.Drawing.Color.FromArgb(230, 90, 120);
        public static readonly System.Drawing.Color Menu_02 = System.Drawing.Color.FromArgb(255, 120, 0);
        public static readonly System.Drawing.Color Menu_03 = System.Drawing.Color.FromArgb(255, 200, 0);
        public static readonly System.Drawing.Color Menu_04 = System.Drawing.Color.FromArgb(145, 195, 85);
        public static readonly System.Drawing.Color Menu_05 = System.Drawing.Color.FromArgb(0, 170, 130);
        public static readonly System.Drawing.Color Menu_06 = System.Drawing.Color.FromArgb(100, 170, 255);
        public static readonly System.Drawing.Color Menu_07 = System.Drawing.Color.FromArgb(100, 175, 190);
        public static readonly System.Drawing.Color Menu_08 = System.Drawing.Color.FromArgb(190, 120, 175);
        public static readonly System.Drawing.Color Menu_09 = System.Drawing.Color.FromArgb(240, 145, 140);
        public static readonly System.Drawing.Color Menu_10 = System.Drawing.Color.FromArgb(150, 220, 160);
        public static readonly System.Drawing.Color Menu_11 = System.Drawing.Color.FromArgb(165, 160, 255);
        public static readonly System.Drawing.Color Menu_12 = System.Drawing.Color.FromArgb(150, 220, 200);
        public static readonly System.Drawing.Color Menu_13 = System.Drawing.Color.FromArgb(200, 185, 160);
        public static System.Drawing.Color[] listColor = { Menu_01, Menu_02, Menu_03, Menu_04, Menu_05, Menu_06, Menu_07, Menu_08, Menu_09, Menu_10, Menu_11, Menu_12, Menu_13 };

        #endregion Color

        private string strCashAmount = string.Empty;

        private string strFromPump = string.Empty;

        private string strGradeName = string.Empty;

        private string strGradeNo = string.Empty;

        private string strGST = string.Empty;

        private string strIDPumpHistory = string.Empty;

        private string strIDPumpTempHistory = string.Empty;

        private string strQuery = string.Empty;

        private string strUnitPrice = string.Empty;

        private string strVolumeAmount = string.Empty;

        public static Forecourt _Forecourt;  // Enabler API forecourt object
        Pump _CurrentSelectedPump = null;
        // Controls settings
        PumpControlSettings _PumpControlSettings = new PumpControlSettings();

        bool _ConnectEnablerServerRunning = false;
        public frmOrdersAll()
        {
            InitializeComponent();
           
            txt = new TextBox();
            conn = new Connection.Connection();            
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            SetMultiLanguage();
            mSerialPortBarcode = new Barcode.SerialPort();
            mSerialPortBarcode.OpenAndStart();

            mPrintServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            try
            {
                mCableID = Convert.ToInt32(mReadDBConfig.CableID);
                mTransit.CableID = mCableID;
                mTransit.StaffID = mStaffID;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "Load Cable ID :::" + ex.Message);
                mCableID = 1;
            }
            Init();
            mShiftObject = new DataObject.ShiftObject();
            mShiftObject.StaffID = mTransit.StaffID;
            mShiftObject.CableID = mTransit.CableID;
            mShiftObject.bActiveShift = false;
            CheckShift();
            LoadShift();
            InitFuelControl();
            InitClientMessage();
            BusinessObject.BOFuelHistory.UpdateAllRefreshCableIDByID(mReadConfig.iCableID);
            //Enabler Initialize and Operations
            if (mReadConfig.EnablerConfig.Enabler)
            {
                EnablerOperation();
            }
        }

        void EnablerOperation()
        {
            //Enabler
            this.flowLayoutPanel_Pumps.Controls.Clear();
            PumpControlSettings settings = new PumpControlSettings();
            settings.Style = PumpControlStyle.Small;

            // Create a Forecourt object for Enabler
            if (_Forecourt == null)
            {
                _Forecourt = new Forecourt();
            }

            try
            {
                _Forecourt.Connect(
                  mReadConfig.EnablerConfig.ServerName, // Hostname (or IP address) of the server
                  mReadConfig.EnablerConfig.TerminalID,        // Terminal number for your application
                  mReadConfig.EnablerConfig.myApplicationName, // String to Identify your application
                  mReadConfig.EnablerConfig.TerminalPassword,  // Password for the terminal (setup in Enabler Web Terminals page)
                  true);            // Request active forecourt control

                // Add code here to subscribe to Forecourt events - for example:
                //  MyForecourt.OnServerEvent = MyForecourt_OnServerEvent;
                //  MyForecourt.OnMessageReceived += MyForecourt_OnMessageReceived;
            }
            catch
            {
                // add code here to handle connection failure (e.g. display error to a user) 
            }

            // Update settings here if required
            foreach (Pump pump in _Forecourt.Pumps)
            {
                FormsPumpControl fpc = new FormsPumpControl(pump, settings);
                if (!mReadConfig.EnablerConfig.authorizeModePump)
                {
                    pump.OnHoseEvent += Pump_OnHoseEvent;

                }
                //Catch OnTransactionEvent of pumps
                pump.OnTransactionEvent += Pump_OnTransactionEvent;
                fpc.EnablerControl.OnSelected += PumpControl_OnSelected;
                this.flowLayoutPanel_Pumps.Controls.Add(fpc);
            }
        }
        /// <summary>
        /// Catch OnHoseEvent of the Pump in case we want to Authorise Transaction AUTOMATICALLY
        /// </summary>
        /// <param name="trans"></param>
        void Pump_OnHoseEvent(object sender, PumpHoseEventArgs e)
        {
            Pump pump = sender as Pump;
            _CurrentSelectedPump = pump;

            switch (e.EventType)
            {
                case HoseEventType.Block:
                    break;
                case HoseEventType.DeliveryGradeTimeout:
                    break;
                case HoseEventType.HoseChange:
                    break;
                case HoseEventType.LeftOut:
                    break;
                case HoseEventType.Lifted:
                    if (_CurrentSelectedPump.State == PumpState.Calling)
                    {
                        _CurrentSelectedPump.AuthoriseNoLimits("", "", -1);
                    }
                    break;
                case HoseEventType.Replaced:
                    //Make a sound
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer("Alarm10.wav");
                    player.Play();
                    //
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Catch OnTransactionEvent of the Pump
        /// </summary>
        /// <param name="trans"></param>
        void Pump_OnTransactionEvent(object sender, PumpTransactionEventArgs e)
        {
            Pump pump = sender as Pump;
            _CurrentSelectedPump = pump;

            switch (e.EventType)
            {
                case TransactionEventType.Authorised:
                    _CurrentSelectedPump.CurrentTransaction.GetLock();
                    break;
                case TransactionEventType.Completed:
                    if (_CurrentSelectedPump.CurrentTransaction.ClientActivity == "PrePay")
                    {
                        //If the transaction completed due to Timeout -> ReAuthorise again to make sure Fuel will be delivered to customers after Paid
                        if (_CurrentSelectedPump.CurrentTransaction.HistoryData.CompletionReason == CompletionReason.Timeout)
                        {
                            //Get Value of the CurrentTransaction Money Limit
                            PumpAuthoriseLimits ad = new PumpAuthoriseLimits();
                            ad.Value = _CurrentSelectedPump.CurrentTransaction.AuthoriseData.MoneyLimit;

                            int gradeID = Convert.ToInt32(_CurrentSelectedPump.CurrentTransaction.ClientReference);
                            // Reserving a Pump for a prepay transaction
                            try
                            {
                                // pass client activity and a client reference.
                                // set a flag to automatically clear if this turns out to be a zero
                                // transaction.
                                _CurrentSelectedPump.Reserve("PrePay", "", true);
                            }
                            catch (EnablerException Ex)
                            {
                                String MessageLine = String.Format("Command:{0} Result:{1} Info:{2}",
                                                                    "Reserve", Ex.ResultCode, Ex.Message);
                                //MessageBox.Show(MessageLine);
                                return;
                            }
                            pump.Authorise("PrePay", gradeID.ToString(), -1, ad);
                        }
                        else
                        {
                            try
                            {
                                Transaction trans = _CurrentSelectedPump.CurrentTransaction;
                                if (trans != null)
                                {
                                    trans.Clear(TransactionClearTypes.Normal);
                                }
                            }
                            catch (Exception ex)
                            {
                                SystemLog.LogPOS.WriteLog("Clear transaction: ERROR " + ex.Message);
                            }
                        }
                    }
                    else if(_CurrentSelectedPump.CurrentTransaction.ClientActivity == "")
                    {
                        Transaction _Trans = _CurrentSelectedPump.CurrentTransaction;
                        DeliveryData data = _Trans.DeliveryData;
                        int amount = Convert.ToInt32(data.Money * 1000);
                        int price = Convert.ToInt32(data.UnitPrice * 1000);
                        int qty = Convert.ToInt32(data.Quantity * 1000);
                        int itemID = mProcessOrder.GetitemIDbyName(data.Grade.Name);                   
                       
                        DataObject.FuelHistory fuel = new DataObject.FuelHistory(amount, _Trans.Hose.Number, 0, itemID, _Trans.Pump.Number, price, qty, 0);
                        BusinessObject.BOFuelHistory.InsertFuelHistory(fuel);
                    }
                    break;
                default:
                    break;
            }
        }

        void PumpControl_OnSelected(object sender, EventArgs e)
        {
            _CurrentSelectedPump = ((PumpControl)sender).Pump;
            //groupBox_PumpButtons.Header = String.Format("Pump controls for pump {0}", _CurrentSelectedPump.Number);
            // check for completed transactions and the pump state to decide what to do based on this click
            Transaction trans = _CurrentSelectedPump.CurrentTransaction;

            try
            {
                if (trans != null && _CurrentSelectedPump.TransactionStack.Count > 0)
                {
                    // we have a current transaction and a stacked one - display the transaction list
                    //ShowTransactionList(_CurrentSelectedPump);
                }
                else
                {
                    if (trans != null)
                    {

                        switch (trans.HistoryData.CompletionReason)
                        {
                            case CompletionReason.Normal:
                            case CompletionReason.StoppedByClient:
                            case CompletionReason.StoppedByLimit:
                            case CompletionReason.StoppedByError:
                                // we have a completed transaction; add it to a sale
                                //Add fuel transaction
                                AddFuelTransaction(trans);
                                break;
                            default:
                            case CompletionReason.Zero:
                                //ShowTransactionList(_CurrentSelectedPump);
                                break;

                            // Ignore 
                            case CompletionReason.NotComplete:
                                break;
                        }
                    }
                    else
                    {
                        // decide what to do based on the pump state
                        switch (_CurrentSelectedPump.State)
                        {
                            case PumpState.Locked:
                                //This pump is idle, nozzle in. 
                                //Display Dialog for Prepaying
                                //....

                                //idPumpHistory: 0 -> Normal
                                //idPumpHistory: 1 -> Post pay and has been added to Order list
                                //idPumpHistory: 2 -> Pre pay and hass been added to Order list

                                //Vu hardcode prepay $10 11/08/2015, then add the prepaid order into the Order list
                                //This step should be done in the Prepaying Dialog after user clicks OK button
                                //Get list of Grade from Forecourt
                                List<FuelGrade> lsgrades = new List<FuelGrade>();
                                //foreach (Grade g in _Forecourt.Grades)
                                //{
                                //    FuelGrade grade = new FuelGrade(g.Id, g.Name, g.GetPrice(1));
                                //    grades.Add(grade);
                                //}            
                                foreach(Hose hose in _CurrentSelectedPump.Hoses)
                                {
                                    Grade g = hose.Grade;
                                    FuelGrade grade = new FuelGrade(g.Id, g.Name, Convert.ToDecimal(money.Format((g.GetPrice(1) * 1000).ToString())));
                                    lsgrades.Add(grade);
                                }
                  
                                frmPrePayFuel = new frmPrePayFuel(_CurrentSelectedPump.Id, lsgrades, mReadConfig,money);
                                DialogResult dg = frmPrePayFuel.ShowDialog();
                                if (dg == DialogResult.OK)
                                {
                                    // Reserving a Pump for a prepay transaction
                                    try
                                    {
                                        // pass client activity and a client reference.
                                        // set a flag to automatically clear if this turns out to be a zero
                                        // transaction.
                                        _CurrentSelectedPump.Reserve("PrePay", "", true);
                                    }
                                    catch (EnablerException Ex)
                                    {
                                        String MessageLine = String.Format("Command:{0} Result:{1} Info:{2}",
                                                                            "Reserve", Ex.ResultCode, Ex.Message);
                                        //MessageBox.Show(MessageLine);
                                    }

                                    //Add the Order
                                    
                                    string gradeName = frmPrePayFuel.inputGrade.gradeName; //Hardcode - Can get Grade name from Prepaying Dialog
                                    int itemID = mProcessOrder.GetitemIDbyName(gradeName);
                                    // Some non normal transaction types don't have a grade associatted
                                    double subTotal = 0;
                                    double price = 0;
                                    double quantity = 0;
                                    subTotal = (double)frmPrePayFuel.inputAmount;
                                    price = Convert.ToDouble(frmPrePayFuel.inputGrade.gradePrice);
                                    quantity = frmPrePayFuel.inputVolumn * 1000;                                  

                                    double gst = 0;
                                    int changed = 0;
                                    int optionCount = 0;
                                    string pumpID = _CurrentSelectedPump.Number.ToString();

                                    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(BusinessObject.BOFuelHistory.getIDbyitemIDandPumpID(itemID,_CurrentSelectedPump.Id),
                                        Convert.ToInt32(itemID), gradeName, Convert.ToInt32(quantity), subTotal, Convert.ToDouble(price), gst, changed, optionCount, pumpID);
                                    item.ItemType = 0;
                                    item.IsFuel = true;
                                    item.Weight = 1000;
                                    AddNewItems(item);
                                }
                                break;

                            case PumpState.Calling:
                                _CurrentSelectedPump.AuthoriseNoLimits("", "", -1);
                                break;
                        }
                    }
                }
            }
            catch
            {
                //ShowEnablerError(EE);
            }

            //SetWindowsStates();
        }

        /// <summary>
        /// Add an existing fuel transaction to sale
        /// </summary>
        /// <param name="trans"></param>
        public bool AddFuelTransaction(Transaction trans)
        {
            //Check if transaction has been added to the Order list, if not: Add Fuel Transaction
            bool isAddFuelTransaction = true;
            foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
            {
                int idPumpHistory = Convert.ToInt32(item.IDPumpHistory);
                blnIsFuel = item.IsFuel;
                if (idPumpHistory > 0 && blnIsFuel)
                {
                    int PumpID = Convert.ToInt32(item.PumID);
                    if (_CurrentSelectedPump.Number == PumpID)
                    {
                        isAddFuelTransaction = false;
                    }
                }
            }
            if (!isAddFuelTransaction)
            {
                return true;
            }

            //Add Fuel Transaction
            try
            {
                if (!trans.IsLocked)
                {
                    trans.GetLock();
                }
                else
                {
                    // This could be a ReinsateAndLock transaction so it will be in the pump statck and already locked 
                    // Is it already locked by me
                    if (trans.IsLocked && trans.LockedById != _Forecourt.TerminalId)
                    {
                        // no ignore
                        return false;
                    }

                    //Check if transaction is already in list
                }

               

                // Some non normal transaction types don't have a grade associatted
                Grade grade = trans.DeliveryData.Grade;
                string gradeName = grade == null ? "" : grade.Name;
                int itemID = mProcessOrder.GetitemIDbyName(gradeName);
                // Some non normal transaction types don't have a grade associatted
                double subTotal = (double)trans.DeliveryData.Money * 1000;
                double price = (double)trans.DeliveryData.UnitPrice * 1000;
                double gst = 0;
                int changed = 0;
                int optionCount = 0;
                string pumpID = trans.Pump.Number.ToString();

                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(BusinessObject.BOFuelHistory.getIDbyitemIDandPumpID(itemID,trans.Pump.Number),
                    Convert.ToInt32(itemID), gradeName, Convert.ToInt32(trans.DeliveryData.Quantity * 1000), subTotal, price, gst, changed, optionCount, pumpID);
                item.ItemType = 0;
                item.IsFuel = true;
                item.Weight = 1000;
                AddNewItems(item);
            }
            catch (EnablerException)
            {
                return false;
            }

            return true;
        }

        public frmOrdersAll(int intNoneOfShift)
        {
            InitializeComponent();
            conn = new Connection.Connection();
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPortBarcode = new Barcode.SerialPort();
            mSerialPortBarcode.OpenAndStart();

            mPrintServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            try
            {
                mCableID = Convert.ToInt32(mReadDBConfig.CableID);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "Load Cable ID :::" + ex.Message);
                mCableID = 1;
            }
            Init(intNoneOfShift);
            mShiftObject = new DataObject.ShiftObject();
            mShiftObject.StaffID = mTransit.StaffID;
            mShiftObject.CableID = mTransit.CableID;
            mShiftObject.bActiveShift = false;

            CheckShift();
            //ballowshift = false;
            LoadShift();
            InitFuelControl();
            BusinessObject.BOFuelHistory.UpdateAllRefreshCableIDByID(mReadConfig.iCableID);
        }

        #region InitFuelControl
        private void InitFuelControl()
        {
            ucFuelControls.Init(this, mReadConfig, money);
            ucFuelControls.OnAddItem += new Fuel.UCFuelControls.AddItem(ucFuelControls_OnAddItem);
            ucFuelControls.OnChangeStatus += new Fuel.UCFuelControls.DelegateChangeStatus(ucFuelControls_OnChangeStatus);
        }

        void ucFuelControls_OnChangeStatus(string text)
        {
            lblStatus.Text = text;
        }

        void ucFuelControls_OnAddItem(ProcessOrderNew.Item item)
        {
            AddNewItems(item);
        }
        #endregion

        public frmOrdersAll(BarPrinterServer bar, Connection.Connection con, Class.MoneyFortmat m, Barcode.SerialPort sport)
        {
            InitializeComponent();
            mSerialPortBarcode = sport;
            money = m;
            mPrintServer = bar;
            conn = con;
            Init();
            mShiftObject = new DataObject.ShiftObject();
            mShiftObject.StaffID = mTransit.StaffID;
            mShiftObject.CableID = mTransit.CableID;
            CheckShift();
            InitFuelControl();
            mShiftObject.bActiveShift = false;
            //ballowshift = false;
            LoadShift();
        }

        private delegate void ChangeColorCallbackt(Color color, Control control);

        private delegate void ChangeEnableCallback(bool enable, Control control);

        private delegate void ChangeForceColorCallbackt(Color color, Control control);

        private delegate void ChangeTextCallback(string text, Control control);

        private delegate void ClearListview();

        private delegate void InfoMessageDel(ProcessOrderNew.Item item);

        public Class.MoneyFortmat Money { get { return money; } }

        public Barcode.SerialPort.PortReceiveFuelEvenHandler mSerialPortFuel_Received { get; set; }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnSubtotal.Text = "THANH TOÁN";
                btnFindItem.Text = "TÌM KIẾM";
                btnFunction.Text = "CHỨC NĂNG";
                btnVoidAll.Text = "XÓA TẤT CẢ";
                btnVoidItem.Text = "XÓA";
                btnDiscount.Text = "GIẢM GIÁ";
                btnMechanic.Text = "ME-CHANIC";
                btnPreOrder.Text = "XEM LẠI ĐƠN HÀNG";
                btnPrint.Text = "IN";
                btnFuelDisc.Text = "GIẢM GIÁ XĂNG";
                btnNextCus.Text = "KHÁCH HÀNG KẾ";
                btnNewItem.Text = "SẢN PHẨM MỚI";
                buttonRefun.Text = "TRẢ LẠI HÀNG";
                label1.Text = "Tên hàng :";
                label3.Text = "Số lượng :";
                label4.Text = "Đơn vị giá :";
                label7.Text = "Nhân viên :";
                label8.Text = "Mã vạch :";
                lbOrderID.Text = "Mã hàng :";
                columnHeader1.Text = "Số lượng";
                columnHeader2.Text = "Tên hàng";
                columnHeader3.Text = "Thành tiền";
                money = new MoneyFortmat(Class.MoneyFortmat.VN_TYPE);
                return;
            }
        }

        public void AddNewItems(Class.ProcessOrderNew.Item item)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.ProcessOrderNew.Item itemCus = mOrderMain.AddItem(item, lvOrder, money, true);
            item.Weight = item.IsFuel ? 1000 : 1;
            mOrderMain.CheckComboDiscount(lvOrder, money, mOrderMain.cboDisc, mOrderMain.resultDisc, mOrderMain.ListItem);
            SendToCustomerDisplay(itemCus);
            try
            {
                mOrderMain.PumpID = item.PumID;
            }
            catch{}
            mOrderMain.CheckFuelDiscount(lvOrder, money);
            TotalLabel();

            frmCustomer.ProcessOrder(mOrderMain);
        }
        private void btnNextCus_Click(object sender, EventArgs e)
        {
            btnVoidAll_Click(null, null);
            NextCustomer();
        }

        public void ChangeColort(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeColorCallbackt d = new ChangeColorCallbackt(ChangeColort);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.BackColor = color;
            }
        }

        public void ChangeEnable(bool enable, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeEnableCallback d = new ChangeEnableCallback(ChangeEnable);
                control.Invoke(d, new object[] { enable, control });
            }
            else
            {
                control.Enabled = enable;
            }
        }

        public void Init()
        {
            txtUnitPrice.ucKeypad = uCkeypad1;
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            mSerialPortBarcode.Received += new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received);
            mUCInfoTop = new UCInfoTop();
            mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
            mUCInfoTop.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(mUCInfoTop);
            mProcessOrder = new Class.ProcessOrderNew(conn, mPrintServer);
            mSetPrinter = new Class.Setting();
            mPrintServer.BarName = mSetPrinter.getbarprinter();
            mPrintServer.KitchenName = mSetPrinter.getkitchenprinter();
            mPrintServer.BillName = mSetPrinter.GetBillPrinter();
            LoadDepartment();
            Class.FuelDiscountConfig.GetMaxQtyDiscount();
            try
            {
                mPortPage = new SerialPort.SerialPort();
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
            lvOrder.SelectedIndexChanged += new EventHandler(lvOrder_SelectedIndexChanged);
            ChangeLocalModeLabel();
            if (conn.GetCableID() == "1")
            {
                mUCInfoTop.lbServerMode.Visible = false;
            }
            else
            {
                mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                mThreadServerClient.Start();
            }
            try
            {
                mCustomerDisplay = new Class.CustomerDisplayVFD();
            }
            catch{}
            txt = txtBarcode;

            if (mCustomerDisplay != null)
                mCustomerDisplay.DisplayWelcome();
            if (mReadConfig.IsMechanic)
            {
                ucMenuChange.mShortcut = DataObject.TypeMenu.MECHANICMENU;
                mReadConfig.IsTyreMenu = false;
            }
        }

        public void LoadOrder(string table, int isClearListViewOrder)
        {
            mOrderMain = null;
            if (mOrderMain == null)
            {
                mOrderMain = new Class.ProcessOrderNew.Order();
                mOrderMain.TableID = table;
                mOrderMain.CableID = mCableID;
                mOrderMain.StaffID = mStaffID;
                //Tan sua shiftID cua order
                mOrderMain.ShiftID = mShiftObject.ShiftID;
                //order.StaffID = mStaffID;
                try
                {
                    conn.Open();
                    mOrderMain.OrderID = Convert.ToInt32(conn.ExecuteScalar("select if(max(orderID) is null,0,max(orderID)) from ordersdaily").ToString()) + 1;
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "LoadOrder::" + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                if (table == "")
                {
                    mOrderMain.TableID = "TKA-" + mOrderMain.OrderID;
                }
            }
            LoadListView(mOrderMain, isClearListViewOrder);
        }

        public void NextCustomer()
        {
            if (frmCustomer != null)
            {
                frmCustomer.DeleteAll();
            }
            if (mOrderMain != null)
            {
                mOrderMain = mOrderMain.CoppyOrder();
            }
            if (mCustomerDisplay != null)
                mCustomerDisplay.DisplayWelcome();

            SetText("", txtItemName);
            SetText("", txtQty);
            SetText("", txtUnitPrice);
            SetText("", txtBarcode);
            SetText("", textBox1);
            SetText("", txtItemName);

            ucFuelControls.Enabled = true;
            ChangeEnable(true, txtQty);
            ChangeEnable(true, txtUnitPrice);
            btnPreOrder.Tag = null;
            intPumpID = 0;
            strIDPumpHistory = "";
            LoadOrder("", 1);
            ucFuelControls.ClearListFule();
            ucFuelControls.ChangeFuel();
        }

        public void SetStaff(int staffID, string staffName)
        {
            mStaffID = staffID;
            txtStaffID.Text = staffName;
        }

        public void SetStaffName(string name, int staffID, int per)
        {
            txtStaffID.Text = name;
            mStaffID = staffID;
            mTransit.StaffID = mStaffID;
            mStaffPermission = per;
        }

        public void SetText(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text;
            }
        }

        public void SetText2(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText2);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = "$ " + text;
            }
        }

        public void SetText3(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText3);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text + " L";
            }
        }

        private void AddListView(ProcessOrderNew.Item item)
        {
            if (lvOrder.InvokeRequired)
            {
                InfoMessageDel method = new InfoMessageDel(AddListView);
                lvOrder.Invoke(method, new object[] { item });
                return;
            }
            AddNewItems(item);
        }

        private void btnAllMenu_Click(object sender, EventArgs e)
        {
            ucMenuChange.mShortcut = DataObject.TypeMenu.ALLMENU;
            ucMenuChange.Refresh();
        }

        private void btnDepartment_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblStatus.Text = "Order Hoàn thành";
                    return;
                }
                lblStatus.Text = "Order Completed";
                return;
            }
            Button btn = (Button)sender;
            Class.Departnemt depa = (Class.Departnemt)btn.Tag;
            frmNewItems frm = new frmNewItems(lvOrder, money, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), this);
            frm.Department(depa.FullName, depa.GST);
            frm.ShowDialog();
            //txtqty.Enabled = false;
            txtUnitPrice.Enabled = false;
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog(_nameClass + "btnDiscount_Click");
            if (lvOrder.SelectedIndices.Count > 0)
            {
                ListViewItem li = lvOrder.SelectedItems[0];
                Class.ProcessOrderNew.Item item = mOrderMain.GetItemByKey(Convert.ToInt32(li.Tag));
                if (item == null)
                {
                    mLocketText = false;
                    ClearTextBox();
                    return;
                }
                if (item.IsMechanic)
                {
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (item.ParentItem != 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mOrderMain.CheckContainChildrent(item.KeyItem))
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                frmDiscount frm = new frmDiscount();
                frm.ItemName = item.ItemName;
                frm.Qty = item.Qty;
                frm.Price = item.Price;
                frm.Total = item.SubTotal;
                frm.ShiftID = mOrderMain.ShiftID;
                frm.CableID = mCableID;
                if (item.IsFuel)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (frm.Total <= 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mStaffPermission == 3)
                {
                    frm.IsManager = true;
                }
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Class.ProcessOrderNew.Item itemDiscount = new Class.ProcessOrderNew.Item(0, frm.DiscountID, frm.DiscountName, 1, -1 * frm.Discount, -1 * frm.Discount, 0, 0, 0, "0");
                    itemDiscount.IsLineDicount = true;
                    itemDiscount.ItemType = 1;
                    itemDiscount.EnableFuelDiscount = 1;
                    //itemDiscount.OptionCount = 1;
                    itemDiscount.ParentItem = item.KeyItem;
                    mOrderMain.InsertAfterKey(itemDiscount, item.KeyItem, lvOrder, money);

                    mOrderMain.CheckFuelDiscount(lvOrder, money);
                    frmCustomer.ProcessOrder(mOrderMain);
                    TotalLabel();
                    //order.AddItemToList(itemDiscount,listView1,money);
                }
            }
        }

        private void btnEndOfShift_Click(object sender, EventArgs e)
        {
            SetShiftOrder();
            if (mReadConfig.IsEnableShift)
            {
                frmShiftReport frm = new frmShiftReport(money, this, DataObject.TypeFromShift.Staff);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                    shiftrp.ShiftID = this.mShiftObject.ShiftID;
                    shiftrp = BusinessObject.BOShiftReport.GetReportShift(shiftrp);
                    double cashTotal = shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.refun - shiftrp.PayOut;
                    this.mShiftObject.CashFloatOut = shiftrp.CashFloatIn + cashTotal;

                    if (BusinessObject.BOShift.UpdateCompleted(this.mShiftObject) > 0)
                    {
                        SystemLog.LogPOS.WriteLog(_nameClass + "btnEndOfShift_Click::UpdateCompleted::");
                        lblStatus.Text = "End Shift " + this.mShiftObject.ShiftName;

                        NextShift();
                        BusinessObject.BOConfig.SetCurrentCashFloatIn(this.mShiftObject.CashFloatOut);

                        this.mShiftObject.CashFloatIn = this.mShiftObject.CashFloatOut;
                        this.mShiftObject.CashFloatOut = 0;
                        mShiftObject.bActiveShift = false;
                        mShiftObject.continueShift = false;
                        LoadShift();
                        System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
                        string file = string.Empty;
                        SystemLog.LogPOS.WriteLog("Path: " + Application.ExecutablePath);
                        SystemLog.LogPOS.WriteLog("file: " + file);
                        SystemLog.LogPOS.WriteLog("Path: " + Application.ExecutablePath);

                        System.Diagnostics.Process.Start(Application.ExecutablePath);
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                        SystemLog.LogPOS.WriteLog("End Shift");
                    }
                }
                else
                {
                    this.mShiftObject = frm.so;
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                frmMessageBox frm1 = new frmMessageBox("Thông tin", "Bạn có muốn thoát khỏi hệ thống POS?");
                frm1.ShowDialog();
                if (frm1.DialogResult == DialogResult.OK)
                {
                    BusinessObject.BOFuelHistory.UpdateAllRefreshCableIDByID(mReadConfig.iCableID);
                    Class.LogPOS.WriteLog(_nameClass + "btnExit_Click");
                    Application.Exit();
                }
                return;
            }
            frmMessageBox frm = new frmMessageBox("Information", "Do you want exit POS System?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                BusinessObject.BOFuelHistory.UpdateAllRefreshCableIDByID(mReadConfig.iCableID);
                Class.LogPOS.WriteLog(_nameClass + "btnExit_Click");
                Application.Exit();
            }
        }

        private void btnFindItem_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            frmSearchBarCode frm = new frmSearchBarCode(mSerialPortBarcode, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), DataObject.FindItemType.FindItemInOrder);
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.CHANGE_MENU;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.ItemMenu.Liter == 1)
                {
                    frmLitres frml = new frmLitres(lvOrder, money, this, frm.ItemMenu);
                    frml.StartPosition = FormStartPosition.CenterScreen;
                    frml.ShowDialog();
                }
                else
                {
                    this.AddNewItems(frm.ItemMenu);
                }
            }
            mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void btnFunction_Click(object sender, EventArgs e)
        {
            frmFuntion frm = new frmFuntion(this, money, intTotalPump, mSerialPortBarcode, axCsdEft, mDBConfig, mTransit);

            frm.ShowDialog();
            this.mShiftObject = frm.so;

            if (frm.checkMenuChange)
                ucMenuChange.Refresh();
            mUCInfoTop = new UCInfoTop();
            mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
            mUCInfoTop.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(mUCInfoTop);
            mUCInfoTop.lbCash.Text = "Cash: " + money.Format2(this.mShiftObject.CashFloatIn);
            LoadShift();
            ChangeLocalModeLabel();
            if (conn.GetCableID() == "1")
            {
                mUCInfoTop.lbServerMode.Visible = false;
            }
            else
            {
                mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                mThreadServerClient.Start();
            }
        }

        //private void btnMenuItem_Click(object sender, EventArgs e)
        //{
        //    //if (CheckPreordercompleted(mOrderMain) == true)
        //    //{
        //    //    lblStatus.Text = "Order Completed";
        //    //    return;
        //    //}
        //    //frmMenuItems frm = new frmMenuItems(lvOrder, this, mSerialPortBarcode, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory));
        //    //frm.ShowDialog();
        //}

        private void btnFuelDisc_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog(_nameClass + "btnFuelDisc_Click");
            if (lvOrder.SelectedIndices.Count > 0)
            {
                ListViewItem li = lvOrder.SelectedItems[0];
                Class.ProcessOrderNew.Item item = mOrderMain.GetItemByKey(Convert.ToInt32(li.Tag));
                if (item == null)
                {
                    mLocketText = false;
                    ClearTextBox();
                    return;
                }
                if (item.ParentItem != 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mOrderMain.CheckContainChildrent(item.KeyItem))
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (!item.IsFuel)
                {
                    mLocketText = false;
                    lblStatus.Text = "Only discount Fuel item";
                    return;
                }
                frmFuelDiscount frm = new frmFuelDiscount(conn, money);
                frm.ItemName = item.ItemName;
                frm.Qty = item.Qty;
                frm.Price = item.Price;
                frm.Total = item.SubTotal;
                frm.ShiftID = mOrderMain.ShiftID;
                frm.Weight = item.Weight;
                if (frm.Total <= 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mStaffPermission == 3)
                {
                    frm.IsManager = true;
                }
                if (Class.FuelDiscountConfig.GetListFuelDiscount() == null)
                {
                    mLocketText = false;
                    lblStatus.Text = "No discount in list";
                    return;
                }
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    ProcessOrderNew.Item itemDiscount = new ProcessOrderNew.Item(0, frm.DiscountID, frm.DiscountName, 1, -1 * frm.Discount, -1 * frm.Discount, 0, 0, 0, "0");
                    itemDiscount.ItemType = 1;
                    itemDiscount.EnableFuelDiscount = 1;
                    itemDiscount.EnableRemove = false;
                    itemDiscount.ParentItem = item.KeyItem;
                    mOrderMain.InsertAfterKey(itemDiscount, item.KeyItem, lvOrder, money);
                    mOrderMain.CheckFuelDiscount(lvOrder, money);
                    TotalLabel();
                    frmCustomer.ProcessOrder(mOrderMain);
                }
            }
        }

        private void btnNewItem_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            frmNewItems frm = new frmNewItems(lvOrder, money, this, mCableID);
            frm.ShowDialog();
        }

        private void btnPreOrder_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnPreOrder_Click");
            frmPreOrder frmPreOrder = new frmPreOrder(mReadConfig);
            frmPreOrder.listView = lvOrder;
            frmPreOrder.money = money;
            if (frmPreOrder.ShowDialog() == DialogResult.OK)
            {
                Class.ProcessOrderNew.Order lorder = frmPreOrder.Order;

                if (lorder != null)
                {
                    frmCustomer.ProcessOrder(lorder);
                    lorder.StaffID = mStaffID;
                    txtQty.Enabled = false;
                    txtUnitPrice.Enabled = false;
                    ClearTextBox();
                    mOrderMain = lorder;
                    ucFuelControls.Enabled = false;
                    txtOrderID.Text = mOrderMain.OrderID + "";
                    if (lvOrder.Items.Count > 0)
                    {
                        lvOrder_SelectedIndexChanged(lvOrder, null);
                    }
                    TotalLabel();
                }
                else
                {
                    lblStatus.Text = "There is no order!";
                }
            }
            else
            {
                lblStatus.Text = "There is no order!";
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            object oj = null;
            try
            {
                conn.Open();
                string sql = "select if(max(orderID) is null,0,max(orderID)) as max from ordersdaily";
                oj = conn.ExecuteScalar(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnPrint_Click::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            if (Convert.ToInt32(oj) != 0)
            {
                if (this.mOrderMain.OrderID - 1 == Convert.ToInt32(oj))
                {
                    Class.ProcessOrderNew.Order lastorder = mProcessOrder.GetLastOrder(money);
                    Class.LogPOS.WriteLog(_nameClass + "Print Last Order");
                    int n = 1;
                    if (lastorder.Customer.CustID != 0)
                        n = 2;
                    while (n-- > 0)
                    {
                        mProcessOrder.printTaxtInvoice(lastorder);
                    }

                }
                else
                {
                    Class.LogPOS.WriteLog(_nameClass + "Print Previous Order");
                    if (mOrderMain.CableID != 1)
                    {
                        frmReport frm = new frmReport(mOrderMain);
                    }
                    else
                    {
                        int n = 1;
                        if (mOrderMain.Customer.CustID != 0)
                            n = mReadConfig.CountTaxtInvoiceForCustomer;
                        while (n-- > 0)
                        {
                            mProcessOrder.printTaxtInvoice(mOrderMain);
                        }
                    }

                }
            }
        }

        private void btnRecallOrder_Click(object sender, EventArgs e)
        {
            Class.ProcessOrderNew.Order o = mListOrder.NextOrder();
            if (o != null)
            {
                mOrderMain = o;
                LoadListView(mOrderMain, 1);
            }
        }

        private void btnSaveOrder_Click(object sender, EventArgs e)
        {
            if (mOrderMain != null)
            {
                if (mOrderMain.Completed != 1 && mOrderMain.ListItem.Count > 0)
                {
                    mListOrder.AddOrder(mOrderMain);
                    mListOrderTemp.AddOrder(mOrderMain);
                    NextCustomer();
                }
            }
        }

        private void btnSubtotal_Click(object sender, EventArgs e)
        {
            strUserName = txtStaffID.Text;
            lbChangeAmount.Text = "";
            lbChangeName.Text = "";
            int intCountFuel = 0;
            int intIDFuelHistory = 0;
            string strIDPump = "";
            foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
            {
                Class.LogPOS.WriteLog(_nameClass + "Order Item :" + item.ItemName + "- PumpHistory :" + item.IDPumpHistory + "- PumID :" + item.PumID);
                if (item.IsFuel)
                {
                    intCountFuel++;
                    intIDFuelHistory = item.IDPumpHistory;
                    strIDPump = item.PumID;
                }
            }
            Class.LogPOS.WriteLog(_nameClass + "Order count fuel :" + intCountFuel);
            Class.LogPOS.WriteLog(_nameClass + "--------------- List ---------------");
            foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
            {
                if (!item.IsFuel)
                {
                    item.IDPumpHistory = intIDFuelHistory;
                    item.PumID = strIDPump;
                    Class.LogPOS.WriteLog(_nameClass + "order Item :" + item.ItemName + "- PumpHistory :" + item.IDPumpHistory + "- PumID :" + item.PumID);
                }
            }
            Class.LogPOS.WriteLog(_nameClass + "Button_SubToTal_Click.");
            if (mOrderMain.ListItem.Count == 0)
            {
                lblStatus.Text = "Order item empty!";
                return;
            }
            if (mOrderMain.Completed == 1)
            {
                lblStatus.Text = "Order completed!";
                return;
            }
            if (mOrderMain.ListItem.Count > 0 && mOrderMain.Completed != 1 && mOrderMain.Completed != 2)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnSubtotal_Click::Open Cash Drawer");
                DataObject.ShiftReport shift = new DataObject.ShiftReport();
                if (mShiftObject.bActiveShift == false && mShiftObject.continueShift == false)
                {
                    shift.CashFloatIn = mShiftObject.CashFloatIn;
                }
                else
                {
                    shift.ShiftID = mShiftObject.ShiftID;
                    shift = BusinessObject.BOShiftReport.GetReportShift(shift);
                }
                frmSubtotal = new frmSubTotal(frmCustomer, money, mCustomerDisplay, mSerialPortBarcode, axCsdEft, mDBConfig, shift, mReadConfig);
                frmSubtotal.Order = mOrderMain;
                frmSubtotal.Order.CustCode = "";
                DialogResult dg = frmSubtotal.ShowDialog();
                if (dg == DialogResult.OK)
                {
                    mOrderMain = frmSubtotal.Order;
                    if (mShiftObject.bActiveShift == false && mShiftObject.continueShift == false)
                    {
                        if (BusinessObject.BOShift.InsertShift(ref mShiftObject) > 0)
                        {
                            BusinessObject.BOConfig.SetCurrentShift(mShiftObject.ShiftID);
                            mOrderMain.ShiftID = mShiftObject.ShiftID;
                            mShiftObject.bActiveShift = true;
                        }
                        else
                        {
                            mOrderMain.ShiftID = 0;
                        }
                        mShiftObject.bActiveShift = true;
                    }
                    else
                    {
                        mOrderMain.ShiftID = mShiftObject.ShiftID;
                    }
                    mOrderMain.StaffID = mStaffID;
                    btnPreOrder.Tag = null;
                    mOrderMain.OrderByCardID = "0";
                    if (mOrderMain.OrderByCardID != "")
                    {
                        InsertIntoOrderByCard();
                    }

                    mProcessOrder.subTotalTackeAway(mOrderMain, frmSubtotal.strDataReceive);

                    if (mOrderMain.ChangeByCash > 0 || mOrderMain.CashOut > 0)
                    {
                        lbChangeName.Text = "Change of order " + mOrderMain.OrderID;
                        lbChangeAmount.Text = "$" + money.Format2(mOrderMain.ChangeByCash + mOrderMain.CashOut);
                    }

                    if (mOrderMain.CashOut != 0)
                    {
                        InsertIntoCashOut(mOrderMain.CashOut, mOrderMain.StaffID, mOrderMain.ShiftID);
                    }

                    try
                    {
                        string[] strPumpArrayID = mOrderMain.PumpID.Split('-');
                        for (int i = 0; i < strPumpArrayID.Length; i++)
                        {
                            ChangeColort(Color.White, ucFuelControls.flpFuel.Controls[strPumpArrayID[i]]);
                        }
                    }
                    catch
                    {
                    }
                    //Remove Enabler Transaction for Post Pay Transaction
                    // check for completed transactions and the pump state to decide what to do based on this click
                    // Update settings here if required
                    if (intCountFuel > 0)
                    {
                        intPumIDItemSelect = 0;
                        blnIsFuel = false;
                        foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
                        {
                            int idPumpHistory = Convert.ToInt32(item.IDPumpHistory);
                            blnIsFuel = item.IsFuel;
                            if (blnIsFuel)
                            {
                                //If idPumpHistory is 1 (PostPay) AND the item is Fuel Transaction Then Find the right Pump to Clear Transaction
                                if (idPumpHistory != 0)
                                {
                                    int PumpID = Convert.ToInt32(item.PumID);
                                    foreach (Pump pump in _Forecourt.Pumps)
                                    {
                                        if (pump.Number == PumpID)
                                        {
                                            Transaction trans = pump.CurrentTransaction;
                                            trans.Clear(TransactionClearTypes.Normal);
                                        }
                                    }
                                }

                                //If idPumpHistory is 2 (PrePay) AND the item is Fuel Transaction Then Find the right Pump to Authorise Transaction
                                // Check Order Complete in SubTotal form via access database record before permitting authorise the pump
                                // now authorise the pump for a prepay transaction
                                if (idPumpHistory == 0)
                                {
                                    int PumpID = Convert.ToInt32(item.PumID);
                                    foreach (Pump pump in _Forecourt.Pumps)
                                    {                                        
                                        if (pump.Number == PumpID)
                                        {                                            
                                            try
                                            {
                                                PumpAuthoriseLimits ad = new PumpAuthoriseLimits();
                                                double subTotal = item.SubTotal;
                                                double amount = money.getFortMat(subTotal) / 1000;
                                                ad.Value = Convert.ToDecimal(amount);
                                                int hoseID = 0;
                                                // specify a grade/hose if any
                                                //We need to get the type of Grade through Prepaying Dialog
                                                int gradeID = 0;
                                                foreach (Hose _hose in pump.Hoses)
                                                {
                                                    if (item.ItemName == _hose.Grade.Name)
                                                    {
                                                        hoseID = _hose.Number;
                                                        ad.Products.Add(_hose.Grade.Id);
                                                        gradeID = _hose.Grade.Id;
                                                    }
                                                }
                                                DataObject.FuelHistory fuel = new DataObject.FuelHistory(Convert.ToInt32(money.getFortMat(subTotal)), hoseID, 1, item.ItemID, pump.Number, Convert.ToInt32(item.Price), item.Qty, 0);
                                                BusinessObject.BOFuelHistory.InsertFuelHistory(fuel);
                                                int FuelHistory = BusinessObject.BOFuelHistory.GetmaxID();
                                                BusinessObject.BOFuelHistory.UpdateCompleteOrder(mOrderMain.OrderID, pump.Id, FuelHistory);                                                
                                                pump.Authorise("PrePay", gradeID.ToString(), -1, ad);
                                            }
                                            catch (EnablerException EE)
                                            {
                                                String MessageLine = String.Format("Command:{0} Result:{1} Info:{2}",
                                                                                   "Authorise", EE.ResultCode, EE.Message);
                                                MessageBox.Show(MessageLine);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    mListOrder.RemoveOrdrt(mOrderMain);
                    NextCustomer();
                    mClientMessage.AddEventMessage();
                }
                else if (dg == DialogResult.Abort)
                {
                    mOrderMain = frmSubtotal.Order;
                    mOrderMain.StaffID = mStaffID;

                    mProcessOrder.SendOrder(mOrderMain);
                    mListOrder.RemoveOrdrt(mOrderMain);
                    //PrintDriverOff();
                    mProcessOrder.printDriverOff(mOrderMain);
                    try
                    {
                        ChangeColort(Color.White, ucFuelControls.flpFuel.Controls[Convert.ToInt32(mOrderMain.PumpID) - 1]);
                    }
                    catch (Exception)
                    {
                    }
                    NextCustomer();
                    frmCustomer.DeleteAll();
                }
                else if (dg == DialogResult.Cancel)
                {
                }
            }
        }

        private void btnTyreMenu_Click(object sender, EventArgs e)
        {
            ucMenuChange.mShortcut = DataObject.TypeMenu.TYREMENU;
            ucMenuChange.Refresh();
        }

        private void btnVoidAll_Click(object sender, EventArgs e)
        {
            SetShiftOrder();
            mOrderMain.StaffID = mStaffID;
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog(_nameClass + "btnVoidAll_Click::Delete All Item");
            try
            {
                //Void Fuel items
                if (lvOrder.SelectedIndices.Count > 0)
                {
                    intPumIDItemSelect = 0;
                    blnIsFuel = false;
                    foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
                    {
                        int idPumpHistory = Convert.ToInt32(item.IDPumpHistory);
                        blnIsFuel = item.IsFuel;
                        if (blnIsFuel)
                        {
                            if (idPumpHistory != 0)
                            {
                                item.IDPumpHistory = 0;
                                int PumpID = Convert.ToInt32(item.PumID);
                                foreach (Pump pump in _Forecourt.Pumps)
                                {
                                    if (pump.Number == PumpID)
                                    {
                                        Transaction trans = pump.CurrentTransaction;
                                        trans.ReleaseLock();
                                    }
                                }
                            }
                            if (idPumpHistory == 0)
                            {
                                item.IDPumpHistory = 0;
                                int PumpID = Convert.ToInt32(item.PumID);
                                foreach (Pump pump in _Forecourt.Pumps)
                                {
                                    if (pump.Number == PumpID)
                                    {
                                        if (pump.CurrentTransaction != null && pump.CurrentTransaction.ClientActivity == "PrePay")
                                        {
                                            pump.CancelReserve();
                                        }
                                        else
                                        {
                                            Transaction trans = pump.CurrentTransaction;
                                            trans.ReleaseLock();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                mOrderMain.ClearItemToListAndListView(lvOrder);
                mOrderMain.CheckFuelDiscount(lvOrder, money);
                TotalLabel();
                ClearTextBox();
                NextCustomer();
                BusinessObject.BOFuelHistory.UpdateAllRefreshCableIDByID(mReadConfig.iCableID, mListOrder.GetIDFuel());
                mOrderMain.CableID = mCableID;
                mCustomerDisplay.ClearScreen();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnVoidAll_Click::" + ex.Message);
            }
            ucFuelControls.ClearListFule();
            ucFuelControls.ChangeFuel();
        }

        private void btnVoidItem_Click(object sender, EventArgs e)
        {
            SetShiftOrder();
            mOrderMain.StaffID = mStaffID;
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            mLocketText = true;
            Class.LogPOS.WriteLog(_nameClass + "btnVoidItem_Click");
            if (lvOrder.SelectedIndices.Count > 0)
            {
                intPumIDItemSelect = 0;
                blnIsFuel = false;
                intNumrow = 0;
                try
                {
                    foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
                    {
                        //Enabler: Release transaction for selected items
                        intPumIDItemSelect = Convert.ToInt32(item.PumID);
                        int idPumpHistory = Convert.ToInt32(item.IDPumpHistory);
                        blnIsFuel = item.IsFuel;

                        if (blnIsFuel)
                        {
                            //If idPumpHistory is 1 (PostPay) AND the item is Fuel Transaction Then Find the right Pump to ReleaseLock
                            if (idPumpHistory != 0)
                            {
                                item.IDPumpHistory = 0;
                                int PumpID = Convert.ToInt32(item.PumID);
                                foreach (Pump pump in _Forecourt.Pumps)
                                {
                                    if (pump.Number == PumpID)
                                    {
                                        Transaction trans = pump.CurrentTransaction;
                                        trans.ReleaseLock();
                                    }
                                }
                            }
                            //If idPumpHistory is 2 (PrePay) AND the imte is Fuel Transaction Then Cancel Reserve
                            if (idPumpHistory == 0)
                            {
                                item.IDPumpHistory = 0;
                                int PumpID = Convert.ToInt32(item.PumID);
                                foreach (Pump pump in _Forecourt.Pumps)
                                {
                                    if (pump.Number == PumpID)
                                    {
                                        if (pump.CurrentTransaction != null && pump.CurrentTransaction.ClientActivity == "PrePay")
                                        {
                                            pump.CancelReserve();
                                        }
                                        else
                                        {
                                            Transaction trans = pump.CurrentTransaction;
                                            trans.ReleaseLock();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (ListViewItem item in lvOrder.Items)
                    {
                        blnIsFuel = mOrderMain.ListItem[item.Index].IsFuel;
                        try
                        {
                            if (intPumIDItemSelect == Convert.ToInt32(item.SubItems[4].Text.ToString() == "" ? "0" : item.SubItems[4].Text.ToString()) && blnIsFuel == true)
                            {
                                intNumrow++;
                            }
                        }
                        catch {}
                    }
                }
                catch {}

                ClearTextBox();

                ProcessOrderNew.Item mItem = mOrderMain.GetItemByKey(Convert.ToInt32(lvOrder.SelectedItems[0].Tag));
                if (mItem.IsMechanic)
                {
                    lblStatus.Text = "Item can not void!";
                    return;
                }

                if (mItem != null && mItem.IDPumpHistory != -1)
                {
                    mOrderMain.RemoveItem(Convert.ToInt32(lvOrder.SelectedItems[0].Tag), lvOrder, money, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory));
                    //_Order.CheckLoyaltyByListItem(lvOrder, money);
                    mOrderMain.CheckFuelDiscount(lvOrder, money);
                    mItem.Qty = 0;
                    mOrderMain.CheckComboDiscount(lvOrder, money, mOrderMain.cboDisc, mOrderMain.resultDisc, mOrderMain.ListItem);
                }
                TotalLabel();
                try
                {
                    if (intPumIDItemSelect > 0)
                    {
                        if (mItem.IDPumpHistory != 0)
                            ucFuelControls.ClearListFule(mItem.IDPumpHistory);
                        else if (intNumrow == 1)
                            ucFuelControls.ChangeColorPump(intPumIDItemSelect, -1);
                        BusinessObject.BOFuelHistory.UpdateCableIDByID(mItem.IDPumpHistory, 0);
                    }
                    mCustomerDisplay.ClearScreen();
                }
                catch {}
            }
            if (lvOrder.Items.Count > 0)
                lvOrder.Items[lvOrder.Items.Count - 1].Selected = true;
            mLocketText = false;
        }

        private void buttonRefun_Click(object sender, EventArgs e)
        {
            Forms.frmRefunce frm = new frmRefunce(mOrderMain, money);
            if (frm.ShowDialog() == DialogResult.OK)
                Class.RawPrinterHelper.openCashDrawer(mSetPrinter.GetBillPrinter());
        }

        private void ChangeForceColor(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeForceColorCallbackt d = new ChangeForceColorCallbackt(ChangeForceColor);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.ForeColor = color;
            }
        }

        private void ChangeLocalModeLabel()
        {
            if (conn.CheckLocalhost())
            {
                SetText("OFFLINE", mUCInfoTop.lbServerMode);
                ChangeForceColor(Color.FromArgb(128, 255, 128), mUCInfoTop.lbServerMode);
            }
            else
            {
                SetText("ONLINE", mUCInfoTop.lbServerMode);
                ChangeForceColor(Color.Green, mUCInfoTop.lbServerMode);
            }
        }

        private bool CheckPreordercompleted(Class.ProcessOrderNew.Order order)
        {
            if (order.Completed == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ClearListView()
        {
            if (lvOrder.InvokeRequired)
            {
                ClearListview method = new ClearListview(ClearListView);
                lvOrder.Invoke(method);
                return;
            }
            lvOrder.Items.Clear();
        }

        private void ClearTextBox()
        {
            mLocketText = true;

            txtItemName.Text = "";
            txtUnitPrice.Text = "";
            txtQty.Text = "";

            mLocketText = false;
        }

        private void CountTotalPump()
        {
            try
            {
                intTotalPump = ucFuelControls.flpFuel.Controls.Count;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "CountTotalPump::" + ex.Message);
            }
        }

        private void frmOrdersAll_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                if (pr.ProcessName == "POS")
                    pr.Kill();
            }
        }

        private void frmOrdersAll_Load(object sender, EventArgs e)
        {
            //this.Hide();

            //send mail==============================
            SendMail sendMail = new SendMail();
            sendMail.CreateMessageThread();
            //=======================================

            //Forms.frmAbout frmAbout = new Forms.frmAbout(this);
            //frmAbout.Show();
            UpdateToServer.UpDatabaseToServer(conn, mReadConfig);
            //frmAbout.IsStoped = true;
            frmCustomer = new frmCustomerDisplay(money);
            frmCustomer.ShowCustomerDisplay();
            ucMenuChange.InitMenu(mDBConfig);
            SetSizeListView();

            if (mReadDBConfig.AllowSalesFuel == 1)
            {
                blnFlagIfLoading = true;
                LoadKindOfFuel();
                CountTotalPump();
                blnFlagIfLoading = false;
                NextCustomer();
                ucFuelControls.Enabled = true;
            }
            else
            {
                ucFuelControls.Enabled = false;
            }
            EnableButtonShift();
            LockButton();
        }

        private void LockButton()
        {
            LockButton(btnAllMenu, mReadConfig.IsTyreMenu);
            // LockButton(btnTyreMenu, mReadConfig.IsTyreMenu);
            LockButton(btnRecallOrder, false);
            LockButton(btnSaveOrder, false);
            // LockButton(btnTyreMenu, false);
            //LockButton(btnMenuItem, false);
            //LockButton(btnDepartment6, false);
            //LockButton(btnDepartment5, false);
            //LockButton(btnDepartment4, false);
            //LockButton(btnDepartment3, false);
            //LockButton(btnDepartment2, false);
            //LockButton(btnDepartment1, false);
            //LockButton(btnMenuItem, false);
            LockButton(btnMagTyre, false);
            LockButton(btnRecallOrder, false);
        }

        private void LockButton(Button btn, bool value)
        {
            btn.Enabled = value;
            btn.Text = value ? btn.Text : "";
            btn.BackColor = value ? btn.BackColor : Color.Gray;
        }

        private void frmOrdersAll_VisibleChanged(object sender, EventArgs e)
        {
            txtQty.Focus();
            btnPreOrder.Tag = null;
            if (this.Visible)
            {
                txtOrderID.Text = mOrderMain.OrderID + "";
                txtTableID.Text = mOrderMain.TableID + "";
                textBox1.Text = mOrderMain.NumPeople + "";
            }
            else
            {
                txtItemName.Text = "";
                txtOrderID.Text = "";
                txtQty.Text = "";
                txtTableID.Text = "";
                txtUnitPrice.Text = "";
                textBox1.Text = "";
            }
        }

        private string GetDate(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day + " " + date.ToShortTimeString();
        }

        private bool Getorder(int orderid, int tableid)
        {
            return false;
        }

        private void Init(int intNoneOfShift)
        {
            try
            {
                txtUnitPrice.ucKeypad = uCkeypad1;
                mSerialPortBarcode.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
                mSerialPortBarcode.Received += new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received);
                mUCInfoTop = new UCInfoTop();
                mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
                mUCInfoTop.Dock = DockStyle.Fill;
                panel4.Controls.Clear();
                panel4.Controls.Add(mUCInfoTop);
                mProcessOrder = new Class.ProcessOrderNew(conn, mPrintServer);
                mSetPrinter = new Class.Setting();
                mPrintServer.BarName = mSetPrinter.getbarprinter();
                mPrintServer.KitchenName = mSetPrinter.getkitchenprinter();
                mPrintServer.BillName = mSetPrinter.GetBillPrinter();
                LoadDepartment();
                FuelDiscountConfig.GetMaxQtyDiscount();
                try
                {
                    mPortPage = new SerialPort.SerialPort();
                }
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                }
                lvOrder.SelectedIndexChanged += new EventHandler(lvOrder_SelectedIndexChanged);
                ChangeLocalModeLabel();
                if (conn.GetCableID() == "1")
                {
                    mUCInfoTop.lbServerMode.Visible = false;
                }
                else
                {
                    mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                    mThreadServerClient.Start();
                }
                try
                {
                    mCustomerDisplay = new Class.CustomerDisplayVFD();
                }
                catch {}
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "Init::" + ex.Message);
            }
        }

        private void InsertIntoCashOut(double dblCashOut, int intStaffID, int intShiftID)
        {
            try
            {
                conn.Open();
                dblSubtotalCashOut = dblCashOut;
                conn.ExecuteNonQuery("insert into cashinandcashout(TotalAmount,EmployeeID,CashType,ShiftID,Description) values(" + dblCashOut + "," + intStaffID + ",5," + intShiftID + ",'Cash out subtotal')");
                conn.Close();
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocument_PrintPage);
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }

            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "InsertIntoCashOut::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void InsertIntoOrderByCard()
        {
            try
            {
                conn.Open();
                conn.BeginTransaction();
                //===================================
                string strOrderByCard = "";
                //=================================
                foreach (OrderByCard c in mOrderMain.MoreOrderByCard)
                {
                    strOrderByCard = c.orderbycardID;
                    conn.ExecuteNonQuery(
                        "insert into orderbycard(orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,surcharge) " +
                        "values(" + c.orderbycardID + "," + c.orderID + "," + c.cardID + "," + money.getFortMat(c.subtotal) + "," + money.getFortMat(c.cashOut) + "," + mOrderMain.ShiftID + ",'" + c.AuthCode + "','" + c.AccountType + "','" + c.Pan + "','" + c.DateExpiry + "','" + c.Date + "','" + c.Time + "','" + c.CardType + "'," + money.getFortMat(c.AmtPurchase.ToString()) + "," + money.getFortMat(c.AmtCash.ToString()) + "," + money.getFortMat(c.surcharge.ToString()) + ")"
                    );
                    int id = Convert.ToInt32(conn.ExecuteScalar("select LAST_INSERT_ID()"));
                    conn.ExecuteScalar("update orderbycard set orderbycardID=" + id + " where ID=" + id);
                    mOrderMain.OrderByCardID = id + "";
                }

                conn.Commit();
            }
            catch (Exception ex)
            {
                conn.Rollback();
                Class.LogPOS.WriteLog(_nameClass + "InsertIntoOrderByCard::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void lvOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetShiftOrder();
            try
            {
                if (mLocketText)
                {
                    return;
                }
                mLocketText = true;
                if (lvOrder.SelectedIndices.Count > 0)
                {
                    Class.ProcessOrderNew.Item item = mOrderMain.GetItemByKey(Convert.ToInt32(lvOrder.SelectedItems[0].Tag));
                    if (item != null)
                    {
                        txtItemName.Text = item.ItemName;
                        txtQty.Text = String.Format("{0:#,###}",item.Qty);
                        //txtunitprice.Text = money.Convert3to2(Convert.ToDouble(money.Format(item.Price))).ToString();
                        txtUnitPrice.Text = money.Format(item.Price);
                        txtBarcode.Text = item.BarCode;
                        if (item.IsFuel)
                        {
                            txtQty.Enabled = false;
                            intPumpID = Convert.ToInt32(item.PumID);
                            //txtunitprice.Enabled = false;
                        }
                        else
                        {
                            txtQty.Enabled = true;
                            //txtunitprice.Enabled = true;
                        }
                    }
                }
                mLocketText = false;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "lvOrder_SelectedIndexChanged::" + ex.Message);
            }
        }

        private void LoadDepartment()
        {
            Class.ConfigDepartment configDe = new Class.ConfigDepartment();
            btnDepartment1.Text = configDe.Department1.SortName;
            btnDepartment1.Tag = configDe.Department1;

            btnDepartment2.Text = configDe.Department2.SortName;
            btnDepartment2.Tag = configDe.Department2;

            btnDepartment3.Text = configDe.Department3.SortName;
            btnDepartment3.Tag = configDe.Department3;

            btnDepartment4.Text = configDe.Department4.SortName;
            btnDepartment4.Tag = configDe.Department4;

            btnDepartment5.Text = configDe.Department5.SortName;
            btnDepartment5.Tag = configDe.Department5;

            btnDepartment6.Text = configDe.Department6.SortName;
            btnDepartment6.Tag = configDe.Department6;

            btnDepartment7.Text = configDe.Department7.SortName;
            btnDepartment7.Tag = configDe.Department7;
        }

        private void LoadListView(Class.ProcessOrderNew.Order lorder, int isClearListViewOrder)
        {
            ClearListView();
            mLocketText = true;
            lvOrder.Items.Clear();
            if (frmCustomer != null)
            {
                frmCustomer.ProcessOrder(mOrderMain);
            }
            txtOrderID.Text = mOrderMain.OrderID + "";
            if (mOrderMain.TableID.Contains("TKA-"))
            {
                txtTableID.Text = "";
            }
            else
            {
                txtTableID.Text = mOrderMain.TableID + "";
            }
            for (int i = 0; i < lorder.ListItem.Count; i++)
            {
                Class.ProcessOrderNew.Item item = lorder.ListItem[i];
                string qty = item.Qty + "";
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    qty = money.FormatCurenMoney((double)item.Qty / item.Weight);
                }
                ListViewItem li = new ListViewItem(qty);
                li.SubItems.Add(item.ItemName);
                li.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(item.SubTotal))).ToString());
                li.SubItems.Add(item.PumID.ToString());
                li.SubItems.Add(item.IDPumpHistory.ToString());
                li.Tag = item.KeyItem;
                li.EnsureVisible();
                li.Selected = true;
                lvOrder.Items.Add(li);
            }
            TotalLabel();
            mLocketText = false;
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPortBarcode.TypeOfBarcode == Barcode.SerialPort.ORDER_ALL)
            {
                mSerialPortBarcode.SetText("", txt);
                mSerialPortBarcode.SetText(data, txt);
            }
        }

        private bool PingToServer()
        {
            return true;
        }

        private void RemoveLoyaltyItem(int listViewIndex)
        {
            if (listViewIndex < lvOrder.Items.Count)
            {
                ItemClick item = (ItemClick)lvOrder.Items[listViewIndex].Tag;
                if (item.ItemIndex == -1 && item.ItemOptionIndex == -1)
                {
                    lvOrder.Items.RemoveAt(listViewIndex);
                    RemoveLoyaltyItem(listViewIndex);
                }
            }
        }

        private void SendToCustomerDisplay(Class.ProcessOrderNew.Item item)
        {
            try
            {
                mCustomerDisplay.ClearScreen();
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    mCustomerDisplay.PrintLine1(item.ItemName + " $" + money.Format2(item.SubTotal));
                    mCustomerDisplay.PrintLine2((double)item.Qty / item.Weight + "L @ $" + money.Format(item.Price) + "PL");
                }
                else
                {
                    if (item.Qty > 1)
                    {
                        mCustomerDisplay.PrintLine1(item.ItemName + " $" + money.Format2(item.SubTotal));
                        mCustomerDisplay.PrintLine2(item.Qty + " @ $" + money.Format2(item.Price) + "E");
                    }
                    else
                    {
                        mCustomerDisplay.PrintLine1(item.ItemName);
                        mCustomerDisplay.PrintLine2("$" + money.Format2(item.SubTotal));
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "SendToCustomerDisplay::" + ex.Message);
            }
        }

        private void SetSizeListView()
        {
            int Width = lvOrder.Width;
            lvOrder.Columns[0].Width = Width * 20 / 100;
            lvOrder.Columns[1].Width = Width * 50 / 100;
            lvOrder.Columns[2].Width = Width * 25 / 100;
        }

        private void ShowCustomerDisplay()
        {
            try
            {
                frmCustomer = new frmCustomerDisplay(money);
                Screen[] sc;
                sc = Screen.AllScreens;
                if (sc.Length >= 2)
                {
                    //get all the screen width and heights

                    //f.FormBorderStyle = FormBorderStyle.None;
                    frmCustomer.Left = sc[1].Bounds.Width;
                    frmCustomer.Top = sc[1].Bounds.Height;
                    frmCustomer.StartPosition = FormStartPosition.Manual;
                    frmCustomer.Location = sc[1].Bounds.Location;
                    Point p = new Point(sc[1].Bounds.Location.X, sc[1].Bounds.Location.Y);
                    frmCustomer.Location = p;
                    frmCustomer.WindowState = FormWindowState.Maximized;
                    frmCustomer.Show();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "ShowCustomerDisplay::" + ex.Message);
            }
        }

        private void ThreadServerClient()
        {
            while (mKeyThreadServerClient)
            {
                if (!PingToServer())
                {
                    if (!PingToServer())
                    {
                        if (!PingToServer())
                        {
                            conn.ChangeLocalConnection(1);
                            conn.SetConnectionString();
                            ChangeLocalModeLabel();
                            ChangeEnable(false, btnFuelDisc);
                            ChangeEnable(false, buttonRefun);
                            //ChangeEnable(false, btnTyreMenu);
                            mKeyThreadServerClient = false;
                        }
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int second = 5;
            if (lblStatus.POSTimeMessenge >= 1 && lblStatus.POSTimeMessenge <= second)
            {
                lblStatus.POSTimeMessenge++;
            }

            if (lblStatus.POSTimeMessenge == (second + 1))
            {
                lblStatus.Text = "";
                lblStatus.POSTimeMessenge = 0;
            }
            if (mReadConfig.IsEnableShift == true)
            {
                if (DateTime.Now.Hour == mReadConfig.iHourEndShift && DateTime.Now.Minute == mReadConfig.iMinuteEndShift)
                {
                    CheckShift();
                }
            }
            //Ham kill POS
            //KillPOS();
            AutoResetOrderID();
        }

        private void TotalLabel()
        {
            double thanhtoan = 0;
            foreach(ProcessOrderNew.Item item in mOrderMain.ListItem){
                thanhtoan += item.SubTotal;
            }
            SetText("$" + money.Convert3to2(Convert.ToDouble(money.Format(thanhtoan))), lblTotal);
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if (mLocketText)
            {
                return;
            }
            if (txtBarcode.Text != "")
            {
                try
                {
                    Connection.Connection con = new Connection.Connection();
                    DataTable tbl = con.Select("select * from itemsmenu where deleted<>1 and scanCode='" + txtBarcode.Text + "' limit 0,1");
                    if (tbl.Rows.Count > 0)
                    {
                        Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, Convert.ToInt16(tbl.Rows[0]["itemID"]), tbl.Rows[0]["itemDesc"].ToString(), 1, Convert.ToDouble(tbl.Rows[0]["unitPrice"]), Convert.ToDouble(tbl.Rows[0]["unitPrice"]), Convert.ToDouble(tbl.Rows[0]["gst"]), 0, 0, "0");
                        item.BarCode = txtBarcode.Text;
                        item.EnableFuelDiscount = Convert.ToInt16(tbl.Rows[0]["enableFuelDiscount"]);
                        this.AddNewItems(item);
                    }
                    else
                    {
                        ClearTextBox();
                        lblStatus.Text = "This item not found !";
                    }
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "txtBarcode_TextChanged::" + ex.Message);
                }
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            mLocketText = false;
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (e.KeyChar == (char)46)
            {
                lblStatus.Text = "Invalid Qty";
                e.Handled = true;
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (mLocketText)
            {
                return;
            }
            if (lvOrder.SelectedItems.Count == 0)
            {
                return;
            }
            Class.ProcessOrderNew.Item item = mOrderMain.GetItemByKey(Convert.ToInt32(lvOrder.SelectedItems[0].Tag));
            if (!item.IsMechanic)
            {
                mLocketText = true;
                if (lvOrder.SelectedIndices.Count > 0 && txtQty.Text != "")
                {

                    if (item != null && item.IDPumpHistory != -1)
                    {
                        if (item.ParentItem != 0)
                        {
                            mLocketText = false;
                            lblStatus.Text = "Item can not discount";
                            return;
                        }
                        item.Qty = Convert.ToInt32(txtQty.Text);
                        item.SubTotal = item.Qty * item.Price;

                        mOrderMain.EditItem(item, lvOrder, money);

                        SendToCustomerDisplay(item);
                        TotalLabel();
                        mOrderMain.CheckComboDiscount(lvOrder, money, mOrderMain.cboDisc, mOrderMain.resultDisc, mOrderMain.ListItem);
                    }

                }
                if (lvOrder.SelectedIndices.Count > 0 && (txtQty.Text == "" || Convert.ToInt32(txtQty.Text) == 0))
                {

                    if (item != null && item.IDPumpHistory != -1)
                    {
                        int qtyItemOld = item.Qty;
                        item.Qty = 1;
                        item.SubTotal = item.Qty * item.Price;
                        mOrderMain.EditItem(item, lvOrder, money);
                        SendToCustomerDisplay(item);
                        mOrderMain.CheckComboDiscount(lvOrder, money, mOrderMain.cboDisc, mOrderMain.resultDisc, mOrderMain.ListItem);
                    }
                }
                mOrderMain.CheckFuelDiscount(lvOrder, money);
                TotalLabel();
                mLocketText = false;
                frmCustomer.ProcessOrder(mOrderMain);
            }
            else
            {
                lblStatus.Text = "Item can not change qty";
            }
        }

        private void txttableid_TextChanged(object sender, EventArgs e)
        {
            if (txtTableID.Text.Contains("TKA-"))
            {
                txtTableID.Text = "";
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            /*
            if (txtUnitPrice.Text != "" && txtUnitPrice.Text != "-")
            {
                double price = Convert.ToDouble(txtUnitPrice.Text);
                if (price > 1000)
                {
                    //lblStatus.Text = "Unit Price not more than 1000";
                    txtUnitPrice.Text = "0";
                    return;
                }
            }*/
            if (mLocketText)
            {
                return;
            }
            //ChangeQtyAndPrice();
            try
            {
                ItemClick item = (ItemClick)lvOrder.SelectedItems[0].Tag;
                if (item.ItemOptionIndex == -1)
                {
                    //order.ListItem[item.ItemIndex].Qty = Convert.ToInt16(txtqty.Text);
                    mOrderMain.ListItem[item.ItemIndex].Price = money.getFortMat(txtUnitPrice.Text);
                    mOrderMain.ListItem[item.ItemIndex].SubTotal = mOrderMain.ListItem[item.ItemIndex].Qty * mOrderMain.ListItem[item.ItemIndex].Price;
                    lvOrder.Items[lvOrder.SelectedIndices[0]].SubItems[0].Text = txtQty.Text;
                    lvOrder.Items[lvOrder.SelectedIndices[0]].SubItems[2].Text = money.Format(mOrderMain.ListItem[item.ItemIndex].SubTotal);
                }
                else
                {
                    //order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Qty = Convert.ToInt16(txtqty.Text);
                    mOrderMain.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Price = money.getFortMat(txtUnitPrice.Text);
                    mOrderMain.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].SubTotal = mOrderMain.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Qty * mOrderMain.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Price;
                    lvOrder.Items[lvOrder.SelectedIndices[0]].SubItems[0].Text = txtQty.Text;
                    lvOrder.Items[lvOrder.SelectedIndices[0]].SubItems[2].Text = money.Format(mOrderMain.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].SubTotal);
                }
                //LoadListView(order);
                //TotalLabel();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "txtUnitPrice_TextChanged::" + ex.Message);
            }
        }

        private void ucMenuChange_Click(object sender, EventArgs e)
        {
            try
            {
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), Convert.ToInt32(ucMenuChange._ItemsMenu.ItemID), ucMenuChange._ItemsMenu.ItemShort, 1, ucMenuChange._ItemsMenu.UnitPrice, ucMenuChange._ItemsMenu.UnitPrice, ucMenuChange._ItemsMenu.Gst, 0, 0, intPumpID.ToString());
                if (ucMenuChange._ItemsMenu != null)
                {
                    if (ucMenuChange._ItemsMenu.SellSize == 1000)
                    {
                        frmLitres frml = new frmLitres(lvOrder, money, this, item);
                        frml.StartPosition = FormStartPosition.CenterScreen;
                        frml.ShowDialog();
                    }
                    else
                    {
                        item.BarCode = ucMenuChange._ItemsMenu.Barcode;
                        item.ItemType = 0;
                        AddNewItems(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmOrderAll::ucMenuChange_Click" + ex.Message);
            }
        }

        #region Shift

        public bool EnableCheckShift = true;

        public void LoadShift()
        {
            if (mReadConfig.IsEnableShift)
            {
                mUCInfoTop.lblShift.Text = "Shift: " + mShiftObject.ShiftName;
                mUCInfoTop.lblShift.Visible = true;
                mUCInfoTop.lbCash.Visible = true;
            }
        }

        private void CheckShift()
        {
            if (mReadConfig.IsEnableShift)
            {
                if (mShiftObject == null)
                {
                    mShiftObject = new DataObject.ShiftObject();
                    mShiftObject.StaffID = mTransit.StaffID;
                    mShiftObject.CableID = mTransit.CableID;
                    return;
                }
                if (BusinessObject.BOShift.GetListShift(mTransit.CableID).Count == 0)
                {
                    mShiftObject.ShiftName = 1;
                    mShiftObject.bActiveShift = false;
                    mShiftObject.CashFloatIn = 0;
                    mUCInfoTop.lbCash.Text = "Cash: " + money.Format2(mShiftObject.CashFloatIn);
                    mUCInfoTop.lbCash.Visible = true;
                }
                else
                {
                    if (mShiftObject != null)
                    {
                        if (mShiftObject.continueShift == true)
                        {
                            return;
                        }
                    }
                    mShiftObject.ShiftID = BusinessObject.BOConfig.GetCurrentShift(mDBConfig);
                    DataObject.ShiftObject shift = BusinessObject.BOShift.GetShiftMaxWithShiftID(mShiftObject);
                    if (shift.Completed == (int)DataObject.CompletedShift.NONE) // Shift truoc do chua completed
                    {
                        mShiftObject.CashFloatIn = BusinessObject.BOShift.GetShiftMaxWithShiftID(shift).CashFloatIn; //BusinessObject.BOConfig.GetCurrentCashFloatIn(mDBConfig);
                        mShiftObject.CashFloatIn = shift.CashFloatIn;
                        mUCInfoTop.lbCash.Text = "Cash: " + money.Format2(mShiftObject.CashFloatIn);
                        mUCInfoTop.lbCash.Visible = true;

                        #region Shift Current None Completed

                        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, mReadConfig.iHourEndShift, mReadConfig.iMinuteEndShift, mReadConfig.iSecondEndShift);

                        if (shift.datetime > now) // shift bat dau sau thoi diem quy dinh
                        {
                            mShiftObject.copyFromAnother(shift);
                            mShiftObject.bActiveShift = true;
                        }
                        else
                        {
                            if (now < DateTime.Now)
                            {
                                if (mReadConfig.IsAutoShift) // tu dong danh lai ma
                                {
                                    DataObject.ShiftReport shiftrp = new DataObject.ShiftReport();
                                    shiftrp.ShiftID = this.mShiftObject.ShiftID;
                                    shiftrp = BusinessObject.BOShiftReport.GetReportShift(shiftrp);
                                    double cashTotal = shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.refun;
                                    this.mShiftObject.CashFloatOut = shiftrp.CashFloatIn + cashTotal;
                                    if (mReadConfig.IsAutoSafeDropShift == true)
                                    {
                                        if (this.mShiftObject.CashFloatOut > mReadConfig.MoneyMinAfterSafeDropAuto)
                                        {
                                            DataObject.CashInAndCashOut cico = new DataObject.CashInAndCashOut();
                                            cico.CashType = DataObject.TypeCashInAndCashOut.SafeDropAuto;
                                            cico.Description = "Safe Drop Auto Shift " + this.mShiftObject.ShiftID;
                                            cico.Employee = 0;
                                            cico.TotalAmount = this.mShiftObject.CashFloatOut - mReadConfig.MoneyMinAfterSafeDropAuto;
                                            cico.ShiftID = this.mShiftObject.ShiftID;
                                            if (BusinessObject.BOCashInAndCashOut.InsertSafeDropAuto(cico) > 0)
                                            {
                                                this.mShiftObject.CashFloatOut = mReadConfig.MoneyMinAfterSafeDropAuto;
                                            }
                                        }
                                    }

                                    if (BusinessObject.BOShift.UpdateCompleted(this.mShiftObject) > 0)
                                    {
                                        BusinessObject.BOConfig.SetCurrentShift(this.mShiftObject.ShiftID);
                                        BusinessObject.BOConfig.SetCurrentCashFloatIn(BusinessObject.BOShift.GetShiftMaxWithShiftID(this.mShiftObject).CashFloatOut);

                                        this.mShiftObject.CashFloatIn = this.mShiftObject.CashFloatOut;
                                        this.mUCInfoTop.lbCash.Text = "Cash: " + money.Format2(this.mShiftObject.CashFloatIn);
                                        this.mShiftObject.CashFloatOut = 0;
                                        mShiftObject.ShiftName = 1;
                                        mShiftObject.bActiveShift = false;
                                        LoadShift();
                                    }
                                }
                                else
                                {
                                    frmShiftReport frm = new frmShiftReport(money, this, DataObject.TypeFromShift.Staff, true);
                                    if (EnableCheckShift == true)
                                    {
                                        if (frm.ShowDialog() == DialogResult.OK)
                                        {
                                            if (BusinessObject.BOShift.UpdateCompleted(this.mShiftObject) > 0)
                                            {
                                                lblStatus.Text = "End Shift ";// +this.mshiftobject.ShiftName;
                                                DataObject.ShiftReport ShiftReport = new DataObject.ShiftReport();
                                                ShiftReport.ShiftID = this.mShiftObject.ShiftID;
                                                ShiftReport = BusinessObject.BOShiftReport.GetReportShift(ShiftReport);
                                                double cashTotal = ShiftReport.CashIn + ShiftReport.cash + ShiftReport.cashAccount - ShiftReport.ChangeSales - ShiftReport.CashOut - ShiftReport.SafeDrop;
                                                this.mShiftObject.CashFloatOut = this.mShiftObject.CashFloatIn + cashTotal;

                                                if (BusinessObject.BOShift.UpdateCompleted(this.mShiftObject) > 0)
                                                {
                                                    BusinessObject.BOConfig.SetCurrentShift(this.mShiftObject.ShiftID);
                                                    BusinessObject.BOConfig.SetCurrentCashFloatIn(BusinessObject.BOShift.GetShiftMaxWithShiftID(this.mShiftObject).CashFloatOut);
                                                    this.mShiftObject.CashFloatIn = this.mShiftObject.CashFloatOut;
                                                    this.mShiftObject.CashFloatOut = 0;
                                                }

                                                mShiftObject.ShiftName = 1;
                                                mShiftObject.bActiveShift = false;
                                                mShiftObject.continueShift = false;
                                                mShiftObject.ShiftID = 0;
                                                LoadShift();
                                            }
                                        }
                                        else
                                        {
                                            EnableCheckShift = true;
                                            mShiftObject.copyFromAnother(shift);
                                            mShiftObject.continueShift = true;
                                        }
                                    }
                                }
                            }
                        }

                        #endregion Shift Current None Completed
                    }
                    else
                    {
                        mShiftObject.copyFromAnother(shift);
                        SystemLog.LogPOS.WriteLog(_nameClass + "ShiftName::" + shift.ShiftName.ToString());
                        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, mReadConfig.iHourEndShift, mReadConfig.iMinuteEndShift, mReadConfig.iSecondEndShift);
                        shift.CashFloatOut = BusinessObject.BOShift.GetShiftMaxWithShiftID(shift).CashFloatOut;
                        mShiftObject.CashFloatIn = shift.CashFloatOut;
                        mUCInfoTop.lbCash.Text = "Cash: " + money.Format2(shift.CashFloatOut);
                        mUCInfoTop.lbCash.Visible = true;

                        if (shift.datetime > now)
                        {
                            NextShift();
                        }
                        else
                        {
                            if (now > DateTime.Now)
                            {
                                NextShift();
                            }
                            else
                            {
                                mShiftObject.ShiftName = 1;
                                mShiftObject.bActiveShift = false;
                                mShiftObject.continueShift = false;
                                mShiftObject.ShiftID = 0;
                                LoadShift();
                            }
                        }
                    }
                }
            }
        }

        private void EnableButtonShift()
        {
            if (mReadConfig.IsEnableShift)
            {
            }
            else
            {
                btnEndOfShift.Text = "";
            }
        }

        private void InitShift()
        {
            if (mReadConfig.IsEnableShift)
            {
                mShiftObject.ShiftName = 1;
                mShiftObject.StaffID = mTransit.StaffID;
                mShiftObject.CableID = mTransit.CableID;
                mShiftObject.datetime = DateTime.Now;
                if (BusinessObject.BOShift.InsertShift(ref mShiftObject) > 0)
                {
                    BusinessObject.BOConfig.SetCurrentShift(mShiftObject.ShiftID);
                    mShiftObject.bActiveShift = true;
                }
                else
                {
                    SystemLog.LogPOS.WriteLog(_nameClass + "InitShift");
                    lblStatus.Text = "Problem in shift";
                }
            }
        }

        private void LoadCash()
        {
            DataObject.ShiftReport ShiftReport = new DataObject.ShiftReport();
            ShiftReport.ShiftID = BusinessObject.BOConfig.GetCurrentShift(mDBConfig);//this.mshiftobject.ShiftID;
            ShiftReport = BusinessObject.BOShiftReport.GetReportShift(ShiftReport);
            double cashTotal = ShiftReport.CashIn + ShiftReport.cash + ShiftReport.cashAccount - ShiftReport.ChangeSales - ShiftReport.CashOut - ShiftReport.SafeDrop;
            ShiftReport.CashFloatOut = ShiftReport.CashFloatIn + cashTotal;
            BusinessObject.BOConfig.SetCurrentCashFloatIn(ShiftReport.CashFloatOut);
            this.mShiftObject.CashFloatIn = ShiftReport.CashFloatOut;
            this.mShiftObject.CashFloatOut = 0;
            LoadShift();
        }

        private void NextShift()
        {
            mShiftObject.ShiftName++;
            mShiftObject.continueShift = false;
            mShiftObject.bActiveShift = false;
            mShiftObject.ShiftID = 0;
        }

        private void SetShiftOrder()
        {
            if (mShiftObject != null)
            {
                if (mShiftObject.bActiveShift == false && mShiftObject.continueShift == false) // chua chap nhan shift
                {
                    if (BusinessObject.BOShift.InsertShift(ref mShiftObject) > 0)
                    {
                        BusinessObject.BOConfig.SetCurrentShift(mShiftObject.ShiftID);
                        mOrderMain.ShiftID = mShiftObject.ShiftID;
                        mShiftObject.bActiveShift = true;
                    }
                    else
                    {
                        mOrderMain.ShiftID = 0;
                    }
                    mShiftObject.bActiveShift = true;
                }
                else
                {
                    mOrderMain.ShiftID = mShiftObject.ShiftID;
                }
            }
            else
            {
                mOrderMain.ShiftID = 0;
            }
        }

        #endregion Shift

        #region FuelAutoProtocol


        private List<int> GetIDinListview()
        {
            List<int> lsArray = new List<int>();
            foreach (ProcessOrderNew.Item item in mOrderMain.ListItem)
            {
                lsArray.Add(Convert.ToInt32(item.PumID));
            }
            return lsArray;
        }

        private void LoadKindOfFuel()
        {
            try
            {
                conn.Open();
                string strQuery = "SELECT f.KindID , f.MenuItemID , i.unitPrice,f.PumpHeadID FROM fuel f inner join itemsmenu i on f.MenuItemID = i.itemID";
                dtSourceKindOfFuel = new DataTable();
                dtSourceKindOfFuel = conn.Select(strQuery);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadKindOfFuel::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public char CheckSum(string strReceiveData)
        {
            Int32 LRC = 0;
            int intLth = strReceiveData.Length;
            for (int i = 0; i < intLth; i++)
            {
                Int32 dec = Convert.ToInt32(DecimalFromChar((Char)strReceiveData[i]));
                LRC = LRC ^ dec;
            }
            return DecimalToChar(Convert.ToDecimal(LRC));
        }

        private Char DecimalToChar(decimal argument)
        {
            Char CharValue;
            CharValue = (Char)argument;
            return CharValue;
        }

        public string ConvertToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public Decimal DecimalFromChar(char argument)
        {
            Decimal decValue;
            decValue = argument;
            return decValue;
        }

        #endregion FuelAutoProtocol

        #region ClientMessage

        private void InitClientMessage()
        {
            Class.LogPOS.WriteLog(_nameClass + "InitClientMessage");
            mClientMessage = new ClientMessage(mReadDBConfig.Server, 12342, mReadDBConfig.CableID);
            mClientMessage.MesageChange += new ClientMessage.ClientMessageEvenHandler(mClientMessage_MesageChange);
        }

        private void mClientMessage_MesageChange(object obj, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "mClientMessage_MesageChange");
            ucFuelControls.ChangeFuel();
        }

        #endregion ClientMessage

        #region Print

        private void PrintCreditNote()
        {
            try
            {
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageCreditNote);
                mSetting = new Class.Setting();
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "PrintCreditNote::" + ex.Message);
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = mReadDBConfig.Header1.ToString();
            string bankCode = mReadDBConfig.Header2.ToString();
            string address = mReadDBConfig.Header3.ToString();
            string tell = mReadDBConfig.Header4.ToString();
            string address1 = mReadDBConfig.Header5.ToString();
            string terminal = mReadDBConfig.CableID.ToString();
            string website = mReadDBConfig.FootNode1.ToString();
            string thankyou = mReadDBConfig.FootNode2.ToString();
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += 100;

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;
            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            mPrinter.DrawString("ORDER# " + txtOrderID.Text, e, font11, l_y, 1);
            l_y += 100;
            mPrinter.DrawString("TERMINAL# " + terminal, e, font11, l_y, 1);
            l_y += 100;
            mPrinter.DrawString("OPERATOR# " + strUserName, e, font11, l_y, 1);
            //l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y = mPrinter.DrawString("SHIFT# " + mShiftObject.ShiftName, e, font11, l_y, 3);
            l_y += 100;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += 100;
            l_y += 100;
            l_y = mPrinter.DrawString("Cash Out ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            l_y += 20;
            mPrinter.DrawString("Cash out:", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, new Font("Arial", 10), l_y, 3);
            l_y += 100;

            mPrinter.DrawString("Card:", e, font11, l_y, 1);
            l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, font11, l_y, 3);

            foreach (OrderByCard c in mOrderMain.MoreOrderByCard)
            {
                if (c.subtotal > 0)
                {
                    mPrinter.DrawString("  * " + c.nameCard + ":", e, font11, l_y, 1);
                    l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, font11, l_y, 3);
                }
            }

            //mPrinter.DrawString("Cash out:", e, new Font("Arial", 10), l_y, 1);
            //l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, new Font("Arial", 10), l_y, 3);
            //l_y += 100;
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
            l_y += 200;
            l_y = mPrinter.DrawString("Items with * is no GST", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 50;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += 100;
            l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
        }

        private void printDocument_PrintPageCreditNote(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string strQuery = "SELECT balance,creditNoteCode FROM creditnote where creditNoteCode = '" + frmSubTotal.strCreditCode + "'";
            DataTable dtSource = conn.Select(strQuery);

            float y = 0;
            y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            DateTime dateTime = DateTime.Now;
            y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);

            y += 100;

            y = mPrinter.DrawString("CREDIT NOTE", e, new Font("Arial", 18, FontStyle.Bold), y, 2);

            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            y = mPrinter.DrawString("Code # " + dtSource.Rows[0]["creditNoteCode"].ToString(), e, new Font("Arial", 11), y, 1);
            y = mPrinter.DrawString("Amount : $" + money.Format(Convert.ToDouble(dtSource.Rows[0]["balance"])), e, new Font("Arial", 11), y, 1);
        }

        private void printDocument_PrintPageDriverOff(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float y = 0;
            y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            DateTime dateTime = DateTime.Now;
            y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);

            y += 100;

            y = mPrinter.DrawString("DRIVE OFF", e, new Font("Arial", 18, FontStyle.Bold), y, 2);
        }

        private void PrintDriverOff()
        {
            try
            {
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDriverOff);
                mSetting = new Class.Setting();
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "PrintDriverOff::" + ex.Message);
            }
        }

        #endregion Print

        private void btnMagType_Click(object sender, EventArgs e)
        {
            MagType.frmMagType frm = new MagType.frmMagType(mSerialPortBarcode);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                AddNewItemFromItemMenu(frm.ItemMenuMag);
                AddNewItemFromItemMenu(frm.ItemMenuType);
            }
        }

        private void AddNewItemFromItemMenu(DataObject.ItemsMenu ItemsMenu)
        {
            if (ItemsMenu != null)
            {
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), Convert.ToInt32(ItemsMenu.ItemID), ItemsMenu.ItemShort, 1, ItemsMenu.UnitPrice, ItemsMenu.UnitPrice, ItemsMenu.Gst, 0, 0, intPumpID.ToString());
                item.BarCode = ItemsMenu.Barcode;
                item.ItemType = 0;
                AddNewItems(item);
            }
        }

        private void btnMechanic_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(mOrderMain) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (!mOrderMain.IsMechanic)
            {

                Mechanic.frmMechanic frm = new Mechanic.frmMechanic(mProcessOrder);
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (frm.Order != null)
                    {
                        for (int i = 0; i < frm.Order.ListItem.Count; i++)
                        {
                            Class.ProcessOrderNew.Item item = frm.Order.ListItem[i];
                            item.IsMechanic = true;
                            mOrderMain.AddItem(item, lvOrder, money, true);
                        }
                        TotalLabel();
                    }
                    mOrderMain.Rego = frm.Order.Rego;
                    mOrderMain.Customer = frm.Order.Customer;
                    mOrderMain.RegoID = frm.Order.RegoID;
                    mOrderMain.SlotID = frm.Order.SlotID;
                    mOrderMain.CableID = frm.Order.CableID;
                }
            }
            else
            {
                lblStatus.Text = "Choose one slot!";
            }
        }

        /// <summary>
        /// Called when a pump control is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void pumpControl_OnSelected(object sender, EventArgs e)
        {
            _CurrentSelectedPump = ((PumpControl)sender).Pump;

            // check for completed transactions and the pump state to decide what to do based on this click
            Transaction trans = _CurrentSelectedPump.CurrentTransaction;

            try
            {
                if (trans != null && _CurrentSelectedPump.TransactionStack.Count > 0)
                {
                    // we have a current transaction and a stacked one - display the transaction list
                    //ShowTransactionList(_CurrentSelectedPump);
                }
                else
                {
                    if (trans != null)
                    {

                        switch (trans.HistoryData.CompletionReason)
                        {
                            case CompletionReason.Normal:
                            case CompletionReason.StoppedByClient:
                            case CompletionReason.StoppedByLimit:
                            case CompletionReason.StoppedByError:
                                // we have a completed transaction; add it to a sale
                                //_CurrentSale.AddFuelTransaction(trans);
                                break;


                            default:
                            case CompletionReason.Zero:
                                //ShowTransactionList(_CurrentSelectedPump);
                                break;

                            // Ignore 
                            case CompletionReason.NotComplete:
                                break;
                        }
                    }
                    else
                    {
                        // decide what to do based on the pump state
                        switch (_CurrentSelectedPump.State)
                        {
                            case PumpState.Locked:
                                //if (_CurrentSelectedPump.TransactionStack.Count > 0) ShowTransactionList(_CurrentSelectedPump);
                                break;

                            case PumpState.Calling:
                                _CurrentSelectedPump.AuthoriseNoLimits("", "", -1);
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        void pump_OnTransactionEvent(object sender, PumpTransactionEventArgs e)
        {
            // Fetch transaction properties here. Because transaction object can be null once GUI thread is Invoked (EP-872)
            int PumpNumber = e.Transaction.Pump.Number;
            string ClientActivity = e.Transaction.ClientActivity;
            Transaction t = e.Transaction;

            switch (e.EventType)
            {
                case TransactionEventType.Cleared:
                    //f (ClientActivity.StartsWith(Activity_Prepay))
                    //SetPumpCaption(PumpNumber, "");
                    break;

                case TransactionEventType.Completed:

                    //if (ClientActivity.StartsWith(Activity_Prepay))
                    //SetPumpCaption(PumpNumber, ClientActivity);

                    if (t.DeliveryType.ToString().Contains("Refund"))
                    {
                        //string messageTital = "Prepay Refund";

                        //string refundDetails = string.Format("\n Pump {0} \n Refund Trans ID {1} \n Refund Value {2}",
                        //PumpNumber, t.Id, t.DeliveryData.Money);

                        //MessageBox.Show(this, refundDetails, messageTital, MessageBoxButton.OK);
                    }
                    break;

                default:
                    //if (ClientActivity.StartsWith(Activity_Prepay))
                    //SetPumpCaption(PumpNumber, ClientActivity);
                    break;
            }
        }
        #region
        //Thuc hien kill pos khi het ngay
        private int CursorPositionX = -1;
        private int CursorPositionY = -1;
        private int LockTime = 0;
        private int Hour = 2;
        private int Minute = 0;
        private void KillPOS()
        {
            if (CursorPositionX != MousePosition.X || CursorPositionY != MousePosition.Y)
            {
                LockTime = 0;
                CursorPositionX = MousePosition.X;
                CursorPositionY = MousePosition.Y;
            }
            else
            {
                LockTime++;
            }
            if (LockTime > 60)
            {
                DateTime dt = DateTime.Now;
                if (dt.Hour == Hour && dt.Minute == Minute)
                {
                    Application.Exit();
                }
            }
        }
        #endregion
        //Thuc hien reset orderID khi sang ngay moi
        //Tan code 20/7/2015
        private int countTime = 0;
        private void AutoResetOrderID()
        {
            if (Class.Config.GetEnableResetOrderID())
            {
                DateTime time = DateTime.Now;
                if (time.Hour == 0 && time.Minute == 0)
                {
                    if (countTime > 50)
                    {
                        Class.UpdateToServer.UpDatabaseToServer(conn, mReadConfig);
                        countTime = 0;
                        NextCustomer();
                    }
                    else
                    {
                        countTime++;
                    }
                }
            }
        }
    }
}