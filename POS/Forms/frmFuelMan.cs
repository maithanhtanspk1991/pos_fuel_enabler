﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.Forms
{
    public partial class frmFuelMan : Form
    {
        private Connection.Connection mconnect = new Connection.Connection();
        private Class.MoneyFortmat money;
        private Class.ReadConfig mReadConfig = new ReadConfig();

        public frmFuelMan(Class.MoneyFortmat mmoney)
        {
            InitializeComponent();
            money = mmoney;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Mã nhiên liệu:";
                label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label2.Text = "Tên nhiên liệu:";
                label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                label3.Text = "Đơn vị giá:";
                label4.Text = "Thuế:";
                btnUpdate.Text = "Cập nhật";
                btnBack.Text = "Quay lại";
                columnHeader1.Text = "Mã nhiên liệu";
                columnHeader2.Text = "Tên nhiên liệu";
                columnHeader3.Text = "Đơn vị giá";
                columnHeader4.Text = "Thuế";
                return;
            }
        }

        private DataTable GetFuel()
        {
            DataTable dt = new DataTable();
            try
            {
                mconnect.Open();
                string sql = "select * from itemsmenu where isFuel=1";
                dt = mconnect.Select(sql);
                lvFuel.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    Class.Fuel fuel = new Class.Fuel(Convert.ToInt32(row["itemID"].ToString()), row["itemDesc"].ToString(), Convert.ToDouble(row["unitPrice"].ToString()), Convert.ToInt32(row["gst"].ToString()));
                    ListViewItem lvi = new ListViewItem(fuel.FuelID.ToString());
                    lvi.SubItems.Add(fuel.FuelName);
                    lvi.SubItems.Add(money.Format(fuel.Price).ToString());
                    lvi.SubItems.Add(fuel.GST + "%");
                    lvi.Tag = fuel;
                    lvFuel.Items.Add(lvi);
                }
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmFuelMan_Load(object sender, EventArgs e)
        {
            GetFuel();
        }

        private void lvFuel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvFuel.SelectedIndices.Count > 0)
            {
                Class.Fuel fuel = (Class.Fuel)lvFuel.SelectedItems[0].Tag;
                txtFuelId.Text = fuel.FuelID.ToString();
                txtFuelName.Text = fuel.FuelName;
                txtUnitPrice.Text = money.Format(fuel.Price);
                txtGST.Text = fuel.GST.ToString();
            }
        }

        private void UpdateFuelPrice(string fuelID, string price, int gst)
        {
            try
            {
                string sql = "update itemsmenu set unitPrice=" + price + ", gst = " + gst + " where itemID=" + fuelID;
                mconnect.Open();
                mconnect.BeginTransaction();
                mconnect.ExecuteNonQuery(sql);
                mconnect.Commit();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmFuelMan:::UpdateFuelPrice:::" + ex.Message);
                mconnect.Rollback();
            }
            finally
            {
                mconnect.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvFuel.SelectedIndices.Count > 0)
            {
                Class.Fuel fuel = (Class.Fuel)lvFuel.SelectedItems[0].Tag;
                if (txtFuelId.Text == "" || txtFuelName.Text == "" || txtUnitPrice.Text == "")
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Tất cả thông tin không được trống!";
                        lbStatus.Visible = true;
                        return;
                    }
                    lbStatus.Text = "All information not empty!";
                    lbStatus.Visible = true;
                }
                else
                {
                    try
                    {
                        string gst = txtGST.Text;
                        gst = gst == "" ? "0" : gst;
                        UpdateFuelPrice(txtFuelId.Text, money.getFortMat(txtUnitPrice.Text).ToString(), Convert.ToInt32(gst));
                        GetFuel();
                        lbStatus.Visible = false;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }
}