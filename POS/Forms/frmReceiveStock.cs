﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmReceiveStock : Form
    {
        private SystemConfig.DBConfig mDBConfig;
        private DataObject.Transit mTransit;
        private Barcode.SerialPort mSerialPortBarcode;
        private Class.MoneyFortmat mMoney = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

        public DataObject.ReceiveLine mReceiveLine { get; set; }

        public frmReceiveStock(SystemConfig.DBConfig dBConfig, Barcode.SerialPort serialPort, DataObject.Transit transit)
        {
            InitializeComponent();
            mDBConfig = dBConfig;
            mSerialPortBarcode = serialPort;
            mTransit = transit;
        }

        #region LoadData

        private void Default()
        {
            lvReceive.Items.Clear();
            txtItemName.Text = "";
            txtPrice.Text = "";
            txtQty.Text = "";
            txtTotal.Text = "";
            txtTicket.Text = "";
            dtpDate.Value = DateTime.Now;
            lbSubTotal.Text = "0";
        }

        private void LoadSupplier()
        {
            List<DataObject.Suppliers> lsArray = new List<DataObject.Suppliers>();
            lsArray = BusinessObject.BOSuppliers.GetSuppliers("", 1, mDBConfig);
            cbbSupplier.DataSource = lsArray;
            cbbSupplier.DisplayMember = "SupplierName";
            cbbSupplier.ValueMember = "SupplierID";
        }

        private void LoadConfig()
        {
            lbCableID.Text = mTransit.CableID.ToString();
            lbStaffName.Text = mTransit.StaffName;
        }

        private void SetSizeLsitView()
        {
            int Width = lvReceive.Width;
            lvReceive.Columns[0].Width = Width * 10 / 100;
            lvReceive.Columns[1].Width = Width * 45 / 100;
            lvReceive.Columns[2].Width = Width * 14 / 100;
            lvReceive.Columns[3].Width = Width * 14 / 100;
            lvReceive.Columns[4].Width = Width * 14 / 100;
        }

        private void SetTextBoxReceiveLine()
        {
            txtItemName.Text = mReceiveLine.ItemsMenu.ItemShort;
            txtPrice.Text = mReceiveLine.Price.ToString();
            txtQty.Text = mReceiveLine.Qty.ToString();
            Total();
        }

        public void Total()
        {
            if (txtPrice.Text != "" && txtQty.Text != "")
            {
                double Price = mMoney.getFortMat(txtPrice.Text.Trim());
                int Qty = Convert.ToInt32(txtQty.Text.Trim());
                txtTotal.Text = mMoney.Format2(Price * Qty);
            }
            else
            {
                txtTotal.Text = "0";
            }
        }

        private void AddListView(DataObject.ReceiveLine item, int no)
        {
            ListViewItem li = new ListViewItem(no.ToString());
            li.SubItems.Add(item.ItemsMenu.ItemShort);
            li.SubItems.Add(item.Qty.ToString());
            li.SubItems.Add(mMoney.FormatNew2(item.Price));
            li.SubItems.Add(mMoney.FormatNew2(item.Total));
            li.Tag = item;
            lvReceive.Items.Add(li);
        }

        public void SubTotal()
        {
            double total = 0;
            foreach (ListViewItem li in lvReceive.Items)
            {
                DataObject.ReceiveLine item = (DataObject.ReceiveLine)li.Tag;
                total += item.Total;
            }
            lbSubTotal.Text = mMoney.FormatNew2(total);
        }

        #endregion LoadData

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmReceiveStockLine frmReceiveStockLine = new frmReceiveStockLine(mDBConfig, mTransit, mSerialPortBarcode);
            if (frmReceiveStockLine.ShowDialog() == DialogResult.OK)
            {
                int no = 1;
                foreach (DataObject.ReceiveLine item in frmReceiveStockLine.lsReceiveLine)
                {
                    AddListView(item, no++);
                }
                lbStatus.Text = "Add Successfull";
                SubTotal();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void frmReceiveStock_Load(object sender, EventArgs e)
        {
            SetSizeLsitView();
            LoadSupplier();
            LoadConfig();
        }

        private void lvReceive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                lbStatus.Text = "";
                mReceiveLine = ((DataObject.ReceiveLine)lvReceive.SelectedItems[0].Tag);
                SetTextBoxReceiveLine();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                if (txtQty.Text == "" || txtPrice.Text == "")
                {
                    lbStatus.Text = "Imformation Empty";
                    return;
                }
                ListViewItem li = lvReceive.SelectedItems[0];
                DataObject.ReceiveLine item = ((DataObject.ReceiveLine)lvReceive.SelectedItems[0].Tag);
                item.Qty = Convert.ToInt32(txtQty.Text.Trim());
                item.Price = Convert.ToDouble(txtPrice.Text.Trim());
                item.Total = item.Qty * item.Price;
                li.SubItems[2].Text = txtQty.Text;
                li.SubItems[3].Text = mMoney.FormatNew2(txtPrice.Text);
                li.SubItems[4].Text = mMoney.FormatNew2(item.Total);
                lbStatus.Text = "Update Successfull";
                SubTotal();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvReceive.SelectedItems.Count > 0)
            {
                lbStatus.Text = "Deleted Successfull";
                lvReceive.SelectedItems[0].Remove();
                SubTotal();
            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            Total();
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            Total();
        }

        private void btnReceive_Click(object sender, EventArgs e)
        {
            if (lvReceive.Items.Count > 0)
            {
                DataObject.Receive item = new DataObject.Receive();
                item.TicketID = txtTicket.Text.Trim();
                item.SubTotal = Convert.ToDouble(lbSubTotal.Text);
                item.SupplierID = Convert.ToInt32(cbbSupplier.SelectedValue);
                item.StaffID = mTransit.StaffID;
                item.Date = dtpDate.Value;
                item.CableID = mTransit.CableID;
                foreach (ListViewItem li in lvReceive.Items)
                {
                    DataObject.ReceiveLine rl = (DataObject.ReceiveLine)li.Tag;
                    item.ReceiveLine.Add(rl.Copy());
                }

                if (BusinessObject.BOReceive.Insert(item, mDBConfig) > 0)
                {
                    lbStatus.Text = "Receive Successfull";
                    Default();
                }
                else
                {
                    lbStatus.Text = "Receive failed!";
                }
            }
        }
    }
}