﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmBooking : Form
    {
        private Connection.Connection mConnect = new Connection.Connection();
        private Class.MoneyFortmat money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Printer mprinter;
        private string BookCode = "";
        private static Color[] listColor = { Color.Red, Color.Orange, Color.HotPink, Color.Green, Color.Blue, Color.Purple, Color.Gray };
        private int mIndex = 0;
        private int zIndex = 0;
        private int yIndex = 0;
        private int mGroupIndex = 0;
        private Group mGroup;
        private Item mItem;
        private ItemLine mLine;
        private int mGroupShortCut = 0;

        public frmBooking()
        {
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            mprinter = new Printer();
            InitializeComponent();
            initMenuButton();
            mprinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);
            float l_y = 0;
            string header = "BECAS POS DEMO";
            string bankCode = "ABN. 1234567890";
            string address = "133 Alexander Street, Crows Nest 2065";
            string tell = "T:94327867 M:0401950967";

            l_y = mprinter.DrawString(header, e, new System.Drawing.Font("Arial", 18), l_y, 2);
            l_y = mprinter.DrawString(bankCode, e, font11, l_y, 2);
            l_y = mprinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mprinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 100;

            DateTime dateTime = DateTime.Now;
            mprinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, font11, l_y, 3);
            l_y += 100;

            l_y = mprinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dash, l_y, 1);
            l_y += 100;

            l_y = mprinter.DrawString("Booking Number: " + BookCode, e, font11, l_y, 1);
            //l_y = mprinter.DrawString("Company: " + textBox2.Text, e, font11, l_y, 1);
            //l_y = mprinter.DrawString("Phone: " + textBoxNumbericPOSPhone.Text, e, font11, l_y, 1);
            //l_y = mprinter.DrawString("Mobile: " + textBoxNumbericPOSMobile.Text, e, font11, l_y, 1);
            //l_y = mprinter.DrawString("Customer No: " + CustCode, e, font11, l_y, 1);
            //l_y = mprinter.DrawString("Account Limit: " + textBoxNumbericPOS1.Text + "$", e, font11, l_y, 1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmBooking_Load(object sender, EventArgs e)
        {
            Getdatetimesystem();
        }

        private void Getlistorder()
        {
            //for (int i = 0; i < listbook.Items.Count; i++)
            //{
            //    ListViewItem lvi = new ListViewItem(listbook.Items[i].Tag.ToString());
            //    for (int j = 1; j < listbook.Items[i].SubItems.Count; j++)
            //    {
            //        lvi.SubItems.Add(listbook.Items[i].SubItems[j].Tag.ToString());
            //    }
            //    listView1.Items.Add(lvi);
            //}
        }

        private void Getdatetimesystem()
        {
            textBoxNumbericPOSyear.Text = DateTime.Now.Year.ToString();
            textBoxNumbericPOSmonth.Text = DateTime.Now.Month.ToString();
            textBoxNumbericPOSday.Text = DateTime.Now.Day.ToString();
            textBoxNumbericPOShour.Text = DateTime.Now.Hour.ToString();
            textBoxNumbericPOSminute.Text = DateTime.Now.Minute.ToString();
            textBoxNumbericPOSsecond.Text = DateTime.Now.Second.ToString();
        }

        private bool CheckInputBook()
        {
            if (textBoxNumbericPOSday.Text == "" || textBoxNumbericPOSday.Text.Contains('.') || Convert.ToInt32(textBoxNumbericPOSday.Text) > 31 || Convert.ToInt32(textBoxNumbericPOSday.Text) == 0)
            {
                posLabelStatus1.Text = "Invalid booking day";
                return false;
            }
            else if (textBoxNumbericPOSmonth.Text == "" || textBoxNumbericPOSmonth.Text.Contains('.') || Convert.ToInt32(textBoxNumbericPOSmonth.Text) > 12 || Convert.ToInt32(textBoxNumbericPOSmonth.Text) == 0)
            {
                posLabelStatus1.Text = "Invalid booking month";
                return false;
            }
            else if (textBoxNumbericPOSyear.Text.Length < 4 || textBoxNumbericPOSyear.Text.Contains('.'))
            {
                posLabelStatus1.Text = "Invalid booking year";
                return false;
            }
            else if (textBoxNumbericPOShour.Text == "" || textBoxNumbericPOShour.Text.Contains('.') || Convert.ToInt32(textBoxNumbericPOShour.Text) > 23)
            {
                posLabelStatus1.Text = "Invalid booking hour";
                return false;
            }
            else if (textBoxNumbericPOSminute.Text == "" || textBoxNumbericPOSminute.Text.Contains('.') || Convert.ToInt32(textBoxNumbericPOSminute.Text) > 59)
            {
                posLabelStatus1.Text = "Invalid booking minute";
                return false;
            }
            else if (textBoxNumbericPOSsecond.Text == "" || textBoxNumbericPOSsecond.Text.Contains('.') || Convert.ToInt32(textBoxNumbericPOSsecond.Text) > 59)
            {
                posLabelStatus1.Text = "Invalid booking second";
                return false;
            }
            else if (txtname.Text == "" || txtphone.Text == "" && txtmobile.Text == "")
            {
                posLabelStatus1.Text = "Invalid customer infomation";
                return false;
            }
            else if (txtdeposit.Text == "")
            {
                posLabelStatus1.Text = "Please deposit";
                return false;
            }
            else if (listView1.Items.Count == 0)
            {
                posLabelStatus1.Text = "Please book in menu";
                return false;
            }
            return true;
        }

        private string GetTimeBook()
        {
            string day = "";
            string month = "";
            string year = textBoxNumbericPOSyear.Text;
            string hour = "";
            string minute = "";
            string second = "";
            if (textBoxNumbericPOSday.Text.Length == 1)
            {
                day = "0" + textBoxNumbericPOSday.Text;
            }
            else
            {
                day = textBoxNumbericPOSday.Text;
            }
            if (textBoxNumbericPOSmonth.Text.Length == 1)
            {
                month = "0" + textBoxNumbericPOSmonth.Text;
            }
            else
            {
                month = textBoxNumbericPOSmonth.Text;
            }
            if (textBoxNumbericPOShour.Text.Length == 1)
            {
                hour = "0" + textBoxNumbericPOShour.Text;
            }
            else
            {
                hour = textBoxNumbericPOShour.Text;
            }
            if (textBoxNumbericPOSminute.Text.Length == 1)
            {
                minute = "0" + textBoxNumbericPOSminute.Text;
            }
            else
            {
                minute = textBoxNumbericPOSminute.Text;
            }
            if (textBoxNumbericPOSsecond.Text.Length == 1)
            {
                second = "0" + textBoxNumbericPOSsecond.Text;
            }
            else
            {
                second = textBoxNumbericPOSsecond.Text;
            }
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }

        private class Group
        {
            public string GroupID { get; set; }

            public string GroupName { get; set; }
        }

        private class Item
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }
        }

        private class ItemLine
        {
            public string lineID { get; set; }

            public string lineName { get; set; }
        }

        private void initMenuButton()
        {
            for (int i = 0; i < 7; i++)
            {
                int bgcolor = i;
                Button btn = new Button();
                btn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
                btn.ForeColor = Color.LightGray;
                btn.Dock = DockStyle.Fill;
                btn.Width = 200;
                btn.Height = 200;
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                btn.BackColor = listColor[i];
                tlp_groups.Controls.Add(btn);
                btn.Click += new EventHandler(btn_Click);
            }

            for (int i = 0; i < 21; i++)
            {
                Button btni = new Button();
                btni.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
                btni.Width = 200;
                btni.Height = 200;
                btni.Dock = DockStyle.Fill;
                btni.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tlp_items.Controls.Add(btni);
                btni.Click += new EventHandler(btni_Click);
                btni.Enabled = false;
            }
            mIndex = 0;
            LoadGroup();

            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void btni_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Name == "Item")
            {
                try
                {
                    mItem.ItemID = btn.Tag.ToString();
                    mItem.ItemName = btn.Text;
                    LoadLineItem(mItem.ItemID);
                    mConnect.Open();
                    AddItemToListView(mItem.ItemName, mItem.ItemID.ToString(), "itemsmenu", "itemID");
                    mConnect.Close();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmBooking:::Item Click:::" + ex.Message);
                }
            }
            else
            {
                try
                {
                    mLine.lineID = btn.Tag.ToString();
                    mLine.lineName = btn.Text;
                    mConnect.Open();
                    AddItemToListView(mLine.lineName, mLine.lineID.ToString(), "itemslinemenu", "lineID");
                    mConnect.Close();
                }
                catch (Exception exs)
                {
                    Class.LogPOS.WriteLog("frmBooking:::Item Line Click:::" + exs.Message);
                }
            }
        }

        private void AddItemToListView(string itemname, string itemid, string fieldtableName, string itemIDName)
        {
            string price = mConnect.ExecuteScalar("select unitPrice from " + fieldtableName + " where " + itemIDName + "=" + itemid).ToString();
            ListViewItem lvi = new ListViewItem("1");
            lvi.SubItems.Add(itemname);
            lvi.SubItems.Add(money.Format(Convert.ToDouble(price)));
            lvi.SubItems.Add(itemid);
            if (String.Compare(fieldtableName, "itemsmenu") == 0 && String.Compare(itemIDName, "itemID") == 0)
            {
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, Convert.ToInt32(itemid), itemname, 1, Convert.ToDouble(price), Convert.ToDouble(price), 0, 0, 0, "0");
                lvi.Tag = item;
            }
            else if (String.Compare(fieldtableName, "itemslinemenu") == 0 && String.Compare(itemIDName, "lineID") == 0)
            {
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, 0, itemname, 1, Convert.ToDouble(price), Convert.ToDouble(price), 0, 0, Convert.ToInt32(itemid), "0");
                lvi.Tag = item;
            }
            listView1.Items.Add(lvi);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Tag != null)
            {
                zIndex = 0;
                mGroupIndex = tlp_groups.Controls.GetChildIndex(btn);
                mGroup.GroupID = btn.Tag.ToString();
                mGroup.GroupName = btn.Text;
                mItem = new Item();
                LoadItem(mGroup.GroupID);
            }
        }

        //load group menu
        private void LoadGroup()
        {
            try
            {
                if (mIndex < 0)
                {
                    mIndex = 0;
                }
                DataTable dt = mConnect.Select("SELECT groupID,groupShort FROM groupsmenu where (0=" + mGroupShortCut + " or grShortCut=" + mGroupShortCut + ") and groupID<>44 order by displayOrder limit " + mIndex + ",7");
                if (dt.Rows.Count <= 0)
                {
                    mIndex -= 7;
                    return;
                }

                mGroup.GroupID = dt.Rows[0]["groupID"].ToString();
                mGroup.GroupName = dt.Rows[0]["groupShort"].ToString();
                mItem = new Item();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_groups.Controls[i].Tag = dr["groupID"].ToString();
                    tlp_groups.Controls[i].Text = dr["groupShort"].ToString();
                    tlp_groups.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 7; i++)
                {
                    tlp_groups.Controls[i].Text = "";
                    tlp_groups.Controls[i].Tag = null;
                    tlp_groups.Controls[i].Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmMenuChange.LoadGroup:::" + ex.Message);
            }
        }

        //load item menu
        private bool LoadItem(string groupID)
        {
            try
            {
                if (zIndex < 0)
                {
                    zIndex = 0;
                }
                DataTable dt = mConnect.Select("select i.itemID,i.itemShort from itemsmenu i where groupID=" + groupID + " order by displayOrder limit " + zIndex + ",21");
                if (dt.Rows.Count <= 0 && zIndex > 0)
                {
                    zIndex -= 21;
                    return false;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_items.Controls[i].Text = dr["itemShort"].ToString();
                    tlp_items.Controls[i].Tag = dr["itemID"].ToString();
                    tlp_items.Controls[i].Name = "Item";
                    tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                    tlp_items.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 21; i++)
                {
                    tlp_items.Controls[i].Text = "";
                    tlp_items.Controls[i].Tag = null;
                    tlp_items.Controls[i].BackColor = Color.White;
                    tlp_items.Controls[i].Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmBooking:::Load Item Menu:::" + ex.Message);
            }
            return true;
        }

        //load line menu
        private void LoadLineItem(string itemID)
        {
            try
            {
                if (yIndex < 0)
                {
                    yIndex = 0;
                }
                DataTable dt = mConnect.Select("select lineID,itemShort from itemslinemenu where itemID=" + itemID + " order by displayOrder limit " + yIndex + ",21");
                if (dt.Rows.Count <= 0)
                {
                    yIndex -= 21;
                    return;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_items.Controls[i].Name = "SubItem";
                    tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                    tlp_items.Controls[i].Text = dr["itemShort"].ToString();
                    tlp_items.Controls[i].Tag = dr["lineID"].ToString();
                    tlp_items.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 21; i++)
                {
                    tlp_items.Controls[i].Text = "";
                    tlp_items.Controls[i].Tag = null;
                    tlp_items.Controls[i].Enabled = false;
                    tlp_items.Controls[i].BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmMenuChange.LoadLineItem:::" + ex.Message);
            }
        }

        public void LoadFirstGroup()
        {
            mIndex = 0;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private string SplitDate(DateTime date)
        {
            string strdate = date.ToShortDateString();
            string strtime = date.ToLongTimeString();
            string[] listdate = strdate.Split('/');
            string[] listtime = strtime.Split(':');
            if (listdate[0].ToString().Length == 1)
            {
                string tam = listdate[0].ToString();
                string str = "0" + tam;
                listdate[0] = str;
            }
            if (listdate[1].ToString().Length == 1)
            {
                string dtam = listdate[1].ToString();
                string dstr = "0" + dtam;
                listdate[1] = dstr;
            }
            return listdate[2].ToString() + "-" + listdate[0].ToString() + "-" + listdate[1].ToString() + " " + listtime[0].ToString() + ":" + listtime[1].ToString() + ":" + listtime[2].ToString();
        }

        private string GettypeCombobox(ComboBox cmb)
        {
            if (cmb.Text == "Eat In")
            {
                return "1";
            }
            else if (cmb.Text == "Take A Way")
            {
                return "2";
            }
            return "1";
        }

        private string RandomNumber()
        {
            DateTime tmeNow = DateTime.Now;
            int mls = tmeNow.Millisecond;
            // Generate two random numbers between 100 and 999
            Random rndNumber = new Random(mls);
            int NewNumber1 = rndNumber.Next(100, 999);
            int NewNumber2 = rndNumber.Next(100, 999);
            int NewNumber3 = rndNumber.Next(100, 999);
            int NewNumber4 = rndNumber.Next(100, 999);
            // Create an item number from the random numbers
            String strItemNumber = NewNumber1.ToString() + NewNumber2.ToString() + NewNumber3.ToString() + NewNumber4.ToString();
            return strItemNumber;
        }

        private bool SendBooking()
        {
            string strSQL = "";
            try
            {
                mConnect.Open();
                mConnect.BeginTransaction();
                BookCode = RandomNumber();
                strSQL = "insert into customers(Name,phone,mobile) value(" + "'" + txtname.Text + "'," + "'" + txtphone.Text + "'," + "'" + txtmobile.Text + "');";
                strSQL += "set @cnew = LAST_INSERT_ID();";
                strSQL += "insert into booking(custID,curenDate,bookingDate,booktype,persionals,bookingdeposit,bookcode) value(@cnew," + "'" + SplitDate(DateTime.Now) + "'," + "'" + GetTimeBook() + "'," + "'" + GettypeCombobox(comboBox1) + "'," + "'" + txtpeople.Text + "'," + "'" + money.getFortMat(txtdeposit.Text) + "'," + "'" + BookCode + "');";
                strSQL += "set @bnew = LAST_INSERT_ID();";
                if (listView1.Items.Count > 0)
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        Class.ProcessOrderNew.Item item = (Class.ProcessOrderNew.Item)listView1.Items[i].Tag;
                        strSQL += "insert into bookingline(bookingID,qty,itemID,price,subTotal,optionID) value(@bnew," + "'" + item.Qty.ToString() + "'," + "'" + item.ItemID.ToString() + "'," + "'" + item.Price.ToString() + "'," + "'" + item.SubTotal.ToString() + "'," + "'" + item.OptionCount.ToString() + "'" + ");";
                    }
                }
                //strSQL += "select sum(subTotal) from bookingline where bookingID=@bnew";
                //strSQL += "insert into booking(subTotal) value(" + "'" + subtotal + "')";
                mConnect.ExecuteScript(strSQL);
                mConnect.Commit();
                return true;
            }
            catch (Exception ex)
            {
                mConnect.Rollback();
                Class.LogPOS.WriteLog("frmBooking:::SendBooking:::" + ex.Message);
                return false;
            }
            finally
            {
                mConnect.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CheckInputBook() == true)
            {
                if (SendBooking())
                {
                    mprinter.SetPrinterName(dbconfig.BarPrinter.ToString());
                    mprinter.Print();
                    posLabelStatus1.Text = "Booking Sucessful";
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmMenuChange.MainMenu_Click.");
            mIndex = 0;
            mGroupShortCut = 2;
            LoadFirstGroup();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmMenuChange.DrinkMenu_Click.");
            mIndex = 0;
            mGroupShortCut = 4;
            LoadFirstGroup();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmMenuChange.Short Cut_Click.");
            mIndex = 0;
            mGroupShortCut++;
            if (mGroupShortCut > 4)
            {
                mGroupShortCut = 0;
            }
            LoadFirstGroup();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mIndex -= 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            mIndex += 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            zIndex -= 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            zIndex += 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmBooking.Load Option");
            Button btn = (Button)sender;
            if (btn.Tag == null)
            {
                string groupID = "-1";
                try
                {
                    mConnect.Open();
                    groupID = mConnect.ExecuteScalar("select groupID from groupsmenu where grShortCut=1 and grOption=0 limit 0,1").ToString();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmBooking.buttonOption_click:::" + ex.Message);
                }
                finally
                {
                    mConnect.Close();
                }
                zIndex = 0;
                LoadItem(groupID);
                btn.Tag = 1;
            }
            else if (btn.Tag.ToString() == "1")
            {
                string groupID = "-1";
                try
                {
                    mConnect.Open();
                    groupID = mConnect.ExecuteScalar("select groupID from groupsmenu where grShortCut=1 and grOption=" + tlp_groups.Controls[mGroupIndex].Name + " limit 0,1").ToString();
                }
                catch (Exception exs)
                {
                    Class.LogPOS.WriteLog("frmBooking.buttonOption_click:::" + exs.Message);
                }
                finally
                {
                    mConnect.Close();
                }
                zIndex = 0;
                btn.Tag = 2;
                if (LoadItem(groupID) == false)
                {
                    button10_Click(btn, null);
                }
            }
            else
            {
                zIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Name);
                btn.Tag = null;
            }
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(txtname);
            frm.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                listView1.Items.Clear();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}