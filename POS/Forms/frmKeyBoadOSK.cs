﻿using System;
using System.Runtime.InteropServices;

namespace POS.Forms
{
    internal class frmKeyBoadOSK : System.Windows.Forms.Form
    {
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOREDRAW = 0x0008;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_SHOWWINDOW = 0x0040;
        private const uint SWP_HIDEWINDOW = 0x0080;
        private const uint SWP_NOCOPYBITS = 0x0100;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSENDCHANGING = 0x0400;

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X,
        int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public void showKeypad()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = "C:\\WINDOWS\\system32\\osk.exe";
            process.StartInfo.Arguments = "";
            process.StartInfo.WorkingDirectory = "c:\\";
            System.Diagnostics.Process[] listPro = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pro in listPro)
            {
                if (pro.ProcessName == "osk")
                {
                    pro.Kill();
                }
            }
            process.Start();
            process.WaitForInputIdle();
            int width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            int height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            this.Width = 900;
            this.Height = 350;
            width = (width - this.Width) / 2;
            height = height - this.Height;
            SetWindowPos(process.MainWindowHandle,
            this.Handle, // Parent Window
            width, // Keypad Position X
            height, // Keypad Position Y
            this.Width, // Keypad Width
            this.Height, // Keypad Height
            SWP_SHOWWINDOW | SWP_NOZORDER); // Show Window and Place on Top

            SetForegroundWindow(process.MainWindowHandle);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmKeyBoadOSK));
            this.SuspendLayout();
            //
            // frmKeyBoadOSK
            //
            this.ClientSize = new System.Drawing.Size(888, 326);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmKeyBoadOSK";
            this.Load += new System.EventHandler(this.frmKeyBoadOSK_Load);
            this.ResumeLayout(false);
        }

        private void frmKeyBoadOSK_Load(object sender, EventArgs e)
        {
        }
    }
}