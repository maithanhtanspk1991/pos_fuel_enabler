﻿namespace POS.Forms
{
    partial class frmLoyatyControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonViewItem = new System.Windows.Forms.Button();
            this.buttonViewFullList = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.textBoxPOSKeyBoardDiscount = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyBoardSalesPrice = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyBoardPrice = new POS.TextBoxPOSKeyPad();
            this.textBoxPOSKeyBoardQty = new POS.TextBoxPOSKeyPad();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddNew = new System.Windows.Forms.Button();
            this.checkBoxEnable = new System.Windows.Forms.CheckBox();
            this.checkBoxEndDate = new System.Windows.Forms.CheckBox();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerfrom = new System.Windows.Forms.DateTimePicker();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPOSKeyBoardItemName = new POS.TextBoxPOSKeyBoard();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPOSKeyBoardName = new POS.TextBoxPOSKeyBoard();
            this.label1 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(13, 240);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(842, 209);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 215;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Item";
            this.columnHeader2.Width = 221;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Quantity";
            this.columnHeader3.Width = 77;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Discount";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 82;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "DateStart";
            this.columnHeader5.Width = 93;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "DateEnd";
            this.columnHeader6.Width = 81;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Enable";
            this.columnHeader7.Width = 74;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonViewItem);
            this.panel1.Controls.Add(this.buttonViewFullList);
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardDiscount);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardSalesPrice);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardPrice);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardQty);
            this.panel1.Controls.Add(this.buttonDelete);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.buttonAddNew);
            this.panel1.Controls.Add(this.checkBoxEnable);
            this.panel1.Controls.Add(this.checkBoxEndDate);
            this.panel1.Controls.Add(this.dateTimePickerEnd);
            this.panel1.Controls.Add(this.dateTimePickerfrom);
            this.panel1.Controls.Add(this.labelStartDate);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardItemName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxPOSKeyBoardName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(843, 221);
            this.panel1.TabIndex = 1;
            // 
            // buttonViewItem
            // 
            this.buttonViewItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonViewItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewItem.ForeColor = System.Drawing.Color.White;
            this.buttonViewItem.Location = new System.Drawing.Point(407, 149);
            this.buttonViewItem.Name = "buttonViewItem";
            this.buttonViewItem.Size = new System.Drawing.Size(73, 47);
            this.buttonViewItem.TabIndex = 47;
            this.buttonViewItem.Text = "View Item";
            this.buttonViewItem.UseVisualStyleBackColor = false;
            this.buttonViewItem.Click += new System.EventHandler(this.buttonViewItem_Click);
            // 
            // buttonViewFullList
            // 
            this.buttonViewFullList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonViewFullList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewFullList.ForeColor = System.Drawing.Color.White;
            this.buttonViewFullList.Location = new System.Drawing.Point(493, 149);
            this.buttonViewFullList.Name = "buttonViewFullList";
            this.buttonViewFullList.Size = new System.Drawing.Size(73, 47);
            this.buttonViewFullList.TabIndex = 46;
            this.buttonViewFullList.Text = "View Full List";
            this.buttonViewFullList.UseVisualStyleBackColor = false;
            this.buttonViewFullList.Click += new System.EventHandler(this.buttonViewFullList_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.Red;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.ForeColor = System.Drawing.Color.White;
            this.buttonClose.Location = new System.Drawing.Point(13, 455);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(73, 50);
            this.buttonClose.TabIndex = 46;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonSearch.Location = new System.Drawing.Point(348, 10);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(53, 58);
            this.buttonSearch.TabIndex = 45;
            this.buttonSearch.Text = "Search Item";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // textBoxPOSKeyBoardDiscount
            // 
            this.textBoxPOSKeyBoardDiscount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardDiscount.IsLockDot = false;
            this.textBoxPOSKeyBoardDiscount.Location = new System.Drawing.Point(90, 170);
            this.textBoxPOSKeyBoardDiscount.Name = "textBoxPOSKeyBoardDiscount";
            this.textBoxPOSKeyBoardDiscount.Size = new System.Drawing.Size(141, 26);
            this.textBoxPOSKeyBoardDiscount.TabIndex = 44;
            this.textBoxPOSKeyBoardDiscount.TextChanged += new System.EventHandler(this.textBoxPOSKeyBoardDiscount_TextChanged);
            // 
            // textBoxPOSKeyBoardSalesPrice
            // 
            this.textBoxPOSKeyBoardSalesPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardSalesPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardSalesPrice.IsLockDot = false;
            this.textBoxPOSKeyBoardSalesPrice.Location = new System.Drawing.Point(90, 138);
            this.textBoxPOSKeyBoardSalesPrice.Name = "textBoxPOSKeyBoardSalesPrice";
            this.textBoxPOSKeyBoardSalesPrice.Size = new System.Drawing.Size(141, 26);
            this.textBoxPOSKeyBoardSalesPrice.TabIndex = 44;
            this.textBoxPOSKeyBoardSalesPrice.TextChanged += new System.EventHandler(this.textBoxPOSKeyBoardSalesPrice_TextChanged);
            // 
            // textBoxPOSKeyBoardPrice
            // 
            this.textBoxPOSKeyBoardPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardPrice.Enabled = false;
            this.textBoxPOSKeyBoardPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardPrice.IsLockDot = false;
            this.textBoxPOSKeyBoardPrice.Location = new System.Drawing.Point(90, 106);
            this.textBoxPOSKeyBoardPrice.Name = "textBoxPOSKeyBoardPrice";
            this.textBoxPOSKeyBoardPrice.Size = new System.Drawing.Size(141, 26);
            this.textBoxPOSKeyBoardPrice.TabIndex = 44;
            this.textBoxPOSKeyBoardPrice.TextChanged += new System.EventHandler(this.textBoxPOSKeyBoardPrice_TextChanged);
            // 
            // textBoxPOSKeyBoardQty
            // 
            this.textBoxPOSKeyBoardQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardQty.IsLockDot = true;
            this.textBoxPOSKeyBoardQty.Location = new System.Drawing.Point(90, 74);
            this.textBoxPOSKeyBoardQty.Name = "textBoxPOSKeyBoardQty";
            this.textBoxPOSKeyBoardQty.Size = new System.Drawing.Size(141, 26);
            this.textBoxPOSKeyBoardQty.TabIndex = 44;
            this.textBoxPOSKeyBoardQty.TextChanged += new System.EventHandler(this.textBoxPOSKeyBoardQty_TextChanged);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.White;
            this.buttonDelete.Location = new System.Drawing.Point(751, 149);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(73, 47);
            this.buttonDelete.TabIndex = 43;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(665, 149);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(73, 47);
            this.buttonSave.TabIndex = 43;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddNew
            // 
            this.buttonAddNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonAddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddNew.ForeColor = System.Drawing.Color.White;
            this.buttonAddNew.Location = new System.Drawing.Point(579, 149);
            this.buttonAddNew.Name = "buttonAddNew";
            this.buttonAddNew.Size = new System.Drawing.Size(73, 47);
            this.buttonAddNew.TabIndex = 43;
            this.buttonAddNew.Text = "Add New";
            this.buttonAddNew.UseVisualStyleBackColor = false;
            this.buttonAddNew.Click += new System.EventHandler(this.buttonAddNew_Click);
            // 
            // checkBoxEnable
            // 
            this.checkBoxEnable.AutoSize = true;
            this.checkBoxEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEnable.Location = new System.Drawing.Point(428, 112);
            this.checkBoxEnable.Name = "checkBoxEnable";
            this.checkBoxEnable.Size = new System.Drawing.Size(72, 22);
            this.checkBoxEnable.TabIndex = 42;
            this.checkBoxEnable.Text = "Enable";
            this.checkBoxEnable.UseVisualStyleBackColor = true;
            // 
            // checkBoxEndDate
            // 
            this.checkBoxEndDate.AutoSize = true;
            this.checkBoxEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEndDate.Location = new System.Drawing.Point(428, 71);
            this.checkBoxEndDate.Name = "checkBoxEndDate";
            this.checkBoxEndDate.Size = new System.Drawing.Size(88, 22);
            this.checkBoxEndDate.TabIndex = 40;
            this.checkBoxEndDate.Text = "End Date";
            this.checkBoxEndDate.UseVisualStyleBackColor = true;
            this.checkBoxEndDate.CheckedChanged += new System.EventHandler(this.checkBoxEndDate_CheckedChanged);
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(519, 60);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(204, 44);
            this.dateTimePickerEnd.TabIndex = 39;
            // 
            // dateTimePickerfrom
            // 
            this.dateTimePickerfrom.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerfrom.Location = new System.Drawing.Point(519, 10);
            this.dateTimePickerfrom.Name = "dateTimePickerfrom";
            this.dateTimePickerfrom.Size = new System.Drawing.Size(204, 44);
            this.dateTimePickerfrom.TabIndex = 38;
            // 
            // labelStartDate
            // 
            this.labelStartDate.AutoSize = true;
            this.labelStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartDate.Location = new System.Drawing.Point(442, 23);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(74, 18);
            this.labelStartDate.TabIndex = 0;
            this.labelStartDate.Text = "Start Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "Discount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "Sales Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "Total Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(56, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Qty";
            // 
            // textBoxPOSKeyBoardItemName
            // 
            this.textBoxPOSKeyBoardItemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardItemName.Enabled = false;
            this.textBoxPOSKeyBoardItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardItemName.Location = new System.Drawing.Point(90, 42);
            this.textBoxPOSKeyBoardItemName.Name = "textBoxPOSKeyBoardItemName";
            this.textBoxPOSKeyBoardItemName.Size = new System.Drawing.Size(251, 26);
            this.textBoxPOSKeyBoardItemName.TabIndex = 1;
            this.textBoxPOSKeyBoardItemName.TextChanged += new System.EventHandler(this.textBoxPOSKeyBoardItemName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Item Name";
            // 
            // textBoxPOSKeyBoardName
            // 
            this.textBoxPOSKeyBoardName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxPOSKeyBoardName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPOSKeyBoardName.Location = new System.Drawing.Point(90, 10);
            this.textBoxPOSKeyBoardName.Name = "textBoxPOSKeyBoardName";
            this.textBoxPOSKeyBoardName.Size = new System.Drawing.Size(251, 26);
            this.textBoxPOSKeyBoardName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // labelError
            // 
            this.labelError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelError.ForeColor = System.Drawing.Color.Red;
            this.labelError.Location = new System.Drawing.Point(0, 508);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(867, 21);
            this.labelError.TabIndex = 2;
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmLoyatyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 529);
            this.ControlBox = false;
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLoyatyControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loyaty Control";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmLoyatyControl_FormClosed);
            this.Load += new System.EventHandler(this.frmLoyatyControl_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panel1;
        private TextBoxPOSKeyBoard textBoxPOSKeyBoardName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private TextBoxPOSKeyBoard textBoxPOSKeyBoardItemName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerfrom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxEndDate;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.CheckBox checkBoxEnable;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddNew;
        private System.Windows.Forms.Label labelError;
        private TextBoxPOSKeyPad textBoxPOSKeyBoardDiscount;
        private TextBoxPOSKeyPad textBoxPOSKeyBoardSalesPrice;
        private TextBoxPOSKeyPad textBoxPOSKeyBoardPrice;
        private TextBoxPOSKeyPad textBoxPOSKeyBoardQty;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonViewFullList;
        private System.Windows.Forms.Button buttonViewItem;
    }
}