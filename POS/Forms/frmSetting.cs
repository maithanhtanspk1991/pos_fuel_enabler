﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

//using MySql.Data.MySqlClient;

namespace POS.Forms
{
    public partial class frmSetting : Form
    {
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private Connection.Connection conn = new Connection.Connection();
        private Class.ReadConfig mReadConfig = null;

        public frmSetting(Class.ReadConfig readConfig)
        {
            InitializeComponent();
            mReadConfig = readConfig;
            SetMutiLanguage();
        }

        private void SetMutiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                groupBox1.Text = "Kết nối tới MySQL";
                button1.Text = "Kiểm tra kết nối";
                groupBox2.Text = "Cấu hình máy in";
                tabPage1.Text = "Hệ thống";
                tabPage2.Text = "Thiết bị mở rộng";
                lbbill.Text = "Tên máy in:";
                lbkitchen.Text = "Tên máy in A4";
                label5.Text = "Cho phép in hóa đơn thuế";
                label5.Font = new Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                label6.Text = "Giờ kết thúc ca LV";
                label9.Text = "Cho phép sử dụng ca LV";
                label9.Font = new Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                label10.Text = "Cho phép bán nhiên liệu";
                label10.Font = new Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                label12.Text = "Cho phép bán thức ăn nhanh";
                label12.Font = new Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
                button2.Text = "Lưu lại";
                button3.Text = "Thoát";
                return;
            }
        }

        private void frmSetting_Load(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmSetting.frmSetting_Load.");
            foreach (string s in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                string name = s;
                cbbPrinterNameA4.Items.Add(name);
                cbbPrinterName.Items.Add(name);
            }
            //LoadPort();
            LoadHour();
            LoadMinute();
            LoadSecond();
            LoadAllowPrintTaxInvoice();
            LoadAllowSalesFuel();
            LoadAllowSalesFastFood();
            cbbPrinterNameA4.Text = mReadConfig.PrinterTaxinvoiceA4;
            cbbPrinterName.Text = mReadConfig.PrinterTaxinvoice;
            cboAllowPrintTaxInvoice.SelectedValue = dbconfig.AllowPrint;
            cboAllowSalesFuel.SelectedValue = dbconfig.AllowSalesFuel;
            cboAllowSalesFastFood.SelectedValue = dbconfig.AllowSalesFastFood;
            txtdatabase.Text = dbconfig.Database;
            txtserver.Text = dbconfig.Server;
            txtuser.Text = dbconfig.UserID;
            txtpass.Text = dbconfig.UserPass;
            cboHour.Text = dbconfig.Hour;
            cboMinute.Text = dbconfig.Minute;
            cboSecond.Text = dbconfig.Second;
            LoadInformationSerial();
        }

        private void LoadInformationSerial()
        {
            Class.PortCom PortCom = Class.PortCom.Get(Class.PortComType.Fuel);

            cbbStopBits.Text = PortCom.StopBits.ToString();
            cbbDataBits.Text = PortCom.DataBits.ToString();
            cbbParity.Text = PortCom.Parity.ToString();
            cbbBaudRate.Text = PortCom.BaudRate.ToString();
            cboAllowUseShift.Text = PortCom.AllowUseShift.ToString();
            CurrentDataMode = DataMode.Text;
            cbbFuelAuto.Items.Clear();
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
                cbbFuelAuto.Items.Add(s);
            if (cbbFuelAuto.Items.Contains(PortCom.PortName))
                cbbFuelAuto.Text = PortCom.PortName;
            else
                if (cbbFuelAuto.Items.Count > 0) cbbFuelAuto.SelectedIndex = 0;
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        MessageBox.Show(this, "Không có cổng COM phát hiện trên máy tính này. \nHãy cài đặt một cổng COM và khởi động lại ứng dụng này.", "Không có cổng COM được cài đặt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                        return;
                    }
                    MessageBox.Show(this, "There are no COM Ports detected on this computer.\nPlease install a COM Port and restart this app.", "No COM Ports Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
        }

        private void LoadSecond()
        {
            DataTable dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            for (int i = 0; i <= 60; i++)
            {
                dtSource.Rows.Add(i, i.ToString());
            }
            cboSecond.DataSource = dtSource;
            cboSecond.DisplayMember = "Name";
            cboSecond.ValueMember = "ID";
        }

        private void LoadMinute()
        {
            DataTable dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            for (int i = 0; i <= 60; i++)
            {
                dtSource.Rows.Add(i, i.ToString());
            }
            cboMinute.DataSource = dtSource;
            cboMinute.DisplayMember = "Name";
            cboMinute.ValueMember = "ID";
        }

        private void LoadHour()
        {
            DataTable dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            for (int i = 1; i <= 24; i++)
            {
                dtSource.Rows.Add(i, i.ToString());
            }
            cboHour.DataSource = dtSource;
            cboHour.DisplayMember = "Name";
            cboHour.ValueMember = "ID";
        }

        private DataTable dtSource;

        private void LoadAllowPrintTaxInvoice()
        {
            dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            dtSource.Rows.Add(1, "True");
            dtSource.Rows.Add(0, "False");
            cboAllowPrintTaxInvoice.DataSource = dtSource;
            cboAllowPrintTaxInvoice.DisplayMember = "Name";
            cboAllowPrintTaxInvoice.ValueMember = "ID";
        }

        private void LoadAllowSalesFastFood()
        {
            dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            dtSource.Rows.Add(1, "True");
            dtSource.Rows.Add(0, "False");
            cboAllowSalesFastFood.DataSource = dtSource;
            cboAllowSalesFastFood.DisplayMember = "Name";
            cboAllowSalesFastFood.ValueMember = "ID";
        }

        private void LoadAllowSalesFuel()
        {
            dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(Int32));
            dtSource.Columns.Add("Name", typeof(String));
            dtSource.Rows.Add(1, "True");
            dtSource.Rows.Add(0, "False");
            cboAllowSalesFuel.DataSource = dtSource;
            cboAllowSalesFuel.DisplayMember = "Name";
            cboAllowSalesFuel.ValueMember = "ID";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        public enum DataMode { Text, Hex }

        private DataMode CurrentDataMode
        {
            get
            {
                if (rbHex.Checked)
                    return DataMode.Hex;
                else
                    return DataMode.Text;
            }
            set
            {
                if (value == DataMode.Text)
                    rbText.Checked = true;
                else
                    rbHex.Checked = true;
            }
        }

        private void SaveSettings()
        {
            Class.PortCom PortCom = new Class.PortCom();
            PortCom = Class.PortCom.Set(cboAllowUseShift.Text, cbbBaudRate.Text, cbbDataBits.Text, cbbParity.Text, cbbStopBits.Text, cbbFuelAuto.Text);
            PortCom.PortComType = Class.PortComType.Fuel;
            Class.PortCom.Set(PortCom);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("frmSetting.button_submitprinter_Click.");
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                frmMessageBox frm1 = new frmMessageBox("Thông tin", "Bạn có chắc chắn để chấp nhận các cài đặt cơ sở dữ liệu mới và máy in?");
                frm1.ShowDialog();
                if (frm1.DialogResult == DialogResult.OK)
                {
                    try
                    {
                        mReadConfig.PrinterTaxinvoice = cbbPrinterName.SelectedItem.ToString();
                        mReadConfig.PrinterTaxinvoiceA4 = cbbPrinterNameA4.SelectedItem.ToString();
                        mReadConfig.WritePrinterTaxinvoice(cbbPrinterName.SelectedItem.ToString());
                        mReadConfig.WritePrinterTaxinvoiceA4(cbbPrinterNameA4.SelectedItem.ToString());
                        dbconfig.AllowPrint = Convert.ToInt32(cboAllowPrintTaxInvoice.SelectedValue);
                        dbconfig.AllowSalesFastFood = Convert.ToInt32(cboAllowSalesFastFood.SelectedValue);
                        dbconfig.AllowSalesFuel = Convert.ToInt32(cboAllowSalesFuel.SelectedValue);
                        dbconfig.Hour = cboHour.SelectedValue.ToString();
                        dbconfig.Minute = cboMinute.SelectedValue.ToString();
                        dbconfig.Second = cboSecond.SelectedValue.ToString();
                        dbconfig.WriteConfig(txtdatabase.Text, txtserver.Text, txtuser.Text, txtpass.Text);
                        SaveSettings();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("frmSetting.button_submitprinter_Click:::" + ex.Message);
                    }
                }
                return;
            }
            frmMessageBox frm = new frmMessageBox("Information", "Are you sure to accept the new setting database and printer?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                try
                {
                    mReadConfig.PrinterTaxinvoice = cbbPrinterName.SelectedItem.ToString();
                    mReadConfig.PrinterTaxinvoiceA4 = cbbPrinterNameA4.SelectedItem.ToString();
                    mReadConfig.WritePrinterTaxinvoice(cbbPrinterName.SelectedItem.ToString());
                    mReadConfig.WritePrinterTaxinvoiceA4(cbbPrinterNameA4.SelectedItem.ToString());
                    dbconfig.AllowPrint = Convert.ToInt32(cboAllowPrintTaxInvoice.SelectedValue);
                    dbconfig.AllowSalesFastFood = Convert.ToInt32(cboAllowSalesFastFood.SelectedValue);
                    dbconfig.AllowSalesFuel = Convert.ToInt32(cboAllowSalesFuel.SelectedValue);
                    dbconfig.Hour = cboHour.SelectedValue.ToString();
                    dbconfig.Minute = cboMinute.SelectedValue.ToString();
                    dbconfig.Second = cboSecond.SelectedValue.ToString();
                    dbconfig.WriteConfig(txtdatabase.Text, txtserver.Text, txtuser.Text, txtpass.Text);
                    SaveSettings();
                    this.Close();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmSetting.button_submitprinter_Click:::" + ex.Message);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (conn.TestConnection(txtserver.Text, txtdatabase.Text, txtuser.Text, txtpass.Text) == true)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    frmMessageBoxOK frm1 = new frmMessageBoxOK("THÀNH CÔNG", "Kết nối tới cơ sở dữ liệu thành công!");
                    frm1.ShowDialog();
                    return;
                }
                frmMessageBoxOK frm = new frmMessageBoxOK("SUCESSFULLY", "Connect to database sucessful!");
                frm.ShowDialog();
                //MessageBox.Show(" Connect to database sucessful! "," SUCESSFULLY ",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    frmMessageBoxOK frm1 = new frmMessageBoxOK("THẤT BẠI", "Kết nối tới cơ sở dữ liệu không thành công!");
                    frm1.ShowDialog();
                    return;
                }
                frmMessageBoxOK frm = new frmMessageBoxOK("Connect to database fail!", "FAIL");
                frm.ShowDialog();
                //MessageBox.Show(" Connect to database fail! ", " FAIL ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
        }

        private void lbkitchen_Click(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}