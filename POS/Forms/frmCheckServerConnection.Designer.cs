﻿namespace POS.Forms
{
    partial class frmCheckServerConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTimeOut = new System.Windows.Forms.Label();
            this.btnUsingLocalhost = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTimeOut
            // 
            this.lblTimeOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeOut.Location = new System.Drawing.Point(12, 83);
            this.lblTimeOut.Name = "lblTimeOut";
            this.lblTimeOut.Size = new System.Drawing.Size(452, 52);
            this.lblTimeOut.TabIndex = 0;
            this.lblTimeOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.btnUsingLocalhost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsingLocalhost.Location = new System.Drawing.Point(167, 149);
            this.btnUsingLocalhost.Name = "button1";
            this.btnUsingLocalhost.Size = new System.Drawing.Size(142, 52);
            this.btnUsingLocalhost.TabIndex = 1;
            this.btnUsingLocalhost.Text = "Using Localhost";
            this.btnUsingLocalhost.UseVisualStyleBackColor = true;
            this.btnUsingLocalhost.Click += new System.EventHandler(this.btnUsingLocalhost_Click);
            // 
            // frmCheckServerConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 301);
            this.Controls.Add(this.btnUsingLocalhost);
            this.Controls.Add(this.lblTimeOut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCheckServerConnection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCheckServerConnection";
            this.Load += new System.EventHandler(this.frmCheckServerConnection_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTimeOut;
        private System.Windows.Forms.Button btnUsingLocalhost;
    }
}