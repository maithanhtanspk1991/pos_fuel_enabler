﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using POS.Class;
using POS.FindObject;
using POS.Properties;

namespace POS.Forms
{
    public partial class frmOrdersAllQuickSales : Form
    {
        private SystemConfig.DBConfig mDBConfig = new SystemConfig.DBConfig();
        public static int mStaffID = 0;
        public int mStaffPermission = 0;
        private UCInfoTop mUCInfoTop;
        private static Color[] listColor = { Color.Red, Color.Orange, Color.HotPink, Color.Green, Color.Blue, Color.Purple, Color.Gray };
        private Connection.ReadDBConfig dbconfig = new Connection.ReadDBConfig();
        private int CableID = 0;
        private Connection.Connection conn;
        private ListOrder mListOrder = new ListOrder();
        private ListOrderTemp mListOrderTemp = new ListOrderTemp();
        public Class.ProcessOrderNew.Order order;
        private Class.ProcessOrderNew processOrder;
        private BarPrinterServer printServer;
        private frmCustomerDisplay frmCustomer;
        private SerialPort.SerialPort port;
        private Class.Functions fun = new Class.Functions();
        public Barcode.SerialPort mSerialPort;
        private System.IO.Ports.SerialPort mSerialPort_FuelAuto;
        private Class.MoneyFortmat money;

        public Class.MoneyFortmat Money { get { return money; } }

        public int OrderIDTemp { get; set; }

        public Connection.ReadDBConfig config { get; set; }

        private delegate void ChangeColorCallbackt(Color color, Control control);

        private delegate void ChangeForceColorCallbackt(Color color, Control control);

        private delegate void ChangeTextCallback(string text, Control control);

        private delegate void ChangeEnableCallback(bool enable, Control control);

        private Class.Setting setprinter;
        private System.Threading.Thread mThreadServerClient;
        private SocketServerClient.SocketClient mSocketClient;
        private bool mKeyThreadServerClient = true;
        public Class.CustomerDisplayVFD mCustomerDisplay;
        public Class.Shifts mShift;
        private Class.FuelAutoProtocol mFuelAutoProtocol;
        private Class.ReadConfig readconfig = new ReadConfig();
        private Group mGroup;
        private Item mItem;
        private ItemLine mLine;
        private int mIndex = 0;
        private int zIndex = 0;
        private int yIndex = 0;
        private int mGroupIndex = 0;
        private int mGroupShortCut = 0;
        private bool mLocketText = true;
        private Class.ProcessOrderNew.Item mItemOrder;

        public frmOrdersAllQuickSales()
        {
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            InitializeComponent();
            txt = new TextBox();
            conn = new Connection.Connection();
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

            if (readconfig.FastFood == "true")
            {
                btnoption.Enabled = true;
            }
            else
            {
                btnoption.Text = "";
            }

            try
            {
                mSerialPort = new Barcode.SerialPort();
                mSerialPort.OpenAndStart();//Dung cho barcode
            }
            catch (Exception)
            {
            }

            //mSerialPort_FuelAuto = new System.IO.Ports.SerialPort();
            //OpenAndStart_AutoFuel_T24();

            printServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            try
            {
                CableID = Convert.ToInt32(dbconfig.CableID);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Load Cable ID :::" + ex.Message);
                CableID = 1;
            }
            Init();
            initMenuButton();
        }

        private class Group
        {
            public string GroupID { get; set; }

            public string GroupName { get; set; }

            public int Visual { get; set; }

            public string GroupID_Main { get; set; }
        }

        private class Item
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }

            public double ItemPrice { get; set; }

            public string ScanCode { get; set; }

            public double Price1 { get; set; }

            public double Price2 { get; set; }

            public double GST { get; set; }

            public int EnableFuelDiscount { get; set; }

            public int SellSize { get; set; }

            public int IsFuel { get; set; }
        }

        private class ItemLine
        {
            public string lineID { get; set; }

            public string lineName { get; set; }
        }

        private void LoadListView(Class.ProcessOrderNew.Order lorder)
        {
            mLocketText = true;
            listView1.Items.Clear();
            if (frmCustomer != null)
            {
                frmCustomer.ProcessOrder(order);
            }
            txtorderid.Text = order.OrderID + "";
            if (order.PagerNo > 0)
            {
                txtPort.Text = order.PagerNo + "";
            }
            else
            {
                txtPort.Text = "";
            }
            if (order.TableID.Contains("TKA-"))
            {
                txttableid.Text = "";
            }
            else
            {
                txttableid.Text = order.TableID + "";
            }
            //order.shortItem();
            for (int i = 0; i < lorder.ListItem.Count; i++)
            {
                Class.ProcessOrderNew.Item item = lorder.ListItem[i];
                ListViewItem li;
                //if (item.ItemType==1 && item.Qty==0 && item.Price==0)
                //{
                //    li = new ListViewItem("");
                //    li.SubItems.Add(item.ItemName);
                //    li.SubItems.Add("");
                //    li.Tag = new ItemClick(-1, -1);
                //    li.EnsureVisible();
                //    listView1.Items.Add(li);
                //}
                if (item.ItemType == 1 && item.Qty == 0 && item.Price == 0)
                {
                    li = new ListViewItem("");
                }
                else
                {
                    li = new ListViewItem(item.Qty + "");
                }

                li.SubItems.Add(item.ItemName);
                if (item.ItemType == 1 && item.Qty == 0 && item.Price == 0)
                {
                    li.SubItems.Add("");
                }
                else
                {
                    li.SubItems.Add(money.Format(item.SubTotal));
                }

                li.Tag = new ItemClick(i, -1);
                li.EnsureVisible();
                listView1.Items.Add(li);
                for (int j = 0; j < item.ListSubItem.Count; j++)
                {
                    Class.ProcessOrderNew.SubItem sub = item.ListSubItem[j];
                    li = new ListViewItem(sub.Qty + "");
                    li.SubItems.Add(sub.ItemName);
                    li.SubItems.Add(money.Format(sub.SubTotal));
                    li.Tag = new ItemClick(i, j);
                    listView1.Items.Add(li);
                    li.EnsureVisible();
                }
            }
            if (listView1.Items.Count > 0)
            {
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
            TotalLabel();
            mLocketText = false;
        }

        private void initMenuButton()
        {
            for (int i = 0; i < 7; i++)
            {
                int bgcolor = i;
                Button btn = new Button();
                btn.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
                btn.ForeColor = Color.LightGray;
                btn.Width = 150;
                btn.Height = 100;
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                //btn.BackColor = listColor[i];
                tlp_groups.Controls.Add(btn);
                btn.Click += new EventHandler(btn_Click);
            }

            for (int i = 0; i < 21; i++)
            {
                Button btni = new Button();
                btni.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
                //btni.ForeColor = Color.LightGray;
                btni.Width = 100;
                btni.Height = 100;
                btni.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tlp_items.Controls.Add(btni);
                btni.Click += new EventHandler(btni_Click);
                btni.Enabled = false;
            }
            mGroupShortCut = 2;
            mIndex = 0;
            LoadGroup();

            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                mGroup.GroupID = tlp_groups.Controls[mGroupIndex].Name;
                LoadItem(mGroup.GroupID);
            }
        }

        private void btni_Click(object sender, EventArgs e)
        {
            if (order.Completed != 0)
            {
                lblStatus.Text = "Order completed";
                return;
            }
            try
            {
                Button btn = (Button)sender;
                if (btn.Name == "Item")
                {
                    txtqty.Focus();
                    mItemOrder = ((Class.ProcessOrderNew.Item)btn.Tag).Coppy();
                    mCustomerDisplay.WriteLine(mItemOrder.ItemName, money.Format(mItemOrder.Price));
                    if (mItemOrder.OptionCount > 0)
                    {
                        LoadLineItem(mItemOrder.ItemID + "");
                    }
                    else
                    {
                        Addnewitems(mItemOrder);
                    }
                }
                else
                {
                    txtqty.Focus();
                    Class.ProcessOrderNew.SubItem sub = (Class.ProcessOrderNew.SubItem)btn.Tag;
                    mCustomerDisplay.WriteLine(sub.ItemName, money.Format(sub.Price));
                    //order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(new Class.ProcessOrderNew.SubItem(sub.ItemID, sub.ItemName, sub.Qty, sub.SubTotal, sub.Price));
                    //mItemOrder.addSubItem(sub);
                    order.AddSubItem(mItemOrder, sub);
                    if (readconfig.EnableMultiOption == "0")
                    {
                        Addnewitems(mItemOrder);
                        LoadItem(mGroup.GroupID);
                    }
                    else
                    {
                        order.RemoveItem(mItemOrder.KeyItem, listView1, money, 0);
                        Addnewitems(mItemOrder);
                    }
                }
                //LoadListView(order);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(ex.Message);
            }
        }

        private void LoadFirstGroup()
        {
            mGroupShortCut = 2;
            mIndex = 0;
            LoadGroup();

            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                mGroup.GroupID = tlp_groups.Controls[mGroupIndex].Name;
                LoadItem(mGroup.GroupID);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            btnoption.Tag = null;
            Button btn = (Button)sender;
            zIndex = 0;
            mGroupIndex = tlp_groups.Controls.GetChildIndex(btn);
            if (LoadItem(btn.Name) == false)
            {
                for (int i = 0; i < 21; i++)
                {
                    tlp_items.Controls[i].Text = "";
                    tlp_items.Controls[i].Tag = null;
                    tlp_items.Controls[i].Enabled = false;
                    tlp_items.Controls[i].BackColor = Color.White;
                }
            }
            mGroup.GroupID = btn.Name;
        }

        private void LoadGroup(string groupID)
        {
            try
            {
                if (mIndex < 0)
                {
                    mIndex = 0;
                }
                DataTable dt = conn.Select("SELECT groupID,groupDesc FROM groupsmenu where groupID >=" + groupID + " and visual=0 and (0=" + mGroupShortCut + " or grShortCut=" + mGroupShortCut + ") order by displayOrder limit " + mIndex + ",7");
                if (dt.Rows.Count <= 0)
                {
                    mIndex -= 7;
                    return;
                }

                mGroup.GroupID = dt.Rows[0]["groupID"].ToString();
                mGroup.GroupName = dt.Rows[0]["groupDesc"].ToString();
                mItem = new Item();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_groups.Controls[i].Tag = dr["groupID"].ToString();
                    tlp_groups.Controls[i].Text = dr["groupDesc"].ToString();
                    tlp_groups.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 7; i++)
                {
                    tlp_groups.Controls[i].Text = "";
                    tlp_groups.Controls[i].Tag = null;
                    tlp_groups.Controls[i].Enabled = false;
                }
                //LoadDetailGroup(mGroup.GroupID);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmMenuChange.LoadGroup:::" + ex.Message);
            }
        }

        //Load group
        private void LoadGroup()
        {
            try
            {
                btnoption.Tag = null;
                if (mIndex < 0)
                {
                    mIndex = 0;
                }
                DataTable dt = conn.Select("SELECT groupID,groupShort FROM groupsmenu where deleted<>1 and `visual`=0 and isFF=1 and grShortCut in (2,3,4) order by displayOrder limit " + mIndex + ",7");
                if (dt.Rows.Count <= 0)
                {
                    mIndex -= 7;
                    return;
                }
                mGroup.GroupID = dt.Rows[0]["groupID"].ToString();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string btn_text = Convert.ToString(dr["groupShort"]);
                    string btn_name = Convert.ToString(dr["groupID"]);
                    tlp_groups.Controls[i].Name = btn_name;
                    tlp_groups.Controls[i].Text = btn_text;
                    tlp_groups.Controls[i].Enabled = true;
                    tlp_groups.Controls[i].BackColor = listColor[i];
                }
                for (int i = dt.Rows.Count; i < 7; i++)
                {
                    tlp_groups.Controls[i].Text = "";
                    tlp_groups.Controls[i].Tag = null;
                    tlp_groups.Controls[i].Enabled = false;
                    tlp_groups.Controls[i].BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmOrderAll.LoadGroup:::" + ex.Message);
            }
        }

        private int chkoption = 0;

        //load item
        private bool LoadItem(string groupID)
        {
            int grShortCut = 0;
            if (zIndex < 0)
            {
                zIndex = 0;
            }
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                grShortCut = Convert.ToInt16(conn.ExecuteScalar("select grShortCut from groupsmenu where groupID=" + groupID));
                int enable = Convert.ToInt16(conn.ExecuteScalar("select count(loyaltyID) from loyaltyscheme where `enable`=1"));
                string sql = "select i.itemID,i.gst,i.itemShort,i.unitPrice,(select count(il.lineID) from itemslinemenu il where il.itemID=i.itemID) as optionCount from itemsmenu i where deleted<>1 and groupID=" + groupID + " order by displayOrder limit " + zIndex + ",21";
                if (enable > 0)
                {
                    sql = "select i.itemID,i.gst,i.itemShort,(i.unitPrice*1.1) as unitPrice,(select count(il.lineID) from itemslinemenu il where il.itemID=i.itemID) as optionCount from itemsmenu i where deleted<>1 and groupID=" + groupID + " order by displayOrder limit " + zIndex + ",21";
                }
                dt = conn.Select(sql);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmOrderAll.LoadItem:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }

            //if (dt.Rows.Count <= 0 && zIndex>0)
            if (dt.Rows.Count <= 0)
            {
                zIndex -= 21;
                return false;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0,
                                                                                Convert.ToInt32(dr["itemID"].ToString()),
                                                                                dr["itemShort"].ToString(),
                                                                                1,
                                                                                Convert.ToDouble(dr["unitPrice"].ToString()),
                                                                                Convert.ToDouble(dr["unitPrice"].ToString()),
                                                                                Convert.ToDouble(dr["gst"].ToString()),
                                                                                0,
                                                                                Convert.ToInt32(dr["optionCount"].ToString()),
                                                                                intPumpID.ToString()
                                                                                );
                if (grShortCut == 1)
                {
                    item.IsOption = true;
                }
                tlp_items.Controls[i].Text = item.ItemName;
                tlp_items.Controls[i].Tag = item;
                tlp_items.Controls[i].Name = "Item";
                tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                tlp_items.Controls[i].Enabled = true;
            }
            for (int i = dt.Rows.Count; i < 21; i++)
            {
                tlp_items.Controls[i].Text = "";
                tlp_items.Controls[i].Tag = null;
                tlp_items.Controls[i].BackColor = Color.White;
                tlp_items.Controls[i].Enabled = false;
            }
            yIndex = 0;
            chkoption = 0;
            return true;
        }

        //load line menu
        private void LoadLineItem(string itemID)
        {
            if (yIndex < 0)
            {
                yIndex = 0;
            }
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                int enable = Convert.ToInt16(conn.ExecuteScalar("select count(loyaltyID) from loyaltyscheme where `enable`=1"));
                string sql = "select lineID,itemShort,unitPrice from itemslinemenu where deleted<>1 and itemID=" + itemID + " limit " + yIndex + ",21";
                if (enable > 0)
                {
                    sql = "select lineID,itemShort,(unitPrice*1.1) as unitPrice from itemslinemenu where deleted<>1 and itemID=" + itemID + " limit " + yIndex + ",21";
                }
                dt = conn.Select(sql);
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
            if (dt.Rows.Count <= 0)
            {
                yIndex -= 21;
                return;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                string btn_text = Convert.ToString(dr["itemShort"]);
                string btn_name = Convert.ToString(dr["lineID"]);
                Class.ProcessOrderNew.SubItem sub = new Class.ProcessOrderNew.SubItem(
                                                    Convert.ToInt32(dr["lineID"].ToString()),
                                                    dr["itemShort"].ToString(),
                                                    1,
                                                    Convert.ToDouble(dr["unitPrice"].ToString()),
                                                    Convert.ToDouble(dr["unitPrice"].ToString())
                                                );
                tlp_items.Controls[i].Name = "SubItem";
                tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                tlp_items.Controls[i].Text = sub.ItemName;
                tlp_items.Controls[i].Tag = sub;
                tlp_items.Controls[i].Enabled = true;
            }
            for (int i = dt.Rows.Count; i < 21; i++)
            {
                tlp_items.Controls[i].Text = "";
                tlp_items.Controls[i].Tag = null;
                tlp_items.Controls[i].Enabled = false;
                tlp_items.Controls[i].BackColor = Color.White;
            }
            mItem.ItemID = itemID;
            chkoption = 1;
        }

        public string ConvertToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        private string data = "";
        private string strDataTemp = "";

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int ch = mSerialPort_FuelAuto.ReadChar();
                data += mSerialPort_FuelAuto.ReadExisting();
                strDataTemp = data;

                for (int i = 0; i < strDataTemp.Length; i++)
                {
                    if (ConvertToHex(strDataTemp[i].ToString()).Equals("02"))
                    {
                        strDataTemp = "";
                    }
                    else if (ConvertToHex(strDataTemp[i].ToString()).Equals("03"))
                    {
                    }
                    else if (!ConvertToHex(strDataTemp[i].ToString()).Equals("02") && !ConvertToHex(strDataTemp[i].ToString()).Equals("03"))
                    {
                        strDataTemp += strDataTemp[i].ToString();
                    }
                }

                //Cach xu ly cu
                chrCheckSum = Convert.ToChar(data.Substring(19, 1));
                chrCheckSum2 = Convert.ToChar(CheckSum(data));
                if (chrCheckSum.Equals(chrCheckSum2))
                {
                    POS.Class.LogPOS.WriteLog("SerialPort_T24_" + data);
                    ExcuteDateReceive(data);
                    mListOrderTemp.AddOrder(order);
                    data = data.Remove(0, 20);
                }
            }
            catch (Exception)
            {
                POS.Class.LogPOS.WriteLog("SerialPort_T24_" + data);
                data = "";
            }
        }

        private string strIDPumpHistory = "";
        private string strPumpNo = "";
        private string strGradeNo = "";
        private string strGradeName = "";
        private string strCashAmount = "";
        private string strVolumeAmount = "";
        private string strUnitPrice = "";
        private string strFromPump = "";
        private string strGST = "";
        private Char chrCheckSum;
        private Char chrCheckSum2;

        public Decimal DecimalFromChar(char argument)
        {
            Decimal decValue;
            decValue = argument;
            return decValue;
        }

        private Char DecimalToChar(decimal argument)
        {
            Char CharValue;
            CharValue = (Char)argument;
            return CharValue;
        }

        public char CheckSum(string strReceiveData)
        {
            Int32 LRC = 0;
            int intLth = strReceiveData.Length;
            for (int i = 0; i < intLth; i++)
            {
                Int32 dec = Convert.ToInt32(DecimalFromChar((Char)strReceiveData[i]));
                LRC = LRC ^ dec;
            }
            return DecimalToChar(Convert.ToDecimal(LRC));
        }

        private void ExcuteDateReceive(string strDataReceive)
        {
            Connection.Connection conNew = new Connection.Connection();
            try
            {
                //chrCheckSum = Convert.ToChar(strDataReceive.Substring(19, 1));
                //chrCheckSum2 = Convert.ToChar(CheckSum(strDataReceive));
                //if (chrCheckSum.Equals(chrCheckSum2))
                //{
                //NextCustomer();
                //[]01100354002801263[]=
                conNew.Open();
                conNew.BeginTransaction();
                strPumpNo = Convert.ToInt32(strDataReceive.Substring(1, 2)).ToString();
                strGradeNo = Convert.ToInt32(strDataReceive.Substring(3, 1)).ToString();
                strCashAmount = (Convert.ToDouble(strDataReceive.Substring(4, 5)) * 10).ToString();
                strVolumeAmount = (Convert.ToDouble(strDataReceive.Substring(9, 5)) * 10).ToString();
                strUnitPrice = (Convert.ToDouble(strDataReceive.Substring(14, 4))).ToString();

                //conn.Open();
                //string strQuery = "SELECT f.MenuItemID , i.unitPrice FROM fuel f left join itemsmenu i on f.MenuItemID = i.itemID where KindID = " + strGradeNo;
                //DataTable dtSource = conn.Select(strQuery);
                //strGradeNo = dtSource.Rows[0]["MenuItemID"].ToString();
                //conn.Close();

                DataRow[] foundRows = dtSourceKindOfFuel.Select("KindID = " + strGradeNo);
                strGradeNo = foundRows[0]["MenuItemID"].ToString();
                if (Convert.ToDouble(strDataReceive.Substring(14, 4)) != Convert.ToDouble(foundRows[0]["unitPrice"].ToString()))
                {
                    strQuery = "UPDATE itemsmenu SET unitPrice = '" + Convert.ToDouble(strDataReceive.Substring(14, 4)) + "' WHERE itemID = " + strGradeNo;
                    conNew.ExecuteNonQuery(strQuery);
                }

                strQuery = "INSERT INTO fuelhistory(KindOfFuel,PumID,CashAmount,VolumeAmount,UnitPrice,FromPump) VALUES(" + strGradeNo + "," + strPumpNo + "," + strCashAmount + "," + strVolumeAmount + "," + strUnitPrice + ",1)";
                conNew.ExecuteNonQuery(strQuery);

                string str = "SELECT fh.ID,fh.FromPump,fh.KindOfFuel,if(f.itemDesc = '','Unknow Kind Of Fuel',f.itemDesc) AS KindName,f.gst,fh.PumID,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice " +
                    //"FROM fuelhistory as fh left join fuel as f on fh.KindOfFuel = f.KindID "+
                             "FROM fuelhistory as fh LEFT JOIN itemsmenu f on fh.KindOfFuel=f.itemID " +
                             "where fh.ID = (SELECT MAX(ID) AS ID FROM fuelhistory)";
                dtSource = conNew.Select(str);

                strIDPumpHistory = dtSource.Rows[0]["ID"].ToString();
                strPumpNo = dtSource.Rows[0]["PumID"].ToString();
                strGradeNo = dtSource.Rows[0]["KindOfFuel"].ToString();
                strGradeName = dtSource.Rows[0]["KindName"].ToString();
                strVolumeAmount = dtSource.Rows[0]["VolumeAmount"].ToString();
                strCashAmount = dtSource.Rows[0]["CashAmount"].ToString();
                strUnitPrice = dtSource.Rows[0]["UnitPrice"].ToString();
                strFromPump = dtSource.Rows[0]["FromPump"].ToString();
                strGST = dtSource.Rows[0]["gst"].ToString();

                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
                    Convert.ToInt32(strIDPumpHistory),
                    Convert.ToInt32(strGradeNo),
                    strGradeName,
                    Convert.ToInt32(strVolumeAmount),
                    Convert.ToDouble(strCashAmount),
                    Convert.ToDouble(strUnitPrice),
                    Convert.ToDouble(strGST),
                    0, 0,
                    strPumpNo);

                item.Weight = 1000;
                item.IsFuel = true;
                item.PumID = strPumpNo;

                UCPump uct = (UCPump)flp_takeaway.Controls[strPumpNo];
                SetText3(money.Format2(Convert.ToDouble(dtSource.Rows[0]["VolumeAmount"])), uct.lbLitre);
                SetText2(money.Format2(Convert.ToDouble(dtSource.Rows[0]["CashAmount"])), uct.lbTotal);
                uct.Tag = item.IDPumpHistory;

                if (Convert.ToInt32(strFromPump) == 1)
                {
                    ChangeColort(Color.Blue, flp_takeaway.Controls[strPumpNo]);
                }
                else
                {
                    ChangeColort(Color.LimeGreen, flp_takeaway.Controls[strPumpNo]);
                }

                str = "SELECT COUNT(f.ID) FROM fuelhistory f where f.IsComplete = 0 AND f.PumID = " + strPumpNo;
                int intCountRecord = Convert.ToInt32(conNew.ExecuteScalar(str));

                if (intCountRecord != 1)
                {
                    ChangeColort(Color.Red, flp_takeaway.Controls[strPumpNo]);
                }
                //ChangeForceColor(Color.White, uct.label3);
                //AddListView(item);
                //}
                conNew.Commit();
            }
            catch (Exception ex)
            {
                conNew.Rollback();
                Class.LogPOS.WriteLog("ExcuteDateReceive::" + ex.Message);
            }
            finally
            {
                conNew.Close();
            }
        }

        private delegate void InfoMessageDel(ProcessOrderNew.Item item);

        private void AddListView(ProcessOrderNew.Item item)
        {
            if (listView1.InvokeRequired)
            {
                InfoMessageDel method = new InfoMessageDel(AddListView);
                listView1.Invoke(method, new object[] { item });
                return;
            }
            Addnewitems(item);
        }

        //delegate void InfoMessageDelsub(ProcessOrderNew.SubItem subitem);
        //private void AddListView(ProcessOrderNew.SubItem subitem)
        //{
        //    if (listView1.InvokeRequired)
        //    {
        //        InfoMessageDel method = new InfoMessageDel(AddListView);
        //        listView1.Invoke(method, new object[] { subitem });
        //        return;
        //    }
        //    Addnewitems(subitem);
        //}

        private delegate void ClearListview();

        private void ClearListView()
        {
            if (listView1.InvokeRequired)
            {
                ClearListview method = new ClearListview(ClearListView);
                listView1.Invoke(method);
                return;
            }
            listView1.Items.Clear();
        }

        public void Addnewitems2(Class.ProcessOrderNew.Item item)
        {
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            //order.ListItem.Add(item);
            Class.ProcessOrderNew.Item itemCus = order.AddItem(item, listView1, money, true);
            SendToCustomerDisplay(itemCus);
            //order.CheckLoyaltyByListItem(listView1, money);
            order.CheckFuelDiscount(listView1, money);
            TotalLabel();
            //LoadListView(order);
            //order.AddItemToList(item, listView1, money);
        }

        private void ChangeTakeAway()
        {
            try
            {
                List<Class.Functions.TakeAway> list = fun.GetStatusTakeAway();
                for (int i = 0; i < list.Count; i++)
                {
                    try
                    {
                        Class.Functions.TakeAway tk = list[i];
                        UCPostaw uc = (UCPostaw)flp_takeaway.Controls[i];
                        uc.Tag = tk.tableID;
                        SetText(tk.orderid + "", uc.lblOrderID);
                        SetText(money.Format(tk.SubTotal), uc.lblSubTotal);
                        if (tk.complete == 0)
                        {
                            ChangeColort(Color.Blue, flp_takeaway.Controls[i]);
                        }
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("ChangeTakeAway()Sub::" + ex.Message);
                    }
                }
                for (int i = list.Count; i < flp_takeaway.Controls.Count; i++)
                {
                    UCPostaw uc = (UCPostaw)flp_takeaway.Controls[i];
                    uc.Tag = null;
                    SetText("", uc.lblOrderID);
                    SetText("", uc.lblSubTotal);
                    ChangeColort(Color.White, flp_takeaway.Controls[i]);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ChangeTakeAway::" + ex.Message);
            }
        }

        public frmOrdersAllQuickSales(int intNoneOfShift)
        {
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            InitializeComponent();
            conn = new Connection.Connection();
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPort = new Barcode.SerialPort();
            mSerialPort.OpenAndStart();

            printServer = new BarPrinterServer(new Printer(), conn, new SocketServer(), money);
            try
            {
                CableID = Convert.ToInt32(dbconfig.CableID);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Load Cable ID :::" + ex.Message);
                CableID = 1;
            }
            Init(intNoneOfShift);
            initMenuButton();
        }

        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private Connection.ReadDBConfig mReadDBConfig;

        private void prinCreditNote()
        {
            try
            {
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageCreditNote);
                mSetting = new Class.Setting();
                mReadDBConfig = new Connection.ReadDBConfig();
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("prinCreditNote::" + ex.Message);
            }
        }

        private void printDocument_PrintPageCreditNote(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string strQuery = "SELECT balance,creditNoteCode FROM creditnote where creditNoteCode = '" + frmSubTotal.strCreditCode + "'";
            DataTable dtSource = conn.Select(strQuery);

            float y = 0;
            y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            DateTime dateTime = DateTime.Now;
            y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);

            y += 100;

            y = mPrinter.DrawString("CREDIT NOTE", e, new Font("Arial", 18, FontStyle.Bold), y, 2);

            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            y = mPrinter.DrawString("Code # " + dtSource.Rows[0]["creditNoteCode"].ToString(), e, new Font("Arial", 11), y, 1);
            y = mPrinter.DrawString("Amount : $" + money.Format(Convert.ToDouble(dtSource.Rows[0]["balance"])), e, new Font("Arial", 11), y, 1);
        }

        public frmOrdersAllQuickSales(BarPrinterServer bar, Connection.Connection con, Class.MoneyFortmat m, Barcode.SerialPort sport)
        {
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            InitializeComponent();
            mSerialPort = sport;
            money = m;
            printServer = bar;
            conn = con;
            Init();
            initMenuButton();
        }

        private void LoadDepartment()
        {
            Class.ConfigDepartment configDe = new Class.ConfigDepartment();
            btnDepartment1.Text = configDe.Department1.SortName;
            btnDepartment1.Tag = configDe.Department1;

            btnDepartment2.Text = configDe.Department2.SortName;
            btnDepartment2.Tag = configDe.Department2;

            btnDepartment3.Text = configDe.Department3.SortName;
            btnDepartment3.Tag = configDe.Department3;

            btnDepartment4.Text = configDe.Department4.SortName;
            btnDepartment4.Tag = configDe.Department4;

            //btnDepartment5.Text = configDe.Department5.SortName;
            //btnDepartment5.Tag = configDe.Department5;

            //btnDepartment6.Text = configDe.Department6.SortName;
            //btnDepartment6.Tag = configDe.Department6;

            //btnDepartment7.Text = configDe.Department7.SortName;
            //btnDepartment7.Tag = configDe.Department7;

            //btnDepartment8.Text = configDe.Department8.SortName;
            //btnDepartment8.Tag = configDe.Department8;
        }

        public void SetShift(int shiftID)
        {
            if (order != null)
            {
                order.ShiftID = shiftID;
            }
        }

        public void Init()
        {
            txtunitprice.ucKeypad = uCkeypad1;
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            mSerialPort.Received += new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received);

            mUCInfoTop = new UCInfoTop();
            mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
            mUCInfoTop.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(mUCInfoTop);
            processOrder = new Class.ProcessOrderNew(conn, printServer);
            setprinter = new Class.Setting();
            printServer.BarName = setprinter.getbarprinter();
            //printServer.KitchenName = setprinter.getkitchenprinter();
            printServer.KitchenName = setprinter.getkitchenprinter();
            printServer.BillName = setprinter.GetBillPrinter();
            mShift = new Shifts();
            LoadShift();
            LoadDepartment();
            Class.FuelDiscountConfig.GetMaxQtyDiscount();
            //threadChangeColort = new System.Threading.Thread(runChangeColort);
            //threadChangeColort.Start();
            try
            {
                port = new SerialPort.SerialPort();
                //port_T24 = new SerialPort.SerialPort();
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
            //initMenuButton();
            listView1.SelectedIndexChanged += new EventHandler(listView1_SelectedIndexChanged);
            ChangeLocalModeLabel();
            //conn.ChangeLocalMode += new Connection.Connection.ChangeLocalModeEventHandler(conn_ChangeLocalMode);
            //conn_ChangeLocalMode("");
            if (conn.GetCableID() == "1")
            {
                mUCInfoTop.lbServerMode.Visible = false;
            }
            else
            {
                mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                mThreadServerClient.Start();
            }
            try
            {
                mCustomerDisplay = new Class.CustomerDisplayVFD();
            }
            catch (Exception)
            {
            }
            //for (int i = 0; i < 7; i++)
            //{
            //    tlp_groups.Controls[i].Text = list[i].itemName;
            //    tlp_groups.Controls[i].Tag = list[i];
            //}
            //ShowCustomerDisplay();
            txt = txtBarcode;
            try
            {
                mFuelAutoProtocol = new FuelAutoProtocol();
                mFuelAutoProtocol.Received += new FuelAutoProtocol.FuelAutoEvenHandler(mFuelAutoProtocol_Received);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Init:::" + ex.Message);
            }
        }

        private void mFuelAutoProtocol_Received(FuelAutoProtocol.FuelData data)
        {
            char chekSum = CheckSum(data.Data + data.End);
            Class.LogPOS.WriteLog("Checksum::" + chekSum + "::" + CheckSum(data.Data) + "::" + data.CheckSum);
            if (chekSum == data.CheckSum)
            {
                POS.Class.LogPOS.WriteLog("SerialPort_T24_" + data);
                ExcuteDateReceive(data.Start + data.Data + data.End + data.CheckSum);
                mListOrderTemp.AddOrder(order);
            }
        }

        public void Init2()
        {
            mUCInfoTop = new UCInfoTop();
            mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
            mUCInfoTop.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(mUCInfoTop);
            mShift = new Shifts();
            LoadShift();
            LoadDepartment();
            listView1.SelectedIndexChanged += new EventHandler(listView1_SelectedIndexChanged);
            ChangeLocalModeLabel();
            if (conn.GetCableID() == "1")
            {
                mUCInfoTop.lbServerMode.Visible = false;
            }
            else
            {
                mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                mThreadServerClient.Start();
            }
            try
            {
                mCustomerDisplay = new Class.CustomerDisplayVFD();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Init2::" + ex.Message);
            }
        }

        private void Init(int intNoneOfShift)
        {
            try
            {
                txtunitprice.ucKeypad = uCkeypad1;
                mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
                mSerialPort.Received += new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received);
                mUCInfoTop = new UCInfoTop();
                mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
                mUCInfoTop.Dock = DockStyle.Fill;
                panel4.Controls.Clear();
                panel4.Controls.Add(mUCInfoTop);
                processOrder = new Class.ProcessOrderNew(conn, printServer);
                setprinter = new Class.Setting();
                printServer.BarName = setprinter.getbarprinter();
                //printServer.KitchenName = setprinter.getkitchenprinter();
                printServer.KitchenName = setprinter.GetBillPrinter();
                printServer.BillName = setprinter.GetBillPrinter();
                mShift = new Shifts(intNoneOfShift);
                LoadShift();
                LoadDepartment();
                Class.FuelDiscountConfig.GetMaxQtyDiscount();
                //threadChangeColort = new System.Threading.Thread(runChangeColort);
                //threadChangeColort.Start();
                try
                {
                    port = new SerialPort.SerialPort();
                }
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                }
                //initMenuButton();
                listView1.SelectedIndexChanged += new EventHandler(listView1_SelectedIndexChanged);
                ChangeLocalModeLabel();
                //conn.ChangeLocalMode += new Connection.Connection.ChangeLocalModeEventHandler(conn_ChangeLocalMode);
                //conn_ChangeLocalMode("");
                if (conn.GetCableID() == "1")
                {
                    mUCInfoTop.lbServerMode.Visible = false;
                }
                else
                {
                    mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                    mThreadServerClient.Start();
                }
                try
                {
                    mCustomerDisplay = new Class.CustomerDisplayVFD();
                }
                catch (Exception)
                {
                }
                //for (int i = 0; i < 7; i++)
                //{
                //    tlp_groups.Controls[i].Text = list[i].itemName;
                //    tlp_groups.Controls[i].Tag = list[i];
                //}
                //ShowCustomerDisplay();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Init::" + ex.Message);
            }
        }

        public static string dataReceive;
        public TextBox txt;

        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.ORDER_ALL)
            {
                //if (data.Length>12)
                //{
                //    data = data.Substring(0, 12);
                //}
                mSerialPort.SetText("", txt);
                mSerialPort.SetText(data, txt);
            }
        }

        public void LoadShift()
        {
            mUCInfoTop.lblShift.Text = "Shift:" + mShift.ShiftID;
            mUCInfoTop.lblShift.Visible = Settings.Default.AllowUseShift;
        }

        public void SetStaffName(string name, int staffID, int per)
        {
            txtstaffid.Text = name;
            mStaffID = staffID;
            mStaffPermission = per;
        }

        private bool blnReset = false;

        private void ThreadServerClient()
        {
            while (mKeyThreadServerClient)
            {
                //mSocketClient = new SocketServerClient.SocketClient(12341, conn.GetServer());
                //string resuilt = mSocketClient.SendData("are you online",500);
                //if (resuilt!="ok")
                //{
                //    conn.ChangeLocalConnection(1);
                //    conn.SetConnectionString();
                //    ChangeLocalModeLabel();
                //    mKeyThreadServerClient = false;
                //}
                if (!PingToServer())
                {
                    if (blnReset == false)
                    {
                        frmMessageBox frm = new frmMessageBox("Information", "Not Connect to server " + dbconfig.Server + ",Do you want to retry?");
                        frm.ShowDialog();
                        if (frm.DialogResult != DialogResult.OK)
                        {
                            mKeyThreadServerClient = false;
                            Application.Exit();
                        }
                        else
                        {
                            blnReset = false;
                            //ChangeLocalModeLabel();
                            SetText("OFFLINE", mUCInfoTop.lbServerMode);
                            ChangeForceColor(Color.Red, mUCInfoTop.lbServerMode);
                        }
                    }

                    //if (!PingToServer())
                    //{
                    //    if (!PingToServer())
                    //    {
                    //conn.ChangeLocalConnectiozzn(1);
                    //conn.SetConnectionString();
                    //ChangeLocalModeLabel();
                    ////button10.Enabled = false;
                    ////buttonRefun.Enabled = false;
                    ////button26.Enabled = false;
                    //ChangeEnable(false, btnMergeOrder);
                    //ChangeEnable(false, buttonRefun);
                    //ChangeEnable(false, btnoption);
                    //mKeyThreadServerClient = false;
                    //    }
                    //}
                }
                else
                {
                    ChangeLocalModeLabel();
                }

                System.Threading.Thread.Sleep(100);
            }
        }

        private bool PingToServer()
        {
            mSocketClient = new SocketServerClient.SocketClient(12341, conn.GetServer());
            string resuilt = mSocketClient.SendData("are you online", 500);
            if (resuilt != "ok")
            {
                return false;
            }
            return true;
        }

        private void ChangeLocalModeLabel()
        {
            if (conn.CheckLocalhost())
            {
                //mUCInfoTop.lblServerMode.Text = "OFFLINE";
                SetText("OFFLINE", mUCInfoTop.lbServerMode);
                //mUCInfoTop.lblServerMode.ForeColor = Color.Red;
                ChangeForceColor(Color.Red, mUCInfoTop.lbServerMode);
            }
            else
            {
                //mUCInfoTop.lblServerMode.Text = "ONLINE";
                //mUCInfoTop.lblServerMode.ForeColor = Color.Green;
                SetText("ONLINE", mUCInfoTop.lbServerMode);
                ChangeForceColor(Color.Green, mUCInfoTop.lbServerMode);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int second = 5;
            if (lblStatus.POSTimeMessenge >= 1 && lblStatus.POSTimeMessenge <= second)
            {
                lblStatus.POSTimeMessenge++;
            }

            if (lblStatus.POSTimeMessenge == (second + 1))
            {
                lblStatus.Text = "";
                lblStatus.POSTimeMessenge = 0;
            }
            //if (Settings.Default.AllowUseShift)
            //{
            //    //DateTime dtDate = DateTime.Now;
            //    //if (Convert.ToInt32(dbconfig.Hour) == Convert.ToInt32(dtDate.Hour) && Convert.ToInt32(dbconfig.Minute) == Convert.ToInt32(dtDate.Minute) && Convert.ToInt32(dbconfig.Second) == Convert.ToInt32(dtDate.Second) && Convert.ToInt32(txtorderid.Text.ToString()) != 1)
            //    //{
            //    //    UpdateToServer();
            //    //}
            //}
            //if (Convert.ToInt32(dbconfig.Hour) == Convert.ToInt32(DateTime.Now.Hour) && Convert.ToInt32(dbconfig.Minute) == Convert.ToInt32(DateTime.Now.Minute))
            //{
            //    if (frmShiftReport.blnActive == false && frmShiftReport.blnHasEndShift == false)
            //    {
            //        frmShiftReport frmshift = new frmShiftReport(money, this, mShift);
            //        frmshift.ShowDialog();
            //    }
            //}

            //=========================
            //if (!Settings.Default.AllowUseShift )
            //{
            //    try
            //    {
            //        //blnMessageBoxActive = true;
            //        System.Data.DataTable tbl = conn.Select("select orderID from ordersdaily where date(ts)<date(now())");
            //        if (tbl.Rows.Count != 0)
            //        {
            //            Class.UpdateToServer.UpDatabaseToServer(conn);
            //            txtorderid.Text = "1";
            //        }
            //        //blnMessageBoxActive = false;
            //    }
            //    catch (Exception)
            //    {
            //            Ping ping = new Ping();
            //            PingReply pingReply = ping.Send(dbconfig.Server);
            //            if (pingReply.Status.ToString().Equals("TimedOut") && blnMessageBoxActive == false)
            //            {
            //                frmMessageBox frm = new frmMessageBox("Information", "Don't Connect to server " + dbconfig.Server + ",Do you want to retry?");
            //                blnMessageBoxActive = true;
            //                frm.ShowDialog();
            //                if (frm.DialogResult == DialogResult.OK)
            //                {
            //                    blnMessageBoxActive = false;
            //                }
            //                else
            //                {
            //                    Application.Exit();
            //                }
            //            }
            //            else
            //            {
            //                conn.ChangeLocalConnection(0);
            //                conn.SetConnectionString();
            //                ChangeLocalModeLabel();
            //            }
            //        //        if (blnReset == false)
            //        //        {
            //        //            frmMessageBox frm = new frmMessageBox("Information", "Don't Connect to server " + dbconfig.Server + ",Do you want to retry?");
            //        //            frm.ShowDialog();
            //        //            if (frm.DialogResult != DialogResult.OK)
            //        //            {
            //        //                Application.Exit();
            //        //            }
            //        //            else
            //        //            {
            //        //                blnReset = false;
            //        //            }
            //        //        }
            //    }
            //}
            //============================
            //Tuan rao 18-10-2012
            //ping = new Ping();
            //PingReply pingReply = ping.Send(dbconfig.Server);
            //if (pingReply.Status.ToString().Equals("Success"))
            //{
            //    conn.ChangeLocalConnection(0);
            //    conn.SetConnectionString();
            //    ChangeLocalModeLabel();
            //}
            //else if (pingReply.Status.ToString().Equals("TimedOut"))
            //{
            //    conn.ChangeLocalConnection(1);
            //    conn.SetConnectionString();
            //    ChangeLocalModeLabel();
            //}
        }

        //Ping ping;
        private bool blnMessageBoxActive = false;

        private void UpdateToServer()
        {
            Class.UpdateToServer.UpDatabaseToServer(conn, new Class.ReadConfig());
            //send mail==============================
            POS.Class.SendMail sendMail = new SendMail();
            sendMail.CreateMessageThread();
            //=======================================
            //frmShiftReport frmshift = new frmShiftReport(money, this, this.mShift, 1);
            //frmshift.ShowDialog();
            Init2();
            txtorderid.Text = "1";
        }

        private void ShowCustomerDisplay()
        {
            try
            {
                Screen[] sc;
                sc = Screen.AllScreens;
                if (sc.Length >= 2)
                {
                    //get all the screen width and heights
                    frmCustomer = new frmCustomerDisplay(money);
                    //f.FormBorderStyle = FormBorderStyle.None;
                    frmCustomer.Left = sc[1].Bounds.Width;
                    frmCustomer.Top = sc[1].Bounds.Height;
                    frmCustomer.StartPosition = FormStartPosition.Manual;
                    frmCustomer.Location = sc[1].Bounds.Location;
                    Point p = new Point(sc[1].Bounds.Location.X, sc[1].Bounds.Location.Y);
                    frmCustomer.Location = p;
                    frmCustomer.WindowState = FormWindowState.Maximized;
                    frmCustomer.Show();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ShowCustomerDisplay::" + ex.Message);
            }
        }

        //// khai báo 1 hàm delegate
        //public delegate void GetString(String name, int qty,int price);
        //// khai báo 1 kiểu hàm delegate
        //public GetString MyGetData;
        //public string getTakeAwayTable()
        //{
        //    foreach (UCPostaw uc in flp_takeaway.Controls)
        //    {
        //        if (uc.BackColor==Color.White)
        //        {
        //            return uc.Tag.ToString();
        //        }
        //    }
        //    return "";
        //}
        //public void loadOrder(int orderID)
        //{
        //    order = processOrder.getOrderByOrderID(orderID);
        //    if (order == null)
        //    {
        //        order = new Class.ProcessOrderNew.Order();
        //        order.TableID = "";
        //        order.CableID = CableID;
        //        try
        //        {
        //            conn.Open();
        //            order.OrderID = Convert.ToInt32(conn.ExecuteScalar("select max(orderID) from ordersdaily").ToString()) + 1;
        //        }
        //        catch (Exception)
        //        {
        //        }
        //        finally
        //        {
        //            conn.Close();
        //        }

        //    }
        //    LoadListView(order);
        //}
        public void loadOrder(string table, int isClearListViewOrder)
        {
            order = null;
            //order= processOrder.getOrderByTable(table);
            if (order == null)
            {
                order = new Class.ProcessOrderNew.Order();
                order.TableID = table;
                order.CableID = CableID;
                order.StaffID = mStaffID;
                try
                {
                    conn.Open();
                    order.OrderID = Convert.ToInt32(conn.ExecuteScalar("select if(max(orderID) is null,0,max(orderID)) from ordersdaily").ToString()) + 1;
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmOrderAll.loadOrder_1:::" + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                if (table == "")
                {
                    order.TableID = "TKA-" + order.OrderID;
                }
            }
            LoadListView(order, isClearListViewOrder);
            order.ShiftID = mShift.ShiftID;
        }

        private void LoadListView(Class.ProcessOrderNew.Order lorder, int isClearListViewOrder)
        {
            ClearListView();
            mLocketText = true;
            listView1.Items.Clear();
            if (isClearListViewOrder == 1)
                //lstOrderOfPump.Items.Clear();
                if (frmCustomer != null)
                {
                    frmCustomer.ProcessOrder(order);
                }
            txtorderid.Text = order.OrderID + "";
            if (order.TableID.Contains("TKA-"))
            {
                txttableid.Text = "";
            }
            else
            {
                txttableid.Text = order.TableID + "";
            }
            //order.shortItem();
            //double sumTotal = 0;
            for (int i = 0; i < lorder.ListItem.Count; i++)
            {
                Class.ProcessOrderNew.Item item = lorder.ListItem[i];
                string qty = item.Qty + "";
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    qty = money.FormatCurenMoney((double)item.Qty / item.Weight);
                }
                ListViewItem li = new ListViewItem(qty);
                li.SubItems.Add(item.ItemName);
                li.SubItems.Add(money.Convert3to2(Convert.ToDouble(money.Format(item.SubTotal))).ToString());
                li.SubItems.Add(item.PumID.ToString());
                li.Tag = item.KeyItem;
                li.EnsureVisible();
                li.Selected = true;
                listView1.Items.Add(li);
                for (int j = 0; j < item.ListSubItem.Count; j++)
                {
                    Class.ProcessOrderNew.SubItem sub = item.ListSubItem[j];
                    //sumTotal += sub.Price * sub.Qty;
                    li = new ListViewItem(sub.Qty + "");
                    li.SubItems.Add(sub.ItemName);
                    li.SubItems.Add(money.Format(sub.SubTotal));
                    li.Tag = new ItemClick(i, j);
                    li.Selected = true;
                    listView1.Items.Add(li);
                    li.EnsureVisible();
                }
            }
            if (listView1.Items.Count > 0)
            {
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
            TotalLabel();
            mLocketText = false;
            txtqty_Enter(txtqty, null);
        }

        private bool blnFlagIfLoading;

        private void frmOrdersAll_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(dbconfig.CableID) != 1)
            {
                btnSubtotal.Enabled = false;
            }
            //Tuan khoa,tai vi update nhieu record qua,xay ra treo may
            //Class.UpdateToServer.UpDatabaseToServer(conn);
            txtqty_Enter(txtqty, null);
            if (dbconfig.AllowSalesFuel == 1)
            {
                blnFlagIfLoading = true;
                LoadFuelPump(1);
                LoadKindOfFuel();
                CountTotalPump();
                blnFlagIfLoading = false;
                NextCustomer();
                flp_takeaway.Enabled = true;
            }
            else
            {
                flp_takeaway.Enabled = false;
            }

            //btnSendOrder.Text = "";
            //btnListDelivery.Text = "";
            //btnBill.Text = "";
            //btnFindAcc.Text = "";
        }

        private void CountTotalPump()
        {
            try
            {
                intTotalPump = flp_takeaway.Controls.Count;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("CountTotalPump::" + ex.Message);
            }
        }

        public static DataTable dtSourceKindOfFuel;
        public int intTotalPump;

        private void LoadKindOfFuel()
        {
            try
            {
                conn.Open();
                string strQuery = "SELECT f.KindID , f.MenuItemID , i.unitPrice,f.PumpHeadID FROM fuel f inner join itemsmenu i on f.MenuItemID = i.itemID";
                dtSourceKindOfFuel = new DataTable();
                dtSourceKindOfFuel = conn.Select(strQuery);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadKindOfFuel::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private DataTable dtSource;

        private void LoadFuelPump(int intIsFirstLoad)
        {
            try
            {
                conn.Open();
                flp_takeaway.Controls.Clear();
                dtSource = new DataTable();
                int intCountRow = 0;
                string str;
                for (int i = 1; i <= 16; i++)
                {
                    UCPump uct = new UCPump();
                    str = "SELECT COUNT(ID) FROM fuelhistory fh where fh.IsComplete = 0 AND fh.PumID = " + i;

                    intCountRow = Convert.ToInt32(conn.ExecuteScalar(str));

                    str = "SELECT ID,PumID,VolumeAmount,CashAmount,FromPump FROM fuelhistory fh where fh.IsComplete = 0 AND fh.PumID = " + i;
                    dtSource = conn.Select(str);

                    uct.Name = "" + i;
                    uct.lbPumpID.Text = "" + i;
                    uct.lbPumpID.Name = "" + i;

                    if (dtSource.Rows.Count > 0)
                    {
                        uct.lbLitre.Text = money.Format(Convert.ToDouble(dtSource.Rows[0]["VolumeAmount"])) + " L";
                        uct.lbTotal.Text = "$ " + money.Format2(Convert.ToDouble(dtSource.Rows[0]["CashAmount"]));

                        if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 1 && intCountRow == 1)
                        {
                            uct.BackColor = Color.Blue;
                        }
                        else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 0 && intCountRow == 1)
                        {
                            uct.BackColor = Color.LimeGreen;
                        }
                        else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 1 && intCountRow != 1)
                        {
                            uct.BackColor = Color.Red;
                        }
                        else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 0 && intCountRow != 1)
                        {
                            uct.BackColor = Color.Red;
                        }
                        //uct.label3.ForeColor = Color.White;
                        uct.Tag = dtSource.Rows[0]["ID"];
                    }
                    else
                    {
                        uct.Tag = 0;
                        uct.lbLitre.Text = "0.000 L";
                        uct.lbTotal.Text = "$ 0.00";
                        uct.BackColor = Color.White;
                        uct.lbPumpID.ForeColor = Color.Black;
                    }

                    uct.Click += new EventHandler(uct_Click);

                    flp_takeaway.Controls.Add(uct);
                }
                blnIsOpen = false;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadFuelPump::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private bool blnIsOpen = true;

        private void uct_Click(object sender, EventArgs e)
        {
            ////if (order.ContainFuelItem() == false)
            ////{
            ////    UCPump uct = (UCPump)sender;
            ////    frmPump frm = new frmPump(uct.label3.Text, listView1, money, this);
            ////    frm.ShowDialog();
            ////}
            ////else
            ////{
            ////    lblStatus.Text = "Order exist fuel";
            ////}
            try
            {
                conn.Open();
                if (!blnIsOpen)
                {
                    string str = "";
                    UCPump uct = (UCPump)sender;
                    if (uct.BackColor == Color.Red)
                    {
                        str = "SELECT fh.ID,fh.VolumeAmount,im.itemShort AS Name,fh.CashAmount FROM fuelhistory fh LEFT JOIN itemsmenu im ON im.itemID = fh.KindOfFuel where fh.IsComplete = 0 AND fh.PumID = " + uct.lbPumpID.Text;
                        frmDetailPump frm = new frmDetailPump(conn, str, uct.lbPumpID.Text, money);
                        DialogResult dlg = frm.ShowDialog();
                        if (dlg == DialogResult.OK)
                        {
                            AddToOrder(frm.strFuelIsChose);
                        }

                        //DataTable dtSource = conn.Select(str);
                        //lstOrderOfPump.Items.Clear();
                        //foreach (DataRow drRow in dtSource.Rows)
                        //{
                        //    try
                        //    {
                        //        ListViewItem li = new ListViewItem(drRow["Name"].ToString());
                        //        li.SubItems.Add(money.Format(drRow["VolumeAmount"].ToString()));
                        //        li.SubItems.Add(money.Format(drRow["CashAmount"].ToString()));
                        //        li.Tag = drRow["ID"].ToString();
                        //        lstOrderOfPump.Items.Add(li);
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Class.LogPOS.WriteLog("uct_Click:::foreach:::" + ex.Message);
                        //    }
                        //}
                    }
                    else if ((uct.BackColor == Color.Blue || uct.BackColor == Color.LimeGreen) && uct.BackColor != Color.Red)
                    {
                        //lstOrderOfPump.Items.Clear();
                        LoadDetailInformationFuelPump(Convert.ToInt32(uct.lbPumpID.Text), uct.BackColor, Convert.ToInt32(uct.Tag));
                        //uct.label3.ForeColor = Color.White;
                    }
                    if (uct.BackColor == Color.White || uct.BackColor == Color.LimeGreen)
                    {
                        if (blnFlagIfLoading == false)
                        {
                            frmPump frm = new frmPump(uct.lbPumpID.Text, money, this);
                            if (frm.ShowDialog() == DialogResult.OK)
                            {
                                ChangeColort(Color.LimeGreen, flp_takeaway.Controls[Convert.ToString(uct.lbPumpID.Text)]);
                                //uct.label3.ForeColor = Color.White;
                                uct.lbLitre.Text = frm.strVolumeAmount + " L";
                                uct.lbTotal.Text = "$ " + frm.strCashAmount;
                                str = "SELECT COUNT(f.ID) FROM fuelhistory f where f.IsComplete = 0 AND f.PumID = " + Convert.ToString(uct.lbPumpID.Text);
                                int intCountRecord = Convert.ToInt32(conn.ExecuteScalar(str));
                                if (intCountRecord > 1)
                                {
                                    ChangeColort(Color.Red, flp_takeaway.Controls[Convert.ToString(uct.lbPumpID.Text)]);
                                }
                                //lstOrderOfPump.Items.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("uct_Click::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void AddToOrder(string strFuelIsChose)
        {
            try
            {
                conn.Open();
                if (strFuelIsChose != "")
                {
                    string strQuery = "SELECT ID,PumID,VolumeAmount,CashAmount,FromPump FROM fuelhistory fh where fh.ID IN (" + strFuelIsChose + ")";
                    dtSource = conn.Select(strQuery);
                    if (dtSource.Rows.Count > 0)
                    {
                        foreach (DataRow drItem in dtSource.Rows)
                        {
                            UCPump uc = (UCPump)flp_takeaway.Controls[Convert.ToInt32(drItem["PumID"]) - 1];
                            uc.Tag = drItem["ID"];
                            SetText(drItem["PumID"].ToString(), uc.lbPumpID);
                            SetText(money.Format(Convert.ToDouble(drItem["VolumeAmount"])) + " L", uc.lbLitre);
                            SetText("$ " + money.Format2(Convert.ToDouble(drItem["CashAmount"])), uc.lbTotal);
                            if (Convert.ToInt32(drItem["FromPump"]) == 1)
                                ChangeColort(Color.Blue, flp_takeaway.Controls[Convert.ToInt32(drItem["PumID"]) - 1]);
                            else
                                ChangeColort(Color.LimeGreen, flp_takeaway.Controls[Convert.ToInt32(drItem["PumID"]) - 1]);
                            LoadDetailInformationFuelPump(Convert.ToInt32(uc.lbPumpID.Text), uc.BackColor, Convert.ToInt32(uc.Tag));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("lstOrderOfPump_SelectedIndexChanged::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private int intPumpID;
        private string strQuery;
        private string strIDPumpTempHistory;

        private void LoadDetailInformationFuelPump(int intPumpID, Color color, int intTag)
        {
            try
            {
                //NextCustomer();
                //conn.Open();
                if ((color == Color.Blue || color == Color.LimeGreen) && intTag == 0)
                {
                    strQuery = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + intPumpID;
                }
                else if ((color == Color.Blue || color == Color.LimeGreen) && intTag != 0)
                {
                    strQuery = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + intPumpID + " AND fh.ID =" + intTag;
                }
                else if (color == Color.Red)
                {
                    strQuery = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,i.itemDesc AS KindName,i.gst,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice FROM fuelhistory AS fh LEFT JOIN itemsmenu i on fh.KindOfFuel=i.itemID WHERE fh.IsComplete = 0 AND fh.PumID =" + intPumpID + " AND fh.ID =" + intTag;
                }

                DataTable dtSource = conn.Select(strQuery);

                strIDPumpHistory = dtSource.Rows[0]["ID"].ToString();
                strGradeName = dtSource.Rows[0]["KindName"].ToString();

                foreach (ListViewItem it in listView1.Items)
                {
                    try
                    {
                        if (strIDPumpHistory.Equals(it.SubItems[3].Text.ToString()) && strGradeName.Equals(it.SubItems[1].Text.ToString()))
                            return;
                    }
                    catch (Exception)
                    {
                    }
                }

                strIDPumpTempHistory = strIDPumpHistory;
                strGradeName = dtSource.Rows[0]["KindName"].ToString();
                strGradeNo = dtSource.Rows[0]["KindOfFuel"].ToString();
                strCashAmount = dtSource.Rows[0]["CashAmount"].ToString();
                strVolumeAmount = dtSource.Rows[0]["VolumeAmount"].ToString();
                strUnitPrice = dtSource.Rows[0]["UnitPrice"].ToString();
                intPumpID = Convert.ToInt32(dtSource.Rows[0]["PumID"]);
                strGST = dtSource.Rows[0]["gst"].ToString();
                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
                    Convert.ToInt32(strIDPumpHistory),
                    Convert.ToInt32(strGradeNo),
                    strGradeName,
                    Convert.ToInt32(strVolumeAmount),
                    Convert.ToDouble(strCashAmount),
                    Convert.ToDouble(strUnitPrice),
                    Convert.ToDouble(strGST),
                    0, 0,
                    intPumpID.ToString());
                item.Weight = 1000;
                item.IsFuel = true;
                AddListView(item);

                //strQuery = "SELECT PumpID,ItemID,ItemName,Quantity,Subtotal,UnitPrice,GST,ChangeAmount,OptionCount,ItemType,EnableFuelDiscount,BarCode,pumpHistoryID FROM itemshistory where pumpHistoryID = " + strIDPumpHistory;
                //conn.Open();
                //dtSource = conn.Select(strQuery);
                //conn.Close();
                //if (dtSource.Rows.Count > 0)
                //{
                //    foreach (DataRow drItem in dtSource.Rows)
                //    {
                //        item = new Class.ProcessOrderNew.Item(
                //        Convert.ToInt32(drItem["pumpHistoryID"].ToString()),
                //        Convert.ToInt32(drItem["ItemID"].ToString()),
                //        drItem["ItemName"].ToString(),
                //        Convert.ToInt32(drItem["Quantity"].ToString()),
                //        Convert.ToDouble(drItem["Subtotal"].ToString()),
                //        Convert.ToDouble(drItem["UnitPrice"].ToString()),
                //        Convert.ToDouble(drItem["GST"].ToString()),
                //        Convert.ToInt32(drItem["ChangeAmount"].ToString()),
                //        Convert.ToInt32(drItem["OptionCount"].ToString()),
                //        drItem["PumpID"].ToString());
                //        item.ItemType = Convert.ToInt32(drItem["ItemType"].ToString());
                //        item.EnableFuelDiscount = Convert.ToInt32(drItem["EnableFuelDiscount"].ToString());
                //        item.BarCode = drItem["BarCode"].ToString();
                //        AddListView(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadDetailInformationFuelPump::" + ex.Message);
            }
            finally
            {
                //conn.Close();
            }
        }

        private bool Getorder(int orderid, int tableid)
        {
            return false;
        }

        private void button37_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            frmSearchBarCode frm = new frmSearchBarCode(mSerialPort, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), DataObject.FindItemType.FindItemInOrder);
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.CHANGE_MENU;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                this.Addnewitems(frm.ItemMenu);
            }
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
        }

        private void button36_Click(object sender, EventArgs e)
        {
            ReLoadGroup();

            #region MyRegion

            //order.shortItem();
            //Class.LogPOS.WriteLog("frmOrderAll.Button_Send Order_Click.");
            //if (order.Completed!=0)
            //{
            //    return;
            //}
            //if (order.ListItem.Count == 0)
            //{
            //    return;
            //}
            //if (textBox1.Text!="")
            //{
            //    order.NumPeople = Convert.ToInt32(textBox1.Text);
            //}

            //processOrder.SendOrder(order);
            //if (!order.TableID.Contains("TKA-"))
            //{
            //    this.Visible = false;
            //}

            #endregion MyRegion

            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            frmMenuItems frm = new frmMenuItems(listView1, this, mSerialPort, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory));
            frm.ShowDialog();
        }

        //private void btnloyalty_Click(object sender, EventArgs e)
        //{
        //    if (CheckPreordercompleted(order) == true)
        //    {
        //        lblStatus.Text = "Order Completed";
        //        return;
        //    }
        //    frmNewItems frm = new frmNewItems(listView1, money, this);
        //    frm.Department("Garden");
        //    frm.ShowDialog();
        //}

        //int intPumpID;

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (mLocketText)
                {
                    return;
                }
                mLocketText = true;
                if (listView1.SelectedIndices.Count > 0)
                {
                    Class.ProcessOrderNew.Item item = order.GetItemByKey(Convert.ToInt32(listView1.SelectedItems[0].Tag));
                    if (item != null)
                    {
                        txtitemname.Text = item.ItemName;
                        txtqty.Text = item.Qty + "";
                        //txtunitprice.Text = money.Convert3to2(Convert.ToDouble(money.Format(item.Price))).ToString();
                        txtunitprice.Text = money.Format(item.Price);
                        txtBarcode.Text = item.BarCode;
                        if (item.IsFuel)
                        {
                            txtqty.Enabled = false;
                            intPumpID = Convert.ToInt32(item.PumID);
                            //txtunitprice.Enabled = false;
                        }
                        else
                        {
                            txtqty.Enabled = true;
                            //txtunitprice.Enabled = true;
                        }
                        if (item.ListSubItem.Count > 0)
                        {
                            txtqty.Enabled = false;
                        }
                    }
                    else
                    {
                        Class.ProcessOrderNew.SubItem sub = order.GetSubItemByKey(Convert.ToInt32(listView1.SelectedItems[0].Tag));
                        if (sub != null)
                        {
                            txtitemname.Text = sub.ItemName;
                            txtqty.Text = sub.Qty + "";
                            //txtunitprice.Text = money.Convert3to2(Convert.ToDouble(money.Format(item.Price))).ToString();
                            txtunitprice.Text = money.Format(sub.Price);
                            txtBarcode.Text = sub.BarCode;
                            txtqty.Enabled = false;
                            //if (item.IsFuel)
                            //{
                            //    txtqty.Enabled = false;
                            //    intPumpID = Convert.ToInt32(item.PumID);
                            //}
                            //else
                            //{
                            //    txtqty.Enabled = true;
                            //}
                        }
                    }
                }
                mLocketText = false;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("listView1_SelectedIndexChanged::" + ex.Message);
            }
        }

        public void Addnewitems(Class.ProcessOrderNew.Item item)
        {
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            //order.ListItem.Add(item);
            Class.ProcessOrderNew.Item itemCus = order.AddItem(item, listView1, money, true);
            SendToCustomerDisplay(itemCus);
            try
            {
                //koi chung error
                order.PumpID = item.PumID;
            }
            catch (Exception)
            {
            }

            //order.CheckLoyaltyByListItem(listView1, money);
            order.CheckFuelDiscount(listView1, money);
            TotalLabel();
            //LoadListView(order);
            //order.AddItemToList(item, listView1, money);
        }

        //public void Addnewitems(Class.ProcessOrderNew.SubItem subitem)
        //{
        //    if (CheckPreordercompleted(order) == true)
        //    {
        //        lblStatus.Text = "Order Completed";
        //        return;
        //    }
        //    SendToCustomerDisplay(subitem);

        //    order.CheckLoyaltyByListItem(listView1, money);
        //    order.CheckFuelDiscount(listView1, money);
        //    TotalLabel();
        //}

        private void SendToCustomerDisplay(Class.ProcessOrderNew.Item item)
        {
            try
            {
                mCustomerDisplay.ClearScreen();
                item.Weight = item.Weight > 1 ? item.Weight : 1;
                if (item.Weight > 1)
                {
                    mCustomerDisplay.PrintLine1(item.ItemName + " $" + money.Format2(item.SubTotal));
                    mCustomerDisplay.PrintLine2((double)item.Qty / item.Weight + "L @ $" + money.Format(item.Price) + "PL");
                }
                else
                {
                    if (item.Qty > 1)
                    {
                        mCustomerDisplay.PrintLine1(item.ItemName + " $" + money.Format2(item.SubTotal));
                        mCustomerDisplay.PrintLine2(item.Qty + " @ $" + money.Format2(item.Price) + "E");
                    }
                    else
                    {
                        mCustomerDisplay.PrintLine1(item.ItemName);
                        mCustomerDisplay.PrintLine2("$" + money.Format2(item.SubTotal));
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("SendToCustomerDisplay::" + ex.Message);
            }
        }

        private void SendToCustomerDisplay(Class.ProcessOrderNew.SubItem subitem)
        {
            try
            {
                mCustomerDisplay.ClearScreen();
                if (subitem.Qty > 1)
                {
                    mCustomerDisplay.PrintLine1(subitem.ItemName + " $" + money.Format2(subitem.SubTotal));
                    mCustomerDisplay.PrintLine2(subitem.Qty + " @ $" + money.Format2(subitem.Price) + "E");
                }
                else
                {
                    mCustomerDisplay.PrintLine1(subitem.ItemName);
                    mCustomerDisplay.PrintLine2("$" + money.Format2(subitem.SubTotal));
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("SendToCustomerDisplay::" + ex.Message);
            }
        }

        private class ItemClick
        {
            public int ItemIndex { get; set; }

            public int ItemOptionIndex { get; set; }

            public ItemClick(int itemIndex, int itemOptionIndex)
            {
                ItemIndex = itemIndex;
                ItemOptionIndex = itemOptionIndex;
            }
        }

        //private void button33_Click(object sender, EventArgs e)
        //{
        //    Class.LogPOS.WriteLog("Load Short Cut.");
        //    mIndex = 0;
        //    mGroupShortCut++;
        //    if (mGroupShortCut>4)
        //    {
        //        mGroupShortCut = 0;
        //    }
        //    //LoadGroup();
        //    //LoadFirstGroup();
        //}

        //private void button32_Click(object sender, EventArgs e)
        //{
        //    Class.LogPOS.WriteLog("Load Drink Menu.");
        //    mIndex = 0;
        //    mGroupShortCut = 4;
        //    //LoadGroup();
        //    //LoadFirstGroup();
        //}

        //private void button31_Click(object sender, EventArgs e)
        //{
        //    Class.LogPOS.WriteLog("Load Main Menu.");
        //    mIndex = 0;
        //    mGroupShortCut = 2;
        //    //LoadGroup();
        //    LoadFirstGroup();
        //}

        //private void button30_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable tbl = conn.Select("select * from ordersdaily where tableID="+order.TableID+" and completed=0");
        //        if (tbl.Rows.Count==0)
        //        {
        //            order.ListItem.Clear();
        //            LoadListView(order);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        private void button29_Click(object sender, EventArgs e)
        {
            OrderIDTemp = 0;
            ReLoadGroup();
            Class.LogPOS.WriteLog("frmOrderAll.Button_Previous Order_Click.");
            button5.Tag = null;
            Button btn = (Button)sender;
            Class.ProcessOrderNew.Order lorder = processOrder.getPrevOrder(btn, listView1, money);
            if (lorder != null)
            {
                txtqty.Enabled = false;
                txtunitprice.Enabled = false;
                ClearTextBox();
                order = lorder;
                flp_takeaway.Enabled = false;
                txtorderid.Text = order.OrderID + "";

                try
                {
                    conn.Open();
                    if (order.CustCode != "" && order.CustCode != null)
                    {
                        txtCusName.Text = Convert.ToString(conn.ExecuteScalar("SELECT Name FROM customers where memberNo = '" + order.CustCode + "'"));
                        txtCusName.Visible = true;
                        label9.Visible = true;
                    }
                    else
                    {
                        txtCusName.Visible = false;
                        label9.Visible = false;
                    }
                }
                catch (Exception)
                {
                    txtCusName.Visible = false;
                    label9.Visible = false;
                }
                finally
                {
                    conn.Close();
                }

                if (listView1.Items.Count > 0)
                {
                    listView1_SelectedIndexChanged(listView1, null);
                }
                TotalLabel();
                //LoadListView(order);
            }
        }

        public static string strUserName = "";
        private frmSubTotal frmSubtotal;
        private frmCust frmcus;
        //string strArrayPumID;

        // goi insert 1 lan tai day
        private void button28_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            strUserName = txtstaffid.Text;
            order.ShiftID = mShift.ShiftID;
            lbChangeAmount.Text = "";
            lbChangeName.Text = "";
            int intCountFuel = 0;
            int intIDFuelHistory = 0;
            string strIDPump = "";

            Class.LogPOS.WriteLog("Begin Total Item :-----------------------------------------");
            foreach (ProcessOrderNew.Item item in order.ListItem)
            {
                Class.LogPOS.WriteLog("order Item :" + item.ItemName + "- PumpHistory :" + item.IDPumpHistory + "- PumID :" + item.PumID);
                if (item.IsFuel)
                {
                    intCountFuel++;
                    intIDFuelHistory = item.IDPumpHistory;
                    strIDPump = item.PumID;
                }
                if (item.ListSubItem.Count > 0)
                {
                    foreach (ProcessOrderNew.SubItem sub in item.ListSubItem)
                    {
                        Class.LogPOS.WriteLog("Subitem of " + item.ItemName + ":" + sub.ItemName);
                    }
                }
            }

            Class.LogPOS.WriteLog("order count fuel :" + intCountFuel);
            Class.LogPOS.WriteLog("--------------- List ---------------");
            foreach (ProcessOrderNew.Item item in order.ListItem)
            {
                if (!item.IsFuel)
                {
                    item.IDPumpHistory = intIDFuelHistory;
                    item.PumID = strIDPump;
                    Class.LogPOS.WriteLog("order Item :" + item.ItemName + "- PumpHistory :" + item.IDPumpHistory + "- PumID :" + item.PumID);
                }
            }
            Class.LogPOS.WriteLog("End Total Item :-----------------------------------------");

            //processOrder.subTotal(order);
            //this.Visible = false;
            Class.LogPOS.WriteLog("frmOrderAll.Button_SubToTal_Click.");
            if (order.ListItem.Count == 0)
            {
                lblStatus.Text = "Order item empty!";
                return;
            }

            if (order.Completed == 1 && order.Completed == 1)
            {
                lblStatus.Text = "Order completed!";
                return;
            }

            if (order.ListItem.Count > 0 && order.Completed != 1 && order.Completed != 2)
            {
                Class.LogPOS.WriteLog("frmOrderAll.Button_SubToTal_Click.Open Cash Drawer");
                //DataObject.ShiftReport shift = new DataObject.ShiftReport();
                //if (mshiftobject.bActiveShift == false && mshiftobject.bActiveShift == false)
                //{
                //    shift.CashFloatIn = mshiftobject.CashFloatIn;
                //}
                //else
                //{
                //    shift.ShiftID = mshiftobject.ShiftID;
                //    shift = BusinessObject.BOShiftReport.GetReportShift(shift);
                //}
                frmSubtotal = new frmSubTotal(frmCustomer, money, mCustomerDisplay, mSerialPort, axCsdEft, mDBConfig, new DataObject.ShiftReport(), new Class.ReadConfig());
                if (order.Delivery == 1)
                {
                    order.DeliveryFee = dbconfig.DeliveryAmount;
                }
                frmSubtotal.Order = order;
                //frmSubtotal.txtCustomerCode.Text = order.CustCode;

                ////////if (strMemberNo != "" && strMemberNo != null)
                ////////{
                ////////    frmSubtotal.Order.Delivery = 1;
                ////////    frmSubtotal.txtCustomerCode.Text = strMemberNo;
                ////////}
                ////////else
                ////////{
                ////////    frmSubtotal.Order.Delivery = 0;
                ////////    frmSubtotal.txtCustomerCode.Text = "";
                ////////}

                ////Tuan disable 07/02/2012
                //foreach (Class.ProcessOrderNew.Item item in order.ListItem)
                //{
                //    if (item.IsFuel == true)
                //    {
                //        order.PumpID = item.PumID;
                //    }
                //}
                //txt = frmSubtotal.txtCustomerCode;
                DialogResult dg = frmSubtotal.ShowDialog();
                if (dg == DialogResult.OK)
                {
                    OrderIDTemp = frmSubtotal.Order.OrderID;
                    order = frmSubtotal.Order;
                    order.StaffID = mStaffID;
                    button29.Tag = null;
                    //if (!order.TableID.Contains("TKA-") && txttableid.Text != "")
                    //{
                    //    processOrder.subTotal(order);
                    //    this.Visible = false;
                    //}
                    //else
                    //{
                    //    processOrder.subTotalTackeAway(order);
                    //}
                    Class.LogPOS.WriteLog(order.OrderByCardID);
                    if (order.OrderByCardID != "")
                    {
                        //insert iton order by card
                        InsertIntoOrderByCard();
                    }

                    if (!order.TableID.Contains("TKA-") && txttableid.Text != "")
                    {
                        lock ("subTotal")
                        {
                            processOrder.subTotal(order, frmSubtotal.strDataReceive);
                        }
                    }
                    else
                    {
                        lock ("subTotalTackeAway")
                        {
                            processOrder.subTotalTackeAway(order, frmSubtotal.strDataReceive);
                        }
                    }
                    //processOrder.subTotalTackeAway(order, frmSubtotal.strQueryExecuteAccount,frmSubtotal.strDataReceive);

                    if (order.ChangeByCash > 0 || order.CashOut > 0)
                    {
                        lbChangeName.Text = "Change of order " + order.OrderID;
                        lbChangeAmount.Text = "$" + money.Format2(order.ChangeByCash + order.CashOut);
                    }
                    //if (frmSubTotal.blnIsUseCreditNote ==  true)
                    //{
                    //    prinCreditNote();
                    //}

                    if (order.CashOut != 0)
                    {
                        InsertIntoCashOut(order.CashOut, order.StaffID, order.ShiftID);
                    }

                    try
                    {
                        string[] strPumpArrayID = order.PumpID.Split('-');
                        for (int i = 0; i < strPumpArrayID.Length; i++)
                        {
                            ChangeColort(Color.White, flp_takeaway.Controls[strPumpArrayID[i]]);
                        }
                    }
                    catch (Exception)
                    {
                    }

                    mListOrder.RemoveOrdrt(order);

                    NextCustomer();
                    //for (int i = 0; i <= 15; i++)
                    //{
                    //    ChangeColort(Color.White, flp_takeaway.Controls[i]);
                    //}
                }
                else if (dg == DialogResult.Abort)
                {
                    order = frmSubtotal.Order;
                    order.StaffID = mStaffID;

                    processOrder.SendOrder(order);
                    mListOrder.RemoveOrdrt(order);
                    //PrintDriverOff();
                    processOrder.printDriverOff(order);
                    try
                    {
                        ChangeColort(Color.White, flp_takeaway.Controls[Convert.ToInt32(order.PumpID) - 1]);
                    }
                    catch (Exception)
                    {
                    }
                    NextCustomer();
                }
            }
        }

        private double dblSubtotalCashOut;

        private void InsertIntoCashOut(double dblCashOut, int intStaffID, int intShiftID)
        {
            try
            {
                conn.Open();
                dblSubtotalCashOut = dblCashOut;
                conn.ExecuteNonQuery("insert into cashinandcashout(TotalAmount,EmployeeID,CashType,ShiftID,Description) values(" + dblCashOut + "," + intStaffID + ",5," + intShiftID + ",'Cash out subtotal')");
                conn.Close();
                //processOrder.PrintCashInOrCashOut(dblCashOut);
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }

            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("InsertIntoCashOut::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = dbconfig.Header1.ToString();
            string bankCode = dbconfig.Header2.ToString();
            string address = dbconfig.Header3.ToString();
            string tell = dbconfig.Header4.ToString();
            string address1 = dbconfig.Header5.ToString();
            string terminal = dbconfig.CableID.ToString();
            string website = dbconfig.FootNode1.ToString();
            string thankyou = dbconfig.FootNode2.ToString();
            System.Drawing.Font font11 = new System.Drawing.Font("Arial", 11);

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += 100;

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += 100;
            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);
            mPrinter.DrawString("ORDER# " + txtorderid.Text, e, font11, l_y, 1);
            l_y += 100;
            mPrinter.DrawString("TERMINAL# " + terminal, e, font11, l_y, 1);
            l_y += 100;
            mPrinter.DrawString("OPERATOR# " + strUserName, e, font11, l_y, 1);
            //l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y = mPrinter.DrawString("SHIFT# " + mShift.ShiftID, e, font11, l_y, 3);
            l_y += 100;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += 100;
            l_y += 100;
            l_y = mPrinter.DrawString("Cash Out ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            l_y += 20;
            mPrinter.DrawString("Cash out:", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, new Font("Arial", 10), l_y, 3);
            l_y += 100;

            mPrinter.DrawString("Card:", e, font11, l_y, 1);
            l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, font11, l_y, 3);

            foreach (OrderByCard c in order.MoreOrderByCard)
            {
                if (c.subtotal > 0)
                {
                    mPrinter.DrawString("  * " + c.nameCard + ":", e, font11, l_y, 1);
                    l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, font11, l_y, 3);
                }
            }

            //mPrinter.DrawString("Cash out:", e, new Font("Arial", 10), l_y, 1);
            //l_y = mPrinter.DrawString("$" + money.Format2(dblSubtotalCashOut), e, new Font("Arial", 10), l_y, 3);
            //l_y += 100;
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);
            l_y += 200;
            l_y = mPrinter.DrawString("Items with * is no GST", e, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y += 50;
            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
            l_y += 100;
            l_y = mPrinter.DrawString(website, e, new System.Drawing.Font("Arial", 9), l_y, 2);
            l_y = mPrinter.DrawString(thankyou, e, new System.Drawing.Font("Arial", 9), l_y, 2);
        }

        private void PrintDriverOff()
        {
            try
            {
                mPrinter = new Printer();
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPageDriverOff);
                mSetting = new Class.Setting();
                mReadDBConfig = new Connection.ReadDBConfig();
                mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                mPrinter.printDocument.Print();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("PrintDriverOff::" + ex.Message);
            }
        }

        private void printDocument_PrintPageDriverOff(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float y = 0;
            y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            DateTime dateTime = DateTime.Now;
            y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);

            y += 100;

            y = mPrinter.DrawString("DRIVE OFF", e, new Font("Arial", 18, FontStyle.Bold), y, 2);
        }

        private void InsertIntoOrderByCard()
        {
            try
            {
                conn.Open();
                string strOrderByCard = "";
                Class.LogPOS.WriteLog(order.MoreOrderByCard.Count + "");
                foreach (OrderByCard c in order.MoreOrderByCard)
                {
                    strOrderByCard = c.orderbycardID;

                    // insert 1 lan vao orderbycard
                    string dstr = "insert into orderbycard(orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash) values(" + c.orderbycardID + "," + c.orderID + "," + c.cardID + "," + money.getFortMat(c.subtotal) + "," + money.getFortMat(c.cashOut) + "," + c.shiftID + ",'" + c.AuthCode + "','" + c.AccountType + "','" + c.Pan + "','" + c.DateExpiry + "','" + c.Date + "','" + c.Time + "','" + c.CardType + "'," + money.getFortMat(c.AmtPurchase.ToString()) + "," + money.getFortMat(c.AmtCash.ToString()) + ")";

                    conn.ExecuteNonQuery(dstr);
                    Class.LogPOS.WriteLog("insert into orderbycard(orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash) values(" + c.orderbycardID + "," + c.orderID + "," + c.cardID + "," + money.getFortMat(c.subtotal) + "," + money.getFortMat(c.cashOut) + "," + c.shiftID + ",'" + c.AuthCode + "','" + c.AccountType + "','" + c.Pan + "','" + c.DateExpiry + "','" + c.Date + "','" + c.Time + "','" + c.CardType + "'," + money.getFortMat(c.AmtPurchase.ToString()) + "," + money.getFortMat(c.AmtCash.ToString()) + ")");
                }

                string strQuerySelect = "SELECT DISTINCT orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash FROM orderbycard o where orderbycardID = '" + strOrderByCard + "'";

                DataTable dtSource = conn.Select(strQuerySelect);
                // duc comment 4-4- 2013
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("InsertIntoOrderByCard::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private bool CheckPreordercompleted(Class.ProcessOrderNew.Order order)
        {
            if (order.Completed == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int intPumIDItemSelect;
        private int intNumrow;
        private bool blnIsFuel;

        private void button27_Click(object sender, EventArgs e)
        {
            #region MyRegion

            //Class.LogPOS.WriteLog("frmOrderAll.Button_Bill_Click.");
            //if (txttableid.Text=="")
            //{
            //    return;
            //}
            //object orderID = null;
            //try
            //{
            //    conn.Open();
            //    orderID = conn.ExecuteScalar("select orderID from ordersdaily where orderID="+order.OrderID);
            //}
            //catch (Exception)
            //{
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //if (orderID==null)
            //{
            //    lblStatus.Text = "Order can not print to Bill !";
            //    return;
            //}
            //if (order.ListItem.Count>0)
            //{
            //    processOrder.billOrder(order);
            //    if (!order.TableID.Contains("TKA-"))
            //    {
            //        this.Visible = false;
            //    }
            //}

            #endregion MyRegion

            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            mLocketText = true;
            Class.LogPOS.WriteLog("frmOrderAll.Delete Item");
            if (listView1.SelectedIndices.Count > 0)
            {
                intPumIDItemSelect = 0;
                blnIsFuel = false;
                intNumrow = 0;
                try
                {
                    foreach (ListViewItem item in listView1.SelectedItems)
                    {
                        intPumIDItemSelect = Convert.ToInt32(order.ListItem[item.Index].PumID);
                        blnIsFuel = order.ListItem[item.Index].IsFuel;
                    }
                    foreach (ListViewItem item in listView1.Items)
                    {
                        blnIsFuel = order.ListItem[item.Index].IsFuel;
                        try
                        {
                            if (intPumIDItemSelect == Convert.ToInt32(item.SubItems[4].Text.ToString() == "" ? "0" : item.SubItems[4].Text.ToString()) && blnIsFuel == true)
                            {
                                intNumrow++;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                catch (Exception)
                {
                }

                ClearTextBox();
                int index = listView1.SelectedIndices[0];
                order.RemoveItem(Convert.ToInt32(listView1.SelectedItems[0].Tag), listView1, money, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory));
                //order.CheckLoyaltyByListItem(listView1, money);
                order.CheckFuelDiscount(listView1, money);
                if (listView1.Items.Count > 0)
                {
                    if (!(index >= 0 && index < listView1.Items.Count))
                    {
                        index = listView1.Items.Count - 1;
                    }
                    listView1.Items[index].Selected = true;
                }
                TotalLabel();
                try
                {
                    UCPump uct = (UCPump)flp_takeaway.Controls[intPumIDItemSelect - 1];
                    if (uct.BackColor == Color.LimeGreen && intNumrow == 1)
                    {
                        uct.BackColor = Color.White;
                    }
                    mCustomerDisplay.ClearScreen();
                }
                catch (Exception)
                {
                }
            }
            //if (listView1.Items.Count == 0)
            //{
            try
            {
                conn.Open();
                UCPump uct = (UCPump)flp_takeaway.Controls[intPumpID - 1];

                string str = "SELECT COUNT(ID) FROM fuelhistory fh where fh.IsComplete = 0 AND fh.PumID = " + intPumpID;
                int intCountRow = Convert.ToInt32(conn.ExecuteScalar(str));

                str = "SELECT ID,PumID,VolumeAmount,CashAmount,FromPump FROM fuelhistory fh where fh.IsComplete = 0 AND fh.PumID = " + intPumpID;
                dtSource = conn.Select(str);

                if (dtSource.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 1 && intCountRow == 1)
                    {
                        uct.BackColor = Color.Blue;
                    }
                    else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 0 && intCountRow == 1)
                    {
                        uct.BackColor = Color.LimeGreen;
                    }
                    else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 1 && intCountRow != 1)
                    {
                        uct.BackColor = Color.Red;
                    }
                    else if (Convert.ToInt32(dtSource.Rows[0]["FromPump"]) == 0 && intCountRow != 1)
                    {
                        uct.BackColor = Color.Red;
                    }
                }

                //if (uct.BackColor != Color.Blue)
                //{
                //    ChangeColort(Color.White, flp_takeaway.Controls[intPumpID - 1]);
                //    //ChangeColort(Color.White, flp_takeaway.Controls[intPumpID]);
                //}
                listView1.Items[listView1.Items.Count - 1].Selected = true;

                //lstOrderOfPump.Items.Clear();
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
            mLocketText = false;
            //}
        }

        private int index = 1;

        private void button26_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    conn.Open();
            //    string str = "SELECT COUNT(*) AS NumRecord FROM fuelhistory WHERE IsComplete = 0";
            //    DataTable dtSource = conn.Select(str);
            //    int intNumRecord = Convert.ToInt32(dtSource.Rows[0]["NumRecord"]) - index;
            //    if (intNumRecord <= 0)
            //    {
            //        index = 0;
            //    }
            //    index++;
            //    str = "SELECT fh.ID,fh.KindOfFuel,fh.PumID,f.KindName,fh.CashAmount,fh.VolumeAmount,fh.UnitPrice FROM fuelhistory AS fh LEFT JOIN FUEL AS f on fh.KindOfFuel = f.KindID WHERE fh.IsComplete = 0 ORDER BY ID DESC LIMIT " + intNumRecord + ",1";
            //    dtSource = conn.Select(str);

            //    strIDPumpHistory = dtSource.Rows[0]["ID"].ToString();
            //    strGradeName = dtSource.Rows[0]["KindName"].ToString();
            //    strGradeNo = dtSource.Rows[0]["KindOfFuel"].ToString();
            //    strCashAmount = dtSource.Rows[0]["CashAmount"].ToString();
            //    strVolumeAmount = dtSource.Rows[0]["VolumeAmount"].ToString();
            //    strUnitPrice = dtSource.Rows[0]["UnitPrice"].ToString();
            //    string strPumpNo = dtSource.Rows[0]["PumID"].ToString();
            //    NextCustomer2();
            //    Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
            //        Convert.ToInt32(strIDPumpHistory),
            //        Convert.ToInt32(strGradeNo),
            //        strGradeName,
            //        Convert.ToInt32(strVolumeAmount),
            //        Convert.ToDouble(strCashAmount),
            //        Convert.ToDouble(strUnitPrice),
            //        0, 0, 0,
            //        strPumpNo);
            //    item.Weight = 1000;
            //    item.IsFuel = true;
            //    item.PumID = strPumpNo;
            //    intPumpID = Convert.ToInt32( item.PumID );
            //    ChangeColort(Color.Blue, flp_takeaway.Controls[strPumpNo]);
            //    AddListView(item);
            //}
            //catch (Exception ex)
            //{
            //    Class.LogPOS.WriteLog("button26_Click::" + ex.Message);
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //Banh nhanh thuc an
            //Class.LogPOS.WriteLog("frmOrderAll.Button FastFood_Click.");
            //frmOrderAllQS frm = new frmOrderAllQS(this.money, listView1, this, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory));
            //frm.ShowDialog();

            Class.LogPOS.WriteLog("frmOrderAll.Load Option");
            int tagOption = 0;
            Button btn = (Button)sender;
            DataTable tbl = new DataTable();
            if (btn.Tag == null)
            {
                mGroup.GroupID_Main = mGroup.GroupID;
            }
            try
            {
                conn.Open();
                tbl = conn.Select("select groupID,if(grOption=0,-1,displayOrder) as displayOrder from groupsmenu where grShortCut=1 and (grOption=0 or grOption=" + mGroup.GroupID_Main + ") order by displayOrder");
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
            try
            {
                if (tbl.Rows.Count == 0)
                {
                    LoadItem(mGroup.GroupID);
                }
                else
                {
                    if (btn.Tag == null)
                    {
                        mGroup.GroupID = tbl.Rows[tagOption]["groupID"].ToString();
                        zIndex = 0;
                        LoadItem(tbl.Rows[tagOption]["groupID"].ToString());
                        btn.Tag = tagOption;
                    }
                    else
                    {
                        tagOption = Convert.ToInt32(btn.Tag) + 1;
                        if (tagOption < tbl.Rows.Count)
                        {
                            mGroup.GroupID = tbl.Rows[tagOption]["groupID"].ToString();
                            zIndex = 0;
                            LoadItem(tbl.Rows[tagOption]["groupID"].ToString());
                            btn.Tag = tagOption;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void NextCustomer2()
        {
            if (order != null)
            {
                order = order.CoppyOrder();
            }
            txtitemname.Text = "";
            txtqty.Text = "";
            txtunitprice.Text = "";
            txtBarcode.Text = "";
            txtqty.Enabled = true;
            txtunitprice.Enabled = true;
            textBox1.Text = "";
            //txtqty_Enter(txtqty, null);
            loadOrder("", 1);
            for (int i = 0; i <= 15; i++)
            {
                ChangeColort(Color.White, flp_takeaway.Controls[i]);
            }
        }

        private void frmOrdersAll_VisibleChanged(object sender, EventArgs e)
        {
            txtqty.Focus();
            button29.Tag = null;
            if (this.Visible)
            {
                txtorderid.Text = order.OrderID + "";
                txttableid.Text = order.TableID + "";
                textBox1.Text = order.NumPeople + "";
            }
            else
            {
                txtitemname.Text = "";
                txtorderid.Text = "";
                txtqty.Text = "";
                txttableid.Text = "";
                txtunitprice.Text = "";
                textBox1.Text = "";
            }
        }

        private void txtqty_Enter(object sender, EventArgs e)
        {
            mLocketText = false;
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void txtqty_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    if (CheckPreordercompleted(order) == true)
        //    {
        //        lblStatus.Text = "Order Completed";
        //        return;
        //    }
        //    frmNewItems frm = new frmNewItems(listView1, money, this);
        //    frm.Department("Hardware");
        //    frm.ShowDialog();
        //}

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    if (CheckPreordercompleted(order) == true)
        //    {
        //        lblStatus.Text = "Order Completed";
        //        return;
        //    }
        //    frmNewItems frm = new frmNewItems(listView1, money, this);
        //    frm.Department("Electrical");
        //    frm.ShowDialog();
        //}

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            //if (txtqty.Text!="")
            //{
            //    int qty=Convert.ToInt32(txtqty.Text);
            //    if (qty>100)
            //    {
            //        txtqty.Text = 1+"";
            //        lblStatus.Text = "Qty of item not more than 100";
            //        return;
            //    }
            //}
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (mLocketText)
            {
                return;
            }
            mLocketText = true;
            if (listView1.SelectedIndices.Count > 0 && txtqty.Text != "")
            {
                Class.ProcessOrderNew.Item item = order.GetItemByKey(Convert.ToInt32(listView1.SelectedItems[0].Tag));
                if (item != null)
                {
                    item.Qty = Convert.ToInt32(txtqty.Text);
                    item.SubTotal = item.Qty * item.Price;

                    order.EditItem(item, listView1, money);
                    SendToCustomerDisplay(item);
                    TotalLabel();
                }
                else
                {
                    Class.ProcessOrderNew.SubItem sub = order.GetSubItemByKey(Convert.ToInt32(listView1.SelectedItems[0].Tag));
                    if (sub != null)
                    {
                        sub.Qty = Convert.ToInt32(txtqty.Text);
                        sub.SubTotal = sub.Qty * sub.Price;
                        order.EditSubItem(sub, listView1, money);
                        TotalLabel();
                    }
                }
            }
            //if (listView1.SelectedIndices.Count > 0 && (txtqty.Text == "" || Convert.ToInt32(txtqty.Text) == 0))
            //{
            //    Class.ProcessOrderNew.Item item = order.GetItemByKey(Convert.ToInt32(listView1.SelectedItems[0].Tag));
            //    item.Qty = 1;
            //    item.SubTotal = item.Qty * item.Price;
            //    order.EditItem(item, listView1, money);
            //    SendToCustomerDisplay(item);
            //    TotalLabel();
            //}
            //order.CheckLoyaltyByListItem(listView1, money);
            order.CheckFuelDiscount(listView1, money);
            TotalLabel();
            mLocketText = false;
        }

        private void RemoveLoyaltyItem(int listViewIndex)
        {
            if (listViewIndex < listView1.Items.Count)
            {
                ItemClick item = (ItemClick)listView1.Items[listViewIndex].Tag;
                if (item.ItemIndex == -1 && item.ItemOptionIndex == -1)
                {
                    listView1.Items.RemoveAt(listViewIndex);
                    RemoveLoyaltyItem(listViewIndex);
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //frmCust frmTN = new frmCust(1);
            //txt = frmTN.textBox1;
            //frmTN.ShowDialog();
            ReLoadGroup();
            frmFuntion frm = new frmFuntion(this, money, intTotalPump, mSerialPort, conn, axCsdEft, mDBConfig);
            //frmFuntion frm = new frmFuntion(this,money,mShift,txt,intTotalPump);
            //txt = frm.txtBarCode;
            frm.ShowDialog();
            //txt = txtBarcode;
            mUCInfoTop = new UCInfoTop();
            mUCInfoTop.timer1.Tick += new EventHandler(timer1_Tick);
            mUCInfoTop.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(mUCInfoTop);
            mShift = new Shifts();
            LoadShift();
            ChangeLocalModeLabel();
            if (conn.GetCableID() == "1")
            {
                mUCInfoTop.lbServerMode.Visible = false;
            }
            else
            {
                mThreadServerClient = new System.Threading.Thread(ThreadServerClient);
                mThreadServerClient.Start();
            }
            //Init();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (order.Completed == 0 && order.ListItem.Count > 0 && order.SubTotal > 0)
            {
                processOrder.RePrintOrder(order);
            }
            else
            {
                ReLoadGroup();
                object oj = null;
                try
                {
                    conn.Open();
                    //string sql = "select if(max(orderID) is null,0,max(orderID)) as max from ordersdaily";
                    string sql;
                    if (OrderIDTemp != 0)
                    {
                        sql = "select if(max(orderID) is null,0,max(orderID)) as max from ordersdaily where (completed=1 or completed=2) and orderID=" + OrderIDTemp + "";
                    }
                    else
                    {
                        sql = "select if(max(orderID) is null,0,max(orderID)) as max from ordersdaily where (completed=1 or completed=2) and orderID=" + this.order.OrderID + "";
                    }

                    oj = conn.ExecuteScalar(sql);
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog("frmOrderAll:::button5_Click:::" + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                if (Convert.ToInt32(oj) != 0)
                {
                    if (this.order.OrderID - 1 == Convert.ToInt32(oj))
                    {
                        Class.ProcessOrderNew.Order lastorder = processOrder.GetLastOrder(money);
                        Class.LogPOS.WriteLog("Print Last Order");
                        processOrder.printTaxtInvoice(lastorder);
                    }
                    else if (OrderIDTemp != 0)
                    {
                        Class.ProcessOrderNew.Order lastorder = processOrder.GetLastOrderByID(money, OrderIDTemp);
                        Class.LogPOS.WriteLog("Print Last Order");
                        processOrder.printTaxtInvoice(lastorder);
                    }
                    else
                    {
                        Class.LogPOS.WriteLog("Print Previous Order");
                        processOrder.printTaxtInvoice(order);
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog("frmOrderAll.btnloyalty_Click.");
            if (listView1.SelectedIndices.Count > 0)
            {
                ListViewItem li = listView1.SelectedItems[0];
                Class.ProcessOrderNew.Item item = order.GetItemByKey(Convert.ToInt32(li.Tag));
                Class.ProcessOrderNew.SubItem sub = null;
                if (item == null)
                {
                    //mLocketText = false;
                    //ClearTextBox();
                    //return;
                    sub = order.GetSubItemByKey(Convert.ToInt32(li.Tag));
                    item = order.GetItemBySub(sub);
                }
                if (item.ParentItem != 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (order.CheckContainChildrent(item.KeyItem))
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (item.IsFuel)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                frmDiscount frm = new frmDiscount();
                frm.ItemName = item.ItemName;
                frm.Qty = 1;
                frm.Price = item.getTotal();
                frm.Total = item.getTotal();
                frm.ShiftID = order.ShiftID;

                if (frm.Total <= 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mStaffPermission == 3)
                {
                    frm.IsManager = true;
                }
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Class.ProcessOrderNew.Item itemDiscount = new Class.ProcessOrderNew.Item(0, frm.DiscountID, frm.DiscountName, 1, -1 * frm.Discount, -1 * frm.Discount, 0, 0, 0, "0");
                    itemDiscount.ItemType = 1;
                    itemDiscount.EnableFuelDiscount = 1;
                    //itemDiscount.OptionCount = 1;
                    itemDiscount.ParentItem = item.KeyItem;
                    order.InsertAfterKey(itemDiscount, item.KeyItem, listView1, money);
                    order.CheckFuelDiscount(listView1, money);
                    TotalLabel();
                    //order.AddItemToList(itemDiscount,listView1,money);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog("frmOrderAll.Delete All Item");
            try
            {
                order.ClearItemToListAndListView(listView1);
                //order.CheckLoyaltyByListItem(listView1, money);
                order.CheckFuelDiscount(listView1, money);
                TotalLabel();
                ClearTextBox();
                //for (int i = 0; i <= 15; i++)
                //{
                //    ChangeColort(Color.White, flp_takeaway.Controls[i]);
                //}
                mListOrder.RemoveOrdrt(order);
                NextCustomer();
                mCustomerDisplay.ClearScreen();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmOrderAll.Button_DeleteAll_Click:::" + ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmMessageBox frm = new frmMessageBox("Information", "Do you want exit POS System?");
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                Class.LogPOS.WriteLog("frmMain.btnexit_Click.");
                Application.Exit();
            }
            //this.Visible = false;
            //this.Close();
        }

        public void ChangeEnable(bool enable, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeEnableCallback d = new ChangeEnableCallback(ChangeEnable);
                control.Invoke(d, new object[] { enable, control });
            }
            else
            {
                control.Enabled = enable;
            }
        }

        public void ChangeColort(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeColorCallbackt d = new ChangeColorCallbackt(ChangeColort);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.BackColor = color;
            }
        }

        private void ChangeForceColor(Color color, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeForceColorCallbackt d = new ChangeForceColorCallbackt(ChangeForceColor);
                control.Invoke(d, new object[] { color, control });
            }
            else
            {
                control.ForeColor = color;
            }
        }

        public void SetText(string text, Control control)
        {
            try
            {
                if (control.InvokeRequired)
                {
                    ChangeTextCallback d = new ChangeTextCallback(SetText);
                    control.Invoke(d, new object[] { text, control });
                }
                else
                {
                    control.Text = text;
                }
            }
            catch (Exception)
            {
            }
        }

        public void SetText2(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText2);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = "$ " + text;
            }
        }

        public void SetText3(string text, Control control)
        {
            if (control.InvokeRequired)
            {
                ChangeTextCallback d = new ChangeTextCallback(SetText3);
                control.Invoke(d, new object[] { text, control });
            }
            else
            {
                control.Text = text + " L";
            }
        }

        //private void runChangeColort()
        //{
        //    while (true)
        //    {
        //        List<Class.Functions.TakeAway> list = fun.GetStatusTakeAway();
        //        for (int i = 0; i < list.Count; i++)
        //        {
        //            try
        //            {
        //                Class.Functions.TakeAway tk = list[i];
        //                UCPostaw uc = (UCPostaw)flp_takeaway.Controls[i];
        //                uc.Tag = tk.tableID;
        //                SetText(tk.orderid + "", uc.lblOrderID);
        //                SetText(money.Format(tk.SubTotal), uc.lblSubTotal);
        //                if (tk.complete == 0)
        //                {
        //                    ChangeColort(Color.Blue, flp_takeaway.Controls[i]);
        //                }
        //                if (tk.complete == 2)
        //                {
        //                    ChangeColort(Color.Orange, flp_takeaway.Controls[i]);
        //                }
        //                else if (tk.complete == 3)
        //                {
        //                    ChangeColort(Color.Green, flp_takeaway.Controls[i]);
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }
        //        }
        //        for (int i = list.Count; i < flp_takeaway.Controls.Count; i++)
        //        {
        //            UCPostaw uc = (UCPostaw)flp_takeaway.Controls[i];
        //            uc.Tag = null;
        //            SetText("", uc.lblOrderID);
        //            SetText("", uc.lblSubTotal);
        //            ChangeColort(Color.White, flp_takeaway.Controls[i]);
        //        }
        //        System.Threading.Thread.Sleep(1000);
        //    }
        //}

        private void txttableid_TextChanged(object sender, EventArgs e)
        {
            if (txttableid.Text.Contains("TKA-"))
            {
                txttableid.Text = "";
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Class.LogPOS.WriteLog("frmOrderAll.btnloyalty_Click.");
            if (listView1.SelectedIndices.Count > 0)
            {
                ListViewItem li = listView1.SelectedItems[0];
                Class.ProcessOrderNew.Item item = order.GetItemByKey(Convert.ToInt32(li.Tag));
                if (item == null)
                {
                    mLocketText = false;
                    ClearTextBox();
                    return;
                }
                if (item.ParentItem != 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (order.CheckContainChildrent(item.KeyItem))
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (!item.IsFuel)
                {
                    mLocketText = false;
                    lblStatus.Text = "Only discount Fuel item";
                    return;
                }
                //frmFuelDiscount frm = new frmFuelDiscount();
                //frm.ShowDialog();
                frmFuelDiscount frm = new frmFuelDiscount(conn, money);
                frm.ItemName = item.ItemName;
                frm.Qty = item.Qty;
                frm.Price = item.Price;
                frm.Total = item.SubTotal;
                frm.ShiftID = order.ShiftID;
                frm.Weight = item.Weight;
                //if (item.IsFuel)
                //{
                //    mLocketText = false;
                //    lblStatus.Text = "Item can not discount";
                //    return;
                //}
                if (frm.Total <= 0)
                {
                    mLocketText = false;
                    lblStatus.Text = "Item can not discount";
                    return;
                }
                if (mStaffPermission == 3)
                {
                    frm.IsManager = true;
                }
                if (Class.FuelDiscountConfig.GetListFuelDiscount() == null)
                {
                    mLocketText = false;
                    lblStatus.Text = "No discount in list";
                    return;
                }
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Class.ProcessOrderNew.Item itemDiscount = new Class.ProcessOrderNew.Item(0, frm.DiscountID, frm.DiscountName, 1, -1 * frm.Discount, -1 * frm.Discount, 0, 0, 0, "0");
                    itemDiscount.ItemType = 1;
                    itemDiscount.EnableFuelDiscount = 1;
                    itemDiscount.EnableRemove = false;
                    //itemDiscount.OptionCount = 1;
                    itemDiscount.ParentItem = item.KeyItem;
                    order.InsertAfterKey(itemDiscount, item.KeyItem, listView1, money);
                    order.CheckFuelDiscount(listView1, money);
                    TotalLabel();
                    //order.AddItemToList(itemDiscount,listView1,money);
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            if (order != null)
            {
                if (order.Completed != 1 && order.ListItem.Count > 0)
                {
                    mListOrder.AddOrder(order);
                    mListOrderTemp.AddOrder(order);
                    //order = order.CoppyOrder();
                    NextCustomer();
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                NextCustomer();
                ReLoadGroup();
                Class.ProcessOrderNew.Order o = mListOrder.NextOrder();
                if (o != null)
                {
                    order = o;
                    //LoadListView(order,1);
                    foreach (Class.ProcessOrderNew.Item item in order.ListItem)
                    {
                        order.InsertItemListView(item, -1, listView1, money, true);
                    }
                    lblTotal.Text = money.Format2(order.getSubTotal());

                    try
                    {
                        conn.Open();
                        if (order.CustCode != "" && order.CustCode != null)
                        {
                            txtCusName.Text = Convert.ToString(conn.ExecuteScalar("SELECT Name FROM customers where memberNo = '" + order.CustCode + "'"));
                            txtCusName.Visible = true;
                            label9.Visible = true;
                        }
                        else
                        {
                            txtCusName.Visible = false;
                            label9.Visible = false;
                        }
                    }
                    catch (Exception)
                    {
                        txtCusName.Visible = false;
                        label9.Visible = false;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("Recal Order::" + ex.Message);
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            mLocketText = false;
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (e.KeyChar == (char)46)
            {
                lblStatus.Text = "Invalid Qty";
                e.Handled = true;
            }
            //else if (txtqty.Text.Length>0)
            //{
            //    if (!Char.IsControl(e.KeyChar) && Convert.ToInt16(txtqty.Text+(char)e.KeyChar)>100)
            //    {
            //        lblStatus.Text = "Invalid Qty";
            //        e.Handled = true;
            //    }
            //}
        }

        private void ReLoadGroup()
        {
            //LoadItem(mGroup.GroupID);
            LoadFirstGroup();
        }

        public void button11_Click(object sender, EventArgs e)
        {
            //if (order!=null)
            //{
            //    order = order.CoppyOrder();
            //}
            //button1_Click(null, null);
            NextCustomer();
            ReLoadGroup();
        }

        public void NextCustomer()
        {
            if (order != null)
            {
                order = order.CoppyOrder();
            }
            index = 1;

            SetText("", txtitemname);
            SetText("", txtqty);
            SetText("", txtunitprice);
            SetText("", txtBarcode);
            SetText("", textBox1);
            SetText("", txtitemname);
            SetText("", txtCusName);

            strCustomerName = "";
            strMemberNo = "";
            intCusID = 0;
            intCusType = 0;

            flp_takeaway.Enabled = true;
            btnSendOrder.Enabled = false;
            btnBill.Enabled = false;
            ChangeEnable(true, txtqty);
            ChangeEnable(true, txtunitprice);
            button29.Tag = null;

            lbChangeName.Visible = true;
            lbChangeAmount.Visible = true;

            txtCusName.Visible = false;
            label9.Visible = false;
            intPumpID = 0;
            strIDPumpHistory = "";
            loadOrder("", 1);
            try
            {
                conn.Open();

                string str = "SELECT PumID,count(ID) AS NumRecord,FromPump FROM fuelhistory fh where fh.IsComplete = 0 group by PumID";

                dtSource = conn.Select(str);

                for (int i = 0; i <= 15; i++)
                {
                    ChangeColort(Color.White, flp_takeaway.Controls[i]);
                }

                foreach (DataRow drRow in dtSource.Rows)
                {
                    try
                    {
                        if (Convert.ToInt32(drRow["FromPump"]) == 1 && Convert.ToInt32(drRow["NumRecord"]) == 1)
                        {
                            str = "SELECT ID,PumID,CashAmount,VolumeAmount,UnitPrice,Gst,FromPump FROM fuelhistory fh where fh.IsComplete = 0 and fh.FromPump = 1 and fh.PumID = " + Convert.ToInt32(drRow["PumID"]);

                            dtSource = conn.Select(str);

                            UCPump uc = (UCPump)flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1];
                            uc.Tag = dtSource.Rows[0]["ID"];
                            SetText(dtSource.Rows[0]["PumID"].ToString(), uc.lbPumpID);
                            SetText3(money.Format(dtSource.Rows[0]["VolumeAmount"].ToString()), uc.lbLitre);
                            SetText2(money.Format(dtSource.Rows[0]["CashAmount"].ToString()), uc.lbTotal);

                            ChangeColort(Color.Blue, flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1]);
                        }
                        else if (Convert.ToInt32(drRow["FromPump"]) == 0 && Convert.ToInt32(drRow["NumRecord"]) == 1)
                        {
                            str = "SELECT ID,PumID,CashAmount,VolumeAmount,UnitPrice,Gst,FromPump FROM fuelhistory fh where fh.IsComplete = 0 and fh.FromPump = 0 and fh.PumID = " + Convert.ToInt32(drRow["PumID"]);
                            dtSource = conn.Select(str);
                            UCPump uc = (UCPump)flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1];
                            uc.Tag = dtSource.Rows[0]["ID"];
                            SetText(dtSource.Rows[0]["PumID"].ToString(), uc.lbPumpID);
                            SetText3(money.Format(dtSource.Rows[0]["VolumeAmount"].ToString()), uc.lbLitre);
                            SetText2(money.Format(dtSource.Rows[0]["CashAmount"].ToString()), uc.lbTotal);
                            ChangeColort(Color.LimeGreen, flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1]);
                        }
                        else if (Convert.ToInt32(drRow["FromPump"]) == 1 && Convert.ToInt32(drRow["NumRecord"]) != 1)
                        {
                            ChangeColort(Color.Red, flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1]);
                        }
                        else if (Convert.ToInt32(drRow["FromPump"]) == 0 && Convert.ToInt32(drRow["NumRecord"]) != 1)
                        {
                            ChangeColort(Color.Red, flp_takeaway.Controls[Convert.ToInt32(drRow["PumID"]) - 1]);
                        }
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("NextCustomer::" + ex.Message);
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            frmNewItems frm = new frmNewItems(listView1, money, this);
            frm.ShowDialog();
        }

        private void buttonRefun_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            Class.RawPrinterHelper.openCashDrawer(setprinter.GetBillPrinter());
            Forms.frmRefunce frm = new frmRefunce(order, money);
            frm.ShowDialog();
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if (mLocketText)
            {
                return;
            }
            if (txtBarcode.Text != "")
            {
                try
                {
                    Connection.Connection con = new Connection.Connection();
                    DataTable tbl = con.Select("select * from itemsmenu where scanCode='" + txtBarcode.Text + "' limit 0,1");
                    if (tbl.Rows.Count > 0)
                    {
                        Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0, Convert.ToInt16(tbl.Rows[0]["itemID"]), tbl.Rows[0]["itemDesc"].ToString(), 1, Convert.ToDouble(tbl.Rows[0]["unitPrice"]), Convert.ToDouble(tbl.Rows[0]["unitPrice"]), Convert.ToDouble(tbl.Rows[0]["gst"]), 0, 0, "0");
                        item.BarCode = txtBarcode.Text;
                        item.EnableFuelDiscount = Convert.ToInt16(tbl.Rows[0]["enableFuelDiscount"]);
                        this.Addnewitems(item);
                    }
                    else
                    {
                        ClearTextBox();
                        lblStatus.Text = "This item not found !";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("txtBarcode_TextChanged" + ex.Message);
                }
            }
        }

        private void ClearTextBox()
        {
            mLocketText = true;

            txtitemname.Text = "";
            txtunitprice.Text = "";
            txtqty.Text = "";

            mLocketText = false;
        }

        private void txtBarcode_Click(object sender, EventArgs e)
        {
        }

        private class ListOrder
        {
            private List<Class.ProcessOrderNew.Order> listOrder;
            private int mCurren = 0;

            public ListOrder()
            {
                listOrder = new List<Class.ProcessOrderNew.Order>();
            }

            public void AddOrder(Class.ProcessOrderNew.Order order)
            {
                if (!listOrder.Contains(order))
                {
                    //listOrder.Add(order.CoppyOrder());
                    listOrder.Add(order);
                }
            }

            public void RemoveOrdrt(Class.ProcessOrderNew.Order order)
            {
                if (listOrder.Contains(order))
                {
                    listOrder.Remove(order);
                }
            }

            public Class.ProcessOrderNew.Order CurrenOrder()
            {
                if (listOrder.Count > 0 && mCurren < listOrder.Count)
                {
                    return listOrder[mCurren];
                }
                else
                {
                    return null;
                }
            }

            public Class.ProcessOrderNew.Order NextOrder()
            {
                if (listOrder.Count > 0)
                {
                    mCurren++;
                    if (mCurren >= listOrder.Count)
                    {
                        mCurren = 0;
                    }
                    return listOrder[mCurren];
                }
                return null;
            }
        }

        private class ListOrderTemp
        {
            private List<Class.ProcessOrderNew.Order> listOrderTemp;
            private int mCurren = 0;

            public ListOrderTemp()
            {
                listOrderTemp = new List<Class.ProcessOrderNew.Order>();
            }

            public int CountListOrderTemp()
            {
                return listOrderTemp.Count;
            }

            public void AddOrder(Class.ProcessOrderNew.Order order)
            {
                if (!listOrderTemp.Contains(order))
                {
                    //listOrder.Add(order.CoppyOrder());
                    listOrderTemp.Add(order);
                }
            }

            public void RemoveOrdrt(Class.ProcessOrderNew.Order order)
            {
                if (listOrderTemp.Contains(order))
                {
                    listOrderTemp.Remove(order);
                }
            }

            public Class.ProcessOrderNew.Order CurrenOrder()
            {
                if (listOrderTemp.Count > 0 && mCurren < listOrderTemp.Count)
                {
                    return listOrderTemp[mCurren];
                }
                else
                {
                    return null;
                }
            }

            public Class.ProcessOrderNew.Order NextOrder()
            {
                if (listOrderTemp.Count > 0)
                {
                    mCurren++;
                    if (mCurren >= listOrderTemp.Count)
                    {
                        mCurren = 0;
                    }
                    return listOrderTemp[mCurren];
                }
                return null;
            }
        }

        private void uCkeypad1_Load(object sender, EventArgs e)
        {
        }

        private void frmOrdersAll_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Diagnostics.Process[] prs = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pr in prs)
            {
                //Console.WriteLine(pr.ProcessName);
                if (pr.ProcessName == "POS")
                    pr.Kill();
            }
        }

        private void txtunitprice_TextChanged(object sender, EventArgs e)
        {
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            if (txtunitprice.Text != "" && txtunitprice.Text != "-")
            {
                double price = Convert.ToDouble(txtunitprice.Text);
                if (price > 1000)
                {
                    lblStatus.Text = "Unit Price not more than 1000";
                    txtunitprice.Text = "0";
                    return;
                }
            }
            if (mLocketText)
            {
                return;
            }
            //ChangeQtyAndPrice();
            try
            {
                ItemClick item = (ItemClick)listView1.SelectedItems[0].Tag;
                if (item.ItemOptionIndex == -1)
                {
                    //order.ListItem[item.ItemIndex].Qty = Convert.ToInt16(txtqty.Text);
                    order.ListItem[item.ItemIndex].Price = money.getFortMat(txtunitprice.Text);
                    order.ListItem[item.ItemIndex].SubTotal = order.ListItem[item.ItemIndex].Qty * order.ListItem[item.ItemIndex].Price;
                    listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text = txtqty.Text;
                    listView1.Items[listView1.SelectedIndices[0]].SubItems[2].Text = money.Format(order.ListItem[item.ItemIndex].SubTotal);
                }
                else
                {
                    //order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Qty = Convert.ToInt16(txtqty.Text);
                    order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Price = money.getFortMat(txtunitprice.Text);
                    order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].SubTotal = order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Qty * order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].Price;
                    listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text = txtqty.Text;
                    listView1.Items[listView1.SelectedIndices[0]].SubItems[2].Text = money.Format(order.ListItem[item.ItemIndex].ListSubItem[item.ItemOptionIndex].SubTotal);
                }
                //LoadListView(order);
                //TotalLabel();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("txtunitprice_TextChanged::" + ex.Message);
            }
        }

        private void TotalLabel()
        {
            //double total = 0;
            //foreach (ListViewItem item in listView1.Items)
            //{
            //    if (item.SubItems[2].Text!="")
            //    {
            //        total += Convert.ToDouble(item.SubItems[2].Text);
            //    }
            //}
            //lblTotal.Text = total + "";
            SetText("$" + money.Convert3to2(Convert.ToDouble(money.Format(order.getSubTotal()))), lblTotal);
            //lblTotal.Text = ;
        }

        private void ChangeQtyAndPrice()
        {
            double price = 0;
            if (txtunitprice.Text != "")
            {
                price = Convert.ToDouble(txtunitprice.Text);
            }
            int qty = 0;
            if (txtqty.Text != "")
            {
                qty = Convert.ToInt32(txtqty.Text);
            }
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedItems[0].SubItems[2].Text = (qty * price) + "";
                listView1.SelectedItems[0].SubItems[0].Text = qty + "";
            }
        }

        private void button31_Click(object sender, EventArgs e)
        {
        }

        public void SetStaff(int staffID, string staffName)
        {
            mStaffID = staffID;
            txtstaffid.Text = staffName;
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {
        }

        public Barcode.SerialPort.PortReceiveFuelEvenHandler mSerialPortFuel_Received { get; set; }

        private void btngroupup_Click(object sender, EventArgs e)
        {
            mIndex -= 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                if (LoadItem(tlp_groups.Controls[mGroupIndex].Name) == false)
                {
                    for (int i = 0; i < 21; i++)
                    {
                        tlp_items.Controls[i].Text = "";
                        tlp_items.Controls[i].Tag = null;
                        tlp_items.Controls[i].BackColor = Color.White;
                        tlp_items.Controls[i].Enabled = false;
                    }
                }
            }
        }

        private void btngroupdown_Click(object sender, EventArgs e)
        {
            mIndex += 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                if (LoadItem(tlp_groups.Controls[mGroupIndex].Name) == false)
                {
                    for (int i = 0; i < 21; i++)
                    {
                        tlp_items.Controls[i].Text = "";
                        tlp_items.Controls[i].Tag = null;
                        tlp_items.Controls[i].BackColor = Color.White;
                        tlp_items.Controls[i].Enabled = false;
                    }
                }
            }
        }

        private void btnitemup_Click(object sender, EventArgs e)
        {
            if (chkoption == 1)
            {
                if (btnoption.Tag == null || btnoption.Tag.ToString() == "1" || btnoption.Tag.ToString() == "2")
                {
                    yIndex -= 21;
                    LoadLineItem(mItem.ItemID);
                }
            }
            else
            {
                zIndex -= 21;
                LoadItem(mGroup.GroupID);
            }
        }

        private void btnitemdown_Click(object sender, EventArgs e)
        {
            if (chkoption == 1)
            {
                if (btnoption.Tag == null || btnoption.Tag.ToString() == "1" || btnoption.Tag.ToString() == "2")
                {
                    yIndex += 21;
                    LoadLineItem(mItem.ItemID);
                }
            }
            else
            {
                zIndex += 21;
                LoadItem(mGroup.GroupID);
            }
        }

        private void btnDepartment8_Click(object sender, EventArgs e)
        {
            //frmOrderLayout frm = new frmOrderLayout();
            //frm.POSLoadDelivery();
            //if (frm.ShowDialog()==DialogResult.OK)
            //{
            //    order = processOrder.GetOrderByOrderID(listView1,frm.OrderID, money);
            //}
            ReLoadGroup();
            Class.LogPOS.WriteLog("frmOrderAll.Button_Send Order_Click.");
            if (order.Completed != 0)
            {
                lblStatus.Text = "Order can't send to kitchen";
                return;
            }
            if (order.ListItem.Count == 0)
            {
                lblStatus.Text = "List item empty";
                return;
            }
            if (textBox1.Text != "")
            {
                order.NumPeople = Convert.ToInt32(textBox1.Text);
            }

            order.CustCode = strMemberNo;

            order.Customer.CustID = intCusID;
            if (order.Customer.CustID != 0)
            {
                label9.Visible = true;
                txtCusName.Visible = true;

                if (intCusType == 1)
                {
                    order.Delivery = 1;
                    order.DeliveryFee = dbconfig.DeliveryAmount;
                }
                else if (intCusType == 2)
                {
                    order.Pickup = 1;
                }

                try
                {
                    conn.Open();
                    txtCusName.Text = Convert.ToString(conn.ExecuteScalar("SELECT Name FROM customers where memberNo = '" + strMemberNo + "'"));
                }
                catch (Exception)
                {
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                label9.Visible = false;
                txtCusName.Visible = false;
            }
            printServer.KitchenName = setprinter.getkitchenprinter();
            processOrder.SendOrder(order);

            mListOrder.RemoveOrdrt(order);
            mListOrderTemp.RemoveOrdrt(order);

            NextCustomer();
        }

        private void btnDepartment7_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            frmOrderLayout frm = new frmOrderLayout();
            frm.POSLoadDelivery();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                order = processOrder.GetOrderByOrderID(listView1, frm.OrderID, money);
                try
                {
                    conn.Open();
                    txtCusName.Text = Convert.ToString(conn.ExecuteScalar("select Name from customers where memberNo ='" + order.CustCode + "'"));
                    if (txtCusName.Text != "")
                    {
                        label9.Visible = true;
                        txtCusName.Visible = true;
                    }
                    else
                    {
                        label9.Visible = false;
                        txtCusName.Visible = false;
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    conn.Close();
                }

                txtorderid.Text = order.OrderID.ToString();
                lblTotal.Text = money.Format2(order.getSubTotal());
                if (Convert.ToBoolean(order.Delivery))
                {
                    btnBill.Enabled = true;
                }
                else
                {
                    btnBill.Enabled = false;
                }
            }
        }

        private void btnDepartment6_Click(object sender, EventArgs e)
        {
            //ReLoadGroup();
            //frmCust frm = new frmCust(this);
            //frm.ShowDialog();
            Class.LogPOS.WriteLog("frmOrderAll.Button_Bill_Click.");
            ReLoadGroup();
            CheckPreordercompleted(order);
            object orderID = null;
            try
            {
                conn.Open();
                orderID = conn.ExecuteScalar("select orderID from ordersdaily where orderID=" + order.OrderID);
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
            if (orderID == null)
            {
                lblStatus.Text = "Order can not print to Bill !";
                return;
            }
            if (order.Delivery == 1)
            {
                order.DeliveryFee = dbconfig.DeliveryAmount;
            }
            if (order.ListItem.Count > 0)
            {
                processOrder.billOrder(order);
                //mChangeCompleted.Change(1);
                if (!order.TableID.Contains("TKA-"))
                {
                    this.Visible = false;
                }
            }
            NextCustomer();
        }

        private string strMemberNo = "";
        private string strCustomerName = "";
        private int intCusID = 0;
        private int intCusType = 0;

        private void btnDepartment5_Click(object sender, EventArgs e)
        {
            frmFindCustomerByPhone frm = new frmFindCustomerByPhone(conn);
            DialogResult dlg = frm.ShowDialog();
            if (dlg == DialogResult.OK)
            {
                strCustomerName = frm.CustomerName;
                strMemberNo = frm.CustomerCode;
                intCusID = frm.CustomerID;
                intCusType = frm.CustomerType;
                txtCusName.Visible = true;
                label9.Visible = true;
                lbChangeName.Visible = false;
                lbChangeAmount.Visible = false;
                txtCusName.Text = frm.CustomerName;

                if (textBox1.Text != "")
                {
                    order.NumPeople = Convert.ToInt32(textBox1.Text);
                }
                order.CustCode = strMemberNo;
                order.Customer.CustID = intCusID;
                if (order.Customer.CustID != 0)
                {
                    if (intCusType == 1)
                    {
                        order.Delivery = 1;
                        order.DeliveryFee = dbconfig.DeliveryAmount;
                    }
                    else if (intCusType == 2)
                    {
                        order.Pickup = 1;
                    }
                }
            }
            else
            {
                txtCusName.Visible = false;
                label9.Visible = false;
                lbChangeName.Visible = true;
                lbChangeAmount.Visible = true;
            }
        }

        private void btnDepartment1_Click(object sender, EventArgs e)
        {
            ReLoadGroup();
            if (CheckPreordercompleted(order) == true)
            {
                lblStatus.Text = "Order Completed";
                return;
            }
            Button btn = (Button)sender;
            Class.Departnemt depa = (Class.Departnemt)btn.Tag;
            frmNewItems frm = new frmNewItems(listView1, money, intPumpID, Convert.ToInt32(strIDPumpHistory == "" ? "0" : strIDPumpHistory), this);
            frm.Department(depa.FullName, depa.GST);
            frm.ShowDialog();
            //txtqty.Enabled = false;
            txtunitprice.Enabled = false;
        }

        private void txtCusName_TextChanged(object sender, EventArgs e)
        {
            if (txtCusName.Text != "" || txtCusName.Text != null)
            {
                btnSendOrder.Enabled = true;
            }
            else
            {
                btnSendOrder.Enabled = false;
            }
        }
    }
}