﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmLoyatyByItem : Form
    {
        private Connection.Connection mConnection;
        private Class.MoneyFortmat mMoneyFortmat;
        private Barcode.SerialPort mSerialPort;

        public frmLoyatyByItem(Barcode.SerialPort ser)
        {
            InitializeComponent();
            mSerialPort = ser;
        }

        private void frmLoyatyByItem_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.MENU_BARCODE;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            LoadListLoyalty();
        }

        private void mSerialPort_Received(string data)
        {
            if (mSerialPort.TypeOfBarcode == Barcode.SerialPort.MENU_BARCODE)
            {
                mSerialPort.SetText("", txtBarcode);
                mSerialPort.SetText(data, txtBarcode);
            }
        }

        private void LoadListItem(int loyID)
        {
            try
            {
                mConnection.Open();
                lvListItem.Items.Clear();
                DataTable tbl = mConnection.Select("select l.*,(select i.itemDesc from itemsmenu i where i.itemID=l.itemID) as Name,(select i.unitPrice from itemsmenu i where i.itemID=l.itemID) as Price from loyaltybylistitemdetail l where l.loyID=" + loyID);
                foreach (DataRow row in tbl.Rows)
                {
                    Item item = new Item(Convert.ToInt32(row["itemID"]), Convert.ToInt32(row["loyDetailID"]), row["Name"].ToString(), Convert.ToInt16(row["qty"]));
                    item.Price = Convert.ToDouble(row["Price"]);
                    AddItemListView(item);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void LoadListLoyalty()
        {
            try
            {
                mConnection.Open();
                DataTable tbl = mConnection.Select(
                        "select " +
                            "loyID," +
                            "loyName," +
                            "discount," +
                            "qty," +
                            "dateStart," +
                            "if(dateEnd='0000-00-00 00:00:00',null,date(dateEnd)) as dateEnd," +
                            "enable " +
                        "from loyaltybylistitem l "
                    );
                lstListLoyalty.Items.Clear();
                foreach (DataRow row in tbl.Rows)
                {
                    bool enable;
                    if (row["enable"].ToString() == "1")
                    {
                        enable = true;
                    }
                    else
                    {
                        enable = false;
                    }
                    Loyalty loyalty;
                    if (row["dateEnd"] != DBNull.Value)
                    {
                        loyalty = new Loyalty(Convert.ToInt32(row["loyID"]), row["loyName"].ToString(), Convert.ToInt32(row["qty"]), Convert.ToDouble(row["discount"]), Convert.ToDateTime(row["dateStart"]), Convert.ToDateTime(Encoding.Default.GetString((byte[])row["dateEnd"])), enable, true);
                    }
                    else
                    {
                        loyalty = new Loyalty(Convert.ToInt32(row["loyID"]), row["loyName"].ToString(), Convert.ToInt32(row["qty"]), Convert.ToDouble(row["discount"]), Convert.ToDateTime(row["dateStart"]), DateTime.Now, enable, false);
                    }
                    AddItemListView(loyalty);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void AddItemListView(Loyalty loyalty)
        {
            ListViewItem li = new ListViewItem(loyalty.Name);
            li.SubItems.Add(loyalty.Qty + "");
            li.SubItems.Add(mMoneyFortmat.Format2(loyalty.Discount));
            li.SubItems.Add(GetDate(loyalty.DateStart));
            if (loyalty.IsDateEnd)
            {
                li.SubItems.Add(GetDate(loyalty.DateEnd));
            }
            else
            {
                li.SubItems.Add("");
            }
            if (loyalty.Enable)
            {
                li.SubItems.Add("Enable");
            }
            else
            {
                li.SubItems.Add("Disable");
            }
            li.Tag = loyalty;
            li.Selected = true;
            lstListLoyalty.Items.Add(li);
        }

        private void AddItemListView(Item item)
        {
            ListViewItem li = new ListViewItem(item.ItemName);
            li.SubItems.Add(mMoneyFortmat.Format2(item.Price));
            li.Tag = item;
            li.Selected = true;
            lvListItem.Items.Add(li);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            this.DialogResult = DialogResult.Cancel;
        }

        private string GetDate(DateTime dt)
        {
            return dt.Day + "/" + dt.Month + "/" + dt.Year;
        }

        private string GetDateData(DateTime dt, bool isDate)
        {
            if (isDate)
            {
                return dt.Year + "-" + dt.Month + "-" + dt.Day;
            }
            else
            {
                return "0000-00-00";
            }
        }

        private void lstListLoyalty_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstListLoyalty.SelectedIndices.Count > 0)
            {
                Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                SetTextData(loy);
                LoadListItem(loy.LoyID);
            }
        }

        private void SetTextData(Loyalty loy)
        {
            btnSave.Tag = 1;
            txtLoyaltyName.Text = loy.Name;
            txtDiscount.Text = mMoneyFortmat.Format2(loy.Discount);
            dtpStart.Value = loy.DateStart;
            chkEnd.Checked = loy.IsDateEnd;
            dtpEnd.Enabled = loy.IsDateEnd;
            dtpEnd.Value = loy.DateEnd;
            chkEnable.Checked = loy.Enable;
            txtDiscountQty.Text = loy.Qty + "";
        }

        private void SetTextData(Item item)
        {
            btnItemSave.Tag = 1;
            txtItemName.Text = item.ItemName;
            txtQty.Text = item.Qty + "";
        }

        private void GetTextData(Loyalty loy)
        {
            loy.Qty = 1;
            if (txtDiscountQty.Text != "")
            {
                loy.Qty = Convert.ToInt32(txtDiscountQty.Text);
            }
            loy.Name = txtLoyaltyName.Text;
            loy.Discount = mMoneyFortmat.getFortMat(txtDiscount.Text);
            loy.DateStart = dtpStart.Value;
            loy.DateEnd = dtpEnd.Value;
            loy.Enable = chkEnable.Checked;
            loy.IsDateEnd = chkEnd.Checked;
        }

        private void GetTextData(Item item)
        {
            item.Qty = 1;
            if (txtQty.Text != "")
            {
                item.Qty = Convert.ToInt16(txtQty.Text);
            }
        }

        private void ClearTextData()
        {
            lstListLoyalty.SelectedIndices.Clear();
            txtDiscountQty.Text = "";
            txtItemName.Text = "";
            txtQty.Text = "";
            lvListItem.Items.Clear();
            btnSave.Tag = null;
            txtLoyaltyName.Text = "";
            txtDiscount.Text = "";
            chkEnable.Checked = true;
            chkEnd.Checked = false;
            dtpStart.Value = dtpEnd.Value = DateTime.Now;
        }

        private void chkEnd_CheckedChanged(object sender, EventArgs e)
        {
            dtpEnd.Enabled = chkEnd.Checked;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lstListLoyalty.SelectedIndices.Count > 0)
            {
                Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();

                    mConnection.ExecuteNonQuery("delete from loyaltybylistitem where loyID=" + loy.LoyID);
                    mConnection.ExecuteNonQuery("delete from loyaltybylistitemdetail where loyID=" + loy.LoyID);
                    lstListLoyalty.Items.RemoveAt(lstListLoyalty.SelectedIndices[0]);
                    ClearTextData();

                    mConnection.Commit();
                }
                catch (Exception)
                {
                    mConnection.Rollback();
                }
                finally
                {
                    mConnection.Close();
                }
            }
            LoadListLoyalty();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearTextData();
        }

        private bool CheckDiscount()
        {
            double money = -1;
            bool first = true;
            foreach (ListViewItem li in lvListItem.Items)
            {
                Item i = (Item)li.Tag;
                if (i != null)
                    if (first)
                    {
                        money = i.Price / 1000;
                        first = false;
                    }
                if (i.Price / 1000 < money)
                    money = i.Price / 1000;
            }
            int qty = txtDiscountQty.Text != "" ? Convert.ToInt32(txtDiscountQty.Text) : 0;
            double dis = txtDiscount.Text != "" ? Convert.ToDouble(txtDiscount.Text) : 0;
            if (money > 0 && money * qty >= dis)
            {
                lbStatus.Text = "";

                return true;
            }
            else
            {
                lbStatus.Text = "Discount max " + mMoneyFortmat.Format2(money * qty * 1000);
                return false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!CheckDiscount())
            //    return;
            if (btnSave.Tag == null)
            {
                Loyalty loy = new Loyalty();
                GetTextData(loy);
                try
                {
                    mConnection.Open();
                    //mConnection.BeginTransaction();

                    mConnection.ExecuteNonQuery("insert into loyaltybylistitem(loyName,discount,dateStart,dateEnd,enable,qty) values('" + loy.Name + "'," + loy.Discount + ",'" + GetDateData(loy.DateStart, true) + "','" + GetDateData(loy.DateEnd, loy.IsDateEnd) + "'," + loy.Enable + "," + loy.Qty + ")");
                    loy.LoyID = Convert.ToInt32(mConnection.ExecuteScalar("select max(loyID) from loyaltybylistitem"));
                    AddItemListView(loy);
                    btnSave.Tag = 1;
                    //mConnection.Commit();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(":::" + ex.Message);
                    mConnection.Rollback();
                }
                finally
                {
                    mConnection.Close();
                }
            }
            else
            {
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();

                    if (lstListLoyalty.SelectedIndices.Count > 0)
                    {
                        Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                        GetTextData(loy);
                        int enable = 0;
                        if (loy.Enable)
                        {
                            enable = 1;
                        }
                        mConnection.ExecuteNonQuery("update loyaltybylistitem set loyName='" + loy.Name + "',discount=" + loy.Discount + ",dateStart='" + GetDateData(loy.DateStart, true) + "',dateEnd='" + GetDateData(loy.DateEnd, loy.IsDateEnd) + "',enable=" + enable + ",qty=" + loy.Qty + " where loyID=" + loy.LoyID);
                    }

                    mConnection.Commit();
                }
                catch (Exception ex)
                {
                    mConnection.Rollback();
                    Class.LogPOS.WriteLog(":::" + ex.Message);
                }
                finally
                {
                    mConnection.Close();
                }
            }
            LoadListLoyalty();
        }

        private class Loyalty
        {
            public int LoyID { get; set; }

            public string Name { get; set; }

            public double Discount { get; set; }

            public int Qty { get; set; }

            public DateTime DateStart { get; set; }

            public DateTime DateEnd { get; set; }

            public bool Enable { get; set; }

            public bool IsDateEnd { get; set; }

            public Loyalty(int loyID, string name, int qty, double discount, DateTime dateStart, DateTime dateEnd, bool enable, bool isDateEnd)
            {
                LoyID = loyID;
                Name = name;
                Qty = qty;
                Discount = discount;
                DateStart = dateStart;
                DateEnd = dateEnd;
                Enable = enable;
                IsDateEnd = isDateEnd;
            }

            public Loyalty()
            { }
        }

        private class Item
        {
            public int LoyDetailID { get; set; }

            public int ItemID { get; set; }

            public string ItemName { get; set; }

            public int Qty { get; set; }

            public double Price { get; set; }

            public Item(int itemID, int loyDetailID, string itemName, int qty)
            {
                ItemID = itemID;
                LoyDetailID = loyDetailID;
                ItemName = itemName;
                Qty = qty;
            }
        }

        private void btnItemNew_Click(object sender, EventArgs e)
        {
            if (lstListLoyalty.SelectedIndices.Count > 0)
            {
                frmSearchBarCode frm = new frmSearchBarCode(DataObject.FindItemType.FindItemInOrder);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        mConnection.Open();
                        mConnection.BeginTransaction();
                        Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                        Item item = new Item(frm.ItemMenu.ItemID, 0, frm.ItemMenu.ItemName, 1);
                        item.Price = frm.ItemMenu.Price;
                        if (!CheckListItem(item))
                        {
                            mConnection.ExecuteNonQuery("insert into loyaltybylistitemdetail(loyID,itemID,qty) values(" + loy.LoyID + "," + item.ItemID + "," + item.Qty + ")");
                            item.LoyDetailID = Convert.ToInt16(mConnection.ExecuteScalar("select max(loyDetailID) from loyaltybylistitemdetail"));
                            AddItemListView(item);
                        }

                        mConnection.Commit();
                    }
                    catch (Exception ex)
                    {
                        Class.LogPOS.WriteLog("loyaltybyitem:::" + ex.Message);
                        mConnection.Rollback();
                    }
                    finally
                    {
                        mConnection.Close();
                    }
                }
            }
        }

        private void lstListItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvListItem.SelectedIndices.Count > 0)
            {
                Item item = (Item)lvListItem.SelectedItems[0].Tag;
                SetTextData(item);
            }
        }

        private void btnItemSave_Click(object sender, EventArgs e)
        {
            if (lvListItem.SelectedIndices.Count > 0)
            {
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();
                    Item item = (Item)lvListItem.SelectedItems[0].Tag;
                    GetTextData(item);
                    mConnection.ExecuteNonQuery("update loyaltybylistitemdetail set qty=" + item.Qty + " where loyDetailID=" + item.LoyDetailID);
                    mConnection.Commit();
                }
                catch (Exception)
                {
                    mConnection.Rollback();
                }
                finally
                {
                    mConnection.Close();
                }
                if (lstListLoyalty.SelectedIndices.Count > 0)
                {
                    Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                    LoadListItem(loy.LoyID);
                }
            }
        }

        private void btnItemDelete_Click(object sender, EventArgs e)
        {
            if (lvListItem.SelectedIndices.Count > 0)
            {
                try
                {
                    mConnection.Open();
                    mConnection.BeginTransaction();
                    Item item = (Item)lvListItem.SelectedItems[0].Tag;
                    mConnection.ExecuteNonQuery("delete from loyaltybylistitemdetail where loyDetailID=" + item.LoyDetailID);
                    mConnection.Commit();
                }
                catch (Exception)
                {
                    mConnection.Rollback();
                }
                finally
                {
                    mConnection.Close();
                }
                if (lstListLoyalty.SelectedIndices.Count > 0)
                {
                    Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                    LoadListItem(loy.LoyID);
                }
            }
        }

        private bool CheckListItem(Item itemCheck)
        {
            foreach (ListViewItem li in lvListItem.Items)
            {
                try
                {
                    Item item = (Item)li.Tag;
                    if (item.ItemID == itemCheck.ItemID)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                }
            }
            return false;
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbStatus.Text = "";
                if (lstListLoyalty.Items.Count == 0)
                {
                    lbStatus.Text = "Please create group !!!";
                    return;
                }
                else if (lstListLoyalty.SelectedIndices.Count == 0)
                {
                    lbStatus.Text = "Please chose group !!!";
                    return;
                }
                else if (txtBarcode.Text != "")
                {
                    try
                    {
                        mConnection.Open();
                        mConnection.BeginTransaction();
                        Loyalty loy = (Loyalty)lstListLoyalty.SelectedItems[0].Tag;
                        DataTable tbl = mConnection.Select("select itemDesc,itemID from itemsmenu where scanCode='" + txtBarcode.Text + "' limit 0,1");
                        if (tbl.Rows.Count > 0)
                        {
                            Item item = new Item(Convert.ToInt32(tbl.Rows[0]["itemID"]), 0, tbl.Rows[0]["itemDesc"].ToString(), 1);
                            if (!CheckListItem(item))
                            {
                                mConnection.ExecuteNonQuery("insert into loyaltybylistitemdetail(loyID,itemID,qty) values(" + loy.LoyID + "," + item.ItemID + "," + item.Qty + ")");
                                item.LoyDetailID = Convert.ToInt16(mConnection.ExecuteScalar("select max(loyDetailID) from loyaltybylistitemdetail"));
                                AddItemListView(item);
                            }
                        }
                        else
                        {
                            lbStatus.Text = "Item not found !!!";
                        }
                        mConnection.Commit();
                    }
                    catch (Exception)
                    {
                        mConnection.Rollback();
                    }
                    finally
                    {
                        mConnection.Close();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtDiscountQty_TextChanged(object sender, EventArgs e)
        {
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            if (chkEnd.Checked)
            {
                TimeSpan timeSpan = dtpEnd.Value - dtpStart.Value;
                if (timeSpan.TotalMilliseconds < 0)
                {
                    dtpEnd.Value = dtpStart.Value;
                    return;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtBarcode.Text = "8930052483280";
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }
    }
}