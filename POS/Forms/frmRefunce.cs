﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmRefunce : Form
    {
        private Class.MoneyFortmat mMoney;
        private Connection.Connection mConnection;
        private Connection.ReadDBConfig mReadDBConfig;
        private Class.Setting mSetTing;
        private string mOrderID;
        private double mDiscount;
        private double mSubTotal;
        private Printer mPrinter;
        private int mOrder = 0;
        private int mBk = 0;
        private DataTable mDataTable = new DataTable();
        private string mCreditNodeCode = "";
        private string mOrderIDText = "";
        private string mCableIDText = "";
        private Class.ProcessOrderNew.Order mOrderData;
        //private bool mIsOrdersAll=false;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmRefunce(Class.ProcessOrderNew.Order order, Class.MoneyFortmat money)
        {
            InitializeComponent();
            mOrderID = order.OrderID + "";
            mOrderData = order;
            mMoney = money;
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                this.Text = "Hoàn trả sản phẩm";
                label6.Text = "Ngày:";
                label9.Text = "Ca LV:";
                buttonFindOrder.Text = "Tìm kiếm Order";
                label3.Text = "Tên SP:";
                label4.Text = "Số lượng:";
                label5.Text = "Giá";
                label1.Text = "Tống cộng:";
                label2.Text = "Còn nợ:";
                btnAcept.Text = "Chấp nhận";
                btnBack.Text = "Quay lại";
                columnHeader1.Text = "SL";
                columnHeader2.Text = "Sản phẩm";
                columnHeader3.Text = "Tống cộng";
                columnHeader4.Text = "SL";
                columnHeader5.Text = "Sản phẩm";
                columnHeader6.Text = "Tổng cộng";
                groupBox1.Text = "Hoàn trả";
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void frmRefunce_Load(object sender, EventArgs e)
        {
            //Duc2052013
            txtCable.Text = "1";

            //43 194 59
            mConnection = new Connection.Connection();
            mPrinter = new Printer();
            mSetTing = new Class.Setting();
            mReadDBConfig = new Connection.ReadDBConfig();
            mPrinter.SetPrinterName(mSetTing.GetBillPrinter());
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            listView1.Columns[0].Width = listView2.Columns[0].Width = 43;
            listView1.Columns[1].Width = listView2.Columns[1].Width = 194;
            listView1.Columns[2].Width = listView2.Columns[2].Width = 59;
            listView1.Columns[2].TextAlign = listView2.Columns[2].TextAlign = HorizontalAlignment.Right;
            ClearTextBox();
            txtShift.Text = "1";
            txtShift.Text = mOrderData.ShiftID + "";
            if (mOrderData.Completed == 1)
            {
                txtOrderID.Text = mOrderData.OrderID + "";
                txtCable.Text = mOrderData.CableID + "";
                dateTimePickerfrom.Value = mOrderData.DateOrder;
                buttonFindOrder_Click(null, null);
            }
            //try
            //{
            //    //System.Data.DataTable tbl = mConnection.Select("select * from ordersdaily where orderID=" + mOrderID + " and refun=0");
            //    System.Data.DataTable tbl = mConnection.Select("select * from ordersdaily where orderID=" + mOrderID);
            //    if (tbl.Rows.Count>0)
            //    {
            //        mDiscount = Convert.ToDouble(tbl.Rows[0]["discount"]);
            //        mSubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
            //        double percent = 0;
            //        if (mSubTotal>0)
            //        {
            //            percent= mDiscount / mSubTotal;
            //        }
            //        //System.Data.DataTable tblItem = mConnection.Select("select ol.refund,ol.lineID,ol.qty,ol.subTotal,i.itemDesc from ordersdailyline ol inner join itemsmenu i on ol.itemID=i.itemID where orderID=" + mOrderID);
            //        System.Data.DataTable tblItem = mConnection.Select("select ol.refund,ol.lineID,ol.qty,ol.subTotal,if(ol.itemID<>0,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.dynID<>0,(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID),(select i.itemDesc from itemslinemenu i where i.lineID=ol.optionID))) as itemDesc from ordersdailyline ol where ol.orderID=" + mOrderID);
            //        foreach (System.Data.DataRow row in tblItem.Rows)
            //        {
            //            ListViewItem li = new ListViewItem(row["qty"].ToString());
            //            li.SubItems.Add(row["itemDesc"].ToString());
            //            li.SubItems.Add(mMoney.Format(Convert.ToDouble(row["subTotal"]) * (1 - percent)));
            //            li.Tag = row["lineID"];
            //            if (row["refund"].ToString()=="1")
            //            {
            //                li.BackColor = Color.Green;
            //                li.Tag = null;
            //                li.SubItems[2].Text = "Refunded";
            //            }
            //            listView1.Items.Add(li);
            //        }
            //    }
            //    CheckSubTotal();
            //}
            //catch (Exception ex)
            //{
            //    Class.LogPOS.WriteLog("frmRefun.frmRefunce_Load:::" + ex.Message);
            //}
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float y = 0;
            y = mPrinter.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 18), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 11), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y = mPrinter.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 10, FontStyle.Italic), y, 2);
            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            DateTime dateTime = DateTime.Now;
            y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);
            mPrinter.DrawString("ORDER#" + mOrderIDText, e, new Font("Arial", 11), y, 1);
            y = mPrinter.DrawString("TERMINAL#" + mCableIDText, e, new Font("Arial", 11), y, 3);

            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            y = mPrinter.DrawString("CREDIT NOTE", e, new Font("Arial", 18, FontStyle.Bold), y, 2);

            y += 100;

            y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += 100;

            y = mPrinter.DrawString("Code # " + mCreditNodeCode, e, new Font("Arial", 11), y, 1);
            y = mPrinter.DrawString("Amount : $" + mMoney.FormatCurenMoney(Convert.ToDouble(lblTotal.Text)), e, new Font("Arial", 11), y, 1);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                txtQty.Text = listView1.SelectedItems[0].SubItems[0].Text;
                txtItemName.Text = listView1.SelectedItems[0].SubItems[1].Text;
                txtPrice.Text = listView1.SelectedItems[0].SubItems[2].Text;
                //txtItemName.Tag = listView1.SelectedItems[0].Tag;
            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            int qty = 1;
            try
            {
                qty = Convert.ToInt16(txtQty.Text);
                if (qty > Convert.ToInt16(listView1.SelectedItems[0].SubItems[0].Text) || qty <= 0)
                {
                    qty = 1;
                }
                double price = Convert.ToDouble(listView1.SelectedItems[0].SubItems[2].Text);
                int qty1 = Convert.ToInt16(listView1.SelectedItems[0].SubItems[0].Text);
                if (qty1 > 0)
                {
                    txtPrice.Text = price / qty1 * qty + "";
                }
                else
                {
                    txtPrice.Text = "0";
                }
                txtQty.Text = qty + "";
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmRefunce.txtQty_TextChanged:::" + ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //if (txtQty.Text!="" && txtPrice.Text!="")
                //{
                //    ListViewItem li = new ListViewItem(txtQty.Text);
                //    li.SubItems.Add(txtItemName.Text);
                //    li.SubItems.Add(txtPrice.Text);
                //    listView2.Items.Add(li);

                //    if (listView1.SelectedItems[0].SubItems[0].Text == txtQty.Text)
                //    {
                //        listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                //    }
                //    else
                //    {
                //        int qty = Convert.ToInt16(listView1.SelectedItems[0].SubItems[0].Text);
                //        double price = 0;
                //        if (qty > 0)
                //        {
                //            price = Convert.ToDouble(listView1.SelectedItems[0].SubItems[2].Text) / qty;
                //        }
                //        qty = qty - Convert.ToInt16(txtQty.Text);
                //        listView1.SelectedItems[0].SubItems[0].Text = qty + "";
                //        listView1.SelectedItems[0].SubItems[2].Text = price * qty + "";
                //    }
                //    ClearTextBox();
                //    CheckSubTotal();
                //}
                if (listView1.SelectedIndices.Count > 0)
                {
                    ListViewItem li = listView1.SelectedItems[0];
                    if (li.Tag == null)
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            lblError.Text = "Sản phẩm này đã được hoàn trả !";
                            return;
                        }
                        lblError.Text = "This item was refunded !";
                        return;
                    }
                    listView1.Items.Remove(li);
                    listView2.Items.Add(li);
                    ClearTextBox();
                    CheckSubTotal();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmRefunce.btnAdd_Click:::" + ex.Message);
            }
        }

        private void btnAddAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView1.Items)
            {
                //listView2.Items.Add((ListViewItem)li.Clone());
                if (li.Tag != null)
                {
                    listView1.Items.Remove(li);
                    listView2.Items.Add(li);
                }
            }
            //listView1.Items.Clear();
            ClearTextBox();
            CheckSubTotal();
        }

        private void btnDivAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView2.Items)
            {
                listView1.Items.Add((ListViewItem)li.Clone());
            }
            listView2.Items.Clear();
            ClearTextBox();
            CheckSubTotal();
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                ListViewItem li = listView2.SelectedItems[0];
                listView2.Items.Remove(li);
                listView1.Items.Add(li);
                ClearTextBox();
                CheckSubTotal();
            }
        }

        private void ClearTextBox()
        {
            lblError.Text = "";
            txtQty.Text = "";
            txtItemName.Text = "";
            txtPrice.Text = "";
        }

        private void CheckSubTotal()
        {
            double total = 0;
            foreach (ListViewItem li in listView2.Items)
            {
                total += Convert.ToDouble(li.SubItems[2].Text);
            }
            lblTotal.Text = mMoney.FormatCurenMoney2(Class.Round.RoundSubTotal(total));
        }

        private string[] strOrderline;

        private void btnAcept_Click(object sender, EventArgs e)
        {
            if (listView2.Items.Count == 0)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Không có sản phẩm để hoàn trả!!";
                    return;
                }
                lblError.Text = "No item to refund!!";
                return;
            }
            if (mOrder != 0 && mBk != 0)
            {
                return;
            }
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                if (mOrder != 0)
                {
                    int efpos = 0;
                    efpos = int.Parse(mMoney.getFortMat(lblTotal.Text).ToString()) / 10;
                    if (efpos != 0)
                    {
                        frmChooseRefund frm = new frmChooseRefund(efpos);
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            string sql = "update ordersdailyline set refund=1 where lineID in (";
                            string lineID = "";
                            foreach (ListViewItem li in listView2.Items)
                            {
                                lineID += li.Tag.ToString() + ",";
                            }
                            if (lineID.Length > 0)
                            {
                                lineID = lineID.Substring(0, lineID.Length - 1);
                                sql = sql + lineID + ")";
                                mConnection.ExecuteNonQuery(sql);
                            }
                            mConnection.ExecuteNonQuery("update ordersdaily set ts=ts,refun=" + mMoney.getFortMat(lblTotal.Text) + " where orderID=" + mOrder);
                            mConnection.ExecuteNonQuery("insert into receivedstock(TypeReceive) values(2)");
                            int receivedID = Convert.ToInt16(mConnection.ExecuteScalar("select max(receivedID) from receivedstock"));
                            mConnection.ExecuteNonQuery(
                                "INSERT INTO recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) " +
                                "SELECT " + receivedID + " AS receivedID,itemID,qty,(SELECT max(costPrice) FROM recvstocklines where itemID = OAL.itemID) AS costPrice,qty AS AvailableQuantity,OAL.price AS SellPrice " +
                                "FROM ordersdailyline AS OAL " +
                                "WHERE lineID IN (" + lineID + ")"
                            );
                            PrintRefund();
                        }
                        
                    }
                    else
                    {
                        string sql = "update ordersdailyline set refund=1 where lineID in (";
                        string lineID = "";
                        foreach (ListViewItem li in listView2.Items)
                        {
                            lineID += li.Tag.ToString() + ",";
                        }
                        if (lineID.Length > 0)
                        {
                            lineID = lineID.Substring(0, lineID.Length - 1);
                            sql = sql + lineID + ")";
                            mConnection.ExecuteNonQuery(sql);
                        }
                        mConnection.ExecuteNonQuery("update ordersdaily set ts=ts,refun=" + mMoney.getFortMat(lblTotal.Text) + " where orderID=" + mOrder);
                        mConnection.ExecuteNonQuery("insert into receivedstock(TypeReceive) values(2)");
                        int receivedID = Convert.ToInt16(mConnection.ExecuteScalar("select max(receivedID) from receivedstock"));
                        mConnection.ExecuteNonQuery(
                            "INSERT INTO recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) " +
                            "SELECT " + receivedID + " AS receivedID,itemID,qty,(SELECT max(costPrice) FROM recvstocklines where itemID = OAL.itemID) AS costPrice,qty AS AvailableQuantity,OAL.price AS SellPrice " +
                            "FROM ordersdailyline AS OAL " +
                            "WHERE lineID IN (" + lineID + ")"
                        );
                        PrintRefund();
                    }
                    
                }
                else if (mBk != 0)
                {
                    string sql = "update ordersallline set refund=1 where lineID in (";
                    string lineID = "";
                    foreach (ListViewItem li in listView2.Items)
                    {
                        lineID += li.Tag.ToString() + ",";
                    }
                    if (lineID.Length > 0)
                    {
                        lineID = lineID.Substring(0, lineID.Length - 1);
                        sql = sql + lineID + ")";
                        mConnection.ExecuteNonQuery(sql);

                        mConnection.ExecuteNonQuery("insert into receivedstock(TypeReceive) values(2)");
                        int receivedID = Convert.ToInt16(mConnection.ExecuteScalar("select max(receivedID) from receivedstock"));
                        mConnection.ExecuteNonQuery(
                            "INSERT INTO recvstocklines(receivedID,itemID,qty,costPrice,AvailableQuantity,SellPrice) " +
                            "SELECT " + receivedID + " AS receivedID,itemID,qty,(SELECT max(costPrice) FROM recvstocklines where itemID = OAL.itemID) AS costPrice,qty AS AvailableQuantity,OAL.price AS SellPrice " +
                            "FROM ordersallline AS OAL " +
                            "WHERE lineID IN (" + lineID + ")"
                        );
                    }
                    mConnection.ExecuteNonQuery("update ordersall set ts=ts,refun=" + mMoney.getFortMat(lblTotal.Text) + " where bkId=" + mBk);
                    PrintRefund();
                }
                //mCreditNodeCode=CreateNodeCode();
                //mConnection.ExecuteNonQuery("insert into creditnote(balance,creditNoteCode) values("+mMoney.getFortMat(lblTotal.Text)+",'"+mCreditNodeCode+"')");
                //mPrinter.Print();
                Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                mConnection.Commit();
            }
            catch (Exception ex)
            {
                mConnection.Rollback();
                Class.LogPOS.WriteLog("frmRefunce.btnAcept_Click:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
            DialogResult = DialogResult.OK;
        }

        private void PrintRefund()
        {
            PrinterNew printer = new PrinterNew();
            printer.PrinterName = mSetTing.GetBillPrinter();
            printer.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printer_PrintPage);
            printer.Print();
        }

        private void printer_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            PrinterNew printer = (PrinterNew)sender;
            float y = 0;
            y = printer.DrawString(mReadDBConfig.Header1, e, new System.Drawing.Font("Arial", 15), y, 2);
            y = printer.DrawString(mReadDBConfig.Header2, e, new System.Drawing.Font("Arial", 10), y, 2);
            y = printer.DrawString(mReadDBConfig.Header3, e, new System.Drawing.Font("Arial", 9, FontStyle.Italic), y, 2);
            y = printer.DrawString(mReadDBConfig.Header4, e, new System.Drawing.Font("Arial", 9, FontStyle.Italic), y, 2);
            y += printer.GetHeightPrinterLine();

            y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += printer.GetHeightPrinterLine();

            DateTime dateTime = DateTime.Now;
            y = printer.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new Font("Arial", 11), y, 3);
            printer.DrawString("ORDER#" + txtOrderID.Text, e, new Font("Arial", 10), y, 1);
            y = printer.DrawString("TERMINAL#" + txtCable.Text, e, new Font("Arial", 10), y, 3);
            y = printer.DrawString("SHIFT#" + txtShift.Text, e, new Font("Arial", 10), y, 1);
            string date =
                    (dateTimePickerfrom.Value.Day < 10 ? "0" + dateTimePickerfrom.Value.Day : dateTimePickerfrom.Value.Day + "") + "/" +
                    (dateTimePickerfrom.Value.Month < 10 ? "0" + dateTimePickerfrom.Value.Month : dateTimePickerfrom.Value.Month + "") + "/" +
                    (dateTimePickerfrom.Value.Year < 10 ? "0" + dateTimePickerfrom.Value.Year : dateTimePickerfrom.Value.Year + "") + " " +
                    dateTimePickerfrom.Value.ToShortTimeString();
            y = printer.DrawString("DATE ORDER#" + date, e, new Font("Arial", 10), y, 1);
            y += printer.GetHeightPrinterLine();

            y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y += printer.GetHeightPrinterLine();

            y = printer.DrawString("REFUND", e, new Font("Arial", 13, FontStyle.Bold), y, 2);

            y += printer.GetHeightPrinterLine() * 2;

            y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, y, 1);
            foreach (ListViewItem item in listView2.Items)
            {
                int weight = Convert.ToInt32(item.SubItems[0].Tag);
                double qty = Convert.ToDouble(item.SubItems[0].Text);
                string strQty = "";
                if (weight != 0)
                {
                    strQty = "(" + qty + "L)";
                }
                else if (qty != 1)
                {
                    strQty = "(" + qty + ")";
                }

                printer.DrawString(item.SubItems[1].Text + " " + strQty, e, new Font("Arial", 9), y, 1);
                y = printer.DrawString(item.SubItems[2].Text, e, new Font("Arial", 9), y, 3);
                y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.Dot, y, 1);
            }

            y += printer.GetHeightPrinterLine() * 2;
            printer.DrawString("  Total:", e, new Font("Arial", 11, FontStyle.Bold), y, 1);
            y = printer.DrawString("$" + lblTotal.Text, e, new Font("Arial", 11, FontStyle.Bold), y, 3);
            y += printer.GetHeightPrinterLine();

            y = printer.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, y, 1);

            y = printer.DrawString(mReadDBConfig.FootNode1, e, new System.Drawing.Font("Arial", 8), y, 2);
            y = printer.DrawString(mReadDBConfig.FootNode2, e, new System.Drawing.Font("Arial", 8), y, 2);
        }

        private string CreateNodeCode()
        {
            DateTime dt = DateTime.Now;
            return "" + dt.Year + dt.Month + dt.Day + dt.Hour + dt.Minute + dt.Second + dt.Millisecond;
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void LoadData(DataTable tbl)
        {
            double percent = 0;
            if (mSubTotal > 0)
            {
                percent = mDiscount / mSubTotal;
            }
            foreach (System.Data.DataRow row in tbl.Rows)
            {
                int weight = Convert.ToInt32(row["weight"]);
                int qty = Convert.ToInt32(row["qty"]);
                string sqty = qty + "";
                if (weight != 0)
                {
                    sqty = ((double)qty / weight) + "";
                }
                ListViewItem li = new ListViewItem(sqty);
                li.SubItems.Add(row["itemDesc"].ToString());
                li.SubItems.Add(mMoney.Format2(Convert.ToDouble(row["subTotal"]) * (1 - percent)));
                li.SubItems[0].Tag = weight;
                li.Tag = row["lineID"];
                if (row["refund"].ToString() == "1")
                {
                    li.BackColor = Color.Green;
                    li.Tag = null;
                    li.SubItems[2].Text = "Refunded";
                }
                listView1.Items.Add(li);
            }
        }

        public Class.ProcessOrderNew.Order FindOrder()
        {
            Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder.");
            Connection.Connection con = new Connection.Connection();
            Class.ProcessOrderNew.Order order = null;
            try
            {
                con.Open();
                string sql = "";
                DataTable tblOrder = mConnection.Select("select * from ordersdaily where date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text + " and cableID=" + txtCable.Text);
                if (tblOrder.Rows.Count == 0)
                {
                    tblOrder = mConnection.Select("select * from ordersall where date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text + " and cableID=" + txtCable.Text);
                    if (tblOrder.Rows.Count > 0)
                    {
                        mBk = Convert.ToInt16(tblOrder.Rows[0]["bkId"]);
                        order = new Class.ProcessOrderNew.Order();
                        order.TableID = tblOrder.Rows[0]["tableID"].ToString();
                        order.SubTotal = Convert.ToDouble(tblOrder.Rows[0]["subTotal"].ToString());
                        order.Completed = Convert.ToInt32(tblOrder.Rows[0]["completed"].ToString());
                        order.OrderID = Convert.ToInt32(tblOrder.Rows[0]["orderID"]);
                        order.Discount = Convert.ToDouble(tblOrder.Rows[0]["discount"]);
                        order.Card = Convert.ToDouble(tblOrder.Rows[0]["eftpos"]);
                        order.Cash = Convert.ToDouble(tblOrder.Rows[0]["cash"]);
                        order.Tendered = order.Cash;
                        sql = "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and d.cableID=" + txtCable.Text + " and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "')) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil " +
                        "from ordersallline ol " +
                        "where ol.bkId=" + mBk;
                    }
                    else
                    {
                        lblError.Text = "Not found!";
                    }
                }
                else
                {
                    mOrder = Convert.ToInt16(tblOrder.Rows[0]["orderID"]);
                    order = new Class.ProcessOrderNew.Order();
                    order.TableID = tblOrder.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tblOrder.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tblOrder.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tblOrder.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tblOrder.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tblOrder.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tblOrder.Rows[0]["cash"]);
                    order.Tendered = order.Cash;
                    sql = "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "ol.pumpHistoryID," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil," +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + mOrder;
                }
                //System.Data.DataTable tbl = con.Select("select * from (select * from ordersdaily where completed=1 order by ts desc limit 0,10) as tmp order by ts desc limit " + index + ",1");
                if (order != null)
                {
                    System.Data.DataTable tblItem = con.Select(sql);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
                                Convert.ToInt32(row["pumpHistoryID"]),
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                            order.ListItem.Add(item);
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(
                                Convert.ToInt32(row["pumpHistoryID"]),
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                Convert.ToDouble(row["gst"]),
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                order.ListItem.Add(item);
                            }
                            else
                            {
                                Class.ProcessOrderNew.SubItem subitem = new Class.ProcessOrderNew.SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return order;
        }

        private void buttonFindOrder_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (txtCable.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Cable rỗng";
                    return;
                }
                lblError.Text = "Cable Empty";
                return;
            }
            if (txtOrderID.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Order rỗng";
                    return;
                }
                lblError.Text = "Order Empty";
                return;
            }
            if (txtShift.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblError.Text = "Ca làm việc rỗng";
                    return;
                }
                lblError.Text = "Shift Empty";
                return;
            }
            try
            {
                listView1.Items.Clear();
                listView2.Items.Clear();
                mConnection.Open();
                mOrderIDText = txtOrderID.Text;
                mCableIDText = txtCable.Text;

                //! Minh Tiến bỏ 24/01/2014, lý do: có điều kiện shift, nhìu máy shift không bít lấy được, bỏ cả cableid
                //string sql =
                //    "select * from (" +
                //    "select 0 as bkId,orderID,discount,subTotal,shiftID from ordersdaily where (completed=1 or completed=2) and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text + " and cableID=" + txtCable.Text + " and shiftID=" + txtShift.Text + " " +
                //    "union all " +
                //    "select bkId,orderID,discount,subTotal,shiftID from ordersall where (completed=1 or completed=2) and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text + " and cableID=" + txtCable.Text + " and shiftID=" + txtShift.Text + " " +
                //    ") as tmp " +
                //    "limit 0,1 ";
                string sql =
                    "select * from (" +
                    "select 0 as bkId,orderID,discount,subTotal,shiftID from ordersdaily where (completed=1 or completed=2) and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text +  " " +
                    "union all " +
                    "select bkId,orderID,discount,subTotal,shiftID from ordersall where (completed=1 or completed=2) and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and orderID=" + txtOrderID.Text + " " +
                    ") as tmp " +
                    "limit 0,1 ";

                DataTable tbl = mConnection.Select(sql);
                if (tbl.Rows.Count > 0)
                {
                    mBk = Convert.ToInt16(tbl.Rows[0]["bkId"]);
                    mOrder = Convert.ToInt16(tbl.Rows[0]["orderID"]);
                    mDiscount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    mSubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"]);
                    if (mBk > 0)
                    {
                        sql = "select ol.refund,ol.lineID,ol.qty,ol.weight,ol.subTotal,if(ol.itemID<>0,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.dynID<>0,(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(ts)=date('" + dateTimePickerfrom.Value.Year + "-" + dateTimePickerfrom.Value.Month + "-" + dateTimePickerfrom.Value.Day + "') and d.shiftID=" + txtShift.Text + " limit 0,1 ),(select i.itemDesc from itemslinemenu i where i.lineID=ol.optionID))) as itemDesc from ordersallline ol where ol.bkId=" + mBk;
                    }
                    else
                    {
                        sql = "select ol.refund,ol.lineID,ol.qty,ol.weight,ol.subTotal,if(ol.itemID<>0,(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID),if(ol.dynID<>0,(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID),(select i.itemDesc from itemslinemenu i where i.lineID=ol.optionID))) as itemDesc from ordersdailyline ol where ol.orderID=" + mOrder;
                    }
                    mDataTable = mConnection.Select(sql);
                    LoadData(mDataTable);
                    CheckSubTotal();
                    DataObject.ShiftObject so = new DataObject.ShiftObject();
                    so.ShiftID = Convert.ToInt16(tbl.Rows[0]["shiftID"]);
                    so = BusinessObject.BOShift.GetShiftMaxWithShiftID(so);

                    txtShift.Text = so.ShiftName.ToString();
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lblError.Text = "Không tìm thấy!";
                    }
                    lblError.Text = "Not found !";
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
            }
        }
    }
}