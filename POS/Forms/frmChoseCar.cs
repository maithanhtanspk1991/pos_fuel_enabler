﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmChoseCar : Form
    {
        public DataObject.Rego mRego { get; set; }

        private List<DataObject.Rego> lsManageCar;
        private int Row = 1;
        private int Column = 4;
        private Class.MoneyFortmat mMoney;

        public frmChoseCar(List<DataObject.Rego> lsArray, Class.MoneyFortmat money)
        {
            InitializeComponent();
            mMoney = money;
            lsManageCar = lsArray;
            mRego = new DataObject.Rego();
        }

        private void frmChoseCar_Load(object sender, EventArgs e)
        {
            if (lsManageCar.Count > 4)
            {
                Column = 4;
                Row = lsManageCar.Count / 4 + 1;
            }
            else
            {
                Row = 1;
                Column = lsManageCar.Count;
            }
            SetForm();
            SetSlide();
            LoadListPump();
        }

        private void SetForm()
        {
            this.Width = Column * 150;
            this.Height = Row * 180;
        }

        private void LoadListPump()
        {
            int t = 0;
            UCOfPump[] lsUCOfPump = new UCOfPump[lsManageCar.Count];
            foreach (DataObject.Rego item in lsManageCar)
            {
                lsUCOfPump[t] = new UCOfPump();
                lsUCOfPump[t].BackColor = Color.Green;
                lsUCOfPump[t].Name = item.CarNumber;
                lsUCOfPump[t].Price = item.CarColor;
                lsUCOfPump[t].Weight = item.CarProduct;
                lsUCOfPump[t].Tag = item;
                lsUCOfPump[t].Dock = DockStyle.Fill;
                lsUCOfPump[t].SetValues();
                lsUCOfPump[t].Click += new EventHandler(uc_Click);
                t++;
            }
            tlpSelect.Controls.AddRange(lsUCOfPump);
            //for (int i = t; i < Row * Column; i++)
            //{
            //    if (tlpSelect.Controls[i] != null)
            //        tlpSelect.Controls[i].Enabled = false;
            //}
        }

        private void uc_Click(object sender, EventArgs e)
        {
            UCOfPump uc = (UCOfPump)sender;
            mRego = (DataObject.Rego)uc.Tag;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void SetSlide()
        {
            tlpSelect.RowStyles.Clear();
            tlpSelect.RowCount = Row;
            System.Single LenghtRow = 100 / Row;
            for (int i = 0; i < Row; i++)
            {
                tlpSelect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, LenghtRow));
            }

            tlpSelect.ColumnStyles.Clear();
            System.Single LenghtColumn = 100 / Column;
            tlpSelect.ColumnCount = Column;
            for (int i = 0; i < Column; i++)
            {
                tlpSelect.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, LenghtColumn));
            }
        }
    }
}