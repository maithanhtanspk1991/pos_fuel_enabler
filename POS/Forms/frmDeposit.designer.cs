﻿namespace POS.Forms
{
    partial class frmDeposit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeposit));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.uCkeypad1 = new POS.UCkeypad();
            this.textBoxNumbericPOSCode = new POS.TextBoxNumbericPOS();
            this.textBoxNumbericPOSDeposit = new POS.TextBoxNumbericPOS();
            this.textBoxNumbericPOSMobile = new POS.TextBoxNumbericPOS();
            this.textBoxNumbericPOSPhone = new POS.TextBoxNumbericPOS();
            this.textBoxPOSKeyBoardName = new POS.TextBoxPOSKeyBoard();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Phone";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mobile";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Deposit";
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(0)))));
            this.buttonOk.FlatAppearance.BorderSize = 0;
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(30, 203);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(164, 107);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Red;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(214, 203);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(164, 107);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Code";
            // 
            // labelError
            // 
            this.labelError.ForeColor = System.Drawing.Color.Red;
            this.labelError.Location = new System.Drawing.Point(1, 175);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(401, 23);
            this.labelError.TabIndex = 5;
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uCkeypad1
            // 
            this.uCkeypad1.Location = new System.Drawing.Point(413, 3);
            this.uCkeypad1.Name = "uCkeypad1";
            this.uCkeypad1.Size = new System.Drawing.Size(212, 270);
            this.uCkeypad1.TabIndex = 6;
            this.uCkeypad1.txtResult = null;
            // 
            // textBoxNumbericPOSCode
            // 
            this.textBoxNumbericPOSCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxNumbericPOSCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxNumbericPOSCode.Location = new System.Drawing.Point(80, 3);
            this.textBoxNumbericPOSCode.Name = "textBoxNumbericPOSCode";
            this.textBoxNumbericPOSCode.Size = new System.Drawing.Size(322, 32);
            this.textBoxNumbericPOSCode.TabIndex = 3;
            this.textBoxNumbericPOSCode.ucKeypad = null;
            this.textBoxNumbericPOSCode.uckeypadDiv = null;
            // 
            // textBoxNumbericPOSDeposit
            // 
            this.textBoxNumbericPOSDeposit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxNumbericPOSDeposit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxNumbericPOSDeposit.Location = new System.Drawing.Point(80, 140);
            this.textBoxNumbericPOSDeposit.Name = "textBoxNumbericPOSDeposit";
            this.textBoxNumbericPOSDeposit.Size = new System.Drawing.Size(322, 32);
            this.textBoxNumbericPOSDeposit.TabIndex = 3;
            this.textBoxNumbericPOSDeposit.ucKeypad = this.uCkeypad1;
            this.textBoxNumbericPOSDeposit.uckeypadDiv = null;
            // 
            // textBoxNumbericPOSMobile
            // 
            this.textBoxNumbericPOSMobile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxNumbericPOSMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxNumbericPOSMobile.Location = new System.Drawing.Point(80, 106);
            this.textBoxNumbericPOSMobile.Name = "textBoxNumbericPOSMobile";
            this.textBoxNumbericPOSMobile.Size = new System.Drawing.Size(322, 32);
            this.textBoxNumbericPOSMobile.TabIndex = 2;
            this.textBoxNumbericPOSMobile.ucKeypad = this.uCkeypad1;
            this.textBoxNumbericPOSMobile.uckeypadDiv = null;
            // 
            // textBoxNumbericPOSPhone
            // 
            this.textBoxNumbericPOSPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxNumbericPOSPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxNumbericPOSPhone.Location = new System.Drawing.Point(80, 72);
            this.textBoxNumbericPOSPhone.Name = "textBoxNumbericPOSPhone";
            this.textBoxNumbericPOSPhone.Size = new System.Drawing.Size(322, 32);
            this.textBoxNumbericPOSPhone.TabIndex = 1;
            this.textBoxNumbericPOSPhone.ucKeypad = this.uCkeypad1;
            this.textBoxNumbericPOSPhone.uckeypadDiv = null;
            // 
            // textBoxPOSKeyBoardName
            // 
            this.textBoxPOSKeyBoardName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.textBoxPOSKeyBoardName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxPOSKeyBoardName.Location = new System.Drawing.Point(80, 38);
            this.textBoxPOSKeyBoardName.Name = "textBoxPOSKeyBoardName";
            this.textBoxPOSKeyBoardName.Size = new System.Drawing.Size(322, 32);
            this.textBoxPOSKeyBoardName.TabIndex = 0;
            // 
            // frmDeposit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 322);
            this.Controls.Add(this.uCkeypad1);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.textBoxNumbericPOSCode);
            this.Controls.Add(this.textBoxNumbericPOSDeposit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxNumbericPOSMobile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxNumbericPOSPhone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPOSKeyBoardName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDeposit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DEPOSIT";
            this.Load += new System.EventHandler(this.frmDeposit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBoxPOSKeyBoard textBoxPOSKeyBoardName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private TextBoxNumbericPOS textBoxNumbericPOSPhone;
        private System.Windows.Forms.Label label3;
        private TextBoxNumbericPOS textBoxNumbericPOSMobile;
        private System.Windows.Forms.Label label4;
        private TextBoxNumbericPOS textBoxNumbericPOSDeposit;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label5;
        private TextBoxNumbericPOS textBoxNumbericPOSCode;
        private System.Windows.Forms.Label labelError;
        private UCkeypad uCkeypad1;

    }
}