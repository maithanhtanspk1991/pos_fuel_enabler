﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class FormFinal : Form
    {
        private int mStaffID;
        private DataObject.ShiftReport shiftrp;
        private Class.ReadConfig mreadconfig;
        private Boolean mlock;
        private Class.MoneyFortmat mmoney;
        private Printer mPrinter;
        private Connection.ReadDBConfig dbconfig;
        private Class.Setting mSetting;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public FormFinal(DataObject.ShiftReport shift, int staffID, Class.MoneyFortmat money)
        {
            InitializeComponent();
            SetMultiLanguage();
            shiftrp = shift;
            mStaffID = staffID;
            mreadconfig = new Class.ReadConfig();
            mlock = false;
            mmoney = money;
            mPrinter = new Printer();
            mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
            dbconfig = new Connection.ReadDBConfig();
            mSetting = new Class.Setting();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                button1.Text = "Chấp nhận";
                button2.Text = "Thoát";
                return;
            }
        }

        private void textBoxNumbericPOS3_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Name == txtFinalDrop.Name)
            {
                if (mlock == false)
                {
                    mlock = true;
                    try
                    {
                        txttotalCFO.Text = (Convert.ToDouble(txtCFO.Text) - Convert.ToDouble(txtFinalDrop.Text)).ToString();
                    }
                    catch
                    {
                        txttotalCFO.Text = "0";
                    }
                    mlock = false;
                }
            }
            else
            {
                if (mlock == false)
                {
                    mlock = true;
                    try
                    {
                        txtFinalDrop.Text = (Convert.ToDouble(txtCFO.Text) - Convert.ToDouble(txttotalCFO.Text)).ToString();
                    }
                    catch
                    {
                        txtFinalDrop.Text = "0";
                    }
                    mlock = false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToDouble(txttotalCFO.Text) < 0 && Convert.ToDouble(txtFinalDrop) < 0)
                {
                    lbStatus.Text = "Invalid Final Total Cash Float Out!!!";
                    return;
                }
                else
                {
                    DataObject.CashInAndCashOut cico = new DataObject.CashInAndCashOut();
                    cico.ShiftID = shiftrp.ShiftID;

                    cico.TotalAmount = mmoney.getFortMat(Convert.ToDouble(txtFinalDrop.Text));
                    cico.Employee = mStaffID;
                    cico.CashType = DataObject.TypeCashInAndCashOut.SafeDrop;
                    cico.Description = "Final Safe Drop Shift " + shiftrp.ShiftID;
                    if (BusinessObject.BOCashInAndCashOut.InsertSafeDropAuto(cico) > 0)
                    {
                        mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                        mPrinter.printDocument.Print();

                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                    }
                }
            }
            catch
            {
                lbStatus.Text = "Invalid Final Total Cash Float Out!!!";
                return;
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;
            string header = dbconfig.Header1.ToString();
            string bankCode = dbconfig.Header2.ToString();
            string address = dbconfig.Header3.ToString();
            string tell = dbconfig.Header4.ToString();

            string website = dbconfig.FootNode1.ToString();
            string thankyou = dbconfig.FootNode2.ToString();

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();

            l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);

            l_y += mPrinter.GetHeightPrinterLine();
            DateTime dateTime = DateTime.Now;
            l_y = mPrinter.DrawString(dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.ToShortTimeString(), e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);

            l_y = mPrinter.DrawString("SHIFT #" + shiftrp.ShiftName, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 3);

            l_y = mPrinter.DrawString("FINAL DROP ", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);

            //l_y += 100;
            //l_y = mPrinter.DrawString(DateTime.Now.ToShortDateString(), e, new Font("Arial", 10, FontStyle.Italic), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine() / 5;

            mPrinter.DrawString("Total Cash ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString("$" + mmoney.Format2(Convert.ToDouble(txtFinalDrop.Text) * 1000), e, new Font("Arial", 10), l_y, 3);

            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawLine("***", new Font("Arial", 10), e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 2);
        }

        private void FormFinal_Load(object sender, EventArgs e)
        {
            double cashTotal = shiftrp.CashIn + shiftrp.cash + shiftrp.cashAccount - shiftrp.ChangeSales - shiftrp.CashOut - shiftrp.SafeDrop - shiftrp.refun;
            txtCFO.Text = mmoney.Format2(shiftrp.CashFloatIn + cashTotal);
            txttotalCFO.Text = mmoney.Format2(mreadconfig.MoneyMinAfterSafeDropAuto);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}