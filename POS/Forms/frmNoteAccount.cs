﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmNoteAccount : Form
    {
        public static string noteAcct;
        public frmNoteAccount()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            noteAcct = txtNote.Text;
            this.Close();
        }

        private void frmNoteAccount_Load(object sender, EventArgs e)
        {
            txtNote.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
