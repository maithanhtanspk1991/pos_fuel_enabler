﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmBulk : Form
    {
        private Connection.Connection mConnection;
        private Class.Bulk mBulk;
        private Class.Bulkline mbulkline;
        private bool mIsLock = false;

        public frmBulk()
        {
            InitializeComponent();
        }

        private void frmBulk_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            LoadBulk();
            splitContainer1.Panel2Collapsed = true;
            splitContainer2.Panel1Collapsed = true;
        }

        private void LoadBulk()
        {
            try
            {
                System.Data.DataTable tbl = mConnection.Select("select * from bulkmenu");
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    Class.Bulk bulk = new Class.Bulk(Convert.ToInt16(row["bulkID"].ToString()), row["name"].ToString(), row["description"].ToString(), Convert.ToInt16(row["packsize"].ToString()), Convert.ToInt16(row["supplierID"].ToString()));
                    mBulk = bulk;
                    ListViewItem li = new ListViewItem(bulk.Name);
                    li.SubItems.Add(bulk.Description);
                    li.SubItems.Add(bulk.Size + "");
                    li.SubItems.Add(bulk.SupplierID + "");
                    li.Tag = bulk;
                    listView1.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmBulk.LoadBulk:::" + ex.Message);
            }
        }

        private void LoadBulkLine(int bulkID)
        {
            listView2.Items.Clear();
            try
            {
                mConnection.Open();
                DataTable dt = new DataTable();
                dt = mConnection.Select("select * from bulkmenulines where bulkID=" + bulkID + "");
                foreach (DataRow row in dt.Rows)
                {
                    string itemname_bulk = mConnection.ExecuteScalar("select itemDesc from itemsmenu where itemID=" + row["itemID"].ToString()).ToString();
                    Class.Bulkline bulkline = new Class.Bulkline(Convert.ToInt16(row["lineID"].ToString()), Convert.ToInt16(row["bulkID"].ToString()), Convert.ToInt16(row["itemID"].ToString()), Convert.ToInt16(row["size"].ToString()), itemname_bulk);
                    mbulkline = bulkline;
                    ListViewItem li = new ListViewItem(bulkline.BulkID.ToString());
                    string itemname = mConnection.ExecuteScalar("select itemDesc from itemsmenu where itemID=" + "'" + bulkline.ItemID.ToString() + "'").ToString();
                    //li.SubItems.Add(bulkline.ItemID.ToString());
                    li.SubItems.Add(itemname);
                    li.SubItems.Add(bulkline.Size.ToString());
                    li.Tag = bulkline;
                    //ListViewItem li = new ListViewItem(Convert.ToInt16(row["bulkID"].ToString()).ToString());
                    //li.SubItems.Add(Convert.ToInt16(row["itemID"].ToString()).ToString());
                    //li.SubItems.Add(Convert.ToInt16(row["size"].ToString()).ToString());
                    listView2.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadBulkLine:::" + ex.Message);
            }
            finally
            {
                mConnection.Close();
            }
        }

        public Class.Bulk GetBulk()
        {
            return mBulk;
        }

        private void AddBulk(string name, string description, string packsize, int supplierID)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("insert into bulkmenu(name,description,packsize,supplierID) value(" + "'" + name + "'" + "," + "'" + description + "'" + "," + "'" + packsize + "'" + "," + "'" + supplierID + "'" + ")");
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("AddBulk:::" + ex.Message);
            }
        }

        private void AddBulkLine(int bulkID, int itemID, int size)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("insert into bulkmenulines(bulkID,itemID,size) value(" + bulkID + "," + itemID + "," + size + ")");
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("AddBulkLine:::" + ex.Message);
            }
        }

        private void DeleteBulk(int bulkID)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("delete from bulkmenu where bulkID=" + bulkID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("DeleteBulk:::" + ex.Message);
            }
        }

        private void DeleteBulkLine(int lineID)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("delete from bulkmenulines where lineID=" + lineID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("DeleteBulkLine:::" + ex.Message);
            }
        }

        private void UpdateBulk(int bulkID, string name, string description, string packsize)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("update bulkmenu set name=" + "'" + name + "'" + ",description=" + "'" + description + "'" + ",packsize=" + "'" + packsize + "'" + " where bulkID=" + bulkID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UpdateBulk:::" + ex.Message);
            }
        }

        private void UpdateBulkLine(int lineID, int bulkID, int itemID, int size)
        {
            try
            {
                mConnection.Open();
                mConnection.ExecuteNonQuery("update bulkmenulines set bulkID=" + "'" + bulkID + "'" + ",itemID=" + "'" + itemID + "'" + ",size=" + "'" + size + "'" + " where lineID=" + lineID);
                mConnection.Close();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UpdateBulkLine:::" + ex.Message);
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                try
                {
                    Class.Bulkline bulkl = (Class.Bulkline)listView2.SelectedItems[0].Tag;
                    mbulkline = bulkl;
                    //textBoxPOS_OSKItem.Text = bulkl.ItemID.ToString();
                    textBoxPOS_OSKItem.Text = bulkl.ItemName.ToString();
                    textBoxPOS_OSK1.Text = bulkl.BulkID.ToString();
                    textBoxPOS_OSKItemSize.Text = bulkl.Size.ToString();
                    listView2.Tag = bulkl.LineID;
                    button12.Enabled = true;
                    button13.Enabled = true;
                }
                catch (Exception)
                {
                }
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView2.SelectedIndices[0];
                    listView2.SelectedIndices.Clear();
                    if (oldselection + 2 > listView2.Items.Count)
                    {
                        listView2.SelectedIndices.Add(0);
                        listView2.Items[0].Selected = true;
                        listView2.Items[0].EnsureVisible();
                    }
                    else
                    {
                        listView2.SelectedIndices.Add(oldselection + 1);
                        listView2.Items[oldselection + 1].Selected = true;
                        listView2.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView2.SelectedIndices[0];
                    listView2.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        listView2.SelectedIndices.Add(listView2.Items.Count - 1);
                        listView2.Items[listView2.Items.Count - 1].Selected = true;
                        listView2.Items[listView2.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        listView2.SelectedIndices.Add(oldselection - 1);
                        listView2.Items[oldselection - 1].Selected = true;
                        listView2.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                listView2.SelectedIndices.Clear();
                listView2.SelectedIndices.Add(0);
                listView2.Items[0].Selected = true;
                listView2.Items[0].EnsureVisible();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                listView2.SelectedIndices.Clear();
                listView2.SelectedIndices.Add(listView2.Items.Count - 1);
                listView2.Items[listView2.Items.Count - 1].Selected = true;
                listView2.Items[listView2.Items.Count - 1].EnsureVisible();
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBoxPOS_OSKItem.Text = "";
            textBoxPOS_OSKItemSize.Text = "";
            textBoxPOS_OSK1.Text = "";
            button11.Enabled = true;
            button12.Enabled = false;
            button13.Enabled = false;
            // code ben new receistock
            //mIsLock = true;
            //ListViewItem li = new ListViewItem("");
            //li.SubItems.Add("");
            //li.SubItems.Add("0");
            //ReceivedStockLine re = new ReceivedStockLine(null, null, 0);
            //li.Tag = re;
            //listView1.Items.Add(li);
            //li.Selected = true;
            //li.EnsureVisible();
            //mIsLock = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (textBoxPOS_OSKItem.Text == "" || textBoxPOS_OSKItemSize.Text == "")
            {
                label9.Text = "All information not empty!";
                label9.Visible = true;
            }
            else
            {
                try
                {
                    AddBulkLine(mBulk.BulkID, Convert.ToInt16(textBoxPOS_OSKItem.Tag.ToString()), Convert.ToInt16(textBoxPOS_OSKItemSize.Text));
                    listView2.Items.Clear();
                    LoadBulkLine(mBulk.BulkID);
                    button11.Enabled = false;
                    button12.Enabled = true;
                    button13.Enabled = true;
                    label9.Visible = false;
                }
                catch (Exception)
                {
                }
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                DeleteBulkLine(Convert.ToInt32(listView2.Tag.ToString()));
                listView2.Items.Clear();
                LoadBulkLine(mBulk.BulkID);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                UpdateBulkLine(Convert.ToInt16(listView2.Tag.ToString()), Convert.ToInt16(textBoxPOS_OSK1.Text), Convert.ToInt16(textBoxPOS_OSKItem.Tag.ToString()), Convert.ToInt16(textBoxPOS_OSKItemSize.Text));
                listView2.Items.Clear();
                LoadBulkLine(mBulk.BulkID);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                Class.Bulk bulk = (Class.Bulk)listView1.SelectedItems[0].Tag;
                mBulk = bulk;
                textBoxPOS_OSKName.Text = bulk.Name;
                textBoxPOS_OSKDescription.Text = bulk.Description;
                textBoxPOS_OSKSize.Text = bulk.Size + "";
                textBoxPOS_OSK2.Text = bulk.SupplierID + "";
                listView1.Tag = bulk.BulkID;
                LoadBulkLine(bulk.BulkID);
                button6.Enabled = true;
                button7.Enabled = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                DeleteBulk(Convert.ToInt32(listView1.Tag.ToString()));
                listView1.Items.Clear();
                LoadBulk();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                UpdateBulk(Convert.ToInt32(listView1.Tag.ToString()), textBoxPOS_OSKName.Text, textBoxPOS_OSKDescription.Text, textBoxPOS_OSKSize.Text);
                listView1.Items.Clear();
                LoadBulk();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBoxPOS_OSKName.Text = "";
            textBoxPOS_OSKDescription.Text = "";
            textBoxPOS_OSKSize.Text = "";
            button18.Enabled = true;
            button6.Enabled = false;
            button7.Enabled = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (textBoxPOS_OSKName.Text == "" || textBoxPOS_OSKSize.Text == "" || textBoxPOS_OSKDescription.Text == "" || textBoxPOS_OSK2.Text == "")
            {
                label8.Text = "All information not empty!";
                label8.Visible = true;
            }
            else
            {
                try
                {
                    AddBulk(textBoxPOS_OSKName.Text, textBoxPOS_OSKDescription.Text, textBoxPOS_OSKSize.Text, Convert.ToInt16(textBoxPOS_OSK2.Text));
                    listView1.Items.Clear();
                    LoadBulk();
                    button18.Enabled = false;
                    button6.Enabled = true;
                    button7.Enabled = true;
                    label8.Visible = false;
                }
                catch (Exception)
                {
                }
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(0);
                listView1.Items[0].Selected = true;
                listView1.Items[0].EnsureVisible();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                        listView1.Items[listView1.Items.Count - 1].Selected = true;
                        listView1.Items[listView1.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection - 1);
                        listView1.Items[oldselection - 1].Selected = true;
                        listView1.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection + 2 > listView1.Items.Count)
                    {
                        listView1.SelectedIndices.Add(0);
                        listView1.Items[0].Selected = true;
                        listView1.Items[0].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection + 1);
                        listView1.Items[oldselection + 1].Selected = true;
                        listView1.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (splitContainer2.Panel1Collapsed == true)
            {
                splitContainer2.Panel1Collapsed = false;
            }
            else
            {
                splitContainer2.Panel1Collapsed = true;
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (splitContainer1.Panel2Collapsed == true)
            {
                splitContainer1.Panel2Collapsed = false;
            }
            else
            {
                splitContainer1.Panel2Collapsed = true;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Forms.frmItems frm = new Forms.frmItems();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Forms.frmItems.Item item = frm.GetItem();
                //if (item != null && listView2.SelectedIndices.Count > 0)
                //{
                //    ReceivedStockLine re = (ReceivedStockLine)listView2.SelectedItems[0].Tag;
                //    listView2.SelectedItems[0].SubItems[0].Text = item.lineName;
                //    textBoxPOS_OSKItem.Text = item.lineName;
                //    re.Item = item;
                //    textBoxPOS_OSKItemSize.Text = "";
                //    listView2.SelectedItems[0].SubItems[1].Text = "";
                //    re.Bulk = null;
                //}
                //MessageBox.Show(item.ItemID+":::"+item.ItemName);
                textBoxPOS_OSKItem.Text = item.ItemName;
                textBoxPOS_OSKItem.Tag = item.ItemID;
            }
        }

        private class ReceivedStockLine
        {
            //public Forms.frmItems.ItemLine Item { get; set; }
            //public Class.Bulk Bulk { get; set; }
            //public int Size { get; set; }
            public int LineID { get; set; }

            public int ItemID { get; set; }

            public string ItemName { get; set; }

            public ReceivedStockLine(int lineid, int itemid, string itemname)
            {
                //Item = item;
                //Bulk = bulk;
                //Size = size;
                LineID = lineid;
                ItemID = itemid;
                ItemName = itemname;
            }
        }

        private void textBoxPOS_OSKName_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKName);
            textBoxPOS_OSKName.Tag = textBoxPOS_OSKName.BackColor;
            textBoxPOS_OSKName.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKName_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKName.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSKDescription_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKDescription);
            textBoxPOS_OSKDescription.Tag = textBoxPOS_OSKDescription.BackColor;
            textBoxPOS_OSKDescription.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKDescription_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKDescription.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSKSize_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKSize);
            textBoxPOS_OSKSize.Tag = textBoxPOS_OSKSize.BackColor;
            textBoxPOS_OSKSize.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKSize_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKSize.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSK2_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSK2);
            textBoxPOS_OSK2.Tag = textBoxPOS_OSK2.BackColor;
            textBoxPOS_OSK2.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSK2_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSK2.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSKItem_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKItem);
            textBoxPOS_OSKItem.Tag = textBoxPOS_OSKItem.BackColor;
            textBoxPOS_OSKItem.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKItem_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKItem.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSKItemSize_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSKItemSize);
            textBoxPOS_OSKItemSize.Tag = textBoxPOS_OSKItemSize.BackColor;
            textBoxPOS_OSKItemSize.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSKItemSize_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSKItemSize.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        private void textBoxPOS_OSK1_Click(object sender, EventArgs e)
        {
            Forms.frmKeyboard frm = new Forms.frmKeyboard(textBoxPOS_OSK1);
            textBoxPOS_OSK1.Tag = textBoxPOS_OSK1.BackColor;
            textBoxPOS_OSK1.BackColor = Color.White;
            frm.ShowDialog();
        }

        private void textBoxPOS_OSK1_Leave(object sender, EventArgs e)
        {
            textBoxPOS_OSK1.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }
    }
}