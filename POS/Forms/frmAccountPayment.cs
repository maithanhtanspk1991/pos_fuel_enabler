﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Properties;

namespace POS.Forms
{
    public partial class frmAccountPayment : Form
    {
        private frmCustomerDisplay frmCust;
        private Class.MoneyFortmat money;
        private Connection.Connection con;
        private double mTotal = 0;
        private double Balance = 0;
        private double Change = 0;
        private bool mLockTextChange = false;
        private POS.Printer mPrinter;
        private Class.Setting mSetting;
        private bool mIsLockText = false;
        private double dblDepositAccount;
        private int intType;
        private int shiftID, mCustID;
        private const string _nameClass = "POS::Forms::frmAccountPayment::";
        //private AxCSDEFTLib.AxCsdEft axCsdEft;
        private CardInterface.POSAxCsdEft axCsdEft;
        private CardSubtotal mCardSubtotal;
        private bool blnEnd = false;
        private Class.ReadConfig mReadConfig = new ReadConfig();

        public frmAccountPayment(Class.MoneyFortmat m, int CustID, string strChangeCustomerNo, string strChangeAccountLimit, string strChangeDebt, string strCustomerName, double dblDepositAccount, int intType, int shiftID, CardInterface.POSAxCsdEft axC)
        {
            InitializeComponent();
            SetMultiLanguage();
            mCustID = CustID;
            blnEnd = false;
            if (intType == 1 || intType == 3)
            {
                //btnCash.Visible = true;
                buttonCard.Visible = false;
            }
            else if (intType == 2 || intType == 4)
            {
                btnCash.Visible = false;
                buttonCard.Visible = false;
                lblChange.Visible = false;
                lblChangeText.Text = "Total :";
            }

            this.intType = intType;
            con = new Connection.Connection();
            AccPayment = new Class.AccountPayment();
            money = m;
            AccPayment.CustomerNo = strChangeCustomerNo;
            AccPayment.Limit = Convert.ToDouble(strChangeAccountLimit.Replace("$", "").Replace("VND", "").Trim());
            AccPayment.Debt = Convert.ToDouble(strChangeDebt.Replace("$", "").Replace("VND", "").Trim());
            AccPayment.CustomerName = strCustomerName;
            txtAccountLimit.Text = AccPayment.Limit.ToString();
            txtBalance.Text = AccPayment.Debt.ToString();
            this.dblDepositAccount = dblDepositAccount;
            dbconfig = new Connection.ReadDBConfig();
            this.shiftID = shiftID;
            mPrinter = new Printer();
            mSetting = new Class.Setting();
            LoadCard();

            this.axCsdEft = axC;
            InitCardAx();
        }

        private void InitCardAx()
        {
            foreach (CardInterface.AXCSEventObject obj in axCsdEft.POSListEventObject)
            {
                this.axCsdEft.TransactionEvent -= obj.TransactionEvent;
                this.axCsdEft.PrintReceiptEvent -= obj.PrintReceiptEvent;
            }
            axCsdEft.POSListEventObject.Clear();
            CardInterface.AXCSEventObject objNew = new CardInterface.AXCSEventObject(new AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEventHandler(axCsdEft_PrintReceiptEvent), new EventHandler(axCsdEft_TransactionEvent));
            this.axCsdEft.PrintReceiptEvent += objNew.PrintReceiptEvent;
            this.axCsdEft.TransactionEvent += objNew.TransactionEvent;
            axCsdEft.POSListEventObject.Add(objNew);
        }

        private void SetMultiLanguage()
        {
            this.Text = "Thanh toán tài khoản";
            label5.Text = "CHỦ TÀI KHOẢN";
            label6.Text = "THẺ";
            label1.Text = "PHỤ THU";
            label2.Text = "TÀI KHOẢN GIỚI HẠN";
            label4.Text = "GHI NỢ";
            label8.Text = "TÍN DỤNG";
            ckEnableSurcharge.Text = "Kích hoạt tính năng phụ phí";
            ckEnableChange.Text = "Kích hoạt tính năng đổi";
            label14.Text = "Thanh toán tới:";
            button8.Text = "Xóa";
            btnCancel.Text = "THOÁT";
            btnAccept.Text = "CHẤP NHẬN";
            btnUP.Text = "LÊN";
            btnDown.Text = "XUỐNG";
            btnDoTransaction.Text = "";
            lblChangeText.Text = "THAY ĐỔI";
            return;
        }

        private void LoadCard()
        {
            try
            {
                flCard.Controls.Clear();
                List<DataObject.Card> lsArray = BusinessObject.BOCard.GetAll(1);

                UCCardButton[] lsUCCardButton = new UCCardButton[lsArray.Count];
                int n = 0;
                foreach (DataObject.Card item in lsArray)
                {
                    item.IsActive = false;
                    lsUCCardButton[n] = new UCCardButton();
                    lsUCCardButton[n].POSKeyPad = uCkeypad1;
                    lsUCCardButton[n].lblNameCard.Text = item.CardName;
                    lsUCCardButton[n].btnCard.Tag = new Class.CardSubtotal(item.Copy(), lsUCCardButton[n]);
                    lsUCCardButton[n].btnCard.Click += new EventHandler(btnCard_Click);
                    lsUCCardButton[n].txtTotalAmount.Enabled = false;
                    lsUCCardButton[n].Tag = item.IsSurchart.ToString();
                    n++;
                }
                flCard.Controls.AddRange(lsUCCardButton);

            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadCard::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private double CardAmound()
        {
            double discountPercent = 0;
            double cash = 0;
            try
            {
                cash = Convert.ToDouble(txtTendered.Text);
            }
            catch (Exception) { }
            double balance = 0;
            try
            {
                balance = Convert.ToDouble(txtBalance.Text);
            }
            catch (Exception) { }
            double card = ((100 - discountPercent) * balance / 100) - cash;
            int intCard = (int)(card * 1000);
            if ((intCard % 10) >= 5)
            {
                intCard = ((intCard / 10) + 1) * 10;
            }
            card = (double)intCard / 1000;
            if (card < 0)
            {
                card = 0;
            }
            return card;
        }

        private void btnCard_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                ExecuteCardClick(btn, true);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnCard_Click::" + ex.Message);
            }
        }

        private void ExecuteCardClick(Button btn, bool blnEnableTextBox)
        {
            mIsLockText = true;
            CardSubtotal cardSubtotal = (CardSubtotal)btn.Tag;
            if (mCardSubtotal != null)
            {
                if (mCardSubtotal.Card.CardID != cardSubtotal.Card.CardID)
                {
                    mCardSubtotal.CardButton.btnCard.Image = (Bitmap)Resources.UnCheck;
                    mCardSubtotal.Card.IsActive = false;
                    mCardSubtotal.CardButton.txtTotalAmount.Enabled = blnEnableTextBox;
                    mCardSubtotal.CardButton.txtTotalAmount.Text = "0.00";
                }
            }
            EnableCashOut(false);
            if (cardSubtotal.Card.IsActive)
            {
                cardSubtotal.CardButton.btnCard.Image = (Bitmap)Resources.UnCheck;
                cardSubtotal.Card.IsActive = !cardSubtotal.Card.IsActive;
                //cardSubtotal.CardButton.txtTotalAmount.Enabled = blnEnableTextBox;
                cardSubtotal.CardButton.txtTotalAmount.Text = "0.00";
                txtCashOut.Text = "0";
                CheckSubTotal(cardSubtotal);
                mCardSubtotal = null;
            }
            else
            {
                EnableCashOut(cardSubtotal.Card.IsCashOut);
                mCardSubtotal = cardSubtotal;
                txtCashOut.Text = "";
                btn.Image = (Bitmap)Resources.checkbox;
                cardSubtotal.Card.IsActive = !cardSubtotal.Card.IsActive;
                if (cardSubtotal.Card.IsSurchart)
                {
                    double total = CalcultorDiscountForCard();
                    double card = total + total * cardSubtotal.getSurChar(ckEnableSurcharge.Checked);
                    cardSubtotal.CardButton.txtTotalAmount.Text = money.FormatCurenMoney2(card);
                    txtCashOut.Text = cardSubtotal.CardButton.txtTotalAmount.Text;
                    CheckSubTotal(cardSubtotal);
                }
                else
                {
                    double card = CalcultorDiscountForCard();
                    cardSubtotal.CardButton.txtTotalAmount.Text = money.FormatCurenMoney2(card);
                    frmKeyPad frm = new frmKeyPad(cardSubtotal.CardButton.txtTotalAmount);
                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        cardSubtotal.CardButton.txtTotalAmount.Text = cardSubtotal.CardButton.txtTotalAmount.Text == "" ? "0" : cardSubtotal.CardButton.txtTotalAmount.Text;
                        txtCashOut.Text = cardSubtotal.CardButton.txtTotalAmount.Text;
                        CheckSubTotal(cardSubtotal);
                    }
                    else
                    {
                        mCardSubtotal.CardButton.btnCard.Image = (Bitmap)Resources.UnCheck;
                        mCardSubtotal.Card.IsActive = false;
                        mCardSubtotal.CardButton.txtTotalAmount.Enabled = blnEnableTextBox;
                        mCardSubtotal.CardButton.txtTotalAmount.Text = "0.00";
                    }
                }
            }
            mIsLockText = false;
        }

        private double CalcultorDiscountForCard()
        {
            double tendered = 0;
            try
            {
                tendered = Convert.ToDouble(txtTendered.Text);
            }
            catch (Exception) { }
            double balance = 0;
            try
            {
                balance = Convert.ToDouble(txtBalance.Text);
            }
            catch (Exception) { }

            double card = balance - tendered;
            return card > 0 ? card : 0;
        }

        public Class.AccountPayment AccPayment { get; set; }

        private void button5_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            double total = 0.00;
            try
            {
                total += Convert.ToDouble(txtTendered.Text);
            }
            catch (Exception)
            {
            }
            total += Convert.ToDouble(btn.Tag.ToString());
            txtTendered.Text = String.Format("{0:0.00}", total);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            txtTendered.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
            axCsdEft.Tag = null;
        }

        private void frmSubTotal_Load(object sender, EventArgs e)
        {
            //lblServeEmployee.Text = frmLogin.strEmployeeName;//Thanh sua ngay 2104/08/01
            lblServeEmployee.Text = AccPayment.CustomerName;////Thanh sua ngay 2104/08/01
            this.Location = new Point(this.Location.X, 0);
            txtTendered.SetForcus();
        }

        private DataTable dtSource;
        public static List<OrderByCard> moreOrderByCard;
        private void EnableCashOut(bool value)
        {
            if (value)
            {
                txtCashOut.Enabled = true;
                txtCashOut.Text = "";
            }
            else
            {
                txtCashOut.Enabled = false;
                txtCashOut.Text = "";
            }
        }


        private void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                double dblCard = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                double dblCast = Convert.ToDouble(txtTendered.Text == "" ? "0" : txtTendered.Text);
                double dblDebt = Convert.ToDouble(txtBalance.Text == "" ? "0" : txtBalance.Text);
                double dbChange = Convert.ToDouble(lblChange.Text == "" ? "0" : lblChange.Text);
                AccPayment.Cash = dblCast; ;
                AccPayment.Change = dbChange;
                AccPayment.Card = dblCard;
                double x = mCardSubtotal == null ? 0 : mCardSubtotal.getSurChar(ckEnableSurcharge.Checked);
                AccPayment.Surcharge = money.Round(AccPayment.Card - (AccPayment.Card / (1 + x)), 2);
                AccPayment.SubTotal = AccPayment.Card + AccPayment.Cash;
                AccPayment.PayAmount = money.Round(AccPayment.Card + AccPayment.Cash - AccPayment.Surcharge - AccPayment.Change, 2);
                int i = 0;
                moreOrderByCard = new List<OrderByCard>();
                i = this.flCard.Controls.Count;
                if (i != 0)
                {
                    foreach (UCCardButton uccCard in this.flCard.Controls)
                    {
                        CardSubtotal cardSubTotal = (CardSubtotal)uccCard.btnCard.Tag;
                        if (cardSubTotal.Card.IsActive)
                        {
                            OrderByCard orderBC = new OrderByCard();
                            orderBC.orderbycardID = AccPayment.OrderByCardID;
                            orderBC.cardID = cardSubTotal.Card.CardID;
                            orderBC.subtotal = Convert.ToDouble(uccCard.txtTotalAmount.Text == "" ? "0" : uccCard.txtTotalAmount.Text);
                            orderBC.surcharge = orderBC.subtotal - orderBC.subtotal / (1 + cardSubTotal.getSurChar(ckEnableSurcharge.Checked));
                            orderBC.nameCard = uccCard.lblNameCard.Text;
                            orderBC.shiftID = shiftID;
                            try
                            {
                                if (AccPayment.AuthCode != "" || AccPayment.AuthCode != null) { orderBC.AuthCode = AccPayment.AuthCode; } else { orderBC.AuthCode = ""; }
                                if (AccPayment.AccountType != "" || AccPayment.AccountType != null) { orderBC.AccountType = AccPayment.AccountType; } else { orderBC.AccountType = ""; }
                                if (AccPayment.Pan != "" || AccPayment.Pan != null) { orderBC.Pan = AccPayment.Pan; } else { orderBC.Pan = ""; }
                                if (AccPayment.DateExpiry != "" || AccPayment.DateExpiry != null) { orderBC.DateExpiry = AccPayment.DateExpiry; } else { orderBC.DateExpiry = ""; }
                                if (AccPayment.Date != "" || AccPayment.Date != null) { orderBC.Date = AccPayment.Date; } else { orderBC.Date = ""; }
                                if (AccPayment.Time != "" || AccPayment.Time != null) { orderBC.Time = AccPayment.Time; } else { orderBC.Time = ""; }
                                if (AccPayment.CardType != "" || AccPayment.CardType != null) { orderBC.CardType = AccPayment.CardType; } else { orderBC.CardType = ""; }
                                if (AccPayment.AmtPurchase != 0) { orderBC.AmtPurchase = AccPayment.AmtPurchase; } else { orderBC.AmtPurchase = 0; }
                                if (AccPayment.AmtCash != 0) { orderBC.AmtCash = AccPayment.AmtPurchase; } else { orderBC.AmtCash = 0; }
                                AccPayment.OrderByCardID = "1";
                                orderBC.orderbycardID = AccPayment.OrderByCardID;
                            }
                            catch (Exception)
                            {
                            }
                            moreOrderByCard.Add(orderBC);
                        }
                    }
                }
                else
                {
                    AccPayment.OrderByCardID = "0";
                }

                AccPayment.EmployeeID = frmLogin.intEmployeeID;
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);

                frmNoteAccount frmNote = new frmNoteAccount();
                frmNote.ShowDialog();
                    UpdatePaymentAccount();
                    if (AccPayment.Cash > 0 || AccPayment.Change > 0)
                    {
                        Class.RawPrinterHelper.openCashDrawer(new Class.Setting().GetBillPrinter());
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;

                    axCsdEft.Tag = null;
            }
            catch (Exception exs)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnAccept_Click::" + exs.Message);
            }
        }

        private Connection.ReadDBConfig dbconfig;

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float l_y = 0;

            string header = dbconfig.Header1.ToString();
            string bankCode = dbconfig.Header2.ToString();
            string address = dbconfig.Header3.ToString();
            string tell = dbconfig.Header4.ToString();

            string website = dbconfig.FootNode1.ToString();
            string thankyou = dbconfig.FootNode2.ToString();

            l_y = mPrinter.DrawString(header, e, new System.Drawing.Font("Arial", 14), l_y, 2);
            l_y = mPrinter.DrawString(bankCode, e, new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(address, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);
            l_y = mPrinter.DrawString(tell, e, new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Italic), l_y, 2);

            l_y += mPrinter.GetHeightPrinterLine();

            if (intType == 3)
                l_y = mPrinter.DrawString("CREDIT COMPANY", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            else
                l_y = mPrinter.DrawString("CREDIT ACCOUNT", e, new Font("Arial", 15, FontStyle.Bold), l_y, 2);
            l_y += mPrinter.GetHeightPrinterLine();
            DateTime datetime = DateTime.Now;
            l_y = mPrinter.DrawString(datetime.Day + "/" + datetime.Month + "/" + datetime.Year + " " + datetime.ToShortTimeString(), e, new Font("Arial", 10, FontStyle.Italic), l_y, 3);
            l_y += mPrinter.GetHeightPrinterLine() / 5;

            mPrinter.DrawString("Member No : ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(AccPayment.CustomerNo, e, new Font("Arial", 10), l_y, 3);

            mPrinter.DrawString("Customer Name : ", e, new Font("Arial", 10), l_y, 1);
            l_y = mPrinter.DrawString(AccPayment.CustomerName, e, new Font("Arial", 10), l_y, 3);

            if (AccPayment.Cash > 0)
            {
                mPrinter.DrawString("Cash : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(money.getFortMat(AccPayment.Cash)), e, new Font("Arial", 10), l_y, 3);
            }
            if (AccPayment.Change > 0)
            {
                mPrinter.DrawString("Change : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(money.getFortMat(AccPayment.Change)), e, new Font("Arial", 10), l_y, 3);
            }
            if (AccPayment.Card > 0)
            {
                mPrinter.DrawString("Card : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(money.getFortMat(AccPayment.Card)), e, new Font("Arial", 10), l_y, 3);
            }
            if (AccPayment.Surcharge > 0)
            {
                mPrinter.DrawString("Card Surcharge : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString(money.Format2(money.getFortMat(AccPayment.Surcharge)), e, new Font("Arial", 10), l_y, 3);
            }

            if (intType == 1 || intType == 3)
            {
                mPrinter.DrawString("Opening Balance : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat(AccPayment.Debt * -1)), e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Credit : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat(AccPayment.PayAmount)), e, new Font("Arial", 10), l_y, 3);

                mPrinter.DrawString("Closing Balance : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat((AccPayment.Debt - AccPayment.PayAmount) * -1)), e, new Font("Arial", 10), l_y, 3);
            }
            else if (intType == 2 || intType == 4)
            {
                mPrinter.DrawString("Total Amount : ", e, new Font("Arial", 10), l_y, 1);
                l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat(AccPayment.PayAmount)).ToString(), e, new Font("Arial", 10), l_y, 3);
                if (AccPayment.Debt - (AccPayment.Card + AccPayment.Cash) < 0)
                {
                    mPrinter.DrawString("Balance Amount : ", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat((-1) * (AccPayment.Debt - (AccPayment.Card + AccPayment.Cash)))).ToString(), e, new Font("Arial", 10), l_y, 3);
                }
                else
                {
                    mPrinter.DrawString("Balance Amount : ", e, new Font("Arial", 10), l_y, 1);
                    l_y = mPrinter.DrawString("$" + money.Format2(money.getFortMat(AccPayment.Debt - (AccPayment.Card + AccPayment.Cash))).ToString(), e, new Font("Arial", 10), l_y, 3);
                }
            }
            l_y += mPrinter.GetHeightPrinterLine();
            l_y = mPrinter.DrawString("", e, new Font("Arial", 10), l_y, 1);

            try
            {
                if (Convert.ToInt32(AccPayment.AmtCash + AccPayment.AmtPurchase) != 0)
                {
                    Class.LogPOS.WriteLog(_nameClass + "printDocument_PrintPage::Begin");
                    try
                    {
                        l_y += mPrinter.GetHeightPrinterLine();
                        l_y = mPrinter.DrawLine("", null, e, System.Drawing.Drawing2D.DashStyle.DashDot, l_y, 1);
                        l_y += mPrinter.GetHeightPrinterLine();
                        l_y = mPrinter.DrawString(" ... " + AccPayment.Pan.Substring(AccPayment.Pan.Length - 4, 4), e, new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Italic), l_y, 1);
                    }
                    catch (Exception err1)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai Pan:::End:" + err1.Message + ":" + AccPayment.Pan);
                    }

                    try
                    {
                        l_y += mPrinter.GetHeightPrinterLine();
                        l_y = mPrinter.DrawString(AccPayment.AccountType + " A/C", e, new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Italic), l_y, 1);
                    }
                    catch (Exception err2)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai AccountType:::End:" + err2.Message + ":" + AccPayment.AccountType);
                    }

                    try
                    {
                        mPrinter.DrawString(AccPayment.CardType, e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        l_y = mPrinter.DrawString(AccPayment.DateExpiry.Substring(0, 2) + " " + AccPayment.DateExpiry.Substring(2, 2), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err3)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai DateExpiry:::End:" + err3.Message + ":" + AccPayment.DateExpiry);
                    }

                    try
                    {
                        mPrinter.DrawString(AccPayment.Date.Substring(0, 2) + "-" + AccPayment.Date.Substring(2, 2) + "-" + AccPayment.Date.Substring(4, 2), e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        l_y = mPrinter.DrawString(AccPayment.Time.Substring(0, 2) + " " + AccPayment.Time.Substring(2, 2), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err4)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai Time:::End:" + err4.Message + ":" + AccPayment.Time);
                    }

                    try
                    {
                        mPrinter.DrawString("APPROVAL CODE", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        l_y = mPrinter.DrawString(AccPayment.AuthCode, e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err5)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai Time:::End:" + err5.Message + ":" + AccPayment.AuthCode);
                    }

                    try
                    {
                        if (AccPayment.AmtCash != 0)
                        {
                            l_y += mPrinter.GetHeightPrinterLine();
                            mPrinter.DrawString("CASH OUT", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                            mPrinter.DrawString("AUD", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                            l_y = mPrinter.DrawString(money.FormatNorman(AccPayment.AmtCash.ToString()), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                        }
                    }
                    catch (Exception err6)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai AmtCash:::End:" + err6.Message + ":" + AccPayment.AmtCash);
                    }

                    try
                    {
                        if (AccPayment.AmtPurchase != 0)
                        {
                            l_y += mPrinter.GetHeightPrinterLine();
                            mPrinter.DrawString("PURCHASE", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                            mPrinter.DrawString("AUD", e, new System.Drawing.Font("Arial", 8), l_y, 2);
                            l_y = mPrinter.DrawString(money.FormatNorman(AccPayment.AmtPurchase.ToString()), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                        }
                    }
                    catch (Exception err7)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai AmtPurchase:::End:" + err7.Message + ":" + AccPayment.AmtPurchase);
                    }

                    l_y += mPrinter.GetHeightPrinterLine();
                    l_y = mPrinter.DrawString("---------", e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    l_y += mPrinter.GetHeightPrinterLine();

                    try
                    {
                        mPrinter.DrawString("TOTAL", e, new System.Drawing.Font("Arial", 9), l_y, 1);
                        mPrinter.DrawString("AUD", e, new System.Drawing.Font("Arial", 9), l_y, 2);
                        l_y = mPrinter.DrawString(money.FormatNorman(Convert.ToString(AccPayment.AmtCash + AccPayment.AmtPurchase)), e, new System.Drawing.Font("Arial", 9), l_y, 3);
                    }
                    catch (Exception err8)
                    {
                        Class.LogPOS.WriteLog(_nameClass + "Loi kai AmtCash+AmtPurchase:::End:" + err8.Message + ":" + AccPayment.AmtCash + AccPayment.AmtPurchase);
                    }
                    Class.LogPOS.WriteLog(_nameClass + "printDocument_PrintPage::TaxInvoice:::End");
                }
            }
            catch (Exception err)
            {
                Class.LogPOS.WriteLog(_nameClass + "printDocument_PrintPage::TaxInvoice::End:" + err.Message);
            }
        }

        private double dblSubtotalHasPay;

        private bool UpdatePaymentAccount()
        {
            bool resuilt = false;
            try
            {
                con.Open();
                con.BeginTransaction();
                if (intType == 1 || intType == 3)
                {
                    if (intType == 3)
                    {
                        dtSource = con.Select("select SUM(cash + card) AS Total from accountpayment AS ap inner join company AS c on ap.CustomerNo = c.idCompany where ap.CustomerNo = " + AccPayment.CustomerNo + "");
                        try
                        {
                            dblSubtotalHasPay = Convert.ToDouble(dtSource.Rows[0][0]) / 1000;
                        }
                        catch (Exception)
                        {
                            dblSubtotalHasPay = 0;
                        }
                    }
                    else
                    {
                        dblSubtotalHasPay = 0;
                    }

                    if (AccPayment.OrderByCardID != "")
                    {
                        AccPayment.OrderByCardID = InsertIntoOrderByCard() + "";
                    }
                   con.ExecuteNonQuery("insert into accountpayment(CustID, CustomerNo,shiftID,PayAmount,EmployeeID,cash,card,surcharge,Subtotal,isCredit,typePayment,orderbycardID, balance,NoteAccount) values(" +
                        mCustID +
                    ",'" + AccPayment.CustomerNo +
                    "','" + shiftID.ToString() +
                    "'," + money.getFortMat(AccPayment.PayAmount) +
                    "," + frmLogin.intEmployeeID +
                    "," + money.getFortMat(AccPayment.Cash) +
                    "," + money.getFortMat(AccPayment.Card) +
                    "," + money.getFortMat(AccPayment.Surcharge) +
                    "," + money.getFortMat(AccPayment.SubTotal) + ",0,2 " +
                    ",'" + AccPayment.OrderByCardID +
                    "'," + money.getFortMat(AccPayment.Debt - AccPayment.PayAmount) +
                    ",'" + frmNoteAccount.noteAcct.ToString() +
                    "')");
                    //con.ExecuteNonQuery("update customers set debt=" + money.getFortMat(AccPayment.Debt-(AccPayment.Card + AccPayment.Cash - AccPayment.Change)) + " where memberNo=" + "'" + AccPayment.CustomerNo + "'");
                    if (intType == 1)
                        con.ExecuteNonQuery("update customers set debt=" + money.getFortMat(AccPayment.Debt - AccPayment.PayAmount) + " where memberNo=" + "'" + AccPayment.CustomerNo + "'");
                    else if (intType == 3)
                    {
                        con.ExecuteNonQuery("update company set debt=" + money.getFortMat(AccPayment.Debt - AccPayment.PayAmount) + " where idCompany=" + "'" + AccPayment.CustomerNo + "'");
                        if (money.getFortMat(AccPayment.Debt - AccPayment.PayAmount) == 0)
                        {
                            con.ExecuteNonQuery("update customers set debt = 0 where Company =" + "'" + AccPayment.CustomerNo + "'");
                            con.ExecuteNonQuery("insert into accountpayment(CustomerNo,shiftID,PayAmount,EmployeeID,cash,card,Subtotal,isCredit,typePayment,orderbycardID) " +
                                                "select c.memberNo,'" + shiftID.ToString() + "',0,'" + frmLogin.intEmployeeID + "',0,0,0,0,0,'" + AccPayment.OrderByCardID + "' from customers AS c where c.Company = " + AccPayment.CustomerNo);
                        }
                    }
                }
                else if (intType == 2 || intType == 4)
                {
                    con.ExecuteNonQuery("insert into accountpayment(CustID,CustomerNo,shiftID,PayAmount,EmployeeID,cash,card,Subtotal,isCredit,typePayment,balance) values(" +
                    mCustID +
                    ",'" + AccPayment.CustomerNo +
                    "','" + shiftID.ToString() +
                    "'," + money.getFortMat(AccPayment.PayAmount) +
                    "," + frmLogin.intEmployeeID +
                    "," + money.getFortMat(AccPayment.Cash) +
                    "," + money.getFortMat(AccPayment.Card) +
                    "," + money.getFortMat(AccPayment.SubTotal / 10 - AccPayment.Cash - AccPayment.Card) + ",1,3 " +
                    "," + money.getFortMat(AccPayment.Debt - AccPayment.Cash - AccPayment.Card) +
                    ")");
                    //con.ExecuteNonQuery("update customers set debt=" + money.getFortMat(AccPayment.Debt-(AccPayment.Card + AccPayment.Cash - AccPayment.Change)) + " where memberNo=" + "'" + AccPayment.CustomerNo + "'");
                    if (intType == 2)
                        con.ExecuteNonQuery("update customers set debt=" + money.getFortMat(AccPayment.Debt - AccPayment.Cash - AccPayment.Card) + " where memberNo=" + "'" + AccPayment.CustomerNo + "'");
                    else if (intType == 4)
                        con.ExecuteNonQuery("update company set debt=" + money.getFortMat(AccPayment.Debt - AccPayment.Cash - AccPayment.Card) + " where idCompany=" + "'" + AccPayment.CustomerNo + "'");
                }

                Class.LogPOS.WriteLog(_nameClass + "Print Report");
                try
                {
                    mPrinter.printDocument.PrinterSettings.PrinterName = mSetting.GetBillPrinter();
                    mPrinter.printDocument.Print();
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_nameClass + "UpdatePaymentAccount::" + ex.Message);
                }

                con.Commit();
                resuilt = true;
            }
            catch (Exception)
            {
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            return resuilt;
        }

        private int InsertIntoOrderByCard()
        {
            int resuilt = 0;
            try
            {
                string strOrderByCard = "";
                foreach (OrderByCard c in moreOrderByCard)
                {
                    strOrderByCard = c.orderbycardID;
                    con.ExecuteNonQuery(
                        "insert into orderbycard(orderbycardID,orderID,cardID,subtotal,surcharge,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,IsPayment) " +
                        "values(" +
                            c.orderbycardID + "," +
                            c.orderID + "," +
                            c.cardID + "," +
                            money.getFortMat(c.subtotal) + "," +
                            money.getFortMat(c.surcharge) + "," +
                            money.getFortMat(c.cashOut) + "," +
                            c.shiftID + ",'" +
                            c.AuthCode + "','" +
                            c.AccountType + "','" +
                            c.Pan + "','" +
                            c.DateExpiry + "','" +
                            c.Date + "','" +
                            c.Time + "','" +
                            c.CardType + "'," +
                            money.getFortMat(c.AmtPurchase.ToString()) + "," +
                            money.getFortMat(c.AmtCash.ToString()) + "," +
                            "1" +
                    ")");
                    int id = Convert.ToInt32(con.ExecuteScalar("select LAST_INSERT_ID()"));
                    con.ExecuteScalar("update orderbycard set orderbycardID=" + id + " where ID=" + id);
                    resuilt = id;
                }
                // Duc 21052013
                //string strQuerySelect = "SELECT DISTINCT orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,IsPayment FROM orderbycard o where orderbycardID = '" + strOrderByCard + "'";

                //DataTable dtSource = con.Select(strQuerySelect);
                //con.ExecuteNonQuery("Delete from orderbycard where orderbycardID = '" + strOrderByCard + "'");

                //foreach (DataRow dr in dtSource.Rows)
                //{
                //    con.ExecuteNonQuery("insert into orderbycard(orderbycardID,orderID,cardID,subtotal,cashOut,shiftsID,AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,IsPayment) values(" + dr["orderbycardID"].ToString() + "," + dr["orderID"].ToString() + "," + dr["cardID"].ToString() + "," + dr["subtotal"].ToString() + "," + dr["cashOut"].ToString() + "," + dr["shiftsID"].ToString() + ",'" + dr["AuthCode"].ToString() + "','" + dr["AccountType"].ToString() + "','" + dr["Pan"].ToString() + "','" + dr["DateExpiry"].ToString() + "','" + dr["Date"].ToString() + "','" + dr["Time"].ToString() + "','" + dr["CardType"].ToString() + "'," + dr["AmtPurchase"].ToString() + "," + dr["AmtCash"].ToString() + "," + dr["IsPayment"].ToString() + ")");
                //}
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "InsertIntoOrderByCard::" + ex.Message);
            }
            return resuilt;
        }

        private double decDeptAmount = 0;

        private bool CheckSubTotal(CardSubtotal card)
        {
            lbStatus.Text = "";

            double dbLimit = AccPayment.Limit;
            double dblDebt = AccPayment.Debt;
            double dblCash = Convert.ToDouble(txtTendered.Text == "" ? "0" : txtTendered.Text);
            double dblCard = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
            double currentCard = dblCard;
            if (card != null)
            {
                currentCard = currentCard / (card.getSurChar(ckEnableSurcharge.Checked) + 1);
            }

            double dblPay = dblCash + currentCard;

            AccPayment.Card = dblCard;
            txtSurcharge.Text = money.getFortMat2(dblCard - currentCard);
            if (ckEnableChange.Checked && dblPay > dblDebt)
            {
                txtChangePay.Text = money.getFortMat2(dblDebt);
                lblChange.Text = money.getFortMat2(dblPay - dblDebt);
            }
            else
            {
                txtChangePay.Text = money.getFortMat2(dblPay);
                lblChange.Text = "0.00";
            }

            return true;
        }

        private void txtTendered_TextChanged(object sender, EventArgs e)
        {
            if (!mLockTextChange)
            {
                CheckSubTotal(mCardSubtotal);
            }
        }

        private bool checkTotal()
        {
            lbStatus.Text = "";
            try
            {
                double tendered = money.getFortMat(txtTendered.Text);
                if (tendered < (AccPayment.getSubTotal()))
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Không đủ tiền";
                        lblChange.Text = "";
                        return true;
                    }
                    lbStatus.Text = "Not enough money";
                    lblChange.Text = "";
                }
                else
                {
                    double change = (tendered - AccPayment.getSubTotal());

                    lbStatus.Text = "";
                    lblChange.Text = money.Format(change);
                    if (frmCust != null)
                    {
                        frmCust.ChangeTenter = tendered;
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lblChange.Text = "";
                    lbStatus.Text = "Số không hợp lệ";
                    return false;
                }
                lblChange.Text = "";
                lbStatus.Text = "Number Invalid";
            }
            return false;
        }

        private void txtCashOut_TextChanged(object sender, EventArgs e)
        {
            CheckSubTotal(mCardSubtotal);
        }

        private void textBoxPOS1_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void buttonCash_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtBalance.Text) > 0)
            {
                double dblCash = Convert.ToDouble(txtTendered.Text == "" ? "0" : txtTendered.Text);
                double dblCard = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                if (dblCash == 0)
                {
                    txtTendered.Text = Convert.ToString(AccPayment.Debt);
                }
                if (dblCash != 0 && dblCash < AccPayment.Debt)
                {
                    txtTendered.Text = Convert.ToString(AccPayment.Debt);
                }
                if (dblCard != 0)
                {
                    txtTendered.Text = Convert.ToString(AccPayment.Debt - dblCard);
                }
                CheckSubTotal(null);
                Class.LogPOS.WriteLog(_nameClass + "buttonCash_Click");
            }
        }

        private void buttonCard_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtBalance.Text) > 0)
            {
                double dblCash = Convert.ToDouble(txtTendered.Text == "" ? "0" : txtTendered.Text);
                double dblCard = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                if (dblCard == 0)
                {
                    txtCashOut.Text = Convert.ToString(AccPayment.Debt);
                }
                if (dblCard != 0 && dblCard < AccPayment.Debt)
                {
                    txtTendered.Text = Convert.ToString(AccPayment.Debt);
                }
                if (dblCash != 0)
                {
                    txtCashOut.Text = Convert.ToString(AccPayment.Debt - dblCash);
                }
                CheckSubTotal(null);
                Class.LogPOS.WriteLog(_nameClass + "buttonCard_Click");
            }
        }

        private void txtCustomerCode_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
            Class.LogPOS.WriteLog(_nameClass + "txtCustomerCode_Enter");
        }

        private void btnDepositAccount_Click(object sender, EventArgs e)
        {
            try
            {
                double dblCard = Convert.ToDouble(txtCashOut.Text == "" ? "0" : txtCashOut.Text);
                double dblCast = Convert.ToDouble(txtTendered.Text == "" ? "0" : txtTendered.Text);
                double dblDebt = Convert.ToDouble(txtBalance.Text == "" ? "0" : txtBalance.Text);

                AccPayment.Card = dblCard;
                AccPayment.Cash = dblCast;
                AccPayment.SubTotal = AccPayment.Card + AccPayment.Cash;
                //AccPayment.SubTotal = money.Format(Convert.ToDouble(con.Select("Select Subtotal from accountpayment where CustomerNo = '" + AccPayment.CustomerNo + "' order by PayDate DESC limit 1 ").Rows[0][0].ToString()));

                AccPayment.EmployeeID = frmLogin.intEmployeeID;
                mPrinter.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument_PrintPage);
                UpdatePaymentAccount();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception exs)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnDepositAccount_Click::" + exs.Message);
            }
        }

        private void UpdateDebtAndDepositAccount(string strCustomerNo, double dblDept, double dblDepositAccount)
        {
            con.ExecuteNonQuery("(update customers set debt = " + dblDept +
                ", depositAccount = " + dblDepositAccount +
                " where memberNo = '" + strCustomerNo +
                "')");
        }

        private void ExecuteInsertAccountPayment(string strCustomerNo, string strEmployeeID, double dblCard, double dblCash, double dblBalance)
        {
            con.ExecuteNonQuery("insert into accountpayment(customerNo,shiftID,employeeID,card,cash,payAmount) values('"
                + strCustomerNo + "','"
                + shiftID.ToString() + "',"
                + strEmployeeID + ","
                + money.Format(dblCard) + ","
                + money.Format(dblCash) + ","
                + money.Format(dblBalance) + ")");
        }

        private void ExecuteInsertAccountDeposit(string strCustomerNo, string strEmployeeID, double dblCard, double dblCash, double dblBalance)
        {
            con.ExecuteNonQuery("insert into accountdeposit(customerNo,shiftID,employeeID,card,cash,subTotal) values('"
                + strCustomerNo + "','"
                + shiftID.ToString() + "',"
                + strEmployeeID + ","
                + money.Format(dblCard) + ","
                + money.Format(dblCash) + ","
                + money.Format(dblBalance) + ")");
        }

        private void axCsdEft_TransactionEvent(object sender, EventArgs e)
        {
            try
            {
                if (axCsdEft.Success == true)
                {
                    Class.LogPOS.WriteLog(_nameClass + "AccountPayment:::axCsdEft_TransactionEvent::" + axCsdEft.Success + "--" + blnEnd);
                    AccPayment.AuthCode = axCsdEft.AuthCode;
                    AccPayment.CardType = axCsdEft.CardType;
                    AccPayment.AccountType = axCsdEft.AccountType;
                    AccPayment.Pan = axCsdEft.Pan;
                    AccPayment.DateExpiry = axCsdEft.DateExpiry;
                    AccPayment.Date = axCsdEft.Date;
                    AccPayment.Time = axCsdEft.Time;
                    AccPayment.AmtPurchase = axCsdEft.AmtPurchase;
                    AccPayment.AmtCash = axCsdEft.AmtCash;
                    btnCancel.Enabled = false;
                    btnAccept.Enabled = true;

                    if (blnEnd == false)
                    {
                        try
                        {
                            btnAccept_Click(sender, e);
                        }
                        catch (Exception)
                        {
                        }
                    }

                    blnEnd = true;
                }
                else
                    if (axCsdEft.Success == false)
                    {
                        string strSQL = "insert into cardreject(AuthCode,AccountType,Pan,DateExpiry,Date,Time,CardType,AmtPurchase,AmtCash,ShiftID) " +
                                        "values('" + axCsdEft.AuthCode + "','"
                                                   + axCsdEft.CardType + "','"
                                                   + axCsdEft.Pan + "','"
                                                   + axCsdEft.DateExpiry + "','"
                                                   + axCsdEft.DateExpiry + "','"
                                                   + axCsdEft.Time + "','"
                                                   + axCsdEft.CardType + "','"
                                                   + money.getFortMat(Convert.ToDouble(axCsdEft.AmtPurchase)) + "','"
                                                   + money.getFortMat(Convert.ToDouble(axCsdEft.AmtCash)) + "','"
                                                   + shiftID + "')";
                    }
                lbStatus.Text = axCsdEft.ResponseText;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "axCsdEft_TransactionEvent::" + ex.Message);
            }
        }

        public string strDataReceive = "";

        private void axCsdEft_PrintReceiptEvent(object sender, AxCSDEFTLib._DCsdEftEvents_PrintReceiptEventEvent e)
        {
            if (e.receiptType.Length < 1) return;
            switch (e.receiptType[0])
            {
                case 'R': // Receipt data is ready to print!
                    strDataReceive += axCsdEft.Receipt + " \\ ";
                    break;

                case 'C':
                    strDataReceive += "Customer Receipt \\ ";
                    break;

                case 'M':
                    strDataReceive += "Merchant Receipt \\ ";
                    break;

                case 'S':
                    strDataReceive += "Settlement Receipt \\ ";
                    break;

                case 'L':
                    strDataReceive += "Logon Receipt \\ ";
                    break;

                case 'A':
                    strDataReceive += "Audit Receipt \\ ";
                    break;

                default:
                    strDataReceive += "Unknown Receipt \\ ";
                    break;
            }
        }

        private void btnDoTransaction_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog(_nameClass + "btnDoTransaction_Click");
            try
            {
                axCsdEft.CutReceipt = false;
                axCsdEft.ReceiptAutoPrint = false;
                axCsdEft.TxnRef = "1";
                axCsdEft.TxnType = "P";
                axCsdEft.AmtPurchase = decimal.Parse(txtCashOut.Text);
                axCsdEft.DoTransaction();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "btnDoTransaction_Click::" + ex.Message);
            }
        }



        private void checkBoxEnableChange_CheckedChanged(object sender, EventArgs e)
        {
            if (!mLockTextChange)
            {
                CheckSubTotal(mCardSubtotal);
            }
        }

        private void checkBoxEnableSurcharge_CheckedChanged(object sender, EventArgs e)
        {
            if (!mLockTextChange)
            {
                if (mCardSubtotal != null)
                {
                    ExecuteCardClick(mCardSubtotal.CardButton.btnCard, true);
                }
            }
        }
    }
}