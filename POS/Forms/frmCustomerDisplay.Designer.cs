﻿namespace POS.Forms
{
    partial class frmCustomerDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnRight = new System.Windows.Forms.Panel();
            this.pnTop = new System.Windows.Forms.Panel();
            this.pnContent = new System.Windows.Forms.Panel();
            this.pnHeader = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.lvOrderMain = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbTender = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbCard = new System.Windows.Forms.Label();
            this.lbChange = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbGST = new System.Windows.Forms.Label();
            this.lbDiscount = new System.Windows.Forms.Label();
            this.Card = new System.Windows.Forms.Label();
            this.pnLeft = new System.Windows.Forms.Panel();
            this.imgCustomerDisplay = new POS.Controls.ImageViewer();
            this.pnRight.SuspendLayout();
            this.pnTop.SuspendLayout();
            this.pnHeader.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.pnLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCustomerDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnRight
            // 
            this.pnRight.Controls.Add(this.pnTop);
            this.pnRight.Controls.Add(this.pnBottom);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnRight.Location = new System.Drawing.Point(505, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(519, 768);
            this.pnRight.TabIndex = 10;
            // 
            // pnTop
            // 
            this.pnTop.BackgroundImage = global::POS.Properties.Resources.customerTop;
            this.pnTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnTop.Controls.Add(this.pnContent);
            this.pnTop.Controls.Add(this.pnHeader);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(519, 463);
            this.pnTop.TabIndex = 10;
            // 
            // pnContent
            // 
            this.pnContent.BackColor = System.Drawing.Color.Transparent;
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(0, 64);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(519, 399);
            this.pnContent.TabIndex = 4;
            // 
            // pnHeader
            // 
            this.pnHeader.BackColor = System.Drawing.Color.Transparent;
            this.pnHeader.Controls.Add(this.label12);
            this.pnHeader.Controls.Add(this.label13);
            this.pnHeader.Controls.Add(this.label14);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(519, 64);
            this.pnHeader.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(129)))), ((int)(((byte)(184)))));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label12.Size = new System.Drawing.Size(339, 64);
            this.label12.TabIndex = 0;
            this.label12.Text = "Item Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Right;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(129)))), ((int)(((byte)(184)))));
            this.label13.Location = new System.Drawing.Point(339, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 64);
            this.label13.TabIndex = 1;
            this.label13.Text = "Qty";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Right;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(129)))), ((int)(((byte)(184)))));
            this.label14.Location = new System.Drawing.Point(419, 0);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.label14.Size = new System.Drawing.Size(100, 64);
            this.label14.TabIndex = 2;
            this.label14.Text = "Subtotal";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnBottom
            // 
            this.pnBottom.BackColor = System.Drawing.Color.Transparent;
            this.pnBottom.BackgroundImage = global::POS.Properties.Resources.customerbottom;
            this.pnBottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnBottom.Controls.Add(this.lvOrderMain);
            this.pnBottom.Controls.Add(this.label1);
            this.pnBottom.Controls.Add(this.label8);
            this.pnBottom.Controls.Add(this.lbTender);
            this.pnBottom.Controls.Add(this.lblTotal);
            this.pnBottom.Controls.Add(this.label10);
            this.pnBottom.Controls.Add(this.label9);
            this.pnBottom.Controls.Add(this.label7);
            this.pnBottom.Controls.Add(this.label6);
            this.pnBottom.Controls.Add(this.label11);
            this.pnBottom.Controls.Add(this.label5);
            this.pnBottom.Controls.Add(this.label2);
            this.pnBottom.Controls.Add(this.label4);
            this.pnBottom.Controls.Add(this.lbCard);
            this.pnBottom.Controls.Add(this.lbChange);
            this.pnBottom.Controls.Add(this.label3);
            this.pnBottom.Controls.Add(this.lbGST);
            this.pnBottom.Controls.Add(this.lbDiscount);
            this.pnBottom.Controls.Add(this.Card);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 463);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(519, 305);
            this.pnBottom.TabIndex = 9;
            // 
            // lvOrderMain
            // 
            this.lvOrderMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvOrderMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvOrderMain.FullRowSelect = true;
            this.lvOrderMain.GridLines = true;
            this.lvOrderMain.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvOrderMain.Location = new System.Drawing.Point(116, 270);
            this.lvOrderMain.Name = "lvOrderMain";
            this.lvOrderMain.Size = new System.Drawing.Size(111, 23);
            this.lvOrderMain.TabIndex = 2;
            this.lvOrderMain.UseCompatibleStateImageBehavior = false;
            this.lvOrderMain.View = System.Windows.Forms.View.Details;
            this.lvOrderMain.Visible = false;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Qty";
            this.columnHeader1.Width = 68;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 224;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Price";
            this.columnHeader3.Width = 94;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 46);
            this.label1.TabIndex = 7;
            this.label1.Text = "Total";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.label8.Location = new System.Drawing.Point(6, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 31);
            this.label8.TabIndex = 3;
            this.label8.Text = "Tender";
            // 
            // lbTender
            // 
            this.lbTender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTender.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lbTender.Location = new System.Drawing.Point(318, 196);
            this.lbTender.Name = "lbTender";
            this.lbTender.Size = new System.Drawing.Size(178, 31);
            this.lbTender.TabIndex = 4;
            this.lbTender.Text = "0.00";
            this.lbTender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lblTotal.Location = new System.Drawing.Point(303, 16);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(193, 46);
            this.lblTotal.TabIndex = 6;
            this.lblTotal.Text = "0.00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label10.Location = new System.Drawing.Point(152, 236);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 31);
            this.label10.TabIndex = 3;
            this.label10.Text = ":    $";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label9.Location = new System.Drawing.Point(152, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 31);
            this.label9.TabIndex = 3;
            this.label9.Text = ":    $";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label7.Location = new System.Drawing.Point(152, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 31);
            this.label7.TabIndex = 3;
            this.label7.Text = ":    $";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label6.Location = new System.Drawing.Point(152, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 31);
            this.label6.TabIndex = 3;
            this.label6.Text = ":    $";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label11.Location = new System.Drawing.Point(152, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 46);
            this.label11.TabIndex = 3;
            this.label11.Text = ":  $";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.label5.Location = new System.Drawing.Point(152, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 31);
            this.label5.TabIndex = 3;
            this.label5.Text = ":    $";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.label2.Location = new System.Drawing.Point(6, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "GST";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.label4.Location = new System.Drawing.Point(6, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "Discount";
            // 
            // lbCard
            // 
            this.lbCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lbCard.Location = new System.Drawing.Point(318, 156);
            this.lbCard.Name = "lbCard";
            this.lbCard.Size = new System.Drawing.Size(178, 31);
            this.lbCard.TabIndex = 4;
            this.lbCard.Text = "0.00";
            this.lbCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbChange
            // 
            this.lbChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChange.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lbChange.Location = new System.Drawing.Point(318, 236);
            this.lbChange.Name = "lbChange";
            this.lbChange.Size = new System.Drawing.Size(178, 31);
            this.lbChange.TabIndex = 4;
            this.lbChange.Text = "0.00";
            this.lbChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.label3.Location = new System.Drawing.Point(6, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 31);
            this.label3.TabIndex = 3;
            this.label3.Text = "Change";
            // 
            // lbGST
            // 
            this.lbGST.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGST.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGST.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lbGST.Location = new System.Drawing.Point(318, 76);
            this.lbGST.Name = "lbGST";
            this.lbGST.Size = new System.Drawing.Size(178, 31);
            this.lbGST.TabIndex = 4;
            this.lbGST.Text = "0.00";
            this.lbGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbDiscount
            // 
            this.lbDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiscount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(24)))));
            this.lbDiscount.Location = new System.Drawing.Point(318, 116);
            this.lbDiscount.Name = "lbDiscount";
            this.lbDiscount.Size = new System.Drawing.Size(178, 31);
            this.lbDiscount.TabIndex = 4;
            this.lbDiscount.Text = "0.00";
            this.lbDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Card
            // 
            this.Card.AutoSize = true;
            this.Card.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(130)))), ((int)(((byte)(182)))));
            this.Card.Location = new System.Drawing.Point(6, 156);
            this.Card.Name = "Card";
            this.Card.Size = new System.Drawing.Size(73, 31);
            this.Card.TabIndex = 3;
            this.Card.Text = "Card";
            // 
            // pnLeft
            // 
            this.pnLeft.BackColor = System.Drawing.Color.Transparent;
            this.pnLeft.BackgroundImage = global::POS.Properties.Resources.customerLeft;
            this.pnLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnLeft.Controls.Add(this.imgCustomerDisplay);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(505, 768);
            this.pnLeft.TabIndex = 9;
            // 
            // imgCustomerDisplay
            // 
            this.imgCustomerDisplay.BackColor = System.Drawing.Color.Transparent;
            this.imgCustomerDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgCustomerDisplay.ImageLinkFolder = null;
            this.imgCustomerDisplay.Location = new System.Drawing.Point(0, 0);
            this.imgCustomerDisplay.Name = "imgCustomerDisplay";
            this.imgCustomerDisplay.Size = new System.Drawing.Size(505, 768);
            this.imgCustomerDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCustomerDisplay.TabIndex = 0;
            this.imgCustomerDisplay.TabStop = false;
            // 
            // frmCustomerDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnLeft);
            this.Controls.Add(this.pnRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCustomerDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCustomerDisplay";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCustomerDisplay_Load);
            this.pnRight.ResumeLayout(false);
            this.pnTop.ResumeLayout(false);
            this.pnHeader.ResumeLayout(false);
            this.pnBottom.ResumeLayout(false);
            this.pnBottom.PerformLayout();
            this.pnLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgCustomerDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lbChange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbTender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView lvOrderMain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbDiscount;
        private System.Windows.Forms.Timer timer1;
       
        private System.Windows.Forms.Label Card;
        private System.Windows.Forms.Label lbCard;
        private Controls.ImageViewer imgCustomerDisplay;
        private System.Windows.Forms.Panel pnLeft;
        private System.Windows.Forms.Panel pnRight;
        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbGST;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Panel pnHeader;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
    }
}