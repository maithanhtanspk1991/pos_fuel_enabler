﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using POS.Class;
using POS.Properties;

namespace POS.Forms
{
    public partial class frmDetailPump : Form
    {
        public string strTableName;
        public string strSQL;
        public string strOrderID;
        private string strQuery;
        private string[] strButtonOrder;
        private Connection.Connection con;
        private Class.MoneyFortmat money;
        private string TableID;
        private string OrderID;

        public frmDetailPump(Connection.Connection con, string strSQL, string strPumID, MoneyFortmat money)
        {
            InitializeComponent();
            this.con = con;
            this.money = money;
            this.strSQL = strSQL;
            LoadFuelHistory(strSQL, strPumID);
        }

        private void LoadFuelHistory(string strSQL, string strPumID)
        {
            try
            {
                lblPumID.Text = strPumID;
                DataTable dtSource = con.Select(strSQL);
                foreach (DataRow drItem in dtSource.Rows)
                {
                    UCDetailPump ucCard = new UCDetailPump();
                    flow_layout_order.Controls.Add(ucCard);
                    ucCard.lblName.Text = Convert.ToString(drItem["Name"]);
                    ucCard.lblSubtotal.Text = "$ " + Convert.ToString(money.Format(Convert.ToDouble(drItem["CashAmount"])));
                    ucCard.lblVolume.Text = Convert.ToString(money.Format(Convert.ToDouble(drItem["VolumeAmount"]))) + " L";
                    ucCard.btnFuelInformation.Tag = drItem["ID"].ToString() + "-" + "false";
                    ucCard.btnFuelInformation.Click += new EventHandler(btnOrderInformation_Click);
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnOrderInformation_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                string strID = "";
                string strCheck = "";
                string str = btn.Tag.ToString();
                strButtonOrder = btn.Tag.ToString().Split('-');
                strID = strButtonOrder[0];
                strCheck = strButtonOrder[1];
                if (Convert.ToBoolean(strCheck))
                {
                    btn.Image = (Bitmap)Resources.UnCheck;
                    btn.Name = strID;
                    btn.Tag = strID + "-" + "false";
                }
                else
                {
                    btn.Image = (Bitmap)Resources.checkbox;
                    btn.Name = strID;
                    btn.Tag = strID + "-" + "true";
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public string strFuelIsChose;

        private void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                strFuelIsChose = "";
                string strID = "";
                string strCheck = "";
                foreach (UCDetailPump uc in flow_layout_order.Controls)
                {
                    strButtonOrder = uc.btnFuelInformation.Tag.ToString().Split('-');
                    strID = strButtonOrder[0];
                    strCheck = strButtonOrder[1];
                    if (Convert.ToBoolean(strCheck))
                    {
                        strFuelIsChose += uc.btnFuelInformation.Name.ToString() + ",";
                    }
                }
                strFuelIsChose = strFuelIsChose.Substring(0, strFuelIsChose.Length - 1);
            }
            catch (Exception exs)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.Send Old Order:::" + exs.Message);
                con.Rollback();
            }
            finally
            {
                con.Close();
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.flow_layout_order.VerticalScroll.Value -= 200;
                flow_layout_order.PerformLayout();
            }
            catch (Exception)
            {
                this.flow_layout_order.VerticalScroll.Value = 0;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                this.flow_layout_order.VerticalScroll.Value += 200;
                flow_layout_order.PerformLayout();
            }
            catch (Exception)
            {
            }
        }
    }
}