﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmBookingList : Form
    {
        private List<Class.ProcessOrderNew.Item> mListItem = new List<Class.ProcessOrderNew.Item>();
        private Class.MoneyFortmat money;
        private Connection.Connection mconnect;
        private frmOrdersAll frmorder;
        private Class.ProcessOrderNew.Order morder;
        private DataTable dtall = new DataTable();

        public frmBookingList(Class.MoneyFortmat mon, Connection.Connection con, Forms.frmOrdersAll frm, Class.ProcessOrderNew.Order order)
        {
            InitializeComponent();
            money = mon;
            mconnect = new Connection.Connection();
            frmorder = frm;
            morder = order;
        }

        private void frmBookingList_Load(object sender, EventArgs e)
        {
            dtall = mconnect.Select("select b.bookingID,b.bookingDate,b.persionals,b.booktype,c.Name,c.custID from customers c inner join booking b on c.custID=b.custID where day(bookingDate)=" + DateTime.Now.Day.ToString());
            if (dtall.Rows.Count > 0)
            {
                foreach (DataRow row in dtall.Rows)
                {
                    Class.Booking book = new Class.Booking(Convert.ToInt32(row["bookingID"]), Convert.ToInt32(row["custID"]), Convert.ToDateTime(row["bookingDate"]), Convert.ToInt32(row["persionals"]), Convert.ToInt32(row["booktype"]));
                    ListViewItem lvi = new ListViewItem(row["bookingDate"].ToString());
                    lvi.SubItems.Add(row["Name"].ToString());
                    lvi.SubItems.Add(row["persionals"].ToString());
                    lvi.Tag = book;
                    listView2.Items.Add(lvi);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                listView1.Items.Clear();
                mListItem.Clear();
                Class.Booking book = (Class.Booking)listView2.SelectedItems[0].Tag;
                txtpeople.Text = book.Person.ToString();
                DataTable dt = new DataTable();
                dt = mconnect.Select(
                    "select " +
                        "qty," +
                        "itemID," +
                        "if(dynID<>0,(select itemDesc from dynitemsmenu where dynID=b.dynID),if(itemID<>0,(select itemDesc from itemsmenu where itemID=b.itemID),(select itemDesc from itemslinemenu where lineID=b.optionID))) as itemDesc," +
                        "dynID," +
                        "price," +
                        "subTotal," +
                        "optionID " +
                    "from bookingline b " +
                    "where bookingID=" + book.BookID.ToString());
                foreach (DataRow row in dt.Rows)
                {
                    ListViewItem lvi = new ListViewItem(row["qty"].ToString());
                    if (row["dynID"].ToString() != "0")
                    {
                        //lvi.SubItems.Add(row["itemDesc"].ToString(), "itemslinemenu", "lineID"));
                        //Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(Convert.ToInt32(row["optionID"].ToString()), row["itemDesc"].ToString(), "itemslinemenu", "lineID"), Convert.ToInt32(row["qty"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToDouble(row["price"].ToString()), 0, 0);
                        Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0,
                               Convert.ToInt32(row["dynID"]),
                               row["itemDesc"].ToString(),
                               Convert.ToInt32(row["qty"]),
                               Convert.ToDouble(row["subTotal"]),
                               Convert.ToDouble(row["price"]),
                               0,
                               0,
                               0, "0");
                        item.ItemType = 1;
                        mListItem.Add(item);
                    }
                    else
                    {
                        if (Convert.ToInt32(row["itemID"].ToString()) != 0)
                        {
                            //lvi.SubItems.Add(GetItemName(row["optionID"].ToString(), "itemslinemenu", "lineID"));
                            ////Class.ProcessOrderNew.SubItem subitem = new Class.ProcessOrderNew.SubItem(Convert.ToInt32(row["optionID"].ToString()), GetItemName(row["optionID"].ToString(), "itemslinemenu", "lineID"), Convert.ToInt32(row["qty"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToDouble(row["price"].ToString()));
                            ////lvi.Tag = subitem;
                            //Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(Convert.ToInt32(row["optionID"].ToString()), GetItemName(row["optionID"].ToString(), "itemslinemenu", "lineID"), Convert.ToInt32(row["qty"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToDouble(row["price"].ToString()), 0, 0);
                            ////lvi.Tag = item;
                            Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0,
                               Convert.ToInt32(row["itemID"]),
                               row["itemDesc"].ToString(),
                               Convert.ToInt32(row["qty"]),
                               Convert.ToDouble(row["subTotal"]),
                               Convert.ToDouble(row["price"]),
                               Convert.ToDouble(row["gst"]),
                               0,
                               0, "0");
                            mListItem.Add(item);
                        }
                        //else if (Convert.ToInt32(row["optionID"].ToString()) == 0)
                        else
                        {
                            //lvi.SubItems.Add(GetItemName(row["itemID"].ToString(), "itemsmenu", "itemID"));
                            //Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(Convert.ToInt32(row["itemID"].ToString()), GetItemName(row["itemID"].ToString(), "itemsmenu", "itemID"), Convert.ToInt32(row["qty"].ToString()), Convert.ToDouble(row["subTotal"].ToString()), Convert.ToDouble(row["price"].ToString()), 0, 0);
                            ////lvi.Tag = item;
                            mListItem[mListItem.Count - 1].ListSubItem.Add(new Class.ProcessOrderNew.SubItem(
                                Convert.ToInt32(row["optionID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"])));
                        }
                    }
                    //lvi.SubItems.Add(row["subTotal"].ToString());
                    lvi.SubItems.Add(row["itemDesc"].ToString());
                    lvi.SubItems.Add(row["subTotal"].ToString());
                    listView1.Items.Add(lvi);
                }
            }
        }

        private string GetItemName(string itemid, string fieldname, string fieldidname)
        {
            mconnect.Open();
            string name = mconnect.ExecuteScalar("select itemShort,itemDesc from " + fieldname + " where " + fieldidname + "=" + itemid + "").ToString();
            mconnect.Close();
            return name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < listView1.Items.Count; i++)
            //{
            //    Class.ProcessOrderNew.Item item = (Class.ProcessOrderNew.Item)listView1.Items[i].Tag;
            //    frmorder.Addnewitems(item);
            //}
            foreach (Class.ProcessOrderNew.Item item in mListItem)
            {
                frmorder.AddNewItems(item);
            }
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
        }
    }
}