﻿namespace POS.Forms
{
    partial class frmChoseKindOfFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flow_layout_chose_kind_of_fuel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAccept = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flow_layout_chose_kind_of_fuel
            // 
            this.flow_layout_chose_kind_of_fuel.AllowDrop = true;
            this.flow_layout_chose_kind_of_fuel.AutoScroll = true;
            this.flow_layout_chose_kind_of_fuel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flow_layout_chose_kind_of_fuel.Location = new System.Drawing.Point(12, 12);
            this.flow_layout_chose_kind_of_fuel.Name = "flow_layout_chose_kind_of_fuel";
            this.flow_layout_chose_kind_of_fuel.Size = new System.Drawing.Size(710, 426);
            this.flow_layout_chose_kind_of_fuel.TabIndex = 1;
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccept.Location = new System.Drawing.Point(595, 444);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(127, 41);
            this.btnAccept.TabIndex = 2;
            this.btnAccept.Text = "ACCEPT";
            this.btnAccept.UseVisualStyleBackColor = false;
            // 
            // frmChoseKindOfFuel
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnAccept;
            this.ClientSize = new System.Drawing.Size(734, 491);
            this.ControlBox = false;
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.flow_layout_chose_kind_of_fuel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChoseKindOfFuel";
            this.Load += new System.EventHandler(this.frmChoseKindOfFuel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel flow_layout_chose_kind_of_fuel;
        private System.Windows.Forms.Button btnAccept;
    }
}