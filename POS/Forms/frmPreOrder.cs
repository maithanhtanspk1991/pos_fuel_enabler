﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmPreOrder : Form
    {
        private Class.ReadConfig mReadConfig;
        public frmPreOrder(Class.ReadConfig readConfig)
        {
            InitializeComponent();
            Order = new Class.ProcessOrderNew.Order();
            mReadConfig = readConfig;
            LoadData();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnBack.Text = "Quay lại";
                btnBack.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }

        public Class.ProcessOrderNew.Order Order { get; set; }
        public System.Windows.Forms.ListView listView { get; set; }
        public Class.MoneyFortmat money { get; set; }
        private string _nameClass = "POS::Forms::frmPreOrder::";

        private void LoadData()
        {
            flpOrder.Controls.Clear();
            Connection.Connection con = new Connection.Connection();
            try
            {
                con.Open();
                string sql = "select * from ordersdaily where (completed = 1 || completed = 2) order by ts desc ";
                BusinessObject.BOLimit.GetLimit(sql, ucLimit.mLimit);
                sql += " " + ucLimit.mLimit.SqlLimit + ";";
                System.Data.DataTable tbl = con.Select(sql);
                if (tbl.Rows.Count == 0)
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                POS.Controls.UCPreOrder[] lsucOrder = new POS.Controls.UCPreOrder[tbl.Rows.Count];
                int no = 0;
                foreach (DataRow row in tbl.Rows)
                {
                    lsucOrder[no] = new POS.Controls.UCPreOrder();
                    lsucOrder[no].lbOrderID.Text = row["orderID"].ToString();
                    lsucOrder[no].lbContent.Text = "Total: " + Class.MoneyFortmat.FormatStatic2(row["subTotal"].ToString());
                    lsucOrder[no].Tag = row["orderID"].ToString();
                    lsucOrder[no].Click += new EventHandler(frmPreOrder_Click);
                    no++;
                }
                ucLimit.PageNumReload();
                flpOrder.Controls.AddRange(lsucOrder);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_nameClass + "LoadData::" + ex.Message);
            }
        }

        void frmPreOrder_Click(object sender, EventArgs e)
        {
            POS.Controls.UCPreOrder uc = (POS.Controls.UCPreOrder)sender;
            string orderID = uc.Tag.ToString();
            Order = getPrevOrder(orderID);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public Class.ProcessOrderNew.Order getPrevOrder(string orderID)
        {
            Connection.Connection con = new Connection.Connection();
            Class.ProcessOrderNew.Order order = null;
            try
            {
                con.Open();
                System.Data.DataTable tbl = con.Select("select * from (select * from ordersdaily where orderID = " + orderID + " And (completed = 1 || completed = 2) order by ts desc limit 0,10) as tmp;");
                if (tbl.Rows.Count > 0)
                {
                    order = new Class.ProcessOrderNew.Order();
                    order.TableID = tbl.Rows[0]["tableID"].ToString();
                    order.SubTotal = Convert.ToDouble(tbl.Rows[0]["subTotal"].ToString());
                    order.Completed = Convert.ToInt32(tbl.Rows[0]["completed"].ToString());
                    order.OrderID = Convert.ToInt32(tbl.Rows[0]["orderID"]);
                    order.Discount = Convert.ToDouble(tbl.Rows[0]["discount"]);
                    order.Card = Convert.ToDouble(tbl.Rows[0]["eftpos"]);
                    order.Cash = Convert.ToDouble(tbl.Rows[0]["cash"]);
                    order.ChangeByCash = Convert.ToDouble(tbl.Rows[0]["changebycash"]);
                    order.ShiftID = Convert.ToInt32(tbl.Rows[0]["shiftID"]);
                    order.Account = Convert.ToDouble(tbl.Rows[0]["account"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Change = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.ChangeAmount = Convert.ToDouble(tbl.Rows[0]["change"]);
                    order.Tendered = order.Cash + order.ChangeByCash;
                    order.NumPeople = Convert.ToInt32(tbl.Rows[0]["clients"]);
                    order.Gst = Convert.ToDouble(tbl.Rows[0]["gst"]);
                    order.Customer.CustID = Convert.ToInt32(tbl.Rows[0]["custID"]);
                    order.OrderByCardID = tbl.Rows[0]["orderbycardID"].ToString();
                    order.DateOrder = Convert.ToDateTime(tbl.Rows[0]["ts"]);
                    order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                    order.Surchart = Convert.ToDouble(tbl.Rows[0]["Surchart"]);
                    order.Delivery = Convert.ToInt32(tbl.Rows[0]["IsDelivery"]);
                    order.DeliveryFee = Convert.ToDouble(tbl.Rows[0]["FeeDelivery"]);
                    order.Pickup = Convert.ToInt32(tbl.Rows[0]["IsPickup"]);
                    order.CustCode = tbl.Rows[0]["CustCode"].ToString();
                    order.RegoID = Convert.ToInt32(tbl.Rows[0]["managecar"]);
                    order.IsPreOrder = true;
                    if (order.Delivery != 0)
                    {
                        order.Paid = 1;
                    }
                    System.Data.DataTable tblItem = con.Select(
                        "select ol.itemID," +
                            "ol.dynID," +
                            "ol.qty," +
                            "ol.subTotal," +
                            "ol.price," +
                            "ol.optionID," +
                            "ol.weight," +
                            "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as dynName," +
                            "(select i.itemDesc from itemsmenu i where i.itemID=ol.itemID) as itemDesc," +
                            "(select i.scanCode from itemsmenu i where i.itemID=ol.itemID) as scanCodei," +
                            "(select il.itemDesc from itemslinemenu il where il.lineID=ol.optionID) as optionName, " +
                            "(select il.scanCode from itemslinemenu il where il.lineID=ol.optionID) as scanCodeil, " +
                            "ol.pumpID " +
                        "from ordersdailyline ol " +
                        "where ol.orderID=" + order.OrderID);

                    order.ClearItemToListAndListView(listView);
                    foreach (System.Data.DataRow row in tblItem.Rows)
                    {
                        if (row["dynID"].ToString() != "0")
                        {
                            Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0,
                                Convert.ToInt32(row["dynID"].ToString()),
                                row["dynName"].ToString(),
                                Convert.ToInt32(row["qty"].ToString()),
                                Convert.ToDouble(row["subTotal"].ToString()),
                                Convert.ToDouble(row["price"].ToString()),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                            item.ItemType = 1;
                            item.Weight = Convert.ToInt16(row["weight"].ToString());
                            //order.ListItem.Add(item);
                            order.AddItemNoCheck(item, listView, money, true);
                        }
                        else
                        {
                            if (row["itemID"].ToString() != "0")
                            {
                                Class.ProcessOrderNew.Item item = new Class.ProcessOrderNew.Item(0,
                                Convert.ToInt32(row["itemID"]),
                                row["itemDesc"].ToString(),
                                Convert.ToInt32(row["qty"]),
                                Convert.ToDouble(row["subTotal"]),
                                Convert.ToDouble(row["price"]),
                                0,
                                0,
                                0,
                                row["pumpID"].ToString()
                                );
                                item.BarCode = row["scanCodei"].ToString();
                                item.Weight = Convert.ToInt16(row["weight"].ToString());
                                order.AddItemNoCheck(item, listView, money, true);
                            }
                            else
                            {
                                Class.ProcessOrderNew.SubItem subitem = new Class.ProcessOrderNew.SubItem(
                                    Convert.ToInt32(row["optionID"].ToString()),
                                    row["optionName"].ToString(),
                                    Convert.ToInt32(row["qty"].ToString()),
                                    Convert.ToDouble(row["subTotal"].ToString()),
                                    Convert.ToDouble(row["price"].ToString())
                                    );
                                subitem.BarCode = row["scanCodeil"].ToString();
                                order.ListItem[order.ListItem.Count - 1].ListSubItem.Add(subitem);
                                order.InsertSubItemListView(order.ListItem[order.ListItem.Count - 1], subitem, -1, listView, money, true);
                            }
                        }
                    }
                }
                System.Data.DataTable tblCard = con.Select("SELECT obc.orderbycardID AS orderbycardID,obc.cardID AS cardID,obc.subtotal AS subtotal,obc.cashOut as cashOut,obc.orderID AS orderID,c.cardName FROM orderbycard as obc inner join card as c on obc.cardID = c.cardID where obc.ID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CashOut = Convert.ToDouble(tblCard.Rows[0]["cashOut"]);
                if (tblCard.Rows.Count > 0)
                {
                    order.MoreOrderByCard = new List<POS.Class.OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.MoreOrderByCard.Add(new POS.Class.OrderByCard()
                        {
                            orderbycardID = drRow["orderbycardID"].ToString(),
                            cardID = Convert.ToInt32(drRow["cardID"].ToString()),
                            subtotal = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["subtotal"].ToString()))),
                            cashOut = Convert.ToDouble(money.Format2(Convert.ToDouble(drRow["cashOut"].ToString()))),
                            orderID = Convert.ToInt32(drRow["orderID"].ToString()),
                            nameCard = drRow["cardName"].ToString()
                        }
                        );
                    }
                }
                tblCard = con.Select("SELECT obc.AuthCode,obc.AccountType,obc.Pan,obc.DateExpiry,obc.Date,obc.Time,obc.CardType,IF(obc.AmtPurchase IS NULL,0,obc.AmtPurchase) AS AmtPurchase,IF(obc.AmtCash IS NULL,0,obc.AmtCash) AS AmtCash FROM orderbycard as obc where obc.orderbycardID = '" + order.OrderByCardID + "'" + " and orderID=" + order.OrderID);
                order.CableID = Convert.ToInt32(tbl.Rows[0]["cableID"]);
                if (tblCard.Rows.Count > 0)
                {
                    //frmSubTotal.moreOrderByCard = new List<OrderByCard>();
                    foreach (DataRow drRow in tblCard.Rows)
                    {
                        order.AuthCode = drRow["AuthCode"].ToString();
                        order.CardType = drRow["CardType"].ToString();
                        order.AccountType = drRow["AccountType"].ToString();
                        order.Pan = drRow["Pan"].ToString();
                        order.DateExpiry = drRow["DateExpiry"].ToString();
                        order.Date = drRow["Date"].ToString();
                        order.Time = drRow["Time"].ToString();
                        order.AmtPurchase = Convert.ToDecimal(drRow["AmtPurchase"]);
                        order.AmtCash = Convert.ToDecimal(drRow["AmtCash"]);
                    }
                }


            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("ProcessOrderNew.getPrevOrder:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            if (order.CableID != 1)
            {
                order.Customer = BusinessObject.BOCustomers.GetByID(order.Customer.CustID);
                order.Rego = BusinessObject.BORego.GetByID(order.RegoID);
            }
            return order;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void ucLimit1_EventPage(DataObject.Limit limit)
        {
            LoadData();
        }
    }
}
