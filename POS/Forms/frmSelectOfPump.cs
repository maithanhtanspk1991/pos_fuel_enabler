﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmSelectOfPump : Form
    {
        private int Column = 4;

        private System.Data.DataTable dt;

        private Class.MoneyFortmat mMoney;

        private int Row = 1;

        public frmSelectOfPump(System.Data.DataTable d, Class.MoneyFortmat money)
        {
            InitializeComponent();
            dt = d;
            mMoney = money;
        }

        public string strPumIDHistory { get; set; }

        private void LoadListPump()
        {
            int t = 0;
            foreach (System.Data.DataRow row in dt.Rows)
            {
                UCOfPump uc = new UCOfPump();
                uc.BackColor = Color.Green;
                tlpSelect.Controls.Add(uc);
                uc.Name = row["Name"].ToString();
                uc.Price = mMoney.Format(row["CashAmount"].ToString());
                uc.Weight = mMoney.Format(row["VolumeAmount"].ToString());
                uc.Tag = row["ID"].ToString();
                uc.Dock = DockStyle.Fill;
                uc.SetValues();
                uc.Click += new EventHandler(uc_Click);
                t++;
            }
            /*
            for (int i = t; i < Row * Column; i++)
            {
                if (tlpSelect.Controls[i] != null)
                    tlpSelect.Controls[i].Enabled = false;
            }*/
        }

        private void SelectOfPump_Load(object sender, EventArgs e)
        {
            if (dt.Rows.Count > 4)
            {
                Column = 4;
                Row = dt.Rows.Count / 4 + 1;
            }
            else
            {
                Row = 1;
                Column = dt.Rows.Count;
            }
            SetForm();
            SetSlide();
            LoadListPump();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void SetForm()
        {
            this.Width = Column * 200;
            this.Height = Row * 200;
        }

        private void SetSlide()
        {
            tlpSelect.RowStyles.Clear();
            tlpSelect.RowCount = Row;
            System.Single LenghtRow = 100 / Row;
            for (int i = 0; i < Row; i++)
            {
                tlpSelect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, LenghtRow));
            }

            tlpSelect.ColumnStyles.Clear();
            System.Single LenghtColumn = 100 / Column;
            tlpSelect.ColumnCount = Column;
            for (int i = 0; i < Column; i++)
            {
                tlpSelect.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, LenghtColumn));
            }
        }

        private void uc_Click(object sender, EventArgs e)
        {
            UCOfPump uc = (UCOfPump)sender;

            strPumIDHistory = Convert.ToInt32(uc.Tag).ToString();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}