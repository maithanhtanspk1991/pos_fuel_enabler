﻿using POS.Class;
using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmDriverOffDetail : Form
    {
        private Connection.Connection mConnection = new Connection.Connection();
        private string OrderID = "";
        private DateTime TS;
        private int mShiftID;
        private Class.MoneyFortmat money;
  
        public frmDriverOffDetail(string orderid, DateTime ts, Class.MoneyFortmat mmoney, int shiftID)
        {
            InitializeComponent();
            OrderID = orderid;
            TS = ts;
            money = mmoney;
            mShiftID = shiftID;
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                button8.Text = "Thoát";
                columnHeader1.Text = "Số lượng";
                columnHeader2.Text = "Tên";
                columnHeader3.Text = "Thanh toán";
                return;
            }
        }

        private string GetDateString(DateTime dt)
        {
            return dt.Year + "-" + dt.Month + "-" + dt.Day;
        }

        private DataTable LoadDriverOffDetail(string orderid, DateTime date)
        {
            string dateString = GetDateString(date);
            DataTable dt = new DataTable();
            try
            {
                //string sql = "";
                //if (date.Day == DateTime.Now.Day && date.Month == DateTime.Now.Month && date.Year == DateTime.Now.Year)
                //{
                //    sql = "select o.*,if(o.itemID<>0,(select itemDesc from itemsmenu where o.itemID=itemID),if(o.dynID<>0,'Dyn Item',(select itemDesc from itemslinemenu where o.optionID=lineID))) as ItemDesc from ordersdailyline o where orderID=" + orderid;
                //}
                //else
                //{
                //    try
                //    {
                //        mConnection.Open();
                //        string bkid = mConnection.ExecuteScalar("select bkId from ordersall where (orderID=" + orderid + " and day(ts)=" + date.Day.ToString() + " and month(ts)=" + date.Month.ToString() + " and year(ts)=" + date.Year.ToString() + ") limit 0,1").ToString();
                //        sql = "select o.*,if(o.itemID<>0,(select itemDesc from itemsmenu where o.itemID=itemID),if(o.dynID<>0,'Dyn Item',(select itemDesc from itemslinemenu where o.optionID=lineID))) as ItemDesc from ordersallline o where bkId=" + bkid;
                //    }
                //    catch (Exception)
                //    {
                //    }
                //    finally
                //    {
                //        mConnection.Close();
                //    }
                //}
                string sql = "select " +
                         "if(groupName is null,'Unknown',groupName) as itemDesc , " +
                         "sum(qty) AS Quantity, " +
                         "sum(subTotal) AS Total " +
                       "from " +
                       "(select " +
                         "if( " +
                           "itemID<>0, " +
                           "(select i.itemDesc from itemsmenu i where i.itemID=tmp1.itemID), " +
                           "if( " +
                             "optionID<>0, " +
                             "(select i.itemDesc from itemsmenu i inner join itemslinemenu il on i.itemID=il.itemID where il.lineID=tmp1.optionID), " +
                              "'Dyn Item' " +
                           ") " +
                          ") as groupName, " +
                         "sum(qty) as qty, " +
                         "sum(subTotal) as subTotal " +
                       "from " +
                       "(select " +
                         "ol.itemID, " +
                         "ol.optionID, " +
                         "ol.dynID, " +
                         "sum(ol.qty) as qty, " +
                         "sum(ol.subTotal) as subTotal " +
                       "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                       "where o.completed = 0 and ol.dynID=0 and date(o.ts)=date('" + dateString + "') and o.orderID=" + orderid + " and o.shiftID=" + mShiftID + " " +
                       "group by ol.itemID,ol.optionID,ol.dynID " +
                       "union all " +
                       "select " +
                         "ol.itemID, " +
                         "ol.optionID, " +
                         "ol.dynID, " +
                         "sum(ol.qty) as qty, " +
                         "sum(ol.subTotal) as subTotal " +
                       "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                       "where o.completed = 0 and ol.dynID=0  and date(o.ts)=date('" + dateString + "') and o.orderID=" + orderid + " and o.shiftID=" + mShiftID + " " +
                       "group by ol.itemID,ol.optionID,ol.dynID) as tmp1 " +
                       "group by groupName " +

                       "union all " +
                          "select " +
                             "(select d.itemDesc from dynitemsall d where d.dynID=ol.dynID and date(d.ts)=date(o.ts) and d.shiftID=o.shiftID limit 0,1) as groupName, " +
                             "sum(ol.qty) as qty, " +
                             "sum(ol.subTotal) as subTotal " +
                           "from ordersall o inner join ordersallline ol on o.bkId=ol.bkId " +
                           "where o.completed = 0 and ol.dynID<>0  and date(o.ts)=date('" + dateString + "') and o.orderID=" + orderid + " and o.shiftID=" + mShiftID + " " +
                           "group by groupName " +
                       "union all " +
                           "select " +
                             "(select d.itemDesc from dynitemsmenu d where d.dynID=ol.dynID) as groupName, " +
                             "sum(ol.qty) as qty, " +
                             "sum(ol.subTotal) as subTotal " +
                           "from ordersdaily o inner join ordersdailyline ol on o.orderID=ol.orderID " +
                           "where o.completed = 0 and ol.dynID<>0  and date(o.ts)=date('" + dateString + "') and o.orderID=" + orderid + " and o.shiftID=" + mShiftID + " " +
                           "group by groupName) as tmp2 " +
                       "group by itemDesc " +
                      "";
                mConnection.Open();
                dt = mConnection.Select(sql);
                listView1.Items.Clear();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ListViewItem lvi = new ListViewItem(row["Quantity"].ToString());
                        lvi.SubItems.Add(row["ItemDesc"].ToString());
                        lvi.SubItems.Add(money.Format(row["Total"].ToString()));
                        listView1.Items.Add(lvi);
                    }
                }
                return dt;
            }
            catch
            {
                return dt;
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmDriverOffDetail_Load(object sender, EventArgs e)
        {
            LoadDriverOffDetail(OrderID, TS);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(0);
                listView1.Items[0].Selected = true;
                listView1.Items[0].EnsureVisible();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection - 1 < 0)
                    {
                        listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                        listView1.Items[listView1.Items.Count - 1].Selected = true;
                        listView1.Items[listView1.Items.Count - 1].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection - 1);
                        listView1.Items[oldselection - 1].Selected = true;
                        listView1.Items[oldselection - 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                try
                {
                    int oldselection = listView1.SelectedIndices[0];
                    listView1.SelectedIndices.Clear();
                    if (oldselection + 2 > listView1.Items.Count)
                    {
                        listView1.SelectedIndices.Add(0);
                        listView1.Items[0].Selected = true;
                        listView1.Items[0].EnsureVisible();
                    }
                    else
                    {
                        listView1.SelectedIndices.Add(oldselection + 1);
                        listView1.Items[oldselection + 1].Selected = true;
                        listView1.Items[oldselection + 1].EnsureVisible();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedIndices.Clear();
                listView1.SelectedIndices.Add(listView1.Items.Count - 1);
                listView1.Items[listView1.Items.Count - 1].Selected = true;
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();
            }
        }
    }
}