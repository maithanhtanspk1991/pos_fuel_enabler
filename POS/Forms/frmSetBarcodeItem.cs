﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmSetBarcodeItem : Form
    {
        public frmSetBarcodeItem()
        {
            InitializeComponent();
        }

        private readonly string _NameClass = "POS::Forms::frmSetBarcodeItem::";

        private Connection.Connection conn = new Connection.Connection();
        private Class.MoneyFortmat mMoneyFortmat = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);

        private Barcode.SerialPort mSerialPort;
        public Class.ItemComboDisc[] cboListItem;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public frmSetBarcodeItem(Barcode.SerialPort ser)
        {
            InitializeComponent();
            mSerialPort = ser;
            mSerialPort.AddEvent(new Barcode.SerialPort.MyPortEvenHandler(mSerialPort_Received));
            ucLimit.EventPage+=new POS.Controls.UCLimit.MyEventPage(ucLimit_EventPage);
            LoadBarcode();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                label1.Text = "Tên sản phẩm:";
                label2.Text = "Mã sản phẩm:";
                label8.Text = "Mã vạch:";
                btnClear.Text = "XÓA";
                btnExit.Text = "THOÁT";
                btnUpdate.Text = "CẬP NHẬT";
                btnSearch.Text = "TÌM KIẾM";
                columnHeader1.Text = "Tên nhóm";
                columnHeader3.Text = "Tên sản phẩm";
                columnHeader7.Text = "Mã sản phẩm";
                columnHeader2.Text = "Mã quét";
                columnHeader4.Text = "Đơn vị giá";
                return;
            }
        }

        private void LoadBarcode()
        {
            try
            {
                conn.Open();
                string sql = "select i.itemID, g.groupDesc, i.scanCode, i.ItemCode, i.itemShort, i.itemDesc, i.unitPrice from itemsmenu i inner Join groupsmenu g on g.GroupID = i.groupID where i.deleted<>1 and i.isFuel = '0'";
                if (ucLimit.mLimit.IsLimit)
                {
                    BusinessObject.BOLimit.GetLimit(sql, ucLimit.mLimit);
                    sql += ucLimit.mLimit.SqlLimit;
                }
                sql += ";";
                ucLimit.PageNumReload();
                DataTable dt = conn.Select(sql);
                lvMenu.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                    li.SubItems.Add(row["itemDesc"].ToString());
                    li.SubItems.Add(row["ItemCode"].ToString());
                    li.SubItems.Add(row["scanCode"].ToString());
                    li.SubItems.Add(mMoneyFortmat.Format2(Convert.ToDouble(row["unitPrice"])));
                    li.Tag = row;
                    lvMenu.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_NameClass + "SearchItems:::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void mSerialPort_Received(string data)
        {
            mSerialPort.SetText(data, txtBarcode);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchItems();
        }

        private void SearchItems()
        {
            lbStatus.Text = "";
            if (txtItemName.Text == "" && txtItemCode.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Mã sản phẩm và tên sản phẩm bị trống!";
                }
				else{
					lvMenu.Items.Clear();
                lbStatus.Text = "Item code and item name empty!";
				}
                
            }
            else if (txtItemCode.Text.Length < 2 && txtItemName.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Từ khóa tìm kiếm mã sản phẩm phải dài hơn 3 từ.";
                }else{
					lvMenu.Items.Clear();
                lbStatus.Text = "Keyword length search of item code must be great more than three word.";
				}
                
            }
            else if (txtItemCode.Text == "" && txtItemName.Text.Length < 2)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lvMenu.Items.Clear();
                    lbStatus.Text = "Từ khóa tìm kiếm tên sản phẩm phải dài hơn 3 từ.";
                }
				else{
					lvMenu.Items.Clear();
                lbStatus.Text = "Keyword length search of Item Name must be great more than three word.";
				}
                
            }
            else
            {
                try
                {
                    conn.Open();
                    string sql = "select i.itemID, g.groupDesc, i.scanCode, i.ItemCode, i.itemShort, i.itemDesc, i.unitPrice from itemsmenu i inner Join groupsmenu g on g.GroupID = i.groupID where i.deleted<>1 and i.isFuel = 0 and i.ItemCode LIKE " + "'%" + txtItemCode.Text.Trim() + "%' and (i.itemDesc like '%" + txtItemName.Text.Trim() + "%' or i.itemShort like '%" + txtItemName.Text.Trim() + "%')";
                    if (ucLimit.mLimit.IsLimit)
                    {
                        BusinessObject.BOLimit.GetLimit(sql, ucLimit.mLimit);
                        sql += ucLimit.mLimit.SqlLimit;
                    }
                    sql += ";";
                    ucLimit.PageNumReload();
                    DataTable dt = conn.Select(sql);
                    lvMenu.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        ListViewItem li = new ListViewItem(row["groupDesc"].ToString());
                        li.SubItems.Add(row["itemDesc"].ToString());
                        li.SubItems.Add(row["ItemCode"].ToString());
                        li.SubItems.Add(row["scanCode"].ToString());
                        li.SubItems.Add(mMoneyFortmat.Format2(Convert.ToDouble(row["unitPrice"])));
                        li.Tag = row;
                        lvMenu.Items.Add(li);
                    }
                }
                catch (Exception ex)
                {
                    Class.LogPOS.WriteLog(_NameClass + "SearchItems:::" + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (mSerialPort != null)
            {
                mSerialPort.TypeOfBarcode = Barcode.SerialPort.ORDER_ALL;
            }
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        void ucLimit_EventPage(DataObject.Limit limit)
        {
            LoadBarcode();
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            if (IsTextChanged && txtItemName.Text.Length > 2)
                SearchItems();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lbStatus.Text = "";
            IsTextChanged = false;
            txtItemCode.Text = "";
            txtItemName.Text = "";
            IsTextChanged = true;
        }

        private bool IsTextChanged = true;
        private void txtItemCode_TextChanged(object sender, EventArgs e)
        {

            if (IsTextChanged && txtItemCode.Text.Length > 2)
                SearchItems();
        }

        private DataRow mRow = null;

        private void lvMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
            if (lvMenu.SelectedItems.Count > 0)
            {
                mRow = (DataRow)lvMenu.SelectedItems[0].Tag;
                IsTextChanged = false;
                txtItemCode.Text = mRow["ItemCode"].ToString();
                txtItemName.Text = mRow["itemDesc"].ToString();
                txtBarcode.Text = mRow["scanCode"].ToString();
                IsTextChanged = true;
            }
        }

        private bool CheckBarcodeExsits()
        {
            bool result = false;
            try
            {
                conn.Open();
                string sql = "Select COUNT(*) FROM itemsmenu Where scanCode = " + txtBarcode.Text.Trim() + " and deleted = 0;";
                result = conn.Select(sql).Rows[0][0].ToString() != "0" ? true : false;
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog(_NameClass + "btnUpdate_Click::" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvMenu.SelectedItems.Count > 0)
            {
                mRow = (DataRow)lvMenu.SelectedItems[0].Tag;
                if (txtBarcode.Text != "")
                {
                    if (!CheckBarcodeExsits())
                    {
                        try
                        {
                            conn.Open();
                            string sql = "Update itemsmenu set scanCode = " + txtBarcode.Text.Trim() + " Where itemID = " + mRow["itemID"].ToString() + ";";
                            conn.ExecuteNonQuery(sql);
                        }
                        catch (Exception ex)
                        {
                            Class.LogPOS.WriteLog(_NameClass + "btnUpdate_Click::" + ex.Message);
                        }
                        finally
                        {
                            conn.Close();
                        }
                        SearchItems();
                    }
                    else
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            lbStatus.Text = "Mã vạch đã tồn tại!";
                            return;
                        }
                        lbStatus.Text = "Barcode exsist!";
                    }
                }
                else
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Mã vạch không được trống!";
                        return;
                    }
                    lbStatus.Text = "Barcode not empty!";
                }
            }
            else
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Chọn sản phẩm!";
                    return;
                }
                lbStatus.Text = "Select item!";
            }
        }
    }
}
