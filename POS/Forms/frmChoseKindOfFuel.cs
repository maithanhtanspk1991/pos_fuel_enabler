﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class frmChoseKindOfFuel : Form
    {
        private int intTotalPump;
        private DataTable dtSourceKindOfFuel;
        private DataTable dtSourceCountPumpHaveFuel;
        private DataTable dtSourceAllKindOfFuel;
        private DataTable dtSourceAllPumpHead;
        private Connection.Connection con;
        private string strSQLQuery;

        public frmChoseKindOfFuel()
        {
            InitializeComponent();
        }

        public frmChoseKindOfFuel(int intTotalPump, DataTable dtSourceKindOfFuel)
        {
            InitializeComponent();
            this.intTotalPump = intTotalPump;
            this.dtSourceKindOfFuel = dtSourceKindOfFuel;
        }

        private void frmChoseKindOfFuel_Load(object sender, EventArgs e)
        {
            con = new Connection.Connection();
            LoadAllPumpHeah();
            LoadAllKindOfFuel();
            LoadAllInformationAboutFuelPump();
        }

        private void LoadAllPumpHeah()
        {
            try
            {
                con.Open();
                dtSourceAllPumpHead = new DataTable();
                dtSourceAllPumpHead = con.Select("SELECT PumpHeadID,PumpHeadName FROM pumphead");
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadAllPumpHeah:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void LoadAllKindOfFuel()
        {
            try
            {
                con.Open();
                dtSourceAllKindOfFuel = new DataTable();
                dtSourceAllKindOfFuel = con.Select("SELECT itemID,itemShort FROM itemsmenu i where i.isFuel = 1");
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadAllKindOfFuel:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void LoadAllInformationAboutFuelPump()
        {
            DataRow[] foundRows;
            try
            {
                for (int i = 1; i <= intTotalPump; i++)
                {
                    UCChoseKindOfFuel ucChosePump = new UCChoseKindOfFuel();
                    ucChosePump.lblPumID.Text = i.ToString();

                    foundRows = dtSourceKindOfFuel.Select("KindID = " + i);
                    ucChosePump.cboKindOfFuel.DataSource = dtSourceAllKindOfFuel;
                    ucChosePump.cboKindOfFuel.ValueMember = "itemID";
                    ucChosePump.cboKindOfFuel.DisplayMember = "itemShort";
                    try
                    {
                        ucChosePump.cboKindOfFuel.SelectedValue = foundRows[0]["MenuItemID"];
                    }
                    catch (Exception)
                    {
                    }
                    ucChosePump.cboKindOfFuel.Tag = i.ToString();

                    ucChosePump.cboPumpHead.DataSource = dtSourceAllPumpHead;
                    ucChosePump.cboPumpHead.ValueMember = "PumpHeadID";
                    ucChosePump.cboPumpHead.DisplayMember = "PumpHeadName";
                    try
                    {
                        ucChosePump.cboPumpHead.SelectedValue = foundRows[0]["PumpHeadID"];
                    }
                    catch (Exception)
                    {
                    }
                    ucChosePump.cboPumpHead.Tag = i.ToString();

                    ucChosePump.Tag = i.ToString();

                    flow_layout_chose_kind_of_fuel.Controls.Add(ucChosePump);
                    ucChosePump.cboKindOfFuel.SelectedIndexChanged += new EventHandler(cboKindOfFuel_SelectedIndexChanged);
                    ucChosePump.cboPumpHead.SelectedIndexChanged += new EventHandler(cboPumpHead_SelectedIndexChanged);
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("LoadAllInformationAboutFuelPump::" + ex.Message);
            }
        }

        private void cboPumpHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                ComboBox cboCombobox = (ComboBox)sender;
                strSQLQuery = "UPDATE fuel set PumpHeadID = " + cboCombobox.SelectedValue + " where KindID = " + cboCombobox.Tag;
                con.ExecuteNonQuery(strSQLQuery);
                strSQLQuery = "SELECT f.KindID , f.MenuItemID , i.unitPrice,f.PumpHeadID FROM fuel f inner join itemsmenu i on f.MenuItemID = i.itemID";
                frmOrdersAll.dtSourceKindOfFuel = con.Select(strSQLQuery);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboPumpHead_SelectedIndexChanged:::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void cboKindOfFuel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                ComboBox cboCombobox = (ComboBox)sender;
                dtSourceCountPumpHaveFuel = con.Select("SELECT COUNT(ID) AS Numrows FROM fuel where KindID = " + cboCombobox.Tag);
                if (Convert.ToInt32(dtSourceCountPumpHaveFuel.Rows[0]["Numrows"]) > 0)
                    strSQLQuery = "UPDATE fuel set MenuItemID = " + cboCombobox.SelectedValue + " where KindID = " + cboCombobox.Tag;
                else
                    strSQLQuery = "INSERT INTO fuel(MenuItemID,KindID) values('" + cboCombobox.SelectedValue + "','" + cboCombobox.Tag + "')";
                con.ExecuteNonQuery(strSQLQuery);
                strSQLQuery = "SELECT f.KindID , f.MenuItemID , i.unitPrice,f.PumpHeadID FROM fuel f inner join itemsmenu i on f.MenuItemID = i.itemID";
                frmOrdersAll.dtSourceKindOfFuel = con.Select(strSQLQuery);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("cboKindOfFuel::" + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}