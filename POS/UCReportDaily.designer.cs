﻿namespace POS
{
    partial class UCReportDaily
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.labelTotalSale = new System.Windows.Forms.Label();
            this.labelTotalDiscount = new System.Windows.Forms.Label();
            this.labelTotalGST = new System.Windows.Forms.Label();
            this.labelCard = new System.Windows.Forms.Label();
            this.labelCash = new System.Windows.Forms.Label();
            this.labelRefunc = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button25 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.dateTimePickerfrom = new System.Windows.Forms.DateTimePicker();
            this.labelCreditNote = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCashOut = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tcSalesPerStaff = new System.Windows.Forms.TabControl();
            this.tbpDailyReport = new System.Windows.Forms.TabPage();
            this.panel15 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstCard = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotalCard = new System.Windows.Forms.Label();
            this.lblTotalSurcharge = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lstCardCashOut = new System.Windows.Forms.ListView();
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblCashTotal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelCashTotal = new System.Windows.Forms.Label();
            this.labelCardTotal = new System.Windows.Forms.Label();
            this.labelSumTotal = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblSumTotal = new System.Windows.Forms.Label();
            this.lblCardTotal = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelAccount = new System.Windows.Forms.Label();
            this.labelBankEftposCashOut = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelSafeDrop = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelPayOut = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lblFeeDelivery = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelCashIn = new System.Windows.Forms.Label();
            this.lblSurcharge = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTotalAccount = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelCardAccount = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelCashAccount = new System.Windows.Forms.Label();
            this.tbpQuantity = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lstQuantityReportShopSales = new System.Windows.Forms.ListView();
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblTotal1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.lstQuantityReportFuelSales = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.tbpGroupQuantity = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lstGroupItemShopSales = new System.Windows.Forms.ListView();
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstGroupItemFuelSales = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblTotal2 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.tbpDelivery = new System.Windows.Forms.TabPage();
            this.tbpListPickup = new System.Windows.Forms.TabPage();
            this.tbpSalesPerStaff = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.lstEmployee = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tbpCashInOrCashOut = new System.Windows.Forms.TabPage();
            this.lstCashOut = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblSafe = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lblCashIn = new System.Windows.Forms.Label();
            this.lblCashOut = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lstHistoryShift = new System.Windows.Forms.ListView();
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbpDebitAccount = new System.Windows.Forms.TabPage();
            this.lstDebitAccount = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblTotal3 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.tbpCreditAccount = new System.Windows.Forms.TabPage();
            this.lstCreditAccount = new System.Windows.Forms.ListView();
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel11 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.tbpDriverOff = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.listViewdriveroff = new System.Windows.Forms.ListView();
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel17 = new System.Windows.Forms.Panel();
            this.button21 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.lbtotaldriveroff = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.tbpCardReject = new System.Windows.Forms.TabPage();
            this.listViewCardReject = new System.Windows.Forms.ListView();
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbpStockTake = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ucListDelivery = new POS.UCComponentReport.ucListDelivery();
            this.ucListPickup = new POS.UCComponentReport.ucListDelivery();
            this.ucListStockTake2 = new POS.ucReport.ucListStockTake();
            this.panel9.SuspendLayout();
            this.tcSalesPerStaff.SuspendLayout();
            this.tbpDailyReport.SuspendLayout();
            this.panel15.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel14.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tbpQuantity.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbpGroupQuantity.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tbpDelivery.SuspendLayout();
            this.tbpListPickup.SuspendLayout();
            this.tbpSalesPerStaff.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tbpCashInOrCashOut.SuspendLayout();
            this.panel16.SuspendLayout();
            this.tbpDebitAccount.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tbpCreditAccount.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tbpDriverOff.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel17.SuspendLayout();
            this.tbpCardReject.SuspendLayout();
            this.tbpStockTake.SuspendLayout();
            this.panel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 528);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(862, 55);
            this.button1.TabIndex = 3;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelTotalSale
            // 
            this.labelTotalSale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelTotalSale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTotalSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelTotalSale.Location = new System.Drawing.Point(232, 0);
            this.labelTotalSale.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalSale.Name = "labelTotalSale";
            this.labelTotalSale.Size = new System.Drawing.Size(158, 23);
            this.labelTotalSale.TabIndex = 4;
            this.labelTotalSale.Text = "0.00";
            this.labelTotalSale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTotalDiscount
            // 
            this.labelTotalDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTotalDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelTotalDiscount.Location = new System.Drawing.Point(232, 23);
            this.labelTotalDiscount.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalDiscount.Name = "labelTotalDiscount";
            this.labelTotalDiscount.Size = new System.Drawing.Size(158, 23);
            this.labelTotalDiscount.TabIndex = 4;
            this.labelTotalDiscount.Text = "0.00";
            this.labelTotalDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTotalGST
            // 
            this.labelTotalGST.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelTotalGST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTotalGST.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelTotalGST.Location = new System.Drawing.Point(232, 46);
            this.labelTotalGST.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalGST.Name = "labelTotalGST";
            this.labelTotalGST.Size = new System.Drawing.Size(158, 23);
            this.labelTotalGST.TabIndex = 4;
            this.labelTotalGST.Text = "0.00";
            this.labelTotalGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCard
            // 
            this.labelCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelCard.Location = new System.Drawing.Point(232, 69);
            this.labelCard.Margin = new System.Windows.Forms.Padding(0);
            this.labelCard.Name = "labelCard";
            this.labelCard.Size = new System.Drawing.Size(158, 23);
            this.labelCard.TabIndex = 4;
            this.labelCard.Text = "0.00";
            this.labelCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCash
            // 
            this.labelCash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelCash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelCash.Location = new System.Drawing.Point(232, 92);
            this.labelCash.Margin = new System.Windows.Forms.Padding(0);
            this.labelCash.Name = "labelCash";
            this.labelCash.Size = new System.Drawing.Size(158, 23);
            this.labelCash.TabIndex = 4;
            this.labelCash.Text = "0.00";
            this.labelCash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRefunc
            // 
            this.labelRefunc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelRefunc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefunc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelRefunc.Location = new System.Drawing.Point(232, 138);
            this.labelRefunc.Margin = new System.Windows.Forms.Padding(0);
            this.labelRefunc.Name = "labelRefunc";
            this.labelRefunc.Size = new System.Drawing.Size(158, 23);
            this.labelRefunc.TabIndex = 4;
            this.labelRefunc.Text = "0.00";
            this.labelRefunc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.button25);
            this.panel9.Controls.Add(this.button18);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.dateTimePickerfrom);
            this.panel9.Controls.Add(this.labelCreditNote);
            this.panel9.Controls.Add(this.label5);
            this.panel9.Controls.Add(this.labelCashOut);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(862, 48);
            this.panel9.TabIndex = 11;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.Green;
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(458, 4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(286, 41);
            this.button25.TabIndex = 41;
            this.button25.Text = "PRINT ALL REPORT";
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button18
            // 
            this.button18.Image = global::POS.Properties.Resources.refesh;
            this.button18.Location = new System.Drawing.Point(404, 2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(47, 44);
            this.button18.TabIndex = 40;
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(207, 44);
            this.label11.TabIndex = 39;
            this.label11.Text = "Date view:";
            // 
            // dateTimePickerfrom
            // 
            this.dateTimePickerfrom.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerfrom.Location = new System.Drawing.Point(216, 2);
            this.dateTimePickerfrom.Name = "dateTimePickerfrom";
            this.dateTimePickerfrom.Size = new System.Drawing.Size(187, 44);
            this.dateTimePickerfrom.TabIndex = 38;
            this.dateTimePickerfrom.ValueChanged += new System.EventHandler(this.dateTimePickerfrom_ValueChanged);
            // 
            // labelCreditNote
            // 
            this.labelCreditNote.Enabled = false;
            this.labelCreditNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCreditNote.Location = new System.Drawing.Point(547, 17);
            this.labelCreditNote.Name = "labelCreditNote";
            this.labelCreditNote.Size = new System.Drawing.Size(163, 20);
            this.labelCreditNote.TabIndex = 6;
            this.labelCreditNote.Text = "0.00";
            this.labelCreditNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCreditNote.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(464, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Eftpos:";
            this.label5.Visible = false;
            // 
            // labelCashOut
            // 
            this.labelCashOut.Enabled = false;
            this.labelCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashOut.Location = new System.Drawing.Point(584, 2);
            this.labelCashOut.Name = "labelCashOut";
            this.labelCashOut.Size = new System.Drawing.Size(126, 20);
            this.labelCashOut.TabIndex = 12;
            this.labelCashOut.Text = "0.00";
            this.labelCashOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCashOut.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(456, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Cash Out:";
            this.label8.Visible = false;
            // 
            // tcSalesPerStaff
            // 
            this.tcSalesPerStaff.Controls.Add(this.tbpDailyReport);
            this.tcSalesPerStaff.Controls.Add(this.tbpQuantity);
            this.tcSalesPerStaff.Controls.Add(this.tbpGroupQuantity);
            this.tcSalesPerStaff.Controls.Add(this.tbpDelivery);
            this.tcSalesPerStaff.Controls.Add(this.tbpListPickup);
            this.tcSalesPerStaff.Controls.Add(this.tbpSalesPerStaff);
            this.tcSalesPerStaff.Controls.Add(this.tbpCashInOrCashOut);
            this.tcSalesPerStaff.Controls.Add(this.tbpDebitAccount);
            this.tcSalesPerStaff.Controls.Add(this.tbpCreditAccount);
            this.tcSalesPerStaff.Controls.Add(this.tbpDriverOff);
            this.tcSalesPerStaff.Controls.Add(this.tbpCardReject);
            this.tcSalesPerStaff.Controls.Add(this.tbpStockTake);
            this.tcSalesPerStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSalesPerStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcSalesPerStaff.Location = new System.Drawing.Point(0, 0);
            this.tcSalesPerStaff.Name = "tcSalesPerStaff";
            this.tcSalesPerStaff.SelectedIndex = 0;
            this.tcSalesPerStaff.Size = new System.Drawing.Size(862, 480);
            this.tcSalesPerStaff.TabIndex = 3;
            this.tcSalesPerStaff.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tbpDailyReport
            // 
            this.tbpDailyReport.Controls.Add(this.panel15);
            this.tbpDailyReport.Controls.Add(this.panel14);
            this.tbpDailyReport.Location = new System.Drawing.Point(4, 33);
            this.tbpDailyReport.Name = "tbpDailyReport";
            this.tbpDailyReport.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDailyReport.Size = new System.Drawing.Size(854, 443);
            this.tbpDailyReport.TabIndex = 0;
            this.tbpDailyReport.Text = "Daily Sales";
            this.tbpDailyReport.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.groupBox2);
            this.panel15.Controls.Add(this.groupBox4);
            this.panel15.Controls.Add(this.groupBox5);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(399, 3);
            this.panel15.Name = "panel15";
            this.panel15.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.panel15.Size = new System.Drawing.Size(452, 437);
            this.panel15.TabIndex = 71;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstCard);
            this.groupBox2.Controls.Add(this.tableLayoutPanel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(5, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(447, 170);
            this.groupBox2.TabIndex = 54;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Card";
            // 
            // lstCard
            // 
            this.lstCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader46});
            this.lstCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCard.FullRowSelect = true;
            this.lstCard.GridLines = true;
            this.lstCard.HideSelection = false;
            this.lstCard.HoverSelection = true;
            this.lstCard.Location = new System.Drawing.Point(3, 22);
            this.lstCard.MultiSelect = false;
            this.lstCard.Name = "lstCard";
            this.lstCard.Size = new System.Drawing.Size(441, 115);
            this.lstCard.TabIndex = 53;
            this.lstCard.UseCompatibleStateImageBehavior = false;
            this.lstCard.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Card Name";
            this.columnHeader7.Width = 146;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Subtotal";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader8.Width = 119;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "Surcharge";
            this.columnHeader46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader46.Width = 102;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblTotalCard, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblTotalSurcharge, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 137);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(441, 30);
            this.tableLayoutPanel3.TabIndex = 55;
            // 
            // lblTotalCard
            // 
            this.lblTotalCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCard.Location = new System.Drawing.Point(0, 0);
            this.lblTotalCard.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotalCard.Name = "lblTotalCard";
            this.lblTotalCard.Size = new System.Drawing.Size(220, 30);
            this.lblTotalCard.TabIndex = 54;
            this.lblTotalCard.Text = "Total:";
            this.lblTotalCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalSurcharge
            // 
            this.lblTotalSurcharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSurcharge.Location = new System.Drawing.Point(220, 0);
            this.lblTotalSurcharge.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotalSurcharge.Name = "lblTotalSurcharge";
            this.lblTotalSurcharge.Size = new System.Drawing.Size(221, 30);
            this.lblTotalSurcharge.TabIndex = 54;
            this.lblTotalSurcharge.Text = "Total:";
            this.lblTotalSurcharge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lstCardCashOut);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(5, 170);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(447, 186);
            this.groupBox4.TabIndex = 69;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bank Eftpos Cash Out";
            // 
            // lstCardCashOut
            // 
            this.lstCardCashOut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader33,
            this.columnHeader34});
            this.lstCardCashOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCardCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCardCashOut.FullRowSelect = true;
            this.lstCardCashOut.GridLines = true;
            this.lstCardCashOut.HideSelection = false;
            this.lstCardCashOut.HoverSelection = true;
            this.lstCardCashOut.Location = new System.Drawing.Point(3, 22);
            this.lstCardCashOut.MultiSelect = false;
            this.lstCardCashOut.Name = "lstCardCashOut";
            this.lstCardCashOut.Size = new System.Drawing.Size(441, 131);
            this.lstCardCashOut.TabIndex = 53;
            this.lstCardCashOut.UseCompatibleStateImageBehavior = false;
            this.lstCardCashOut.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Card Name";
            this.columnHeader33.Width = 142;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Subtotal";
            this.columnHeader34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader34.Width = 188;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 153);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(441, 30);
            this.label28.TabIndex = 54;
            this.label28.Text = "Total:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.lblCashTotal);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.labelCashTotal);
            this.groupBox5.Controls.Add(this.labelCardTotal);
            this.groupBox5.Controls.Add(this.labelSumTotal);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.lblSumTotal);
            this.groupBox5.Controls.Add(this.lblCardTotal);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox5.Location = new System.Drawing.Point(5, 356);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(447, 81);
            this.groupBox5.TabIndex = 72;
            this.groupBox5.TabStop = false;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F);
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(25, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 19);
            this.label15.TabIndex = 56;
            this.label15.Text = "Total Cash";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCashTotal
            // 
            this.lblCashTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCashTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.75F);
            this.lblCashTotal.ForeColor = System.Drawing.Color.Blue;
            this.lblCashTotal.Location = new System.Drawing.Point(6, 37);
            this.lblCashTotal.Name = "lblCashTotal";
            this.lblCashTotal.Size = new System.Drawing.Size(120, 30);
            this.lblCashTotal.TabIndex = 59;
            this.lblCashTotal.Text = "0.00";
            this.lblCashTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F);
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(151, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 19);
            this.label4.TabIndex = 57;
            this.label4.Text = "Total Card";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCashTotal
            // 
            this.labelCashTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashTotal.ForeColor = System.Drawing.Color.Blue;
            this.labelCashTotal.Location = new System.Drawing.Point(384, 36);
            this.labelCashTotal.Name = "labelCashTotal";
            this.labelCashTotal.Size = new System.Drawing.Size(31, 32);
            this.labelCashTotal.TabIndex = 46;
            this.labelCashTotal.Text = "0.00";
            this.labelCashTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCashTotal.Visible = false;
            // 
            // labelCardTotal
            // 
            this.labelCardTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardTotal.ForeColor = System.Drawing.Color.Blue;
            this.labelCardTotal.Location = new System.Drawing.Point(421, 35);
            this.labelCardTotal.Name = "labelCardTotal";
            this.labelCardTotal.Size = new System.Drawing.Size(26, 32);
            this.labelCardTotal.TabIndex = 46;
            this.labelCardTotal.Text = "0.00";
            this.labelCardTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCardTotal.Visible = false;
            // 
            // labelSumTotal
            // 
            this.labelSumTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSumTotal.ForeColor = System.Drawing.Color.Red;
            this.labelSumTotal.Location = new System.Drawing.Point(453, 36);
            this.labelSumTotal.Name = "labelSumTotal";
            this.labelSumTotal.Size = new System.Drawing.Size(20, 32);
            this.labelSumTotal.TabIndex = 46;
            this.labelSumTotal.Text = "0.00";
            this.labelSumTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSumTotal.Visible = false;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F);
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(293, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 19);
            this.label22.TabIndex = 58;
            this.label22.Text = "Sum";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSumTotal
            // 
            this.lblSumTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSumTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.75F);
            this.lblSumTotal.ForeColor = System.Drawing.Color.Red;
            this.lblSumTotal.Location = new System.Drawing.Point(258, 37);
            this.lblSumTotal.Name = "lblSumTotal";
            this.lblSumTotal.Size = new System.Drawing.Size(120, 30);
            this.lblSumTotal.TabIndex = 61;
            this.lblSumTotal.Text = "0.00";
            this.lblSumTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCardTotal
            // 
            this.lblCardTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCardTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.75F);
            this.lblCardTotal.ForeColor = System.Drawing.Color.Blue;
            this.lblCardTotal.Location = new System.Drawing.Point(132, 37);
            this.lblCardTotal.Name = "lblCardTotal";
            this.lblCardTotal.Size = new System.Drawing.Size(120, 30);
            this.lblCardTotal.TabIndex = 60;
            this.lblCardTotal.Text = "0.00";
            this.lblCardTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.groupBox1);
            this.panel14.Controls.Add(this.groupBox3);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(3, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(396, 437);
            this.panel14.TabIndex = 70;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 331);
            this.groupBox1.TabIndex = 52;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sales";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.67302F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.32698F));
            this.tableLayoutPanel1.Controls.Add(this.labelAccount, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.labelBankEftposCashOut, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.labelTotalSale, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelSafeDrop, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.labelTotalDiscount, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelPayOut, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.labelTotalGST, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelCard, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label29, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblFeeDelivery, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelCash, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelCashIn, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblSurcharge, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label31, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelRefunc, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(390, 303);
            this.tableLayoutPanel1.TabIndex = 57;
            // 
            // labelAccount
            // 
            this.labelAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelAccount.Location = new System.Drawing.Point(232, 276);
            this.labelAccount.Margin = new System.Windows.Forms.Padding(0);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(158, 27);
            this.labelAccount.TabIndex = 52;
            this.labelAccount.Text = "0.00";
            this.labelAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelBankEftposCashOut
            // 
            this.labelBankEftposCashOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBankEftposCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelBankEftposCashOut.Location = new System.Drawing.Point(232, 253);
            this.labelBankEftposCashOut.Margin = new System.Windows.Forms.Padding(0);
            this.labelBankEftposCashOut.Name = "labelBankEftposCashOut";
            this.labelBankEftposCashOut.Size = new System.Drawing.Size(158, 23);
            this.labelBankEftposCashOut.TabIndex = 56;
            this.labelBankEftposCashOut.Text = "0.00";
            this.labelBankEftposCashOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 276);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(232, 27);
            this.label13.TabIndex = 51;
            this.label13.Text = "Account:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(0, 253);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(232, 23);
            this.label19.TabIndex = 55;
            this.label19.Text = "*Bank eftpos cash out";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSafeDrop
            // 
            this.labelSafeDrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelSafeDrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSafeDrop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelSafeDrop.Location = new System.Drawing.Point(232, 230);
            this.labelSafeDrop.Margin = new System.Windows.Forms.Padding(0);
            this.labelSafeDrop.Name = "labelSafeDrop";
            this.labelSafeDrop.Size = new System.Drawing.Size(158, 23);
            this.labelSafeDrop.TabIndex = 52;
            this.labelSafeDrop.Text = "0.00";
            this.labelSafeDrop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 23);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(232, 23);
            this.label17.TabIndex = 2;
            this.label17.Text = "Total Discount:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(0, 230);
            this.label26.Margin = new System.Windows.Forms.Padding(0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(232, 23);
            this.label26.TabIndex = 51;
            this.label26.Text = "Safe Drop:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPayOut
            // 
            this.labelPayOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPayOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelPayOut.Location = new System.Drawing.Point(232, 207);
            this.labelPayOut.Margin = new System.Windows.Forms.Padding(0);
            this.labelPayOut.Name = "labelPayOut";
            this.labelPayOut.Size = new System.Drawing.Size(158, 23);
            this.labelPayOut.TabIndex = 52;
            this.labelPayOut.Text = "0.00";
            this.labelPayOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(0, 46);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(232, 23);
            this.label25.TabIndex = 2;
            this.label25.Text = "Total GST:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 207);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(232, 23);
            this.label12.TabIndex = 51;
            this.label12.Text = "Pay Out:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 69);
            this.label27.Margin = new System.Windows.Forms.Padding(0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(232, 23);
            this.label27.TabIndex = 2;
            this.label27.Text = "Card:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 92);
            this.label29.Margin = new System.Windows.Forms.Padding(0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(232, 23);
            this.label29.TabIndex = 2;
            this.label29.Text = "Cash:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFeeDelivery
            // 
            this.lblFeeDelivery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblFeeDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFeeDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblFeeDelivery.Location = new System.Drawing.Point(232, 184);
            this.lblFeeDelivery.Margin = new System.Windows.Forms.Padding(0);
            this.lblFeeDelivery.Name = "lblFeeDelivery";
            this.lblFeeDelivery.Size = new System.Drawing.Size(158, 23);
            this.lblFeeDelivery.TabIndex = 11;
            this.lblFeeDelivery.Text = "0.00";
            this.lblFeeDelivery.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(0, 184);
            this.label24.Margin = new System.Windows.Forms.Padding(0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(232, 23);
            this.label24.TabIndex = 9;
            this.label24.Text = "Fee Delivery:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 115);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(232, 23);
            this.label9.TabIndex = 2;
            this.label9.Text = "Surcharge:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCashIn
            // 
            this.labelCashIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCashIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelCashIn.Location = new System.Drawing.Point(232, 161);
            this.labelCashIn.Margin = new System.Windows.Forms.Padding(0);
            this.labelCashIn.Name = "labelCashIn";
            this.labelCashIn.Size = new System.Drawing.Size(158, 23);
            this.labelCashIn.TabIndex = 11;
            this.labelCashIn.Text = "0.00";
            this.labelCashIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSurcharge
            // 
            this.lblSurcharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSurcharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblSurcharge.Location = new System.Drawing.Point(232, 115);
            this.lblSurcharge.Margin = new System.Windows.Forms.Padding(0);
            this.lblSurcharge.Name = "lblSurcharge";
            this.lblSurcharge.Size = new System.Drawing.Size(158, 23);
            this.lblSurcharge.TabIndex = 4;
            this.lblSurcharge.Text = "0.00";
            this.lblSurcharge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 161);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(232, 23);
            this.label7.TabIndex = 9;
            this.label7.Text = "Cash In:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 138);
            this.label31.Margin = new System.Windows.Forms.Padding(0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(232, 23);
            this.label31.TabIndex = 2;
            this.label31.Text = "Refund:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(232, 23);
            this.label6.TabIndex = 1;
            this.label6.Text = "Sales:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(0, 331);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(396, 106);
            this.groupBox3.TabIndex = 55;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Account Payment";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelTotalAccount, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label23, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelCardAccount, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelCashAccount, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(390, 81);
            this.tableLayoutPanel2.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(195, 27);
            this.label10.TabIndex = 45;
            this.label10.Text = "Card";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTotalAccount
            // 
            this.labelTotalAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelTotalAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTotalAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAccount.Location = new System.Drawing.Point(195, 54);
            this.labelTotalAccount.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalAccount.Name = "labelTotalAccount";
            this.labelTotalAccount.Size = new System.Drawing.Size(195, 27);
            this.labelTotalAccount.TabIndex = 46;
            this.labelTotalAccount.Text = "0.00";
            this.labelTotalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 54);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(195, 27);
            this.label23.TabIndex = 45;
            this.label23.Text = "Total";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCardAccount
            // 
            this.labelCardAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelCardAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCardAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardAccount.Location = new System.Drawing.Point(195, 0);
            this.labelCardAccount.Margin = new System.Windows.Forms.Padding(0);
            this.labelCardAccount.Name = "labelCardAccount";
            this.labelCardAccount.Size = new System.Drawing.Size(195, 27);
            this.labelCardAccount.TabIndex = 46;
            this.labelCardAccount.Text = "0.00";
            this.labelCardAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 27);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(195, 27);
            this.label16.TabIndex = 45;
            this.label16.Text = "Cash";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCashAccount
            // 
            this.labelCashAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCashAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashAccount.Location = new System.Drawing.Point(195, 27);
            this.labelCashAccount.Margin = new System.Windows.Forms.Padding(0);
            this.labelCashAccount.Name = "labelCashAccount";
            this.labelCashAccount.Size = new System.Drawing.Size(195, 27);
            this.labelCashAccount.TabIndex = 46;
            this.labelCashAccount.Text = "0.00";
            this.labelCashAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbpQuantity
            // 
            this.tbpQuantity.Controls.Add(this.panel2);
            this.tbpQuantity.Controls.Add(this.panel1);
            this.tbpQuantity.Location = new System.Drawing.Point(4, 33);
            this.tbpQuantity.Name = "tbpQuantity";
            this.tbpQuantity.Padding = new System.Windows.Forms.Padding(3);
            this.tbpQuantity.Size = new System.Drawing.Size(854, 443);
            this.tbpQuantity.TabIndex = 1;
            this.tbpQuantity.Text = "Quantity Report";
            this.tbpQuantity.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstQuantityReportShopSales);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.lstQuantityReportFuelSales);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(848, 414);
            this.panel2.TabIndex = 8;
            // 
            // lstQuantityReportShopSales
            // 
            this.lstQuantityReportShopSales.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader32,
            this.columnHeader41,
            this.columnHeader42});
            this.lstQuantityReportShopSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstQuantityReportShopSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstQuantityReportShopSales.FullRowSelect = true;
            this.lstQuantityReportShopSales.GridLines = true;
            this.lstQuantityReportShopSales.HideSelection = false;
            this.lstQuantityReportShopSales.Location = new System.Drawing.Point(0, 117);
            this.lstQuantityReportShopSales.MultiSelect = false;
            this.lstQuantityReportShopSales.Name = "lstQuantityReportShopSales";
            this.lstQuantityReportShopSales.Size = new System.Drawing.Size(848, 259);
            this.lstQuantityReportShopSales.TabIndex = 38;
            this.lstQuantityReportShopSales.UseCompatibleStateImageBehavior = false;
            this.lstQuantityReportShopSales.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Item name - SHOP SALES";
            this.columnHeader32.Width = 351;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Quantity";
            this.columnHeader41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader41.Width = 145;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "Total";
            this.columnHeader42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader42.Width = 205;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblTotal1);
            this.panel6.Controls.Add(this.button2);
            this.panel6.Controls.Add(this.button4);
            this.panel6.Controls.Add(this.button3);
            this.panel6.Controls.Add(this.button5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 376);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(848, 38);
            this.panel6.TabIndex = 37;
            // 
            // lblTotal1
            // 
            this.lblTotal1.AutoSize = true;
            this.lblTotal1.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblTotal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal1.Location = new System.Drawing.Point(799, 0);
            this.lblTotal1.Name = "lblTotal1";
            this.lblTotal1.Size = new System.Drawing.Size(49, 20);
            this.lblTotal1.TabIndex = 43;
            this.lblTotal1.Text = "Total";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(2, 4);
            this.button2.Margin = new System.Windows.Forms.Padding(1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 31);
            this.button2.TabIndex = 33;
            this.button2.Text = "<<";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(248, 4);
            this.button4.Margin = new System.Windows.Forms.Padding(1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 31);
            this.button4.TabIndex = 36;
            this.button4.Text = ">>";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(84, 4);
            this.button3.Margin = new System.Windows.Forms.Padding(1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 31);
            this.button3.TabIndex = 34;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(166, 4);
            this.button5.Margin = new System.Windows.Forms.Padding(1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 31);
            this.button5.TabIndex = 35;
            this.button5.Text = ">";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // lstQuantityReportFuelSales
            // 
            this.lstQuantityReportFuelSales.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader5});
            this.lstQuantityReportFuelSales.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstQuantityReportFuelSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstQuantityReportFuelSales.FullRowSelect = true;
            this.lstQuantityReportFuelSales.GridLines = true;
            this.lstQuantityReportFuelSales.HideSelection = false;
            this.lstQuantityReportFuelSales.Location = new System.Drawing.Point(0, 0);
            this.lstQuantityReportFuelSales.MultiSelect = false;
            this.lstQuantityReportFuelSales.Name = "lstQuantityReportFuelSales";
            this.lstQuantityReportFuelSales.Size = new System.Drawing.Size(848, 117);
            this.lstQuantityReportFuelSales.TabIndex = 0;
            this.lstQuantityReportFuelSales.UseCompatibleStateImageBehavior = false;
            this.lstQuantityReportFuelSales.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item name - FUEL SALES";
            this.columnHeader1.Width = 351;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Quantity";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 145;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Total";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 205;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label33);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(848, 23);
            this.panel1.TabIndex = 7;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 20);
            this.label33.TabIndex = 6;
            this.label33.Text = "Date:";
            // 
            // tbpGroupQuantity
            // 
            this.tbpGroupQuantity.Controls.Add(this.panel5);
            this.tbpGroupQuantity.Controls.Add(this.panel4);
            this.tbpGroupQuantity.Controls.Add(this.panel3);
            this.tbpGroupQuantity.Location = new System.Drawing.Point(4, 33);
            this.tbpGroupQuantity.Name = "tbpGroupQuantity";
            this.tbpGroupQuantity.Padding = new System.Windows.Forms.Padding(3);
            this.tbpGroupQuantity.Size = new System.Drawing.Size(854, 443);
            this.tbpGroupQuantity.TabIndex = 2;
            this.tbpGroupQuantity.Text = "Group Quantity Report";
            this.tbpGroupQuantity.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lstGroupItemShopSales);
            this.panel5.Controls.Add(this.lstGroupItemFuelSales);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 26);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(848, 358);
            this.panel5.TabIndex = 42;
            // 
            // lstGroupItemShopSales
            // 
            this.lstGroupItemShopSales.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45});
            this.lstGroupItemShopSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstGroupItemShopSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstGroupItemShopSales.FullRowSelect = true;
            this.lstGroupItemShopSales.GridLines = true;
            this.lstGroupItemShopSales.HideSelection = false;
            this.lstGroupItemShopSales.HoverSelection = true;
            this.lstGroupItemShopSales.Location = new System.Drawing.Point(0, 115);
            this.lstGroupItemShopSales.MultiSelect = false;
            this.lstGroupItemShopSales.Name = "lstGroupItemShopSales";
            this.lstGroupItemShopSales.Size = new System.Drawing.Size(848, 243);
            this.lstGroupItemShopSales.TabIndex = 9;
            this.lstGroupItemShopSales.UseCompatibleStateImageBehavior = false;
            this.lstGroupItemShopSales.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "Group name - SHOP SALES";
            this.columnHeader43.Width = 341;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "Quantity";
            this.columnHeader44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader44.Width = 156;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Total";
            this.columnHeader45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader45.Width = 212;
            // 
            // lstGroupItemFuelSales
            // 
            this.lstGroupItemFuelSales.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6});
            this.lstGroupItemFuelSales.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstGroupItemFuelSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstGroupItemFuelSales.FullRowSelect = true;
            this.lstGroupItemFuelSales.GridLines = true;
            this.lstGroupItemFuelSales.HideSelection = false;
            this.lstGroupItemFuelSales.HoverSelection = true;
            this.lstGroupItemFuelSales.Location = new System.Drawing.Point(0, 0);
            this.lstGroupItemFuelSales.MultiSelect = false;
            this.lstGroupItemFuelSales.Name = "lstGroupItemFuelSales";
            this.lstGroupItemFuelSales.Size = new System.Drawing.Size(848, 115);
            this.lstGroupItemFuelSales.TabIndex = 8;
            this.lstGroupItemFuelSales.UseCompatibleStateImageBehavior = false;
            this.lstGroupItemFuelSales.View = System.Windows.Forms.View.Details;
            this.lstGroupItemFuelSales.SelectedIndexChanged += new System.EventHandler(this.lstGroupItem_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Group name - FUEL SALES";
            this.columnHeader3.Width = 341;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Quantity";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 156;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Total";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader6.Width = 212;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblTotal2);
            this.panel4.Controls.Add(this.button9);
            this.panel4.Controls.Add(this.button6);
            this.panel4.Controls.Add(this.button8);
            this.panel4.Controls.Add(this.button7);
            this.panel4.Controls.Add(this.button10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 384);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(848, 56);
            this.panel4.TabIndex = 41;
            // 
            // lblTotal2
            // 
            this.lblTotal2.AutoSize = true;
            this.lblTotal2.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblTotal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal2.Location = new System.Drawing.Point(799, 0);
            this.lblTotal2.Name = "lblTotal2";
            this.lblTotal2.Size = new System.Drawing.Size(49, 20);
            this.lblTotal2.TabIndex = 42;
            this.lblTotal2.Text = "Total";
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(329, 3);
            this.button9.Margin = new System.Windows.Forms.Padding(1);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(122, 50);
            this.button9.TabIndex = 41;
            this.button9.Text = "view";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(1, 3);
            this.button6.Margin = new System.Windows.Forms.Padding(1);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(80, 50);
            this.button6.TabIndex = 37;
            this.button6.Text = "<<";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(247, 3);
            this.button8.Margin = new System.Windows.Forms.Padding(1);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(80, 50);
            this.button8.TabIndex = 40;
            this.button8.Text = ">>";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(83, 3);
            this.button7.Margin = new System.Windows.Forms.Padding(1);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 50);
            this.button7.TabIndex = 38;
            this.button7.Text = "<";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(165, 3);
            this.button10.Margin = new System.Windows.Forms.Padding(1);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(80, 50);
            this.button10.TabIndex = 39;
            this.button10.Text = ">";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label34);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(848, 23);
            this.panel3.TabIndex = 9;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 20);
            this.label34.TabIndex = 6;
            this.label34.Text = "Date:";
            // 
            // tbpDelivery
            // 
            this.tbpDelivery.Controls.Add(this.ucListDelivery);
            this.tbpDelivery.Location = new System.Drawing.Point(4, 33);
            this.tbpDelivery.Name = "tbpDelivery";
            this.tbpDelivery.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDelivery.Size = new System.Drawing.Size(854, 443);
            this.tbpDelivery.TabIndex = 9;
            this.tbpDelivery.Text = "Delivery List ";
            this.tbpDelivery.UseVisualStyleBackColor = true;
            // 
            // tbpListPickup
            // 
            this.tbpListPickup.Controls.Add(this.ucListPickup);
            this.tbpListPickup.Location = new System.Drawing.Point(4, 33);
            this.tbpListPickup.Name = "tbpListPickup";
            this.tbpListPickup.Padding = new System.Windows.Forms.Padding(3);
            this.tbpListPickup.Size = new System.Drawing.Size(854, 443);
            this.tbpListPickup.TabIndex = 10;
            this.tbpListPickup.Text = "Pickup List ";
            this.tbpListPickup.UseVisualStyleBackColor = true;
            // 
            // tbpSalesPerStaff
            // 
            this.tbpSalesPerStaff.Controls.Add(this.panel8);
            this.tbpSalesPerStaff.Controls.Add(this.lstEmployee);
            this.tbpSalesPerStaff.Controls.Add(this.panel7);
            this.tbpSalesPerStaff.Location = new System.Drawing.Point(4, 33);
            this.tbpSalesPerStaff.Name = "tbpSalesPerStaff";
            this.tbpSalesPerStaff.Padding = new System.Windows.Forms.Padding(3);
            this.tbpSalesPerStaff.Size = new System.Drawing.Size(854, 443);
            this.tbpSalesPerStaff.TabIndex = 3;
            this.tbpSalesPerStaff.Text = "Sales Per Staff";
            this.tbpSalesPerStaff.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.button11);
            this.panel8.Controls.Add(this.button12);
            this.panel8.Controls.Add(this.button13);
            this.panel8.Controls.Add(this.button14);
            this.panel8.Controls.Add(this.button15);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(3, 384);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(848, 56);
            this.panel8.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(799, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Total";
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(329, 3);
            this.button11.Margin = new System.Windows.Forms.Padding(1);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(122, 50);
            this.button11.TabIndex = 41;
            this.button11.Text = "view";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(1, 3);
            this.button12.Margin = new System.Windows.Forms.Padding(1);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(80, 50);
            this.button12.TabIndex = 37;
            this.button12.Text = "<<";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(247, 3);
            this.button13.Margin = new System.Windows.Forms.Padding(1);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(80, 50);
            this.button13.TabIndex = 40;
            this.button13.Text = ">>";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(83, 3);
            this.button14.Margin = new System.Windows.Forms.Padding(1);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(80, 50);
            this.button14.TabIndex = 38;
            this.button14.Text = "<";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(165, 3);
            this.button15.Margin = new System.Windows.Forms.Padding(1);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(80, 50);
            this.button15.TabIndex = 39;
            this.button15.Text = ">";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // lstEmployee
            // 
            this.lstEmployee.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader13});
            this.lstEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstEmployee.FullRowSelect = true;
            this.lstEmployee.GridLines = true;
            this.lstEmployee.HideSelection = false;
            this.lstEmployee.Location = new System.Drawing.Point(3, 26);
            this.lstEmployee.MultiSelect = false;
            this.lstEmployee.Name = "lstEmployee";
            this.lstEmployee.Size = new System.Drawing.Size(848, 414);
            this.lstEmployee.TabIndex = 9;
            this.lstEmployee.UseCompatibleStateImageBehavior = false;
            this.lstEmployee.View = System.Windows.Forms.View.Details;
            this.lstEmployee.SelectedIndexChanged += new System.EventHandler(this.lstEmployee_SelectedIndexChanged);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Name Employee";
            this.columnHeader10.Width = 315;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Subtotal";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader13.Width = 189;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(848, 23);
            this.panel7.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Date:";
            // 
            // tbpCashInOrCashOut
            // 
            this.tbpCashInOrCashOut.Controls.Add(this.lstCashOut);
            this.tbpCashInOrCashOut.Controls.Add(this.panel16);
            this.tbpCashInOrCashOut.Controls.Add(this.lstHistoryShift);
            this.tbpCashInOrCashOut.Location = new System.Drawing.Point(4, 33);
            this.tbpCashInOrCashOut.Name = "tbpCashInOrCashOut";
            this.tbpCashInOrCashOut.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCashInOrCashOut.Size = new System.Drawing.Size(854, 443);
            this.tbpCashInOrCashOut.TabIndex = 4;
            this.tbpCashInOrCashOut.Text = "Cash In / Cash Out";
            this.tbpCashInOrCashOut.UseVisualStyleBackColor = true;
            // 
            // lstCashOut
            // 
            this.lstCashOut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader17,
            this.columnHeader15,
            this.columnHeader16});
            this.lstCashOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCashOut.FullRowSelect = true;
            this.lstCashOut.GridLines = true;
            this.lstCashOut.Location = new System.Drawing.Point(3, 189);
            this.lstCashOut.MultiSelect = false;
            this.lstCashOut.Name = "lstCashOut";
            this.lstCashOut.Size = new System.Drawing.Size(848, 251);
            this.lstCashOut.TabIndex = 46;
            this.lstCashOut.UseCompatibleStateImageBehavior = false;
            this.lstCashOut.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Cash";
            this.columnHeader9.Width = 0;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Date";
            this.columnHeader17.Width = 217;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Total";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader15.Width = 141;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Description";
            this.columnHeader16.Width = 199;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.lblSafe);
            this.panel16.Controls.Add(this.lblSubtotal);
            this.panel16.Controls.Add(this.lblCashIn);
            this.panel16.Controls.Add(this.lblCashOut);
            this.panel16.Controls.Add(this.label18);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(3, 160);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(848, 29);
            this.panel16.TabIndex = 50;
            // 
            // lblSafe
            // 
            this.lblSafe.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSafe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSafe.Location = new System.Drawing.Point(444, 0);
            this.lblSafe.Name = "lblSafe";
            this.lblSafe.Size = new System.Drawing.Size(169, 29);
            this.lblSafe.TabIndex = 49;
            this.lblSafe.Text = "0.00";
            this.lblSafe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSafe.Visible = false;
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(291, 0);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(153, 29);
            this.lblSubtotal.TabIndex = 49;
            this.lblSubtotal.Text = "0.00";
            this.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSubtotal.Visible = false;
            // 
            // lblCashIn
            // 
            this.lblCashIn.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCashIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashIn.Location = new System.Drawing.Point(166, 0);
            this.lblCashIn.Name = "lblCashIn";
            this.lblCashIn.Size = new System.Drawing.Size(125, 29);
            this.lblCashIn.TabIndex = 49;
            this.lblCashIn.Text = "0.00";
            this.lblCashIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCashIn.Visible = false;
            // 
            // lblCashOut
            // 
            this.lblCashOut.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashOut.Location = new System.Drawing.Point(59, 0);
            this.lblCashOut.Name = "lblCashOut";
            this.lblCashOut.Size = new System.Drawing.Size(107, 29);
            this.lblCashOut.TabIndex = 49;
            this.lblCashOut.Text = "0.00";
            this.lblCashOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCashOut.Visible = false;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 29);
            this.label18.TabIndex = 48;
            this.label18.Text = "Total :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label18.Visible = false;
            // 
            // lstHistoryShift
            // 
            this.lstHistoryShift.AutoArrange = false;
            this.lstHistoryShift.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader27,
            this.columnHeader26});
            this.lstHistoryShift.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstHistoryShift.FullRowSelect = true;
            this.lstHistoryShift.GridLines = true;
            this.lstHistoryShift.Location = new System.Drawing.Point(3, 3);
            this.lstHistoryShift.MultiSelect = false;
            this.lstHistoryShift.Name = "lstHistoryShift";
            this.lstHistoryShift.Size = new System.Drawing.Size(848, 157);
            this.lstHistoryShift.TabIndex = 47;
            this.lstHistoryShift.UseCompatibleStateImageBehavior = false;
            this.lstHistoryShift.View = System.Windows.Forms.View.Details;
            this.lstHistoryShift.SelectedIndexChanged += new System.EventHandler(this.lstHistoryShift_SelectedIndexChanged);
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Shift ID";
            this.columnHeader22.Width = 83;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Float In";
            this.columnHeader23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader23.Width = 78;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Cash Float";
            this.columnHeader24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader24.Width = 105;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Cash Drop";
            this.columnHeader25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader25.Width = 108;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Float Out";
            this.columnHeader27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader27.Width = 107;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Balance Count";
            this.columnHeader26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader26.Width = 0;
            // 
            // tbpDebitAccount
            // 
            this.tbpDebitAccount.Controls.Add(this.lstDebitAccount);
            this.tbpDebitAccount.Controls.Add(this.panel10);
            this.tbpDebitAccount.Location = new System.Drawing.Point(4, 33);
            this.tbpDebitAccount.Name = "tbpDebitAccount";
            this.tbpDebitAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDebitAccount.Size = new System.Drawing.Size(854, 443);
            this.tbpDebitAccount.TabIndex = 5;
            this.tbpDebitAccount.Text = "Debit Account";
            this.tbpDebitAccount.UseVisualStyleBackColor = true;
            // 
            // lstDebitAccount
            // 
            this.lstDebitAccount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader14,
            this.columnHeader18});
            this.lstDebitAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDebitAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDebitAccount.FullRowSelect = true;
            this.lstDebitAccount.GridLines = true;
            this.lstDebitAccount.HideSelection = false;
            this.lstDebitAccount.Location = new System.Drawing.Point(3, 3);
            this.lstDebitAccount.MultiSelect = false;
            this.lstDebitAccount.Name = "lstDebitAccount";
            this.lstDebitAccount.Size = new System.Drawing.Size(848, 381);
            this.lstDebitAccount.TabIndex = 10;
            this.lstDebitAccount.UseCompatibleStateImageBehavior = false;
            this.lstDebitAccount.View = System.Windows.Forms.View.Details;
            this.lstDebitAccount.SelectedIndexChanged += new System.EventHandler(this.lstDebitAccount_SelectedIndexChanged);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Member No";
            this.columnHeader11.Width = 136;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Account Name";
            this.columnHeader12.Width = 207;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Account Limit";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader14.Width = 190;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Subtotal";
            this.columnHeader18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader18.Width = 148;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.lblTotal3);
            this.panel10.Controls.Add(this.button16);
            this.panel10.Controls.Add(this.button19);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(3, 384);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(848, 56);
            this.panel10.TabIndex = 49;
            // 
            // lblTotal3
            // 
            this.lblTotal3.AutoSize = true;
            this.lblTotal3.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblTotal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal3.Location = new System.Drawing.Point(799, 0);
            this.lblTotal3.Name = "lblTotal3";
            this.lblTotal3.Size = new System.Drawing.Size(49, 20);
            this.lblTotal3.TabIndex = 49;
            this.lblTotal3.Text = "Total";
            // 
            // button16
            // 
            this.button16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(4, 4);
            this.button16.Margin = new System.Windows.Forms.Padding(1);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(122, 49);
            this.button16.TabIndex = 47;
            this.button16.Text = "View Invoice";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button19
            // 
            this.button19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(130, 4);
            this.button19.Margin = new System.Windows.Forms.Padding(1);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(122, 49);
            this.button19.TabIndex = 48;
            this.button19.Text = "View Pay";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Visible = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // tbpCreditAccount
            // 
            this.tbpCreditAccount.Controls.Add(this.lstCreditAccount);
            this.tbpCreditAccount.Controls.Add(this.panel11);
            this.tbpCreditAccount.Location = new System.Drawing.Point(4, 33);
            this.tbpCreditAccount.Name = "tbpCreditAccount";
            this.tbpCreditAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCreditAccount.Size = new System.Drawing.Size(854, 443);
            this.tbpCreditAccount.TabIndex = 6;
            this.tbpCreditAccount.Text = "Credit Account";
            this.tbpCreditAccount.UseVisualStyleBackColor = true;
            // 
            // lstCreditAccount
            // 
            this.lstCreditAccount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21});
            this.lstCreditAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCreditAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCreditAccount.FullRowSelect = true;
            this.lstCreditAccount.GridLines = true;
            this.lstCreditAccount.HideSelection = false;
            this.lstCreditAccount.Location = new System.Drawing.Point(3, 3);
            this.lstCreditAccount.MultiSelect = false;
            this.lstCreditAccount.Name = "lstCreditAccount";
            this.lstCreditAccount.Size = new System.Drawing.Size(848, 381);
            this.lstCreditAccount.TabIndex = 45;
            this.lstCreditAccount.UseCompatibleStateImageBehavior = false;
            this.lstCreditAccount.View = System.Windows.Forms.View.Details;
            this.lstCreditAccount.SelectedIndexChanged += new System.EventHandler(this.lstCreditAccount_SelectedIndexChanged);
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Member No";
            this.columnHeader19.Width = 136;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Account Name";
            this.columnHeader20.Width = 207;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Subtotal";
            this.columnHeader21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader21.Width = 179;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label14);
            this.panel11.Controls.Add(this.button22);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(3, 384);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(848, 56);
            this.panel11.TabIndex = 44;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Right;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(799, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 20);
            this.label14.TabIndex = 43;
            this.label14.Text = "Total";
            // 
            // button22
            // 
            this.button22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.Location = new System.Drawing.Point(3, 3);
            this.button22.Margin = new System.Windows.Forms.Padding(1);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(122, 49);
            this.button22.TabIndex = 41;
            this.button22.Text = "View detail";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // tbpDriverOff
            // 
            this.tbpDriverOff.Controls.Add(this.panel12);
            this.tbpDriverOff.Controls.Add(this.panel17);
            this.tbpDriverOff.Location = new System.Drawing.Point(4, 33);
            this.tbpDriverOff.Name = "tbpDriverOff";
            this.tbpDriverOff.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDriverOff.Size = new System.Drawing.Size(854, 443);
            this.tbpDriverOff.TabIndex = 7;
            this.tbpDriverOff.Text = "Driver Off";
            this.tbpDriverOff.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.listViewdriveroff);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(848, 370);
            this.panel12.TabIndex = 0;
            // 
            // listViewdriveroff
            // 
            this.listViewdriveroff.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31});
            this.listViewdriveroff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewdriveroff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewdriveroff.FullRowSelect = true;
            this.listViewdriveroff.GridLines = true;
            this.listViewdriveroff.HideSelection = false;
            this.listViewdriveroff.Location = new System.Drawing.Point(0, 0);
            this.listViewdriveroff.MultiSelect = false;
            this.listViewdriveroff.Name = "listViewdriveroff";
            this.listViewdriveroff.Size = new System.Drawing.Size(848, 370);
            this.listViewdriveroff.TabIndex = 46;
            this.listViewdriveroff.UseCompatibleStateImageBehavior = false;
            this.listViewdriveroff.View = System.Windows.Forms.View.Details;
            this.listViewdriveroff.SelectedIndexChanged += new System.EventHandler(this.listViewdriveroff_SelectedIndexChanged);
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "OrderID";
            this.columnHeader28.Width = 150;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Subtotal";
            this.columnHeader29.Width = 150;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Shift";
            this.columnHeader30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader30.Width = 150;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Staff";
            this.columnHeader31.Width = 150;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.button21);
            this.panel17.Controls.Add(this.button24);
            this.panel17.Controls.Add(this.button23);
            this.panel17.Controls.Add(this.lbtotaldriveroff);
            this.panel17.Controls.Add(this.button20);
            this.panel17.Controls.Add(this.button17);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(3, 373);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(848, 67);
            this.panel17.TabIndex = 46;
            // 
            // button21
            // 
            this.button21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Location = new System.Drawing.Point(87, 8);
            this.button21.Margin = new System.Windows.Forms.Padding(1);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(80, 50);
            this.button21.TabIndex = 38;
            this.button21.Text = "<";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button24
            // 
            this.button24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(333, 8);
            this.button24.Margin = new System.Windows.Forms.Padding(1);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(115, 50);
            this.button24.TabIndex = 45;
            this.button24.Text = "View Detail";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.Location = new System.Drawing.Point(169, 8);
            this.button23.Margin = new System.Windows.Forms.Padding(1);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(80, 50);
            this.button23.TabIndex = 39;
            this.button23.Text = ">";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // lbtotaldriveroff
            // 
            this.lbtotaldriveroff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbtotaldriveroff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotaldriveroff.Location = new System.Drawing.Point(451, 9);
            this.lbtotaldriveroff.Name = "lbtotaldriveroff";
            this.lbtotaldriveroff.Size = new System.Drawing.Size(286, 48);
            this.lbtotaldriveroff.TabIndex = 44;
            this.lbtotaldriveroff.Text = "Total";
            this.lbtotaldriveroff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button20
            // 
            this.button20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(251, 8);
            this.button20.Margin = new System.Windows.Forms.Padding(1);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(80, 50);
            this.button20.TabIndex = 40;
            this.button20.Text = ">>";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button17
            // 
            this.button17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(5, 8);
            this.button17.Margin = new System.Windows.Forms.Padding(1);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(80, 50);
            this.button17.TabIndex = 37;
            this.button17.Text = "<<";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // tbpCardReject
            // 
            this.tbpCardReject.Controls.Add(this.listViewCardReject);
            this.tbpCardReject.Location = new System.Drawing.Point(4, 33);
            this.tbpCardReject.Name = "tbpCardReject";
            this.tbpCardReject.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCardReject.Size = new System.Drawing.Size(854, 443);
            this.tbpCardReject.TabIndex = 8;
            this.tbpCardReject.Text = "Card Reject";
            this.tbpCardReject.UseVisualStyleBackColor = true;
            this.tbpCardReject.Click += new System.EventHandler(this.tabPage9_Click);
            // 
            // listViewCardReject
            // 
            this.listViewCardReject.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40});
            this.listViewCardReject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewCardReject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewCardReject.FullRowSelect = true;
            this.listViewCardReject.GridLines = true;
            this.listViewCardReject.HideSelection = false;
            this.listViewCardReject.Location = new System.Drawing.Point(3, 3);
            this.listViewCardReject.MultiSelect = false;
            this.listViewCardReject.Name = "listViewCardReject";
            this.listViewCardReject.Size = new System.Drawing.Size(848, 437);
            this.listViewCardReject.TabIndex = 47;
            this.listViewCardReject.UseCompatibleStateImageBehavior = false;
            this.listViewCardReject.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Auth Code";
            this.columnHeader35.Width = 210;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Account Type";
            this.columnHeader36.Width = 212;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Pan";
            this.columnHeader37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader37.Width = 118;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Card Type";
            this.columnHeader38.Width = 104;
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Amt Purchase";
            this.columnHeader39.Width = 119;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Amt Cash";
            this.columnHeader40.Width = 128;
            // 
            // tbpStockTake
            // 
            this.tbpStockTake.Controls.Add(this.ucListStockTake2);
            this.tbpStockTake.Location = new System.Drawing.Point(4, 33);
            this.tbpStockTake.Name = "tbpStockTake";
            this.tbpStockTake.Padding = new System.Windows.Forms.Padding(3);
            this.tbpStockTake.Size = new System.Drawing.Size(854, 443);
            this.tbpStockTake.TabIndex = 11;
            this.tbpStockTake.Text = "Stock Take";
            this.tbpStockTake.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(349, 24);
            this.label20.TabIndex = 2;
            this.label20.Text = "Delivery List----------------------------------------";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(351, 24);
            this.label21.TabIndex = 3;
            this.label21.Text = "Pickup List------------------------------------------";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.tcSalesPerStaff);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 48);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(862, 480);
            this.panel13.TabIndex = 12;
            // 
            // ucListDelivery
            // 
            this.ucListDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucListDelivery.Location = new System.Drawing.Point(3, 3);
            this.ucListDelivery.Margin = new System.Windows.Forms.Padding(6);
            this.ucListDelivery.Name = "ucListDelivery";
            this.ucListDelivery.Size = new System.Drawing.Size(848, 437);
            this.ucListDelivery.TabIndex = 0;
            // 
            // ucListPickup
            // 
            this.ucListPickup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucListPickup.Location = new System.Drawing.Point(3, 3);
            this.ucListPickup.Margin = new System.Windows.Forms.Padding(11);
            this.ucListPickup.Name = "ucListPickup";
            this.ucListPickup.Size = new System.Drawing.Size(848, 437);
            this.ucListPickup.TabIndex = 1;
            // 
            // ucListStockTake2
            // 
            this.ucListStockTake2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucListStockTake2.Location = new System.Drawing.Point(3, 3);
            this.ucListStockTake2.Margin = new System.Windows.Forms.Padding(6);
            this.ucListStockTake2.Name = "ucListStockTake2";
            this.ucListStockTake2.Size = new System.Drawing.Size(848, 437);
            this.ucListStockTake2.TabIndex = 0;
            // 
            // UCReportDaily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.button1);
            this.Name = "UCReportDaily";
            this.Size = new System.Drawing.Size(862, 583);
            this.Load += new System.EventHandler(this.UCReportDaily_Load);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tcSalesPerStaff.ResumeLayout(false);
            this.tbpDailyReport.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tbpQuantity.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tbpGroupQuantity.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tbpDelivery.ResumeLayout(false);
            this.tbpListPickup.ResumeLayout(false);
            this.tbpSalesPerStaff.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tbpCashInOrCashOut.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.tbpDebitAccount.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tbpCreditAccount.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tbpDriverOff.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.tbpCardReject.ResumeLayout(false);
            this.tbpStockTake.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelTotalSale;
        private System.Windows.Forms.Label labelTotalDiscount;
        private System.Windows.Forms.Label labelTotalGST;
        private System.Windows.Forms.Label labelCard;
        private System.Windows.Forms.Label labelCash;
        private System.Windows.Forms.Label labelRefunc;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dateTimePickerfrom;
        private System.Windows.Forms.TabControl tcSalesPerStaff;
        private System.Windows.Forms.TabPage tbpDailyReport;
        private System.Windows.Forms.Label labelSumTotal;
        private System.Windows.Forms.Label labelCardTotal;
        private System.Windows.Forms.Label labelCashTotal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tbpQuantity;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblTotal1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ListView lstQuantityReportFuelSales;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tbpGroupQuantity;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView lstGroupItemFuelSales;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblTotal2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tbpSalesPerStaff;
        private System.Windows.Forms.ListView lstEmployee;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lstCard;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label lblTotalCard;
        private System.Windows.Forms.Label labelCreditNote;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelCashIn;
        private System.Windows.Forms.Label labelCashOut;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tbpCashInOrCashOut;
        private System.Windows.Forms.ListView lstCashOut;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelTotalAccount;
        private System.Windows.Forms.Label labelCashAccount;
        private System.Windows.Forms.Label labelCardAccount;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tbpDebitAccount;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.ListView lstDebitAccount;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.Label lblTotal3;
        private System.Windows.Forms.TabPage tbpCreditAccount;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.ListView lstCreditAccount;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ListView lstHistoryShift;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.Label lblSafe;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label lblCashIn;
        private System.Windows.Forms.Label lblCashOut;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.TabPage tbpDriverOff;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ListView listViewdriveroff;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label lbtotaldriveroff;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSumTotal;
        private System.Windows.Forms.Label lblCardTotal;
        private System.Windows.Forms.Label lblCashTotal;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.TabPage tbpCardReject;
        public System.Windows.Forms.ListView listViewCardReject;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.ListView lstCardCashOut;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        public System.Windows.Forms.Label labelBankEftposCashOut;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ListView lstQuantityReportShopSales;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ListView lstGroupItemShopSales;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.Label lblSurcharge;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.Label lblTotalSurcharge;
        private System.Windows.Forms.Label labelPayOut;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tbpDelivery;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tbpListPickup;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblFeeDelivery;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tbpStockTake;
        private ucReport.ucListStockTake ucListStockTake1;
        private ucReport.ucListStockTake ucListStockTake2;
        private UCComponentReport.ucListDelivery ucListDelivery;
        private UCComponentReport.ucListDelivery ucListPickup;
        private System.Windows.Forms.Label labelSafeDrop;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
    }
}
