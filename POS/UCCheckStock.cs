﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS
{
    public partial class UCCheckStock : UserControl
    {
        private Connection.Connection mConnection;
        private Connection.ReadDBConfig dbConfig = new Connection.ReadDBConfig();

        public UCCheckStock()
        {
            InitializeComponent();
        }

        private void UCCheckStock_Load(object sender, EventArgs e)
        {
            mConnection = new Connection.Connection();
            ucItem1.ItemClick += new UCItem.MyEventHandler(ucItem1_ItemClick);
        }

        private int LoadQty(string itemID)
        {
            int qty = 0;
            int qtySale = 0;
            int qtyReceived = 0;
            int qtyOnhand = 0;
            //qty ordersdaily
            object obj = mConnection.ExecuteScalar("select sum(qty) from ordersdailyline where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtySale += Convert.ToInt16(obj);
            }
            //qty ordersall
            obj = mConnection.ExecuteScalar("select sum(qty) from ordersallline where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtySale += Convert.ToInt16(obj);
            }
            //qtyreCeived
            obj = mConnection.ExecuteScalar("select sum(qty) from recvstocklines where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtyReceived += Convert.ToInt16(obj);
            }
            obj = mConnection.ExecuteScalar("select sum(unitsOnHand) from products where itemID=" + itemID);
            if (obj != DBNull.Value)
            {
                qtyOnhand += Convert.ToInt16(obj);
            }
            qty += (qtyReceived + qtyOnhand - qtySale);
            return qty;
        }

        private Item GetItemQty(string itemID)
        {
            Item item = new Item(Convert.ToInt16(itemID), 1);
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                int bulkID = Convert.ToInt16(mConnection.ExecuteScalar("select if(bulkID is null,0,bulkID) from bulkmenulines where itemID=" + itemID + " limit 0,1"));
                if (bulkID > 0)
                {
                    //========================
                    DataTable tbl = mConnection.Select("select itemID,(select i.sellSize from itemsmenu i where i.itemID=bl.itemID) as size from bulkmenulines bl where bulkID=" + bulkID);
                    foreach (DataRow row in tbl.Rows)
                    {
                        item.Qty += LoadQty(row["itemID"].ToString()) * Convert.ToInt16(row["size"]);
                    }
                    object obj = mConnection.ExecuteScalar("select unitsOnHand from products where bulkID=" + bulkID);
                    if (obj != null)
                    {
                        item.Qty += Convert.ToInt16(obj);
                    }
                    item.Size = Convert.ToInt16(mConnection.ExecuteScalar("select i.sellSize from itemsmenu i where i.itemID=" + itemID));
                    item.BulkID = bulkID;
                }
                else
                {
                    item.Qty += LoadQty(itemID);
                }
                mConnection.Commit();
            }
            catch (Exception)
            {
                item = new Item(Convert.ToInt16(itemID), 1);
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
            return item;
        }

        private int CheckStock(string itemID)
        {
            int qty = 0;
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                int bulkID = Convert.ToInt16(mConnection.ExecuteScalar("select if(bulkID is null,0,bulkID) from bulkmenulines where itemID=" + itemID + " limit 0,1"));
                if (bulkID > 0)
                {
                    //========================
                    DataTable tbl = mConnection.Select("select itemID,(select i.sellSize from itemsmenu i where i.itemID=bl.itemID) as size from bulkmenulines bl where bulkID=" + bulkID);
                    foreach (DataRow row in tbl.Rows)
                    {
                        qty += LoadQty(row["itemID"].ToString()) * Convert.ToInt16(row["size"]);
                    }
                    object obj = mConnection.ExecuteScalar("select unitsOnHand from products where bulkID=" + bulkID);
                    if (obj != null)
                    {
                        qty += Convert.ToInt16(obj);
                    }
                    int size = Convert.ToInt16(mConnection.ExecuteScalar("select i.sellSize from itemsmenu i where i.itemID=" + itemID));
                    if (size > 0)
                    {
                        qty = qty / size;
                    }
                }
                else
                {
                    qty += LoadQty(itemID);
                }
                mConnection.Commit();
            }
            catch (Exception)
            {
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
            return qty;
        }

        private void ucItem1_ItemClick(UCItem.Item item)
        {
            textBoxPOS_OSK_ItemID.Text = item.ItemID + "";
            textBoxPOS_OSK_ItemName.Text = item.ItemName;
            textBoxPOS_OSK_Quantity.Text = CheckStock(item.ItemID) + "";
        }

        public void CheckStock(string itemName, string itemID)
        {
            textBoxPOS_OSK_ItemID.Text = itemID;
            textBoxPOS_OSK_ItemName.Text = itemName;
            textBoxPOS_OSK_Quantity.Text = CheckStock(itemID) + "";
        }

        public void InitMenu()
        {
            ucItem1.initMenuButton();
        }

        private class Bulk
        {
            public int BulkID { get; set; }

            public int Size { get; set; }

            public Bulk(int bulk, int size)
            {
                BulkID = bulk;
                Size = size;
            }
        }

        public class Item
        {
            public int ItemID { get; set; }

            public int Size { get; set; }

            public int Qty { get; set; }

            public int BulkID { get; set; }

            public Item(int itemID, int size)
            {
                ItemID = itemID;
                Size = size;
            }
        }

        private void buttonStockTake_Click(object sender, EventArgs e)
        {
            int areaID = 0;
            //int qty = CheckStock(textBoxPOS_OSK_ItemID.Text);
            Item item = GetItemQty(textBoxPOS_OSK_ItemID.Text);
            try
            {
                mConnection.Open();
                mConnection.BeginTransaction();
                string itemID = textBoxPOS_OSK_ItemID.Text;
                int qtyCurent = Convert.ToInt16(textBoxPOS_OSK_Quantity.Text);
                string sql = "select eventID from products where itemID=" + itemID;
                if (item.BulkID != 0)
                {
                    sql = "select eventID from products where bulkID=" + item.BulkID;
                }
                object obj = mConnection.ExecuteScalar(sql);
                if (obj == DBNull.Value || obj == null)
                {
                    sql = "insert into products(itemID) values(" + itemID + ")";
                    if (item.BulkID != 0)
                    {
                        sql = "insert into products(bulkID) values(" + item.BulkID + ")";
                    }
                    mConnection.ExecuteNonQuery(sql);
                    obj = mConnection.ExecuteScalar("select max(eventID) from products");
                }
                mConnection.ExecuteNonQuery("update products set unitsOnHand=" + (qtyCurent * item.Size - item.Qty) + "+unitsOnHand where eventID=" + obj.ToString());

                mConnection.Commit();
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCCheckStock.buttonStockTake_Click:::" + ex.Message);
                mConnection.Rollback();
            }
            finally
            {
                mConnection.Close();
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
        }
    }
}