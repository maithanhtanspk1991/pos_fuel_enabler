﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCCardButton : UserControl
    {
        public UCkeypad POSKeyPad { get; set; }

        public Double dblCashFromButton { get; set; }

        public UCCardButton()
        {
            InitializeComponent();
        }

        private bool blnCheck = false;
        public static double dblAmount = 0;

        private void btnCard_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("UCCardButton.btnCard_Click.");
            try
            {
                if (blnCheck == true)
                {
                    blnCheck = false;
                    txtTotalAmount.Text = "0";
                    //txtTotalAmount.Enabled = false;
                }
                else
                {
                    blnCheck = true;
                    //txtTotalAmount.Enabled = true;

                    txtTotalAmount.Text = dblAmount.ToString();
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCCardButton:::btnCard_Click:::" + ex.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void txtQuantity_Enter(object sender, EventArgs e)
        {
            if (POSKeyPad != null)
            {
                TextBox txt = (TextBox)sender;
                POSKeyPad.txtResult = txt;
                txt.Tag = txt.BackColor;
                txt.BackColor = Color.White;
            }
        }

        private void txtQuantity_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = Color.FromArgb(255, 255, 128);
        }

        private void txtQuantity_Click(object sender, EventArgs e)
        {
            if (POSKeyPad != null)
            {
                TextBox txt = (TextBox)sender;
                POSKeyPad.txtResult = txt;
                txt.Tag = txt.BackColor;
                txt.BackColor = Color.White;
            }
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
        }
    }
}