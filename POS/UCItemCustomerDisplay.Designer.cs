﻿namespace POS
{
    partial class UCItemCustomerDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSubtotal = new System.Windows.Forms.Label();
            this.lbQty = new System.Windows.Forms.Label();
            this.lbItemName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbSubtotal
            // 
            this.lbSubtotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSubtotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(183)))), ((int)(((byte)(186)))));
            this.lbSubtotal.Location = new System.Drawing.Point(402, 0);
            this.lbSubtotal.Name = "lbSubtotal";
            this.lbSubtotal.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbSubtotal.Size = new System.Drawing.Size(100, 35);
            this.lbSubtotal.TabIndex = 0;
            this.lbSubtotal.Text = "0.00";
            this.lbSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbQty
            // 
            this.lbQty.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(183)))), ((int)(((byte)(186)))));
            this.lbQty.Location = new System.Drawing.Point(302, 0);
            this.lbQty.Name = "lbQty";
            this.lbQty.Size = new System.Drawing.Size(100, 35);
            this.lbQty.TabIndex = 1;
            this.lbQty.Text = "1";
            this.lbQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbItemName
            // 
            this.lbItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbItemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(183)))), ((int)(((byte)(186)))));
            this.lbItemName.Location = new System.Drawing.Point(0, 0);
            this.lbItemName.Name = "lbItemName";
            this.lbItemName.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lbItemName.Size = new System.Drawing.Size(302, 35);
            this.lbItemName.TabIndex = 2;
            this.lbItemName.Text = "Xăng";
            this.lbItemName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UCItemCustomerDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lbItemName);
            this.Controls.Add(this.lbQty);
            this.Controls.Add(this.lbSubtotal);
            this.Name = "UCItemCustomerDisplay";
            this.Size = new System.Drawing.Size(502, 35);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbSubtotal;
        private System.Windows.Forms.Label lbQty;
        private System.Windows.Forms.Label lbItemName;
    }
}
