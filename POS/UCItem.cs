﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCItem : UserControl
    {
        public delegate void MyEventHandler(Item item);

        public event MyEventHandler ItemClick;

        private Connection.Connection conn;
        private static Color[] listColor = { Color.Red, Color.Orange, Color.HotPink, Color.Green, Color.Blue, Color.Purple, Color.Gray };
        private int mIndex = 0;
        private int zIndex = 0;
        private int mGroupIndex = 0;
        private int mGroupShortCut = 0;
        private Group mGroup;
        private Item mItem;
        private ItemLine mLine;
        private Item mResuilt;

        public UCItem()
        {
            InitializeComponent();
        }

        private void OnItemClick(Item item)
        {
            if (ItemClick != null)
            {
                ItemClick(item);
            }
        }

        private void uCItem_Load(object sender, EventArgs e)
        {
            conn = new Connection.Connection();
            mGroup = new Group();
            mItem = new Item();
            mLine = new ItemLine();
            //initMenuButton();
        }

        public void initMenuButton()
        {
            for (int i = 0; i < 7; i++)
            {
                int bgcolor = i;
                Button btn = new Button();
                btn.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
                btn.ForeColor = Color.LightGray;
                btn.Width = 200;
                btn.Height = 200;
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                btn.BackColor = listColor[i];
                tlp_groups.Controls.Add(btn);
                btn.Click += new EventHandler(btn_Click);
            }

            for (int i = 0; i < 21; i++)
            {
                Button btni = new Button();
                btni.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
                btni.Width = 200;
                btni.Height = 200;
                btni.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tlp_items.Controls.Add(btni);
                btni.Click += new EventHandler(btni_Click);
                btni.Enabled = false;
            }
            mIndex = 0;
            LoadGroup();

            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void btni_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                mResuilt = new Item();
                mResuilt.ItemID = btn.Tag.ToString();
                mResuilt.ItemName = btn.Text;
                OnItemClick(mResuilt);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmItems.btni_Click:::" + ex.Message);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Tag != null)
            {
                zIndex = 0;
                mGroupIndex = tlp_groups.Controls.GetChildIndex(btn);
                mGroup.GroupID = btn.Tag.ToString();
                mGroup.GroupName = btn.Text;
                mItem = new Item();
                LoadItem(mGroup.GroupID);
                //SetTextStatus("");

                //LoadDetailGroup(mGroup.GroupID);
                //flagdelete = 1;
                //flagupdate = 1;
                //btnsubmitupdate.Enabled = true;
                //btnsave.Enabled = false;
            }
        }

        private void LoadGroup()
        {
            try
            {
                if (mIndex < 0)
                {
                    mIndex = 0;
                }
                DataTable dt = conn.Select("SELECT groupID,groupDesc FROM groupsmenu where 0=" + mGroupShortCut + " or grShortCut=" + mGroupShortCut + " order by displayOrder limit " + mIndex + ",7");
                if (dt.Rows.Count <= 0)
                {
                    mIndex -= 7;
                    return;
                }

                mGroup.GroupID = dt.Rows[0]["groupID"].ToString();
                mGroup.GroupName = dt.Rows[0]["groupDesc"].ToString();
                mItem = new Item();
                //SetTextStatus("");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_groups.Controls[i].Tag = dr["groupID"].ToString();
                    tlp_groups.Controls[i].Text = dr["groupDesc"].ToString();
                    tlp_groups.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 7; i++)
                {
                    tlp_groups.Controls[i].Text = "";
                    tlp_groups.Controls[i].Tag = null;
                    tlp_groups.Controls[i].Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmItems.LoadGroup:::" + ex.Message);
            }
        }

        private void LoadItem(string groupID)
        {
            try
            {
                if (zIndex < 0)
                {
                    zIndex = 0;
                }
                DataTable dt = conn.Select("select i.itemID,i.itemDesc from itemsmenu i where groupID=" + groupID + " limit " + zIndex + ",21");
                if (dt.Rows.Count <= 0 && zIndex > 0)
                {
                    zIndex -= 21;
                    return;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    tlp_items.Controls[i].Text = dr["itemDesc"].ToString();
                    tlp_items.Controls[i].Tag = dr["itemID"].ToString();
                    Item item = new Item();

                    tlp_items.Controls[i].Name = "Item";
                    tlp_items.Controls[i].BackColor = listColor[mGroupIndex];
                    tlp_items.Controls[i].Enabled = true;
                }
                for (int i = dt.Rows.Count; i < 21; i++)
                {
                    tlp_items.Controls[i].Text = "";
                    tlp_items.Controls[i].Tag = null;
                    tlp_items.Controls[i].BackColor = Color.White;
                    tlp_items.Controls[i].Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("frmItems.LoadItem:::" + ex.Message);
            }
        }

        public class Group
        {
            public string GroupID { get; set; }

            public string GroupName { get; set; }
        }

        public class Item
        {
            public string ItemID { get; set; }

            public string ItemName { get; set; }
        }

        public class ItemLine
        {
            public string lineID { get; set; }

            public string lineName { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mIndex -= 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mIndex += 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            zIndex -= 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            zIndex += 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }

        public Item GetItem()
        {
            return mResuilt;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.OK;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            mIndex -= 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            mIndex += 7;
            LoadGroup();
            if (tlp_groups.Controls.Count > 0)
            {
                zIndex = 0;
                mGroupIndex = 0;
                LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            zIndex += 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            zIndex -= 21;
            LoadItem(tlp_groups.Controls[mGroupIndex].Tag.ToString());
        }
    }
}