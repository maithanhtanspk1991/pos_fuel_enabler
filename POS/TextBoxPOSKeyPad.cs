﻿using System;
using POS.Forms;

namespace POS
{
    public partial class TextBoxPOSKeyPad : System.Windows.Forms.TextBox
    {
        public bool IsLockDot { get; set; }

        public TextBoxPOSKeyPad()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
        }

        protected override void OnEnter(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.White;
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            Forms.frmKeyPadNew frm = new Forms.frmKeyPadNew(this, IsLockDot);
            frmCashInOrOut.iIsRefesh = 0;
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmCashInOrOut.iIsRefesh = 1;
                base.OnClick(e);
            }
        }
    }
}