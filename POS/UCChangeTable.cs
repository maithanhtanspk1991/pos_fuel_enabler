﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS
{
    public partial class UCChangeTable : UserControl
    {
        private Connection.Connection conn = new Connection.Connection();
        private Class.Functions fun = new Class.Functions();

        public UCChangeTable()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class.LogPOS.WriteLog("UCChangeTable.buttonOK_Click.");
            try
            {
                if (fun.GetCompletedTable(txtfrom.Text) == 1)
                {
                    Forms.frmMessageBoxOK frm = new Forms.frmMessageBoxOK("Warning", "Table from must be used");
                    frm.ShowDialog();
                    //MessageBox.Show("Table from must be used");
                }
                else if (fun.GetCompletedTable(txtto.Text) != 1 || Convert.ToInt32(txtto.Text) > 60)
                {
                    Forms.frmMessageBoxOK frm = new Forms.frmMessageBoxOK("Warning", "Table to must be unused");
                    frm.ShowDialog();
                    //MessageBox.Show("Table from must be unused");
                }
                else
                {
                    SwapTable(txtfrom.Text, txtto.Text);
                    this.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCChangeTable.buttonOK_Click:::" + ex.Message);
            }
        }

        private void SwapTable(string fromid, string toid)
        {
            try
            {
                conn.Open();
                string orderid = conn.ExecuteScalar("select orderID from ordersdaily where tableID=" + fromid + " and completed<>1").ToString();
                conn.ExecuteNonQuery("update ordersdaily set tableID=" + toid + " where orderID=" + orderid);
            }
            catch (Exception ex)
            {
                Class.LogPOS.WriteLog("UCChangeTable.SwapTable:::" + ex.Message);
            }
        }

        private void txtfrom_Enter(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            uCkeypad1.txtResult = txt;
            txt.Tag = txt.BackColor;
            txt.BackColor = Color.White;
        }

        private void txtfrom_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.BackColor = (Color)txt.Tag;
        }

        private void UCChangeTable_Load(object sender, EventArgs e)
        {
            txtfrom.Focus();
        }
    }
}