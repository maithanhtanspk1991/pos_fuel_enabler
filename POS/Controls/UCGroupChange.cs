﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCGroupChange : UserControl
    {
        public DataObject.GroupMenu _GroupMenu { get; set; }

        public bool _DispalyOrderChange { get; set; }

        private int DispalyOrderOld = 0;

        public UCGroupChange()
        {
            InitializeComponent();
            _GroupMenu = new DataObject.GroupMenu();
            _DispalyOrderChange = false;
        }

        public void SetValue()
        {
            txtGroupShort.Text = _GroupMenu.GroupShort;
            txtGroupDesc.Text = _GroupMenu.GroupDesc;
            txtDispalyOrder.Text = _GroupMenu.ShowDisplay.ToString();
            cbVisual.Checked = _GroupMenu.Visual;
            DispalyOrderOld = _GroupMenu.ShowDisplay;
        }

        public void GetValue()
        {
            _GroupMenu.GroupShort = txtGroupShort.Text;
            _GroupMenu.GroupDesc = txtGroupDesc.Text;
            _GroupMenu.ShowDisplay = Convert.ToInt32(txtDispalyOrder.Text.ToString());
            _GroupMenu.Visual = cbVisual.Checked;
            if (_GroupMenu.ShowDisplay != DispalyOrderOld)
                _DispalyOrderChange = true;
        }

        public void Clear()
        {
            _GroupMenu = null;
            _GroupMenu = new DataObject.GroupMenu();
            _GroupMenu.Visual = true;
            txtGroupShort.Text = ""; ;
            txtGroupDesc.Text = ""; ;
            txtDispalyOrder.Text = "1";
            //cbVisual.Checked = true; ;
        }

        public bool CheckInput()
        {
            bool result = true;
            if (txtGroupShort.Text == "")
            {
                lbStatus.Text = "Item short is not empty";
                result = false;
            }
            if (txtGroupDesc.Text == "")
            {
                lbStatus.Text = "Item desc is not empty";
                result = false;
            }
            if (txtDispalyOrder.Text == "")
            {
                lbStatus.Text = "Display order is not empty";
                result = false;
            }
            return result;
        }

        private void UCGroupChange_Load(object sender, EventArgs e)
        {
        }

        private void txtGroupShort_TextChanged(object sender, EventArgs e)
        {
            if (txtGroupDesc.Text == "")
                txtGroupDesc.Text = txtGroupShort.Text;
        }
    }
}