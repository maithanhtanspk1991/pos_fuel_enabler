﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCMenu : UserControl
    {
        public int IndexColor = 0;

        /// <summary>
        /// Danh sách màu
        /// </summary>
        #region Color
        public static readonly System.Drawing.Color Menu_01 = System.Drawing.Color.FromArgb(230, 90, 120);
        public static readonly System.Drawing.Color Menu_02 = System.Drawing.Color.FromArgb(255, 120, 0);
        public static readonly System.Drawing.Color Menu_03 = System.Drawing.Color.FromArgb(255, 200, 0);
        public static readonly System.Drawing.Color Menu_04 = System.Drawing.Color.FromArgb(145, 195, 85);
        public static readonly System.Drawing.Color Menu_05 = System.Drawing.Color.FromArgb(0, 170, 130);
        public static readonly System.Drawing.Color Menu_06 = System.Drawing.Color.FromArgb(100, 170, 255);
        public static readonly System.Drawing.Color Menu_07 = System.Drawing.Color.FromArgb(100, 175, 190);
        public static readonly System.Drawing.Color Menu_08 = System.Drawing.Color.FromArgb(190, 120, 175);
        public static readonly System.Drawing.Color Menu_09 = System.Drawing.Color.FromArgb(240, 145, 140);
        public static readonly System.Drawing.Color Menu_10 = System.Drawing.Color.FromArgb(150, 220, 160);
        public static readonly System.Drawing.Color Menu_11 = System.Drawing.Color.FromArgb(165, 160, 255);
        public static readonly System.Drawing.Color Menu_12 = System.Drawing.Color.FromArgb(150, 220, 200);
        public static readonly System.Drawing.Color Menu_13 = System.Drawing.Color.FromArgb(200, 185, 160);
        public static System.Drawing.Color[] listColor = { Menu_01, Menu_02, Menu_03, Menu_04, Menu_05, Menu_06, Menu_07, Menu_08, Menu_09, Menu_10, Menu_11, Menu_12, Menu_13 };
        #endregion

        private int _ColumnGroup = 1;

        /// <summary>
        /// Giữ giá trị hiện thị số cột bên Items
        /// </summary>
        ///
        private int _ColumnItem = 2;

        private int _MenuGroupWidth = 100;

        private string _MenuImagesGroup = "";

        private string _MenuImagesItem = "";

        private string _MenuNoImages = "";

        private int _RowGroup = 7;

        private int _RowItem = 7;

        private bool checkConnectionServer = false;

        private DataObject.DeleteType delete = DataObject.DeleteType.None;

        private DataObject.IsFuelType isFuel = DataObject.IsFuelType.None;

        private bool loadMenu = true;

        private DataObject.VisualType visual = DataObject.VisualType.None;

        private DataObject.TypeMenu shortcut = DataObject.TypeMenu.ALLMENU;

        public DataObject.TypeMenu mShortcut
        {
            get { return shortcut; }
            set { shortcut = value; }
        }

        /// <summary>
        /// Khởi tạo
        /// </summary>
        public UCMenu()
        {
            InitializeComponent();
            _ConnectionServer = true;
        }

        public string _Barcode { get; set; }

        /// <summary>
        /// Biến có kiểm tra server hay không
        /// </summary>
        public bool _CheckConnectionServer
        {
            get { return checkConnectionServer; }
            set { checkConnectionServer = value; }
        }

        public bool _ClickGroup { get; set; }

        public bool _ClickItems { get; set; }

        /// <summary>
        /// Biến giữ giá trị kết nối server
        /// </summary>
        public bool _ConnectionServer { get; set; }

        public DataObject.DeleteType _Delete
        {
            get { return delete; }
            set { delete = value; }
        }

        public bool _ExistsItem { get; set; }

        /// <summary>
        /// Giá trị giữ GroupID hiện tại ở bất cứ chỗ nào
        /// </summary>
        public int _GroupID { get; set; }

        public DataObject.GroupMenu _GroupMenu { get; set; }

        public DataObject.IsFuelType _IsFuel
        {
            get { return isFuel; }
            set { isFuel = value; }
        }

        public int _ItemID { get; set; }

        public DataObject.ItemsMenu _ItemsMenu { get; set; }

        public bool _LoadMenu
        {
            get { return loadMenu; }
            set { loadMenu = value; }
        }

        /// <summary>
        /// Giá trị giữ ShortCut hiện tại ở bất cứ chỗ nào
        /// </summary>

        public DataObject.ListItemType _Type { get; set; }

        public DataObject.VisualType _Visual
        {
            get { return visual; }
            set { visual = value; }
        }

        public SystemConfig.DBConfig mDBConfig { get; set; }

        #region Phan trang

        private int IndexGroupItems = 0;
        private int IndexGroupMenu = 0;
        private int MaxGroupItem = 0;
        private int MaxGroupMenu = 0;

        public int GroupDisplayOrderMax { get; set; }

        public int ItemDisplayOrderMax { get; set; }

        #endregion Phan trang

        /// <summary>
        /// Hàm gán thuộc tính lúc khởi tạo
        /// </summary>
        /// <param name="db"></param>
        public void InitMenu(SystemConfig.DBConfig db)
        {
            mDBConfig = db;
            ReadConfig();
            RefreshLayoutItemMenu();
            LoadFrameGroup();
            LoadFrameItem();
            if (_LoadMenu)
                LoadMenuGroup(0);
        }

        public void LoadGroupbByItem(int g_ID, int i_ID)
        {
            int ListCount = 0;
            List<DataObject.GroupMenuGroup> lsArray = new List<DataObject.GroupMenuGroup>();
            lsArray = BusinessObject.BOGroupMenu.GetGroupMenuGroup(ref ListCount, _ColumnGroup * _RowGroup, (int)mShortcut, BusinessObject.FunctionEnum.GetVisual(_Visual), BusinessObject.FunctionEnum.GetDelete(_Delete), _ExistsItem, mDBConfig);
            GroupDisplayOrderMax = ListCount;
            MaxGroupMenu = lsArray.Count;
            IndexGroupMenu = 0;
            int i = 0;
            foreach (DataObject.GroupMenuGroup item in lsArray)
            {
                foreach (DataObject.GroupMenu g in item.Group)
                {
                    if (g.GroupID == g_ID)
                    {
                        IndexGroupMenu = i;
                        _GroupMenu = g;
                    }
                }
                i++;
            }
            LoadItemByItem(g_ID, i_ID);
            LoadMenuGroup(g_ID);
        }

        private void SetShortCut(DataObject.GroupMenu item)
        {
            mShortcut = BusinessObject.FunctionEnum.GetTypeMenu(item.GrShortCut);
        }

        public void LoadItemByItem(int g_ID, int i_ID)
        {
            int ListCount = 0;
            List<DataObject.ItemsMenuGroup> lsArray = new List<DataObject.ItemsMenuGroup>();
            lsArray = BusinessObject.BOItemsMenu.GetItemsMenuGroup(ref ListCount, g_ID, _ColumnItem * _RowItem, BusinessObject.FunctionEnum.GetVisual(_Visual), BusinessObject.FunctionEnum.GetDelete(_Delete), BusinessObject.FunctionEnum.GetIsFuel(_IsFuel), mDBConfig);
            ItemDisplayOrderMax = ListCount;
            MaxGroupItem = lsArray.Count;
            IndexGroupItems = 0;
            int k = 0;
            foreach (DataObject.ItemsMenuGroup item in lsArray)
            {
                foreach (DataObject.ItemsMenu i in item.Items)
                {
                    if (i.ItemID == i_ID)
                    {
                        IndexGroupItems = k;
                        _ItemsMenu = i;
                    }
                }
                k++;
            }
        }

        /// <summary>
        /// Load danh sách nút Group
        /// </summary>
        public void LoadMenuGroup(int ID)
        {
            int ListCount = 0;
            List<DataObject.GroupMenuGroup> lsArray = new List<DataObject.GroupMenuGroup>();
            lsArray = BusinessObject.BOGroupMenu.GetGroupMenuGroup(ref ListCount, _RowGroup * _ColumnGroup, (int)mShortcut, BusinessObject.FunctionEnum.GetVisual(_Visual), BusinessObject.FunctionEnum.GetDelete(_Delete), _ExistsItem, mDBConfig);
            GroupDisplayOrderMax = ListCount;
            MaxGroupMenu = lsArray.Count;
            int i = 0;
            ClearGroup(0);
            ClearItems(0);
            if (lsArray.Count > 0)
            {
                DataObject.GroupMenuGroup gmg = lsArray[IndexGroupMenu];
                bool first = true;

                foreach (DataObject.GroupMenu item in gmg.Group)
                {
                    if (first && ID == 0)
                    {
                        _GroupMenu = item;
                        _GroupID = item.GroupID;
                        IndexColor = i;
                        LoadMenuItems();
                        first = false;
                    }
                    if (ID == item.GroupID)
                    {
                        _GroupMenu = item;
                        _GroupID = item.GroupID;
                        IndexColor = i;
                        LoadMenuItems();
                        first = false;
                    }

                    tlpGroupMenu.Controls[i].Text = item.GroupShort;
                    tlpGroupMenu.Controls[i].Name = "Group";
                    tlpGroupMenu.Controls[i].Tag = item;
                    tlpGroupMenu.Controls[i].BackColor = listColor[i];
                    tlpGroupMenu.Controls[i].Enabled = true;
                    i++;
                }
            }
            ClearGroup(i);
        }

        /// <summary>
        /// Load danh sách nút item
        /// </summary>
        public void LoadMenuItems()
        {
            int ListCount = 0;
            List<DataObject.ItemsMenuGroup> lsArray = new List<DataObject.ItemsMenuGroup>();
            lsArray = BusinessObject.BOItemsMenu.GetItemsMenuGroup(ref ListCount, _GroupID, _ColumnItem * _RowItem, BusinessObject.FunctionEnum.GetVisual(_Visual), BusinessObject.FunctionEnum.GetDelete(_Delete), BusinessObject.FunctionEnum.GetIsFuel(_IsFuel), mDBConfig);
            ItemDisplayOrderMax = ListCount;
            MaxGroupItem = lsArray.Count;
            int i = 0;
            if (lsArray.Count > 0)
            {
                DataObject.ItemsMenuGroup img = lsArray[IndexGroupItems];

                foreach (DataObject.ItemsMenu item in img.Items)
                {
                    tlpItemMenu.Controls[i].Text = item.ItemShort;
                    tlpItemMenu.Controls[i].Name = "Item";
                    tlpItemMenu.Controls[i].Tag = item;
                    tlpItemMenu.Controls[i].BackColor = listColor[IndexColor];
                    
                    tlpItemMenu.Controls[i].Enabled = true;
                    i++;
                }
            }
            ClearItems(i);
        }

        public void Refresh()
        {
            IndexGroupItems = 0;
            IndexGroupMenu = 0;
            LoadMenuGroup(0);
        }

        public bool RefreshItemID()
        {
            DataObject.ItemsMenu item = BusinessObject.BOItemsMenu.GetItemMenuByID(_ItemID, mDBConfig);
            if (item.ItemID > 0)
            {
                _ItemsMenu = item;
                DataObject.GroupMenu gm = new DataObject.GroupMenu();
                gm = BusinessObject.BOGroupMenu.GetGroupMenuByID(item.GroupID, mDBConfig);
                SetShortCut(gm);
                LoadGroupbByItem(item.GroupID, item.ItemID);
                return true;
            }
            else
                return false;
        }

        public bool RefreshBarcode()
        {
            DataObject.ItemsMenu item = BusinessObject.BOItemsMenu.GetItemMenuByBarcode(_Barcode, mDBConfig);
            if (item.ItemID > 0)
            {
                _ItemsMenu = item;
                DataObject.GroupMenu gm = new DataObject.GroupMenu();
                gm = BusinessObject.BOGroupMenu.GetGroupMenuByID(item.GroupID, mDBConfig);
                SetShortCut(gm);
                LoadGroupbByItem(item.GroupID, item.ItemID);
                return true;
            }
            else
                return false;
        }

        public Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        private void btnDown_GroupMenu_Click(object sender, EventArgs e)
        {
            if (IndexGroupMenu < MaxGroupMenu - 1)
            {
                IndexGroupMenu++;
                IndexGroupItems = 0;
                LoadMenuGroup(0);
            }
        }

        private void btnDown_ItemMenu_Click(object sender, EventArgs e)
        {
            if (IndexGroupItems < MaxGroupItem - 1)
            {
                IndexGroupItems++;
                LoadMenuItems();
            }
        }

        /// <summary>
        /// Sự kiện click vào Group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGroupMenu_Click(object sender, EventArgs e)
        {
            ButtonMenuGroup btnMenuGroup = (ButtonMenuGroup)sender;
            _GroupMenu = null;
            if (btnMenuGroup != null)
            {
                DataObject.GroupMenu gm = (DataObject.GroupMenu)btnMenuGroup.Tag;
                _GroupID = gm.GroupID;
                IndexColor = gm.Color;
                IndexGroupItems = 0;
                _Type = DataObject.ListItemType.Items;
                _ItemsMenu = null;
                _GroupMenu = gm;
                LoadMenuItems();
                if (_ClickGroup)
                    base.OnClick(e);
            }
        }

        /// <summary>
        /// Sự kiện nút Items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemsMenu_Click(object sender, EventArgs e)
        {
            ButtonMenuItem btnMenuItems = (ButtonMenuItem)sender;
            _ItemsMenu = null;
            if (btnMenuItems != null)
            {
                DataObject.ItemsMenu im = (DataObject.ItemsMenu)btnMenuItems.Tag;
                switch (_Type)
                {
                    case DataObject.ListItemType.Items:
                        _ItemsMenu = im;
                        if (_ClickItems)
                            base.OnClick(e);
                        break;
                }
            }
        }

        #region Thiet ke lai layout

        public void RefreshLayoutItemMenu()
        {
            LayoutColumnItem();
            LayoutRowItem();
            LayoutRowGroup();
            LayoutColumnGroup();
            LayoutGroupWidth();
        }

        private void LayoutColumnGroup()
        {
            _ColumnGroup = BusinessObject.BOConfig.GetMenuColumnsGroupCount;
            tlpGroupMenu.ColumnStyles.Clear();
            System.Single Lenght = 100 / _ColumnGroup;
            tlpGroupMenu.ColumnCount = _ColumnGroup;
            for (int i = 0; i < _ColumnGroup; i++)
            {
                tlpGroupMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, Lenght));
            }
        }

        private void LayoutColumnItem()
        {
            _ColumnItem = BusinessObject.BOConfig.GetMenuColumnsItemCount;
            tlpItemMenu.ColumnStyles.Clear();
            System.Single Lenght = 100 / _ColumnItem;
            tlpItemMenu.ColumnCount = _ColumnItem;
            for (int i = 0; i < _ColumnItem; i++)
            {
                tlpItemMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, Lenght));
            }
        }

        private void LayoutGroupWidth()
        {
            tlpGroupMenu.Width = _MenuGroupWidth;
        }

        private void LayoutRowGroup()
        {
            _RowGroup = BusinessObject.BOConfig.GetMenuRowGroupCount;
            tlpGroupMenu.RowStyles.Clear();
            tlpGroupMenu.RowCount = _RowGroup;
            System.Single Lenght = 100 / _RowGroup;
            for (int i = 0; i < _RowGroup; i++)
            {
                tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, Lenght));
            }
        }

        private void LayoutRowItem()
        {
            _RowItem = BusinessObject.BOConfig.GetMenuRowItemCount;
            tlpItemMenu.RowStyles.Clear();
            tlpItemMenu.RowCount = _RowItem;
            System.Single Lenght = 100 / _RowItem;
            for (int i = 0; i < _RowItem; i++)
            {
                tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, Lenght));
            }
        }

        #endregion Thiet ke lai layout

        /// <summary>
        /// Sự kiện Up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUp_GroupMenu_Click(object sender, EventArgs e)
        {
            if (IndexGroupMenu > 0)
            {
                IndexGroupMenu--;
                IndexGroupItems = 0;
                LoadMenuGroup(0);
            }
        }

        private void btnUp_ItemMenu_Click(object sender, EventArgs e)
        {
            if (IndexGroupItems > 0)
            {
                IndexGroupItems--;
                LoadMenuItems();
            }
        }

        private void ClearGroup(int Index)
        {
            if (tlpGroupMenu.Controls.Count > 0)
            {
                for (int i = Index; i < _ColumnGroup * _RowGroup; i++)
                {
                    ButtonMenuGroup btn = (ButtonMenuGroup)tlpGroupMenu.Controls[i];
                    tlpGroupMenu.Controls[i].Text = "";
                    tlpGroupMenu.Controls[i].Tag = null;
                    tlpGroupMenu.Controls[i].BackColor = Color.White;
                    tlpGroupMenu.Controls[i].Enabled = false;
                    btn.Image = null;
                }
            }
        }

        private void ClearItems(int Index)
        {
            if (tlpItemMenu.Controls.Count > 0)
            {
                for (int i = Index; i < _RowItem * _ColumnItem; i++)
                {
                    ButtonMenuItem btn = (ButtonMenuItem)tlpItemMenu.Controls[i];
                    tlpItemMenu.Controls[i].Text = "";
                    tlpItemMenu.Controls[i].Tag = null;
                    tlpItemMenu.Controls[i].BackColor = Color.White;
                    tlpItemMenu.Controls[i].Enabled = false;
                    btn.Image = null;
                }
            }
        }

        private void LoadFrameGroup()
        {
            int size = _ColumnGroup * _RowGroup;
            ButtonMenuGroup[] listGroup = new ButtonMenuGroup[size];
            for (int i = 0; i < size; i++)
            {
                listGroup[i] = new ButtonMenuGroup();
                listGroup[i].Click += new EventHandler(btnGroupMenu_Click);
            }
            tlpGroupMenu.Controls.AddRange(listGroup);
            //for (int i = 0; i < size; i++)
            //{
            //    tlpGroupMenu.Controls.Add(listGroup[i]);
            //}
            //ButtonMenuGroup btn;
            //for (int i = 0; i < size; i++)
            //{
            //     btn= new ButtonMenuGroup();
            //    btn.BackColor = listColor[i];
            //    tlpGroupMenu.Controls.Add(btn);
            //    btn.Click += new EventHandler(btnGroupMenu_Click);
            //}
        }

        private void LoadFrameItem()
        {
            int size = _RowItem * _ColumnItem;
            ButtonMenuItem[] listButton = new ButtonMenuItem[size];
            for (int i = 0; i < size; i++)
            {
                listButton[i] = new ButtonMenuItem();
                listButton[i].Click += new EventHandler(btnItemsMenu_Click);
            }
            tlpItemMenu.Controls.AddRange(listButton);
            //ButtonMenuItem btn;
            //for (int i = 0; i < size; i++)
            //{
            //     btn= new ButtonMenuItem();
            //    tlpItemMenu.Controls.Add(btn);
            //    btn.Click += new EventHandler(btnItemsMenu_Click);
            //}
        }

        /// <summary>
        /// Đọc file config một lần duy nhất
        /// </summary>
        private void ReadConfig()
        {
            _ColumnItem = BusinessObject.BOConfig.GetMenuColumnsItemCount;
            _RowItem = BusinessObject.BOConfig.GetMenuRowItemCount;
            _MenuImagesGroup = BusinessObject.BOConfig.GetMenuImagesGroup;
            _MenuImagesItem = BusinessObject.BOConfig.GetMenuImagesItems;
            _MenuNoImages = BusinessObject.BOConfig.GetMenuNoImages;
        }

        private void UCMenu_Load(object sender, EventArgs e)
        {
        }
    }
}