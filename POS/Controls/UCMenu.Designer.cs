﻿namespace POS.Controls
{
    partial class UCMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnBottom = new System.Windows.Forms.Panel();
            this.btnDown_ItemMenu = new System.Windows.Forms.Button();
            this.btnUp_ItemMenu = new System.Windows.Forms.Button();
            this.btnDown_GroupMenu = new System.Windows.Forms.Button();
            this.btnUp_GroupMenu = new System.Windows.Forms.Button();
            this.tlpGroupMenu = new System.Windows.Forms.TableLayoutPanel();
            this.tlpItemMenu = new System.Windows.Forms.TableLayoutPanel();
            this.pnBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.btnDown_ItemMenu);
            this.pnBottom.Controls.Add(this.btnUp_ItemMenu);
            this.pnBottom.Controls.Add(this.btnDown_GroupMenu);
            this.pnBottom.Controls.Add(this.btnUp_GroupMenu);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 350);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(401, 74);
            this.pnBottom.TabIndex = 0;
            // 
            // btnDown_ItemMenu
            // 
            this.btnDown_ItemMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDown_ItemMenu.FlatAppearance.BorderSize = 0;
            this.btnDown_ItemMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown_ItemMenu.Image = global::POS.Properties.Resources.arrow_double_down;
            this.btnDown_ItemMenu.Location = new System.Drawing.Point(318, 4);
            this.btnDown_ItemMenu.Name = "btnDown_ItemMenu";
            this.btnDown_ItemMenu.Size = new System.Drawing.Size(80, 65);
            this.btnDown_ItemMenu.TabIndex = 0;
            this.btnDown_ItemMenu.UseVisualStyleBackColor = true;
            this.btnDown_ItemMenu.Click += new System.EventHandler(this.btnDown_ItemMenu_Click);
            // 
            // btnUp_ItemMenu
            // 
            this.btnUp_ItemMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUp_ItemMenu.FlatAppearance.BorderSize = 0;
            this.btnUp_ItemMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUp_ItemMenu.Image = global::POS.Properties.Resources.arrow_double_up;
            this.btnUp_ItemMenu.Location = new System.Drawing.Point(233, 4);
            this.btnUp_ItemMenu.Name = "btnUp_ItemMenu";
            this.btnUp_ItemMenu.Size = new System.Drawing.Size(84, 65);
            this.btnUp_ItemMenu.TabIndex = 0;
            this.btnUp_ItemMenu.UseVisualStyleBackColor = true;
            this.btnUp_ItemMenu.Click += new System.EventHandler(this.btnUp_ItemMenu_Click);
            // 
            // btnDown_GroupMenu
            // 
            this.btnDown_GroupMenu.FlatAppearance.BorderSize = 0;
            this.btnDown_GroupMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown_GroupMenu.Image = global::POS.Properties.Resources.Arrow_down;
            this.btnDown_GroupMenu.Location = new System.Drawing.Point(78, 4);
            this.btnDown_GroupMenu.Name = "btnDown_GroupMenu";
            this.btnDown_GroupMenu.Size = new System.Drawing.Size(72, 65);
            this.btnDown_GroupMenu.TabIndex = 0;
            this.btnDown_GroupMenu.UseVisualStyleBackColor = true;
            this.btnDown_GroupMenu.Click += new System.EventHandler(this.btnDown_GroupMenu_Click);
            // 
            // btnUp_GroupMenu
            // 
            this.btnUp_GroupMenu.FlatAppearance.BorderSize = 0;
            this.btnUp_GroupMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUp_GroupMenu.Image = global::POS.Properties.Resources.Arrow_up;
            this.btnUp_GroupMenu.Location = new System.Drawing.Point(4, 4);
            this.btnUp_GroupMenu.Name = "btnUp_GroupMenu";
            this.btnUp_GroupMenu.Size = new System.Drawing.Size(72, 65);
            this.btnUp_GroupMenu.TabIndex = 0;
            this.btnUp_GroupMenu.Text = "          ";
            this.btnUp_GroupMenu.UseVisualStyleBackColor = true;
            this.btnUp_GroupMenu.Click += new System.EventHandler(this.btnUp_GroupMenu_Click);
            // 
            // tlpGroupMenu
            // 
            this.tlpGroupMenu.ColumnCount = 1;
            this.tlpGroupMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGroupMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlpGroupMenu.Location = new System.Drawing.Point(0, 0);
            this.tlpGroupMenu.Margin = new System.Windows.Forms.Padding(0);
            this.tlpGroupMenu.Name = "tlpGroupMenu";
            this.tlpGroupMenu.RowCount = 7;
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpGroupMenu.Size = new System.Drawing.Size(150, 350);
            this.tlpGroupMenu.TabIndex = 1;
            // 
            // tlpItemMenu
            // 
            this.tlpItemMenu.ColumnCount = 3;
            this.tlpItemMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpItemMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpItemMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpItemMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpItemMenu.Location = new System.Drawing.Point(150, 0);
            this.tlpItemMenu.Margin = new System.Windows.Forms.Padding(0);
            this.tlpItemMenu.Name = "tlpItemMenu";
            this.tlpItemMenu.RowCount = 7;
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlpItemMenu.Size = new System.Drawing.Size(251, 350);
            this.tlpItemMenu.TabIndex = 2;
            // 
            // UCMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpItemMenu);
            this.Controls.Add(this.tlpGroupMenu);
            this.Controls.Add(this.pnBottom);
            this.Name = "UCMenu";
            this.Size = new System.Drawing.Size(401, 424);
            this.Load += new System.EventHandler(this.UCMenu_Load);
            this.pnBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.TableLayoutPanel tlpGroupMenu;
        private System.Windows.Forms.TableLayoutPanel tlpItemMenu;
        private System.Windows.Forms.Button btnDown_ItemMenu;
        private System.Windows.Forms.Button btnUp_ItemMenu;
        private System.Windows.Forms.Button btnDown_GroupMenu;
        private System.Windows.Forms.Button btnUp_GroupMenu;
    }
}
