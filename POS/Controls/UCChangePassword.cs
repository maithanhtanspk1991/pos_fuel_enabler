﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
namespace POS.Controls
{
    public partial class UCChangePassword : UserControl
    {
        Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public UCChangePassword()
        {
            InitializeComponent();
            SetMultiLanguge();
        }

        private void SetMultiLanguge()
        {
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnChange.Text = "Thay đổi";
                label5.Text = "Thay đổi mật khẩu";
                label1.Text = "Tài khoản:";
                label1.TextAlign = ContentAlignment.TopRight;
                label2.Text = "Mật khẩu cũ:";
                label2.TextAlign = ContentAlignment.TopRight;
                label3.Text = "Mật khẩu mới:";
                label3.TextAlign = ContentAlignment.TopRight;
                label4.Text = "Xác thực mật khẩu mới:";
                label4.TextAlign = ContentAlignment.TopRight;
                return;
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (CheckTextBox())
            {
                Connection.Connection con = new Connection.Connection();
                try
                {
                    con.Open();
                    con.BeginTransaction();
                    string sql = "";
                    sql = "select * from staffs where staffID=" + txtStaffID.Text + " and `password`=password('" + txtPasswordOld.Text + "')";
                    DataTable tbl = con.Select(sql);
                    if (tbl.Rows.Count > 0)
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            sql = "update staffs set `password`=password(" + txtPasswordNew.Text + ")  where staffID=" + txtStaffID.Text;
                            con.ExecuteNonQuery(sql);
                            con.Commit();
                            lbStatus.Text = "Thay đổi mật khẩu thành công";
                            txtStaffID.Text = "";
                            txtPasswordOld.Text = "";
                            txtPasswordNew.Text = "";
                            txtConfirmedPasswordNew.Text = "";
                            return;
                        }
                        sql = "update staffs set `password`=password(" + txtPasswordNew.Text + ")  where staffID=" + txtStaffID.Text;
                        con.ExecuteNonQuery(sql);
                        con.Commit();
                        lbStatus.Text = "Chagne password successfull";
                        txtStaffID.Text = "";
                        txtPasswordOld.Text = "";
                        txtPasswordNew.Text = "";
                        txtConfirmedPasswordNew.Text = "";
                    }
                    else
                    {
                        if (mReadConfig.LanguageCode.Equals("vi"))
                        {
                            lbStatus.Text = "Tài khoản hay mật khẩu không đúng!";
                            return;
                        }
                        lbStatus.Text = "User or password do not match!";
                    }
                }
                catch (Exception ex)
                {
                    if (mReadConfig.LanguageCode.Equals("vi"))
                    {
                        lbStatus.Text = "Thay đổi mật khẩu không thành công";
                        con.Rollback();
                        Class.LogPOS.WriteLog("POS.Controls::UCChangePassword::btnChange_Click" + ex.Message);
                        return;
                    }
                    lbStatus.Text = "Change password faield";
                    con.Rollback();
                    Class.LogPOS.WriteLog("POS.Controls::UCChangePassword::btnChange_Click" + ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private bool CheckTextBox()
        {
            if (txtStaffID.Text == "" || txtPasswordOld.Text == "" || txtPasswordNew.Text == "" || txtConfirmedPasswordNew.Text == "")
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Thông tin không được trống";
                    return false;
                }
                lbStatus.Text = "Imformation Empty";
                return false;
            }
            if (CheckPass(txtPasswordNew.Text, txtConfirmedPasswordNew.Text) == false)
            {
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    lbStatus.Text = "Xác thực mật khảu không đúng!";
                    return false;
                }
                lbStatus.Text = "Confirmed password dose not match!";
                return false;
            }
            return true;
        }

        private bool CheckPass(string str1, string str2)
        {
            if (String.Compare(str1, str2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}