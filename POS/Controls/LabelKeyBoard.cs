﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace POS.Controls
{
    public partial class LabelKeyBoard : System.Windows.Forms.Label
    {
        public LabelKeyBoard()
        {

        }

        protected override void OnClick(EventArgs e)
        {
            System.Windows.Forms.TextBox txt = new System.Windows.Forms.TextBox();
            txt.Text = Text;
            txt.Location = Location;
            FormKey.frmKeyboard frm = new FormKey.frmKeyboard(txt);            
            frm.ShowDialog();
            Text = txt.Text;
            base.OnClick(e);
        }
    }
}
