﻿namespace POS.Controls
{
    partial class UCGroupChange
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbVisual = new System.Windows.Forms.Label();
            this.lbshort = new System.Windows.Forms.Label();
            this.lbdisplay = new System.Windows.Forms.Label();
            this.lbdesc = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbVisual = new System.Windows.Forms.CheckBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.txtDispalyOrder = new POS.Controls.TextBoxPOSKeyPad();
            this.txtGroupDesc = new POS.Controls.TextBoxPOSKeyBoard();
            this.txtGroupShort = new POS.Controls.TextBoxPOSKeyBoard();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbVisual);
            this.panel1.Controls.Add(this.lbshort);
            this.panel1.Controls.Add(this.lbdisplay);
            this.panel1.Controls.Add(this.lbdesc);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(136, 205);
            this.panel1.TabIndex = 0;
            // 
            // lbVisual
            // 
            this.lbVisual.AutoSize = true;
            this.lbVisual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVisual.Location = new System.Drawing.Point(10, 131);
            this.lbVisual.Name = "lbVisual";
            this.lbVisual.Size = new System.Drawing.Size(58, 20);
            this.lbVisual.TabIndex = 86;
            this.lbVisual.Text = "Visual";
            // 
            // lbshort
            // 
            this.lbshort.AutoSize = true;
            this.lbshort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshort.Location = new System.Drawing.Point(10, 53);
            this.lbshort.Margin = new System.Windows.Forms.Padding(0);
            this.lbshort.Name = "lbshort";
            this.lbshort.Size = new System.Drawing.Size(105, 20);
            this.lbshort.TabIndex = 83;
            this.lbshort.Text = "Group Desc";
            this.lbshort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdisplay
            // 
            this.lbdisplay.AutoSize = true;
            this.lbdisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdisplay.Location = new System.Drawing.Point(10, 95);
            this.lbdisplay.Margin = new System.Windows.Forms.Padding(0);
            this.lbdisplay.Name = "lbdisplay";
            this.lbdisplay.Size = new System.Drawing.Size(117, 20);
            this.lbdisplay.TabIndex = 85;
            this.lbdisplay.Text = "Display Order";
            this.lbdisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbdesc
            // 
            this.lbdesc.AutoSize = true;
            this.lbdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdesc.Location = new System.Drawing.Point(10, 13);
            this.lbdesc.Margin = new System.Windows.Forms.Padding(0);
            this.lbdesc.Name = "lbdesc";
            this.lbdesc.Size = new System.Drawing.Size(108, 20);
            this.lbdesc.TabIndex = 82;
            this.lbdesc.Text = "Group Short";
            this.lbdesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbVisual);
            this.panel2.Controls.Add(this.lbStatus);
            this.panel2.Controls.Add(this.txtDispalyOrder);
            this.panel2.Controls.Add(this.txtGroupDesc);
            this.panel2.Controls.Add(this.txtGroupShort);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(136, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(212, 205);
            this.panel2.TabIndex = 1;
            // 
            // cbVisual
            // 
            this.cbVisual.AutoSize = true;
            this.cbVisual.Location = new System.Drawing.Point(7, 134);
            this.cbVisual.Name = "cbVisual";
            this.cbVisual.Size = new System.Drawing.Size(15, 14);
            this.cbVisual.TabIndex = 59;
            this.cbVisual.UseVisualStyleBackColor = true;
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(0, 163);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(212, 42);
            this.lbStatus.TabIndex = 58;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDispalyOrder
            // 
            this.txtDispalyOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtDispalyOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtDispalyOrder.IsLockDot = false;
            this.txtDispalyOrder.IsNegative = false;
            this.txtDispalyOrder.Location = new System.Drawing.Point(7, 89);
            this.txtDispalyOrder.Name = "txtDispalyOrder";
            this.txtDispalyOrder.Size = new System.Drawing.Size(80, 32);
            this.txtDispalyOrder.TabIndex = 2;
            // 
            // txtGroupDesc
            // 
            this.txtGroupDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGroupDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtGroupDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtGroupDesc.Location = new System.Drawing.Point(6, 47);
            this.txtGroupDesc.Name = "txtGroupDesc";
            this.txtGroupDesc.Size = new System.Drawing.Size(195, 32);
            this.txtGroupDesc.TabIndex = 0;
            // 
            // txtGroupShort
            // 
            this.txtGroupShort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGroupShort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtGroupShort.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtGroupShort.Location = new System.Drawing.Point(6, 7);
            this.txtGroupShort.Name = "txtGroupShort";
            this.txtGroupShort.Size = new System.Drawing.Size(195, 32);
            this.txtGroupShort.TabIndex = 0;
            this.txtGroupShort.TextChanged += new System.EventHandler(this.txtGroupShort_TextChanged);
            // 
            // UCGroupChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "UCGroupChange";
            this.Size = new System.Drawing.Size(348, 205);
            this.Load += new System.EventHandler(this.UCGroupChange_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbshort;
        private System.Windows.Forms.Label lbdisplay;
        private System.Windows.Forms.Label lbdesc;
        private TextBoxPOSKeyPad txtDispalyOrder;
        private TextBoxPOSKeyBoard txtGroupDesc;
        private TextBoxPOSKeyBoard txtGroupShort;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbVisual;
        private System.Windows.Forms.CheckBox cbVisual;
    }
}
