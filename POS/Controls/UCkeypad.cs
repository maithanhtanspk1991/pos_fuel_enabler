﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCkeypad : UserControl
    {
        public string Result;

        public UCkeypad()
        {
            InitializeComponent();
        }

        public TextBox txtResult { get; set; }

        private void btn0_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("9");
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Text = "";
            //ktdot = false;
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("{BACKSPACE}");
        }

        private void btndot_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();

            if (txtResult.Text.Length == 0)
            {
                SendKeys.Send("0");
                SendKeys.Send(".");
            }
            else
            {
                if (txtResult.Text.Contains('.'))
                {
                    return;
                }
                else
                {
                    SendKeys.Send(".");
                }
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            Result = txtResult.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("000");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            txtResult.Focus();
            SendKeys.Send("00");
        }

        private void UCkeypad_Load(object sender, EventArgs e)
        {
        }
    }
}