﻿namespace POS.Controls
{
    partial class UCPreOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOrderID = new System.Windows.Forms.Label();
            this.lbContent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbOrderID
            // 
            this.lbOrderID.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderID.ForeColor = System.Drawing.Color.White;
            this.lbOrderID.Location = new System.Drawing.Point(0, 0);
            this.lbOrderID.Name = "lbOrderID";
            this.lbOrderID.Size = new System.Drawing.Size(120, 56);
            this.lbOrderID.TabIndex = 0;
            this.lbOrderID.Text = "OrderID";
            this.lbOrderID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbOrderID.Click += new System.EventHandler(this.UCPreOrder_Click);
            // 
            // lbContent
            // 
            this.lbContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbContent.ForeColor = System.Drawing.Color.White;
            this.lbContent.Location = new System.Drawing.Point(0, 56);
            this.lbContent.Name = "lbContent";
            this.lbContent.Size = new System.Drawing.Size(120, 104);
            this.lbContent.TabIndex = 1;
            this.lbContent.Text = "Content";
            this.lbContent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbContent.Click += new System.EventHandler(this.UCPreOrder_Click);
            // 
            // UCPreOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.Controls.Add(this.lbContent);
            this.Controls.Add(this.lbOrderID);
            this.Name = "UCPreOrder";
            this.Size = new System.Drawing.Size(120, 160);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lbOrderID;
        public System.Windows.Forms.Label lbContent;

    }
}
