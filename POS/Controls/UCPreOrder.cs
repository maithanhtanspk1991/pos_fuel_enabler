﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCPreOrder : UserControl
    {
        public UCPreOrder()
        {
            InitializeComponent();
            lbContent.Text = "";
            lbOrderID.Text = "";
        }

        private void UCPreOrder_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }
    }
}
