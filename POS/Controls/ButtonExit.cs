﻿namespace POS.Controls
{
    internal class ButtonExit : System.Windows.Forms.Button
    {
        public ButtonExit()
        {
            this.BackColor = System.Drawing.Color.Red;
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.Dock = System.Windows.Forms.DockStyle.Left;

            this.ForeColor = System.Drawing.Color.White;
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                Class.ReadConfig mReadConfig = new Class.ReadConfig();
                if (mReadConfig.LanguageCode.Equals("vi"))
                {
                    base.Text = "Thoát";
                    return;
                }
                base.Text = "Exit";
            }
        }
    }
}