﻿using System;

namespace POS.Controls
{
    partial class TextBoxPOSKeyPad : System.Windows.Forms.TextBox
    {
        public bool IsLockDot { get; set; }

        public bool IsNegative { get; set; }

        public TextBoxPOSKeyPad()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
        }

        protected override void OnEnter(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.White;
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            FormKey.frmKeyPadNew frm = new FormKey.frmKeyPadNew(this, IsLockDot);
            frm.IsNegative = IsNegative;

            frm.ShowDialog();
            base.OnClick(e);
        }
    }
}