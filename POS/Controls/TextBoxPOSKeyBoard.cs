﻿using System;

namespace POS.Controls
{
    partial class TextBoxPOSKeyBoard : System.Windows.Forms.TextBox
    {
        public TextBoxPOSKeyBoard()
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 12);
        }

        protected override void OnEnter(EventArgs e)
        {
            this.InitForcus();
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            this.InitForcus();
            base.OnClick(e);
        }

        private void InitForcus()
        {
            FormKey.frmKeyboard frm = new FormKey.frmKeyboard(this);
            this.BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            frm.ShowDialog();
        }

        public void SetForcus()
        {            
            this.Focus();
        }
    }
}