﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace POS.Controls
{
    public partial class ButtonBecas : System.Windows.Forms.Button
    {
        public ButtonBecas()
        {
            FlatAppearance.BorderSize = 0;
            FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ForeColor = System.Drawing.Color.White;
            UseVisualStyleBackColor = false;
        }
    }

    public partial class ButtonBecasOK : ButtonBecas
    {
        public ButtonBecasOK()
        {
            BackColor = System.Drawing.Color.FromArgb(100, 170, 0);
            Text = "OK";
        }
    }

    public partial class ButtonBecasBack : ButtonBecas
    {
        public ButtonBecasBack()
        {
            BackColor = System.Drawing.Color.Red;
            Text = "Back";
        }
    }

    public partial class ButtonBecasClear : ButtonBecas
    {
        public ButtonBecasClear()
        {
            BackColor = System.Drawing.Color.FromArgb(255, 120, 0);
            Text = "Clear";
        }
    }

    public partial class ButtonBecasNew : ButtonBecas
    {
        public ButtonBecasNew()
        {
            BackColor = System.Drawing.Color.FromArgb(100, 170, 255);
            Text = "New";
        }
    }
}
