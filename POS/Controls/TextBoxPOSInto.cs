﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class TextBoxPOSInto : TextBox
    {
        public Label _LabelStatusError { get; set; }

        public POS.Controls.UCkeypad _Keypad { get; set; }

        public POS.Controls.UCMoney _Money { get; set; }

        public bool _LocketText { get; set; }

        private double maxNumber = -1;
        private int precision = 2;

        public int _Precision
        {
            get { return precision; }
            set { precision = value; }
        }

        private TextBoxType type = TextBoxType.All;
        private string statusError = "Invalid";

        public string _StatusError
        {
            get { return statusError; }
            set { statusError = value; }
        }

        public TextBoxType _Type
        {
            get { return type; }
            set { type = value; }
        }

        public double _MaxNumber
        {
            get { return maxNumber; }
            set { maxNumber = value; }
        }

        public TextBoxPOSInto()
        {
            this.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
        }

        protected override void OnClick(EventArgs e)
        {
            IniForcus();
            this.SelectAll();
            base.OnClick(e);
        }

        protected override void OnEnter(EventArgs e)
        {
            IniForcus();
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
            base.OnLeave(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            _LabelStatusError.Text = "";
            switch (_Type)
            {
                case TextBoxType.String:
                    break;

                case TextBoxType.NumberInterger:

                    if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
                    {
                        _LabelStatusError.Text = _StatusError;
                        e.Handled = true;
                    }
                    else if (!Char.IsControl(e.KeyChar) && _MaxNumber > -1 && Convert.ToDouble(Text + (char)e.KeyChar) > _MaxNumber)
                    {
                        _LabelStatusError.Text = _StatusError;
                        e.Handled = true;
                    }
                    break;

                case TextBoxType.NumberDouble:
                    if (e.KeyChar == (char)46 && Text.IndexOf('.') > 0)
                    {
                        e.Handled = true;
                    }
                    else if (e.KeyChar != (char)8 && Text.IndexOf('.') > 0 && TextLength - Text.IndexOf('.') > _Precision)
                    {
                        _LabelStatusError.Text = "Precision on numbers is " + _Precision.ToString();
                        e.Handled = true;
                    }
                    else if (!Char.IsControl(e.KeyChar) && _MaxNumber > -1 && Convert.ToDouble(Text + (char)e.KeyChar) > _MaxNumber)
                    {
                        _LabelStatusError.Text = _StatusError;
                        e.Handled = true;
                    }

                    break;

                case TextBoxType.Digit:
                    if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
                        e.Handled = true;
                    break;
            }
            base.OnKeyPress(e);
        }

        private void IniForcus()
        {
            if (_Keypad != null)
            {
                _Keypad.txtResult = this;
            }
            if (_Money != null)
            {
                _Money.txtResult = this;
            }
            _LocketText = false;
            this.BackColor = System.Drawing.Color.White;
        }

        public void SetForcus()
        {
            IniForcus();
            this.Focus();
        }
    }

    public enum TextBoxType
    {
        All,
        String,
        NumberDouble,
        NumberInterger,
        Digit
    }
}