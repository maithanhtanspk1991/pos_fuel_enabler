﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace POS.Controls
{
    public partial class LabelKeyPad : System.Windows.Forms.Label
    {
        public bool IsLockDot { get; set; }

        public bool IsNegative { get; set; }

        public LabelKeyPad()
        {
            IsLockDot = true;
            IsNegative = false;
        }

        protected override void OnClick(EventArgs e)
        {
            System.Windows.Forms.TextBox txt = new System.Windows.Forms.TextBox();
            txt.Location = new System.Drawing.Point(Location.X, Location.Y);
            txt.Size = new System.Drawing.Size(Size.Width, Size.Height);
            txt.Text = Text;
            txt.Parent = Parent;
            txt.TextChanged += new EventHandler(txt_TextChanged);
            FormKey.frmKeyPadNew frm = new FormKey.frmKeyPadNew(txt, IsLockDot);
            frm.IsNegative = IsNegative;
            frm.ShowDialog();
            Text = txt.Text;
            base.OnClick(e);
        }

        void txt_TextChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
            Text = txt.Text;
        }

        protected override void OnLeave(EventArgs e)
        {
            BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }
    }
}
