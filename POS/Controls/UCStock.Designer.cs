﻿namespace POS.Controls
{
    partial class UCStock
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSupplier = new System.Windows.Forms.Button();
            this.btnReceiveStock = new System.Windows.Forms.Button();
            this.btnCheckTake = new System.Windows.Forms.Button();
            this.btnHistoryReceiveStock = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSupplier
            // 
            this.btnSupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupplier.ForeColor = System.Drawing.Color.White;
            this.btnSupplier.Location = new System.Drawing.Point(519, 3);
            this.btnSupplier.Margin = new System.Windows.Forms.Padding(0);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(169, 97);
            this.btnSupplier.TabIndex = 4;
            this.btnSupplier.Text = "Supplier";
            this.btnSupplier.UseVisualStyleBackColor = false;
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // btnReceiveStock
            // 
            this.btnReceiveStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnReceiveStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReceiveStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveStock.ForeColor = System.Drawing.Color.White;
            this.btnReceiveStock.Location = new System.Drawing.Point(3, 3);
            this.btnReceiveStock.Margin = new System.Windows.Forms.Padding(0);
            this.btnReceiveStock.Name = "btnReceiveStock";
            this.btnReceiveStock.Size = new System.Drawing.Size(169, 97);
            this.btnReceiveStock.TabIndex = 0;
            this.btnReceiveStock.Text = "Receive Stock";
            this.btnReceiveStock.UseVisualStyleBackColor = false;
            this.btnReceiveStock.Click += new System.EventHandler(this.btnReceiveStock_Click);
            // 
            // btnCheckTake
            // 
            this.btnCheckTake.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCheckTake.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCheckTake.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckTake.ForeColor = System.Drawing.Color.White;
            this.btnCheckTake.Location = new System.Drawing.Point(347, 3);
            this.btnCheckTake.Margin = new System.Windows.Forms.Padding(0);
            this.btnCheckTake.Name = "btnCheckTake";
            this.btnCheckTake.Size = new System.Drawing.Size(169, 97);
            this.btnCheckTake.TabIndex = 1;
            this.btnCheckTake.Text = "Check - Take";
            this.btnCheckTake.UseVisualStyleBackColor = false;
            this.btnCheckTake.Click += new System.EventHandler(this.btnCheckTake_Click);
            // 
            // btnHistoryReceiveStock
            // 
            this.btnHistoryReceiveStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnHistoryReceiveStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHistoryReceiveStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistoryReceiveStock.ForeColor = System.Drawing.Color.White;
            this.btnHistoryReceiveStock.Location = new System.Drawing.Point(175, 3);
            this.btnHistoryReceiveStock.Margin = new System.Windows.Forms.Padding(0);
            this.btnHistoryReceiveStock.Name = "btnHistoryReceiveStock";
            this.btnHistoryReceiveStock.Size = new System.Drawing.Size(169, 97);
            this.btnHistoryReceiveStock.TabIndex = 0;
            this.btnHistoryReceiveStock.Text = "History Receive Stock";
            this.btnHistoryReceiveStock.UseVisualStyleBackColor = false;
            this.btnHistoryReceiveStock.Click += new System.EventHandler(this.btnHistoryReceiveStock_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.button1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnReceiveStock, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSupplier, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnHistoryReceiveStock, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCheckTake, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(863, 505);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(691, 3);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 97);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // UCStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UCStock";
            this.Size = new System.Drawing.Size(863, 505);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReceiveStock;
        private System.Windows.Forms.Button btnCheckTake;
        private System.Windows.Forms.Button btnSupplier;
        private System.Windows.Forms.Button btnHistoryReceiveStock;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
    }
}
