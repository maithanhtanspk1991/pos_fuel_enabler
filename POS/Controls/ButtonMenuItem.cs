﻿using System.Drawing;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class ButtonMenuItem : Button
    {
        public ButtonMenuItem()
        {
            Dock = DockStyle.Fill;
            Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            TextAlign = ContentAlignment.MiddleCenter;
            ImageAlign = ContentAlignment.TopCenter;
            FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(224, 224, 224);
            FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
        }
    }
}