﻿namespace POS.Controls
{
    partial class UCMoney
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMoney_Clear = new System.Windows.Forms.Button();
            this.btnMoney_6 = new System.Windows.Forms.Button();
            this.btnMoney_5 = new System.Windows.Forms.Button();
            this.btnMoney_3 = new System.Windows.Forms.Button();
            this.btnMoney_4 = new System.Windows.Forms.Button();
            this.btnMoney_2 = new System.Windows.Forms.Button();
            this.btnMoney_1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMoney_Clear
            // 
            this.btnMoney_Clear.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_Clear.Location = new System.Drawing.Point(0, 204);
            this.btnMoney_Clear.Name = "btnMoney_Clear";
            this.btnMoney_Clear.Size = new System.Drawing.Size(200, 69);
            this.btnMoney_Clear.TabIndex = 8;
            this.btnMoney_Clear.Tag = "";
            this.btnMoney_Clear.Text = "Clear";
            this.btnMoney_Clear.UseVisualStyleBackColor = false;
            this.btnMoney_Clear.Click += new System.EventHandler(this.btnMoney_Clear_Click);
            // 
            // btnMoney_6
            // 
            this.btnMoney_6.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_6.Location = new System.Drawing.Point(100, 136);
            this.btnMoney_6.Name = "btnMoney_6";
            this.btnMoney_6.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_6.TabIndex = 9;
            this.btnMoney_6.Tag = "";
            this.btnMoney_6.UseVisualStyleBackColor = false;
            this.btnMoney_6.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // btnMoney_5
            // 
            this.btnMoney_5.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_5.Location = new System.Drawing.Point(0, 136);
            this.btnMoney_5.Name = "btnMoney_5";
            this.btnMoney_5.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_5.TabIndex = 10;
            this.btnMoney_5.Tag = "";
            this.btnMoney_5.UseVisualStyleBackColor = false;
            this.btnMoney_5.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // btnMoney_3
            // 
            this.btnMoney_3.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_3.Location = new System.Drawing.Point(0, 68);
            this.btnMoney_3.Name = "btnMoney_3";
            this.btnMoney_3.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_3.TabIndex = 7;
            this.btnMoney_3.Tag = "";
            this.btnMoney_3.UseVisualStyleBackColor = false;
            this.btnMoney_3.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // btnMoney_4
            // 
            this.btnMoney_4.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_4.Location = new System.Drawing.Point(100, 68);
            this.btnMoney_4.Name = "btnMoney_4";
            this.btnMoney_4.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_4.TabIndex = 4;
            this.btnMoney_4.Tag = "";
            this.btnMoney_4.UseVisualStyleBackColor = false;
            this.btnMoney_4.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // btnMoney_2
            // 
            this.btnMoney_2.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_2.Location = new System.Drawing.Point(100, 0);
            this.btnMoney_2.Name = "btnMoney_2";
            this.btnMoney_2.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_2.TabIndex = 5;
            this.btnMoney_2.Tag = "";
            this.btnMoney_2.UseVisualStyleBackColor = false;
            this.btnMoney_2.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // btnMoney_1
            // 
            this.btnMoney_1.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnMoney_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoney_1.Location = new System.Drawing.Point(0, 0);
            this.btnMoney_1.Name = "btnMoney_1";
            this.btnMoney_1.Size = new System.Drawing.Size(100, 68);
            this.btnMoney_1.TabIndex = 6;
            this.btnMoney_1.Tag = "";
            this.btnMoney_1.UseVisualStyleBackColor = false;
            this.btnMoney_1.Click += new System.EventHandler(this.btnMoney_Click);
            // 
            // UCMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnMoney_Clear);
            this.Controls.Add(this.btnMoney_6);
            this.Controls.Add(this.btnMoney_5);
            this.Controls.Add(this.btnMoney_3);
            this.Controls.Add(this.btnMoney_4);
            this.Controls.Add(this.btnMoney_2);
            this.Controls.Add(this.btnMoney_1);
            this.Name = "UCMoney";
            this.Size = new System.Drawing.Size(200, 272);
            this.Load += new System.EventHandler(this.UCMoney_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMoney_Clear;
        private System.Windows.Forms.Button btnMoney_6;
        private System.Windows.Forms.Button btnMoney_5;
        private System.Windows.Forms.Button btnMoney_3;
        private System.Windows.Forms.Button btnMoney_4;
        private System.Windows.Forms.Button btnMoney_2;
        private System.Windows.Forms.Button btnMoney_1;
    }
}
