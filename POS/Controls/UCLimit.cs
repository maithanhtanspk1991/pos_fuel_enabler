﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCLimit : UserControl
    {
        public DataObject.Limit mLimit { get; set; }
        public int PageSize { get { return mLimit.PageSize; } set { mLimit.PageSize = value; } }
        public delegate void MyEventPage(DataObject.Limit limit);
        public event MyEventPage EventPage;

        private void OnEventPage(DataObject.Limit limit)
        {
            if (limit != null)
            {
                btnPage.Text = limit.Page.ToString() + "/" + limit.PageNum;
                EventPage(limit);
            }
        }
        public UCLimit()
        {
            InitializeComponent();
            mLimit = new DataObject.Limit(1, 10);
            Default();
            SetMultiLanguage();
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnBack.Text = "Quay lại";
                btnBack.Font = new Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnFirstPage.Text = "Trang đầu";
                btnGoPage.Text = "Tới trang";
                btnLastPage.Text = "Trang cuối";
                btmNext.Text = "Tiếp theo";
                btmNext.Font = new Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                return;
            }
        }
        public void PageNumReload()
        {
            btnPage.Text = mLimit.Page.ToString() + "/" + mLimit.PageNum;
        }

        public void Default()
        {
            if (mLimit != null)
            {
                mLimit.Page = 1;
                btnPage.Text = mLimit.Page.ToString() + "/" + mLimit.PageNum;
            }
        }

        private void btnLastPage_Click(object sender, EventArgs e)
        {
            if (mLimit.LastPage())
                OnEventPage(mLimit);
        }

        private void btmNext_Click(object sender, EventArgs e)
        {
            if (mLimit.Next())
                OnEventPage(mLimit);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (mLimit.Back())
                OnEventPage(mLimit);
        }

        private void btnFirstPage_Click(object sender, EventArgs e)
        {
            if (mLimit.FirstPage())
                OnEventPage(mLimit);
        }

        private void btnGoPage_Click(object sender, EventArgs e)
        {
            string str = btnPage.Text.Split('/')[0];
            mLimit.GoPage(Convert.ToInt32(str == "" ? 1 : Convert.ToInt32(str)));
            OnEventPage(mLimit);
        }

        private void btnPage_Click(object sender, EventArgs e)
        {
            Point p = new Point(btnPage.Location.X, btnPage.Location.Y);
            System.Windows.Forms.TextBox txt = new System.Windows.Forms.TextBox();
            txt.Visible = false;
            txt.Size = new System.Drawing.Size(btnPage.Size.Width, btnPage.Size.Height);
            txt.Text = btnPage.Text;
            txt.Parent = btnPage.Parent;
            txt.Location = new System.Drawing.Point(p.X, p.Y);
            txt.TextChanged += new EventHandler(txt_TextChanged);
            txt.Height = btnPage.Height;

            FormKey.frmKeyPadLimit frm = new FormKey.frmKeyPadLimit(txt, true);
            frm.IsNegative = false;
            frm.ShowDialog();
            btnPage.Text = txt.Text + "/" + mLimit.PageNum;
            btnGoPage_Click(sender, e);
        }

        void txt_TextChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
            btnPage.Text = txt.Text;
        }
    }
}
