﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCItemsChange : UserControl
    {
        private Class.MoneyFortmat money;

        public DataObject.ItemsMenu _ItemsMenu { get; set; }

        public bool _DispalyOrderChange { get; set; }

        private int DispalyOrderOld = 0;

        public SystemConfig.DBConfig mDBConfig { get; set; }

        public bool LockSellType
        {
            set
            {
                cbbSellType.Enabled = !value;
                btnGenerateBarcode.Enabled = !value;
            }
        }

        public UCItemsChange()
        {
            _ItemsMenu = new DataObject.ItemsMenu();
            InitializeComponent();
            money = new Class.MoneyFortmat(Class.MoneyFortmat.AU_TYPE);
        }

        private void UCItemsChange_Load(object sender, EventArgs e)
        {
        }

        public void LoadTypeMenu()
        {
            cbbTypeMenu.DataSource = Class.BOTypeMenu.GetAll();
            cbbTypeMenu.ValueMember = "ID";
            cbbTypeMenu.DisplayMember = "Name";
            cbbTypeMenu.SelectedIndex = 0;
        }
        public void SetValue(SystemConfig.DBConfig mDBConfig)
        {
            LoadSellType(mDBConfig);
            LoadPrinterType();
            LoadTypeMenu();
            txtItemDesc.Text = _ItemsMenu.ItemDesc;
            txtItemShort.Text = _ItemsMenu.ItemShort;
            txtBarcode.Text = _ItemsMenu.Barcode;
            txtSellSize.Text = _ItemsMenu.SellSize.ToString();
            cbbSellType.SelectedValue = _ItemsMenu.SellType.ToString();
            cbbTypeMenu.SelectedValue = _ItemsMenu.TypeMenu;
            cbbPrinterType.SelectedValue = _ItemsMenu.PrinterType.ToString();
            txtDisplayOrder.Text = _ItemsMenu.ShowDisplay.ToString();
            txtSizeItem.Text = _ItemsMenu.SizeItem.ToString(); ;
            txtUnitPrice.Text = money.Format2(_ItemsMenu.UnitPrice);
            txtGST.Text = _ItemsMenu.Gst.ToString();
            cbIsFuel.Checked = _ItemsMenu.IsFuel;
            cbVisual.Checked = _ItemsMenu.Visual;
            cbEnableFuelDiscount.Checked = _ItemsMenu.EnableFuelDiscount;
            DispalyOrderOld = _ItemsMenu.ShowDisplay;
        }

        private void LoadSellType(SystemConfig.DBConfig mDBConfig)
        {
            cbbSellType.DataSource = BusinessObject.BOSellType.GetType(mDBConfig);
            cbbSellType.ValueMember = "Value";
            cbbSellType.DisplayMember = "Display";
            cbbSellType.SelectedIndex = 0;
        }

        private void LoadPrinterType()
        {
            cbbPrinterType.DataSource = BusinessObject.BOPrintType.Get();
            cbbPrinterType.ValueMember = "Value";
            cbbPrinterType.DisplayMember = "Display";
            cbbPrinterType.SelectedIndex = 0;
        }

        public void GetValue()
        {
            _ItemsMenu.ItemDesc = txtItemDesc.Text;
            _ItemsMenu.ItemShort = txtItemShort.Text;
            _ItemsMenu.Barcode = txtBarcode.Text;
            _ItemsMenu.SellSize = Convert.ToInt32(txtSellSize.Text.Trim());
            _ItemsMenu.SizeItem = Convert.ToInt32(txtSizeItem.Text.Trim());
            _ItemsMenu.SellType = Convert.ToInt32(cbbSellType.SelectedValue.ToString());
            _ItemsMenu.TypeMenu = Convert.ToInt32(cbbTypeMenu.SelectedValue.ToString());
            _ItemsMenu.PrinterType = Convert.ToInt32(cbbPrinterType.SelectedValue.ToString());
            _ItemsMenu.ShowDisplay = Convert.ToInt32(txtDisplayOrder.Text.Trim());
            _ItemsMenu.UnitPrice = Convert.ToDouble(txtUnitPrice.Text.Trim()) * 1000;
            _ItemsMenu.Gst = Convert.ToInt32(txtGST.Text.Trim());
            _ItemsMenu.Visual = cbVisual.Checked;
            _ItemsMenu.IsFuel = cbIsFuel.Checked;
            _ItemsMenu.EnableFuelDiscount = cbEnableFuelDiscount.Checked;
            if (_ItemsMenu.ShowDisplay != DispalyOrderOld)
                _DispalyOrderChange = true;
        }

        public void Clear()
        {
            _ItemsMenu = null;
            _ItemsMenu = new DataObject.ItemsMenu();
            _ItemsMenu.Visual = true;
            txtItemDesc.Text = "";
            txtItemShort.Text = "";
            txtBarcode.Text = "";
            txtSellSize.Text = "0";
            txtDisplayOrder.Text = "1";
            txtUnitPrice.Text = "0";
            txtGST.Text = "10";
            cbIsFuel.Checked = false;
        }

        private void txtItemShort_TextChanged(object sender, EventArgs e)
        {
            if (txtItemDesc.Text == "")
                txtItemDesc.Text = txtItemShort.Text;
        }

        public bool CheckInput()
        {
            bool result = true;
            if (txtItemShort.Text == "")
            {
                lbStatus.Text = "Item short is not empty";
                result = false;
            }
            else if (txtItemDesc.Text == "")
            {
                lbStatus.Text = "Item desc is not empty";
                result = false;
            }
            else if (txtUnitPrice.Text == "")
            {
                txtUnitPrice.Text = "0";
                lbStatus.Text = "Unit price is not empty";
                result = false;
            }
            else if (txtDisplayOrder.Text == "")
            {
                txtDisplayOrder.Text = _ItemsMenu.DisplayOrder.ToString();
                lbStatus.Text = "Display order is not empty";
                result = false;
            }
            else if (txtGST.Text == "")
            {
                txtGST.Text = "10";
                lbStatus.Text = "GST order is not empty";
                result = false;
            }
            else if (txtSellSize.Text == "")
            {
                lbStatus.Text = "Sell Size is not empty";
                result = false;
            }
            else if (!(Convert.ToInt32(txtSellSize.Text) > 0))
            {
                lbStatus.Text = "Sell Size Invalid";
                result = false;
            }
            return result;
        }

        private void cbbSellType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbSellType.Items.Count > 0)
            {
                DataObject.SellTypeObject _SellTypeObject = (DataObject.SellTypeObject)cbbSellType.SelectedItem;
                if (_SellTypeObject != null)
                {
                    txtSellSize.Text = _SellTypeObject.SellSize.ToString();
                    //LockSellSize(_SellTypeObject.SellType);
                }
            }
        }

        private void txtUnitPrice_Click(object sender, EventArgs e)
        {
        }

        private void LockSellSize(DataObject.SellType Type)
        {
            //switch (Type)
            //{
            //    case DataObject.SellType.Each:
            //        txtSellSize.Enabled = false;
            //        break;

            //    case DataObject.SellType.Kg:
            //        txtSellSize.Enabled = true;
            //        break;

            //    case DataObject.SellType.Mls:
            //        txtSellSize.Enabled = true;
            //        break;
            //}
        }

        private void btnGenerateBarcode_Click(object sender, EventArgs e)
        {
            txtBarcode.Text = BusinessObject.BOItemsMenu.GetBarcode(mDBConfig); ;
            cbVisual.Checked = false;
        }

        private void cbbTypeMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbTypeMenu.Items.Count > 0)
            {
                Class.TypeMenu tm = (Class.TypeMenu)cbbTypeMenu.SelectedItem;
                if (tm != null)
                    txtSizeItem.Enabled = tm.IsSize;
            }
        }
    }
}