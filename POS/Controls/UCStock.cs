﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCStock : UserControl
    {
        private SystemConfig.DBConfig mDBConfig;
        public Barcode.SerialPort mSerialPort;
        public DataObject.Transit mTransit;

        public UCStock(SystemConfig.DBConfig dBConfig, Barcode.SerialPort serialPort, DataObject.Transit transit)
        {
            InitializeComponent();
            SetMutiLanguage();
            mDBConfig = dBConfig;
            mSerialPort = serialPort;
            mTransit = transit;
        }

        private void SetMutiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnReceiveStock.Text = "Nhận Kho";
                btnHistoryReceiveStock.Text = "Lịch sử nhận kho";
                btnCheckTake.Text = "Kiểm tra - Take";
                btnSupplier.Text = "Nhà cung cấp";
                return;
            }
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            POS.Stock.frmSupplier frmSupplier = new POS.Stock.frmSupplier(mDBConfig);
            frmSupplier.ShowDialog();
        }

        private void btnReceiveStock_Click(object sender, EventArgs e)
        {
            POS.Stock.frmReceiveStock frmReceiveStock = new Stock.frmReceiveStock(mDBConfig, mSerialPort, mTransit);
            frmReceiveStock.ShowDialog();
        }

        private void btnHistoryReceiveStock_Click(object sender, EventArgs e)
        {
            POS.Stock.frmHistoryReceiveStock frmHistoryReceiveStock = new Stock.frmHistoryReceiveStock(mDBConfig);
            frmHistoryReceiveStock.ShowDialog();
        }

        private void btnCheckTake_Click(object sender, EventArgs e)
        {
            POS.Stock.frmStockCheck frmStockCheck = new POS.Stock.frmStockCheck(mDBConfig, mSerialPort, mTransit);
            frmStockCheck.ShowDialog();
        }
    }
}