﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public enum TypeNumber
    {
        Number,
        Decimal,
        Text
    }

    public partial class TextBoxBecas : System.Windows.Forms.TextBox
    {
        public TypeNumber typeNumber = TypeNumber.Text;

        public TypeNumber _TypeNumber
        {
            get { return typeNumber; }
            set { typeNumber = value; }
        }

        private string statusError = "Invalid";
        private int precision = 2;

        public int _Precision
        {
            get { return precision; }
            set { precision = value; }
        }

        public string _StatusError
        {
            get { return statusError; }
            set { statusError = value; }
        }

        public TextBoxBecas()
        {
            BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            _TypeNumber = TypeNumber.Text;
        }

        public POS.UCkeypad _Keypad { get; set; }

        public Label _LabelStatusError { get; set; }

        protected override void OnEnter(EventArgs e)
        {
            if (_Keypad != null)
            {
                _Keypad.txtResult = this;
            }
            BackColor = System.Drawing.Color.White;
            base.OnEnter(e);
        }

        protected override void OnKeyPress(System.Windows.Forms.KeyPressEventArgs e)
        {
            if (_LabelStatusError != null)
                _LabelStatusError.Text = "";
            switch (_TypeNumber)
            {
                case TypeNumber.Number:
                    if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
                    {
                        if (_LabelStatusError != null)
                            _LabelStatusError.Text = _StatusError;
                        e.Handled = true;
                    }
                    break;

                case TypeNumber.Decimal:
                    if (e.KeyChar == (char)46 && TextLength == 0)
                        e.Handled = true;
                    if (e.KeyChar == (char)46 && Text.IndexOf('.') > 0)
                    {
                        e.Handled = true;
                    }
                    else if (e.KeyChar != (char)8 && Text.IndexOf('.') > 0 && TextLength - Text.IndexOf('.') > _Precision)
                    {
                        if (_LabelStatusError != null)
                            _LabelStatusError.Text = "Precision on numbers is " + _Precision.ToString();
                        e.Handled = true;
                    }
                    else if (!Char.IsControl(e.KeyChar))
                    {
                        if (_LabelStatusError != null)
                            _LabelStatusError.Text = _StatusError;
                        e.Handled = true;
                    }
                    break;

                case TypeNumber.Text:
                    break;

                default:
                    break;
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            BackColor = System.Drawing.Color.FromArgb(185, 230, 255);
            base.OnLeave(e);
        }
    }
}