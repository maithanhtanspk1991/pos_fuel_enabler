﻿using System;
using System.Windows.Forms;

namespace POS.Controls
{
    public partial class UCMoney : UserControl
    {
        public string Money_1 { get; set; }

        public string Money_2 { get; set; }

        public string Money_3 { get; set; }

        public string Money_4 { get; set; }

        public string Money_5 { get; set; }

        public string Money_6 { get; set; }

        public TextAlignCurency CurencyTextAlign { get; set; }

        public string Curency { get; set; }

        public TextBox txtResult { get; set; }

        public string Result;
        private Class.ReadConfig mReadConfig = new Class.ReadConfig();

        public UCMoney()
        {
            Money_1 = "500";
            Money_2 = "200";
            Money_3 = "100";
            Money_4 = "50";
            Money_5 = "20";
            Money_6 = "10";
            Curency = "$";
            CurencyTextAlign = TextAlignCurency.Left;
            InitializeComponent();
        }

        private void UCMoney_Load(object sender, EventArgs e)
        {
            LoadShowText();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                btnMoney_Clear.Text = "Xóa";
                return;
            }
        }

        private void LoadShowText()
        {
            btnMoney_1.Tag = Money_1;
            btnMoney_2.Tag = Money_2;
            btnMoney_3.Tag = Money_3;
            btnMoney_4.Tag = Money_4;
            btnMoney_5.Tag = Money_5;
            btnMoney_6.Tag = Money_6;

            btnMoney_1.Text = ShowText(Money_1);
            btnMoney_2.Text = ShowText(Money_2);
            btnMoney_3.Text = ShowText(Money_3);
            btnMoney_4.Text = ShowText(Money_4);
            btnMoney_5.Text = ShowText(Money_5);
            btnMoney_6.Text = ShowText(Money_6);
        }

        private string ShowText(string str)
        {
            if (CurencyTextAlign == TextAlignCurency.Left)
                return Curency + str;
            else
                return str + Curency;
        }

        private void btnMoney_Click(object sender, EventArgs e)
        {
            if (txtResult == null)
            {
                return;
            }
            if (txtResult.Text != "")
            {
                double Max = -1;
                if (txtResult is POS.Controls.TextBoxPOSInto)
                {
                    POS.Controls.TextBoxPOSInto txt = (POS.Controls.TextBoxPOSInto)txtResult;
                    if (txt._MaxNumber > -1)
                    {
                        Max = txt._MaxNumber;
                    }
                }
                Double Money = Convert.ToDouble(txtResult.Text);
                Button btn = (Button)sender;
                txtResult.Focus();
                Money += Convert.ToDouble(btn.Tag.ToString());
                if (Max >= Money || Max == -1)
                {
                    txtResult.Text = "";
                    SendKeys.Send(Money.ToString());
                }
            }
            else
            {
                Button btn = (Button)sender;
                txtResult.Focus();
                SendKeys.Send(btn.Tag.ToString());
            }
        }

        private void btnMoney_Clear_Click(object sender, EventArgs e)
        {
            txtResult.Focus();
            txtResult.Text = "";
            SendKeys.Send("");
        }
    }

    public enum TextAlignCurency
    {
        Left, Right
    }
}