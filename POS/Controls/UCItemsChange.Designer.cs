﻿namespace POS.Controls
{
    partial class UCItemsChange
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbVisual = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnGenerateBarcode = new System.Windows.Forms.Button();
            this.lbStatus = new System.Windows.Forms.Label();
            this.cbbTypeMenu = new System.Windows.Forms.ComboBox();
            this.cbbPrinterType = new System.Windows.Forms.ComboBox();
            this.cbbSellType = new System.Windows.Forms.ComboBox();
            this.txtDisplayOrder = new POS.Controls.TextBoxPOSKeyPad();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSellSize = new POS.Controls.TextBoxPOSKeyPad();
            this.txtGST = new POS.Controls.TextBoxPOSKeyPad();
            this.txtSizeItem = new POS.Controls.TextBoxPOSKeyPad();
            this.txtUnitPrice = new POS.Controls.TextBoxPOSKeyPad();
            this.txtBarcode = new POS.Controls.TextBoxPOSKeyPad();
            this.txtItemDesc = new POS.Controls.TextBoxPOSKeyBoard();
            this.txtItemShort = new POS.Controls.TextBoxPOSKeyBoard();
            this.cbVisual = new System.Windows.Forms.CheckBox();
            this.cbEnableFuelDiscount = new System.Windows.Forms.CheckBox();
            this.cbIsFuel = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.lbSize);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lbVisual);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(193, 541);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 52;
            this.label1.Text = "Barcode";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 436);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 20);
            this.label11.TabIndex = 50;
            this.label11.Text = "Print";
            this.label11.Visible = false;
            // 
            // lbSize
            // 
            this.lbSize.AutoSize = true;
            this.lbSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSize.Location = new System.Drawing.Point(10, 310);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(44, 20);
            this.lbSize.TabIndex = 50;
            this.lbSize.Text = "Size";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 272);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 20);
            this.label7.TabIndex = 50;
            this.label7.Text = "Type Menu";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 413);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(181, 20);
            this.label13.TabIndex = 50;
            this.label13.Text = "Enable Fuel Discount";
            // 
            // lbVisual
            // 
            this.lbVisual.AutoSize = true;
            this.lbVisual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVisual.Location = new System.Drawing.Point(10, 383);
            this.lbVisual.Name = "lbVisual";
            this.lbVisual.Size = new System.Drawing.Size(58, 20);
            this.lbVisual.TabIndex = 50;
            this.lbVisual.Text = "Visual";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 20);
            this.label6.TabIndex = 49;
            this.label6.Text = "Sell Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 20);
            this.label8.TabIndex = 48;
            this.label8.Text = "GST";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 346);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 20);
            this.label5.TabIndex = 48;
            this.label5.Text = "Display Order";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 47;
            this.label4.Text = "Sell Size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 46;
            this.label3.Text = "Unit Price";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 20);
            this.label2.TabIndex = 45;
            this.label2.Text = "Item Desc";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 20);
            this.label10.TabIndex = 44;
            this.label10.Text = "Item Short";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(41, 383);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 20);
            this.label12.TabIndex = 50;
            this.label12.Text = "Is Fuel";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnGenerateBarcode);
            this.panel2.Controls.Add(this.lbStatus);
            this.panel2.Controls.Add(this.cbbTypeMenu);
            this.panel2.Controls.Add(this.cbbPrinterType);
            this.panel2.Controls.Add(this.cbbSellType);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtDisplayOrder);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtSellSize);
            this.panel2.Controls.Add(this.txtGST);
            this.panel2.Controls.Add(this.txtSizeItem);
            this.panel2.Controls.Add(this.txtUnitPrice);
            this.panel2.Controls.Add(this.txtBarcode);
            this.panel2.Controls.Add(this.txtItemDesc);
            this.panel2.Controls.Add(this.txtItemShort);
            this.panel2.Controls.Add(this.cbVisual);
            this.panel2.Controls.Add(this.cbEnableFuelDiscount);
            this.panel2.Controls.Add(this.cbIsFuel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(193, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(346, 541);
            this.panel2.TabIndex = 1;
            // 
            // btnGenerateBarcode
            // 
            this.btnGenerateBarcode.BackColor = System.Drawing.Color.Fuchsia;
            this.btnGenerateBarcode.FlatAppearance.BorderSize = 0;
            this.btnGenerateBarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateBarcode.ForeColor = System.Drawing.Color.White;
            this.btnGenerateBarcode.Location = new System.Drawing.Point(213, 81);
            this.btnGenerateBarcode.Name = "btnGenerateBarcode";
            this.btnGenerateBarcode.Size = new System.Drawing.Size(127, 31);
            this.btnGenerateBarcode.TabIndex = 58;
            this.btnGenerateBarcode.Text = "Generate Barcode";
            this.btnGenerateBarcode.UseVisualStyleBackColor = false;
            this.btnGenerateBarcode.Click += new System.EventHandler(this.btnGenerateBarcode_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Location = new System.Drawing.Point(0, 512);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(346, 29);
            this.lbStatus.TabIndex = 57;
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbbTypeMenu
            // 
            this.cbbTypeMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTypeMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTypeMenu.FormattingEnabled = true;
            this.cbbTypeMenu.Location = new System.Drawing.Point(10, 266);
            this.cbbTypeMenu.Name = "cbbTypeMenu";
            this.cbbTypeMenu.Size = new System.Drawing.Size(255, 33);
            this.cbbTypeMenu.TabIndex = 56;
            this.cbbTypeMenu.SelectedIndexChanged += new System.EventHandler(this.cbbTypeMenu_SelectedIndexChanged);
            // 
            // cbbPrinterType
            // 
            this.cbbPrinterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbPrinterType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPrinterType.FormattingEnabled = true;
            this.cbbPrinterType.Location = new System.Drawing.Point(10, 436);
            this.cbbPrinterType.Name = "cbbPrinterType";
            this.cbbPrinterType.Size = new System.Drawing.Size(255, 33);
            this.cbbPrinterType.TabIndex = 56;
            this.cbbPrinterType.Visible = false;
            // 
            // cbbSellType
            // 
            this.cbbSellType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbSellType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSellType.FormattingEnabled = true;
            this.cbbSellType.Location = new System.Drawing.Point(10, 155);
            this.cbbSellType.Name = "cbbSellType";
            this.cbbSellType.Size = new System.Drawing.Size(255, 33);
            this.cbbSellType.TabIndex = 56;
            this.cbbSellType.SelectedIndexChanged += new System.EventHandler(this.cbbSellType_SelectedIndexChanged);
            // 
            // txtDisplayOrder
            // 
            this.txtDisplayOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtDisplayOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplayOrder.IsLockDot = false;
            this.txtDisplayOrder.IsNegative = false;
            this.txtDisplayOrder.Location = new System.Drawing.Point(10, 341);
            this.txtDisplayOrder.Name = "txtDisplayOrder";
            this.txtDisplayOrder.Size = new System.Drawing.Size(255, 31);
            this.txtDisplayOrder.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(81, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 20);
            this.label9.TabIndex = 48;
            this.label9.Text = "%";
            // 
            // txtSellSize
            // 
            this.txtSellSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtSellSize.Enabled = false;
            this.txtSellSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSellSize.IsLockDot = false;
            this.txtSellSize.IsNegative = false;
            this.txtSellSize.Location = new System.Drawing.Point(10, 194);
            this.txtSellSize.Name = "txtSellSize";
            this.txtSellSize.Size = new System.Drawing.Size(255, 31);
            this.txtSellSize.TabIndex = 54;
            // 
            // txtGST
            // 
            this.txtGST.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtGST.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGST.IsLockDot = false;
            this.txtGST.IsNegative = false;
            this.txtGST.Location = new System.Drawing.Point(10, 231);
            this.txtGST.Name = "txtGST";
            this.txtGST.Size = new System.Drawing.Size(66, 31);
            this.txtGST.TabIndex = 55;
            this.txtGST.Click += new System.EventHandler(this.txtUnitPrice_Click);
            // 
            // txtSizeItem
            // 
            this.txtSizeItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtSizeItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSizeItem.IsLockDot = false;
            this.txtSizeItem.IsNegative = false;
            this.txtSizeItem.Location = new System.Drawing.Point(10, 305);
            this.txtSizeItem.Name = "txtSizeItem";
            this.txtSizeItem.Size = new System.Drawing.Size(255, 31);
            this.txtSizeItem.TabIndex = 55;
            this.txtSizeItem.Click += new System.EventHandler(this.txtUnitPrice_Click);
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtUnitPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnitPrice.IsLockDot = false;
            this.txtUnitPrice.IsNegative = false;
            this.txtUnitPrice.Location = new System.Drawing.Point(10, 118);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(255, 31);
            this.txtUnitPrice.TabIndex = 55;
            this.txtUnitPrice.Click += new System.EventHandler(this.txtUnitPrice_Click);
            // 
            // txtBarcode
            // 
            this.txtBarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtBarcode.Enabled = false;
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.IsLockDot = false;
            this.txtBarcode.IsNegative = false;
            this.txtBarcode.Location = new System.Drawing.Point(10, 81);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(197, 31);
            this.txtBarcode.TabIndex = 50;
            // 
            // txtItemDesc
            // 
            this.txtItemDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtItemDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemDesc.Location = new System.Drawing.Point(10, 44);
            this.txtItemDesc.Name = "txtItemDesc";
            this.txtItemDesc.Size = new System.Drawing.Size(330, 31);
            this.txtItemDesc.TabIndex = 50;
            // 
            // txtItemShort
            // 
            this.txtItemShort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemShort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.txtItemShort.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemShort.Location = new System.Drawing.Point(10, 7);
            this.txtItemShort.Name = "txtItemShort";
            this.txtItemShort.Size = new System.Drawing.Size(330, 31);
            this.txtItemShort.TabIndex = 51;
            this.txtItemShort.TextChanged += new System.EventHandler(this.txtItemShort_TextChanged);
            // 
            // cbVisual
            // 
            this.cbVisual.AutoSize = true;
            this.cbVisual.Location = new System.Drawing.Point(10, 386);
            this.cbVisual.Name = "cbVisual";
            this.cbVisual.Size = new System.Drawing.Size(15, 14);
            this.cbVisual.TabIndex = 49;
            this.cbVisual.UseVisualStyleBackColor = true;
            // 
            // cbEnableFuelDiscount
            // 
            this.cbEnableFuelDiscount.AutoSize = true;
            this.cbEnableFuelDiscount.Location = new System.Drawing.Point(10, 416);
            this.cbEnableFuelDiscount.Name = "cbEnableFuelDiscount";
            this.cbEnableFuelDiscount.Size = new System.Drawing.Size(15, 14);
            this.cbEnableFuelDiscount.TabIndex = 49;
            this.cbEnableFuelDiscount.UseVisualStyleBackColor = true;
            // 
            // cbIsFuel
            // 
            this.cbIsFuel.AutoSize = true;
            this.cbIsFuel.Location = new System.Drawing.Point(121, 386);
            this.cbIsFuel.Name = "cbIsFuel";
            this.cbIsFuel.Size = new System.Drawing.Size(15, 14);
            this.cbIsFuel.TabIndex = 49;
            this.cbIsFuel.UseVisualStyleBackColor = true;
            // 
            // UCItemsChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "UCItemsChange";
            this.Size = new System.Drawing.Size(539, 541);
            this.Load += new System.EventHandler(this.UCItemsChange_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private TextBoxPOSKeyPad txtDisplayOrder;
        private TextBoxPOSKeyPad txtSellSize;
        private TextBoxPOSKeyPad txtUnitPrice;
        private TextBoxPOSKeyBoard txtItemDesc;
        private TextBoxPOSKeyBoard txtItemShort;
        private System.Windows.Forms.Label label1;
        private TextBoxPOSKeyPad txtBarcode;
        private System.Windows.Forms.ComboBox cbbSellType;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label label8;
        private TextBoxPOSKeyPad txtGST;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbbPrinterType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox cbEnableFuelDiscount;
        private System.Windows.Forms.CheckBox cbIsFuel;
        private System.Windows.Forms.Label lbVisual;
        private System.Windows.Forms.CheckBox cbVisual;
        private System.Windows.Forms.Button btnGenerateBarcode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.ComboBox cbbTypeMenu;
        private TextBoxPOSKeyPad txtSizeItem;
    }
}
