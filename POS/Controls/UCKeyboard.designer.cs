﻿namespace POS.Controls
{
    partial class UCKeyboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.btndel = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnenter = new System.Windows.Forms.Button();
            this.btnp = new System.Windows.Forms.Button();
            this.btno = new System.Windows.Forms.Button();
            this.btni = new System.Windows.Forms.Button();
            this.btnu = new System.Windows.Forms.Button();
            this.btny = new System.Windows.Forms.Button();
            this.btnt = new System.Windows.Forms.Button();
            this.btnr = new System.Windows.Forms.Button();
            this.btne = new System.Windows.Forms.Button();
            this.btnw = new System.Windows.Forms.Button();
            this.btnq = new System.Windows.Forms.Button();
            this.btndot = new System.Windows.Forms.Button();
            this.btnl = new System.Windows.Forms.Button();
            this.btnk = new System.Windows.Forms.Button();
            this.btnj = new System.Windows.Forms.Button();
            this.btnh = new System.Windows.Forms.Button();
            this.btng = new System.Windows.Forms.Button();
            this.btnf = new System.Windows.Forms.Button();
            this.btnd = new System.Windows.Forms.Button();
            this.btns = new System.Windows.Forms.Button();
            this.btna = new System.Windows.Forms.Button();
            this.btnspace = new System.Windows.Forms.Button();
            this.btnm = new System.Windows.Forms.Button();
            this.btnn = new System.Windows.Forms.Button();
            this.btnb = new System.Windows.Forms.Button();
            this.btnv = new System.Windows.Forms.Button();
            this.btnc = new System.Windows.Forms.Button();
            this.btnx = new System.Windows.Forms.Button();
            this.btnz = new System.Windows.Forms.Button();
            this.txtresult = new System.Windows.Forms.TextBox();
            this.btnshift = new System.Windows.Forms.Button();
            this.btncapslock = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnnhonmo = new System.Windows.Forms.Button();
            this.btnnhondong = new System.Windows.Forms.Button();
            this.btnhoac = new System.Windows.Forms.Button();
            this.btnnhohon = new System.Windows.Forms.Button();
            this.btnvuongmo = new System.Windows.Forms.Button();
            this.btntru = new System.Windows.Forms.Button();
            this.btncong = new System.Windows.Forms.Button();
            this.btndauhoi = new System.Windows.Forms.Button();
            this.btnvuongdong = new System.Windows.Forms.Button();
            this.btnhaicham = new System.Windows.Forms.Button();
            this.btnchamphay = new System.Windows.Forms.Button();
            this.btnchia = new System.Windows.Forms.Button();
            this.btnphay = new System.Windows.Forms.Button();
            this.btnthan = new System.Windows.Forms.Button();
            this.btnacong = new System.Windows.Forms.Button();
            this.btnthang = new System.Windows.Forms.Button();
            this.btnbang = new System.Windows.Forms.Button();
            this.btnphantram = new System.Windows.Forms.Button();
            this.btndola = new System.Windows.Forms.Button();
            this.btntronmo = new System.Windows.Forms.Button();
            this.btnsao = new System.Windows.Forms.Button();
            this.btnmu = new System.Windows.Forms.Button();
            this.btntrondong = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(686, 242);
            this.btn1.Margin = new System.Windows.Forms.Padding(1);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 60);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(748, 242);
            this.btn2.Margin = new System.Windows.Forms.Padding(1);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 60);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(686, 180);
            this.btn4.Margin = new System.Windows.Forms.Padding(1);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(60, 60);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(810, 242);
            this.btn3.Margin = new System.Windows.Forms.Padding(1);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(60, 60);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(748, 118);
            this.btn8.Margin = new System.Windows.Forms.Padding(1);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(60, 60);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(686, 118);
            this.btn7.Margin = new System.Windows.Forms.Padding(1);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(60, 60);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(810, 180);
            this.btn6.Margin = new System.Windows.Forms.Padding(1);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(60, 60);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(748, 180);
            this.btn5.Margin = new System.Windows.Forms.Padding(1);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 60);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btnexit
            // 
            this.btnexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Location = new System.Drawing.Point(624, 304);
            this.btnexit.Margin = new System.Windows.Forms.Padding(1);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 60);
            this.btnexit.TabIndex = 11;
            this.btnexit.Text = "QUIT";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btndel
            // 
            this.btndel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndel.Location = new System.Drawing.Point(624, 56);
            this.btndel.Margin = new System.Windows.Forms.Padding(1);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(60, 60);
            this.btndel.TabIndex = 10;
            this.btndel.Text = "DEL";
            this.btndel.UseVisualStyleBackColor = true;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(748, 304);
            this.btn0.Margin = new System.Windows.Forms.Padding(1);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(60, 60);
            this.btn0.TabIndex = 9;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(810, 118);
            this.btn9.Margin = new System.Windows.Forms.Padding(1);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(60, 60);
            this.btn9.TabIndex = 8;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btnenter
            // 
            this.btnenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenter.Location = new System.Drawing.Point(624, 242);
            this.btnenter.Margin = new System.Windows.Forms.Padding(1);
            this.btnenter.Name = "btnenter";
            this.btnenter.Size = new System.Drawing.Size(60, 60);
            this.btnenter.TabIndex = 22;
            this.btnenter.Text = "ENTER";
            this.btnenter.UseVisualStyleBackColor = true;
            this.btnenter.Click += new System.EventHandler(this.btnenter_Click);
            // 
            // btnp
            // 
            this.btnp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnp.Location = new System.Drawing.Point(562, 118);
            this.btnp.Margin = new System.Windows.Forms.Padding(1);
            this.btnp.Name = "btnp";
            this.btnp.Size = new System.Drawing.Size(60, 60);
            this.btnp.TabIndex = 21;
            this.btnp.Text = "P";
            this.btnp.UseVisualStyleBackColor = true;
            this.btnp.Click += new System.EventHandler(this.btnp_Click);
            // 
            // btno
            // 
            this.btno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btno.Location = new System.Drawing.Point(500, 118);
            this.btno.Margin = new System.Windows.Forms.Padding(1);
            this.btno.Name = "btno";
            this.btno.Size = new System.Drawing.Size(60, 60);
            this.btno.TabIndex = 20;
            this.btno.Text = "O";
            this.btno.UseVisualStyleBackColor = true;
            this.btno.Click += new System.EventHandler(this.btno_Click);
            // 
            // btni
            // 
            this.btni.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btni.Location = new System.Drawing.Point(438, 118);
            this.btni.Margin = new System.Windows.Forms.Padding(1);
            this.btni.Name = "btni";
            this.btni.Size = new System.Drawing.Size(60, 60);
            this.btni.TabIndex = 19;
            this.btni.Text = "I";
            this.btni.UseVisualStyleBackColor = true;
            this.btni.Click += new System.EventHandler(this.btni_Click);
            // 
            // btnu
            // 
            this.btnu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnu.Location = new System.Drawing.Point(376, 118);
            this.btnu.Margin = new System.Windows.Forms.Padding(1);
            this.btnu.Name = "btnu";
            this.btnu.Size = new System.Drawing.Size(60, 60);
            this.btnu.TabIndex = 18;
            this.btnu.Text = "U";
            this.btnu.UseVisualStyleBackColor = true;
            this.btnu.Click += new System.EventHandler(this.btnu_Click);
            // 
            // btny
            // 
            this.btny.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btny.Location = new System.Drawing.Point(314, 118);
            this.btny.Margin = new System.Windows.Forms.Padding(1);
            this.btny.Name = "btny";
            this.btny.Size = new System.Drawing.Size(60, 60);
            this.btny.TabIndex = 17;
            this.btny.Text = "Y";
            this.btny.UseVisualStyleBackColor = true;
            this.btny.Click += new System.EventHandler(this.btny_Click);
            // 
            // btnt
            // 
            this.btnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnt.Location = new System.Drawing.Point(252, 118);
            this.btnt.Margin = new System.Windows.Forms.Padding(1);
            this.btnt.Name = "btnt";
            this.btnt.Size = new System.Drawing.Size(60, 60);
            this.btnt.TabIndex = 16;
            this.btnt.Text = "T";
            this.btnt.UseVisualStyleBackColor = true;
            this.btnt.Click += new System.EventHandler(this.btnt_Click);
            // 
            // btnr
            // 
            this.btnr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnr.Location = new System.Drawing.Point(190, 118);
            this.btnr.Margin = new System.Windows.Forms.Padding(1);
            this.btnr.Name = "btnr";
            this.btnr.Size = new System.Drawing.Size(60, 60);
            this.btnr.TabIndex = 15;
            this.btnr.Text = "R";
            this.btnr.UseVisualStyleBackColor = true;
            this.btnr.Click += new System.EventHandler(this.btnr_Click);
            // 
            // btne
            // 
            this.btne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btne.Location = new System.Drawing.Point(128, 118);
            this.btne.Margin = new System.Windows.Forms.Padding(1);
            this.btne.Name = "btne";
            this.btne.Size = new System.Drawing.Size(60, 60);
            this.btne.TabIndex = 14;
            this.btne.Text = "E";
            this.btne.UseVisualStyleBackColor = true;
            this.btne.Click += new System.EventHandler(this.btne_Click);
            // 
            // btnw
            // 
            this.btnw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnw.Location = new System.Drawing.Point(66, 118);
            this.btnw.Margin = new System.Windows.Forms.Padding(1);
            this.btnw.Name = "btnw";
            this.btnw.Size = new System.Drawing.Size(60, 60);
            this.btnw.TabIndex = 13;
            this.btnw.Text = "W";
            this.btnw.UseVisualStyleBackColor = true;
            this.btnw.Click += new System.EventHandler(this.btnw_Click);
            // 
            // btnq
            // 
            this.btnq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnq.Location = new System.Drawing.Point(4, 118);
            this.btnq.Margin = new System.Windows.Forms.Padding(1);
            this.btnq.Name = "btnq";
            this.btnq.Size = new System.Drawing.Size(60, 60);
            this.btnq.TabIndex = 12;
            this.btnq.Text = "Q";
            this.btnq.UseVisualStyleBackColor = true;
            this.btnq.Click += new System.EventHandler(this.btnq_Click);
            // 
            // btndot
            // 
            this.btndot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndot.Location = new System.Drawing.Point(686, 304);
            this.btndot.Margin = new System.Windows.Forms.Padding(1);
            this.btndot.Name = "btndot";
            this.btndot.Size = new System.Drawing.Size(60, 60);
            this.btndot.TabIndex = 33;
            this.btndot.Text = ".";
            this.btndot.UseVisualStyleBackColor = true;
            this.btndot.Click += new System.EventHandler(this.btndot_Click);
            // 
            // btnl
            // 
            this.btnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnl.Location = new System.Drawing.Point(500, 180);
            this.btnl.Margin = new System.Windows.Forms.Padding(1);
            this.btnl.Name = "btnl";
            this.btnl.Size = new System.Drawing.Size(60, 60);
            this.btnl.TabIndex = 32;
            this.btnl.Text = "L";
            this.btnl.UseVisualStyleBackColor = true;
            this.btnl.Click += new System.EventHandler(this.btnl_Click);
            // 
            // btnk
            // 
            this.btnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnk.Location = new System.Drawing.Point(438, 180);
            this.btnk.Margin = new System.Windows.Forms.Padding(1);
            this.btnk.Name = "btnk";
            this.btnk.Size = new System.Drawing.Size(60, 60);
            this.btnk.TabIndex = 31;
            this.btnk.Text = "K";
            this.btnk.UseVisualStyleBackColor = true;
            this.btnk.Click += new System.EventHandler(this.btnk_Click);
            // 
            // btnj
            // 
            this.btnj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnj.Location = new System.Drawing.Point(376, 180);
            this.btnj.Margin = new System.Windows.Forms.Padding(1);
            this.btnj.Name = "btnj";
            this.btnj.Size = new System.Drawing.Size(60, 60);
            this.btnj.TabIndex = 30;
            this.btnj.Text = "J";
            this.btnj.UseVisualStyleBackColor = true;
            this.btnj.Click += new System.EventHandler(this.btnj_Click);
            // 
            // btnh
            // 
            this.btnh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnh.Location = new System.Drawing.Point(314, 180);
            this.btnh.Margin = new System.Windows.Forms.Padding(1);
            this.btnh.Name = "btnh";
            this.btnh.Size = new System.Drawing.Size(60, 60);
            this.btnh.TabIndex = 29;
            this.btnh.Text = "H";
            this.btnh.UseVisualStyleBackColor = true;
            this.btnh.Click += new System.EventHandler(this.btnh_Click);
            // 
            // btng
            // 
            this.btng.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btng.Location = new System.Drawing.Point(252, 180);
            this.btng.Margin = new System.Windows.Forms.Padding(1);
            this.btng.Name = "btng";
            this.btng.Size = new System.Drawing.Size(60, 60);
            this.btng.TabIndex = 28;
            this.btng.Text = "G";
            this.btng.UseVisualStyleBackColor = true;
            this.btng.Click += new System.EventHandler(this.btng_Click);
            // 
            // btnf
            // 
            this.btnf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnf.Location = new System.Drawing.Point(190, 180);
            this.btnf.Margin = new System.Windows.Forms.Padding(1);
            this.btnf.Name = "btnf";
            this.btnf.Size = new System.Drawing.Size(60, 60);
            this.btnf.TabIndex = 27;
            this.btnf.Text = "F";
            this.btnf.UseVisualStyleBackColor = true;
            this.btnf.Click += new System.EventHandler(this.btnf_Click);
            // 
            // btnd
            // 
            this.btnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnd.Location = new System.Drawing.Point(128, 180);
            this.btnd.Margin = new System.Windows.Forms.Padding(1);
            this.btnd.Name = "btnd";
            this.btnd.Size = new System.Drawing.Size(60, 60);
            this.btnd.TabIndex = 26;
            this.btnd.Text = "D";
            this.btnd.UseVisualStyleBackColor = true;
            this.btnd.Click += new System.EventHandler(this.btnd_Click);
            // 
            // btns
            // 
            this.btns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btns.Location = new System.Drawing.Point(66, 180);
            this.btns.Margin = new System.Windows.Forms.Padding(1);
            this.btns.Name = "btns";
            this.btns.Size = new System.Drawing.Size(60, 60);
            this.btns.TabIndex = 25;
            this.btns.Text = "S";
            this.btns.UseVisualStyleBackColor = true;
            this.btns.Click += new System.EventHandler(this.btns_Click);
            // 
            // btna
            // 
            this.btna.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btna.Location = new System.Drawing.Point(4, 180);
            this.btna.Margin = new System.Windows.Forms.Padding(1);
            this.btna.Name = "btna";
            this.btna.Size = new System.Drawing.Size(60, 60);
            this.btna.TabIndex = 24;
            this.btna.Text = "A";
            this.btna.UseVisualStyleBackColor = true;
            this.btna.Click += new System.EventHandler(this.btna_Click);
            // 
            // btnspace
            // 
            this.btnspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnspace.Location = new System.Drawing.Point(252, 304);
            this.btnspace.Margin = new System.Windows.Forms.Padding(1);
            this.btnspace.Name = "btnspace";
            this.btnspace.Size = new System.Drawing.Size(184, 60);
            this.btnspace.TabIndex = 43;
            this.btnspace.UseVisualStyleBackColor = true;
            this.btnspace.Click += new System.EventHandler(this.btnspace_Click);
            // 
            // btnm
            // 
            this.btnm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnm.Location = new System.Drawing.Point(376, 242);
            this.btnm.Margin = new System.Windows.Forms.Padding(1);
            this.btnm.Name = "btnm";
            this.btnm.Size = new System.Drawing.Size(60, 60);
            this.btnm.TabIndex = 42;
            this.btnm.Text = "M";
            this.btnm.UseVisualStyleBackColor = true;
            this.btnm.Click += new System.EventHandler(this.btnm_Click);
            // 
            // btnn
            // 
            this.btnn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn.Location = new System.Drawing.Point(314, 242);
            this.btnn.Margin = new System.Windows.Forms.Padding(1);
            this.btnn.Name = "btnn";
            this.btnn.Size = new System.Drawing.Size(60, 60);
            this.btnn.TabIndex = 41;
            this.btnn.Text = "N";
            this.btnn.UseVisualStyleBackColor = true;
            this.btnn.Click += new System.EventHandler(this.btnn_Click);
            // 
            // btnb
            // 
            this.btnb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnb.Location = new System.Drawing.Point(252, 242);
            this.btnb.Margin = new System.Windows.Forms.Padding(1);
            this.btnb.Name = "btnb";
            this.btnb.Size = new System.Drawing.Size(60, 60);
            this.btnb.TabIndex = 40;
            this.btnb.Text = "B";
            this.btnb.UseVisualStyleBackColor = true;
            this.btnb.Click += new System.EventHandler(this.btnb_Click);
            // 
            // btnv
            // 
            this.btnv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnv.Location = new System.Drawing.Point(190, 242);
            this.btnv.Margin = new System.Windows.Forms.Padding(1);
            this.btnv.Name = "btnv";
            this.btnv.Size = new System.Drawing.Size(60, 60);
            this.btnv.TabIndex = 39;
            this.btnv.Text = "V";
            this.btnv.UseVisualStyleBackColor = true;
            this.btnv.Click += new System.EventHandler(this.btnv_Click);
            // 
            // btnc
            // 
            this.btnc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnc.Location = new System.Drawing.Point(128, 242);
            this.btnc.Margin = new System.Windows.Forms.Padding(1);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(60, 60);
            this.btnc.TabIndex = 38;
            this.btnc.Text = "C";
            this.btnc.UseVisualStyleBackColor = true;
            this.btnc.Click += new System.EventHandler(this.btnc_Click);
            // 
            // btnx
            // 
            this.btnx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnx.Location = new System.Drawing.Point(66, 242);
            this.btnx.Margin = new System.Windows.Forms.Padding(1);
            this.btnx.Name = "btnx";
            this.btnx.Size = new System.Drawing.Size(60, 60);
            this.btnx.TabIndex = 37;
            this.btnx.Text = "X";
            this.btnx.UseVisualStyleBackColor = true;
            this.btnx.Click += new System.EventHandler(this.btnx_Click);
            // 
            // btnz
            // 
            this.btnz.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnz.Location = new System.Drawing.Point(4, 242);
            this.btnz.Margin = new System.Windows.Forms.Padding(1);
            this.btnz.Name = "btnz";
            this.btnz.Size = new System.Drawing.Size(60, 60);
            this.btnz.TabIndex = 36;
            this.btnz.Text = "Z";
            this.btnz.UseVisualStyleBackColor = true;
            this.btnz.Click += new System.EventHandler(this.btnz_Click);
            // 
            // txtresult
            // 
            this.txtresult.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtresult.Location = new System.Drawing.Point(4, 3);
            this.txtresult.Name = "txtresult";
            this.txtresult.Size = new System.Drawing.Size(866, 47);
            this.txtresult.TabIndex = 48;
            // 
            // btnshift
            // 
            this.btnshift.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshift.Location = new System.Drawing.Point(4, 304);
            this.btnshift.Margin = new System.Windows.Forms.Padding(1);
            this.btnshift.Name = "btnshift";
            this.btnshift.Size = new System.Drawing.Size(122, 60);
            this.btnshift.TabIndex = 49;
            this.btnshift.Text = "Shift";
            this.btnshift.UseVisualStyleBackColor = true;
            this.btnshift.Click += new System.EventHandler(this.btnshift_Click);
            // 
            // btncapslock
            // 
            this.btncapslock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncapslock.Location = new System.Drawing.Point(500, 242);
            this.btncapslock.Margin = new System.Windows.Forms.Padding(1);
            this.btncapslock.Name = "btncapslock";
            this.btncapslock.Size = new System.Drawing.Size(60, 60);
            this.btncapslock.TabIndex = 50;
            this.btncapslock.Text = "Caps Lock";
            this.btncapslock.UseVisualStyleBackColor = true;
            this.btncapslock.Click += new System.EventHandler(this.btncapslock_Click);
            // 
            // btnclear
            // 
            this.btnclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.Location = new System.Drawing.Point(624, 118);
            this.btnclear.Margin = new System.Windows.Forms.Padding(1);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(60, 60);
            this.btnclear.TabIndex = 51;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnnhonmo
            // 
            this.btnnhonmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhonmo.Location = new System.Drawing.Point(686, 56);
            this.btnnhonmo.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhonmo.Name = "btnnhonmo";
            this.btnnhonmo.Size = new System.Drawing.Size(60, 60);
            this.btnnhonmo.TabIndex = 52;
            this.btnnhonmo.Text = "{";
            this.btnnhonmo.UseVisualStyleBackColor = true;
            this.btnnhonmo.Click += new System.EventHandler(this.btnnhonmo_Click);
            // 
            // btnnhondong
            // 
            this.btnnhondong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhondong.Location = new System.Drawing.Point(748, 56);
            this.btnnhondong.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhondong.Name = "btnnhondong";
            this.btnnhondong.Size = new System.Drawing.Size(60, 60);
            this.btnnhondong.TabIndex = 53;
            this.btnnhondong.Text = "}";
            this.btnnhondong.UseVisualStyleBackColor = true;
            this.btnnhondong.Click += new System.EventHandler(this.btnnhondong_Click);
            // 
            // btnhoac
            // 
            this.btnhoac.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhoac.Location = new System.Drawing.Point(810, 56);
            this.btnhoac.Margin = new System.Windows.Forms.Padding(1);
            this.btnhoac.Name = "btnhoac";
            this.btnhoac.Size = new System.Drawing.Size(60, 60);
            this.btnhoac.TabIndex = 54;
            this.btnhoac.Text = "|";
            this.btnhoac.UseVisualStyleBackColor = true;
            this.btnhoac.Click += new System.EventHandler(this.btnhoac_Click);
            // 
            // btnnhohon
            // 
            this.btnnhohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhohon.Location = new System.Drawing.Point(810, 304);
            this.btnnhohon.Margin = new System.Windows.Forms.Padding(1);
            this.btnnhohon.Name = "btnnhohon";
            this.btnnhohon.Size = new System.Drawing.Size(60, 60);
            this.btnnhohon.TabIndex = 55;
            this.btnnhohon.Text = "<";
            this.btnnhohon.UseVisualStyleBackColor = true;
            this.btnnhohon.Click += new System.EventHandler(this.btnnhohon_Click);
            // 
            // btnvuongmo
            // 
            this.btnvuongmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvuongmo.Location = new System.Drawing.Point(128, 304);
            this.btnvuongmo.Margin = new System.Windows.Forms.Padding(1);
            this.btnvuongmo.Name = "btnvuongmo";
            this.btnvuongmo.Size = new System.Drawing.Size(60, 60);
            this.btnvuongmo.TabIndex = 56;
            this.btnvuongmo.Text = "[";
            this.btnvuongmo.UseVisualStyleBackColor = true;
            this.btnvuongmo.Click += new System.EventHandler(this.btnvuongmo_Click);
            // 
            // btntru
            // 
            this.btntru.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntru.Location = new System.Drawing.Point(190, 304);
            this.btntru.Margin = new System.Windows.Forms.Padding(1);
            this.btntru.Name = "btntru";
            this.btntru.Size = new System.Drawing.Size(60, 60);
            this.btntru.TabIndex = 57;
            this.btntru.Text = "-";
            this.btntru.UseVisualStyleBackColor = true;
            this.btntru.Click += new System.EventHandler(this.btntru_Click);
            // 
            // btncong
            // 
            this.btncong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncong.Location = new System.Drawing.Point(438, 304);
            this.btncong.Margin = new System.Windows.Forms.Padding(1);
            this.btncong.Name = "btncong";
            this.btncong.Size = new System.Drawing.Size(60, 60);
            this.btncong.TabIndex = 58;
            this.btncong.Text = "+";
            this.btncong.UseVisualStyleBackColor = true;
            this.btncong.Click += new System.EventHandler(this.btncong_Click);
            // 
            // btndauhoi
            // 
            this.btndauhoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndauhoi.Location = new System.Drawing.Point(500, 304);
            this.btndauhoi.Margin = new System.Windows.Forms.Padding(1);
            this.btndauhoi.Name = "btndauhoi";
            this.btndauhoi.Size = new System.Drawing.Size(60, 60);
            this.btndauhoi.TabIndex = 59;
            this.btndauhoi.Text = "?";
            this.btndauhoi.UseVisualStyleBackColor = true;
            this.btndauhoi.Click += new System.EventHandler(this.btndauhoi_Click);
            // 
            // btnvuongdong
            // 
            this.btnvuongdong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvuongdong.Location = new System.Drawing.Point(562, 304);
            this.btnvuongdong.Margin = new System.Windows.Forms.Padding(1);
            this.btnvuongdong.Name = "btnvuongdong";
            this.btnvuongdong.Size = new System.Drawing.Size(60, 60);
            this.btnvuongdong.TabIndex = 60;
            this.btnvuongdong.Text = "]";
            this.btnvuongdong.UseVisualStyleBackColor = true;
            this.btnvuongdong.Click += new System.EventHandler(this.btnvuongdong_Click);
            // 
            // btnhaicham
            // 
            this.btnhaicham.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhaicham.Location = new System.Drawing.Point(562, 242);
            this.btnhaicham.Margin = new System.Windows.Forms.Padding(1);
            this.btnhaicham.Name = "btnhaicham";
            this.btnhaicham.Size = new System.Drawing.Size(60, 60);
            this.btnhaicham.TabIndex = 61;
            this.btnhaicham.Text = ":";
            this.btnhaicham.UseVisualStyleBackColor = true;
            this.btnhaicham.Click += new System.EventHandler(this.btnhaicham_Click);
            // 
            // btnchamphay
            // 
            this.btnchamphay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchamphay.Location = new System.Drawing.Point(562, 180);
            this.btnchamphay.Margin = new System.Windows.Forms.Padding(1);
            this.btnchamphay.Name = "btnchamphay";
            this.btnchamphay.Size = new System.Drawing.Size(60, 60);
            this.btnchamphay.TabIndex = 62;
            this.btnchamphay.Text = ";";
            this.btnchamphay.UseVisualStyleBackColor = true;
            this.btnchamphay.Click += new System.EventHandler(this.btnchamphay_Click);
            // 
            // btnchia
            // 
            this.btnchia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchia.Location = new System.Drawing.Point(624, 180);
            this.btnchia.Margin = new System.Windows.Forms.Padding(1);
            this.btnchia.Name = "btnchia";
            this.btnchia.Size = new System.Drawing.Size(60, 60);
            this.btnchia.TabIndex = 63;
            this.btnchia.Text = "/";
            this.btnchia.UseVisualStyleBackColor = true;
            this.btnchia.Click += new System.EventHandler(this.btnchia_Click);
            // 
            // btnphay
            // 
            this.btnphay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphay.Location = new System.Drawing.Point(438, 242);
            this.btnphay.Margin = new System.Windows.Forms.Padding(1);
            this.btnphay.Name = "btnphay";
            this.btnphay.Size = new System.Drawing.Size(60, 60);
            this.btnphay.TabIndex = 64;
            this.btnphay.Text = ",";
            this.btnphay.UseVisualStyleBackColor = true;
            this.btnphay.Click += new System.EventHandler(this.btnphay_Click);
            // 
            // btnthan
            // 
            this.btnthan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnthan.Location = new System.Drawing.Point(4, 56);
            this.btnthan.Margin = new System.Windows.Forms.Padding(1);
            this.btnthan.Name = "btnthan";
            this.btnthan.Size = new System.Drawing.Size(60, 60);
            this.btnthan.TabIndex = 65;
            this.btnthan.Text = "!";
            this.btnthan.UseVisualStyleBackColor = true;
            this.btnthan.Click += new System.EventHandler(this.btnthan_Click);
            // 
            // btnacong
            // 
            this.btnacong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnacong.Location = new System.Drawing.Point(66, 56);
            this.btnacong.Margin = new System.Windows.Forms.Padding(1);
            this.btnacong.Name = "btnacong";
            this.btnacong.Size = new System.Drawing.Size(60, 60);
            this.btnacong.TabIndex = 66;
            this.btnacong.Text = "@";
            this.btnacong.UseVisualStyleBackColor = true;
            this.btnacong.Click += new System.EventHandler(this.btnacong_Click);
            // 
            // btnthang
            // 
            this.btnthang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnthang.Location = new System.Drawing.Point(128, 56);
            this.btnthang.Margin = new System.Windows.Forms.Padding(1);
            this.btnthang.Name = "btnthang";
            this.btnthang.Size = new System.Drawing.Size(60, 60);
            this.btnthang.TabIndex = 67;
            this.btnthang.Text = "#";
            this.btnthang.UseVisualStyleBackColor = true;
            this.btnthang.Click += new System.EventHandler(this.btnthang_Click);
            // 
            // btnbang
            // 
            this.btnbang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbang.Location = new System.Drawing.Point(314, 56);
            this.btnbang.Margin = new System.Windows.Forms.Padding(1);
            this.btnbang.Name = "btnbang";
            this.btnbang.Size = new System.Drawing.Size(60, 60);
            this.btnbang.TabIndex = 70;
            this.btnbang.Text = "=";
            this.btnbang.UseVisualStyleBackColor = true;
            this.btnbang.Click += new System.EventHandler(this.btnbang_Click);
            // 
            // btnphantram
            // 
            this.btnphantram.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphantram.Location = new System.Drawing.Point(252, 56);
            this.btnphantram.Margin = new System.Windows.Forms.Padding(1);
            this.btnphantram.Name = "btnphantram";
            this.btnphantram.Size = new System.Drawing.Size(60, 60);
            this.btnphantram.TabIndex = 69;
            this.btnphantram.Text = "%";
            this.btnphantram.UseVisualStyleBackColor = true;
            this.btnphantram.Click += new System.EventHandler(this.btnphantram_Click);
            // 
            // btndola
            // 
            this.btndola.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndola.Location = new System.Drawing.Point(190, 56);
            this.btndola.Margin = new System.Windows.Forms.Padding(1);
            this.btndola.Name = "btndola";
            this.btndola.Size = new System.Drawing.Size(60, 60);
            this.btndola.TabIndex = 68;
            this.btndola.Text = "$";
            this.btndola.UseVisualStyleBackColor = true;
            this.btndola.Click += new System.EventHandler(this.btndola_Click);
            // 
            // btntronmo
            // 
            this.btntronmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntronmo.Location = new System.Drawing.Point(500, 56);
            this.btntronmo.Margin = new System.Windows.Forms.Padding(1);
            this.btntronmo.Name = "btntronmo";
            this.btntronmo.Size = new System.Drawing.Size(60, 60);
            this.btntronmo.TabIndex = 73;
            this.btntronmo.Text = "(";
            this.btntronmo.UseVisualStyleBackColor = true;
            this.btntronmo.Click += new System.EventHandler(this.btntronmo_Click);
            // 
            // btnsao
            // 
            this.btnsao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsao.Location = new System.Drawing.Point(438, 56);
            this.btnsao.Margin = new System.Windows.Forms.Padding(1);
            this.btnsao.Name = "btnsao";
            this.btnsao.Size = new System.Drawing.Size(60, 60);
            this.btnsao.TabIndex = 72;
            this.btnsao.Text = "*";
            this.btnsao.UseVisualStyleBackColor = true;
            this.btnsao.Click += new System.EventHandler(this.btnsao_Click);
            // 
            // btnmu
            // 
            this.btnmu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmu.Location = new System.Drawing.Point(376, 56);
            this.btnmu.Margin = new System.Windows.Forms.Padding(1);
            this.btnmu.Name = "btnmu";
            this.btnmu.Size = new System.Drawing.Size(60, 60);
            this.btnmu.TabIndex = 71;
            this.btnmu.Text = "^";
            this.btnmu.UseVisualStyleBackColor = true;
            this.btnmu.Click += new System.EventHandler(this.btnmu_Click);
            // 
            // btntrondong
            // 
            this.btntrondong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntrondong.Location = new System.Drawing.Point(562, 56);
            this.btntrondong.Margin = new System.Windows.Forms.Padding(1);
            this.btntrondong.Name = "btntrondong";
            this.btntrondong.Size = new System.Drawing.Size(60, 60);
            this.btntrondong.TabIndex = 74;
            this.btntrondong.Text = ")";
            this.btntrondong.UseVisualStyleBackColor = true;
            this.btntrondong.Click += new System.EventHandler(this.btntrondong_Click);
            // 
            // UCKeyboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.btntrondong);
            this.Controls.Add(this.btntronmo);
            this.Controls.Add(this.btnsao);
            this.Controls.Add(this.btnmu);
            this.Controls.Add(this.btnbang);
            this.Controls.Add(this.btnphantram);
            this.Controls.Add(this.btndola);
            this.Controls.Add(this.btnthang);
            this.Controls.Add(this.btnacong);
            this.Controls.Add(this.btnthan);
            this.Controls.Add(this.btnphay);
            this.Controls.Add(this.btnchia);
            this.Controls.Add(this.btnchamphay);
            this.Controls.Add(this.btnhaicham);
            this.Controls.Add(this.btnvuongdong);
            this.Controls.Add(this.btndauhoi);
            this.Controls.Add(this.btncong);
            this.Controls.Add(this.btntru);
            this.Controls.Add(this.btnvuongmo);
            this.Controls.Add(this.btnnhohon);
            this.Controls.Add(this.btnhoac);
            this.Controls.Add(this.btnnhondong);
            this.Controls.Add(this.btnnhonmo);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btncapslock);
            this.Controls.Add(this.btnshift);
            this.Controls.Add(this.txtresult);
            this.Controls.Add(this.btnspace);
            this.Controls.Add(this.btnm);
            this.Controls.Add(this.btnn);
            this.Controls.Add(this.btnb);
            this.Controls.Add(this.btnv);
            this.Controls.Add(this.btnc);
            this.Controls.Add(this.btnx);
            this.Controls.Add(this.btnz);
            this.Controls.Add(this.btndot);
            this.Controls.Add(this.btnl);
            this.Controls.Add(this.btnk);
            this.Controls.Add(this.btnj);
            this.Controls.Add(this.btnh);
            this.Controls.Add(this.btng);
            this.Controls.Add(this.btnf);
            this.Controls.Add(this.btnd);
            this.Controls.Add(this.btns);
            this.Controls.Add(this.btna);
            this.Controls.Add(this.btnenter);
            this.Controls.Add(this.btnp);
            this.Controls.Add(this.btno);
            this.Controls.Add(this.btni);
            this.Controls.Add(this.btnu);
            this.Controls.Add(this.btny);
            this.Controls.Add(this.btnt);
            this.Controls.Add(this.btnr);
            this.Controls.Add(this.btne);
            this.Controls.Add(this.btnw);
            this.Controls.Add(this.btnq);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btndel);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Name = "UCKeyboard";
            this.Size = new System.Drawing.Size(875, 368);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btnp;
        private System.Windows.Forms.Button btno;
        private System.Windows.Forms.Button btni;
        private System.Windows.Forms.Button btnu;
        private System.Windows.Forms.Button btny;
        private System.Windows.Forms.Button btnt;
        private System.Windows.Forms.Button btnr;
        private System.Windows.Forms.Button btne;
        private System.Windows.Forms.Button btnw;
        private System.Windows.Forms.Button btnq;
        private System.Windows.Forms.Button btndot;
        private System.Windows.Forms.Button btnl;
        private System.Windows.Forms.Button btnk;
        private System.Windows.Forms.Button btnj;
        private System.Windows.Forms.Button btnh;
        private System.Windows.Forms.Button btng;
        private System.Windows.Forms.Button btnf;
        private System.Windows.Forms.Button btnd;
        private System.Windows.Forms.Button btns;
        private System.Windows.Forms.Button btna;
        private System.Windows.Forms.Button btnspace;
        private System.Windows.Forms.Button btnm;
        private System.Windows.Forms.Button btnn;
        private System.Windows.Forms.Button btnb;
        private System.Windows.Forms.Button btnv;
        private System.Windows.Forms.Button btnc;
        private System.Windows.Forms.Button btnx;
        private System.Windows.Forms.Button btnz;
        private System.Windows.Forms.Button btnshift;
        private System.Windows.Forms.Button btncapslock;
        private System.Windows.Forms.Button btnclear;
        public System.Windows.Forms.Button btnexit;
        public System.Windows.Forms.Button btnenter;
        public System.Windows.Forms.TextBox txtresult;
        private System.Windows.Forms.Button btnnhonmo;
        private System.Windows.Forms.Button btnnhondong;
        private System.Windows.Forms.Button btnhoac;
        private System.Windows.Forms.Button btnnhohon;
        private System.Windows.Forms.Button btnvuongmo;
        private System.Windows.Forms.Button btntru;
        private System.Windows.Forms.Button btncong;
        private System.Windows.Forms.Button btndauhoi;
        private System.Windows.Forms.Button btnvuongdong;
        private System.Windows.Forms.Button btnhaicham;
        private System.Windows.Forms.Button btnchamphay;
        private System.Windows.Forms.Button btnchia;
        private System.Windows.Forms.Button btnphay;
        private System.Windows.Forms.Button btnthan;
        private System.Windows.Forms.Button btnacong;
        private System.Windows.Forms.Button btnthang;
        private System.Windows.Forms.Button btnbang;
        private System.Windows.Forms.Button btnphantram;
        private System.Windows.Forms.Button btndola;
        private System.Windows.Forms.Button btntronmo;
        private System.Windows.Forms.Button btnsao;
        private System.Windows.Forms.Button btnmu;
        private System.Windows.Forms.Button btntrondong;
    }
}
