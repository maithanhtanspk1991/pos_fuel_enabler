﻿namespace POS.SerialPort
{
    internal class SerialPort
    {
        private System.IO.Ports.SerialPort port;

        public SerialPort()
        {
            //port = new System.IO.Ports.SerialPort("COM3", 8600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            //port.Open();
            //try
            //{
            //    string[] portNames = System.IO.Ports.SerialPort.GetPortNames();
            //    foreach (string portName in portNames)
            //    {
            //        port = new System.IO.Ports.SerialPort(portName, 8600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            //    }
            //    port.Open();
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}
        }

        public string sendToPager(string pagerID)
        {
            string sdata = "CPG," + pagerID + ",1,3";
            sdata += (char)10;
            port.Write(sdata);
            return sdata;
        }
    }
}