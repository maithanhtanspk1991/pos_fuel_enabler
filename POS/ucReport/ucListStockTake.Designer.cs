﻿namespace POS.ucReport
{
    partial class ucListStockTake
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstList = new System.Windows.Forms.ListView();
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colQuantityCurrent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colQuantityUpdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colQuantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEmployeeName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lstList
            // 
            this.lstList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDate,
            this.colItemName,
            this.colQuantityCurrent,
            this.colQuantityUpdate,
            this.colQuantity,
            this.colEmployeeName});
            this.lstList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstList.FullRowSelect = true;
            this.lstList.GridLines = true;
            this.lstList.HideSelection = false;
            this.lstList.Location = new System.Drawing.Point(0, 0);
            this.lstList.MultiSelect = false;
            this.lstList.Name = "lstList";
            this.lstList.Size = new System.Drawing.Size(647, 226);
            this.lstList.TabIndex = 49;
            this.lstList.UseCompatibleStateImageBehavior = false;
            this.lstList.View = System.Windows.Forms.View.Details;
            // 
            // colDate
            // 
            this.colDate.Text = "Date";
            this.colDate.Width = 95;
            // 
            // colItemName
            // 
            this.colItemName.Text = "Item Name";
            this.colItemName.Width = 80;
            // 
            // colQuantityCurrent
            // 
            this.colQuantityCurrent.Text = "Qty Current";
            this.colQuantityCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colQuantityCurrent.Width = 106;
            // 
            // colQuantityUpdate
            // 
            this.colQuantityUpdate.Text = "Qty Update";
            this.colQuantityUpdate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colQuantityUpdate.Width = 107;
            // 
            // colQuantity
            // 
            this.colQuantity.Text = "Quantity";
            this.colQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colQuantity.Width = 98;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Text = "Employee Name";
            this.colEmployeeName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colEmployeeName.Width = 87;
            // 
            // ucListStockTake
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstList);
            this.Name = "ucListStockTake";
            this.Size = new System.Drawing.Size(647, 226);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListView lstList;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colItemName;
        private System.Windows.Forms.ColumnHeader colQuantity;
        private System.Windows.Forms.ColumnHeader colEmployeeName;
        private System.Windows.Forms.ColumnHeader colQuantityCurrent;
        private System.Windows.Forms.ColumnHeader colQuantityUpdate;
    }
}
