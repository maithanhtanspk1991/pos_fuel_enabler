﻿using System;
using System.Data;
using System.Windows.Forms;
using POS.Class;

namespace POS.UCComponentReport
{
    public partial class ucListDelivery : UserControl
    {
        public ucListDelivery()
        {
            InitializeComponent();
        }

        public void LoadReport(Connection.Connection con, MoneyFortmat money, int isPickup, int isDelivery, string strCondition)
        {
            //con = new Connection.Connection();
            SetMultiLanguage();
            try
            {
                //con.Open();
                lstList.Items.Clear();
                string strSQL = "SELECT od.ts,od.tableID,c.Name,od.subtotal,od.completed " +
                                "FROM ordersdaily AS od INNER JOIN customers AS c on od.CustCode = c.memberNo " +
                                "where od.IsDelivery = " + isDelivery + " AND od.IsPickup = " + isPickup + " ";
                strSQL = strSQL + strCondition;
                strSQL = strSQL + "UNION ALL ";
                strSQL = strSQL + "SELECT od.ts,od.tableID,c.Name,od.subtotal,od.completed " +
                                "FROM ordersall AS od INNER JOIN customers AS c on od.CustCode = c.memberNo " +
                                "where od.IsDelivery = " + isDelivery + " AND od.IsPickup = " + isPickup + " ";
                strSQL = strSQL + strCondition;
                DataTable dtSource = con.Select(strSQL);
                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(drItem["ts"].ToString());
                    li.SubItems.Add(drItem["tableID"].ToString());
                    li.SubItems.Add(drItem["Name"].ToString());
                    li.SubItems.Add(money.Format2(drItem["subtotal"].ToString()));
                    li.SubItems.Add(Convert.ToInt32(drItem["completed"].ToString()) == 1 ? "DONE" : "NONE");
                    lstList.Items.Add(li);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                //con.Close();
            }
        }
        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                colDate.Text = "Ngày";
                colOrderID.Text = "Mã sản phẩm";
                colCustName.Text = "Tên khách hàng";
                colSubtotal.Text = "Thành tiền";
                colStatus.Text = "Trạng thái";
                return;
            }
        }
    }
}