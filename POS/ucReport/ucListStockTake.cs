﻿using System;
using System.Data;
using System.Windows.Forms;

namespace POS.ucReport
{
    public partial class ucListStockTake : UserControl
    {
        public ucListStockTake()
        {
            InitializeComponent();
        }

        public void LoadReport(Connection.Connection mConnection, Class.MoneyFortmat money, string weekfrom, string weekto)
        {
            SetMultiLanguage();
            try
            {
                lstList.Items.Clear();
                string strSQL = "CALL SP_STOCK_TAKE_LIST('" + weekfrom + "','" + weekto + "',1,10,0,0);";
                DataTable dtSource = mConnection.Select(strSQL);
                foreach (DataRow drItem in dtSource.Rows)
                {
                    ListViewItem li = new ListViewItem(Convert.ToDateTime(drItem["ts"].ToString()).ToString("dd/MM/yyyy hh:mm:ss"));
                    li.SubItems.Add(drItem["ItemName"].ToString());
                    li.SubItems.Add(drItem["unitUpdate"].ToString());
                    li.SubItems.Add(drItem["unitCurrent"].ToString());
                    li.SubItems.Add(drItem["Quantity"].ToString());
                    li.SubItems.Add(drItem["Name"].ToString());
                    lstList.Items.Add(li);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                //con.Close();
            }
        }

        private void SetMultiLanguage()
        {
            Class.ReadConfig mReadConfig = new Class.ReadConfig();
            if (mReadConfig.LanguageCode.Equals("vi"))
            {
                colDate.Text = "Ngày";
                colEmployeeName.Text = "Tên nhân viên";
                colItemName.Text = "Tên sản phẩm";
                colQuantity.Text="Số lượng";
                colQuantityCurrent.Text = "Số lượng hiện tại";
                colQuantityUpdate.Text = "Số lượng cập nhật";
                return;
            }
        }
    }
}