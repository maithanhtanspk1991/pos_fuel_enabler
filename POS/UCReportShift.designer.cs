﻿namespace POS
{
    partial class UCReportShift
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbto = new System.Windows.Forms.Label();
            this.lbfrom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbshiftid = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTotalAccount = new System.Windows.Forms.Label();
            this.labelCashAccount = new System.Windows.Forms.Label();
            this.labelCardAccount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalCard = new System.Windows.Forms.Label();
            this.ListViewCard = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbtotalcash = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lbAccount = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbSafeDrop = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbPayOut = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbCashOut = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbCashIn = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbcashsales = new System.Windows.Forms.Label();
            this.lbcardsales = new System.Windows.Forms.Label();
            this.lbRefunc = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbtotalgst = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbCashFloatOut = new System.Windows.Forms.Label();
            this.lbtotaldiscount = new System.Windows.Forms.Label();
            this.lbCashFloatIn = new System.Windows.Forms.Label();
            this.lbTotalsales = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbto);
            this.panel1.Controls.Add(this.lbfrom);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lbshiftid);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(882, 80);
            this.panel1.TabIndex = 0;
            // 
            // lbto
            // 
            this.lbto.AutoSize = true;
            this.lbto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbto.Location = new System.Drawing.Point(512, 54);
            this.lbto.Name = "lbto";
            this.lbto.Size = new System.Drawing.Size(0, 18);
            this.lbto.TabIndex = 45;
            // 
            // lbfrom
            // 
            this.lbfrom.AutoSize = true;
            this.lbfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfrom.Location = new System.Drawing.Point(334, 54);
            this.lbfrom.Name = "lbfrom";
            this.lbfrom.Size = new System.Drawing.Size(88, 18);
            this.lbfrom.TabIndex = 44;
            this.lbfrom.Text = "Date From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(265, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 42;
            this.label2.Text = "From : ";
            // 
            // lbshiftid
            // 
            this.lbshiftid.AutoSize = true;
            this.lbshiftid.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbshiftid.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbshiftid.Location = new System.Drawing.Point(109, 47);
            this.lbshiftid.Name = "lbshiftid";
            this.lbshiftid.Size = new System.Drawing.Size(38, 29);
            this.lbshiftid.TabIndex = 41;
            this.lbshiftid.Text = "ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(3, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 29);
            this.label11.TabIndex = 40;
            this.label11.Text = "Shift ID :";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(882, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shift Report";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 80);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(882, 475);
            this.panel2.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.labelTotalAccount);
            this.groupBox3.Controls.Add(this.labelCashAccount);
            this.groupBox3.Controls.Add(this.labelCardAccount);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(383, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 154);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Account Payment";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 105);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 20);
            this.label23.TabIndex = 45;
            this.label23.Text = "Total : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 20);
            this.label16.TabIndex = 45;
            this.label16.Text = "Cash : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 45;
            this.label10.Text = "Card : ";
            // 
            // labelTotalAccount
            // 
            this.labelTotalAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAccount.Location = new System.Drawing.Point(97, 110);
            this.labelTotalAccount.Name = "labelTotalAccount";
            this.labelTotalAccount.Size = new System.Drawing.Size(259, 20);
            this.labelTotalAccount.TabIndex = 46;
            this.labelTotalAccount.Text = "0.00";
            this.labelTotalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCashAccount
            // 
            this.labelCashAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashAccount.Location = new System.Drawing.Point(97, 70);
            this.labelCashAccount.Name = "labelCashAccount";
            this.labelCashAccount.Size = new System.Drawing.Size(259, 20);
            this.labelCashAccount.TabIndex = 46;
            this.labelCashAccount.Text = "0.00";
            this.labelCashAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCardAccount
            // 
            this.labelCardAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardAccount.Location = new System.Drawing.Point(97, 30);
            this.labelCardAccount.Name = "labelCardAccount";
            this.labelCardAccount.Size = new System.Drawing.Size(259, 20);
            this.labelCardAccount.TabIndex = 46;
            this.labelCardAccount.Text = "0.00";
            this.labelCardAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblTotalCard);
            this.groupBox2.Controls.Add(this.ListViewCard);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(384, 160);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 244);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Card";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 54;
            this.label4.Text = "Total:";
            // 
            // lblTotalCard
            // 
            this.lblTotalCard.AutoSize = true;
            this.lblTotalCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCard.Location = new System.Drawing.Point(321, 221);
            this.lblTotalCard.Name = "lblTotalCard";
            this.lblTotalCard.Size = new System.Drawing.Size(54, 20);
            this.lblTotalCard.TabIndex = 54;
            this.lblTotalCard.Text = "Total:";
            // 
            // ListViewCard
            // 
            this.ListViewCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8});
            this.ListViewCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewCard.FullRowSelect = true;
            this.ListViewCard.GridLines = true;
            this.ListViewCard.HideSelection = false;
            this.ListViewCard.HoverSelection = true;
            this.ListViewCard.Location = new System.Drawing.Point(3, 24);
            this.ListViewCard.MultiSelect = false;
            this.ListViewCard.Name = "ListViewCard";
            this.ListViewCard.Size = new System.Drawing.Size(376, 173);
            this.ListViewCard.TabIndex = 53;
            this.ListViewCard.UseCompatibleStateImageBehavior = false;
            this.ListViewCard.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Card Name";
            this.columnHeader7.Width = 210;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Subtotal";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader8.Width = 160;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbtotalcash);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.lbAccount);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lbSafeDrop);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbPayOut);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lbCashOut);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbCashIn);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbcashsales);
            this.groupBox1.Controls.Add(this.lbcardsales);
            this.groupBox1.Controls.Add(this.lbRefunc);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.lbtotalgst);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.lbCashFloatOut);
            this.groupBox1.Controls.Add(this.lbtotaldiscount);
            this.groupBox1.Controls.Add(this.lbCashFloatIn);
            this.groupBox1.Controls.Add(this.lbTotalsales);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 402);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sales";
            // 
            // lbtotalcash
            // 
            this.lbtotalcash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lbtotalcash.Location = new System.Drawing.Point(195, 375);
            this.lbtotalcash.Name = "lbtotalcash";
            this.lbtotalcash.Size = new System.Drawing.Size(171, 20);
            this.lbtotalcash.TabIndex = 57;
            this.lbtotalcash.Text = "0.00";
            this.lbtotalcash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(6, 375);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(134, 20);
            this.label20.TabIndex = 56;
            this.label20.Text = "Total Cash:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbAccount
            // 
            this.lbAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccount.Location = new System.Drawing.Point(157, 215);
            this.lbAccount.Name = "lbAccount";
            this.lbAccount.Size = new System.Drawing.Size(212, 20);
            this.lbAccount.TabIndex = 52;
            this.lbAccount.Text = "0.00";
            this.lbAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 215);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 20);
            this.label13.TabIndex = 51;
            this.label13.Text = "Account:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbSafeDrop
            // 
            this.lbSafeDrop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSafeDrop.Location = new System.Drawing.Point(157, 327);
            this.lbSafeDrop.Name = "lbSafeDrop";
            this.lbSafeDrop.Size = new System.Drawing.Size(212, 20);
            this.lbSafeDrop.TabIndex = 6;
            this.lbSafeDrop.Text = "0.00";
            this.lbSafeDrop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 327);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Safe Drop:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPayOut
            // 
            this.lbPayOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPayOut.Location = new System.Drawing.Point(157, 302);
            this.lbPayOut.Name = "lbPayOut";
            this.lbPayOut.Size = new System.Drawing.Size(212, 20);
            this.lbPayOut.TabIndex = 6;
            this.lbPayOut.Text = "0.00";
            this.lbPayOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 302);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "Pay Out:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbCashOut
            // 
            this.lbCashOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCashOut.Location = new System.Drawing.Point(157, 273);
            this.lbCashOut.Name = "lbCashOut";
            this.lbCashOut.Size = new System.Drawing.Size(212, 20);
            this.lbCashOut.TabIndex = 6;
            this.lbCashOut.Text = "0.00";
            this.lbCashOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 273);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cash Out:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbCashIn
            // 
            this.lbCashIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCashIn.Location = new System.Drawing.Point(157, 244);
            this.lbCashIn.Name = "lbCashIn";
            this.lbCashIn.Size = new System.Drawing.Size(212, 20);
            this.lbCashIn.TabIndex = 6;
            this.lbCashIn.Text = "0.00";
            this.lbCashIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Cash In:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 351);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = " Till Balance:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "Cash Float In:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Total Sales:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbcashsales
            // 
            this.lbcashsales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcashsales.Location = new System.Drawing.Point(157, 165);
            this.lbcashsales.Name = "lbcashsales";
            this.lbcashsales.Size = new System.Drawing.Size(212, 20);
            this.lbcashsales.TabIndex = 4;
            this.lbcashsales.Text = "0.00";
            this.lbcashsales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbcardsales
            // 
            this.lbcardsales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcardsales.Location = new System.Drawing.Point(157, 137);
            this.lbcardsales.Name = "lbcardsales";
            this.lbcardsales.Size = new System.Drawing.Size(212, 20);
            this.lbcardsales.TabIndex = 4;
            this.lbcardsales.Text = "0.00";
            this.lbcardsales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbRefunc
            // 
            this.lbRefunc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRefunc.Location = new System.Drawing.Point(161, 188);
            this.lbRefunc.Name = "lbRefunc";
            this.lbRefunc.Size = new System.Drawing.Size(208, 20);
            this.lbRefunc.TabIndex = 4;
            this.lbRefunc.Text = "0.00";
            this.lbRefunc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(6, 188);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(134, 20);
            this.label31.TabIndex = 2;
            this.label31.Text = "Refund:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 82);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Total Discount:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbtotalgst
            // 
            this.lbtotalgst.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotalgst.Location = new System.Drawing.Point(157, 111);
            this.lbtotalgst.Name = "lbtotalgst";
            this.lbtotalgst.Size = new System.Drawing.Size(212, 20);
            this.lbtotalgst.TabIndex = 4;
            this.lbtotalgst.Text = "0.00";
            this.lbtotalgst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 111);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(134, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Total GST:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbCashFloatOut
            // 
            this.lbCashFloatOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCashFloatOut.Location = new System.Drawing.Point(157, 351);
            this.lbCashFloatOut.Name = "lbCashFloatOut";
            this.lbCashFloatOut.Size = new System.Drawing.Size(212, 20);
            this.lbCashFloatOut.TabIndex = 4;
            this.lbCashFloatOut.Text = "0.00";
            this.lbCashFloatOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtotaldiscount
            // 
            this.lbtotaldiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotaldiscount.Location = new System.Drawing.Point(157, 82);
            this.lbtotaldiscount.Name = "lbtotaldiscount";
            this.lbtotaldiscount.Size = new System.Drawing.Size(212, 20);
            this.lbtotaldiscount.TabIndex = 4;
            this.lbtotaldiscount.Text = "0.00";
            this.lbtotaldiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCashFloatIn
            // 
            this.lbCashFloatIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCashFloatIn.Location = new System.Drawing.Point(154, 25);
            this.lbCashFloatIn.Name = "lbCashFloatIn";
            this.lbCashFloatIn.Size = new System.Drawing.Size(212, 20);
            this.lbCashFloatIn.TabIndex = 4;
            this.lbCashFloatIn.Text = "0.00";
            this.lbCashFloatIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTotalsales
            // 
            this.lbTotalsales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalsales.Location = new System.Drawing.Point(157, 51);
            this.lbTotalsales.Name = "lbTotalsales";
            this.lbTotalsales.Size = new System.Drawing.Size(212, 20);
            this.lbTotalsales.TabIndex = 4;
            this.lbTotalsales.Text = "0.00";
            this.lbTotalsales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 137);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(134, 20);
            this.label27.TabIndex = 2;
            this.label27.Text = "Card:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 165);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(134, 20);
            this.label29.TabIndex = 2;
            this.label29.Text = "Cash:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 500);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(882, 55);
            this.button1.TabIndex = 4;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UCReportShift
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "UCReportShift";
            this.Size = new System.Drawing.Size(882, 555);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbshiftid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbAccount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbCashIn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbcashsales;
        private System.Windows.Forms.Label lbcardsales;
        private System.Windows.Forms.Label lbRefunc;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbtotalgst;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbtotaldiscount;
        private System.Windows.Forms.Label lbTotalsales;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelTotalAccount;
        private System.Windows.Forms.Label labelCashAccount;
        private System.Windows.Forms.Label labelCardAccount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblTotalCard;
        private System.Windows.Forms.ListView ListViewCard;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbfrom;
        private System.Windows.Forms.Label lbto;
        private System.Windows.Forms.Label lbSafeDrop;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbPayOut;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbCashOut;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbCashFloatOut;
        private System.Windows.Forms.Label lbCashFloatIn;
        private System.Windows.Forms.Label lbtotalcash;
        private System.Windows.Forms.Label label20;
    }
}
